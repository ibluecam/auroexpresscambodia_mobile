


                
                    <?php
                        if ($_SESSION['mobile']==false) {
                    ?>
                    <div id="second_search">
                        <div id="search_title" class="clearfix">
                            <p><a href="#" class="current" id="quick_search">Quick Search</a> <span>&nbsp;&nbsp; | &nbsp;&nbsp;</span> <a href="#" id="ad_search">Advance Search</a></p>
                        </div>
                        <div id="search_wrap" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <select class="make formSelection" name="make" id="make" style="width:225px;">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>


                                <select class="model formSelection" name="model" id="model">
                                    <option value="" class="notranslate">- Model -</option>
                                </select>
                                
                                    <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                    <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div>
                        <!--======================== ADVANCE SEARCH =====================================-->
                        <div id="advance_search" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" name="search" value="advance"/>
                                <select class="equipment_first category formSelection" name="category" id="category_adv">
                                    <?php 
                                        include('ajax/load_saved_category.php');
                                    ?>
                                </select>
                                
                                <select class="equipment make formSelection" name="make" id="make_adv">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                                
                                
                                <select class="equipment model formSelection" name="model" id="model_adv">
                                    <option value="">-- Model --</option>
                                </select>
                                <select class="equipment condition formSelection " name="condition" id="condition_adv">
                                    <option value="">-- Condition --</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                </select>

                                <!--<span class="displacement"> Displacement </span>-->
                                <!--<select class="any formSelection " name="engine_volume_from" id="engine_volume_from_adv">
                                    <option value="">--Max Cylinder Capacity--</option>
                                    <?php 
                                        //for($i=500;$i<=6000;$i+=100){
                                            //echo "<option value='{$i}'>{$i}</option>";
                                       // }
                                    ?>
                                </select>-->
 
                                <select class="year formSelection" name="year_from" id="year_from_adv">
                                    <option value="">-- From year --</option>
                                     <?php
                                     for($i=1990;$i<=2014;$i++){
                                         echo "<option value='$i'>$i</option>";
                                     }
                                     ?>
                                </select>
                                     
                                <select class="year toSelection" name="year_to" id="year_to_adv">
                                    <option value="">-- To year --</option>
                                     <?php
                                     for($i=1990;$i<=2014;$i++){
                                         echo "<option value='$i'>$i</option>";
                                     }
                                     ?>
                                </select>
                                
                                <!--<span style="margin-left:10px;"> Price </span>-->
                                <input type="text" class="price" name="price_from" value="<?php echo $price_from;?>" placeholder="Min price" id="price_from_adv" onkeypress="return isNumberKey(event)" />
                                <!--<span class="sep"> - </span>-->

                                <input type="text" class="price" name="price_to" value="<?php echo $price_to;?>" placeholder="Max price" id="price_to_adv" onkeypress="return isNumberKey(event)" />
                                
                                <select class="equipment steering formSelection" name="steering" id="steering_adv">
                                    <option selected value="">-- Bucket Size --</option>
                                    
                                </select>
                                <select class="equiment_country formSelection " name="country" id="country_adv">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>
                                
                                <input type="text" class="text_search" name="customSearch" placeholder="Keywords" id="customSearch_adv" style="margin-left:3px; width:223px;" />

                                <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div>                  
                    </div>
                    <!-- <table border="0" cellspacing="0" cellpadding="0">
                        <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                        
                          <tr>
                        
                            <td class="top condition">                                
                                <select class="formSelection condition" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>                                
                            </td>
                            <td class="top category">                                
                                <select class="formSelection category" name="category" id="category">
                                    <?php 
                                        include('ajax/load_category.php');
                                    ?>
                                </select>                                
                            </td>
                            <td class="top category2">                                
                                <select class="formSelection" name="category2" id="category2">
                                    <option value="">- Category 2 -</option>
                                    
                                </select>                                
                            </td>
                            <td>
                                <select class="formSelection category2" name="make" id="make">
                                    <?php 
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                            </td>
                            <td class="model">
                                <select class="formSelection model"  name="model" id="model">
                                    <option value="">- Model -</option>                                    
                                </select>
                            </td>
                            <td class="country">
                                <select class="formSelection country" name="country" id="country">
                                    <?php 
                                        include('ajax/load_country.php');
                                    ?>
                                </select>
                            </td>
                            <td class="year_truck">
                                <div id="divYear">
                                    
                                    <select class="fromSelection year" name="year_from" id="year_from">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                    ~
                                    <select class="toSelection year" name="year_to" id="year_to">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                </div>
                                
                            </td>
                            <td class="search_truck">
                                <input type="text" class="textBox_equipment" name="customSearch" placeholder="Model, name..." id="customSearch" />                      
                                <input type="submit" value="" class="search" />
                                
                            </td>
                          </tr>
                        
                    </table> -->
                    <?php }
                    else {
                        ?>
                    <div class="new_searchwrap">
                        <div class="newsearch">

                        <table border="0" cellspacing="0" cellpadding="0">
                        <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                        
                        <tr>
                        <!-------------------------- BLOCK 1 ------------------------------>
                            <td class="top condition">                                
                                <select class="formSelection condition" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="top category">                                
                                <select class="formSelection category" name="category" id="category">
                                    <?php 
                                        include('ajax/load_category.php');
                                    ?>
                                </select>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="top category2">                                
                                <select class="formSelection" name="category2" id="category2">
                                    <option value="">- Category 2 -</option>
                                    
                                </select>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select class="formSelection category2" name="make" id="make">
                                    <?php 
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="model">
                                <select class="formSelection model"  name="model" id="model">
                                    <option value="">- Model -</option>                                    
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="country">
                                <select class="formSelection country" name="country" id="country">
                                    <?php 
                                        include('ajax/load_country.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="year_truck">
                                <div id="divYear">
                                    
                                    <select class="fromSelection year" name="year_from" id="year_from">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                    <select class="toSelection year_end" name="year_to" id="year_to">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="search_truck">
                                <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />                 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="" class="search" />
                            </td>
                        </tr>                        
                    </table>
                    </div>
                </div>
                        <?php

                    }
                    ?>
                    </form>
                