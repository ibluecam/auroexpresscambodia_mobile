<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class usercarmanagereditdetails {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $car_id;
    private $details_list;
    private $car;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function usercarmanagereditdetails($lang=array()){
        $this->lang = $lang;
        // validate if car id has been declared and load list.
        $this->car_id = trim($_GET['cid']);
        $this->car = array();
        $this->details_list = array();
        
        if( empty($this->car_id) ){
            $this->media_list = array();
            $this->form_message = $this->lang['EDIT_DETAILS_INVALID_VEHICLE_ID_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        else {
            // handle update form.
            if( isset($_POST['savebtn']) ){
                $this->handleUpdateForm();
            }
            
            // load details list.
            $this->loadDetailsList();
        }
    }
    
    /**
     * Private method: handle update form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleUpdateForm(){
        $engine_size    = trim($_POST['engineInput']);
        $trim           = trim($_POST['trimInput']);
        $type           = trim($_POST['typeInput']);
        $gear           = trim($_POST['gearInput']);
        $mot            = trim($_POST['motInput']);
        $fuel           = trim($_POST['fuelInput']);
        $color          = trim($_POST['colorInput']);
        $prev_owner     = (int)$_POST['prevOwnerInput'];
        $last_service   = trim($_POST['lastServiceInput']);
        $tax_band       = trim($_POST['taxInput']);
        $top_speed      = trim($_POST['topSpeedInput']);
        $torque         = (int)$_POST['torqueInput'];
        $power          = (int)$_POST['powerInput'];
        $transmission   = trim($_POST['transmissionInput']);
        $html           = stripslashes($_POST['htmlInput']);
        
        if( empty($this->car_id) ){
            echo 'wtf';
            return;
        }
        
        // validate.
        $undefined = $this->lang['EDIT_DETAILS_UNDEFINED_LABEL'];
        
        if( empty($engine_size) ){
            $engine_size = $undefined;
        }
        if( empty($trim) ){
            $trim = $undefined;
        }
        if( empty($type) ){
            $type = $undefined;
        }
        if( empty($gear) ){
            $gear = $undefined;
        }
        if( empty($mot) ){
            $mot = $undefined;
        }
        if( empty($fuel) ){
            $fuel = $undefined;
        }
        if( empty($color) ){
            $color = $undefined;
        }
        if( empty($last_service) ){
            $last_service = $undefined;
        }
        if( empty($tax_band) ){
            $tax_band = $undefined;
        }
        if( empty($top_speed) ){
            $top_speed = $undefined;
        }
        if( empty($transmission) ){
            $transmission = $undefined;
        }
        if( $prev_owner < 1 ){
            $prev_owner = '0';
        }
        if( $torque < 1 ){
            $torque = '0';
        }
        if( $power < 1 ){
            $power = '0';
        }
        
        // update.
        require_once BASE_CLASS  . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $engine_size    = @mysql_real_escape_string($engine_size);
        $trim           = @mysql_real_escape_string($trim);
        $type           = @mysql_real_escape_string($type);
        $gear           = @mysql_real_escape_string($gear);
        $mot            = @mysql_real_escape_string($mot);
        $fuel           = @mysql_real_escape_string($fuel);
        $color          = @mysql_real_escape_string($color);
        $prev_owner     = @mysql_real_escape_string($prev_owner);
        $last_service   = @mysql_real_escape_string($last_service);
        $tax_band       = @mysql_real_escape_string($tax_band);
        $top_speed      = @mysql_real_escape_string($top_speed);
        $torque         = @mysql_real_escape_string($torque);
        $power          = @mysql_real_escape_string($power);
        $transmission   = @mysql_real_escape_string($transmission);
        $html           = @mysql_real_escape_string($html);
        
        if( !@mysql_query("UPDATE `car_details` SET `engine_size`='$engine_size',
                                                    `trim`='$trim',
                                                    `type`='$type',
                                                    `gear`='$gear',
                                                    `fuel`='$fuel',
                                                    `color`='$color',
                                                    `prev_owners`='$prev_owner',
                                                    `last_service`='$last_service',
                                                    `mot`='$mot',
                                                    `tax_band`='$tax_band',
                                                    `top_speed`='$top_speed',
                                                    `engine_torque_rpm`='$torque',
                                                    `engine_power_kw`='$power',
                                                    `transmission_type`='$transmission',
                                                    `html`='$html' WHERE `car_id`='$this->car_id' LIMIT 1;") ){
            $cnx->close();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to update vehicle details due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['EDIT_DETAILS_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['EDIT_DETAILS_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: load details list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadDetailsList(){
        // load vehicle info data first.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $this->car_id = @mysql_real_escape_string($this->car_id);
        
        if( !$sql = @mysql_query("SELECT `id`,`maker`,`model`,`year` FROM `car` WHERE `id`='$this->car_id' LIMIT 1;") ){
            $cnx->close();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load vehicle information due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['EDIT_DETAILS_LOADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        
        $this->car['id'] = $r['id'];
        $this->car['maker'] = stripslashes($r['maker']);
        $this->car['model'] = stripslashes($r['model']);
        $this->car['year'] = $r['year'];
        
        @mysql_free_result($sql);
        
        if( !$sql = @mysql_query("SELECT * FROM `car_details` WHERE `car_id`='$this->car_id' LIMIT 1;") ){
            $cnx->close();
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load vehicle information due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->car = array();
            $this->form_message = $this->lang['EDIT_DETAILS_LOADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            return;
        }
        
        $r = @mysql_fetch_assoc($sql);
        
        $this->details_list['id']                   = (int)$r['id'];
        $this->details_list['car_id']               = $r['car_id'];
        $this->details_list['engine_size']          = stripslashes($r['engine_size']);
        $this->details_list['trim']                 = stripslashes($r['trim']);
        $this->details_list['type']                 = stripslashes($r['type']);
        $this->details_list['gear']                 = stripslashes($r['gear']);
        $this->details_list['fuel']                 = stripslashes($r['fuel']);
        $this->details_list['color']                = stripslashes($r['color']);
        $this->details_list['prev_owners']          = stripslashes($r['prev_owners']);
        $this->details_list['last_service']         = stripslashes($r['last_service']);
        $this->details_list['mot']                  = stripslashes($r['mot']);
        $this->details_list['tax_band']             = stripslashes($r['tax_band']);
        $this->details_list['top_speed']            = stripslashes($r['top_speed']);
        $this->details_list['engine_torque_rpm']    = stripslashes($r['engine_torque_rpm']);
        $this->details_list['engine_power_kw']      = stripslashes($r['engine_power_kw']);
        $this->details_list['transmission_type']    = stripslashes($r['transmission_type']);
        $this->details_list['html']                 = stripslashes($r['html']);
        
        @mysql_free_result($sql);
        $cnx->close();        
        return;
    }
    
    /**
     * Public method: get vehicle name
     * @return array
     */
    public function getVehicle(){
        return $this->car;
    }
    
    /**
     * Public method: get vehicle details
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getVehicleDetails(){
        return $this->details_list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}