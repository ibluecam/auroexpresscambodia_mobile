var loading_html='<table class="city_table" id="city_table"><tr><th></th><th>N<sup>o</sup></th><th>City Name</th><th>Number of Products</th><th></th></tr><table>';
loading_html+='<img id="loading_image" src="images/loading_75.gif" style="position:absolute;left:0;right:0;margin:auto;z-index:999;margin-top:20px;"/>';
$(document).ready(function(){
    showLoading();
    if(country!=''){
        $("#countrySelect").val(country);
    }
    $("#city_table_wrapper").load("ajax/admin_saved_city_table.php?country="+country, function(e){
        load_ready();
    });
    $(document.body).on('click', "#delete_selected", function(e){
        if(confirm("Are you sure want to delete all products with the selected cities?")==false) return false;
    });
    $(document.body).on('click', ".edit_button", function(e){
        var current_id=$(this).attr('edit_id');
        var current_city=$("#city_"+current_id).text();

        if(!$("#new_city_"+current_id).length){
            $("#city_"+current_id).html(function(e){
                return "<input type='text' class='city_text' value='"+current_city+"' id='new_city_"+current_id+"' name='new_city["+current_id+"]'/>";
            });
        }
        return false;
    });
    $(document.body).on('change', "#countrySelect", function(e){
        showLoading();
        var currentCity=$(this).val();
        $("#city_table_wrapper").load("ajax/admin_saved_city_table.php?country="+currentCity, function(e){
            //load_ready();
        });
    });
    $(document.body).on('click', ".delete_button", function(e){
        
        $(".city_check").prop('checked', false);
        var delete_id=$(this).attr('delete_id');
        $("#city_check_"+delete_id).prop('checked', true);
        $("#delete_selected").click();
    });
});

function load_ready(){   
    
   
}

function showLoading(){
        
        $("#city_table_wrapper").html(loading_html);
    }
