<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$trad_list=$_CLASS->get_tradlist();
$pagination=$_CLASS->pagination_html;
$total=$_CLASS->gettotal();
$total_page=$_CLASS->gettotalpage();
$current_page=$_CLASS->getcurrentpage();

?>



<script type="text/javascript">
   $(document).ready(function(e) {
      
        $('#link').click(function(e){
            $('#submit').click();
        });

        $('#mlink').click(function(e){
            $('#mywrite').click();

        });

        var option='<?php if(isset($_GET['category'])) echo $_GET['category']; ?>';
        var search_option='<?php if(isset($_GET['searchoption'])) echo $_GET['searchoption']; ?>';
        var mytext='<?php if(isset($_GET['mywrite'])) 
                            {echo "";}
                          elseif(isset($_GET['text']))
                            {echo $_GET['text'];} ?>';

         $('#mytext').val(mytext);
         $('#searchoption').val(search_option);  
         if(option!=""){
         $("[name=category]").val([option]);}
		
   });
</script>
 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > Trade Infomation</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Trade Infomation</p>
                </div>
                <div id="auto_detail" class="clearfix">
                	<div id="text">
                   		<p class="first">You may discuss about all kinds of information including import/export business.</p>
                        <p>This is a section where you can post information yourself and share with those in need!</p>
                    </div>
                    <div id="button">
                    	<div id="article">
                        	<a href="<?php echo BASE_RELATIVE;?>trade-info-write"><img src="<?php echo BASE_RELATIVE;?>images/community/write.png"  class="linkfade"/></a>
                        </div>
                        <div id="view">
                        	<a id="mlink" href="#"><img src="<?php echo BASE_RELATIVE;?>images/community/view.png" class="linkfade"/></a>
                        </div>
                    </div>
                </div>
                <div id="auto_form">
                	<form method="get">
                	<div id="radio_wrap" class="clearfix">
                    	<div id="all"><input id="chkall" type="radio" value="All" name="category"  checked="check"/> All</div>
                        <div id="customs"><input id="chkcus" type="radio" value="Custome" name="category" /> Customs</div>
                        <div id="shipping"><input id="chkchip" type="radio" value="Shipping" name="category" /> Shipping</div>
                        <div id="photo"><input id="chkimp" type="radio" value="Import" name="category" /> Import</div>
                        <div id="photo"><input id="chkexp" type="radio" value="Export" name="category" /> Export</div>
                    </div>
                    <div id="textbox_wrap" class="clearfix">
                    	<select class="topic" name="searchoption" id="searchoption">
                            <option value="">Any</option>
                            <option value="topic">Topic</option>
                            <option value="writer">Writer</option>
                        </select>
                        <input type="text" id="mytext" name="text" class="trade_search" />
                        <div id="btn_search">
                        	<a href="#" id="link"><img src="<?php echo BASE_RELATIVE;?>images/community/search.png" class="linkfade"/></a>
                            <input type="submit" id="submit" name="submit" style="display:none;"/>
                            <input type="submit" id="mywrite" name="mywrite" style="display:none;"/>
                        </div>
                   	</div>
                    </form>
                    <div id="list_detail">
                    	<ul class="clearfix title">
                        	<li class="trade_no blue_color">No.</li>
                            <li class="trade_category blue_color">Category</li>
                            <li class="trade_topic blue_color">Topic</li>
                            <li class="trade_writen blue_color">Written by</li>
                            <li class="trade_register blue_color">Registered Date</li>
                        </ul>

                        <?php 
                        if(sizeof($trad_list)==0)
                        {
                            echo "<div class ='bg_color'>  
                            :: We are very sorry. There's no available data at the moment. ::</div>";
                        }
                        else{
                        foreach($trad_list as $trad)
                        {
                            ?>
                        <div class ='bg_color'>
                        <ul class="clearfix">
                        	<li class="trade_no"><?php echo $trad['trade_info_id'];?></li>
                            <li class="trade_category"><?php echo $trad['category'];?></li>
                            <li class="trade_topic">
                                <a href="<?php if( $_SESSION['log_id']==$trad['writer'])
                                            {
                                               echo BASE_RELATIVE."trade-info-edit?wid=".$trad['trade_info_id']; 
                                            }
                                            else
                                            {
                                              echo BASE_RELATIVE."trade-view?wid=".$trad['trade_info_id'];
                                            }?>"
                                style="color:black;">  
                                        
                                    <?php echo $trad['topic'];?>
                                </a>
                            </li>
                            <li class="trade_writen"><?php echo $trad['name'];?></li>
                            <li class="trade_register">
                                <?php echo date("F. j. Y",strtotime($trad['date']));?>
                            </li>
                        </ul>
                        </div>
                        <?php }}?>
                    </div>
                </div>
                </br>
                <span class="page-count">Total :<?php echo $total.' item   page: '.$current_page.' / '.$total_page;?></span>
                <div id="pagination">
                	<?php 
      
                    echo $pagination;
                        
                    ?>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  