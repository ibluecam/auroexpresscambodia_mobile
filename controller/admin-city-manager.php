<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class admincitymanager {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $register_type_list;
    //private $city_list;
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function admincitymanager($lang=array()){
        $this->lang = $lang;
        //if user submit press button save changes
        if(isset($_POST['save_changes'])){
            $this->handleSaveChanges();
        }
        //if user submit press button delete selected
        if(isset($_POST['delete_selected'])){
            $this->handleDelete();
        }

        //$this->loadCityList();
        
    }
    
    private function handleSaveChanges(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        if(isset($_POST['old_city'])) $old_cities=$_POST['old_city']; else $old_cities=array('');
        if(isset($_POST['new_city'])) $new_cities=$_POST['new_city']; else $new_cities=array('');
        if(isset($_POST['countrySelect'])) $countrySelect=$_POST['countrySelect']; else $countrySelect='';
        
        $in_cities=array();
        $sql= "UPDATE `product` SET `city` = CASE `city` ";
        foreach($new_cities as $key=>$value){
            array_push($in_cities, $old_cities[$key]);
            $sql.="WHEN '{$old_cities[$key]}' THEN '{$value}' ";
        }
        //Variable of cities that were changed
        $in_old_cities = implode("', '", $in_cities);
        $sql .= "END ";
        //Change only cities that were edit
        $sql .= "WHERE city IN ('$in_old_cities') AND location='$countrySelect'";
        //echo $sql;
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            echo mysql_error();
            $cnx->close();
            
            LogReport::write('Unable to change city due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Cities were changed successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    private function handleDelete(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        //code here
        if(isset($_POST['cityCheck'])) $cityCheck=$_POST['cityCheck']; else $cityCheck=array();
        if(isset($_POST['countrySelect'])) $countrySelect=$_POST['countrySelect']; else $countrySelect='';
        
        if(count($cityCheck)<=0){
            $this->form_message = "Please check the cities to be deleted!";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        $checked_cities = implode("', '", $cityCheck);
        $sql="UPDATE product SET del_flag='1' WHERE product.city IN ('$checked_cities') AND product.location='$countrySelect'";
        //execute delete
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to delete product with these cities due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = 'Unable to delete products with these cities';
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Products with the cities were deleted successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
       
    }
    /**
     * Private method: load news list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    // private function loadCityList(){
    //     require_once BASE_CLASS . 'class-connect.php';
    //     require_once BASE_CLASS . 'class-utilities.php';
        
    //     $cnx = new Connect();
    //     $cnx->open();
                
    //     // load news.
    //     if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
    //     $where="WHERE product.location='$country'";
    //     $sql="SELECT DISTINCT product.city, COUNT(product.id) as product_number from product $where GROUP BY product.city ORDER BY product.city";
    //     //echo $sql;
    //     if( !$result = @mysql_query($sql) ){
    //         require_once BASE_CLASS . 'class-log.php';
    //         $cnx->close();
            
    //         LogReport::write('Unable to load register type list due a query error at ' . __FILE__ . ':' . __LINE__);
            
    //         $this->form_message = $this->lang['MANAGE_CITY_LOAD_QUERY_ERROR'];
    //         $this->form_status = true;
    //         $this->form_style = 'alert-warning';
    //         return;
    //     }
        
    //     $this->city_list = array();
    //     while( $r = @mysql_fetch_assoc($result) ){
            
    //         array_push($this->city_list,$r);
    //     }
        
    //     @mysql_free_result($sql);
    //     $cnx->close();
    // }
    
    /**
     * Public method: get news list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    // public function getCityList(){
    //     return $this->city_list;
    // }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}