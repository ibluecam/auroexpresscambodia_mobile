<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('resize-class.php');
class userCommunityViewEdit
{
    protected $lang;
    protected $group = 'user';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;  
    Protected $status="";  
    private $url="";
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userCommunityViewEdit($lang=array())
    {
        $this->lang = $lang;
        
        
		
    }

    private function registerUploadfile(){

        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $_FILES["myfile"]["name"]);
        $extension = end($temp);
        if ((($_FILES["myfile"]["type"] == "image/gif")
        || ($_FILES["myfile"]["type"] == "image/jpeg")
        || ($_FILES["myfile"]["type"] == "image/jpg")
        || ($_FILES["myfile"]["type"] == "image/pjpeg")
        || ($_FILES["myfile"]["type"] == "image/x-png")
        || ($_FILES["myfile"]["type"] == "image/png"))
        && ($_FILES["myfile"]["size"] < 20480000)
        && in_array($extension, $allowedExts))
        {
            if ($_FILES["myfile"]["error"] > 0)
            {
                $this->form_message = $_FILES["myfile"]["error"];
                $this->form_status = true;
                $this->form_style = 'alert-error';
                $this->status="upload";
                //echo "Return Code: " . $_FILES["myfile"]["error"] . "<br>";
            }
            else
            {

                LogReport::write('File Upload Successfully ' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '. ' . ($_FILES["myfile"]["size"] / 1024).'kB' );
 
               //echo "Upload: " . $_FILES["file"]["name"] . "<br>";
               // echo "Type: " . $_FILES["file"]["type"] . "<br>";
               // echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
               // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

                if (file_exists("image/deal_week/" . $_FILES["myfile"]["name"]))
                {
                    LogReport::write('This file is already exists' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '. ' . ($_FILES["myfile"]["size"] / 1024).'kB' );
                    $dtime = new DateTime(); 
                    $dtime->format('YYYY-mmm-dd H:i:s');
                    $timestamp = $dtime->getTimestamp();
                    $filename =  $timestamp.".". $extension;
                    move_uploaded_file($_FILES["myfile"]["tmp_name"],
                    "image/deal_week/" . $filename);
                    $main_image=new resize("image/deal_week/" . $filename);
 
                    $main_image->resizeImage(210, 155, 'exact');
 
                    $main_image->saveImage("image/deal_week/" . $filename, 100);  
                    $this->url="image/deal_week/".$filename;
                     
                }
                else 
                {
                   
                    $dtime = new DateTime(); 
                    $dtime->format('YYYY-mmm-dd H:i:s');
                    $timestamp = $dtime->getTimestamp();
                    $filename =  $timestamp.".". $extension;
                    move_uploaded_file($_FILES["myfile"]["tmp_name"],
                    "image/deal_week/" . $filename);
                    $main_image=new resize("image/deal_week/" . $filename);
 
                    $main_image->resizeImage(210, 155, 'exact');
 
                    $main_image->saveImage("image/deal_week/" . $filename, 100);
                   $this->url="image/deal_week/".$filename;
                     

                    //echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
                }
            }
        }
        else
        {
             LogReport::write('This file is Invalid file' . $_FILES["myfile"]["name"] . ':' . $_FILES["myfile"]["type"]. '.' .($_FILES["myfile"]["size"] / 1024).'kB' );
                $this->form_message = 'This file is Invalid file.';
                $this->form_status = true;
                $this->form_style = 'alert-error';
             
            $this->status="upload";
            return;
        }

    }


    function loadDealweek($get_id){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT * from deal_week  WHERE id = '$get_id'";

        //$this->status ='ok';        
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
        $deal_week = mysql_fetch_array($sql);        

        @mysql_free_result($sql);
        $cnx->close();
        return $deal_week; 
        
    }


    public function updateDealWeek($loginId, $product_Id){
        $title = $_POST['rd_title'];
        $message = $_POST['text_message'];
        $get_dealid = $_POST['get_id'];

        $this->registerUploadfile(); 
        $image_name=$this->url; 

        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        $date = date('Y-m-d');
        if($image_name!=""){
            $sql_update="UPDATE deal_week SET title = '".$title."', message ='".$message."', `date` = NOW(), source = '".$image_name."'  where id = '".$get_dealid."' AND owner = '".$_SESSION['log_id']."'";                   
        }
        else{
            $sql_update="UPDATE deal_week SET title = '".$title."', message ='".$message."', `date` = NOW()  where id = '".$get_dealid."' AND owner = '".$_SESSION['log_id']."'";       
            
        }
        
        
        //echo $sql_search;
        if( !$sql = @mysql_query($sql_update) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            
            $cnx->close();
            return;
        }
        if($loginId!=$product_Id){
            $this->status="error";
        }
        else{
            $this->status="ok";   
        }
        
        /*if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }*/
        
         
        //$deal_week = mysql_fetch_array($sql);        

        //@mysql_free_result($sql);
        $cnx->close();
        //return $deal_week; 
        
    }

    function loadUserName($get_owner){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT * from register  WHERE id = '$get_owner'";        
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
        $user_name = mysql_fetch_array($sql);        

        @mysql_free_result($sql);
        $cnx->close();
        return $user_name; 
        
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    public function getstatus()
    {
        return $this->status;
    }

}
?>
