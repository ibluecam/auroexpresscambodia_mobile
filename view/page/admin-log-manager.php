<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get framework error log list.
    $fw = $_CLASS->getFWLog();
    
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['LOG_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // no results.
                    if( count($fw) < 1 )
                    {
                ?>
                <p><?php echo $_LANG['LOG_NO_ERRORS_LOG'];?></p>  
                <?php
                    }
                    
                    // display framework logs.
                    if( count($fw) > 0 )
                    {
                        for( $i=0; $i < count($fw); $i++ )
                        {
                ?>
                <form method="post">
                    <div class="input-append input-prepend" style="margin-left:2px;">
                        <span class="add-on" style="width:90px;"><?php echo $_LANG['LOG_DATE_LABEL'];?>:</span>
                        <input type="text" disabled="disabled" value="FW: <?php echo $fw[$i]['datum'];?>" style="width:387px;" />
                        <input type="hidden" name="hidfw" value="<?php echo $fw[$i]['url'];?>"/>
                        <a class="btn" style="width:100px;" href="<?php echo str_replace(BASE_ROOT,BASE_RELATIVE,$fw[$i]['url']);?>" target="_blank"><?php echo $_LANG['LOG_DOWNLOAD_LABEL'];?></a>
                        <input type="submit" name="delBtn" class="btn btn-danger" style="width:100px;" value="<?php echo $_LANG['LOG_DELETE_LABEL'];?>" />
                    </div>
                </form>
                <?php
                        }
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}