<?php

if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
require_once BASE_CLASS . 'class-message.php';
require_once BASE_CLASS . 'class-product.php';
require_once BASE_CLASS . 'class-utilities.php';
class publicdownloadattachment
{

    protected $slug = 'message';
    protected $form_status;
    protected $form_message;
    protected $form_style;


    private $message;


    private $errorMessages=array();


    /*
     * Constructor -------------------------------------------------------------
     */

    public function publicdownloadattachment($lang=array())
    {
        $this->lang = $lang;
        $this->message=new Message();
       
        $this->downloadAttachment();
        
    }
    
    

    /* Download any attachment file by encoded name */
    private function downloadAttachment(){
        if(isset($_GET['download'])) $encodeName=htmlspecialchars($_GET['download']); else $encodeName='';
        /* If get parameter is not empty process downloading */
        if(!empty($encodeName)){
            /* get physical file url by giving encode name to match file name and path in database */
            $attachments=$this->message->getAttachmentByEncodeName($encodeName);
            
            if(count($attachments)>0){
                /* if record of file is found in database */

                if(Utilities::downloadFile($attachments['file_path'].$attachments['file_name'], $attachments['file_title'])){
                    /* if physical file can be success downloaded by user */
                }else{
                    /* if file not found or invalid */
                    $this->errorMessages[]="File Not Found!";
                }
            }else{
                /* if record of file is not found in database */
                $this->errorMessages[]="Message attachment is not found in database!";
            }

            //array_merge($this->errorMessages, $this->message->getErrorMessages());
            //var_dump($this->errorMessages);
        }
        
    }

   

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    /* GET Error Message */
    public function getErrorMessages(){
        return $this->errorMessages;
    }
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }


}
?>
