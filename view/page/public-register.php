
<?php 
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

/*
 * Set security question variables ---------------------------------------------------
 */
$_SESSION['security_question_a'] = mt_rand('1','20');
$_SESSION['security_question_b'] = mt_rand('1','20');
//$countryList=$_CLASS->getCountryList();

/*  FOR DETECT LOCATION 
 include('php/ip2locationlite.class.php');
//Load the class
$ipLite = new ip2location_lite;
$ipLite->setKey('0ceddca8fc2527329d305fb922e7bbade230ca741a2a68055b649696326992be');

//Get errors and locations
$locations = $ipLite->getCity($_SERVER['REMOTE_ADDR']);
$errors = $ipLite->getError();
*/
//get province list
 $province=$_CLASS->load_province();
?>



<script >
 function isNumberKey(evt){

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
       
       return false;
    return true;
  }

 function message_security_null(){
	document.getElementById('msg').innerHTML = "Please Input Security Number";
	document.getElementById('reg_name').style.border="1px solid #a94442";
 }

/*function address_null(){
   document.getElementById('address_null').innerHTML = "Please Input Address";
   document.getElementById('address_seller').style.border="1px solid #a94442";
}*/

 function onlyAlphabets(e, t) {

            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                 return false;
            }
            catch (err) {

                alert(err.Description);
            }
		} 
	
</script>





<!--/END OF ORIGINAL CODE-->
	

       				 <!--register form start here-->
    <div id="wrapper-register-login" >  
		<div id="register">
        
        
        <!--Seller sign Up start here-->
        
        <?php if(isset($_GET['member_type3']) == 'seller'){  ?>
		
		
            <form id="defaultForm"  class="form-horizontal uploadForm" method="post" enctype="multipart/form-data">
            
				<div class="page-title"><?php echo $_LANG['REGISTER_SIGNUP'];?></div>
                <div class="status">Join free at Angkorauto.com, the no.1 auto trader in Cambodia.</div>
              
				<table class="vehicle-info-table" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<!--hidden field member type-->
							<input type="hidden" name="member_type3" value="seller" />
                            
                            
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_firstname)){echo $_CLASS->store_temporary_firstname ;}?>" class="form-control border" type="text" name="first_name" id="user_id" placeholder="<?php echo $_LANG['REGISTER_FIRSTNAME'];?>*" style="width:320px"/>
							</div>
						</td>
						<td class="tdRight">
							<div class="form-group">
							    <input value="<?php if(isset($_CLASS->store_temporary_lasttname)){echo $_CLASS->store_temporary_lasttname ;}?>" class="form-control border" type="text" name="last_name" id="user_id" placeholder="<?php echo $_LANG['REGISTER_LASTNAME'];?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style='width:320px;' />
								
							</div>
						</td>
                        <td rowspan="8" valign="top">
                        <div style="width:220px; height:auto">
						
							<img  id="uploadPreview1"  src='images/profile.png' width="210" height="130" style="border:1px solid #CCC; margin-bottom:15px"/>	
							<p id="filenotfound" style="font-size:10px;color:#a94442;"></p>
							<?php if(isset($_CLASS->not_choose_image)){echo"<p id='clear-message' style='font-size:10px;color:#a94442;'>$_CLASS->not_choose_image</p>";}?>
							
							<a style="float:left;"href="#" onclick="document.getElementById('uploadImage1').click(); return false;"><img style="position:relative;top:19px;" src="images/register/upload.png"/></a>
							<input id="delete_img" type="button" onClick="DeleteImage()" value="Delete" style="float:left;top:20px;position:relative;" />
							<input type="file"  id="uploadImage1" onchange="PreviewImage(1);" style="visibility: hidden;" class="image" name="file" />
					
						
                        </div>
                        </td>
					</tr>
					<tr>
						<td>
							<!--  Userid -->
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_userid)){echo $_CLASS->store_temporary_userid;}?>" class="form-control border" type="text" name="user_id" id="user_id" placeholder="<?php echo $_LANG['REGISTER_USER_ID'];?>*" style="width:320px"/>
							</div>
						
						</td>
						<td class="tdRight">
						<!-- Email-->
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Email)){echo $_CLASS->store_temporary_Email;}?>" type="text" id="email" name="email" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_EMAIL'];?>" style="width:320px;"/>
								
							</div>
							
						</td>
					</tr>
                    
					<tr>
						<td class="left">
						<!-- Company Name -->
						<div class="form-group">
								<input class="form-control border" type="text" name="company_name" id="com_name" placeholder="<?php echo $_LANG['REGISTER_COMPANY_NAME'];?>" value="<?php if(isset($_CLASS->store_temporary_company_name)){ echo $_CLASS->store_temporary_company_name;} ?>" style="width:320px;"/>
								<input type="hidden" name="reg_name" id="reg_name" value="" />	
						</div>
						</td>
						<td class="tdRight">
						<!-- Gender -->
						
							<div class="form-group">
                    <select id="sex" class="form-control border"  name="gender" style="width:320px">
                    <option selected="selected" value=''><?php echo $_LANG['REGISTER_SEX'];?>*</option>
					 <?php if(isset($_CLASS->store_temporary_gender)){?>
						   <?php if($_CLASS->store_temporary_gender=="M"){?>
							<option value='M' selected="selected">Male</option>
							<option value='F'>Female</option>
						   <?php }else{?>
						   <option value='F' selected="selected">Female</option>
						   <option value='M'>Male</option>
						   <?php }?>
					 <?php }else{?>
					 <option value='M'>Male</option>
                     <option value='F'>Female</option>
					 <?php }?>
                    </select>	
			
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<!-- Password -->
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Password)){echo $_CLASS->store_temporary_Password;} ?>" type="password" id="password" name="password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_PASSWORD']; ?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style="width:320px;"/>
							</div>
						</td>
						<td>
							<!-- Confirm Password -->
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Confirm_password)){echo $_CLASS->store_temporary_Confirm_password;}?>" type="password" id="passwords" name="confirm_password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_CONFIRM_PASSWORD'];?>*" style="width:320px;"/>
								
							</div>
						</td>
					</tr>
					<tr>
                    <td class="left">
						 <!--Phone Block -->
						<div class="form-group">
								<input onkeypress="return isNumberKey(event)"  type="text" name="mobile" id="mobile" class="form-control border" value="<?php  if(isset($_CLASS->store_temporary_Phone)){echo $_CLASS->store_temporary_Phone ;} ?>" placeholder="<?php echo $_LANG['REGISTER_PHONE_NUMBER'];?>*" style="width:320px;position:relative;left:61px;"/>
								<input type="hidden" id="phonecode" name="phonecode" value=""/>
								<span class="error" id="mobile_error"><?php echo $_LOCAL['REGISTER_MOBILE_STATUS']; ?></span>
								<span class="error" id="invalid_mobile_error"><?php echo $_LOCAL['REGISTER_INVALID_MOBILE_STATUS']; ?></span>
						</div>
						<!-- End New -->					
						</td>
                        <td class="tdRight">
							<div class="form-group">
                     <select id="sex" class="form-control border"  name="province_city" style="width:320px">
							<option selected="selected" value=''><?php echo $_LANG['REGISTER_CITY/PROVINCE'];?>*</option>
                    <?php foreach($province as $value){?>
							<?php if($_CLASS->store_temporary_province==$value['province_name']){?>
								<option selected="selected" value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php } else{?>
							    <option value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php }?>
					<?php }?>
                    </select>	
			
							</div>
							
						</td>
                        </tr>
                    <!--    <tr>
                    <td colspan="2">
							
					<div class="form-group">
                    <select id="member_type" class="form-control"  name="membership_type" style="width:320px">
                    <option selected="selected" value=''><?php //echo $_LANG['REGISTER_membership_type'];?>*</option>
					<?php //if(isset($_CLASS->membership_type)){?>
					    <?php //if($_CLASS->membership_type=="Free User"){ ?>
							<option selected="selected"value='Free User'>Free User</option>
							<option value='Premium'>Premium</option>
							<option value='superior'>superior</option>
						<?php //}?>
						 <?php //if($_CLASS->membership_type=="Premium"){ ?>
							<option value='Free User'>Free User</option>
							<option selected="selected" value='Premium'>Premium</option>
							<option value='superior'>superior</option>
						<?php //}?>
						<?php //if($_CLASS->membership_type=="superior"){ ?>
							<option value='Free User'>Free User</option>
							<option value='Premium'>Premium</option>
							<option selected="selected" value='superior'>superior</option>
						<?php //}?>
						
					<?php //}else{ ?>
					<option value='Free User'>Free User</option>
					<option value='Premium'>Premium</option>
					<option value='superior'>superior</option>
					<?php //}?>
                    </select>
					
					</div>
							
						</td>
                    </tr>-->
                        <tr>
						<td colspan="2">
                        <?php if(isset($_CLASS->address_null)){?>
						<textarea value="<?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?>" id="address_seller" name="address" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_ADDRESS']; ?>" style="width:664px; height:100px;"/><?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?></textarea>
                        
						<?php }else{?>
                       <textarea value="<?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?>" id="address_seller" name="address" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_ADDRESS']; ?>" style="width:664px; height:100px;"/><?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?></textarea>
                        
						<?php }?>
                        </td>
                        </tr>
                        <tr>
						<td colspan="2">
							
							<!--Security question Block -->
						<div style="margin-top:10px; width:300px;" >
                            <br/> <span style="color: #1a1a1a; font-size:13px; font-weight:bold"><?php echo $_LANG['REGISTER_SECURITY_QUESTION']; ?></span>
                            
                            <?php if($_CLASS->secu_message_null=="null"){?>
                            
                                <div class="security" >                                    	
                                    <input type="text" name="reg_name" class="form-control border" value="" placeholder="<?php require_once BASE_CLASS .'math-security.php'; $math = new BasicMathSecurity( 'math' ); $m= $math->getField(); echo $_LANG['REGISTER_SECURITY_NUMBER']; $_SESSION['remember_sec_number']=$m[1]; ?>"/>
                                </div>                                    
                                <span style="color:#3c3c3c; border:1px solid #CCC; font-size:16px; padding:5px 13px; position:absolute; margin-top:-7px; margin-left:20px"><?php echo $m[0]; ?></span>
                                <p style="font-size:13px;color:#a94442; position:absolute; left:500px; margin-top:-16px">Please Input Security Number</p>
                            
                            <?php }elseif($_CLASS->secu_message_invalid=="Invalid"){?>
                            
                                <div class="security">
                                    <input style="padding-left:10px"  type="text" name="reg_name" class="form-control border" value="" placeholder="<?php require_once BASE_CLASS .'math-security.php'; $math = new BasicMathSecurity( 'math' ); $m= $math->getField(); echo $_LANG['REGISTER_SECURITY_NUMBER']; $_SESSION['remember_sec_number']=$m[1]; ?>"/>
                                </div> 
                                <span style="color:#3c3c3c; border:1px solid #CCC; font-size:16px; padding:5px 13px; position:absolute; margin-top:-7px; margin-left:20px"><?php echo $m[0]; ?></span>
                                <p style="font-size:13px;color:#a94442; position:absolute; left:500px; margin-top:-16px">Invalid Value</p>
                            
                            <?php }else{?>
                            
                                <div class="security" >
                                    <div class="form-group">
                                        <div class="col-sm-2" style="width:330px;">                                        
                                            <input style="padding-left:10px" id="value2" maxlength="4" onkeypress="return isNumberKey(event)" type="text" name="kreusna_reg_name" class="form-control border" value="<?php if(isset($_CLASS->store_temporary_security_number)){echo $_CLASS->store_temporary_security_number;} ?>" placeholder="<?php require_once BASE_CLASS .'math-security.php'; $math = new BasicMathSecurity( 'math' ); $m= $math->getField(); echo $_LANG['REGISTER_SECURITY_NUMBER']; $_SESSION['remember_sec_number']=$m[1]; ?>"/>
                                        </div>
                                    </div>
                                </div>
                            
                                <input type="hidden" value="<?php echo $m[0]; ?>" name="temp" id="tempSecurityNum"/>
                                <span id="value1" style="color:#3c3c3c; border:1px solid #CCC; font-size:16px; padding:5px 13px; position:absolute; margin-top:-7px; margin-left:20px"><?php echo $m[0]; ?></span>
                                <p id="msg" style="font-size:13px;color:#a94442; position:absolute; left:500px; margin-top:-16px"></p>
                            
                            <?php } ?>
								
						</div>
							
							
							
						</td>
					</tr>					
					<tr class="agreement-row">
						<td colspan="2" id="buttom_right">
							<label style="float:left; width:360px">
							
							<?php if($_CLASS->store_temporary_agree==""){?>
								<input type="checkbox" name="term" value="accept"   onClick="agree(this)"/>
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <font color='red'>[</font><a href="<?php echo BASE_RELATIVE.'termcondition'?>" style='text-decoration:underline;color:red;'>Terms of Use</a><font color='red'>]</font> <font style='color:red;'>, </font> <font style='color:red;'>[</font><a href="<?php echo BASE_RELATIVE.'privacy-policy'?>" style='text-decoration:underline;color:red'>Privacy Policy</a><font color='red'>]</font></span>
							<?php }elseif($_CLASS->store_temporary_agree=="checked"){?>
							    <input type="checkbox" name="term" value="accept"  checked="checked" onClick="agree(this)"/>
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?><font color='red'>[</font><a href="<?php echo BASE_RELATIVE.'termcondition'?>" style='text-decoration:underline;color:red;'>Terms of Use</a><font color='red'>]</font> <font style='color:red;'>, </font><font color='red'>]</font><a href="<?php echo BASE_RELATIVE.'privacy-policy'?>" style='text-decoration:underline;color:red'>Privacy Policy</a><font color='red'>]</font></span>
						    <?php }else{?>
						
								<input type="checkbox" name="term" value="accept"  onClick="agree(this)"/>
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?><font color='red'>[</font><a href="<?php echo BASE_RELATIVE.'termcondition'?>" style='text-decoration:underline;color:red;'>Terms of Use</a><font color='red'>]</font> <font style='color:red;'>, </font><font style='color:red;'>[</font><a href="<?php echo BASE_RELATIVE.'privacy-policy'?>" style='text-decoration:underline;color:red'>Privacy Policy</a><font color='red'>]</font></span>
								<p style="font-size:12px;color:red;"><b>Not check</b></p>
							<?php }?>
							
								
							</label>
		<!--				<input type="hidden" id="inputres" name="hidres" value="<?php //echo ($_SESSION['security_question_a'] + $_SESSION['security_question_b']);?>" />-->
        </td>
        </tr>
        <tr class="agreement-row">
        <td id="buttom_right" colspan="2">
						<div class="form-group" style="width:132px;height:30px">
							<input  type="submit" class="linkimg inquiry-button" name="signup-seller" value="<?php echo $_LANG['REGISTER_SUBMIT'];?>" id="signup-seller"/>
						</div>
						</td>

					</tr>
		 
				</table>
			</form>
            <?php }else{ ?>
			
				<form id="defaultForm"  class="form-horizontal uploadForm" method="post" enctype="multipart/form-data">
            
				<div class="page-title"><?php echo $_LANG['REGISTER_SIGNUP'];?></div>
                <div class="status">Join free at Angkorauto.com, the no.1 auto trader in Cambodia.</div>
              
				<table class="vehicle-info-table" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<!--hidden field member type-->
							<input type="hidden" name="member_type3" value="seller" />
                            
                            
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_firstname)){echo $_CLASS->store_temporary_firstname ;}?>" class="form-control border" type="text" name="first_name" id="user_id" placeholder="<?php echo $_LANG['REGISTER_FIRSTNAME'];?>*" style="width:320px"/>
								
								
							</div>
						</td>
						<td class="tdRight">
							<div class="form-group">
							    <input value="<?php if(isset($_CLASS->store_temporary_lasttname)){echo $_CLASS->store_temporary_lasttname ;}?>" class="form-control border" type="text" name="last_name" id="user_id" placeholder="<?php echo $_LANG['REGISTER_LASTNAME'];?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style='width:320px;' />
								
							</div>
						</td>
                        <td rowspan="8" valign="top">
                        <div style="width:220px; height:auto">
						
							<img  id="uploadPreview1"  src='images/profile.png' width="210" height="130" style="border:1px solid #CCC; margin-bottom:15px"/>	
							<p id="filenotfound" style="font-size:10px;color:#a94442;"></p>
							<?php if(isset($_CLASS->not_choose_image)){echo"<p id='clear-message' style='font-size:10px;color:#a94442;'>$_CLASS->not_choose_image</p>";}?>
							
							<a style="float:left;"href="#" onclick="document.getElementById('uploadImage1').click(); return false;"><p style="position:relative;color:blue;top:23px;left:28px;text-decoration:none;">Upload</p><img src="images/register/upload.png" placeholder="Upload"/></a>
							<input id="delete_img" type="button" onClick="DeleteImage()" value="Delete" style="float:left;top:20px;position:relative;" />
							<input type="file"  id="uploadImage1" onchange="PreviewImage(1);" style="visibility: hidden;" class="image" name="file" />
					
						
                        </div>
                        </td>
					</tr>
					<tr>
						<td>
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Email)){echo $_CLASS->store_temporary_Email;}?>" type="text" id="email" name="email" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_EMAIL'];?>*" style="width:320px;"/>
								
							</div>
						</td>
						<td class="tdRight">
							
							<div class="form-group">
                    <select id="sex" class="form-control border"  name="gender" style="width:320px">
                    <option selected="selected" value=''><?php echo $_LANG['REGISTER_SEX'];?>*</option>
					 <?php if(isset($_CLASS->store_temporary_gender)){?>
						   <?php if($_CLASS->store_temporary_gender=="M"){?>
							<option value='M' selected="selected">Male</option>
							<option value='F'>Female</option>
						   <?php }else{?>
						   <option value='F' selected="selected">Female</option>
						   <option value='M'>Male</option>
						   <?php }?>
					 <?php }else{?>
					 <option value='M'>Male</option>
                     <option value='F'>Female</option>
					 <?php }?>
                    </select>	
			
							</div>
							
						</td>
					</tr>
                    
					<tr>
						<td class="left">
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Password)){echo $_CLASS->store_temporary_Password;} ?>" type="password" id="password" name="password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_PASSWORD']; ?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style="width:320px;"/>
								
							</div>
						</td>
						<td class="tdRight">
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Confirm_password)){echo $_CLASS->store_temporary_Confirm_password;}?>" type="password" id="passwords" name="confirm_password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_CONFIRM_PASSWORD'];?>*" style="width:320px;"/>
								
							</div>
						  
						 
						</td>
					</tr>
					<tr>
                    <td class="left">
						 <!--Phone Block -->
						<div class="form-group">
								<input onkeypress="return isNumberKey(event)"  type="text" name="mobile" id="mobile" class="form-control border" value="<?php  if(isset($_CLASS->store_temporary_Phone)){echo $_CLASS->store_temporary_Phone ;} ?>" placeholder="<?php echo $_LANG['REGISTER_PHONE_NUMBER'];?>*" style="width:320px;position:relative;left:61px;"/>
								<input type="hidden" id="phonecode" name="phonecode" value=""/>
								<span class="error" id="mobile_error"><?php echo $_LOCAL['REGISTER_MOBILE_STATUS']; ?></span>
								<span class="error" id="invalid_mobile_error"><?php echo $_LOCAL['REGISTER_INVALID_MOBILE_STATUS']; ?></span>
						</div>
						<!-- End New -->					
						</td>
                        <td class="tdRight">
							<div class="form-group">
                     <select id="sex" class="form-control border"  name="province_city" style="width:320px">
							<option selected="selected" value=''><?php echo $_LANG['REGISTER_CITY/PROVINCE'];?>*</option>
                    <?php foreach($province as $value){?>
							<?php if($_CLASS->store_temporary_province==$value['province_name']){?>
								<option selected="selected" value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php } else{?>
							    <option value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php }?>
					<?php }?>
                    </select>	
			
							</div>
							
						</td>
                        </tr>
                    <!--    <tr>
                    <td colspan="2">
							
					<div class="form-group">
                    <select id="member_type" class="form-control"  name="membership_type" style="width:320px">
                    <option selected="selected" value=''><?php //echo $_LANG['REGISTER_membership_type'];?>*</option>
					<?php //if(isset($_CLASS->membership_type)){?>
					    <?php //if($_CLASS->membership_type=="Free User"){ ?>
							<option selected="selected"value='Free User'>Free User</option>
							<option value='Premium'>Premium</option>
							<option value='superior'>superior</option>
						<?php //}?>
						 <?php //if($_CLASS->membership_type=="Premium"){ ?>
							<option value='Free User'>Free User</option>
							<option selected="selected" value='Premium'>Premium</option>
							<option value='superior'>superior</option>
						<?php //}?>
						<?php //if($_CLASS->membership_type=="superior"){ ?>
							<option value='Free User'>Free User</option>
							<option value='Premium'>Premium</option>
							<option selected="selected" value='superior'>superior</option>
						<?php //}?>
						
					<?php //}else{ ?>
					<option value='Free User'>Free User</option>
					<option value='Premium'>Premium</option>
					<option value='superior'>superior</option>
					<?php //}?>
                    </select>
					
					</div>
							
						</td>
                    </tr>-->
                        <tr>
						<td colspan="2">
                        <?php if(isset($_CLASS->address_null)){?>
						<textarea value="<?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?>" id="address_seller" name="address" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_ADDRESS']; ?>" style="width:664px; height:100px;"/><?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?></textarea>
                        
						<?php }else{?>
                       <textarea value="<?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?>" id="address_seller" name="address" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_ADDRESS']; ?>" style="width:664px; height:100px;"/><?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?></textarea>
                        
						<?php }?>
                        </td>
                        </tr>
                        <tr>
						<td colspan="2">
							
							<!--Security question Block -->
						    <div class="form-group" style="margin-top:10px">
							<br/><span style="color: #999"><?php echo $_LANG['REGISTER_SECURITY_QUESTION'];?></span>
								<div class="security">
                                
									<?php if($_CLASS->secu_message_null=="null"){ ?>	
                                    								
											<input  type="text" name="reg_name_answer" class="form-control border" value="" placeholder="<?php require_once BASE_CLASS .'math-security.php'; $math = new BasicMathSecurity( 'math' );$m= $math->getField(); echo $m[0]; $_SESSION['remember_sec_number']=$m[1];?>"/>
											<p style="font-size:10px;color:#a94442;">Please Input Security Number</p>
                                            
									<?php }elseif($_CLASS->secu_message_invalid=="Invalid"){?>
									
									
											<input   type="text" name="reg_name_answer" class="form-control border" value="" placeholder="<?php require_once BASE_CLASS .'math-security.php'; $math = new BasicMathSecurity( 'math' ); $m= $math->getField(); echo $m[0]; $_SESSION['remember_sec_number']=$m[1];?>"/>
											<p style="font-size:10px;color:#a94442;">Invalid Value</p>
									
								    <?php }else{?>
                                    
											<input id="reg_name" onkeypress="return isNumberKey(event)" type="text" name="reg_name_answer" class="form-control border" value="<?php if(isset($_CLASS->store_temporary_security_number)){echo $_CLASS->store_temporary_security_number;} ?>" placeholder="<?php require_once BASE_CLASS .'math-security.php'; $math = new BasicMathSecurity( 'math' ); $m= $math->getField();echo $m[0]; $_SESSION['remember_sec_number']=$m[1];?>"/>                                    
											<p id="msg" style="font-size:10px;color:#a94442;"></p>
                                            
									<?php } ?>
								</div>
							</div>
							
							
							
						</td>
					</tr>					
					<tr class="agreement-row">
						<td colspan="2" id="buttom_right">
							<label style="float:left; width:360px">
							
							<?php if($_CLASS->store_temporary_agree==""){?>
								<input type="checkbox" name="term" value="accept" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <font color='red'>[</font><a href="<?php echo BASE_RELATIVE.'termcondition'?>" style='text-decoration:underline;color:red;'>Terms of Use</a><font color='red'>]</font> <font style='color:red;'>, </font> <font style='color:red;'>[</font><a href="<?php echo BASE_RELATIVE.'privacy-policy'?>" style='text-decoration:underline;color:red;'>Privacy Policy</a><font color='red'>]</font></span>
							<?php }elseif($_CLASS->store_temporary_agree=="checked"){?>
							    <input type="checkbox" name="term" value="accept" checked="checked" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?><font color='red'>[</font><a href="<?php echo BASE_RELATIVE.'termcondition'?>" style='text-decoration:underline;color:red;'>Terms of Use</a><font color='red'>]</font> <font style='color:red;'>, </font><font color='red'>]</font><a href="<?php echo BASE_RELATIVE.'privacy-policy'?>" style='text-decoration:underline;color:red;'>Privacy Policy</a><font color='red'>]</font></span>
						    <?php }else{?>
						
								<input type="checkbox" name="term" value="accept" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?><font color='red'>[</font><a href="<?php echo BASE_RELATIVE.'termcondition'?>" style='text-decoration:underline;color:red;'>Terms of Use</a><font color='red'>]</font> <font style='color:red;'>, </font><font style='color:red;'>[</font><a href="<?php echo BASE_RELATIVE.'privacy-policy'?>" style='text-decoration:underline;color:red;'>Privacy Policy</a><font color='red'>]</font></span>
								<p style="font-size:12px;color:red;"><b>Not check</b></p>
							<?php }?>
							
								
							</label>
		<!--				<input type="hidden" id="inputres" name="hidres" value="<?php //echo ($_SESSION['security_question_a'] + $_SESSION['security_question_b']);?>" />-->
        </td>
        </tr>
        <tr class="agreement-row">
        <td id="buttom_right" colspan="2">
						<div class="form-group" style="width:132px;height:30px">
							<input  type="submit" class="linkimg inquiry-button" name="signup-seller" value="<?php echo $_LANG['REGISTER_SUBMIT'];?>" id="signup-seller"/>
						</div>
						</td>

					</tr>
		 
				</table>
			</form>
			
			
		    <?php }?>
			
            <!--Seller sign Up ends here-->
            
                   <!--Dealer sign Up start here-->
        
        <!--<?php if($_GET['member_type3'] == 'dealer'){?>-->
		
		
		  <!--<div class="fileinput fileinput-new" data-provides="fileinput">
  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
    <img data-src="holder.js/100%x100%" alt="...">
  </div>
  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
  <div>
    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
  </div>
</div>-->

		
		
		
		
		
            <!--<form id="defaultForm"  class="form-horizontal" method="post" enctype="multipart/form-data">
            
				<div class="page-title"><?php echo $_LANG['REGISTER_BUSINESS_TYPE_DEALER'];?></div>
                <div class="status">Join free at Angkorauto.com, the no.1 auto trader in Cambodia.</div>
            
				<table class="vehicle-info-table" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<!--hidden field member type-->
							<!--<input type="hidden" name="member_type3" value="dealer" />
                            
                            
							<div class="form-group">
								<input class="form-control border" type="text" name="company_name" id="com_name" placeholder="<?php echo $_LANG['REGISTER_COMPANY_NAME'];?>*" value="<?php if(isset($_CLASS->store_temporary_company_name)){ echo $_CLASS->store_temporary_company_name;} ?>" style="width:320px;"/>
								<input type="hidden" name="reg_name" id="reg_name" value="" />
								
								
							</div>
						</td>
						<td class="tdRight">
							<div class="form-group">
							    <input value="<?php if(isset($_CLASS->store_temporary_contact_name)){echo $_CLASS->store_temporary_contact_name ;}?>" class="form-control border" type="text" name="contact_name" id="user_id" placeholder="<?php echo $_LANG['REGISTER_CONTACT_NAME'];?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style='width:320px;' />
								
							</div>
						</td>
                        <td rowspan="8" valign="top">
                        <div style="width:220px; height:auto">
						
							<img  id="uploadPreview1"  src='images/profile.png' width="210" height="130" style="border:1px solid #CCC; margin-bottom:15px"/>	
							<p id="filenotfound" style="font-size:10px;color:#a94442;"></p>
							<?php if(isset($_CLASS->not_choose_image)){echo"<p id='clear-message' style='font-size:10px;color:#a94442;'>$_CLASS->not_choose_image</p>";}?>
							
							<a style="float:left;"href="#" onclick="document.getElementById('uploadImage1').click(); return false;"><p style="position:relative;color:blue;top:23px;left:28px;text-decoration:none;">Upload</p><img src="images/register/upload.png" placeholder="Upload"/></a>
							<input id="delete_img" type="button" onClick="DeleteImage()" value="Delete" style="float:left;top:20px;position:relative;" />
							<input type="file"  id="uploadImage1" onchange="PreviewImage(1);" style="visibility: hidden;" class="image" name="file" />
					
						
                        </div>
                        </td>
					</tr>
					<tr>
						<td>
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Email)){echo $_CLASS->store_temporary_Email;}?>" type="text" id="email" name="email" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_EMAIL'];?>*" style="width:320px;"/>
								
							</div>
						</td>
						<td class="tdRight">
							
							<div class="form-group">
                    <select id="sex" class="form-control border"  name="province_city" style="width:320px">
                    <option selected="selected" value=''><?php echo $_LANG['REGISTER_CITY/PROVINCE'];?>*</option>
                    <?php foreach($province as $value){?>
							<?php if($_CLASS->store_temporary_province==$value['province_name']){?>
								<option selected="selected" value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php } else{?>
							    <option value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php }?>
					<?php }?>

                    </select>	
			
							</div>
							
						</td>
                        
					</tr>
                    <tr>
                    <td>
                    <div class="form-group">
   <input type="text" name="website" id="website" class="form-control border" value="<?php  if(isset($_CLASS->store_tempory_web)){echo $_CLASS->store_tempory_web ;} ?>" placeholder="<?php echo $_LANG['REGISTER_WEBSITE'];?>" style="width:320px"/>
  					 </div>
                    </td>
                    <td class="tdRight">
							
							<div class="form-group">
                    <select id="member_type" class="form-control border"  name="membership_type" style="width:320px">
                    <option selected="selected" value=''><?php echo $_LANG['REGISTER_membership_type'];?>*</option>
                    <?php if(isset($_CLASS->membership_type)){?>
					    <?php if($_CLASS->membership_type=="Free User"){ ?>
							<option selected="selected"value='Free User'>Free User</option>
							<option value='Premium'>Premium</option>
							<option value='superior'>superior</option>
						<?php }?>
						 <?php if($_CLASS->membership_type=="Premium"){ ?>
							<option value='Free User'>Free User</option>
							<option selected="selected" value='Premium'>Premium</option>
							<option value='superior'>superior</option>
						<?php }?>
						<?php if($_CLASS->membership_type=="superior"){ ?>
							<option value='Free User'>Free User</option>
							<option value='Premium'>Premium</option>
							<option selected="selected" value='superior'>superior</option>
						<?php }?>
						
					<?php }else{ ?>
					<option value='Free User'>Free User</option>
					<option value='Premium'>Premium</option>
					<option value='superior'>superior</option>
					<?php }?>
                    </select>	
			
							</div>
							
						</td>
                        
					</tr>
                   
					<tr>
						<td class="left">
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Password)){echo $_CLASS->store_temporary_Password;} ?>" type="password" id="password" name="password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_PASSWORD']; ?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style="width:320px;"/>
								
							</div>
						</td>
						<td class="tdRight">
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Confirm_password)){echo $_CLASS->store_temporary_Confirm_password;}?>" type="password" id="passwords" name="confirm_password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_CONFIRM_PASSWORD'];?>*" style="width:320px;"/>
								
							</div>
						  
						 
						</td>
					</tr>
					<tr>
                    <td class="left">
						 <!--Phone Block -->
						<!--<div class="form-group">
								<input onkeypress="return isNumberKey(event)"  type="text" name="mobile" id="mobile" class="form-control border" value="<?php  if(isset($_CLASS->store_temporary_Phone)){echo $_CLASS->store_temporary_Phone ;} ?>" placeholder="<?php echo $_LANG['REGISTER_PHONE_NUMBER'];?>*" style="width:320px;position:relative;left:61px;"/>
								<input type="hidden" id="phonecode" name="phonecode" value=""/>
								<span class="error" id="mobile_error"><?php echo $_LOCAL['REGISTER_MOBILE_STATUS']; ?></span>
								<span class="error" id="invalid_mobile_error"><?php echo $_LOCAL['REGISTER_INVALID_MOBILE_STATUS']; ?></span>
						</div>
						<!-- End New -->					
						<!--</td>
                        <td class="tdRight">
							<div class="form-group">
                    <input  type="text" name="fax" id="mobile" class="form-control border" value="<?php  if(isset($_CLASS->store_tempory_fax)){echo $_CLASS->store_tempory_fax ;} ?>" placeholder="<?php echo $_LANG['REGISTER_FAX'];?>" style="width:320px;position:relative;left:61px;"/>
								
			
							</div>
							
						</td>
                        </tr>
                        <tr>
						<td colspan="2">
                        <?php if(isset($_CLASS->store_tempory_address)){ ?>
							<textarea  id="address" name="address" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_ADDRESS']; ?>" style="width:664px; height:100px"/><?php if(isset($_CLASS->store_tempory_address)){echo $_CLASS->store_tempory_address;} ?></textarea>
                        <?php } else {?>
						    <textarea  id="address" name="address" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_ADDRESS']; ?>"  style="width:664px; height:100px"/></textarea>
						<?php } ?>
                        </td>
                        </tr>
                        
                         <tr>
						<td colspan="2" style="padding-top:20px">
                        <?php if(isset($_CLASS->store_tempory_company_description)){?>
                         <textarea  id="company_des" name="company_description" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_COMPANY_DES']; ?>" value="<?php if(isset($_CLASS->store_tempory_company_description)){echo $_CLASS->store_tempory_company_description;} ?>" style="width:664px; height:100px"/><?php if(isset($_CLASS->store_tempory_company_description)){echo $_CLASS->store_tempory_company_description;} ?></textarea>
                        <?php } else { ?>
						  <textarea  id="company_des" name="company_description" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_COMPANY_DES']; ?>"  style="width:664px; height:100px"/></textarea>
						<?php } ?>
                        </td>
                        </tr>
                        
                        <tr>
						<td colspan="2">
							
							<!--Security question Block -->
						  <!--<div class="form-group" style="margin-top:10px">
							<br/><span style="color: #999;"><?php echo $_LANG['REGISTER_SECURITY_QUESTION'];?></span>
								<div class="security">
									 <?php if($_CLASS->secu_message_null=="null"){?>
										
									<input  type="text" name="reg_name" class="form-control border" value="" 
									placeholder="<?php require_once BASE_CLASS .'math-security.php';
									$math = new BasicMathSecurity( 'math' );
									$m= $math->getField();
									echo $m[0];
									$_SESSION['remember_sec_number']=$m[1];
									
									?>"/>
									<p style="font-size:10px;color:#a94442;">Please Input Security Number</p>
									<?php }elseif($_CLASS->secu_message_invalid=="Invalid"){?>
									
									
									<input   type="text" name="reg_name" class="form-control border" value="" 
									placeholder="<?php require_once BASE_CLASS .'math-security.php';
									$math = new BasicMathSecurity( 'math' );
									$m= $math->getField();
									echo $m[0];
									$_SESSION['remember_sec_number']=$m[1];
									
									?>"/>
									<p style="font-size:10px;color:#a94442;">Invalid Value</p>
									
								    <?php }else{?>
									
									<input id="reg_name" onkeypress="return isNumberKey(event)" type="text" name="reg_name" class="form-control border" value="<?php if(isset($_CLASS->store_temporary_security_number)){echo $_CLASS->store_temporary_security_number;} ?>" 
									placeholder="<?php require_once BASE_CLASS .'math-security.php';
									$math = new BasicMathSecurity( 'math' );
									$m= $math->getField();
									echo $m[0];
									$_SESSION['remember_sec_number']=$m[1];
									
									?>"/>
									<p id="msg" style="font-size:10px;color:#a94442;"></p>
									<?php } ?>
								</div>
							</div>
							
							
							
						</td>
					</tr>					
					<tr class="agreement-row">
						<td colspan="2" id="buttom_right">
							<label style="float:left; width:360px">
							
							<?php if($_CLASS->store_temporary_agree==""){?>
								<input type="checkbox" name="term" value="accept" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <a href="#"><font color="#ed1c24">[Terms of Use] ,</font></a> <a href="#"></a> <font color="#ed1c24"> [Privacy Policy]</font></a></span>
							<?php }elseif($_CLASS->store_temporary_agree=="checked"){?>
							    <input type="checkbox" name="term" value="accept" checked="checked" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <a href="#"><font color="#ed1c24">[Terms of Use] ,</font></a> <a href="#"></a> <font color="#ed1c24"> [Privacy Policy]</font></a></span>
						    <?php }else{?>
						
								<input type="checkbox" name="term" value="accept" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <a href="#"><font color="#ed1c24">[Terms of Use] ,</font></a> <a href="#"></a> <font color="#ed1c24"> [Privacy Policy]</font></a></span>
								<p style="font-size:12px;color:red;"><b>Not check</b></p>
							<?php }?>
							
								
							</label>
		<!--				<input type="hidden" id="inputres" name="hidres" value="<?php //echo ($_SESSION['security_question_a'] + $_SESSION['security_question_b']);?>" />-->
        <!--</td>
        </tr>
        <tr class="agreement-row">
        <td id="buttom_right" colspan="2">
						<div class="form-group" style="width:132px;height:30px">

						
							<input  type="submit" id="signup-dealer" class="linkimg inquiry-button" name="signup-dealer" value="<?php echo $_LANG['REGISTER_SUBMIT'];?>"/>
				

						</div>
						</td>

					</tr>
		 
				</table>
			</form>
            <?php } ?> 
            <!--Dealer sign Up ends here-->
            
           
           <!--Personal sign Up start here-->
        
        <!--<?php if($_GET['member_type3'] == 'personal'){?>
            <form id="defaultForm"  class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="page-title"><?php echo $_LANG['REGISTER_BUSINESS_TYPE_PERSONAL'];?></div>
                <div class="status">Join free at Angkorauto.com, the no.1 auto trader in Cambodia.</div>
            
				<table class="vehicle-info-table" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<!--hidden field member type-->
							<!--<input type="hidden" name="member_type3" value="personal" />
                            
                            
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_firstname)){echo $_CLASS->store_temporary_firstname ;}?>" class="form-control border" type="text" name="first_name" id="user_id" placeholder="<?php echo $_LANG['REGISTER_FIRSTNAME'];?>*" style="width:320px"/>
							</div>
						</td>
						<td class="tdRight">
							<div class="form-group">
							    <input value="<?php if(isset($_CLASS->store_temporary_lasttname)){echo $_CLASS->store_temporary_lasttname ;}?>" class="form-control border" type="text" name="last_name" id="user_id" placeholder="<?php echo $_LANG['REGISTER_LASTNAME']; ?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style='width:320px;' />
								
							</div>
						</td>
                        <td rowspan="8" valign="top">
                        <div style="width:260px; height:auto"></div>
                        </td>
					</tr>
					<tr>
						<td>
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Email)){echo $_CLASS->store_temporary_Email;}?>" type="text" id="email" name="email" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_EMAIL'];?>*" style="width:320px;"/>
								
							</div>
						</td>
						<td class="tdRight">
							
							<div class="form-group">
                    <select id="sex" class="form-control border"  name="gender" style="width:320px" >
					<option selected="selected" value=''><?php echo $_LANG['REGISTER_SEX'];?></option>
                     <?php if(isset($_CLASS->store_temporary_gender)){?>
						   <?php if($_CLASS->store_temporary_gender=="M"){?>
							<option value='M' selected="selected">M</option>
							<option value='F'>F</option>
						   <?php }else{?>
						   <option value='F' selected="selected">F</option>
						   <option value='M'>M</option>
						   <?php }?>
					 <?php }else{?>
					 <option value='M'>M</option>
                     <option value='F'>F</option>
					 <?php }?>
                    </select>	
			
							</div>
							
						</td>
					</tr>
                    
					<tr>
						<td class="left">
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Password)){echo $_CLASS->store_temporary_Password;} ?>" type="password" id="password" name="password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_PASSWORD']; ?>*" onkeypress="/*return onlyAlphabets(event,this);*/" style="width:320px;"/>
								
							</div>
						</td>
						<td class="tdRight">
							<div class="form-group">
								<input value="<?php if(isset($_CLASS->store_temporary_Confirm_password)){echo $_CLASS->store_temporary_Confirm_password;}?>" type="password" id="passwords" name="confirm_password" class="form-control border" placeholder="<?php echo $_LANG['REGISTER_CONFIRM_PASSWORD'];?>*" style="width:320px;"/>
								
							</div>
						  
						 
						</td>
					</tr>
					<tr>
                    <td class="left">
						 <!--Phone Block -->
						<!--<div class="form-group">
								<input onkeypress="return isNumberKey(event)"  type="text" name="mobile" id="mobile" class="form-control border" value="<?php  if(isset($_CLASS->store_temporary_Phone)){echo $_CLASS->store_temporary_Phone ;} ?>" placeholder="<?php echo $_LANG['REGISTER_PHONE_NUMBER'];?>*" style="width:320px;position:relative;left:61px;"/>
								<input type="hidden" id="phonecode" name="phonecode" value=""/>
								<span class="error" id="mobile_error"><?php echo $_LOCAL['REGISTER_MOBILE_STATUS']; ?></span>
								<span class="error" id="invalid_mobile_error"><?php echo $_LOCAL['REGISTER_INVALID_MOBILE_STATUS']; ?></span>
						</div>
						<!-- End New -->					

						<!--</td>
                        <td class="tdRight">
							<div class="form-group">
                    <select id="sex" class="form-control border"  name="province_city" style="width:320px">
							<option selected="selected" value=''>Province/City</option>
                    <?php foreach($province as $value){?>
							<?php if($_CLASS->store_temporary_province==$value['province_name']){?>
								<option selected="selected" value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php } else{?>
							    <option value="<?php echo $value['province_name'];?>"><?php echo $value['province_name']; ?></option>
							<?php }?>
					<?php }?>
                    </select>	
			
							</div>
							
						</td>
                        </tr>
                       
                        <tr>
						<td colspan="2">
							
							<!--Security question Block -->
						  <!--<div class="form-group" >
						  
							<br/><span style="color: #999"><?php echo $_LANG['REGISTER_SECURITY_QUESTION'];?></span>
								<div class="security">
									<?php if($_CLASS->secu_message_null=="null"){?>
										
									<input  type="text" name="reg_name" class="form-control border" value="" 
									placeholder="<?php require_once BASE_CLASS .'math-security.php';
									$math = new BasicMathSecurity( 'math' );
									$m= $math->getField();
									echo $m[0];
									$_SESSION['remember_sec_number']=$m[1];
									
									?>"/>
									<p style="font-size:10px;color:#a94442;">Please Input Security Number</p>
									<?php }elseif($_CLASS->secu_message_invalid=="Invalid"){?>
									
									
									<input   type="text" name="reg_name" class="form-control border" value="" 
									placeholder="<?php require_once BASE_CLASS .'math-security.php';
									$math = new BasicMathSecurity( 'math' );
									$m= $math->getField();
									echo $m[0];
									$_SESSION['remember_sec_number']=$m[1];
									
									?>"/>
									<p style="font-size:10px;color:#a94442;">Invalid Value</p>
									
								    <?php }else{?>
									
									<input id="reg_name" onkeypress="return isNumberKey(event)" type="text" name="reg_name" class="form-control border" value="<?php if(isset($_CLASS->store_temporary_security_number)){echo $_CLASS->store_temporary_security_number;} ?>" 
									placeholder="<?php require_once BASE_CLASS .'math-security.php';
									$math = new BasicMathSecurity( 'math' );
									$m= $math->getField();
									echo $m[0];
									$_SESSION['remember_sec_number']=$m[1];
									
									?>"/>
									<p id="msg" style="font-size:10px;color:#a94442;"></p>
									<?php } ?>
								</div>
							</div>
							
							
							
						</td>
					</tr>					
					<tr class="agreement-row">
						<td colspan="2" id="buttom_right">
							<label style="float:left; width:360px">
							
							<?php if($_CLASS->store_temporary_agree==""){?>
								<input type="checkbox" name="term" value="accept" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <a href="#"><font color="#ed1c24">[Terms of Use] ,</font></a> <a href="#"></a> <font color="#ed1c24"> [Privacy Policy]</font></a></span>
							<?php }elseif($_CLASS->store_temporary_agree=="checked"){?>
							    <input type="checkbox" name="term" value="accept" checked="checked" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <a href="#"><font color="#ed1c24">[Terms of Use] ,</font></a> <a href="#"></a> <font color="#ed1c24"> [Privacy Policy]</font></a></span>
						    <?php }else{?>
						
								<input type="checkbox" name="term" value="accept" />
								<span style="color: #a3a4a4; font-size:12px"><?php echo $_LANG['REGISTER_TERM'];?> <a href="#"><font color="#ed1c24">[Terms of Use] ,</font></a> <a href="#"></a> <font color="#ed1c24"> [Privacy Policy]</font></a></span>
								<p style="font-size:12px;color:red;"><b>Not check</b></p>
							<?php }?>
							
								
							</label>
		<!--				<input type="hidden" id="inputres" name="hidres" value="<?php //echo ($_SESSION['security_question_a'] + $_SESSION['security_question_b']);?>" />-->
        <!--</td>
        </tr>
        <tr class="agreement-row">
        <td id="buttom_right" colspan="2">
						<div class="form-group" style="width:132px;height:30px">
						
							<input  type="submit" class="linkimg inquiry-button" name="signup" value="<?php echo $_LANG['REGISTER_SUBMIT'];?>" id="signup"/>
						
						</div>
						</td>

					</tr>
		 
				</table>
			</form>
            <?php } ?>
            <!--personal sign Up ends here-->
            
 
             
            
		</div><!-- #register -->
		
           				 <!--register form end here-->
                         
                          
             
           <div class="clear"></div>
	</div>
	
<!-- Import Script -->
<script type="text/javascript" src="<?php echo BASE_RELATIVE; ?>autocam_js/public_sign_up/index.js"></script>







<!-- Script for check agree -->
<script type="text/javascript">
window.onload = function() {
	document.getElementById("signup-seller").style.background='url(img/public_sign_up/submit-gray.jpg)';
	document.getElementById("signup-seller").disabled=true;
	
	}


function agree(val){
    	
    if (val.checked == true){
		//document.getElementById("signup-seller").disabled=true;
		var v1=document.getElementById("value1");
		var v2=document.getElementById("value2").value;
		
		
		if(v1.innerHTML==v2){
		document.getElementById("signup-seller").style.background='url(img/public_sign_up/submit.png)';
		document.getElementById("signup-seller").disabled=false;
		}
		else{
		  document.getElementById("signup-seller").disabled=true;
	      document.getElementById("signup-seller").style.background='url(img/public_sign_up/submit-gray.jpg)';
		}
		
		
	}
    else{
       document.getElementById("signup-seller").disabled=true;
	   document.getElementById("signup-seller").style.background='url(img/public_sign_up/submit-gray.jpg)';
	   
    }
}           
</script>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);
		
		var fileInput = document.getElementById("uploadImage1").files[0];
		var allowed = ["jpeg","png"];
		var found = false;
		var fsize=((((fileInput.size)/1024)/1024)/1024);
		var fsize_mb=(fsize.toFixed(5));
		
		
		allowed.forEach(function(extension) {
                   // alert(fsize_mb);


			if (fileInput.type.match('image/'+extension) && fsize_mb <=2) {
					found = true;
					 oFReader.onload = function (oFREvent) {
					document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
					document.getElementById("clear-message").value=""; //Clear message (please choose image.)
					};
							
			}
				
		})
		if(found==false){
		  document.getElementById("filenotfound").innerHTML="Upload only file(jpeg , png) and less than 2MB";
		  document.getElementById("uploadPreview1").src = "images/profile.png";
		  document.getElementById("uploadImage1").value="";
		  
		}
		else{
			document.getElementById("filenotfound").innerHTML="";
		}
		
		
		
    }
	
	function DeleteImage(){
			document.getElementById("uploadPreview1").src = "images/profile.png";
			document.getElementById("uploadImage1").value="";
		}

</script>

