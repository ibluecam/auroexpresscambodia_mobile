
var selectedCountryLoading="";
var selectedCountryDest="";
var selectedVesselType="";
$(document).ready(function(){

	/**** + DEFAULT PORT SELECT AFTER PAGE LOADED ****/
    //selectedCountryLoading = $("#countryLoadingSelect").val();
	//$("#portLoadingSelect").load("ajax/load_port.php?country="+selectedCountryLoading);
	// var countryLoading="<?php if(isset($shippingInfo['country_loading'])) echo $shippingInfo['country_loading'];?>";
 //    var countryDest="<?php if(isset($shippingInfo['country_dest'])) echo $shippingInfo['country_dest'];?>";
 //    var portLoading="<?php if(isset($shippingInfo['port_loading'])) echo $shippingInfo['port_loading'];?>";
 //    var portDest="<?php if(isset($shippingInfo['port_dest'])) echo $shippingInfo['port_dest'];?>";
 //    var vesselType="<?php if(isset($shippingInfo['vessel_type'])) echo $shippingInfo['vessel_type'];?>";
 //    var size="<?php if(isset($shippingInfo['size'])) echo $shippingInfo['size'];?>";
 //    var transitTime="<?php if(isset($shippingInfo['transit_time'])) echo $shippingInfo['transit_time'];?>";
 //    var line="<?php if(isset($shippingInfo['line'])) echo $shippingInfo['line'];?>";

    if(countryLoading!=""){ 
    	$("#countryLoadingSelect").val(countryLoading);
    	$("#portLoadingSelect").load("ajax/load_port.php?country="+countryLoading, function(e){
    		if(portLoading!=""){
    			$(this).val(portLoading);
    		}
    	});
    }
    if(countryDest!=""){
    	$("#countryDestSelect").val(countryDest);
    	$("#portDestSelect").load("ajax/load_port.php?country="+countryDest, function(e){
    		if(portDest!=""){
    			$(this).val(portDest);
    		}
    	});
    }
    if(vesselType!=""){
    	$("#vesselTypeSelect").val(vesselType);
    	$("#sizeSelect").load("ajax/load_size.php?vessel_type="+vesselType, function(e){
    		if(size!=""){
    			$(this).val(size);
    		}
    	});
    } 
    if(transitTime!="") $("#transitTimeSelect").val(transitTime);
    if(line!="") $("#lineSelect").val(line);

	/**** - DEFAULT PORT SELECT AFTER PAGE LOADED ****/

	/**** + WHEN USER CLICK ON ANY COUNTRY IT LOADS PORT LIST OF THAT COUNTRY ****/
	$("#countryLoadingSelect").change(function(e) {
		selectedCountryLoading = $("#countryLoadingSelect").val();
		
		$("#portLoadingSelect").load("ajax/load_port.php?country="+selectedCountryLoading);
	   
    });

    $("#countryDestSelect").change(function(e) {
		selectedCountryDest = $("#countryDestSelect").val();
		
		$("#portDestSelect").load("ajax/load_port.php?country="+selectedCountryDest);
	     
    });

    $("#vesselTypeSelect").change(function(e) {
		selectedVesselType = $("#vesselTypeSelect").val();
		
		$("#sizeSelect").load("ajax/load_size.php?vessel_type="+selectedVesselType);
	    
    });
    
    /**** - WHEN USER CLICK ON ANY COUNTRY IT LOADS PORT LIST OF THAT COUNTRY ****/

    
	
});
	




