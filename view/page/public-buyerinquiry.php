<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'buyerinquiry';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$listinquiry=$_CLASS->listinquiry();
$country_list = $_CLASS->getCountryList();
$pagination=$_CLASS->pagination_html;
$total=$_CLASS->gettotal();
$total_page=$_CLASS->gettotalpage();
$current_page=$_CLASS->getcurrentpage();
$expert_company = $_CLASS->loadExpertCompany();
?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/buyer-inquiry.js"></script>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/register_item.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/buyer_inquiry.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/pagination.css" />
<script type="text/javascript">
    $(document).ready(function(e) {
        var option='<?php if(isset($_GET['rdo-car-type'])) echo $_GET['rdo-car-type'];?>';
        var select='<?php if(isset($_GET['countryInput'])) echo $_GET['countryInput'];?>';
        var searchoption='<?php if(isset($_GET['dpsearch'])) echo $_GET['dpsearch'];?>';
        var text='<?php if(isset($_GET['searchtext'])) echo $_GET['searchtext'];?>';
        if (option!="")
        {$("[name=rdo-car-type]").val([option]);}
        
        $('#countryInput').val(select);
        $('#dpsearch').val(searchoption);
        $('#searchtext').val(text);
    });
</script>
  <div class="container">
            
            <?php include('php/public-inquiry-left-sidebar.php');?>

            <div class="content-wrapper">
                 <div class="page-title">
                    
                        <table><tr><td><img class="brick" src="images/common/register_item/blue-brick.png" /></td><td>Buyer's Inquiry</td></tr></table> 
                 </div>
                 <form method="get" id="frmbuyerinquiry" name="frmbuyerinquiry" action="<?php echo BASE_RELATIVE;?>buyerinquiry">
                     <div class="car-search-input-wrapper">
                         <div class="car-type-wrapper">
                         
                            <label><input checked="checked" type="radio" value="all" name="rdo-car-type" /><span>All</span></label>
                            <label><input type="radio" value="Car" name="rdo-car-type" /><span>Car</span></label>
                            <label><input type="radio" value="Bus" name="rdo-car-type" /><span>Bus</span></label>
                            <label><input type="radio" value="Truck" name="rdo-car-type" /><span>Truck</span></label>
                            <label><input type="radio" value="Equipment" name="rdo-car-type" /><span>Heavy Machine</span></label>
                            <label><input type="radio" value="accessory" name="rdo-car-type" /><span>Parts/Accessories</span></label>
                            <label><input type="radio" value="other" name="rdo-car-type" /><span>Other</span></label>
                         </div>
                         <div class="buyer-country-wrapper">
                            <label>Buyer Country: </label>
                            
                            <select style="width:150px;" name="countryInput" id="countryInput">
                                <option style="display:block;" value="">-- ALL--</option>
                                <?php
                                    if( count($country_list['cc']) > 0 ){
                                    
                                        for( $i=0; $i < count($country_list['cc']); $i++ ){
                                            if( !empty($country_list['country_name'][$i]) ){
                                          ?>
                                    <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                          <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0">Invalid Country.</option>
                                <?php
                                    }
                                ?>
                            </select>
                            <select name="dpsearch" id="dpsearch">
                               	<option value="" selected>-- Select --</option> 
                                <!--option value="title" >Title</option-->
                                <option value="sellerid" >Seller ID</option>
                                <option value="buyerid" >Buyer ID</option>
                                <option value="itemnumber" >Item Number</option>
                                <!--option value="orderno" >Order No</option-->

                            </select>
                            <div class="searchVehicleBox">
                                <input style="width:150px;" type="text" name="searchtext" id="searchtext">
                                <input style="width:120px;margin-left:10px;" type="submit" name="btnsearch" id="btnsearch" value="Search">
                            </div>

                         </div>
                     </div> 
                 <table class="item-list-wrapper"> 
                 	<tr>
                    	<th>No.</th><th>Requested Item</th><th>Buyer's Message</th><th>Buyer Information</th>
                    </tr>
                    <?php 
                    foreach ($listinquiry as $lst) {
                         
                        ?>
                    <tr>
                        	<td><?php echo $lst['id'];?></td>
                        	<td><img src="<?php echo BASE_RELATIVE.$lst['fileurl'];?> "/></td>
                            <td class="buyer-message" style="word-wrap:break-word;"  >
                            	<a href="<?php echo BASE_RELATIVE.'buyerinquiry-detail?id='.$lst['id'];?>">[ <?php echo $lst['vehicle_type']?> ] 
                                <?php echo $lst['make'].' '.$lst['model']?></a>
                                <div id="pcontent">
                                <p id="pmessage" style="overflow:hidden;text-align:left;"> 
                                    <?php echo substr($lst['message'],0,150)."...";?>
								                </p>
                              </div>
                            </td>
                            <td style="width:100px;">
                              <img class="brick" src="<?php echo BASE_RELATIVE.'images/flag/'.$lst['country'];?>.png" /><?php echo $lst['country_name']?>
                                <br/> 
                                <?php echo date("F. j. Y g:i a",strtotime($lst['dates']));?>
                              </td>
                    </tr> 

             <?php 
                    }
            ?>
              

                 </table>
                 <div class="pagination-wrapper">
                 	<span class="page-count">Total :<?php echo $total.' item   page: '.$current_page.' / '.$total_page;?></span>
                   <div id="pagination">
                    <?php 
      
                    echo $pagination;
                        
                    ?>

               </div>         
            </div>
             </form>
            </div>
          
        </div>