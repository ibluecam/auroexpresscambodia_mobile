<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminnewsletter {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $newsletter;
    protected $mail_template;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminnewsletter($lang=array()){
        $this->lang = $lang;
        
        // load newsletter list.
        $this->loadNewsletter();
        
        // handle send newsletter form.
        if( isset($_POST['sendbtn']) ){
            $this->handleForm();
        }
        
        // load email template.
        $this->loadEmailTemplate();
    }
    
    /**
     * Private method: handle send newsletter form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleForm(){
        // set email list as string.
        $list = '';
        $i = 1;
        foreach($this->newsletter as $k => $v ){
            $list .= $v;
            
            if( $i < count($this->newsletter) ){
                $list .= ',';
            }
            
            $i++;
        }
        
        // set $to field.
        $to = ADMIN_CONTACT_EMAIL;
        
        // subject.
        $subject = trim($_POST['subjectInput']);
        
        // message.
        $message = trim($_POST['msgInput']);
        $message = stripslashes($message);
        
        // validate.
        if( empty($subject) ){
            $subject = $this->lang['NEWSLETTER_NEWSLETTER_DEFAULT_SUBJECT'] . ' [' . date('Y.m.d') . ']';
        }
        
        // headers.
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'To: ' . $to . "\r\n";
        $headers .= 'From: ' . FRAMEWORK_NAME . ' <' . $to . '>' . "\r\n";
        $headers .= 'Bcc: ' . $list  . "\r\n";
        
        // send message.
        if( !@mail($to, $subject, $message, $headers) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to send newsletter email due a mail() error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['NEWSLETTER_MAIL_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->form_message = $this->lang['NEWSLETTER_MAIL_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: load email template.
     * <br>---------------------------------------------------------------------
     * @return string
     */
    private function loadEmailTemplate(){
        $path = BASE_ROOT . 'view/email/template.html';
        $this->mail_template = '';
        
        if( file_exists($path) ){
            if( !$fo = @fopen($path,'r') ){
                require_once BASE_CLASS . 'class-log.php';
                LogReport::write('Unable to load email template. Read rights denied at ' . __FILE__ . ':' . __LINE__);
                return;
            }
            else {
                if( !$data = @fread($fo, filesize($path)) ){
                    @fclose($fo);
                    require_once BASE_CLASS . 'class-log.php';
                    LogReport::write('Unable to load email template. Read rights denied at ' . __FILE__ . ':' . __LINE__);
                    return;
                }
                else {
                    $data = str_replace('[:::MESSAGE:::]', '', $data);
                    $this->mail_template = $data;
                    @fclose($fo);
                }
            }
        }
    }

    /**
     * Private method: load newsletter email addresses list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadNewsletter(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `email` FROM `feedback`") ){
            $cnx->close();
            
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load newsletter list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['NEWSLETTER_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->newsletter = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->newsletter,$r['email']);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    
    /**
     * Public method: get email template
     * <br>---------------------------------------------------------------------
     * @return string
     */
    public function getEmailTemplate(){
        return $this->mail_template;
    }
    
    /**
     * Public method: get newsletter addresses
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getNewsletter(){
        return $this->newsletter;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}