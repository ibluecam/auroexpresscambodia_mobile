<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'my-inquiry';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$listmyinquiry = $_CLASS->listinquiry();
$pagination=$_CLASS->pagination_html;
$total_row=$_CLASS->total_num_row;
$current_page=$_CLASS->current_page;
$total_page=$_CLASS->total_page;




?>
 <div id="sectionContenWrapper">
        	<!--sectionSidebar-->
            <div id="topMenu">
            	<p><a href="#" class="linkfade">My iBlue</a> > My Inquiry</p>
            </div>
        	<?php include("php/sidebar/my-wini.php");?>

            </div>
            <div id="sectionContent">
            	<div id="payment_list">
                	<p>My Inquiry</p>
                </div>
                <div id="checking">
                	<p>Check here all the requests you have made for cars, parts/accessories, and other vehicles.</p>
                </div>
                <form method="get">
                <div id="inquiry_search">

                	<table border="0" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td class="all"><input type="radio" name="radio" value="all"/><span>All<span></td>
                            <td class="car"><input type="radio" name="radio" value="Car"/><span>Car</span></td>
                            <td class="car"><input type="radio" name="radio" value="Bus" /><span>Bus</span></td>
                            <td class="truck"><input type="radio" name="radio" value="Truck" /><span>Truck</span></td>
                            <td class="equipment"><input type="radio" name="radio" value="Equipment" /><span>Heavy Machine</span></td>
                            <td class="part"><input type="radio" name="radio" value="Part/Accessories" /><span>Part/Accessories</span></td>
                            <td class="other"><input type="radio" name="radio" value="Other" /><span>Other</span></td>
                        </tr>
                    </table>

					<div class="clearfix">
                    	<div id="select_left">
                            <select id="select_left" name="select_left" style="width:140px;">
                            	<option value="" style="display:none;">--Select--</option>
                            	<option value="Hyundai">Hyundai</option>
                                <option value="Toyota">Toyota</option>
                                <option value="Kai">Kai</option>
                                <option value="Lexus">Lexus</option>
                            	<option value="Honda">Honda</option>
                            </select>
                        </div>
                        <div id="select_right" style="padding-top:10px;">
	                        <input type="text" placeholder=" Keyword"  class="keyword_selectbox"/>
                        </div>
                    </div>
                    <div id="search_btn">
                    	<input type="submit" name="btnsearch" class="submit" value="" />

                    </div>
                    </form>


                    <div id="show_cardetail" >
                        <div id="car_count">
                            <p>Total :<?php echo $_CLASS->total_num_row;?> items <span>Page <?php echo $_CLASS->current_page;?> / <?php echo $_CLASS->total_page;?></span></p>
                        </div>
                        <?php
					$sql="SELECT inquiry.id, inquiry.`owner`,inquiry.dates,inquiry.vehicle_type,inquiry.`condition`,inquiry.make,inquiry.model,inquiry.year_from,inquiry.year_to,inquiry.steering,inquiry.transmission,inquiry.fuel_type,inquiry.order_quantity,inquiry.country,inquiry.`port`,inquiry.price_from,inquiry.price_to,inquiry.price_term,inquiry.want_to_receive,inquiry.payment_terms,inquiry.message,inquiry.fileurl,rg.email
FROM
 inquiry  left join register rg on inquiry.`owner`=rg.user_id WHERE r.email='".$_SESSION['log_email'];
					?>

                        <?php
							foreach ($listmyinquiry as $lst) {
						?>
                        <div class="car_detail clearfix" style="position:relative">

                        	<div class="car_img">
                            	<img src="<?php echo BASE_RELATIVE. $lst['fileurl'];?>" />

                            </div>
                            <div class="car_text">
                            	<p>
                                	<span><a href="<?php echo BASE_RELATIVE.'myinquiry-detail?id='.$lst['id'];?>">[ <?php echo $lst['vehicle_type']?> ]&nbsp;<?php echo $lst['model']; ?></a></span><br/>
                                    <?php echo substr($lst['message'],0,150)."..."; ?><br/>

                                	<a href="<?php echo BASE_RELATIVE;?>myinquiry-detail?c_id=F2H2DX538MEB3PX00J4PP&m_id=14&inbox=true"></a><br/>


                                    <span class="hit">HIT :&nbsp;<?php echo $lst['hit']; ?> MESSAGE :&nbsp;<?php echo $lst['hit']; ?>
                                </p>
                            </div>
                            <div class="car_date">
                            	<p>
                                <?php echo $lst['dates']; ?>
                                	<br/>
                                </p>
                                <span><a href="#" class="linkfade">Replied by the  seller</a></span>
                            </div>

                            <div class="edit-delete">

                                <a href="<?php echo BASE_RELATIVE."write-inquiry?in_id=".$lst['id'];?>"><img src="<?php echo BASE_RELATIVE;?>images/common/button/search.png" class="edit" /></a>

                                <input id="btndelete" class="delete" type="button" name="delete" value="" onclick="delete_inquiry(<?php echo $lst['id']; ?>)" />

                            </div>

                        </div>
                         <?php }?>

                    </div><!-- end div id="show_cardetail"-->
                   	<div id="pagination">
                    	<?php

     echo $pagination;
     // echo $_CLASS->current_page;

?>

                    </div>
                </div>



            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->


