<?php

if(!isset($_SESSION)) @session_start();


$_SESSION['security_question_a'] = mt_rand('1','9');
$_SESSION['security_question_b'] = mt_rand('1','9');
	$stock_detail = $_CLASS->stock_detail;
    $result=$_CLASS->result;
    $countryList=$_CLASS->getCountryList();
    include('php/ip2locationlite.class.php');
    //Load the class
    $ipLite = new ip2location_lite;
    $ipLite->setKey('0ceddca8fc2527329d305fb922e7bbade230ca741a2a68055b649696326992be');

    //Get errors and locations
    $locations = $ipLite->getCity($_SERVER['REMOTE_ADDR']);
    $errors = $ipLite->getError();

    if(isset($_GET['result'])) $result=htmlspecialchars($_GET['result']);
    //var_dump($_SESSION);
    //if(isset($_SESSION['log_group'])) $log_group=$_SESSION['log_group']; $log_group='';

    if($_SESSION['log_group']=='user'||$_SESSION['log_group']=='admin'){
        $login_popup="#";
        $button_id="action_submit";
    }else{
        $login_popup="#inline_content";
        $button_id="";
    }

    //Check whether the buyer has attemsiblpted to submit or not
    if(isset($_GET['id'])) $get_id=htmlspecialchars($_GET['cid']); else $get_id="";
    if(count($stock_detail)>0){
	    if(isset($_POST['subjectInput'])) $subjectInput=htmlspecialchars($_POST['subjectInput']); else $subjectInput= "I am interested in your ".$stock_detail[0]['model_year'].' '.$stock_detail[0]['make'].' '.$stock_detail[0]['model'];
	    if(isset($_POST['messageInput'])) $messageInput=htmlspecialchars($_POST['messageInput']); else $messageInput='';
	    if(isset($_POST['emailInput'])) $emailInput=htmlspecialchars($_POST['emailInput']); else $emailInput=$_SESSION['log_email'];
	}
    //If the product was attempted to be sent previously, load the previous message
    if(isset($_SESSION['tmp_product_id'])) $product_id=$_SESSION['tmp_product_id'];  else $product_id='';

    if($product_id==$get_id&&!empty($get_id)&&!empty($product_id)){
        if(isset($_SESSION['tmp_subject_input'])) $subjectInput=htmlspecialchars($_SESSION['tmp_subject_input']);
        if(isset($_SESSION['tmp_message_input'])) $messageInput=htmlspecialchars($_SESSION['tmp_message_input']);
    }
?>

<div class="main-div">

    <div class="page-title">
		<table><tr><td><img class="brick" src="<?php echo BASE_RELATIVE;?>images/logout-icon/pop-up-sign-up.png" /></td><td>Contact Seller</td></tr></table>
	</div>

    <div id="content-seller">
        <?php if(empty($result)&&count($stock_detail)>0){ ?>
    	<div id="sub-content">
         <form method="post" class="test" id="form_message" class="form-horizontal" enctype="multipart/form-data">
        	<input type="hidden" value="<?php if(isset($_GET['cid'])) echo htmlspecialchars($_GET['cid']); ?>" name="product_id"/>
            	<table>
                	<tr>

                    	<th id="label">Form :</th>

                        <td>
                        <input type="text" name="emailInput" class="textbox" value="<?php echo $emailInput;?>" placeholder="email address"/>
                        <?php if(isset($_CLASS->error['email'])){ ?>
                            <div style="margin-left:26px;color:red"><?php echo $_CLASS->error['email'];?></div>
                        <?php } ?>
                        </td>

                    </tr>

                    <tr>
                    	<td></td>
                        <td style="padding:0 0 15px 32px; line-height:0;">
                        <font color="red" id="msg"></font>
                        </td
                    ></tr>

                    <tr>
                    	<th id="label">To :</th>
                        <td>
                        <label class="label-sell"><?php echo $stock_detail[0]['company_name']; ?></label>
                        </td>
                    </tr>
                    <tr>
                    	<th></th>
                        <td>
                        	<div class="detail">
                                <hr class="line"/>
                                <a target="_blank" href="vehicle-detail?cid=<?php echo $stock_detail[0]['id']?>"><img class="image" src="<?php echo $stock_detail[0]['thumb']?>" width="140"></a>
                                <span id="content-text">
                                    <table>
                                        <tr>
                                            <td>Stock Id: <?php echo $stock_detail[0]['id'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-detail">Steering: <?php echo $stock_detail[0]['steering'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-detail">
                                            <img width="15" height="12" src="<?php echo BASE_RELATIVE;?>images/flag/<?php echo $stock_detail[0]['flag'];?>.png"/>
                                            <?php echo $stock_detail[0]['country']?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-detail">Seller: <?php echo $stock_detail[0]['company_name']?></td>
                                        </tr>
                                        <tr>
                                            <td style="color:red;" class="text-detail"> <?php

											if ($stock_detail[0]['price'] == 0) {
												echo "Ask for Price";
											}
											else {
												echo "FOB: ";
												echo $stock_detail[0]['currency']." ";
												echo number_format($stock_detail[0]['price']);
											}



											?>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td style="color:blue;" class="text-detail" id="click"><a target="_blank" href="vehicle-detail?cid=<?php echo $stock_detail[0]['id']?>"><font color="blue">More Detail</font></a></td>

                                        </tr>
                                    </table>
                                </span>
                                <p class="clear"></p>
                           </div>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="sub-content-right" >

            	<table>
                	<tr>
                    	<th id="label">Subject :</th>
                        <td>
                        <input type="text" id="subjectInput" required name="subjectInput" class="textbox" value="<?php echo $subjectInput; ?>"/>
						</td>
                    </tr>
                    <tr>
                    	<th class="label-textarea">Message :</th>
                        <td>

                        <textarea dropzone="txtmessage" name="messageInput" id="txtarea" class="textarea"><?php echo $messageInput;?></textarea>
                        </td>
                    </tr>

                    <tr>
                    	<td></td>
                    	<td align="right">
                         <?php
                            if($_SESSION['log_id']>0){ ?>

                                      <input type="submit" value="Send" name="submit" class="hover" id="submit_send"/>

                            <?php }else{ ?>
                                      <a href="<?php echo $login_popup;?>"  class='inline cboxElement'><img class="btn-send" src="<?php echo BASE_RELATIVE;?>images/logout-icon/Contact-Seller.png" /></a>

                       <?php } ?>
                            </td>
                    </tr>

                </table>

        </div>
        </form>
        <div style='display:none;'>
		<div id='inline_content' style="margin:0 auto 0 auto; width:240px;">
        	<span><a class="btnclose" href="#"><img  style="padding:3px 0;" src="images/logout-icon/Close.png"></a></span>
            <div id="tabs-container">
                <ul class="tabs-menu">
                    <li id="tab-1"><a href="#">SignIn</a></li>
                    <li id="tab-2"><a href="#">SignUp</a></li>
              	</ul>
                <div class="tab">
                    <div class="tab-1 tab-content">
                        <div class="content-area">
                            <form action="contact-seller"  method="POST" name="loginform" id="loginform" class="form-horizontal" enctype="multipart/form-data">
                            	<div class="form-group">
                                <p class="label">ID<label class="star" style=" padding-top:10px;">*</label></p>
                                <div class="col-md-5">
                                <input type="text" id="username" class="form-control text" name="emailInput" placeholder="ID..." style="color:#333; font-size:12px;">
								</div>
                                </div>
                                <div class="form-group">
                                <p class="label">Password<label class="star">*</label></p>
                                <div class="col-md-5">
                                <!-- <input id="tab-pass" type="text" name="passwordInput" class="text" value="Password" /> -->
                                <input type="password" id="pass" class="form-control password text" placeholder="Password" name="passwordInput" style="color:#333; font-size:12px;"><br />
                                </div>
                                </div>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <label><input type="checkbox" name="remember" class="check" /> Remember Me</label>
                                            <p><a href="#">Forget your password</a></p>
                                        </td>
                                        <td><div id="sign_in">
                                    <input type="button" id="sign_inbtn" class="sign_in linkfade"  value="Sign In" name="loginBtn">
                                    <input type="hidden" value="<?php if(isset($_GET['cid'])) echo htmlspecialchars($_GET['cid']); ?>" name="product_id"/>

                                     <input type="hidden" id="subjectInputpop" name="subjectInput" class="textbox" value="<?php echo $subjectInput; ?>"/>
                                      <textarea  style="display: none;"  dropzone="txtmessage" name="messageInput" id="get" class="textarea"><?php echo $messageInput; ?></textarea>

                                </div></td>
                                    </tr>
                                </table>
                            </form>

                        </div>
                        <ul>
                            <li><a id="fb" class="a2a_button_facebook" style="cursor:pointer"><img  src="<?php echo BASE_RELATIVE;?>images/contact-seller/bt-fb.png" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="tab-2 tab-content" >
                        <div class="content-area" style="border-top:1px solid #ccc;">


                            <form  method="post" id="defaultForm"  class="form-horizontal" enctype="multipart/form-data">


                                <div class="form-group">
                                <p class="label">ID<label class="star" style=" padding-top:10px;">*</label></p>
                        		<div class="col-md-5">
								<input type="text" class="form-control text"id="id" name="log_id" style="color:#333; font-size:12px;"  placeholder="4-12 characters" value="<?php echo ( isset($_SESSION['reg_log_id']) ? $_SESSION['reg_log_id'] : '' );?>" />
								</div>
                                </div>
                                <div class="form-group">
								<p class="label">Email<label class="star">*</label></p>
                                <div class="col-md-5">
								<input type="email" class="form-control text"  name="email" style="color:#333; font-size:12px;" placeholder="Please enter your valid email address" />
                                </div>
                                </div>
                                <div class="form-group">
								<p class="label">Password<label class="star">*</label></p>
                                <div class="col-md-5">
								<input type="password" class="form-control text" name="password" style="color:#333; font-size:12px;"  id="password" placeholder="4-12 characters" onKeyPress="capLock(event)"/>
                                </div>
                                </div>
                                <div class="form-group">
								<p class="label">Confirm Password<label class="star">*</label></p>
                                <div class="col-md-5">
								<input type="password" class="form-control text"  name="confirm_password" style="color:#333; font-size:12px;" placeholder="Please enter your password again." id="confirm_password" onKeyPress="capLock1(event)" />
                                </div>
                                </div>
                                <div class="form-group">
								<p class="label">Company Name<label class="star">*</label></p>
                                <div class="col-md-5">
								<input type="text" class="form-control text"  name="company_name" style="color:#333; font-size:12px;" value="<?php echo ( isset($_SESSION['reg_company_name']) ? $_SESSION['reg_company_name'] : '' ); ?>" />
                                </div>
								</div>
                                <div class="form-group">
								<p class="label">Contact Persion<label class="star">*</label></p>
                                <div class="col-md-5">
								<input type="text" class="form-control text"  name="contactpersion" style="color:#333; font-size:12px;" value="<?php echo ( isset($_SESSION['reg_contact_persion']) ? $_SESSION['reg_contact_persion'] : '' ); ?>" />
                                </div>
								</div>
								<p class="label">Country<label class="star">*</label></p>
                                        <?php
                                        $countryCode = '';
                                        //Getting the result
                                        if (!empty($locations) && is_array($locations)) {
                                          $countryCode= strtolower($locations['countryCode']);
                                          $_SESSION['countryCode']= $countryCode;
                                        }
                                    ?>
									<div class="form-group">
                                    <div class="col-md-5">
									<select id="country" class="form-control"  name="country" style="color:#424242; text-transform:uppercase; background:url(images/flag/<?php echo $_SESSION['countryCode'].'.png'; ?>) no-repeat 10px center; padding-left:30px; border:1px solid #abadb3; width:208px;">
										<?php foreach($countryList as $country){ ?>
										<option label="<?php echo $country['phonecode']?>"  value='<?php echo $country['cc']; ?>' <?php if($country['cc'] == $countryCode ){echo " selected=selected ";}?>style='background:url(images/flag/<?php echo $country['cc'].'.png'; ?>) no-repeat left center; padding-left:20px;'> <?php echo $country['country_name'];?></option>
										<?php }?>
									</select>
									</div>
                                    </div>
                                <p class="label">Security Question<label class="star">*</label></p>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                    <td><?php echo $_LOCAL['REGISTER_SECURITYQUESTION_QUESTION'];?>:&nbsp;<?php echo $_SESSION['security_question_a'];?> + <?php echo $_SESSION['security_question_b'];?>?</td>
                                    <td>

										<input class="form-control text short-text" style="color:#333; font-size:12px;" type="text"  id="inputsecurity"  name="securityInput" placeholder="<?php echo $_LOCAL['REGISTER_SECURITYQUESTION_LABEL'];?>" />

									</td>

                                    </tr>
                                    
                                </table>

                                <input type="hidden" id="inputres" name="hidres" value="<?php echo ($_SESSION['security_question_a'] + $_SESSION['security_question_b']);?>" />

                                <input id="btn-inquiry-submit" class="inquiry-button submit" type="submit" name="regbtn" value="Send">

                                <!-- <input type="image" class="submit" name="signup" src="<?php echo BASE_RELATIVE;?>images/contact-seller/bt-sign.png" /> -->
                                      <input type="hidden" id="subjectInputpopsignup" name="subjectInput" class="textbox" value="<?php echo $subjectInput; ?>"/>
                                      <textarea  style="display: none;"  dropzone="txtmessage" name="messageInput" id="getsignup" class="textarea"><?php echo $messageInput; ?></textarea>

                            </form>
                        </div>
                    </div>
                </div>
        </div>
      </div>
     </div>
        <?php }elseif($result=="success"){ ?>
        	<?php if(count($stock_detail)>0){?>
	            <div style="padding: 50px 0;text-align: center; color:black;font-size:16px;">

	                Dear <b><?php echo $_SESSION['company_name'];?></b>,<br/><br/>
	                <img src="images/tick.png" style="padding-right:10px;"><font color='green'>Your inquiry has been sent successfully to: <b><?php echo $stock_detail[0]['company_name'];?></b>.</font><br/><br/>
	                Thank you for visiting Motorbb.com<br/><br/><br/>
	                <a href="<?php echo BASE_RELATIVE;?>">Click here</a> to go to homepage.
	            </div>
	        <?php }else{?>
	        	<div style="padding: 50px 0;text-align: center; color:black;font-size:16px;">
	        		<font color='red'>We are sorry. The inquiry was not sent! <br/><br/>There is no item found with this ID at the moment.</font><br/><br/>
	                <br/><a href="<?php echo BASE_RELATIVE;?>">Click here</a> to go to homepage.
	            </div>
	        <?php }?>
        <?php } ?>
    </div>
    <div class="clear"></div>
</div>
    <script type="text/javascript">

			$(document).ready(function(){
                $("#sign_inbtn").click(function(){
                    $.ajax({
                        url     : 'ajax/get-session.php',
                        type    : 'POST',
                        data    :  $('#loginform').serialize(),
                        success : function(data){
                          console.log(data);
                        if(data.data!=null){
                            window.location.href="contact-seller"+data.pro;

                        }else{

                            $('#username').css({"color":"red"});
                            $('#pass').css({"color":"red"});

                        }


                        },
                        error: function(){
                            console.log('error');


                        }
                    });

                    });
					
				$(".inline").colorbox({inline:true, width:"100% auto",height:"100% auto"});
				
				
 

    $(document.body).on('click', ".facebook_login", function(e){
        var form_data=$('#form_message').serializeArray();

        e.preventDefault();
        $.ajax({
            url     : 'ajax/push_tmp_product_inquiry_session.php',
            type    : 'post',
            data    :  $.param(form_data),
            success : function(data){

                // console.log(data);

                var redirect_url='';
                <?php
                    if(count($stock_detail)>0){
                        echo "redirect_url=\"".urlencode("contact-seller?cid={$stock_detail[0]['id']}&action=submit")."\";";
                    }
                ?>
                window.location="login?provider=Facebook&redirect="+redirect_url;
            },
            error: function(){
                alert("Error! Cannot redirect to facebook login page. \n");
            }
        });

    });
    			
    $('#defaultForm').bootstrapValidator({
 
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            log_id: {
                message: '<p style="font-size:10px; width:200px;">The id is not valid</p>',
                validators: {
                    notEmpty: {
                        message: '<p style="font-size:10px; width:200px;">The id is required and cannot be empty</p>'
                    },
                    stringLength: {
                        min: 4,
                        max: 12,
                        message: '<p style="font-size:10px; width:200px;">The ID must be more than 4 and less than 12 characters long</p>'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: '<p style="font-size:10px; width:200px;">The ID can only letters, numbers, underscores and unicode, special charater does not support.</p>'
                    },
					different: {
                        field: 'password',
                        message: '<p style="font-size:10px; width:200px;">The ID and password cannot be the same as each other</p>'
                    },
                    remote: {
                        url: 'ajax/register_chek_available.php',
 
                        message: '<p style="font-size:10px; width:200px;">The ID is not available</p>',
						 data: function(validator) {
 
                            return {
                                user_id: validator.getFieldElements('log_id').val()
							};
						}
					}
				}
            },
            email: {
                validators: {
                     notEmpty: {
                        message: '<p style="font-size:10px; width:200px;">The email is required and cannot be empty</p>'
                    },
                    emailAddress: {
                        message: '<p style="font-size:10px; width:200px;">The input is not a valid email address. If not, registration will be cancelled.</p>'
                    },
                    remote: {
                        url: 'ajax/register_chek_available.php',
                        message: '<p style="font-size:10px; width:200px;">The Email is not available</p>',
                         data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val()
                            };
                        }
                    }
                }
            },

            password: {
                validators: {
                    notEmpty: {
                        message: '<p style="font-size:10px; width:200px;">The password is required and cannot be empty</p>'
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: '<p style="font-size:10px; width:200px;">The password must be more than 4 and less than 30 characters long</p>'
                    },
                }
            },
            confirm_password: {
                validators: {
                    notEmpty: {
                        message: '<p style="font-size:10px; width:200px;">The confirm password is required and cannot be empty</p>'
                    },
                    identical: {
                        field: 'password',
                        message: '<p style="font-size:10px; width:200px;">The password and its confirm are not the same</p>'
                    },
                    different: {
                        field: 'log_id',
                        message: '<p style="font-size:10px; width:200px;">The password cannot be the same as ID</p>'
                    }
                }
            },
 
          company_name:{
				validators: {
 
                    notEmpty: {
                        message: '<p style="font-size:10px; width:200px;">This feild is required and cannot be empty</p>'
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: '<p style="font-size:10px; width:200px;">Company name please enter full name</p>'
                    }
                }
            },
		 country: {
                validators: {
                    notEmpty: {
                        message: '<p style="font-size:10px; width:200px;">The country is required</p>'
                    }
                }
            }
    	}
    });

    // Enable the password/confirm password validators if the password is not empty
   $('#btn-inquiry-submit').click(function() {

 

    $('#btn-inquiry-submit').bootstrapValidator('validate');
 

    });
   $('#txtarea').on('change',function(){
    var txtarea=$('#txtarea').val();
    if(txtarea!=''){
 
        $('#get').val(txtarea);
        $('#getsignup').val(txtarea);
 
    }
   });
   $('#subjectInput').on('change', function(){
    var subjectInput=$('#subjectInput').val();
    $('#subjectInputpop').val(subjectInput);
    $('#subjectInputpopsignup').val(subjectInput);
   });
});
</script>

