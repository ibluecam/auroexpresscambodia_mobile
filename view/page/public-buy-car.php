<?php 
	$get_province=$_CLASS->province;
	//$get_carinfo=$_CLASS->car_info;
	$get_car_color=$_CLASS->car_color;
	$get_make=$_CLASS->car_make;
	$get_car_year=$_CLASS->car_year;
	$get_car_model=$_CLASS->car_model;
?>

<script>
 function isNumberKey(evt){

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
       
       return false;
    return true;
  }
</script>
<div id="wrapper-buycar">
	<h1>Want to buy car?</h1>
	<div id="form-container-left">
	  <h2>Your Contact</h2>
	  <form method="post">
	  <table>
		<tr>
			<td><font class='font-color'>Full Name:</font></td>
			<td><input type='text' name='fullname' class='text-box'/></td>
		</tr>
		<tr>
		   <td><font class='font-color'>Sex:</font></td>
		   <td>
		       <input type='radio' name='gender' value='Male'/><font class='font-color' >Male</font>
			    <input type='radio' name='gender' value='Female'/><font class='font-color' >Female</font>
		   </td>
		</tr>
		<tr>
		   <td><font class='font-color' >Tel:</font></td>
		   <td>
			  <input type='text' name='tel' class='text-box' onkeypress="return isNumberKey(event)"/>	
		   </td>
		</tr>
		<tr>
		   <td><font class='font-color' >Email:</font></td>
		   <td>
			  <input type="email" name='email' class='text-box'/>	
		   </td>
		</tr>
		<tr>
		   <td><font class='font-color' >City / Province:</font></td>
		   <td>
			  <select name='province' class='drop-down'>
			    <option value=''></option>
			    <?php 
					foreach($get_province as $pro){ ?>
						<option><?php echo $pro['province_name'];?></option>
				<?php } ?>
				
			  </select>
		   </td>
		</tr>
	  </table>
	   <h2 class='type-car-youneed'>What type of car do you need?</h2>
	   <table>
			<tr>
				<td><font class='font-color-youneed'>Make:</font></td>
				<td>
					<select name='make' class='drop-down'>
						<option value=''>-select-</option>
						<?php foreach($get_make as $car_make){ ?>
						<?php
							if($car_make['make']!=""){
							  echo"<option>".$car_make['make']."</option>"; 
							}
						?>
						
						<?php }?>
					</select>
				</td>
			</tr>
			<tr>
				<td><font class='font-color-youneed'>Model:</font></td>
				<td>
					<select name='model' class='drop-down'>
						<option value=''>-select-</option>
						<?php foreach($get_car_model as $car_model){ ?>
						<?php
							if($car_model['model']!=""){
							  echo"<option>".$car_model['model']."</option>";
							}
						?>
						<?php }?>
					</select>
				</td>
			</tr>
			<tr>
				<td><font class='font-color-youneed'>Year:</font></td>
				<td>
					<select name='year' class='drop-down'>
						<option value=''>-select-</option>
						<?php foreach($get_car_year as $car_year){ ?>
						<?php
							if($car_year['year']!=""){
								echo"<option>".$car_year['year']."</option>";
							}
						?>
						<?php }?>
					</select>
				</td>
			</tr>
			
			<tr>
				<td><font class='font-color-youneed'>Color:</font></td>
				<td>
					<select name='color' class='drop-down'>
						<option value=''>-select-</option>
						<?php foreach($get_car_color as $car_color){ ?>
						     <?php
							    if($car_color['exterior_color']!=""){
								  echo"<option>".$car_color['exterior_color']."</option>";
								}
							 ?>
						<?php }?>
					</select>
				</td>
			</tr>
			<tr>
		   <td><font class='font-color'>Condition:</font></td>
		   <td>
		       <input type='radio' name='condition' value='used'/><font class='font-color' >Used</font>
			   <input type='radio' name='condition' value='new'/><font class='font-color' >New</font>
		   </td>
		   </tr>
		   <tr>
				<td><font class='font-color' >Price:</font></td>
				<td>
					<input type='text' name='price' class='text-box' onkeypress="return isNumberKey(event)"/>	
				</td>
			</tr>
			<tr>
			   <td><font class='font-color' >Description:</font></td>
			   <td>
					<textarea name='description'>
					</textarea>
			   </td>
			</tr>
			<tr>
			   <td></td>
			   <td>
			      <input type='submit' name='cmdsubmit' value='' class='linkimg'/>
				  <input type='reset' name='cmdreset' value='' class='linkimg' />
			   </td>
			</tr>
	   </table>
	   
	  </form>
	</div><!-- End of container left -->
	
	<div id='container-right'>
		<img src='<?php echo BASE_RELATIVE.'images/buy-car/poster.jpg'?>'/>
	</div><!-- End of container right -->
	<div style='clear:both;'></div>
</div>