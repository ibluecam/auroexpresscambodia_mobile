<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Handles log report (write) --------------------------------------------------
 * - this class is not meant to be initialized!
 */
class LogReport
{
    /*
     * Write log file ----------------------------------------------------------
     * @param $message - the message to be logged.
     * @return void.
     */
    public static function write($message)
    {
        $path = dirname(dirname(__FILE__)) . '/log/';
        
        // check if log dir exists.
        if( !is_dir($path) )
        {
            throw new Exception('Log directory ['.$path.'] does not exists. Please check your installation.');
        }
        
        $path .= date('Ymd') . '.txt';
        
        // if log already exists, append message to it, otherwise just create one.
        if(file_exists($path) )
        {
            $fo = @fopen($path, 'a');
        }
        else
        {
            $fo = @fopen($path, 'w');
        }
        
        if( !$fo )
        {
            throw new Exception('Unable to open log for writting. Please check the log directory write permission.');
        }
        
        $msg  = '################################################################################' . "\r\n";
        $msg .= '### ' . date('H:i:s') . "\r\n";
        $msg .= '################################################################################' . "\r\n";
        $msg .= wordwrap($message,75) . "\r\n";
        
        if( !@fwrite($fo, $msg) )
        {
            @fclose($fo);
            throw new Exception('Unable to write log. Please check the log directory write permission.');
        }
        
        @fclose($fo);
    }
    
    /*
     * Display a styled error box and exit and script been executed ------------
     * @param $message - the message to be displayed.
     * @return void.
     */
    public static function fatalError($message)
    {
        if( empty($message) )
        {
            return;
        }
        
        die('<p style="padding:10px;border:1px solid #CC0000;background:#F00;color:#FFF;font-family:Arial;font-weight:bold;display:block;">'.$message.'</p>');
    }
}
