<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminsettings 
{
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $setting_list;
    protected $page_list;
    
    /*
     * Constructor -------------------------------------------------------------
     */
    public function adminsettings($lang=array())
    {
        $this->lang = $lang;
        
        // handle update currency. [2013.07.15]
        if( isset($_POST['currencyBtn']) ){
            $this->handleUpdateCurrency();
        }
        
        // handle site options form.
        if( isset($_POST['siteOptionInput']) )
        {
            $this->updateSiteOptionsForm();
        }
        
        // handle meta options form.
        if( isset($_POST['metaInput']) )
        {
            $this->updateMetaOptionsForm();
        }
        
        // handle administrator credential form.
        if( isset($_POST['adminBtnInput']) )
        {
            $this->updateAdminForm();
        }
        
        // handle header options form.
        if( isset($_POST['headerInput']) )
        {
            $this->updateHeaderForm();
        }
        
        // handle reservation option form.
        if( isset($_POST['reservationbtn']) ){
            $this->handleReservationForm();
        }
        
        // handle update live support status form.
        if( isset($_POST['livechatBtnInput']) ){
            $this->handleLiveSupportForm();
        }
        
        // handle social status form.
        if( isset($_POST['socialbtn']) ){
            $this->handleSocialStatusForm();
        }
        
        // load settings.
        $this->loadSettings();
        
        // load page list.
        $this->loadPageList();
    }
    
    /**
     * Private method: handle update currency
     * @return void
     */
    private function handleUpdateCurrency(){
        $ci = trim($_POST['currencyInput']);
        
        if( empty($ci) ){
            return;
        }
        
        $ci = explode(',',$ci);
        
        if( count($ci) != 2 ){
            return;
        }
        
        $symbol = $ci[1];
        $code   = $ci[0];
        
        if( strlen($code) != 3 ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $symbol = @mysql_real_escape_string($symbol);
        $code = @mysql_real_escape_string($code);
        
        if( !@mysql_query("UPDATE `setting` SET `currency_symbol`='$symbol', `currency_code`='$code'") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            LogReport::write('Unable to update currency due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADMINSETTING_CURRENCY_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['ADMINSETTING_CURRENCY_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: handle social status form (update:2013.06.12)
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleSocialStatusForm(){
        $status = trim($_POST['socialbarInput']);
        
        ( $status == 'yes' ? $status = '1' : $status = '0' );
        
        // update status.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query("UPDATE `setting` SET `allow_social`='$status'") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to update setting table (allow_social status) due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADMINSETTING_SOCIAL_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $cnx->close();
            
            return;
        }
        
        $cnx->close();
        
        $this->form_message = $this->lang['ADMINSETTING_SOCIAL_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: handle live support form (update:2013.06.02)
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleLiveSupportForm(){
        $status = trim($_POST['liveChatInput']);
                
        ( $status == 'yes' ? $status = '1' : $status = '0' );
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query("UPDATE `setting` SET `live_chat`='$status'") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to update live chat status due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADMINSETTING_LIVE_CHAT_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            $cnx->close();
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['ADMINSETTING_LIVE_CHAT_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /**
     * Private method: handle reservation status form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleReservationForm(){
        ( isset($_POST['reservationInput']) ? $status = trim($_POST['reservationInput']) : $status = false );
        
        ( $status ? $status = '1' : $status = '0' );
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("UPDATE `setting` SET `allow_car_reservation`='$status'") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to update reservation status due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADMINSETTING_HEADER_RESERVATION_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            $cnx->close();
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['ADMINSETTING_HEADER_RESERVATION_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /*
     * Protected method: update header options form ----------------------------
     * @return void.
     */
    protected function updateHeaderForm()
    {
        $top = trim($_POST['topHeaderInput']);
        $menu = trim($_POST['menuHeaderInput']);
        $banner = trim($_POST['bannerHeaderInput']);
        $foot = trim($_POST['footHeaderInput']);
        
        // set as boolean.
        ( $top == 'yes' ? $top = '1' : $top = '0' );
        ( $menu == 'yes' ? $menu = '1' : $menu = '0' );
        ( $banner == 'yes' ? $banner = '1' : $banner = '0' );
        ( $foot == 'yes' ? $foot = '1' : $foot = '0' );
        
        // update database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        if( !@mysql_query("UPDATE `setting` SET `enable_top_header`='$top', `enable_menu_header`='$menu', `enable_banner_header`='$banner', `enable_foot_header`='$foot' LIMIT 1;") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to update setting table for header options form at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $db->close();
            
            $this->form_message = $this->lang['ADMINSETTING_HEADER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $db->close();
        
        $this->form_message = $this->lang['ADMINSETTING_HEADER_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /*
     * Protected method: update administrator form -----------------------------
     * @return void.
     */
    protected function updateAdminForm()
    {
        $email = trim($_POST['adminEmailInput']);
        $pass  = trim($_POST['adminPassInput']);
        
        // validate.
        require_once BASE_CLASS . 'class-utilities.php';
        
        $email = stripslashes($email);
        
        if( !Utilities::checkEmail($email) )
        {
            $this->form_message = $this->lang['ADMINSETTING_ADMIN_INVALID_EMAIL'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
                
        // set queries base on password state.
        if( empty($pass) )
        {
            $query = "UPDATE `setting` SET `admin_email`='$email' LIMIT 1;";
        }
        else
        {
            $pass = md5($pass);
            $query = "UPDATE `setting` SET `admin_email`='$email', `admin_pass`='$pass' LIMIT 1;";
        }
        
        // update database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        if( !@mysql_query($query) )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
            
            LogReport::write('Unable to update administrator credentials due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['ADMINSETTING_ADMIN_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $db->close();
        
        $this->form_message = $this->lang['ADMINSETTING_ADMIN_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /*
     * Protected method: update meta options form ------------------------------
     * @return void.
     */
    protected function updateMetaOptionsForm()
    {
        $copyrights = trim($_POST['copyrightsInput']);
        $canonical = trim($_POST['canonicInput']);
        $google_analytics = trim($_POST['googleInput']);
        
        $copyrights = stripslashes($copyrights);
        $canonical  = stripslashes($canonical);
        $google_analytics = stripslashes($google_analytics);
        
        // validate.
        if( empty($canonical) )
        {
            $canonical = '';
        }
        
        if( empty($copyrights) )
        {
            $copyrights = FRAMEWORK_NAME;
        }
        
        if( empty($google_analytics) )
        {
            $google_analytics = '<script></script>';
        }
        
        // validate and upload favicon if set.
        $favicon_url = '';
        
        if( $_FILES['faviconInput']['size'] > 0 )
        {
            $favicon_extension = explode('.', $_FILES['faviconInput']['name']);
            $favicon_extension = $favicon_extension[(count($favicon_extension)-1)];
            $favicon_extension = strtolower($favicon_extension);
            
            if( $favicon_extension != 'ico' )
            {
                $this->form_message = $this->lang['ADMINSETTING_INVALID_FAVICON_FORMAT'];
                $this->form_status = true;
                $this->form_style = 'alert-error';
                
                return;
            }
            
            // upload favicon.
            if( !@move_uploaded_file($_FILES['faviconInput']['tmp_name'], BASE_ROOT . 'favicon.ico') )
            {
                require_once BASE_CLASS . 'class-log.php';
                
                LogReport::write('Unable to upload favicon at ' . __FILE__ . ':' . __LINE__);
                
                $this->form_message = $this->lang['ADMINSETTING_FAVICON_UPLOAD_ERROR'];
                $this->form_status = true;
                $this->form_style = 'alert-error';
                
                return;
            }
            
            $favicon_url = BASE_RELATIVE . 'favicon.ico';
        }
        
        // update database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        $copyrights = mysql_real_escape_string($copyrights);
        $canonical  = mysql_real_escape_string($canonical);
        $google_analytics = mysql_real_escape_string($google_analytics);
        
        if( !@mysql_query("UPDATE `setting` SET `analytics`='$google_analytics', `meta_favicon`='$favicon_url', `meta_copyrights`='$copyrights', `meta_canonic`='$canonical' LIMIT 1;") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
                
            LogReport::write('Unable to update table setting at ' . __FILE__ . ':' . __LINE__ .'. ' . mysql_error());

            $this->form_message = $this->lang['ADMINSETTING_META_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';

            return;
        }
        
        $db->close();
        
        $this->form_message = $this->lang['ADMINSETTING_META_UPDATE_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /*
     * Protected method: handle site options form ------------------------------
     * @return void.
     */
    protected function updateSiteOptionsForm()
    {
        $framework_name = trim($_POST['fwNameInput']);
        $default_language = trim($_POST['languageInput']);
        $default_home_slug = trim($_POST['defaultHomeInput']);
        
        // get ISO code for language.
        require_once BASE_CLASS . 'class-language.php';
        $lc = new LanguageFormat();
        
        $default_language = ucfirst($default_language);
        $language_iso = $lc->getLanguageISOByName($default_language);
        
        // validate.
        if( empty($framework_name) )
        {
            $this->form_message = $this->lang['ADMINSETTING_FRAMEWORK_NAME_INVALID'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        if( empty($default_language) || !file_exists(BASE_ROOT . 'language/' . $language_iso . '/global.php') )
        {
            $default_language = 'english';
            $language_iso = 'en';
        }
        
        if( empty($default_home_slug) )
        {
            $default_home_slug = 'home';
        }
        
        // update database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        $framework_name = mysql_real_escape_string($framework_name);
        
        if( !@mysql_query("UPDATE `setting` SET `language`='$default_language', `language_iso`='$language_iso', `fwname`='$framework_name', `default_home_slug`='$default_home_slug' LIMIT 1;") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
            
            LogReport::write('Unable to update setting table due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['ADMINSETTING_SITEOPTION_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        $db->close();
        
        $this->form_message = $this->lang['ADMINSETTING_SITEOPTIONS_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /*
     * Protect method: load page list ------------------------------------------
     * @return void.
     */
    protected function loadPageList()
    {
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        // allow only public editable pages to be selected.
        if( !$sql = @mysql_query("SELECT `page_title`,`page_slug`,`editable`,`page_group` FROM `page` WHERE `editable`='1' AND `page_group`='public' ORDER BY `page_title`") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load page list due an query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            LogReport::fatalError($this->lang['ADMINSETTING_LOADPAGELIST_QUERY_ERROR']);
        }
        
        $this->page_list = array();
        
        if( @mysql_num_rows($sql) > 0 )
        {
            while( $r = @mysql_fetch_assoc($sql) )
            {
                $obj = array(
                    'page_title' => $r['page_title'],
                    'page_slug'  => $r['page_slug']
                );
                
                array_push($this->page_list, $obj);
            }
            
            @mysql_free_result($sql);
        }
        
        $db->close();
    }
    
    /*
     * Protected method: load settings -----------------------------------------
     * @return void.
     */
    protected function loadSettings()
    {
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `setting` LIMIT 1;") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
            
            LogReport::write('Unable to query setting table at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            LogReport::fatalError($this->lang['ADMINSETTING_LOADLIST_QUERY_ERROR']);
        }
        
        if( @mysql_num_rows($sql) != 1 )
        {
            $db->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            $db->close();
            
            LogReport::write('Setting database table does not contain configuration data due invalid installation. Error thrown at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            LogReport::fatalError($this->lang['ADMINSETTING_LOADLIST_NOSETTING_ERROR']);
        }
        
        $r = @mysql_fetch_assoc($sql);
        
        @mysql_free_result($sql);
        
        $db->close();
        
        $this->setting_list = array(
            'language'              => stripslashes($r['language']),
            'language_iso'          => stripslashes($r['language_iso']),
            'fwname'                => stripslashes($r['fwname']),
            'analytics'             => stripslashes($r['analytics']),
            'meta_favicon'          => stripslashes($r['meta_favicon']),
            'meta_copyrights'       => stripslashes($r['meta_copyrights']),
            'meta_canonic'          => stripslashes($r['meta_canonic']),
            'admin_email'           => stripslashes($r['admin_email']),
            'admin_pass'            => $r['admin_pass'],
            'enable_top_header'     => (bool)$r['enable_top_header'],
            'enable_menu_header'    => (bool)$r['enable_menu_header'],
            'enable_banner_header'  => (bool)$r['enable_banner_header'],
            'enable_foot_header'    => (bool)$r['enable_foot_header'],
            'default_home_slug'     => stripslashes($r['default_home_slug']),
            'allow_car_reservation' => (bool)$r['allow_car_reservation'],
            'live_chat'             => (bool)$r['live_chat'],
            'allow_social'          => (bool)$r['allow_social']
        );
        
        $_SESSION['CURRENCY_CODE'] = stripslashes($r['currency_code']);
        $_SESSION['CURRENCY_SYMBOL'] = stripslashes($r['currency_symbol']);
    }
    
    /*
     * Public method: get page list --------------------------------------------
     * @return array.
     */
    public function getPageList()
    {
        return $this->page_list;
    }

    /*
     * Public method: get settings list ----------------------------------------
     * @return array.
     */
    public function getSettingList()
    {
        return $this->setting_list;
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    
    
    /**
     * Get currency list
     * @return array
     */
    public function getCurrencyOptionList(){
        $this->currency_option_list = array(
            'Albania Lek-ALL' => 'Lek',
            'Afghanistan Afghani-AFN' => '؋',
            'Argentina Peso-ARS' => '$',
            'Aruba Guilder-AWG' => 'ƒ',
            'Australia Dollar-AUD' => '$',
            'Azerbaijan New Manat-AZN' => 'ман',
            'Bahamas Dollar-BSD' => '$',
            'Barbados Dollar-BBD' => '$',
            'Belarus Ruble-BYR' => 'p.',
            'Belize Dollar-BZD' => 'BZ$',
            'Bermuda Dollar-BMD' => '$',
            'Bolivia Boliviano-BOB' => '$b',
            'Bosnia and Herzegovina Convertible Marka-BAM' => 'KM',
            'Botswana Pula-BWP' => 'P', 
            'Bulgaria Lev-BGN' => 'лв',
            'Brazil Real-BRL' => 'R$',
            'Brunei Darussalam Dollar-BND' => '$',
            'Cambodia Riel-KHR' => '៛',
            'Canada Dollar-CAD' => '$',
            'Cayman Islands Dollar-KYD' => '$',
            'Chile Peso-CLP' => '$',
            'China Yuan Renminbi-CNY' => '¥',
            'Colombia Peso-COP' => '$',
            'Costa Rica Colon-CRC' => '₡',
            'Croatia Kuna-HRK' => 'kn',
            'Cuba Peso-CUP' => '₱',
            'Czech Republic Koruna-CZK' => 'Kč',
            'Denmark Krone-DKK' => 'kr',
            'Dominican Republic Peso-DOP' => 'RD$',
            'East Caribbean Dollar-XCD' => '$',
            'Egypt Pound-EGP' => '£',
            'El Salvador Colon-SVC' => '$',
            'Estonia Kroon-EEK' => 'kr',
            'Euro Member Countries-EUR' => '€',
            'Falkland Islands (Malvinas) Pound-FKP' => '£',
            'Fiji Dollar-FJD' => '$',
            'Ghana Cedis-GHC' => '¢',
            'Gibraltar Pound-GIP' => '£',
            'Guatemala Quetzal-GTQ' => 'Q',
            'Guernsey Pound-GGP' => '£',
            'Guyana Dollar-GYD' => '$',
            'Honduras Lempira-HNL' => 'L',
            'Hong Kong Dollar-HKD' => '$',
            'Hungary Forint-HUF' => 'Ft',
            'Iceland Krona-ISK' => 'kr',
            'India Rupee-INR' => 'Rs',
            'Indonesia Rupiah-IDR' => 'Rp',
            'Iran Rial-IRR' => '﷼',
            'Isle of Man Pound-IMP' => '£',
            'Israel Shekel-ILS' => '₪',
            'Jamaica Dollar-JMD' => 'J$',
            'Japan Yen-JPY' => '¥',
            'Jersey Pound-JEP' => '£',
            'Kazakhstan Tenge-KZT' => 'лв',
            'Korea (North) Won-KPW' => '₩',
            'Korea (South) Won-KRW' => '₩',
            'Kyrgyzstan Som-KGS' => 'лв',
            'Laos Kip-LAK' => '₭',
            'Latvia Lat-LVL' => 'Ls',
            'Lebanon Pound-LBP' => '£',
            'Liberia Dollar-LRD' => '$',
            'Lithuania Litas-LTL' => 'Lt',
            'Macedonia Denar-MKD' => 'ден',
            'Malaysia Ringgit-MYR' => 'RM',
            'Mauritius Rupee-MUR' => '₨',
            'Mexico Peso-MXN' => '$',
            'Mongolia Tughrik-MNT' => '₮',
            'Mozambique Metical-MZN' => 'MT',
            'Namibia Dollar-NAD' => '$',
            'Nepal Rupee-NPR' => '₨',
            'Netherlands Antilles Guilder-ANG' => 'ƒ',
            'New Zealand Dollar-NZD' => '$',
            'Nicaragua Cordoba-NIO' => 'C$',
            'Nigeria Naira-NGN' => '₦',
            'Korea (North) Won-KPW' => '₩',
            'Norway Krone-NOK' => 'kr',
            'Oman Rial-OMR' => '﷼',
            'Pakistan Rupee-PKR' => '₨',
            'Panama Balboa-PAB' => 'B/.',
            'Paraguay Guarani-PYG' => 'Gs',
            'Peru Nuevo Sol-PEN' => 'S/.',
            'Philippines Peso-PHP' => '₱',
            'Poland Zloty-PLN' => 'zł',
            'Qatar Riyal-QAR' => '﷼',
            'Romania New Leu-RON' => 'lei',
            'Russia Ruble-RUB' => 'руб',
            'Saint Helena Pound-SHP' => '£',
            'Saudi Arabia Riyal-SAR' => '﷼',
            'Serbia Dinar-RSD' => 'Дин.',
            'Seychelles Rupee-SCR' => '₨',
            'Singapore Dollar-SGD' => '$',
            'Solomon Islands Dollar-SBD' => '$',
            'Somalia Shilling-SOS' => 'S',
            'South Africa Rand-ZAR' => 'R',
            'Korea (South) Won-KRW' => '₩',
            'Sri Lanka Rupee-LKR' => '₨',
            'Sweden Krona-SEK' => 'kr',
            'Switzerland Franc-CHF' => 'CHF',
            'Suriname Dollar-SRD' => '$',
            'Syria Pound-SYP' => '£',
            'Taiwan New Dollar-TWD' => 'NT$',
            'Thailand Baht-THB' => '฿',
            'Trinidad and Tobago Dollar-TTD' => 'TT$',
            'Turkey Lira-TRY' => 'TR',
            'Turkey Lira-TRL' => '₤',
            'Tuvalu Dollar-TVD' => '$',
            'Ukraine Hryvna-UAH' => '₴',
            'United Kingdom Pound-GBP' => '£',
            'United States Dollar-USD' => '$',	 
            'Uruguay Peso-UYU' => '$U',	 
            'Uzbekistan Som-UZS' => 'лв',	 
            'Venezuela Bolivar-VEF' => 'Bs',
            'Viet Nam Dong-VND' => '₫',
            'Yemen Rial-YER' => '﷼',
            'Zimbabwe Dollar-ZWD' => 'Z$'
        );
        
        return $this->currency_option_list;
    }
}
