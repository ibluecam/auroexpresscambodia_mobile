<?php
if(!isset($_SESSION)) @session_start();
$log_group=$_SESSION['log_group'];
if($log_group!='admin'){
    return;
}
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();

if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
if(isset($_GET['offset'])) $offset=htmlspecialchars($_GET['offset']); else $offset=0;
$where="WHERE product.location='$country' AND product.is_deleted='0'";
$sql="SELECT DISTINCT product.city, COUNT(product.id) as product_number from product $where GROUP BY product.city ORDER BY product.city";

if( !$result = @mysql_query($sql) ){
    require_once BASE_CLASS . 'class-log.php';
    $cnx->close();
    LogReport::write('Unable to load register type list due a query error at ' . __FILE__ . ':' . __LINE__);
    return;
}

$i=$offset;
echo "<table class='city_table' id='city_table'>
        <tr>
            <th></th>
            <th>N<sup>o</sup></th>
            <th>City Name</th>
            <th>Number of Products</th>
            <th></th>
        </tr>";
if(mysql_num_rows($result)>0){
    while( $r = @mysql_fetch_assoc($result) ){
        //array_push($this->city_list,$r);
        $i++;                 
        echo 
        "<tr>
            <td>
                <input type='checkbox' class='city_check' value='{$r['city']}' name='cityCheck[$i]' id='city_check_$i'>
                <input type='hidden' value='{$r['city']}' name='old_city[$i]' id='old_city_$i'>
            </td>
            <td>$i</td>
            <td id='city_$i'>{$r['city']}</td>
            <td>{$r['product_number']}</td>
            <td class='button_td'>
                <a edit_id='$i' class='edit_button' href='#'><img src='images/admin/write.png'></a>
                <a delete_id='$i' class='delete_button' href='#'><img src='images/admin/x.png'></a>
            </td>
        </tr>";
    }
}else{
    echo "<tr><td style='color:#A00000;text-align:center;' colspan='5'>No cities found</td></tr>";
}
echo "<table>";
@mysql_free_result($sql);
$cnx->close();
?>