<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');

class adminModelManager
{
    protected $lang;
    protected $group = 'admin';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $members;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;

    var $member_type1="Seller";
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminmodelmanager($lang=array()){
        $this->lang = $lang;
        //if user submit press button save changes
        if(isset($_POST['save_changes'])){
            $this->handleSaveChanges();
        }
        //if user submit press button delete selected
        if(isset($_POST['delete_selected'])){
            $this->handleDelete();
        }
        if(isset($_POST['submitSave'])){
            $this->handleSaveNew();
        }
        
        //$this->loadCityList();
        
    }
    private function handleSaveNew(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        if(isset($_POST['newModelInput'])) $newModelInput=$_POST['newModelInput']; else $newModelInput='';
        if(isset($_POST['newModelInput'])) $newModelInput=$_POST['newModelInput']; else $newModelInput='';
        if(isset($_POST['makeSelect'])) $makeSelect=$_POST['makeSelect']; else $makeSelect='';
        
        $sql= "INSERT INTO `product_model` (`model`, `maker`) VALUES('$newModelInput', '$makeSelect');";

        if(empty($newModelInput)){
            
            $cnx->close();
            $this->form_message = "Unable to save empty model! Please insert it in the textbox.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            
            LogReport::write('Unable to add model due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Models were added successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }

    private function handleSaveChanges(){
        if(isset($_POST['makeSelect'])) $makeSelect=$_POST['makeSelect']; else $makeSelect='';
        if(isset($_POST['old_model'])) $old_models=$_POST['old_model']; else $old_models=array();
        if(isset($_POST['new_model'])) $new_models=$_POST['new_model']; else $new_models=array();
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        //Delete existing model that match new models first to prevent duplicate
        //$this->deletePreviousModel($new_models, $product_type);

        if(count($new_models)<=0){
            $this->form_message =  "Models were changed successfully!";
            $this->form_status = true;
            $this->form_style = 'alert-success';
            return;
        }
        $this->changeModelInProduct();
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open(); 
        $in_models=array();
        $sql= "UPDATE `product_model` SET `model` = CASE `id` ";
        foreach($new_models as $key=>$value){
            array_push($in_models, $key);
            $sql.="WHEN '{$key}' THEN '{$value}' ";
        }
        //Sql of cities that were changed
        $in_old_models = implode("', '", $in_models);
        $sql .= "END ";
        //Change only cities that were edit
        $sql .= "WHERE id IN ('$in_old_models')";
        //echo $sql;
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            echo mysql_error();
            $cnx->close();
            
            LogReport::write('Unable to change model due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = "Unable to save changes! Please make sure that you enter correct data.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Models were changed successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    private function changeModelInProduct(){
        if(isset($_POST['makeSelect'])) $makeSelect=$_POST['makeSelect']; else $makeSelect='';
        if(isset($_POST['old_model'])) $old_models=$_POST['old_model']; else $old_models=array('');
        if(isset($_POST['new_model'])) $new_models=$_POST['new_model']; else $new_models=array('');
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        //Delete existing make that match new makes first to prevent duplicate
        //$this->deletePreviousMake($new_makes, $product_type);
        $makeText=$this->getMakeText($makeSelect, $product_type);
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $in_models=array();
        $sql= "UPDATE `product` SET `model` = CASE `model` ";
        foreach($new_models as $key=>$value){
            array_push($in_models, $old_models[$key]);
            $sql.="WHEN '{$old_models[$key]}' THEN '{$value}' ";
        }
        //Sql of cities that were changed
        $in_old_models = implode("', '", $in_models);
        $sql .= "END ";
        //Change only cities that were edit
        $sql .= "WHERE model IN ('$in_old_models') AND make='$makeText' AND product_type='$product_type'";
        //echo $sql;
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            echo mysql_error();
            $cnx->close();
            
            LogReport::write('Unable to change model due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = "Unable to save changes! Please make sure that you enter correct data.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        $cnx->close();
        
        $this->form_message =  "Models were changed successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    private function getMakeText($makeId, $product_type){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $sql="SELECT maker FROM product_maker WHERE id='$makeId' AND product_type='$product_type'";
        
        $result=@mysql_query($sql);
        if($result){
            while($row=mysql_fetch_assoc($result)){
                return $row['maker'];
            }
        }
        $cnx->close();
        return '';
    }
    private function handleDelete(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        //code here
        if(isset($_POST['modelCheck'])) $modelChecks=$_POST['modelCheck']; else $modelChecks=array();
        if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
        if(count($modelChecks)<=0){
            $this->form_message = "Please check the cities to be deleted!";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        $checked_models = implode("', '", array_keys($modelChecks));
        $sql="DELETE FROM product_model WHERE id IN ('$checked_models')";
        //execute delete
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to delete model in this category due to a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = 'Unable to delete products with these cities';
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        $cnx->close();
        $this->form_message =  "Models with the selected category were deleted successfully!";
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
