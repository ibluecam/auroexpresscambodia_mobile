<?php
$_LANG = array(
    'PRODUCT_ADD_NEW'=>'Add new',
	'PRODUCT_AT'=>'AT',
	'PRODUCT_MT'=>'MT',
    'PRODUCT_HEADER' => 'Add New Car',
	'PRODUCT_EDIT_HEADER' => 'Edit Car',
    'PRODUCT_SAVE_BUTTON' => 'Save vehicle',
    'PRODUCT_STEP1_LABEL' => 'Add a new vehicle',
    'PRODUCT_STEP2_LABEL' => 'Select vehicle features',
    'PRODUCT_STEP3_LABEL' => 'Upload vehicle picture',
    'PRODUCT_STEP4_LABEL' => 'Enter the vehicle details',
    'PRODUCT_MAKER_LABEL' => 'Make',
    'PRODUCT_MAKER_PLACEHOLDER' => 'Toyota',
    'PRODUCT_MAKER_QUERY_ERROR' => 'Unable to load maker list due an internal error. See log for details.',
    'PRODUCT_OR_SELECT_LABEL' => 'or select',
    'PRODUCT_MODEL_LABEL' => 'Model',
    'PRODUCT_MODEL_PLACEHOLDER' => 'Camry',
    'PRODUCT_BODY_TYPE_LABEL' => 'Body type',
    'PRODUCT_BODY_TYPE_PLACEHOLDER' => 'Hatchback',
	'PRODUCT_COUNTRY_LIST_LABEL' => 'Location',
    'PRODUCT_COUNTRY_LIST_PLACEHOLDER' => 'Korea',
	'PRODUCT_PROVINCE_LIST_LABEL' => 'Province / City',
    'PRODUCT_PRICE_LABEL' => 'Price',
    'PRODUCT_PRICE_PLACEHOLDER' => '32000',
    'PRODUCT_MILES_LABEL' => 'Mileage',
	'PRODUCT_MILES_PLACEHOLDER' => '2000',
    'PRODUCT_DOORS_LABEL' => 'Door(s)',
    'PRODUCT_DOOR_SELECT_LABEL' => 'door(s)',
    'PRODUCT_YES_LABEL' => 'Yes, mark as featured',
    'PRODUCT_NO_LABEL' => 'No',
    'PRODUCT_FEATURED_LABEL' => 'Mark as featured',
    'PRODUCT_FEATURE_ICON_HEADER' => 'Select car features',
    'PRODUCT_STEP1_HEADER' => 'Vehicle information',
    'PRODUCT_NEXT_BUTTON_LABEL' => 'Next step',
    'PRODUCT_PREVIOUS_BUTTON_LABEL' => 'Previous step',
    'PRODUCT_ENGINE_SIZE_LABEL' => 'Engine size',
    'PRODUCT_ENGINE_SIZE_PLACEHOLDER' => '2.4',
    'PRODUCT_TRIM_LABEL' => 'Trim',
    'PRODUCT_TRIM_PLACEHOLDER' => 'JTDM',
    'PRODUCT_TYPE_LABEL' => 'Type',
    'PRODUCT_TYPE_PLACEHOLDER' => '3 Doors Hatch',
    'PRODUCT_GEAR_LABEL' => 'Gear',
    'PRODUCT_GEAR_PLACEHOLDER' => '6-Speed',
    'PRODUCT_TRANSMISSION_LABEL' => 'Transmission',
    'PRODUCT_TRANSMISSION_PLACEHOLDER' => 'Automatic',
    'PRODUCT_FUEL_LABEL' => 'Fuel Type',
    'PRODUCT_FUEL_PLACEHOLDER' => 'Gasoline',
    'PRODUCT_BODY_LABEL' => 'Body Color',
    'PRODUCT_BODY_PLACEHOLDER' => 'Black metallic',
    'PRODUCT_OWNER_LABEL' => 'Nr. Previous Owner(s) [NUMBERS ONLY!]',
    'PRODUCT_OWNER_PLACEHOLDER' => '1',
    'PRODUCT_LAST_SERVICE_LABEL' => 'Last service',
    'PRODUCT_LAST_SERVICE_PLACEHOLDER' => '45.200 km',
    'PRODUCT_MOT_LABEL' => 'MOT',
    'PRODUCT_MOT_PLACEHOLDER' => '2013.04.10',
    'PRODUCT_TAX_LABEL' => 'Tax band',
    'PRODUCT_TAX_PLACEHOLDER' => 'IE. I - &pound;220 PA',
    'PRODUCT_TOP_SPEED_LABEL' => 'Top speed',
    'PRODUCT_TOP_SPEED_PLACEHOLDER' => '280 km/h',
    'PRODUCT_TORQUE_LABEL' => 'Engine torque (RPM)',
    'PRODUCT_TORQUE_PLACEHOLDER' => 'IE. 2000',
    'PRODUCT_KW_LABEL' => 'Engine power (KW)',
    'PRODUCT_KW_PLACEHOLDER' => '147',
    'PRODUCT_HTML_HEADER' => 'More details',
    'PRODUCT_UPLOAD_MEDIA_HEADER' => 'Upload images',
    'PRODUCT_VIDEO_SELECT' => 'Video',
    'PRODUCT_IMAGE_SELECT' => 'Picture',
    'PRODUCT_INTERIOR_SELECT' => 'Interior',
    'PRODUCT_EXTERIOR_SELECT' => 'Exterior',
    'PRODUCT_OPTION_LABEL' => 'Select upload type',
    'PRODUCT_YOUTUBE_URL_LABEL' => 'Video URL',
    'PRODUCT_YOUTUBE_URL_PLACEHOLDER' => 'https://www.youtube.com/watch?v=XEfQNCRtolc',
    'PRODUCT_UPLOAD_LABEL' => 'Upload picture',
    'PRODUCT_SELECT_OPTION_SELECT' => 'Select a option',
    'PRODUCT_UPLOAD_BUTTON' => 'Upload now',
    'PRODUCT_SAVE_BUTTON' => 'Save now',
    'PRODUCT_YEAR_LABEL' => 'Year',
    'PRODUCT_MAX_UPLOAD_FILESIZE_LABEL' => 'Maximum file size allowed',
    'PRODUCT_INVALID_UPLOAD_REQUEST_LABEL' => 'You must be authenticated to request this page.',
    'PRODUCT_INVALID_REQUEST_ID_LABEL' => 'Please try to reload the page. If the problem persist, contact the support.',
    'PRODUCT_INVALID_FILE_EXTENSION' => 'Please upload only valid image files: GIF, PNG or JPG',
	'PRODUCT_INVALID_COUNTRY_ERROR' => 'Please select country that your car is located.',
    'PRODUCT_GOBACK_LABEL' => 'Click here to go back.',
    'PRODUCT_INVALID_IMAGE_SIZE_LABEL' => 'The image you tried to upload is too large or invalid.',
    'PRODUCT_UPLOAD_FAILURE_LABEL' => 'Image could not be uploaded due an internal error. See log for more details.',
    'PRODUCT_UPLOAD_QUERY_ERROR' => 'Unable to upload image due an internal error. See log for more details.',
    'PRODUCT_INVALID_MAKER_ERROR' => 'Enter a maker or select a previously entered maker for the VEHICLE MAKER field on step 1.',
    'PRODUCT_INVALID_MODEL_ERROR' => 'Enter a model or select a previously entered model for the MODEL field on step 1.',
    'PRODUCT_INVALID_BODY_TYPE_ERROR' => 'Enter a body type or select one of the registered body types from the list at the field BODY TYPE on step 1.',
    'PRODUCT_UNDEFINED_LABEL' => 'Undefined',
    'PRODUCT_SAVE_QUERY_ERROR' => 'Unable to save new vehicle entry due an internal error. See log for details.',
    'PRODUCT_SAVE_QUERY_SUCCESS' => 'Car saved successfully!',
    'PRODUCT_NOBODYTYPE_LABEL' => 'You did not create any body type for selection.',
	'PRODUCT_NOCOUNTRY_LABEL' => 'Undefined Country',
    'PRODUCT_ENERGYCLASS_LABEL' => 'Energy Class',
	
	'PRODUCT_CLASS_LABEL' => 'Class',
	'PRODUCT_CLASS_PLACEHOLDER' => 'Single Cab 4 WD Luxury',
	
	'PRODUCT_DRIVE_LABEL' => 'Drive Type',
	
	'PRODUCT_ENGINE_VOLUME_LABEL' => 'Engine Volume (CC)',
	'PRODUCT_ENGINE_VOLUME_PLACEHOLDER' => '2497',
	
	'PRODUCT_ENGINE_POWER_LABEL' => 'Engine Power (HP)',
	'PRODUCT_ENGINE_POWER_PLACEHOLDER' => '133',
	
	'PRODUCT_MANU_DATE_LABEL' => 'First Registration Date',
	'PRODUCT_MANU_DATE_PLACEHOLDER' => '2014-12',
	
	'PRODUCT_NUMBER_CYLINDER_LABEL' => 'Number of Cylinder',
	'PRODUCT_NUMBER_CYLINDER_PLACEHOLDER' => '4',
	'PRODUCT_NUMBER_PASSENGER_LABEL' => 'Number of Passenger',
	'PRODUCT_NUMBER_PASSENGER_PLACEHOLDER' => '5',
	'PRODUCT_PAYMENT_TERM_LABEL' => 'Payment Terms',
	'PRODUCT_STEERING_LABEL' => 'Steering',
	'PRODUCT_VEHICLE_TYPE_LABEL' => 'Vehicle Type',
	'PRODUCT_VEHICLE_TYPE_PLACEHOLDER' => 'Pick Up',
	'PRODUCT_CONDITION_LABEL' => 'Condition',
	
    'PRODUCT_CONDITION_CHASSISNO'=>'CHASSIS NO.',     
	'PRODUCT_CONDITION_ENGINESIZE'=>'ENGINE SIZE.',
	'PRODUCT_CONDITION_BODYTYPE'=>'CAR TYPE', 
	'PRODUCT_CONDITION_CYLINDERCAPACITY'=>'CYLINDER CAPACITY',
	'PRODUCT_CONDITION_SEATINGCAPACITY'=>'SEATING CAPACITY',
	'PRODUCT_CONDITION_COLOR'=>'COLOR',
	'PRODUCT_EXTERIOR_OPTIONS'=>'EXTERIOR OPTIONS',
	'PRODUCT_INTERIOR_OPTIONS'=>'INTERIOR OPTIONS',
	'PRODUCT_Description'=>'Description',
	 
	'PRODUCT_CONDITION_NEW' => 'New',
	'PRODUCT_CONDITION_USED' => 'Used',
	'PRODUCT_CONDITION_SALVAGE' => 'Salvage',
	'PRODUCT_CONDITION_REMADE' => 'Remade',
	
	'PRODUCT_VIN_LABEL' => 'VIN',
	'PRODUCT_VIN_PLACEHOLDER' => 'A123456789',
	'PRODUCT_DRIVE_AVAIL_LABEL' => 'Drive Availability',
	'PRODUCT_DRIVE_AVAIL_PLACEHOLDER' => '...',
	
	'PRODUCT_STEERING_LHD'=>'Left-hand Drive',
	'PRODUCT_STEERING_RHD'=>'Right-hand Drive',
	
	'PRODUCT_PRICE_PLACEHOLDER'=>'32000',
	'PRODUCT_IMPOSSIBLE_DRIVE'=>'Impossible to Drive',
	'PRODUCT_POSSIBLE_DRIVE'=>'Possible to Drive',
	
	'PRODUCT_CATEGORY_LABEL'=>'Category',
	'PRODUCT_SUB_CATEGORY_LABEL'=>'',
	'PRODUCT_SUB_CATEGORY_GROUP_LABEL'=>'',
	
	'PRODUCT_PART_NAME_LABEL'=>'Part Name',
	'PRODUCT_PART_NAME_PLACEHOLDER'=>'Part Name',
	
	'PRODUCT_UNIT_WEIGHT_LABEL'=>'Unit Weight',
	'PRODUCT_UNIT_WEIGHT_PLACEHOLDER'=>'2000',
	
	'PRODUCT_PART_NUMBER_LABEL'=>'Part Number',
	'PRODUCT_PART_NUMBER_PLACEHOLDER'=>'5',
	
	'PRODUCT_NUMBER_CARGO_LABEL'=>'Number of Cargo',
	'PRODUCT_NUMBER_CARGO_PLACEHOLDER'=>'2',
	
	'PRODUCT_CARGO_BOX_SIZE_LABEL'=>'Cargo Box Size',
	'PRODUCT_CARGO_BOX_SIZE_PLACEHOLDER'=>'2.4',
	
	'PRODUCT_LENGTH_LABEL'=>'Length',
	'PRODUCT_LENGTH_PLACEHOLDER'=>'200 mm',
	
	'PRODUCT_HEIGHT_LABEL'=>'Height',
	'PRODUCT_HEIGHT_PLACEHOLDER'=>'200 mm',
	
	'PRODUCT_AXLE_QTY_LABEL'=>'Axle Quantity',
	'PRODUCT_AXLE_QTY_PLACEHOLDER'=>'5',
	
	'PRODUCT_WIDTH_LABEL'=>'Width',
	'PRODUCT_WIDTH_PLACEHOLDER'=>'200 mm',
	
	'PRODUCT_TOTAL_WEIGHT_LABEL'=>'Total Weight',
	'PRODUCT_TOTAL_WEIGHT_PLACEHOLDER'=>'3 tons',
	
	'PRODUCT_LOADING_WEIGHT_LABEL'=>'Loading Weight',
	'PRODUCT_LOADING_WEIGHT_PLACEHOLDER'=>'3 tons',
	
	'PRODUCT_MANUFACTURER_LABEL'=>'Manufacturer',
	'PRODUCT_MANUFACTURER_PLACEHOLDER'=>'Manufacturer',
	
	'PRODUCT_MADE_IN_LABEL'=>'Made In',
	'PRODUCT_MADE_IN_PLACEHOLDER'=>'Made In',
	
	'PRODUCT_FITTING_MAKE_LABEL'=>'Fitting Make',
	'PRODUCT_FITTING_MAKE_PLACEHOLDER'=>'Fitting Make',
	
	'PRODUCT_COLOR_LABEL'=>'Color',
	'PRODUCT_COLOR_PLACEHOLDER'=>'Red',
	
	'PRODUCT_OTHER_OPTIONS_LABEL'=>'Option',
	'PRODUCT_SAFETY_DEVICE_OPTIONS_LABEL'=>'Safety Devices',
	'PRODUCT_EX_OPTIONS_LABEL'=>'Exterior Options',
	'PRODUCT_IN_OPTIONS_LABEL'=>'Interior Options',
	
	'PRODUCT_SELLER_COMMENT_HEADER'=>"Seller's comment",
	
	'PRODUCT_CHASSIS_NO_LABEL'=>'Chassis No',
	'PRODUCT_CHASSIS_NO_PLACEHOLDER'=>'A123456',
	'PRODUCT_CITY_LABEL'=>'Location: City',
	
	'PRODUCT_LICENSE_PLATE_LABEL' => 'LICENSE PLATE',
	'PRODUCT_LICENSE_PLATE_YES' => 'Yes',
	'PRODUCT_LICENSE_PLATE_NO' => 'No',
	
	'PRODUCT_LICENSE_PAPER_LABEL' => 'LICENSE PAPER',
	'PRODUCT_LICENSE_PAPER_YES' => 'Yes',
	'PRODUCT_LICENSE_PAPER_NO' => 'No',
);