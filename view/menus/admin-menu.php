<?php
/*
 * validate admin session ------------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' ) {
?>
        <div id="tab-wrapper">
            
            <a id="newsId" href="#" style="color:#666;border:0" class="tab-title" onclick="displayMenuTab('news_tab');return false;"><?php echo $_GLANG['TOP_MENU_NEWS_HEADER'];?></a>
            <div id="news_tab">
                <a href="<?php echo BASE_RELATIVE;?>home/newsletter/"><?php echo $_GLANG['TOP_MENU_NEWSLETTER_LABEL'];?></a>
                <a href="<?php echo BASE_RELATIVE;?>home/add-news/"><?php echo $_GLANG['TOP_MENU_WRITE_NEWS_LABEL'];?></a>
                <a href="<?php echo BASE_RELATIVE;?>home/community-news/"><?php echo $_GLANG['TOP_MENU_NEWS_MANAGER_LABEL'];?></a>
            </div>
                        
            <a id="generalId" href="#" style="color:#666;border:0" class="tab-title" onclick="displayMenuTab('general_tab');return false;"><?php echo $_GLANG['ADMIN_MENU_GENERAL_TAB'];?></a>
            <div id="general_tab">
                
                

                <a href="<?php echo BASE_RELATIVE;?>home/log-manager/"><?php echo $_GLANG['ADMIN_MENU_LOG_MAMAGER_LABEL'];?></a>
                
            </div>
            
        </div>
<?php } ?>

