<?php 
	ini_set('display_errors', 'on');
	// load class global
	require_once BASE_CLASS . 'class-global.php';
	$global = new classglobal();

	//get spare part data
	$spare_data = $global->getSpareData();

	//copyright
	$copy=$global->getCopyright();

?>
<div id="footer">
	<div id="footerContent">
		<ul >
			<li class="linkimg">
				<a href="<?php echo BASE_RELATIVE; ?>"><p>Home</p></a>
			</li>
			<li class="linkimg">
				<a href="<?php echo BASE_RELATIVE; ?>"><p>Contact</p></a>
			</li>
			<li class="linkimg">
				<a href="http://expressautocambodia.com"><p>PC Version</p></a>
			</li>
			<li class="linkimg">
				<a href="<?php echo BASE_RELATIVE; ?>"><p>Search</p></a>
			</li>			
		</ul>
		
		<div id="copyRight">
			<p><?php echo $copy[0]['text'];?></p>
		</div>
	</div>
</div><!-- #footer -->