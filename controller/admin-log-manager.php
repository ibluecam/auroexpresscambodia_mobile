<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminlogmanager 
{
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $fw_log;
    protected $ecommds_log;
    
    /*
     * Constructor -------------------------------------------------------------
     */
    public function adminlogmanager($lang=array())
    {
        $this->lang = $lang;
        
        // handle delete framework log.
        if( isset($_POST['delBtn']) )
        {
            $this->handleDeleteFWFile();
        }
        
        // load framework logs.
        $this->loadFWLogs();
    }
    
    /*
     * Private method: handle delete framework log file ------------------------
     * @return void.
     */
    private function handleDeleteFWFile()
    {
        $url = trim($_POST['hidfw']);
        $url = stripslashes($url);
        
        if( !file_exists($url) )
        {
            $this->form_message = $this->lang['LOG_DELETE_FW_LOG_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        @unlink($url);
        
        $this->form_message = $this->lang['LOG_DELETE_FW_LOG_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /*
     * Private method: load framework logs -------------------------------------
     * @return void.
     */
    private function loadFWLogs()
    {
        $path = BASE_ROOT . 'log/';
        
        if( !file_exists($path) )
        {
            $this->fw_log = array();
            return;
        }
        
        if( !$dir = @opendir($path) )
        {
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to open framework log directory at ' . __FILE__ . ':' . __LINE__);
            
            $this->fw_log = array();
            
            return;
        }
        
        $this->fw_log = array();
        
        while( ($file = @readdir($dir) ) !== false )
        {
            if( $file != '.' && $file != '..' && $file != 'index.php' && $file != '.htaccess' )
            {
                $dt = substr($file,0,4) . '.' . substr($file,4,2) . '.' . substr($file,6,2);
                $pt = BASE_ROOT . 'log/' . $file;
                
                $o = array(
                    'datum' => $dt,
                    'url' => $pt
                );
                
                array_push($this->fw_log, $o);
            }
        }
        
        @closedir($dir);
        
        return;
    }
    
    /*
     * Public method: get framework log entries --------------------------------
     * @return array.
     */
    public function getFWLog()
    {
        return $this->fw_log;
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
?>
