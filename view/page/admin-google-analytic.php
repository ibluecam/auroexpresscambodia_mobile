<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = 'ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
        <div id="sectionContenWrapper">
            <div id="topMenu">
                <p><a href="#" class="linkfade">Site Manager</a> > News</p>
            </div>
            <?php include("php/sidebar/community.php");?>
        </div>    
            <div id="sectionContent">
                <!-- START CONTENT -->
                <div id="community_title">
                    <p>Google Analytic</p>
                </div>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                	
                     <script type="text/javascript"
    src='https://www.google.com/jsapi?autoload={"modules":[{"name":"visualization","version":"1"}]}'>
  </script>

  <!-- Visualization -->
  <!-- https://developers.google.com/chart/interactive/docs/reference#google.visualization.drawchart -->
  <script type="text/javascript">
    //google.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
      var countryWrapper = new google.visualization.ChartWrapper({
        // Example Country Share Query
         "containerId": "country",
         "dataSourceUrl": "https://testsuperproxy.appspot.com/query?id=ahBzfnRlc3RzdXBlcnByb3h5chULEghBcGlRdWVyeRiAgICAgICACgw&format=data-table-response",
         "refreshInterval": 3600,
         "chartType": "Table",
         "options": {
            "showRowNumber" : true,
			"columnHeader" : false,
            "width": 630,
            "height": 440,
            "is3D": true,
            "title": "all test session"
         }
       });

     
      countryWrapper.draw();

    }
	
  function sessionOneDay() {
      var countryWrapper = new google.visualization.ChartWrapper({
        // Example Country Share Query
         "containerId": "country",
         "dataSourceUrl": "https://testsuperproxy.appspot.com/query?id=ahBzfnRlc3RzdXBlcnByb3h5chULEghBcGlRdWVyeRiAgICA2reXCQw&format=data-table-response",
         "refreshInterval": 3600,
         "chartType": "Table",
         "options": {
            "showRowNumber" : true,
			"columnHeader" : false,
            "width": 630,
            "height": 440,
            "is3D": true,
            "title": "session for one day"
         }
       });

     
      countryWrapper.draw();

    }
	
  function sessionOneweek() {
      var countryWrapper = new google.visualization.ChartWrapper({
        // Example Country Share Query
         "containerId": "country",
         "dataSourceUrl": "https://testsuperproxy.appspot.com/query?id=ahBzfnRlc3RzdXBlcnByb3h5chULEghBcGlRdWVyeRiAgICA2uOGCgw&format=data-table-response",
         "refreshInterval": 3600,
         "chartType": "Table",
         "options": {
            "showRowNumber" : true,
			"columnHeader" : false,
            "width": 630,
            "height": 440,
            "is3D": true,
            "title": "session for one day"
         }
       });

     
      countryWrapper.draw();

    }
	
 function sessionOneMonth() {
      var countryWrapper = new google.visualization.ChartWrapper({
        // Example Country Share Query
         "containerId": "country",
         "dataSourceUrl": "https://testsuperproxy.appspot.com/query?id=ahBzfnRlc3RzdXBlcnByb3h5chULEghBcGlRdWVyeRiAgICAgOSRCgw&format=data-table-response",
         "refreshInterval": 3600,
         "chartType": "Table",
         "options": {
            "showRowNumber" : true,
			"columnHeader" : false,
            "width": 630,
            "height": 440,
            "is3D": true,
            "title": "session for one day"
         }
       });

     
      countryWrapper.draw();

    }
	
	
	
  </script>
  
  		<body onLoad="drawVisualization()" >
	<button id="btnallviewer" onClick="drawVisualization()" >all viewer</button>
    <button id="btnoneday" onClick="sessionOneDay()" >one day</button>
     <button id="btnoneweek" onClick="sessionOneweek()" >7 days</button>
     <button id="btnoneweek" onClick="sessionOneMonth()" >30 days</button>
  
  <div id="browser" style="margin:auto;width:630px;"></div>
  <div id="country" style="margin:auto;width:630px;"></div>

                    
                    
               
              
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}