<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
if( !isset($_SESSION['log_group']) ){
    header('Location: '.BASE_RELATIVE.'login/');
}else{
    if($_SESSION['log_group'] == 'public'){
        header('Location: '.BASE_RELATIVE.'login/');
    }
}
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
/// load page content.
$pg_details = $_CLASS->getPageHTML();
$trad_list=$_CLASS->get_tradlist();
$pic_list=$_CLASS->get_piclist();
 
?>

<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > Write Trade Infomation</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p> Trade Infomation Detail</p>
                </div>
                
                <div id="auto_form">
                	<form  method="post" enctype="multipart/form-data">
                    <table width="705" border="0" cellspacing="0" cellpadding="0">
                        <?php 
                            if(sizeof($trad_list)!=0){ 
                                foreach($trad_list as $trad){
									if ($trad['writer']!=$_SESSION['log_id']){
									   die();
									}
                        ?>
                      <tr>
                        <td class="tdLeft"><p>Title : </p></td>
                        <td>
                            <input type="text" name="topic" value="<?php echo $trad['topic'];?>"  />
                        </td>
                      </tr>
                      <tr>
                        <td class="tdLeft"><p>Category : </p></td>
                        <td> 
                            <div id="radio_wrap" class="clearfix">
                                <div id="all"><input <?php if($trad['category']=='All') echo 'checked="check"';?> type="radio" name="category" value="All"/> All</div>
                                <div id="customs"><input <?php if($trad['category']=='Custome') echo 'checked="check"';?> type="radio" name="category" value="Custome"/> Custome</div>
                                <div id="shipping"><input <?php if($trad['category']=='Shipping') echo 'checked="check"';?> type="radio" name="category" value="Shipping"/> Shipping</div>
                                <div id="photo"><input <?php if($trad['category']=='Import') echo 'checked="check"';?> type="radio" name="category" value="Import"/> Import</div>
                                <div id="photo"><input <?php if($trad['category']=='Export') echo 'checked="check"';?> type="radio" name="category" value="Export"/> Export</div>
                            </div>
                    	</td>
                      </tr>
                      <tr>
                        <td colspan="2">
                        	<textarea id="_ebody" name="rd_htmlInput" ><?php echo $trad['description'];?></textarea>	
                        </td>
                      </tr> 
                      <?php
                            }
                        }
                      ?>                     
                    </table>
                    <div id="deal_register" class="clearfix" style="margin-top:0px;">
                        <div id="attach" class="clearfix" style="margin-top:10px; margin-left:10px;"> 
                        	 <div class='file_upload' id='f1'>
                                <input type="file" name="myfile[]" multiple="multiple"  />
                            </div>
                            <div id='file_tools' style="padding:10px; float:left;">                        	
                                <p> Select your file </p>
                            </div>
                            </div>
                            <div id="button_wrap">
                                <div id="upload" style="margin-left:10">
                           
                                    <input type="submit" class="upload" value="" name="saveTrade" />
                                    
                                    <div id="back">
                                        <a href="<?php echo BASE_RELATIVE;?>trade-info"><img src="<?php echo BASE_RELATIVE;?>images/community/back.png" /></a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <?php
                                    if(sizeof($pic_list)!=0){
                                        foreach($pic_list as $pic){
                                ?>
                                <img id="blah" style="width:185px;hiegth:105px; margin-left:18px; margin-top:20px;" src="<?php echo BASE_RELATIVE."images/trade_info/".$pic['source'];?>" />

                                    <?php
                                        }
                                    }
                                ?>
                            </div>                   	                     
                           
                    </div>
                    
                    </form>
                   
                </div>
                
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  