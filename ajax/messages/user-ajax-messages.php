<?php
/*--------------------- Class Implement ----------------------*/
include('../../config.php');
include('../../core/class-message.php');
if(isset($_GET['type'])) $type=$_GET['type']; else $type="";
$log_id=$_SESSION['log_id'];
$ajaxmessages=new Message();
$ajaxmessages->loadAjaxPagination();
/*--------------------- How to compose message ----------------------*/
//if(isset($_POST['composeMsg'])){
    //$ajaxmessages->composeMessage(200, 'subject', 'message');
//}
/*--------------------- How to move messages to trash ----------------------*/
//$ids=array(8, 9, 11);
//$ajaxmessages->deleteMessages($ids, '1'); //1: soft delete, 2: hard delete
if(isset($_POST['msg_check'])){
    if($type=='trash'){
        $ajaxmessages->deleteMessages($_POST['msg_check'], '2');
    }
    else{
        $ajaxmessages->deleteMessages($_POST['msg_check']);
    }
}
/*--------------------- How to load all messages ----------------------*/
$ajaxmessages->loadAllMessages($type);
$allMessages=$ajaxmessages->getAllMessages();
/*--------------------- How to get all messages of all type ; return array -------*/
$totalNumberMessage=$ajaxmessages->getTotalNumberMessages();
/*/*--------------------- How to get any message by id /*---------------------*/
//$message_detail=$ajaxmessages->getMessageById($_GET['id']);
/*--------------------- How to count unread messages --------------*/
$totalNumberUnreadMessages=$ajaxmessages->getTotalNumberUnreadMessages();
echo "<script type='text/javascript'>
        $('#inboxCount').html({$totalNumberUnreadMessages});
        
    </script>";
/* var_dump error messages */
//var_dump($ajaxmessages->getErrorMessages());


?>

<form method="post" id="message_form">
<div class="content_message_wrapper">
<table cellpadding="0" cellspacing="0" >
    <tr>
        <th class="th_checkbox"><input type="checkbox" id="all_msg_check"/></th>
        <th class="th_msg_img"><img id="delete_selected" src="images/logout-icon/delete.png" onmouseover="this.src='images/logout-icon/delete_over.png'" onmouseout="this.src='images/logout-icon/delete.png'"/></th>
        <th class="th_name">Company Name</th>
        <th class="th_subject">Subject</th>
        <th class="th_date">Date</th>
    </tr>
<?php
/* If there are messages in database */
if(count($allMessages)>0){

    /* Loop through each message */
    foreach($allMessages as $message){
        if($log_id!=$message['receiver']){
            /* User is sender or Type = sent */
            $message_icon='';
            $company_name=$message['receiver_company'];
        }else{
            /* User is receiver or Type = inbox */
            $message_icon="<img src='images/mywini/messagebox/msg{$message['seen']}.gif'/>";
            $company_name=$message['sender_company'];
        }
        echo "
            <tr class='status_{$type}_seen_{$message['seen']}'>
                <td colspan='2'>
                    <input type='checkbox' name='msg_check[]' value='{$message['id']}' class='msg_check'/>
                </td>
				
                <td><a class='msg_link' href='messagebox-detail?id={$message['id']}'><span style='margin-right:5px;'>$message_icon</span>$company_name </a></td>
                <td><a class='msg_link' href='messagebox-detail?id={$message['id']}'>{$message['subject']}</a></td>
                <td><a class='msg_link' href='messagebox-detail?id={$message['id']}'>{$message['updated_date']}</a></td>
            </tr>
            ";
    }
    /* END LOOP */
}else{
/* If no messages */
?>
    <tr>
        <td colspan="5" style="text-align:center;padding:50px;">Sorry you don't have any message at the moment.</td>
       
    </tr>
<?php 
}
/* END IF */
?>
</table>
</div>
<div class="pagination">
    <?php 
    /*------------------------- How to get ajax pagination HTML ---------------------*/
        echo $ajaxmessages->getAjaxPaginationHtml($type);
    ?>
</div>
</form>