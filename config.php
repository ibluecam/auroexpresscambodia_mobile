<?php
/*
 * DATABASE SETTINGS -----------------------------------------------------------Testet
 */

//
 
define('DATABASE_HOST', 'localhost');
define('DATABASE_TABLE', 'auroexpresscambodia');
define('DATABASE_USER', 'root');
define('DATABASE_PASS', '');

//
//define('DATABASE_HOST', 'localhost');
//d/efine('DATABASE_TABLE', 'jtcar');
//d/efine('DATABASE_USER', 'root');
//define('DATABASE_PASS', '');

 

/*
 * #############################################################################
 * ### NO CHANGES ARE REQUIRED AFTER THIS LINE
 * #############################################################################
*/
// uri references
define('BASE_ROOT', dirname(__FILE__) . '/');
define('BASE_SITE', siteURL());
define('BASE_RELATIVE', baseRelative());
define('BASE_CLASS', dirname(__FILE__) . '/core/');

// BBF version reference
define('VERSION', '1.5.1');

/*
* INTERNAL METHODS: DO NOT CHANGE THESE UNLESS YOU KNOW WHAT YOU'RE DOING! ----
*/
// get site URL with the proper protocol
function siteURL() {
    $rel = dirname($_SERVER['PHP_SELF']);
    
    if( $rel == '/' )
    {
        $rel = '';
    }
    
    $protocol = 'http://';
    $domainName = $_SERVER['HTTP_HOST']. $rel . '/';
    return $protocol.$domainName;
}

// check if BBF is installed at the server's root dir
function baseRelative()
{
    $rel = siteURL();
    
    return $rel;
}
