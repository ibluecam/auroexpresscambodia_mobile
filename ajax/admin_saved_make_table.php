<?php
if(!isset($_SESSION)) @session_start();
$log_group=$_SESSION['log_group'];
if($log_group!='admin'){
    return;
}
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();

if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
//if(isset($_GET['offset'])) $offset=htmlspecialchars($_GET['offset']); else $offset=0;
$where="WHERE pm.product_type='$product_type' ";
$sql="SELECT pm.id, pm.maker from product_maker pm $where ORDER BY pm.maker";

if( !$result = @mysql_query($sql) ){
    require_once BASE_CLASS . 'class-log.php';
    $cnx->close();
    LogReport::write('Unable to load make list due a query error at ' . __FILE__ . ':' . __LINE__);
    return;
}
$i=0;
echo "<table class='make_table' id='make_table'>
        <tr>
            <th></th>
            <th>N<sup>o</sup></th>
            <th>Make</th>
            <th></th>
        </tr>";
if(mysql_num_rows($result)>0){
    while( $r = @mysql_fetch_assoc($result) ){
        //array_push($this->city_list,$r);
        $i++;
        $id=$r['id'];                 
        echo 
        "<tr>
            <td>
                <input type='checkbox' class='make_check' value='{$r['maker']}' name='makeCheck[$id]' id='make_check_$id'>
                <input type='hidden' value='{$r['maker']}' name='old_make[$id]' id='old_make_$id'>
            </td>
            <td>$i</td>
            <td id='make_$id'>{$r['maker']}</td>
            <td class='button_td'>
                <a edit_id='$id' class='edit_button' href='#'><img src='images/admin/write.png'></a>
                <a delete_id='$id' class='delete_button' href='#'><img src='images/admin/x.png'></a>
            </td>
        </tr>";
    }
}else{
    echo "<tr><td style='color:#A00000;text-align:center;' colspan='5'>No makes found</td></tr>";
}
echo "<table>";
@mysql_free_result($sql);
$cnx->close();
?>