<?php

require_once './models/Seller.php';
 
class ControllerSeller
{
 
    private $db;
    private $pdo;
    function __construct() 
    {
        require_once './application/DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }
 
    function __destruct() { }
 
    public function updateSeller($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_sellers 
                                        SET 
                                            seller_name = :seller_name, 
                                            contact_no = :contact_no, 
                                            email = :email, 
                                            lat = :lat, 
                                            lon = :lon, 
                                            sms = :sms, 
                                            address = :address 

                                        WHERE seller_id = :seller_id');

        $result = $stmt->execute(
                            array('seller_name' => $itm->seller_name,
                                    'contact_no' => $itm->contact_no,
                                    'email' => $itm->email,
                                    'lat' => $itm->lat,
                                    'lon' => $itm->lon,
                                    'sms' => $itm->sms,
                                    'address' => $itm->address,
                                    'seller_id' => $itm->seller_id) );
        
        return $result ? true : false;
    }

    public function deleteSeller($seller_id, $is_deleted) 
    {
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_sellers 
                                        SET 
                                            is_deleted = :is_deleted 

                                        WHERE seller_id = :seller_id');

        $result = $stmt->execute(
                            array('is_deleted' => $is_deleted,
                                    'seller_id' => $seller_id) );
        
        return $result ? true : false;
    }

    public function insertSeller($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO tbl_carfinder_sellers( 
                                        seller_name, 
                                        contact_no, 
                                        email, 
                                        lat, 
                                        lon, 
                                        sms, 
                                        address ) 

                                    VALUES(
                                        :seller_name, 
                                        :contact_no, 
                                        :email, 
                                        :lat, 
                                        :lon, 
                                        :sms, 
                                        :address )');

        $result = $stmt->execute(
                            array('seller_name' => $itm->seller_name,
                                    'contact_no' => $itm->contact_no,
                                    'email' => $itm->email,
                                    'lat' => $itm->lat,
                                    'lon' => $itm->lon,
                                    'sms' => $itm->sms,
                                    'address' => $itm->address) );
        
        return $result ? true : false;

    }
 
    /**
     * Get user by email and password
     */
    public function getSellers() 
    {
        $stmt = $this->pdo->prepare("SELECT * 
                                        FROM tbl_carfinder_sellers 
                                        WHERE is_deleted = 0 ORDER BY seller_name ASC");

        $result = $stmt->execute( );

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Seller();
                $itm->seller_id = $row['seller_id'];
                $itm->seller_name = $row['seller_name'];
                $itm->contact_no = $row['contact_no'];
                $itm->email = $row['email'];
                $itm->lat = $row['lat'];
                $itm->lon = $row['lon'];
                $itm->sms = $row['sms'];
                $itm->address = $row['address'];

            $array[$ind] = $itm;
            $ind++;
        }

        return $array;
    }

    public function getSellersBySearching($search) 
    {
        $stmt = $this->pdo->prepare("SELECT * 
                                        FROM tbl_carfinder_sellers 
                                        WHERE is_deleted = 0 AND seller_name LIKE :search ORDER BY seller_name ASC");

        $stmt->execute( array('search' => '%'.$search.'%'));

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Seller();
                $itm->seller_id = $row['seller_id'];
                $itm->seller_name = $row['seller_name'];
                $itm->contact_no = $row['contact_no'];
                $itm->email = $row['email'];
                $itm->lat = $row['lat'];
                $itm->lon = $row['lon'];
                $itm->sms = $row['sms'];
                $itm->address = $row['address'];

            $array[$ind] = $itm;
            $ind++;
        }
        return $array;
    }


    public function getSellerBySellerId($seller_id) 
    {
        $stmt = $this->pdo->prepare("SELECT * 
                                        FROM tbl_carfinder_sellers 
                                        WHERE seller_id = :seller_id");

        $stmt->execute( array('seller_id' => $seller_id));
        foreach ($stmt as $row) 
        {
            $itm = new Seller();
                $itm->seller_id = $row['seller_id'];
                $itm->seller_name = $row['seller_name'];
                $itm->contact_no = $row['contact_no'];
                $itm->email = $row['email'];
                $itm->lat = $row['lat'];
                $itm->lon = $row['lon'];
                $itm->sms = $row['sms'];
                $itm->address = $row['address'];

            return $itm;
        }
        return null;
    }


}
 
?>