<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicCompanyDetail
{
    protected $lang;
    protected $group = 'public';
    //protected $slug = 'buyer-directory';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    var $traderList;
    private $car;
    var $total_comitem;
    var $pagination_html;
    var $current_page;
    var $total_page;
	var $relatepost;
    var $register_type="";
    var $vehicle="";
    var $slug_detail='';
    private $companyDetail;
    protected $list_inuquliryimg;
	
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicCompanyDetail($lang=array())
    {
        $this->lang = $lang;
        $this->loadVehicleData();
        $this->listinquiryimg();
        $this->loadCompanyDetail();
		$this->relatedpost();
		
		
		if(isset($_POST['btn_send'])){
			$this->sendEmail();
			}
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        //echo $this->slug;
        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
    /*
     * private method: get name and price of vehicle ------------------------------------------
     * @return bool.
     */
    private function loadVehicleData(){
        $id="";
        require_once BASE_CLASS . 'class-connect.php';
        if(isset($_GET["id"]) ){
            $id = $_GET["id"];
        }

        $cnx = new Connect();
        $cnx->open();
		
		global $pagination_html;
		global $total_comitem;
		
        $where=" WHERE product.is_deleted = 0 ";

        $sql_search="SELECT product.*,car_media.thumb_url,
		register.company_name,register.mobile,register.introduction,
		cf_sellers.seller_name, cf_sellers.contact_no
		
		FROM product 
		LEFT JOIN car_media on product.car_id=car_media.car_id 
		INNER JOIN register on product.`owner`=register.user_id 
		INNER JOIN tbl_carfinder_sellers as cf_sellers on (product.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
        WHERE product.`owner`=$id		
		AND product.is_deleted=0
		AND car_media.sort_photo=1

        GROUP BY product.car_id
        ORDER BY count_sold ASC , count_reserved ASC , modify_date DESC ";		        

        // echo $sql_search;
		$itemPerPage=5;
        $sql_count = @mysql_query($sql_search);
        $total_comitem = @mysql_num_rows($sql_count);

        $links = new Pagination ($total_comitem, $itemPerPage);
        $limit=$links->start_display;
        $pagination_html.= $links->display();
		
		$sql_search.=$limit;
     
        //echo $sql_search;
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }

        $this->vehicle = array();
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->vehicle, $r);
        }
        //var_dump($this->vehicle);
        @mysql_free_result($sql);
        $cnx->close();

    }
    function loadDealweek(){

        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $sql_search="SELECT * from deal_week  ORDER BY id DESC limit 5";

        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }


       $this->deal_week = array();

        while( $r = @mysql_fetch_assoc($sql)){

            array_push($this->deal_week, $r);

        }


        @mysql_free_result($sql);
        $cnx->close();
        return $this->deal_week;

    }

    /*
     * Public method: get load trader data ------------------------------------
     * @return array | false.
     */
    public function loadCompanyDetail(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $company_detail=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['id'])) $id=mysql_real_escape_string(stripcslashes($_GET['id'])); else $id="";

        if(empty($id)){
            return;
        }

        $query = "SELECT `rg`.`firstname`,`rg`.`lastname`,rg.log_id,`rg`.`image`, `rg`.`password`,`rg`.`introduction` , `rg`.`company_name`, `rg`.`message`, `rg`.`email`, `rg`.`messengerID`, `rg`.`mobile`, `rg`.`country`, `rg`.`address`, `rg`.`avalible_service`, `rg`.`web`,
				cf_sellers.seller_name, cf_sellers.contact_no 
                
				FROM `register` AS `rg`             	
				INNER JOIN tbl_carfinder_sellers as cf_sellers on (rg.user_id=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                WHERE `rg`.`user_id`='$id'

        ";

        $result = mysql_query($query);


        //echo $query;

        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($company_detail, $r);
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        //SET PAGE SLUG



        if(mysql_num_rows($result)>0){
            $array_slug=array();
            $array_slug[]=$company_detail['0']['company_name'];
            $array_slug[]=$company_detail['0']['address'];
            $array_slug[]=$company_detail['0']['country'];
			$array_slug[]=$company_detail['0']['mobile'];
			$array_slug[]=$company_detail['0']['message'];
            $str_slug=implode(" ", $array_slug);
            $this->slug_detail=$str_slug;

        }

        $this->companyDetail= $company_detail;
        $cnx->close();
    }
	
	 public function RelatedPost(){
        require_once BASE_CLASS . 'class-connect.php';
        if(isset($_GET['id'])){
            $this->vehicle_own =$_GET['id'];
        }

        $cnx = new Connect();
        $cnx->open();

        $query = "SELECT p.*, c.photo_url as carImg, rg.user_id as company_id
                FROM product as p
                LEFT JOIN car_media as c on p.car_id=c.car_id AND c.primary_photo=1 OR c.primary_photo=2
				INNER JOIN `register` as rg on `p`.`owner`=`rg`.`user_id`
                WHERE p.product_type='Car' AND p.is_deleted='0' AND rg.user_id ='".$this->vehicle_own."' AND c.author='".$this->vehicle_own."' ORDER BY RAND() LIMIT 0,4";
        $result = mysql_query($query);

        $this->related_post = array();

        while( $r = @mysql_fetch_assoc($result) ){
            array_push($this->related_post, $r);
        }

        @mysql_free_result($result);
        $cnx->close();

    }
	
    public function getCompanyImages(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $images=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['id'])) $id=mysql_real_escape_string(stripcslashes($_GET['id'])); else $id="";

        if(empty($id)){
            return;
        }

        $query = "SELECT `mi`.`m_thumb`, `mi`.`m_image`
                FROM `more_image` AS `mi` WHERE `mi`.`owner`='$id'
        ";

        $result = mysql_query($query);


        //echo $query;

        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($images, $r);
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        return $images;
        $cnx->close();
    }
    /*
     * Public method: get number by condition data --------------------------------------------
     * @return array | false.
     */
    public function getExpertCompanyNumberByCountry(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";
        $count="";
        $join="";

        $query = "SELECT `country`, COUNT(rg.user_id) AS `number` FROM register as rg
                WHERE rg.register_type LIKE '%$this->register_type%' GROUP BY rg.`country` ";
        $result = mysql_query($query);
        //echo $query;
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            $sellerNumber[$r['country']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        return $sellerNumber;
        $cnx->close();
    }



    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProductType(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();


        $query = "SELECT pd.product_type, COUNT(DISTINCT(owner)) AS number
                FROM product as pd
                INNER JOIN register as rg ON pd.owner=rg.user_id
                WHERE rg.register_type LIKE '%$this->register_type%'
                GROUP BY pd.product_type"
        ;


        //echo $query;
        $result = mysql_query($query);

        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r['product_type']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;

    }
    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProduct($column){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();
        $where_product_type="";
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(!empty($product_type)){
             $where_product_type=" AND product_type='$product_type'";
        }
        $query = "SELECT pd.`$column`, (SELECT COUNT(DISTINCT(owner)) FROM product WHERE `$column`=pd.`$column`$where_product_type) AS number
                FROM register as rg
                LEFT JOIN product as pd ON pd.owner=rg.user_id
                WHERE rg.register_type LIKE '%$this->register_type%'
                GROUP BY pd.`$column`  "
        ;
        //echo $query."<br/>";
        $result = mysql_query($query);

        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r[$column]]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;

    }
    function countCountry($product_type=""){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $and="";
        $countcountry=array();
        $and=" AND (`member_type1` LIKE '%seller%' OR `member_type1` LIKE '%forwarder%')";
        if(!empty($product_type)){
            $and.= " AND `pd`.`product_type` = '$product_type'";
        }


        //if(!empty($vehicle_type)){
        //  $and.= " AND `vehicle_type`='$vehicle_type'";
        //}


        $sql_str="
                SELECT country, cl.country_name, COUNT(DISTINCT(rg.user_id)) as `number`
                FROM `register` as `rg`
                LEFT JOIN `product` as `pd` ON `pd`.`owner`=`rg`.`user_id`
                INNER JOIN `country_list` as cl ON `cl`.`cc`=`rg`.`country`
                WHERE 1$and
                GROUP BY `rg`.`country`
                ORDER BY `number` DESC
                LIMIT 0, 10
                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($countcountry, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $countcountry;

    }
             public function listinquiryimg()
    {

        require_once BASE_ROOT . 'core/class-connect.php';

        $id=0;
        if(isset($_GET['id'])){
            $id=$_GET['id'];
        }
        if(isset($_GET['id'])) $getId=htmlspecialchars($_GET['id']); else $getId=$_SESSION['log_id'];

        if(empty($getId)||$_SESSION['log_group']=='user'){
            $reg_id=  $_SESSION['log_id'];
        }elseif($_SESSION['log_group']=='admin'){
            $reg_id=  $getId;
        }
        //$reg_id=  $_SESSION['log_id'];


        $cnx = new Connect();
        $cnx->open();
        if( !$sql_more_img = @mysql_query("SELECT *
             FROM  `more_image` WHERE `owner`='$getId' ") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }
		
        $this->list_inuquliryimg = @mysql_fetch_assoc($sql_more_img);
        $cnx->close();
        return $this->list_inuquliryimg;


    }
	
	public function sendEmail(){
		$to=$_POST['email'];
		$message=$_POST['message'];
		$subject = 'Your Account ID';
		$headers = "Reply-To: support@motorbb.com\r\n";
		
		if(mail($to,$subject,$message,$headers)){
			echo "Your have been sent an email...!";
			}
		}
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getTraderList()
    {
        return $this->traderList;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    public function getVehicle(){
        return $this->vehicle;
    }

    /*
     * Public method: get company detail ------------------------------------
     * @return array.
     */
    public function getrelatepost(){
        return $this->related_post;
    }
	 public function getCompanyDetail(){
        return $this->companyDetail;
    }

}
?>

