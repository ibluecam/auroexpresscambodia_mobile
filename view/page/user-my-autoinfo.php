<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

$product_maker = $_CLASS->loadProduct();
 if(isset($_GET['autoinfo_search'])){            
$auto_info = $_CLASS->autoInfoSearch($_GET['id']);

}
else{
    $auto_info = $_CLASS->loadAutoInfo($_GET['id']);
} 
$pagination=$_CLASS->pagination_html;
$total_row=$_CLASS->total_num_row;
$current_page=$_CLASS->current_page;
$total_page=$_CLASS->total_page;
   



?>

<script type="text/javascript">
    $(document).ready(function(e){
        // $('.btn_search').click(function(e){
        //    $('.search').val('123');
        // });
        //$("[name='category']").val("<?php if(isset($_GET['category'])) echo htmlspecialchars($_GET['category']); ?>");

        $("[name=category]").val(['<?php if(isset($_GET['category'])) echo htmlspecialchars($_GET['category']);?>']);
        //$("input[type='radio'][name='category'][checked='checked']").val("<?php if(isset($_GET['category'])) echo htmlspecialchars($_GET['category']);?>");
        //$('input[name=radio]:checked').val("<?php if(isset($_GET['category'])) echo htmlspecialchars($_GET['category']);?>");
        $('.search').val("<?php if(isset($_GET['keyword'])) echo htmlspecialchars($_GET['keyword']); ?>");
        $('.car').val("<?php if(isset($_GET['item'])) echo htmlspecialchars($_GET['item']); ?>");
        $('.make').val("<?php if(isset($_GET['item'])) echo htmlspecialchars($_GET['make']); ?>");

        
        
        
    });
    function showInfo(intValue){
        window.location = "<?php echo BASE_RELATIVE;?>show-autoinfo?id="+intValue;
    }


</script>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/pagination.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > Auto Infomation</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Auto Infomation</p>
                </div>
                <div id="auto_detail" class="clearfix">
                	<div id="text">
                   		<p class="first">Get all the information on all kinds of vehicles here.</p>
                        <p>This is a section where you can post any kinds of information yourself and share with those in need!</p>
                    </div>
                    <div id="button">
                    	<div id="article">
                        	<a href="<?php echo BASE_RELATIVE;?>register-info"><img src="<?php echo BASE_RELATIVE;?>images/community/write.png"  class="linkfade"/></a>
                        </div>
                        
                    </div>
                </div>
                <div id="auto_form">
                	<form action="<?php echo BASE_RELATIVE;?>my-autoinfo" method="get">
                    <input type="hidden" name="id" value="<?php echo $_SESSION['log_id']?>">
                	<div id="radio_wrap" class="clearfix">
                    	<div id="all"><input type="radio" name="category" value="" /> All</div>
                        <div id="spec"><input type="radio" name="category" value="specification" /> Specification</div>
                        <div id="news"><input type="radio" name="category" value="auto_new" /> Auto News</div>
                        <div id="photo"><input type="radio" name="category" value="photo" /> Photo</div>
                    </div>
                    <div id="textbox_wrap" class="clearfix">
                    	<select class="car" name='item'>
                            <option value="">Select</option>
                            <option>Car</option>
                            <option>Truck</option>
                            <option>Bus</option>
                            <option>Heavy Machine</option>
                        </select>
                        <select class="make" name='make'>
                            <option value="">Make</option>

                            <?php foreach($product_maker as $value){ ?>  
                                <option><?php echo $value['make'];?></option>
                            <?php }?>
                            
                        </select>                        
                        <input type="text" class="search" name='keyword' />
                        <input type="submit" class="btn_search" value="" name="autoinfo_search">
                        
                   	</div>
                    </form>
                    <div id="list_detail">
                    	<ul class="clearfix">
                        	<li class="no blue_color">No.</li>
                            <li class="category blue_color">Category</li>
                            <li class="item blue_color">Item</li>
                            <li class="maker blue_color">Make</li>
                            <li class="model blue_color">Model</li>
                            <li class="topic blue_color">Topic</li>
                            <li class="writen blue_color">Writen by</li>
                            <li class="date blue_color">Date</li>
                        </ul>
                        <div class="load_auto_info">
                        <?php $count=1; foreach($auto_info as $row){
                            $str = $row['date'];  
                            $i = strrpos($str, " ");
                            $l = strlen($str) - $i;
                            $str = substr($str, $i, $l);
                            $date = str_ireplace($str, "", $row['date']);
                        ?>
                        <div <?php if($count%2==0) echo "class = ' bg_color'"?>>
                        <ul class="clearfix " style="cursor:pointer;" onclick="showInfo('<?php echo $row['id']?>')">
                        	<li class="no"><?php echo $row['id']?></li>
                            <li class="category"><?php echo $row['category'];?></li>
                            <li class="item"><?php echo $row['item'];?></li>
                            <li class="maker"><?php echo $row['make'];?></li>
                            <li class="model"><?php echo $row['model'];?></li>
                            <li class="topic"><?php echo $row['topic'];?></li>
                            <li class="writen"><?php echo $row['writen_by'];?></li>
                            <li class="date"><?php echo $date;?></li>                            
                        </ul>
                        </div>
                        <?php $count++; }?>
                        </div>
                    </div>
                </div>
                <div id="pagination">
                	<?php       
                    echo $pagination;
                        
?>

                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  