
<link href="<?php echo BASE_RELATIVE;?>css/blog/blog-contact.css" rel="stylesheet" type="text/css" />
    
<div id="sectionContent" class="clearfix">    	
	<?php include('layout/sidebar.php'); ?>
    <div id="sectionFeature" class="clearfix">
    	<div id="contactHead" class="clearfix">
        	<p><b>Contact Us</b></p>
        </div>
        <div id="contactInfo">
        	<p>
            	Company            : Kyung Nam Sky Corporation<br />
Contact Person  : Wan Sik Lee<br />
Tel                      : +82-11-473-0501<br />
Mobile                : +82-010-5473-0501<br />
Fax                     : +82-32-582-3233<br />
Address              : Incheon Seo-gu Baekseok-dong 58-2, Ara autovalley A-130<br />
Business Field    : Car, Bus, Truck, Auto Part, Others<br />
Website              : http://www.kncar.co.kr <br />
            </p>
        </div>
        <div id="contactMap">
        	<iframe width="630" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=203489871496949212402.0004f606addd95c6be5f7&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=11.558323,104.91997&amp;spn=0.003679,0.006759&amp;z=17&amp;output=embed"></iframe>
        </div>
        
        <div id="sectionContact">
        <p>
            <b>Contact Us</b><br />
            <b>To. Wan Sik Lee (ID: kncar)</b><br />
            Test Add [+] Del [x]
        </p>
        <table width="640" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
                <textarea rows="5"></textarea>
            </td>
          </tr>
          <tr>
            <td>
                <!--<input type="file" name="fileUpload" />
                <input type="submit" class="btndelete" value="delete" /><input type="submit" class="btnadd" value="add" />-->
                <div class='file_upload' id='f1'><input name='file[]' type='file'/></div>
                <div id='file_tools'>                        	
                    <img src='images/btnadd.gif' id='add_file' title='Add new input'/>
                    <img src='images/btndelete.gif' id='del_file' title='Delete'/>
                </div>
                
            </td>
          </tr>
          <tr>
            <td>
                <p>
                    * Available File Formats : XLS, DOC, PPT, .JPG, GIF, BMP, ZIP, PDF, PNG, HWP, TXT*<br />
                                                                         Max file upload size : 10MB
                </p>
            </td>
          </tr>
          <tr>
            <td>
                <input type="submit" value="send" class="btnsend linkfade" />
            </td>
          </tr>
        </table>        
    </div>
        
    </div>
    
</div>