<?php
$csv_url = 'mjford.csv';

// check if file exists.
if( !file_exists($csv_url) ){
    die('CSV not found!');
}

// get file content.
require_once 'core/class-csv.php';
$csv = new parseCSV();

$csv->auto($csv_url);

// save values into array.
$result_array = array();

// get array values.
foreach( $csv->data as $key => $row ){
    $obj = array();
    
    foreach( $row as $value => $t ){
        // set obj index.
        $ind = strtolower($value);
        $ind = str_replace(' ', '_', $ind);
        
        // process options.
        if( $ind == 'options' ){
            $opt_array = explode(',',$t);
            $obj[$ind] = $opt_array;
        }
        
        // process categorized options.
        else if( $ind == 'categorized_options' ){
            $cat_array = explode('@',$t);
            $obj[$ind] = $cat_array;
        }
        
        // process image list.
        else if( $ind == 'imagelist' ){
            $img_array = explode(',',$t);
            $obj[$ind] = $img_array;
        }
        
        else {
            $obj[$ind] = $t;
        }
    }
    
    array_push($result_array, $obj);
}

for( $i=0; $i < count($result_array); $i++ ){
    echo $result_array[$i]['imagelist'][0] . '<br>';
}

