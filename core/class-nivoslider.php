<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/**
 * Manage nivo slider properties.
 *
 * @author J. Taniguchi
 * @date 2012.09.10
 */
class NivoSliderManager {
    protected $animation_speed;
    protected $pause_time;
    protected $display_direction_navigation;
    protected $constrol_navigation_thumbs;
    protected $control_navigation_bullets;
    protected $pause_onmouse_hover;
    protected $manual_advance;
    protected $preview_text;
    protected $next_text;
    protected $random_start;
    protected $banner_list;
    protected $nivoslider_enabled = true;  // set this to false if you don't want the slider to load.
    
    /*
     * Constructor
     * load values from database.
     * @return void.
     */
    public function NivoSliderManager()
    {
        if( $this->nivoslider_enabled )
        {
            $this->load();
        }
    }
    
    
    /*
     * Protected method: load nivo slider configuration.
     * @return void.
     */
    protected function load()
    {
        $this->banner_list = array();
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        $sql = @mysql_query("SELECT * FROM `nivo_config` LIMIT 1;");
        
        if( !$sql )
        {
            // fail silently.
            $db->close();
            
            $this->nivoslider_enabled = false;
            
            LogReport::write('Unable to query database to read NivoSlider configuration. Error thrown at ' . __FILE__.':'.__LINE__);
            return;
        }
        else
        {
            if( mysql_num_rows($sql) == 1 )
            {
                $r = mysql_fetch_assoc($sql);
                @mysql_free_result($sql);
                
                $this->animation_speed                  = $r['animation_speed'];                // Slide transition speed
                $this->pause_time                       = $r['pause_time'];                     // How long each slide will show
                $this->display_direction_navigation     = $r['display_direction_navigation'];   // Display Next & Prev navigation
                $this->control_navigation_thumbs        = $r['control_navigation_thumbs'];     // Use thumbnails as Control Nav
                $this->control_navigation_bullets       = $r['control_navigation_bullets'];     // 1,2,3... navigation
                $this->pause_onmouse_hover              = $r['pause_onmouse_hover'];            // Stop animation while hovering
                $this->manual_advance                   = $r['manual_advance'];                 // Force manual transitions
                $this->preview_text                     = $r['preview_text'];                   // Prev directionNav text
                $this->next_text                        = $r['next_text'];                      // Next directionNav text
                $this->random_start                     = $r['random_start'];                   // Start on a random slide
            }
            else
            {
                $this->animation_speed                  = '500';
                $this->pause_time                       = '3000';
                $this->display_direction_navigation     = true;
                $this->control_navigation_thumbs        = true;
                $this->control_navigation_bullets       = true;
                $this->pause_onmouse_hover              = true;
                $this->manual_advance                   = false;
                $this->preview_text                     = 'Next';
                $this->next_text                        = 'Prev';
                $this->random_start                     = false;
            }
            
            // Load banner list.
            $db->open();
            
            $sql = @mysql_query("SELECT * FROM `nivo_list` WHERE `enabled`='1'");
            
            if( !$sql )
            {
                // fail silently.
                $this->nivoslider_enabled = false;
                
                $db->close();
                
                LogReport::write('Unable to query database for nivoslider banner list at ' . __FILE__.':'.__LINE__);
                return;
            }
            else
            {
                if( mysql_num_rows($sql) > 0 )
                {
                    while( $q = mysql_fetch_assoc($sql) )
                    {
                        $obj = array(
                            'id' => $q['id'],
                            'enabled' => $q['enabled'],
                            'source' => $q['source'],
                            'caption' => $q['caption']
                        );
                        
                        array_push($this->banner_list, $obj);
                    }
                    
                    @mysql_free_result($sql);
                    $db->close();
                }
            }
        }
    }
    
    /*
     * Public getter: get banner list
     * @return array.
     */
    public function getBannerList()
    {
        return $this->banner_list;
    }
    
    /*
     * Public getter: enable nivo slider
     * nivo slider enabled status
     * @return boolean.
     */
    public function getNivoStatus()
    {
        return $this->nivoslider_enabled;
    }
    
    /*
     * Public getter: random start
     * start on a random slice.
     * @return true.
     */
    public function getRandomStart()
    {
        if( $this->random_start )
        {
            return 'true';
        }
        else
        {
            return 'false';
        }
    }
    
    /*
     * Public getter: next text
     * Next direction navigation text.
     * @return string.
     */
    public function getNextText()
    {
        return $this->next_text;
    }
    
    /*
     * Public getter: previous text
     * Previous direction navigation text.
     * @return string.
     */
    public function getPrevText()
    {
        return $this->preview_text;
    }
    
    /*
     * Public getter: manual advance
     * force manual transitions.
     * @return string.
     */
    public function getManualAdvance()
    {
        if( $this->manual_advance )
        {
            return 'true';
        }
        else
        {
            return 'false';
        }
    }
    
    /*
     * Public getter: pause on mouse over
     * stop animation while hovering.
     * @return string.
     */
    public function getPauseOnMouseOver()
    {
        if( $this->pause_onmouse_hover )
        {
            return 'true';
        }
        else
        {
            return 'false';
        }
    }
    
    /*
     * Public getter: control navigation bullets
     * 1,2,3....navigation.
     * @return string.
     */
    public function getControlNavigationBullets()
    {
        if( $this->control_navigation_bullets )
        {
            return 'true';
        }
        else
        {
            return 'false';
        }
    }
    
    /*
     * Public getter: control navigation thumbs
     * use thumbnails as control navigation.
     * @return string.
     */
    public function getControlNavigationThumb()
    {
        if( $this->constrol_navigation_thumbs )
        {
            return 'true';
        }
        else
        {
            return 'false';
        }
    }
    
    /*
     * Public getter: display direction navigation
     * display Next & Prev navigation.
     * @return string.
     */
    public function getDirectionNavigation()
    {
        if( $this->display_direction_navigation )
        {
            return 'true';
        }
        else
        {
            return 'false';
        }
    }
    
    /*
     * Public getter: pause time
     * how long each slide will show.
     * @return int.
     */
    public function getPauseTime()
    {
        return $this->pause_time;
    }


    /*
     * Public getter: animation speed
     * the slide transition speed.
     * @return int.
     */
    public function getAnimationSpeed()
    {
        return $this->animation_speed;
    }
    
}
