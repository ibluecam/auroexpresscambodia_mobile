<?php

 
class ControllerRest
{
 
    private $db;
    private $db_path;
    private $pdo;
    function __construct($db_path) 
    {
        $this->db_path = $db_path;
        require_once $this->db_path;
        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }
 
    function __destruct() { }
 
    public function getPhotoCarsResult() 
    {
        $stmt = $this->pdo->prepare('SELECT * FROM car_media WHERE is_deleted = 0');

        $stmt->execute();
        return $stmt;
    }

    public function getPhotoDealersResult() 
    {
        $stmt = $this->pdo->prepare('SELECT * FROM tbl_carfinder_photo_dealers WHERE is_deleted = 0');
        $stmt->execute();
        return $stmt;
    }

    public function getMakeResult() 
    {
        $stmt = $this->pdo->prepare('SELECT * FROM tbl_carfinder_make WHERE is_deleted = 0');
        $stmt->execute();
        return $stmt;
    }

    public function getSellersResult() 
    {
        $stmt = $this->pdo->prepare('SELECT * FROM tbl_carfinder_sellers WHERE is_deleted = 0');
        $stmt->execute();
        return $stmt;
    }

    public function getCarsResult() 
    {
        $stmt = $this->pdo->prepare('SELECT * FROM product WHERE is_deleted = 0');
        $stmt->execute();
        return $stmt;
    }

    public function getDealersResult() 
    {
        $stmt = $this->pdo->prepare('SELECT * FROM tbl_carfinder_dealers WHERE is_deleted = 0');
        $stmt->execute();
        return $stmt;
    }


}
 
?>