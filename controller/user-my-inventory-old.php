<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.   test     ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

require_once BASE_ROOT . 'core/class-pagination.php';

class userMyInventory
{
    protected $lang;
    protected $group = 'user';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    private $delete_form_message;
    private $product_type;
    private $vehicle;
    private $makers;
    private $models;
    private $pagination;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userMyInventory($lang=array())
    {
        $this->lang = $lang;  
        $this->updateProduct();
        $this->loadProductType();  
        $this->loadMaker(@$_GET['product_type']);
        $this->loadModel(@$_GET['make']);
        $this->loadVehicleData();
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
    
    private function loadVehicleData()
    {         
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $from = "FROM ( product AS p LEFT JOIN car_media AS c ON p.id = c.product_id ) ".
                 "LEFT JOIN country_list AS cl ON p.location = cl.cc ".
                 "INNER JOIN register AS r ON p.`owner` = r.id ";
        $where = "WHERE r.email='".$_SESSION['log_email']."' AND del_flag=0 ";

        $query_uri = "";
        if(isset($_GET['text_chassis_no']) && !empty($_GET['text_chassis_no']))
        {
            $where.= "AND p.chassis_no = '". $_GET['text_chassis_no']."' ";        
        }
        else
        {
            if(isset($_GET['product_type']) && ($_GET['product_type'] !== "")){
                $where.= "AND p.product_type = '". $_GET['product_type']."' ";
                $query_uri.= ("product_type=". $_GET['product_type']. "&");
            }

            // Query by maker
            if(isset($_GET['make']) && ($_GET['make'] !== "")){
                $where.= "AND p.make = '". $_GET['make']."' ";
                $query_uri.= ("make=". $_GET["make"]. "&");
            }

            // Query by model
            if(isset($_GET['model']) && ($_GET['model'] !== "")){
                $where.= "AND p.model = '". $_GET['model']."' ";
                $query_uri.= ('model='. $_GET['model']. "&");
            }
        }

        $group_by = "GROUP BY p.id ";

        $order_by = "ORDER BY p.created_date DESC ";
        if(isset($_GET['sort'])){
            $order_by = "ORDER BY p.created_date ". $_GET['sort'] . " ";
            $query_uri.= ('sort='. $_GET['sort']);
        }

        if(!$calc_found_row_count = @mysql_query("SELECT SQL_CALC_FOUND_ROWS * ". $from. $where. $group_by)){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        $rows_count = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows_count'));

        // Allocate pagination.
        $this->pagination = new Pagination($rows_count['rows_count'], @$_GET['page'] === null ? 0 : $_GET['page'], 5);
        $this->pagination->setQueryString($query_uri);


        $limit = "LIMIT ". ($this->pagination->getStartIndex() < 0 ? 0 : $this->pagination->getStartIndex()). ", ". $this->pagination->getNumberOfRecordPerPage(). " ";

        if( !$sql = @mysql_query("SELECT
                p.*,
                (CASE WHEN p.created_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) THEN 'new' ELSE 'old' END) AS arrival,
                (SELECT COUNT(*) FROM message WHERE response_id = r.email AND product_id = p.id AND seen = 0) AS unread_message,
                (SELECT COUNT(*) FROM message WHERE response_id = r.email AND product_id = p.id) AS message,
                r.email,
                (SELECT source FROM car_media WHERE primary_photo = 1 and product_id = p.id LIMIT 1) AS carImg,
                cl.country_name AS country,
                cl.cc AS flag ".
            $from. $where. $group_by. $order_by. $limit) ){

            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);

            $cnx->close();
            return;
        }

        $this->vehicle = array();
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
       }

        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->vehicle, $r);
        }

        @mysql_free_result($rows_count);
        @mysql_free_result($sql);
        $cnx->close();
    }

    private function loadProductType()
    {
        require_once BASE_CLASS . 'class-connect.php';        
        $cnx = new Connect();
        $cnx->open();

        $this->product_type = array();
        if( !$result = mysql_query("SELECT product_type FROM `product_maker` GROUP BY product_type ORDER BY `product_type` ") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car type list due a query error at ' . __FILE__ . ':' . __LINE__);            
        }
        elseif(mysql_num_rows($result) > 0){
            while( $r = mysql_fetch_assoc($result)){
                array_push($this->product_type, $r);
            }
        }

        $cnx->close();
    }

    private function loadMaker($product_type = null)
    {
        $this->makers = array();
        if($product_type === null) 
        {            
            return;
        }

        // Load car maker.
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        if(!$query = @mysql_query("SELECT DISTINCT make FROM product WHERE product_type='$product_type' AND make != '' ORDER BY `make`")){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        while($row = @mysql_fetch_assoc($query)){
            array_push($this->makers, $row);
        }

        @mysql_free_result($query);
        $cnx->close();
    }

    private function loadModel($maker = null)
    {
        $this->models = array();
        if($maker === null) 
        {            
            return;
        }

        // Load car model.
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        if(!$query = @mysql_query("SELECT DISTINCT model FROM product WHERE make='$maker' AND `model` != '' ORDER BY `model`")){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        while($row = @mysql_fetch_assoc($query)){
            array_push($this->models, $row);
        }
        @mysql_free_result($query);
        $cnx->close();
    }

    private function updateProduct(){
        if (isset($_POST['delete'])) {
            require_once BASE_CLASS . 'class-connect.php';
            $cnx = new Connect();
			$owner_id=$_SESSION['log_id'];
            $cnx->open();

            $car_ids = $_POST['delete'];
            $del = 1;
            foreach ($car_ids as $id) {
                if($result = mysql_query("UPDATE product SET del_flag = 1 WHERE id = '{$id}' and owner='$owner_id'")){
                    $del += 1;
                } else {
                    $del = 0;
                    $this->form_status = 'query';
                    return;
                }
            }

            if( $del != 1 ) {
                $this->form_status = 'complete';
            } else {
                $this->form_status = 'select';
            }
        }
        elseif(isset($_POST['sold'])){
            require_once BASE_CLASS . 'class-connect.php';
            $cnx = new Connect();
			$owner_id=$_SESSION['log_id'];
            $cnx->open();

            $car_ids = $_POST['sold'];
            foreach ($car_ids as $id) {
                @mysql_query("UPDATE product SET status = 'Sold' WHERE id = '{$id}' and owner='$owner_id';");
            }

            $cnx->close();
        }
        elseif(isset($_POST['reserved'])){
            require_once BASE_CLASS . 'class-connect.php';
            $cnx = new Connect();
            $cnx->open();

            $car_ids = $_POST['reserved'];
            foreach ($car_ids as $id) {
                @mysql_query("UPDATE product SET status = 'Reserved' WHERE id = '{$id}'");
            }

            $cnx->close();
        }
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -----------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

    public function getProductType()
    {
        return $this->product_type;
    }
	
    public function getMaker(){
        return $this->makers;
    }

    public function getModel(){
        return $this->models;
    }

    public function getVehicleData(){
        return $this->vehicle;
    }
    public function getPagination(){
        return $this->pagination;
    }

}
?>
