
    $(document).ready(function(e) {

            
            

            //$('#make').load(base_url+'ajax/load_car_make.php');

            //$('#model').load(base_url+'ajax/load_car_model.php'); 
            if( $('#make').length ){ 
                $('#make').change(function(e) {
                    make=$('#make option:selected').val();
                    model="";
                    loadModel();
                });
            }
            if( $('#category').length ){     
                $('#category').change(function(e) {
                    category=$('#category option:selected').val();
                    loadSubCategory();
                });
            }

            if( $('#category2').length ){     
                $('#category2').change(function(e) {
                    category2=$('#category2 option:selected').val();
                    loadSubCategory();
                });
            }
            if( $('#category3').length ){     
                $('#category3').change(function(e) {
                    category3=$('#category3 option:selected').val();
                    loadSubCategory();
                });
            }


            if( $('#condition').length ){   
                if(condition!="") $("#condition").val(condition);
            }
            if( $('#category').length ){ 
                loadSubCategory();
                
            }

            if( $('#make').length ){   
                loadModel();
            }
            
            if( $('#steering').length ){   
                if(steering!="") $("#steering").val(steering);
            }
            if( $('#country').length ){   
                if(country!="") $('#country').val(country);
            }
            if( $('#year_to').length ){   
                if(year_to!="") $('#year_to').val(year_to);
            }
            if( $('#year_from').length ){   
                if(year_from!="") $('#year_from').val(year_from);
            }
            if( $('#car_id').length ){ 
                if(car_id!="") $('#car_id').val(car_id);
            }
            if( $('#customSearch').length ){ 
                if(customSearch!="") $('#customSearch').val(customSearch);
            }
            
    });
    
    function loadSubCategory(){
        if(category!=""){
            $("#category").val(category);
            if( $('#category2').length ){ 
                $('#category2').load('ajax/load_category2.php?category='+encodeURIComponent(category), function(e){
                    if(category2!=""){
                        $("#category2").val(category2);
                        if( $('#category3').length ){ 
                            $('#category3').load('ajax/load_category3.php?category2='+encodeURIComponent(category2), function(e){
                                if(category3!="") $("#category3").val(category3);
                            });  
                        }
                    }
                });  
                    
            }
        } 
    }
    function loadModel(){
        if(make!=""){
            $("#make").val(make);
            if( $('#model').length ){ 
                $('#model').load('ajax/load_car_model.php?make='+encodeURIComponent(make)+'&product_type='+encodeURIComponent(product_type), function(e){
                    if(model!="") $("#model").val(model);
                });  
                    
            }        
        }
    }