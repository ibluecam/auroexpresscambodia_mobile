<?php

$_GLOB = array(

    'UNAUTHORIZED_ACCESS_MESSAGE' => 'You must have the proper permissions to view this page.',

     

    

    // ADMINISTRATOR MENU ------------------------------------------------------

    // CONTACT

    'ADMIN_MENU_CONTACT_TAB' => 'Contacts',

    'ADMIN_MENU_INBOX_LABEL' => 'Inbox',

    'ADMIN_MENU_NEWMESSAGE_LABEL' => 'Compose a Message',

    'ADMIN_MENU_EDIT_TEMPLATE_LABEL' => 'Edit E-mail Template',

    

    // GENERAL

    'ADMIN_MENU_GENERAL_TAB' => 'General',

    'ADMIN_MENU_FRAMEWORK_SETTINGS_LABEL' => 'Framework Settings',

    'ADMIN_MENU_MANAGE_ACCOUNTS_LABEL' => 'Manage Accounts',

    'ADMIN_MENU_TERMS_OF_USE_LABEL' => 'Terms of Use Policy',

    'ADMIN_MENU_PRIVACY_POLICY_LABEL' => 'Privacy Policy',

    'ADMIN_MENU_GROUP_MAMAGER_LABEL' => 'Manage User Groups',

    'ADMIN_MENU_LOG_MAMAGER_LABEL' => 'Log Manager',

    'ADMIN_MENU_COMPANY_DETAILS_LABEL' => 'Company details',

    

    // MODULES MENU

    'ADMIN_MENU_PRETTY_PHOTO_SETTINGS' => 'PrettyPhoto Settings',

    'ADMIN_MENU_NIVO_SLIDER_SETTINGS' => 'Nivo Slider Setting',

    'ADMIN_MENU_MODULES_HEADER_LABEL' => 'Modules',

    'ADMIN_MENU_UPDATE_INSTALLER' => 'Update Installer',

    

    // CMS MENU

    'ADMIN_MENU_CMS_MANAGER_HEADER_LABEL' => 'CMS Manager',

    'ADMIN_MENU_MENU_MANAGER_LABEL' => 'Menu Anchor Manager',

    'ADMIN_MENU_CREATE_NEW_MENU_LABEL' => 'Menu Manager',

    'ADMIN_MENU_CREATE_NEW_PAGE_LABEL' => 'Create a New Page',

    'ADMIN_MENU_PAGE_MANAGER_LABEL' => 'Pages Manager',

    'ADMIN_MENU_THEME_HEADER_LABEL' => 'Theme',

    'ADMIN_MENU_THEME_MANAGER_LABEL' => 'Theme Manager',

    

    // ADMIN TOP MENU

    'ADMIN_TOPMENU_HOME_LABEL' => 'Home',

    'ADMIN_TOPMENU_LIVECHAT_LABEL' => 'Live Support Manager',

    

    // PUBLIC MENU HEADER

    'PUBLIC_TOPMENU_HOME_LABEL' => 'Home',

    'PUBLIC_TOPMENU_CATEGORY_LABEL' => 'Categories',

    'PUBLIC_TOPMENU_SELLER_LABEL' => 'Sellers',

    'PUBLIC_TOPMENU_ABOUTUS_LABEL' => 'About Us',

    'PUBLIC_TOPMENU_MYACCOUNT_LABEL' => 'My Account',

    'PUBLIC_TOPMENU_ALL_VEHICLE' => 'All Cars',

	'PUBLIC_TOPMENU_VEHICLE_TYPE' => 'Car Types',

	'PUBLIC_TOPMENU_VEHICLE_USED' => 'Used Cars',

	'PUBLIC_TOPMENU_VEHICLE_NEW' => 'New Cars',
	
	'PUBLIC_TOPMENU_EXPLORE_FAVORITE' => 'Explore your Favorite',
	
	'PUBLIC_TOPMENU_DEALER' => 'Dealers',

    'PUBLIC_TOPMENU_CURRENT_STOCK' => 'Current Stock',

    'PUBLIC_TOPMENU_USA_STOCK' => 'USA Stock',

    'PUBLIC_TOPMENU_FIND_LOCAL_DEALER' => 'Find Local Dealers',

    'PUBLIC_TOPMENU_OTHER_SERVICE' => 'Other Services',

    'PUBLIC_TOPMENU_CONTACT_US' => 'Contacts Us',

    

    // GLOBAL

    'LOGOUT_ANCHOR' => 'Logout',

    'PUBLIC_MENU_GENERAL_LABEL' => 'General',

    

    // TOP MENU HEADER

	'TOP_MENU_TIME' => 'Cambodia Time',

	'TOP_MENU_MEMBER' => 'Not a member yet?',

	'TOP_MENU_TOTAL_VEHICLE' => 'TOTAL VEHICLES',

    'TOP_MENU_REGISTER' => 'Register  /  Login',

    'TOP_MENU_LOGOUT' => 'Log out',

    'TOP_MENU_SUPPORT' => 'support',

	// Top Login Success for private user

	'TOP_MY_ACCOUNT'=>'My account',

	'TOP_MY_INVENTORY'=>'Inventory',

	'TOP_MY_UPLOAD_NEW_CAR'=>'Upload new car',

	'TOP_LOGOUT'=>'Logout',

	// Top Guest

	'TOP_NOT_MEMBER'=>'Not a member yet?',

	'TOP_REGISTER'=>'Sign up',

	'TOP_LOGIN'=>'Sign in',

	'TOP_GUEST'=>'Guest',

    

    // CAR MANAGER TAB

    'TOP_MENU_CAR_MANAGER_HEADER' => 'Car Manager',

    'TOP_MENU_NEW_CAR_LABEL' => 'Add new car',

    'TOP_MENU_CAR_MANAGER_LABEL' => 'Manage cars',

    'TOP_MENU_RESERVATION_MANAGER_LABEL' => 'Car reservation manager',

    'TOP_MENU_FEATURE_ICON_LABEL' => 'Featured icon manager',

    'TOP_MENU_RESERVATION_POLICY_LABEL' => 'Edit reservation policy',

    'TOP_MENU_IMPORT_LIST' => 'Import CSV list',

    

    // NEWS TAB

    'TOP_MENU_NEWSLETTER_LABEL' => 'Send newsletter',

    'TOP_MENU_NEWS_HEADER' => 'News Manager',

    'TOP_MENU_WRITE_NEWS_LABEL' => 'Add news',

    'TOP_MENU_NEWS_MANAGER_LABEL' => 'Manage news',

    

    // MANAGE STATIC PAGES

    'TOP_MENU_STATICPAGE_HEADER' => 'Manage Static Pages',

    'TOP_MENU_STATICPAGE_LABEL' => 'Manage static pages',

    

    // USER MENU

    'TOP_MENU_MY_ACCOUNT_LABEL' => 'My account',

    'TOP_MENU_CAR_RESERVATION_LABEL' => 'My reservations',

    'TOP_MENU_CAR_FAVORITES_LABEL' => 'Favorites list',

    

    // FOOT HEADER

    'FOOT_HEADER_RIGHTS_RESERVED' => 'All rights reserved. Copyrights',

    'FOOT_HEADER_ABOUTUS_LABEL' => 'About Us',

    'FOOT_HEADER_TOU_LABEL' => 'Terms of Use',

    'FOOT_HEADER_PRIVACY_LABEL' => 'Privacy',

    'FOOT_HEADER_PRESS_LABEL' => 'Press',

    'FOOT_HEADER_TERMS' => 'About our company',

    'FOOT_HEADER_LANGUAGE_HEADER' => 'Language',

    'FOOT_HEADER_SOCIAL_HEADER' => 'Social Network',

	

	// RGISTER	

	'REGISTER_BUSINESS_TYPE_STATUS' => 'Register',

	'REGISTER_ID'=>'ID',

	'REGISTER_EMAIL'=>'Email',

	'REGISTER_PASSWORD'=>'Password',

	'REGISTER_CONFIRM_PASSWORD'=>'Confirm password',

	'REGISTER_TERM'=>'I agree with the',

	'REGISTER_COMPANY_NAME'=>'Company name',

	'REGISTER_PHONE_NUMBER'=>'Phone number',

	'REGISTER_SECURITY_QUESTION'=>'Security question',

	'REGISTER_SUBMIT'=>'Submit',

	//Login Translate

	'LOGIN_STATUS'=>'Log in',

	'LOGIN_ID'=>'ID',

	'LOGIN_PASSWORD'=>'Password',

	'LOGIN_KEEP_ME_SIGNIN'=>'Keep me sign in',

	'LOGIN_FORGOT_ID'=>'Forgot ID',

	'LOGIN_FORGOT_PASSWORD'=>'Forgot Password',

	

	

	

	'REGISTER_BUSINESS_FIELD_STATUS' => 'Please check your business field.',

	'REGISTER_CARD_ID_STATUS' => 'The field Card ID is required.',

    'REGISTER_COMPLETE_STATUS' => 'Account registered successfully! Please check your e-mail box to confirm the registration.',

    'REGISTER_COMPLETE_LABEL' => 'Your account has been registered successfully. Please check your e-mail box to authenticate your account registration.',

	'REGISTER_CONFIRMEMAIL_STATUS' => 'Your E-mail does not match.',

    'REGISTER_CONFIRMPASSWORD_STATUS' => 'Your password does not match.',

    'REGISTER_CONFIRMPASSWORD_LABEL' => 'Confirm Password',

	'REGISTER_COUNTRY_STATUS' => 'Please select country.',

    'REGISTER_DESC_LABEL' => 'The fields marked with the * symbol are required.',

    'REGISTER_ERROR_STATUS' => 'Unable to register account due an internal error. Please notify the system administrator.',

    'REGISTER_ERROR_LABEL' => 'Unable to register account due an internal error. Please notify the system administrator.',

    'REGISTER_EMAIL_ERROR_LABEL' => 'The e-mail address you are trying to use is already registered.',

	'REGISTER_EMAIL_ERROR_STATUS' => 'The e-mail address you are trying to use is already registered.',

    'REGISTER_EMAIL_STATUS' => 'The field e-mail is required.',

    'REGISTER_EMAIL_LABEL' => 'E-Mail Address',

    'REGISTER_FIELDALLFIELDS_LABEL' => 'Please fill all fields before submitting the form.',

    'REGISTER_FILLALLFIELDS_STATUS' => 'Please fill all fields before submitting the form.',

	'REGISTER_ID_STATUS' => 'The field ID is required.',

	'REGISTER_ID_LENGTH_STATUS' => 'The field ID is 4-12 characters',

	'REGISTER_ID_EXIST_STATUS' => 'The field ID existed',

	'REGISTER_ID_EXIST_LABEL' => 'The field ID existed',

	'REGISTER_INVALID_EMAIL_STATUS' => 'The E-mail is not valid.',

	'REGISTER_INVALID_EMAIL_LABEL' => 'The E-mail is not valid.',

	'REGISTER_INVALID_MOBILE_STATUS' => 'Invalid mobile numbers.',

	'REGISTER_INVALID_NAME_STATUS' => 'Your name must be English characters only.',

	'REGISTER_INVALID_NAME_LABEL' => 'Your name must be English characters only.',

	'REGISTER_INVALID_TEL_STATUS' => 'Invalid phone numbers.',

	'REGISTER_MEMBER_STATUS' => 'Please check your member type.',

	'REGISTER_MOBILE_STATUS' => 'The field mobile is required.',

    'REGISTER_NAME_LABEL' => 'Your Name',

    'REGISTER_NAME_STATUS' => 'The field name is required.',

    'REGISTER_PASSWORD_LABEL' => 'Your Password',

    'REGISTER_PASSWORD_STATUS' => 'Your password must have between 5 to 20 characters.',

	'REGISTER_TAB_TITLE' => 'Register a new account',

	'REGISTER_ADDRESS_STATUS' => 'Address is required.',

	'REGISTER_TEL_STATUS' => 'Telephone numbers is required.',

    'REGISTER_TERMSOFUSE_LABEL' => 'Terms of Use',

    'REGISTER_TERMSOFUSE_STATUS' => 'You must accept the terms of use to register your account.',

    'REGISTER_TERMSOFUSE_AGREEMENT_LABEL' => 'I have read and accept the <a href="'.BASE_RELATIVE.'view/lib/terms-of-use.html?iframe=true&width=800&height=600" rel="prettyPhoto">Terms of Use</a>.',

    'REGISTER_SECURITYQUESTION_LABEL' => 'Security Question',

    'REGISTER_SECURITYQUESTION_QUESTION' => 'How much is',

    'REGISTER_SECURITYQUESTION_STATUS' => 'Invalid answer.',

	'REGISTER_UPLOAD_ERROR_STATUS' => 'Cannot upload the picture. Make sure the extension is JPEG or PNG and its size is less than 350Kb',

	'REGISTER_UPLOAD_ERROR_LABEL' => 'Cannot upload the picture. Make sure the extension is JPEG or PNG and its size is less than 350Kb',

    'REGISTER_FORM_SUBMIT_BUTTON_LABEL' => 'Register Now',

    'SENDEMAIL_SUBJECT_LABEL' => 'Account Activation',

    'SENDEMAIL_ACTVATE_NOW' => 'Activate now',

    'SENDEMAIL_MESSAGE_REGISTRATION' => 'Thank you for registering your account with us!<br>Please click the link below to activate your account:<br>'

);

