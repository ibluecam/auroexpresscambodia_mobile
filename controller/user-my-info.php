<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class userMyinfo
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	private $userInfo;
    protected $list_inuquliryimg;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userMyinfo($lang=array())
    {
        $this->lang = $lang;
		$this->loadUserData();
        $this->listinquiryimg();

    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
	private function loadUserData(){
		require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

		//$userlog_id=  $_SESSION['userlog_id'];
		$log_id=  $_SESSION['log_id'];
		
		
		//$query = "SELECT rg.image,rg.firstname,rg.lastname,rg.member_type3,rg.contact_name ,rg.company_name, rg.address, rg.mobile, rg.email, rg.id as register_id, rg.country, cl.phonecode, cl.country_name FROM register as rg
        //LEFT JOIN `country_list` as cl ON `cl`.`cc`=`rg`.`country`
         //WHERE `rg`.`user_id`='$log_id' ";
		 
		$query ="SELECT rg.username,rg.image,rg.firstname,rg.lastname,rg.member_type3,rg.contact_name,rg.address, rg.mobile,rg.country, rg.email,cl.phonecode, cl.country_name FROM register as rg  LEFT JOIN `country_list` as cl ON `cl`.`cc`=`rg`.`country` WHERE `rg`.`user_id`='$log_id' ";
		$result = mysql_query($query);


		//echo $query;
		$this->userInfo = array();
		while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($this->userInfo, $r);
		}
		//echo mysql_error();
		//var_dump($this->userInfo);

		$cnx->close();
	}
	public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['user_id']          = $r['user_id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
          public function listinquiryimg()
    {

        require_once BASE_ROOT . 'core/class-connect.php';

        $id=0;
		
        if(isset($_GET['id'])){
            $id=$_GET['id'];
        }
        if(isset($_GET['id'])) $getId=htmlspecialchars($_GET['id']); else $getId='';

        if(empty($getId)||$_SESSION['log_group']=='user'){
            $reg_id=  $_SESSION['log_id'];
        }elseif($_SESSION['log_group']=='admin'){
            $reg_id=  $getId;
        }
        //$reg_id=  $_SESSION['log_id'];


        $cnx = new Connect();
        $cnx->open();
        if( !$sql = @mysql_query("SELECT *
             FROM  `more_image` WHERE `owner`='$reg_id';") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }
         $this->list_inuquliryimg = array();
        while( $r = @mysql_fetch_assoc($sql) ){

            array_push($this->list_inuquliryimg, $r);

        }
        $cnx->close();
        //var_dump($this->list_inuquliry);
        return $this->list_inuquliryimg;


    }


    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

	public function getUserInfo(){
        return $this->userInfo;
    }

}
?>
