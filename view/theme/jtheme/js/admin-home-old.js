/* validate api field */
function validateAPI(){
    var a = document.getElementById('apiField');
    
    if( a == undefined ){
        return false;
    }
    
    // min. 10 digits.
    if( a.value.length < 10 ){
        a.style.borderColor = '#F00';
        return false;
    }
    
    return true;
}
