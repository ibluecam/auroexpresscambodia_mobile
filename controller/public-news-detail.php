<?php

if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this active_product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
require BASE_CLASS . 'class-message.php';
require BASE_CLASS . 'class-product.php';
class publicnewsdetail
{
	
    protected $lang;
    protected $group = 'public';
    protected $slug = 'cardetail';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	
	private $load_news_detail;
	private $load_detail;
	private $new_id;
	
	var $news_video;
	var $news_img;
	var $desc_share;
	var $author_by;
	var $news_title;
	
    public function publicnewsdetail($lang=array())
    {
        $this->lang = $lang;
		$this->loadnewscarbyid();
		$this->loadnewscar_detail();			
    }
	public function loadnewscarbyid()
	{
		require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';
		
		if(isset($_GET['id'])){
            $this->new_id=$_GET['id'];
        }
		
        $cnx = new Connect();
        $cnx->open();	
		
		global $author_by;
		
		$sql_news=" SELECT * FROM news WHERE id='".$this->new_id."' and publish=1";
		
		if( !$sql = @mysql_query($sql_news) ){
		require_once BASE_CLASS . 'class-log.php';
		LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
		$cnx->close();
			return;
		}
		
		if( @mysql_num_rows($sql) < 1 ){
			$cnx->close();
		}

		$cache_offer_products = array();
		while( $r = @mysql_fetch_assoc($sql) ){
			// get author
			$author_by = $r['author'];
			
			// get title
			$this->news_title = $r['title'];
			
			// get image from youtube video share
			/*$html=strchr($r['video_url'],"src=");
			preg_match( '@src="([^"]+)"@' , $html, $match );
			$src = array_pop($match);
			$array_id=explode("/",$src);					
			$vd_youtube = array_pop($array_id);
			$this->news_video = $vd_youtube;*/
			
			$vd_youtube=$r['video_poster'];
			$this->news_video = $vd_youtube;
			
			// get image share
			$html_img=strchr($r['html'],"src=");
			preg_match( '@src="([^"]+)"@' , $html_img, $match_img );
			$src_img = array_pop($match_img);
			$this->news_img = $src_img;
			
			//get description share
			$html_desc=$r['html'];			
			$html_desc=strip_tags($html_desc, '<br><br/>');
			$this->desc_share = $html_desc;
			
			array_push($cache_offer_products, $r);
		}

		$this->load_news_detail=$cache_offer_products;
		@mysql_free_result($sql);
		$cnx->close();
		
	}
	
	public function loadnewscar_detail()
	{
		require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();	
		
		$sql_all=" SELECT * FROM news WHERE publish='1' limit 0,7";
		
		if( !$sql = @mysql_query($sql_all) ){
		require_once BASE_CLASS . 'class-log.php';
		LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
		$cnx->close();
			return;
		}
		
		if( @mysql_num_rows($sql) < 1 ){
			$cnx->close();
		}

		$cache_offer_products = array();
		while( $r = @mysql_fetch_assoc($sql) ){
			array_push($cache_offer_products, $r);
		}

		$this->load_detail=$cache_offer_products;
		@mysql_free_result($sql);
		$cnx->close();
		
	}
	 public function loadcar_id(){
        return $this->load_news_detail;
    }
	public function loadcar_detail(){
        return $this->load_detail;
    }
}
