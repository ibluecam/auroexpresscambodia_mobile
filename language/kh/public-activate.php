<?php
$_LANG = array(
    'ACTIVATE_HEADER_TITLE' => 'Account Activation',
    'ACTIVATE_LABELA' => 'Invalid activation code. Please use the link sent to your email box to activate your account.',
    'ACTIVATE_LABELB' => 'Invalid activation code. Please use the link sent to your email box to activate your account.',
    'ACTIVATE_LABELC' => 'Invalid activation code. Please use the link sent to your email box to activate your account.',
    'ACTIVATE_LABELD' => 'Your account is already active.',
    'ACTIVATE_LABELE' => 'Unable to activate account due an internal error. Please report the problem to the system administrator.',
    'ACTIVATE_LABELF' => 'Your account has been activated successfully! Thank you for having us!'
);