<?php
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();

$sql="SELECT * FROM car_media cm WHERE NOT EXISTS (SELECT p.id from product p WHERE p.id = cm.product_id);";
$result=@mysql_query($sql);
$tmp_data=array();
while($row=@mysql_fetch_assoc($result)){
    array_push($tmp_data, $row);
}
$cnx->close();

echo "<table class='list_table'>
        <tr>
            <th>ID</th>
            <th>Owner</th>
            <th>Product ID</th>
            <th>Source</th>
        </tr>";

if(count($tmp_data)>0){
    foreach($tmp_data as $data){
        echo "<tr>
                <td>{$data['id']}</td>
                <td>{$data['author']}</td>
                <td>{$data['product_id']}</td>
                <td><a href='{$data['source']}'>{$data['source']}</a></td>
            </tr>";
    }
}else{
    echo '<td colspan="5" style="color:#A00000;text-align:center;">No temporary data found</td>';
}
                            
echo "</table>";