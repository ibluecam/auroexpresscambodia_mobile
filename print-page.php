<?php
if(!isset($_SESSION)) @session_start();
/*
 * This file is used by the public-car-detail.php to display a print-friendly
 * page. All HTML and CSS and Javascript should be inline.
 */
$car_id = trim($_GET['cid']);
$valid_content = true;

// include configuration file.
require_once 'config.php';

// include language file.
require_once BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/print-page.php';

// initialize lists.
$car;   // car info.
$thumb; // thumbnail image.
$feat;  // feature names.
$spec;  // details list.

// validate id.
if( empty($valid_content) ){
    $valid_content = false;
}

require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

$car_id = @mysql_real_escape_string($car_id);

if( !$sql = @mysql_query("SELECT * FROM `car` WHERE `id`='$car_id' LIMIT 1;") ){
    require_once BASE_CLASS . 'class-log.php';
    
    $cnx->close();
    $valid_content = false;
    
    LogReport::write('Unable to load vehicle information due a query error at ' . __FILE__ . ':' . __LINE__);
}
else {
    if( @mysql_num_rows($sql) != 1 ){
        $cnx->close();
        $valid_content = false;
    }
    else {
        // set car list.
        $r = @mysql_fetch_assoc($sql);
        
        $car = array(
            'id' => $r['id'],
            'maker' => stripslashes($r['maker']),
            'model' => stripslashes($r['model']),
            'body_type' => (int)$r['body_type'],
            'price' => $r['price'],
            'year' => $r['year'],
            'miles' => $r['miles'],
            'measure_type' => stripslashes($r['measure_type']),
            'door' => (int)$r['doors'],
            'eco' => stripslashes($r['eco'])
        );
        
        @mysql_free_result($sql);
        
        // load features list.
        if( !$sql = @mysql_query("SELECT * FROM `car_features` WHERE `car_id`='$car_id'") ){
            $car = array();
            $valid_content = false;
            $cnx->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load car features list due a query error at ' . __FILE__ . ':' . __LINE__);
        }
         else {
             if( @mysql_num_rows($sql) > 0 ){
                 $feat = array();
                 
                 while( $r = @mysql_fetch_assoc($sql) ){
                     $obj = array(
                         'feat_name' => stripslashes($r['feat_name']),
                         'feat_icon' => stripslashes($r['feat_icon'])
                     );
                     
                     array_push($feat, $obj);
                 }
                 
                 @mysql_free_result($sql);
             }
             
             // load details list.
             if( !$sql = @mysql_query("SELECT * FROM `car_details` WHERE `car_id`='$car_id' LIMIT 1;") ){
                 $cnx->close();
                 $feat = array();
                 $car = array();
                 $valid_content = false;
                 
                 require_once BASE_CLASS . 'class-log.php';
            
                 LogReport::write('Unable to load car details list due a query error at ' . __FILE__ . ':' . __LINE__);
             }
             else {
                 if( @mysql_num_rows($sql) == 1 ){
                     $r = @mysql_fetch_assoc($sql);
                     @mysql_free_result($sql);
                     
                     $spec = array(
                         'engine_size' => stripslashes($r['engine_size']),
                         'trim' => stripslashes($r['trim']),
                         'type' => stripslashes($r['type']),
                         'gear' => stripslashes($r['gear']),
                         'fuel' => stripslashes($r['fuel']),
                         'color' => stripslashes($r['color']),
                         'prev_owners' => stripslashes($r['prev_owners']),
                         'last_service' => stripslashes($r['last_service']),
                         'mot' => stripslashes($r['mot']),
                         'tax_band' => stripslashes($r['tax_band']),
                         'top_speed' => stripslashes($r['top_speed']),
                         'engine_torque_rpm' => stripslashes($r['engine_torque_rpm']),
                         'engine_power_kw' => stripslashes($r['engine_power_kw']),
                         'transmission_type' => stripslashes($r['transmission_type']),
                         'html' => stripslashes($r['html'])
                     );
                 }
                 
                 // load 1 thumbnail from car media.
                 if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE `car_id`='$car_id' AND `mode`='exterior' LIMIT 1;") ){
                     $cnx->close();
                    $feat = array();
                    $car = array();
                    $valid_content = false;

                    require_once BASE_CLASS . 'class-log.php';

                    LogReport::write('Unable to load car media (thumbnail) due a query error at ' . __FILE__ . ':' . __LINE__);
                 }
                 else {
                     if( @mysql_num_rows($sql) != 1 ){
                         $thumb = 'image/default_main_image.jpg';
                     }
                     else {
                         $r = @mysql_fetch_assoc($sql);
                         $thumb = $r['source'];
                         @mysql_free_result($sql);
                     }
                     
                     $cnx->close();
                     $valid_content = true;
                 }
             }
         }
    }
}

/**
 * Public method: get body name by body id
 * <br>-------------------------------------------------------------------------
 * @param $bid The body type id.
 * @return string
 */
function getBodyById($bid, $_LANG){
    $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
    if( !file_exists($path) ){
        return $_LANG['PRINT_UNDEFINED_LABEL'];
    }
    
    // include body type list translation.
    require_once $path;
    
    $result = '';
    
    foreach($_CAR_BODY as $k => $v){
        if( $k == $bid ){
            $result = $v;
            break;
        }
    }
    
    return $result;
}

?>
<html>
    <head>
        <title></title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='http://fonts.googleapis.com/css?family=Alef:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id="container">
            <?php
                // display vehicle information IF request is valid.
                if( $valid_content ){
                    require_once BASE_CLASS . 'class-utilities.php';
            ?>
            <p style="text-align:right;">
                <a href="#" onclick="window.print();return false;" style="font-family: Alef;">&raquo <?php echo $_LANG['PRINT_PRINT_PAGE_BUTTON'];?></a> &nbsp; | &nbsp;
                <a href="#" onclick="window.close();return false;" style="font-family: Alef;">&raquo <?php echo $_LANG['PRINT_CLOSE_BUTTON'];?></a>
            </p>
            <h1 style="font-family: Alef;"><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . getBodyById($car['body_type'], $_LANG) . ' ' . $car['year'];?></h1>
            
            <!-- price -->
            <h2 style="font-family: Alef;color:#006600;text-indent:50px;font-size:26px;"><?php echo $_SESSION['CURRENCY_SYMBOL'] . Utilities::formatPrice($car['price'],$_SESSION['CURRENCY_CODE']);?></h2>
            
            <!-- image and vehicle specifications -->
            <h2 style="font-family: Alef"><?php echo $_LANG['PRINT_VEHICLE_SPECIFICATION_HEADER'];?></h2>
            <img src="<?php echo BASE_RELATIVE . $thumb;?>" width="300" height="200" style="float:left;margin-right:20px;border:1px solid #CCC;"/>
            <div style="width:445px;float:left;overflow:hidden;">
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo strtoupper($car['measure_type']);?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $car['miles'] . ' ' . $car['measure_type'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_TRIM_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['trim'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_GEAR_BOX_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['gear'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_PREV_OWNERS_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['prev_owners'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_MOT_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['mot'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_TOP_SPEED_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['top_speed'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_KW_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['engine_power_kw'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_ENERGY_CLASS_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $car['eco'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_ENGINE_SIZE_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['engine_size'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_TYPE_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['type'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_FUEL_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['fuel'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_COLOR_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['color'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_LAST_SERVICE_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['last_service'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_TAX_BAND_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['tax_band'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_ENGINE_TORQUE_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['engine_torque_rpm'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_TRANSMISSION_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $spec['transmission_type'];?></span>
                
                <span style="font-family: Alef;font-weight:bold;text-transform:uppercase;display:block;width:178px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $_LANG['PRINT_DOORS_LABEL'];?></span>
                <span style="font-family: Alef;display:block;width:266px;height:24px;line-height:24px;overflow:hidden;float:left;"><?php echo $car['door'];?></span>
                <div style="clear:both;float:none;"></div>
            </div>
            
            <!-- feature list -->
            <?php
                if( count($feat) > 0 ){
            ?>
            <h2 style="font-family: Alef"><?php echo $_LANG['PRINT_FEATURES_LABEL'];?></h2>            
            <ul style="font-family: Alef">
            <?php
                    for( $i=0; $i < count($feat); $i++ ){
                        echo '<li>' . $feat[$i]['feat_name'] . '</li>';
                    }
            ?>
            </ul>
            <?php
                }
            ?>
            
            <!-- more details (html) -->
            <?php
                if( !empty($spec['html']) ){
            ?>
            <h2 style="font-family: Alef"><?php echo $_LANG['PRINT_MORE_INFORMATION_LABEL'];?></h2>
            <?php
                    echo $spec['html'];
                }
            ?>
            
            <?php
                }
                else {
                    echo '<p style="text-align:right;"><a href="#" onclick="window.close();return false;" style="font-family: Alef;">&raquo '.$_LANG['PRINT_CLOSE_BUTTON'].'</a></p>';
                    echo '<p style="font-family: Alef">' . $_LANG['PRINT_INVALID_REQUEST_MESSAGE'] . '</p>';
                }
            ?>
        </div>
    </body>
</html>
