<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = 'ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
        <div id="sectionContenWrapper">
            <div id="topMenu">
                <p><a href="#" class="linkfade">Site Manager</a> > News</p>
            </div>
            <?php include("php/sidebar/community.php");?>
        </div>    
            <div id="sectionContent">
                <!-- START CONTENT -->
                <div id="community_title">
                    <p>Add News</p>
                </div>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <div class="form_container">
                    <form method="post">
                        <div class="input-prepend">
                            <span class="add-on" style="width:120px;float:left;"><?php echo $_LANG['ADDNEWS_TITLE_LABEL'];?></span>
                            <input type="text" class="text_box" style="width:200px;" name="titleInput" value="<?php echo ( isset($_SESSION['news_title']) ? $_SESSION['news_title'] : '' );?>" placeholder="<?php echo $_LANG['ADDNEWS_TITLE_PLACEHOLDER'];?>" style=""/>
                        </div>
                        
                        <div style="height:30px;width:250px;float:left;margin-left:30px;padding-top:3px;padding-left:50px;">
                        		Published: 
                                <input style="margin-left:10px" name="publish" value="0" type="radio" />No
                                <input style="margin-left:10px;" checked="checked" name="publish" value="1" type="radio" />Yes
                        </div>
                        
                      
                        
                        <div class="input-prepend">
                            <span class="add-on" style="width:120px;float:left;"><?php echo $_LANG['ADDNEWS_PRODUCT_TYPE_LABEL'];?></span>
                            <select class="text_box" style="width:200px;" name="productTypeSelect" >
                                <option value="Car">Car</option>
                                <option value="Truck">Truck</option>
                                <option value="Bus">Bus</option>
                                <option value="Equipment">Heavy Machine</option>
                                <option value="Part">Part</option>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                        <div class="input-prepend" style="margin-top:15px;">
                            <textarea name="bodyInput" id="ebody"><?php echo ( isset($_SESSION['news_body']) ? $_SESSION['news_body'] : '' );?></textarea>
                            <div class="clearfix"></div>
                            <script type="text/javascript">
				 var editor = CKEDITOR.replace('ebody');
				//editor.resize( '100%', '1000',true);
        </script>
                        </div>
                        <input type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['ADDNEWS_SAVE_BUTTON'];?>" />
                    </form>
                </div>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}