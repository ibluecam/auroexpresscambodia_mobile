<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    $fav = $_CLASS->getFavList();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['FAV_HEADER_LABEL'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display favorite list.
                    if( count($fav) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['FAV_LIST_EMPTY_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                        for( $i=0; $i < count($fav); $i++ ){
                            $car = $_CLASS->getCarByID($fav[$i]['car_id']);
                            
                            // display car if not empty.
                            // if empty, it means car is reserved or sold.
                            if( !empty($car['maker']) ){
                ?>
                <form method="post" class="fav-wrap">
                    <a href="<?php echo $car['img'];?>" rel="prettyPhoto">
                        <img src="<?php echo $car['img'];?>" width="70" height="50" class="img-polaroid" />
                    </a>
                    <p><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $car['year'];?></p>
                    <a href="<?php echo BASE_RELATIVE;?>car-detail/?cid=<?php echo $fav[$i]['car_id'];?>" class="btn btn-small btn-info">More details</a>
                    <input type="hidden" name="cID" value="<?php echo $fav[$i]['car_id'];?>" />
                    <input type="submit" onclick="return confirm('<?php echo $_LANG['FAV_REMOVE_CONFIRM'];?>');" name="rInput" value="<?php echo $_LANG['FAV_REMOVE_BUTTON'];?>" class="btn btn-small btn-danger" />
                    <div class="clearfix"></div>
                </form>
                <?php
                            }
                        }
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}