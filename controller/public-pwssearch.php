<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicpwssearch
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	
	//send error when not input email.
	var $form_error_message;
	var $email_not_has;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicpwssearch($lang=array())
    {

        $this->lang = $lang;

         /*if(isset($_POST['btn_pwd'])){
             $this->sendMail();
         }
		 */
		 
		 
		if(isset($_POST['btn_id'])){
		    $get_email=$_POST['txtid'];
			if($get_email==""){
				$this->form_error_message="notinput";
			}
			else{
				if($_POST['forgot']=="id"){
					$this->sendMailid();
				}
				else{
				  $this->sendMail();
				}
			}
         }
		 
    }
	
    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }


    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */


	public function sendMail(){

		require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

		$emial_to = mysql_real_escape_string($_POST['txtid']);

		$keyReset = md5(mt_rand());
		$query = "UPDATE register SET key_resetpws='".$keyReset."' WHERE email='".$emial_to."'";
		$result = mysql_query($query);
		$subject = 'Reset your password - Account';

		$headers = "From:angkorauto.com <info@angkorauto.com>\r\n";
		$headers .= "Reply-To: support@angkorauto.com\r\n";
		$headers .= "CC: support@angkorauto.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message .= '<html><body style="margin:0 auto; padding:0 auto;">';
		$message .= '<div style="margin:0 auto;line-height:10px;padding:0;width:100%;">
					<div style="width:650px; margin:0 auto;padding:0 auto;background:#eeeeee;">
					<h2 style="padding-top:20px;text-align:center;color:#2c65b6;">Thank you for using Angkorauto.com</h2>
					<!--<p class="pag-1">Thank you for registering with Angkorauto.</p>-->
					<p style="text-align:center;font-size:14px;">Please click on this link below to reset your password.</p>
					<p style="text-align:center; margin-bottom:20px;"> <a href="'.BASE_RELATIVE.'resetpws?keyReset='.$keyReset.'">'.BASE_RELATIVE.'resetpws?keyReset='.$keyReset.'</a></p>
					<div style="border-top:1px dashed #CCC;border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
					<!--<p class="pag-1">Your account will not be activated until you verify your email address.</p>-->
					<p style="text-align:center;font-size:14px; margin:30px 0;"><span style="font-size:14px;font-weight:bold;text-align:center;">Having trouble accessing your account?</span> <span>Please contact</span> 
					<span><a style="text-align:center;text-decoration:underline;color:blue;" href="mailto:support@angkorauto.com">Online Customer Support.</a></span></p>
					</div>
					<div style="border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
					<div style="float:left;padding:10px 0;"><img src="http://angkorauto.com/images/common/header/softbloom.png" /></div>
					<div style="float:left;padding:10px 0; margin-left:160px;width:75px;" >
					<table >
					<tr><td colspan="4" >Email:info@angkorauto.com</td></tr>
					<tr><td></td></tr><tr>
							<td><img src="http://angkorauto.com/images/social_icons/fb.png" /></td>
                            <td><img src="http://angkorauto.com/images/social_icons/tt.png" /></td>
                            <td><img src="http://angkorauto.com/images/social_icons/gplus.png" /></td>
                            <td><img src="http://angkorauto.com/images/social_icons/in.png" /></td>
					</tr></table>
					</div>
					<p style="clear:both"></p> 
					</div>
					<p style="text-align:center;font-size:14px;padding:5px 0 20px 0;">Copyright &copy; 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>
					</div></div>';
			
		$message .= '</body></html>';

		if(mail($emial_to, $subject, $message, $headers)){
			$_SESSION['keyRest'] = $keyReset;
			//echo '<p style="color:#900000">Please check your email, the key for reseting your password already send to your email!</p>';
			header("Location: " .BASE_RELATIVE . "reset-password");
		}

		$cnx->close();
	}
	
	public function sendMailid(){

		require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

		$emial_to = mysql_real_escape_string($_POST['txtid']);

		//$log_id=(mt_rand());
		$result="";
		$com_result="";
		$query =mysql_query("SELECT `username` FROM register WHERE email='".$emial_to."'");
		$result=mysql_fetch_array($query);
		$count_row=mysql_num_rows($query);
		
		if($count_row>0){
		$subject = 'Your Account ID';
		$com_result=$result['username'];
		$headers = "From:angkorauto.com <info@angkorauto.com>\r\n";
		$headers .= "Reply-To: support@angkorauto.com\r\n";
		$headers .= "CC: support@angkorauto.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message .= '<html><body style="margin:0 auto; padding:0 auto;">';
		$message .= '<div style="margin:0 auto;line-height:10px;padding:0;width:100%;">
					<div style="width:650px; margin:0 auto;padding:0 auto;background:#eeeeee;">
					<h2 style="padding-top:20px;text-align:center;color:#2c65b6;">Thank you for using Angkorauto.com</h2>
					<!--<p class="pag-1">Thank you for registering with Angkorauto.</p>-->
					<p style="text-align:center;font-size:14px;">Your account ID is '.': '.$com_result.' </p>
					<div style="border-top:1px dashed #CCC;border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
					<!--<p class="pag-1">Your account will not be activated until you verify your email address.</p>-->
					<p style="text-align:center;font-size:14px; margin:30px 0;"><span style="font-size:14px;font-weight:bold;text-align:center;">Having trouble accessing your account?</span> <span>Please contact</span> 
					<span><a style="text-align:center;text-decoration:underline;color:blue;" href="mailto:support@angkorauto.com">Online Customer Support.</a></span></p>
					</div>
					<div style="border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
					<div style="float:left;padding:10px 0;"><img src="http://angkorauto.com/images/common/header/softbloom.png" /></div>
					<div style="float:left;padding:10px 0; margin-left:160px;width:75px;" >
					<table >
					<tr><td colspan="4" >Email:info@angkorauto.com</td></tr>
					<tr><td></td></tr><tr>
							<td><img src="http://angkorauto.com/images/social_icons/fb.png" /></td>
                            <td><img src="http://angkorauto.com/images/social_icons/tt.png" /></td>
                            <td><img src="http://angkorauto.com/images/social_icons/gplus.png" /></td>
                            <td><img src="http://angkorauto.com/images/social_icons/in.png" /></td>
					</tr></table>
					</div>
					<p style="clear:both"></p> 
					</div>
					<p style="text-align:center;font-size:14px;padding:5px 0 20px 0;">Copyright &copy; 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>
					</div></div>';
			
		$message .= '</body></html>';

		if(mail($emial_to, $subject, $message, $headers)){
			//$_SESSION['log_id'] = $log_id;
			//echo '<p style="color:#900000">Please check your email, the key for reseting your password already send to your email!</p>';
			header("Location: " .BASE_RELATIVE . "account-ID");
		}

		$cnx->close();
		}//end of count rows.
		else{
		     
			$this->email_not_has="Your email doesn't match in our system";
		}
	}
	
	
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
