<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/**
 * Utilities method used throughout the framework
 * <br>---------------------------------------------------------------------
 */

class Utilities
{
    /**
     * Generate slug from string
     * <br>---------------------------------------------------------------------
     * @param $string - the phrase to be filtered.
     * @param $maxLength - the max. string length.
     * @return string.
     */
    public static function genetateSlug($string, $maxLength=50)
    {
        $result = strtolower($string);
        $result = preg_replace("/[^a-z0-9\s-]/", "", $result);
        $result = trim(preg_replace("/[\s-]+/", " ", $result));
        $result = trim(substr($result, 0, $maxLength));
        $result = preg_replace("/\s/", "-", $result);

        return $result;
    }
    /* Reverse key order of array */
    public static function diverse_array($vector) {
        $result = array();
        foreach($vector as $key1 => $value1)
            foreach($value1 as $key2 => $value2)
                $result[$key2][$key1] = $value2;
        return $result;
    }
    /* Generate random string by date now */
    public static function generateStringByNow($salt=''){
        $generateStr=md5(time("now").$salt);
        return $generateStr;
    }
    /**
    * Upload any single file
    * @param $file: any element file name
    * @param $destPath: physical path on server to upload to (not included filename)
    * @param $destName: set Destination File Name as desired or it will take defaul temporary name
    * @param $allowedExts: set array of allow extension such as 'jpg', 'gif', it will not upload and return false if besides theses
    * @return true/false
    */
    public static function uploadAnyFile($file, $destPath, $destName=null, $allowedExts=array()){

        $temp = explode(".", $file["name"]);
        $extension = end($temp);
        /* Set max size here default: 10MB */
        if (($file["size"] < 10485760) && (in_array(strtolower($extension), $allowedExts) || count($allowedExts)<1)) {
            if ($file["error"] > 0) {
                return false;
            } else {
                if($destName==null) $destName=$file["name"];
                move_uploaded_file($file["tmp_name"], BASE_ROOT.$destPath . $destName);
                return true;
            }
        }else {
            return false;
        }

    }
    /**
    * Download file
    * @param $fileurl: any element file name
    * @return true/false
    */
    public static function downloadFile($fileurl, $output_file_name=''){
        $fileurl = realpath($fileurl);
        /* Take default name is not defined by user */
        if(empty($output_file_name)) $output_file_name=basename($fileurl);
        $file_extension = strtolower(substr(strrchr($fileurl,"."),1));
        switch ($file_extension) {
            case "pdf": $ctype="application/pdf"; break;
            case "exe": $ctype="application/octet-stream"; break;
            case "zip": $ctype="application/zip"; break;
            case "doc": $ctype="application/msword"; break;
            case "xls": $ctype="application/vnd.ms-excel"; break;
            case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;
            case "jpe": case "jpeg":
            case "jpg": $ctype="image/jpg"; break;
            default: $ctype="application/force-download";
        }
        if (!file_exists($fileurl)) {
            return false;
        }
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: $ctype");
        header("Content-Disposition: attachment; filename=\"".$output_file_name."\";");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".@filesize($fileurl));
        set_time_limit(0);
        ob_get_clean();
        $result= @readfile("$fileurl");
        ob_end_flush();
        return $result;
    }
    /**
     * Generate a random string based in chars and numbers.
     * <br>---------------------------------------------------------------------
     * @param $length - the length of the string.
     * @return string.
     */
    public static function generateRandomString($length=9)
    {
        require_once BASE_CLASS . 'class-connect.php';

     $cnx = new Connect();
     $cnx->open();
     $sql = @mysql_query("SELECT max(car_id) FROM `product` ") ;
     $id=@mysql_fetch_row($sql);



        $characters = array('0','1','2','3','4','5','6','7','8','9');
        $string = '';

        for( $p = 0; $p < $length; $p++ )
        {
            $string .= $characters[mt_rand(0, count($characters)-1)];

        }

        return $string ;//.dechex($id['0']);
    }
	
	public static function generateRegisterID($length=5)
    {
        require_once BASE_CLASS . 'class-connect.php';

     $cnx = new Connect();
     $cnx->open();
     $sql = @mysql_query("SELECT AUTO_INCREMENT
	 FROM information_schema.tables
	 WHERE table_name = 'register'
	 AND table_schema = DATABASE()") ;
	  
   
     $row = @mysql_fetch_assoc($sql);
	 $id = $row['AUTO_INCREMENT']; 
 
     return  $id;
    }

    /**
     * Validate email address format
     * <br>---------------------------------------------------------------------
     * @param $email - the email to be validated.
     * @return bool.
     */
    public static function checkEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    /**
     * Filter string to accept only A-Z a-z 0-9 and underscore
     * <br>---------------------------------------------------------------------
     * @param $str - the string to be filtered.
     * @return string.
     */
    public static function validateUserId($str)
    {
        return preg_match("/^[a-z\d_]{4,20}$/i", $str);
    }
    /**
     * Filter string to accept only A-Z a-z 0-9
     * <br>---------------------------------------------------------------------
     * @param $str - the string to be filtered.
     * @return string.
     */
    public static function sanitizeString($str)
    {
        return preg_replace("/[^a-zA-Z0-9]+/", "", $str);
    }

    /**
     * Filter string to accept only A-Z a-z
     * <br>---------------------------------------------------------------------
     * @param $str - the string to be filtered.
     * @return string.
     */
	public static function validateLatin($str) {
        $pat = "^[a-zA-Z]*$";
        if (ereg($pat, trim($str))) {
            return true;
        } else {
            return false;
        }
	}

    /**
     * Format date based on configuration (settings)
     * <br>---------------------------------------------------------------------
     * @param $year
     * @param $month
     * @param $day
     * @param $format (yyyymmdd,yyyyddmm,ddmmyyyy)
     * @return string / bool.
     */
    public static function checkDateFormat($year=2012, $month=01, $day=01, $format='yyyymmdd',$separator='')
    {
        if( !empty($format) && !empty($year) && !empty($month) && !empty($day) )
        {
            $date = '';
            $separator = trim($separator);

            // define separator if not declared.
            $sep = '.';

            switch($format)
            {
                case 'yyyymmdd':
                    $sep = '.'; // american format.
                    break;
                case 'yyyyddmm':
                    $sep = '.'; // american format.
                    break;
                case 'mmddyyyy':
                    $sep = '-'; // american format.
                    break;
                default:
                    $sep = '/'; // westen format.
            }

            if( !empty($separator) )
            {
                // override default settings.
                $sep = $separator;
            }
            else
            {
                $separator = $sep;
            }

            switch($format)
            {
                case 'yyyymmdd':
                    $date = $year . $separator . $month . $separator . $day;
                    break;
                case 'yyyyddmm':
                    $date = $year . $separator . $day . $separator . $month;
                    break;
                case 'mmddyyyy':
                    $date = $month . $separator . $day . $separator . $year;
                    break;
                default:
                    $date = $day . $separator . $month . $separator . $year;
            }

            return $date;
        }
        else
        {
            return false;
        }
    }

    /**
     * Public method: create imate thumbnail
     * <br>---------------------------------------------------------------------
     * NOTE: requires GDLib enabled.
     * @return bool.
     */
    public static function createThumbs( $pathToImages, $pathToThumbs, $thumbWidth )
    {
        // parse path for the extension
        $ext = explode('.',$pathToImages);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);

        // create JPG / JPEG
        if ( $ext == 'jpg' || $ext == 'jpeg' )
        {
            // load image and get image size
            if( !$img = @imagecreatefromjpeg( $pathToImages ) )
            {
                @closedir( $dir );
                return false;
            }

            $width = imagesx( $img );
            $height = imagesy( $img );

            // calculate thumbnail size
            $new_width = $thumbWidth;
            $new_height = floor( $height * ( $thumbWidth / $width ) );

            // create a new temporary image
            if( !$tmp_img = imagecreatetruecolor( $new_width, $new_height ) )
            {
                @closedir( $dir );
                return false;
            }

            // copy and resize old image into new image
            if( !@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height ) )
            {
                @closedir( $dir );
                return false;
            }

            // save thumbnail into a file
            if( !@imagejpeg( $tmp_img, $pathToThumbs ) )
            {
                @closedir( $dir );
                return false;
            }
        }

        // create GIF
        else if ( $ext == 'gif' )
        {
            // load image and get image size
            if( !$img = @imagecreatefromgif( $pathToImages ) )
            {
                @closedir( $dir );
                return false;
            }

            $width = imagesx( $img );
            $height = imagesy( $img );

            // calculate thumbnail size
            $new_width = $thumbWidth;
            $new_height = floor( $height * ( $thumbWidth / $width ) );

            // create a new temporary image
            if( !$tmp_img = imagecreatetruecolor( $new_width, $new_height ) )
            {
                @closedir( $dir );
                return false;
            }

            // copy and resize old image into new image
            if( !@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height ) )
            {
                @closedir( $dir );
                return false;
            }

            // save thumbnail into a file
            if( !@imagegif( $tmp_img, $pathToThumbs ) )
            {
                @closedir( $dir );
                return false;
            }
        }

        // create PNG
        else if ( $ext == 'png' )
        {
            // load image and get image size
            if( !$img = @imagecreatefrompng( $pathToImages ) )
            {
                @closedir( $dir );
                return false;
            }

            $width = imagesx( $img );
            $height = imagesy( $img );

            // calculate thumbnail size
            $new_width = $thumbWidth;
            $new_height = floor( $height * ( $thumbWidth / $width ) );

            // create a new temporary image
            if( !$tmp_img = imagecreatetruecolor( $new_width, $new_height ) )
            {
                @closedir( $dir );
                return false;
            }

            // set transparency
            @imagealphablending($tmp_img, false);
            @imagesavealpha($tmp_img, true);

            // copy and resize old image into new image
            if( !@imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height ) )
            {
                @closedir( $dir );
                return false;
            }

            // save thumbnail into a file
            if( !@imagepng( $tmp_img, $pathToThumbs ) )
            {
                @closedir( $dir );
                return false;
            }
        }
        else
        {
            @closedir( $dir );

            return false;
        }

        @closedir( $dir );

        return true;
    }

    /**
     * Encrypt string using key
     * <br>---------------------------------------------------------------------
     * NOTICE: windows has been reported to not work properly with mcrypt. Use
     * it at your own risk on windows based server. Requires MCRYPT library!
     * @param $key - the key to be used to encrypt the string.
     * @param $string - the string message to be encrypted.
     * @return string.
     */
    public static function encrypt($key, $string)
    {
        if( empty($key) || empty($string) )
        {
            return '';
        }

        $td = mcrypt_module_open('cast-256', '', 'ecb', '');
        $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);

        mcrypt_generic_init($td, $key, $iv);

        $encrypted_data = mcrypt_generic($td, $string);

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        $encoded_64 = base64_encode($encrypted_data);

        return $encoded_64;
    }

    /**
     * Required field
     * <br>---------------------------------------------------------------------
     * NOTE: required field
     */
    public static function Required($data) {
        $data = mb_convert_kana($data, "s");
    	return strlen(trim($data));
    }

    /**
     * Decrypt string using key
     * <br>---------------------------------------------------------------------
     * NOTE: requires MCRYPT library!
     * @param $key - the key to decript the message.
     * @return string.
     */
    public static function decrypt($key, $encoded_string)
    {
        if( empty($key) )
        {
            return '';
        }

        $decoded_64 = base64_decode($encoded_string);
        $td = mcrypt_module_open('cast-256', '', 'ecb', '');
        $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);

        mcrypt_generic_init($td, $key, $iv);

        $decrypted_data = mdecrypt_generic($td, $decoded_64);

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return $decrypted_data;
    }

    /**
     * Convert pixels to millimeters
     * <br>---------------------------------------------------------------------
     * NOTE: the minimum DPI count is 25.4 and minimum pixels is 50.
     * @param $pixels - the number to be converted.
     * @param $dpi  - the DPI multiplier.
     * @return number.
     */
    public static function convertPixelsToMillimeters($pixels, $dpi=72)
    {
        // validate for minimum DPI and pixels count.
        if( $pixels < 50 )
        {
            return 0;
        }

        if( $dpi < 25.4 )
        {
            $dpi = 25.4;
        }

        // calculate multiplier.
        $dpi = $dpi / 25.4;

        $pixels = $pixels / $dpi;

        return $pixels;
    }

    /**
     * Convert pixels to inches
     * <br>---------------------------------------------------------------------
     * NOTE: the minimum DPI count is 25.4 and minimum pixels is 50.
     * @param $pixels - the number to be converted.
     * @param $dpi  - the DPI multiplier.
     * @return number.
     */
    public static function convertPixelsToInches($pixels, $dpi=72)
    {
        // validate for minimum DPI and pixels count.
        if( $pixels < 50 )
        {
            return 0;
        }

        if( $dpi < 25.4 )
        {
            $dpi = 25.4;
        }

        // calculate multiplier.
        $dpi = $dpi / 25.4; // 25.4 is exactly 1 inch which is what Adobe PS uses.

        $mm = $pixels / $dpi;
        $in = $mm * 0.0393701;

        return $in;
    }

    /**
     * Convert millimeter to inches
     * <br>---------------------------------------------------------------------
     * @param $mm - the millimeter count.
     * @return number.
     */
    public static function convertMillimeterToInches($mm)
    {
        // validate.
        if( !is_numeric($mm) )
        {
            return 0;
        }

        return $mm * 0.0393701;
    }

    /**
     * Convert millimeter to pixels
     * <br>---------------------------------------------------------------------
     * NOTE: the minimum DPI count is 25.4 and minimum pixels is 50.
     * @param $pixels - the number to be converted.
     * @param $dpi  - the DPI multiplier.
     * @return number.
     */
    public static function convertMillimeterToPixels($mm, $dpi)
    {
        // validate.
        if( !is_numeric($mm) || $mm < 1)
        {
            return 0;
        }

        if( $dpi < 25.4 )
        {
            $dpi = 25.4;
        }

        // set multiplier.
        $dpi = $dpi / 25.4;

        return $dpi * $mm;
    }

    /**
     * Convert inches to millimeter
     * <br>---------------------------------------------------------------------
     * @param $in - the inch size to be converted.
     * @return number.
     */
    public static function convertInchesToMillimeters($in)
    {
        // validate.
        if( !is_numeric($in) || $in < 0 )
        {
            return 0;
        }

        return $in / 0.0393701;
    }

    /**
     * Format price
     * <br>---------------------------------------------------------------------
     * @param $price The price to be formated.
     * @param $currency_code The currency code which is used as base for convertion.
     * @return string The formated price.
     */
    public static function formatPrice($price, $currency_code){
        // it assumes the price has been saved using USD format.
        if( $currency_code == 'USD' ){
            $thousand = ',';
            $hundred  = '.';
        }
        else {
            $thousand = '.';
            $hundred  = ',';
        }

        $result = sprintf('%0.2f', $price);
        $result = number_format($result,2,$hundred,$thousand);

        return $result;
    }

    /**
     * Remove directory recursively
     * <br>---------------------------------------------------------------------
     * @param $directory The directory to be removed.
     * @param $empty Remove the target directory. If set to false, only the
     * content<br> within the directory is removed living the directory itself
     * untouched. Default: TRUE
     * @return bool
     */
    public static function remove_directory($directory, $empty=TRUE){
        // if the path has a slash at the end we remove it here
        if(substr($directory,-1) == '/'){
            $directory = substr($directory,0,-1);
        }

        // if the path is not valid or is not a directory ...
        if(!file_exists($directory) || !is_dir($directory)){
            // ... we return false and exit the function
            return FALSE;

        // ... if the path is not readable
        }
        elseif(!is_readable($directory)){
            // ... we return false and exit the function
            return FALSE;

        // ... else if the path is readable
        }
        else{
            // we open the directory
            $handle = opendir($directory);

            // and scan through the items inside
            while (FALSE !== ($item = readdir($handle))){
                // if the filepointer is not the current directory
                // or the parent directory
                if($item != '.' && $item != '..'){
                    // we build the new path to delete
                    $path = $directory.'/'.$item;

                    // if the new path is a directory
                    if(is_dir($path)){
                        // we call this function with the new path
                        Utilities::remove_directory($path);

                    // if the new path is a file
                    }
                    else{
                        // we remove the file
                        @unlink($path);
                    }
                }
            }

            // close the directory
            closedir($handle);

            // if the option to empty is not set to true
            if($empty){
                // try to delete the now empty directory
                if(!rmdir($directory)){
                    // return false if not possible
                    return FALSE;
                }
            }

            // return success
            return TRUE;
        }
    }

    /**
     * Resize image with pre-fixed size.
     * <br>---------------------------------------------------------------------
     * @param $image_path The path to the image to be resized.
     * @param $width The desired with
     * @param $height The desired height
     * @return boolean
     */
    public static function resizeImageWithFixSize($image_path, $width, $height){
        // validate
        if( !file_exists($image_path) || $width < 1 || $height < 1 ){
            return false;
        }

        // get extension.
        $ext = explode('.',$image_path);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);

        switch($ext){
            case 'jpg':
                $img = imagecreatefromjpeg($image_path);
                break;
            case 'gif':
                $img = imagecreatefromgif($image_path);
                break;
            case 'png':
                $img = imagecreatefrompng($image_path);
                break;
        }

        // define.
        $box_w = $width;
        $box_h = $height;

        //create the image, of the required size
        $new = imagecreatetruecolor($box_w, $box_h);
        if($new === false) {
            //creation failed -- probably not enough memory
            return null;
        }


        //Fill the image with a light grey color
        //(this will be visible in the padding around the image,
        //if the aspect ratios of the image and the thumbnail do not match)
        //Replace this with any color you want, or comment it out for black.
        //I used grey for testing =)
        $fill = imagecolorallocate($new, 255, 255, 255);
        imagefill($new, 0, 0, $fill);

        //compute resize ratio
        $hratio = $box_h / imagesy($img);
        $wratio = $box_w / imagesx($img);
        $ratio = min($hratio, $wratio);

        //if the source is smaller than the thumbnail size,
        //don't resize -- add a margin instead
        //(that is, dont magnify images)
        if($ratio > 1.0)
            $ratio = 1.0;

        //compute sizes
        $sy = floor(imagesy($img) * $ratio);
        $sx = floor(imagesx($img) * $ratio);

        //compute margins
        //Using these margins centers the image in the thumbnail.
        //If you always want the image to the top left,
        //set both of these to 0
        $m_y = floor(($box_h - $sy) / 2);
        $m_x = floor(($box_w - $sx) / 2);

        //Copy the image data, and resample
        //
        //If you want a fast and ugly thumbnail,
        //replace imagecopyresampled with imagecopyresized
        if(!imagecopyresampled($new, $img,
            $m_x, $m_y, //dest x, y (margins)
            0, 0, //src x, y (0,0 means top left)
            $sx, $sy,//dest w, h (resample to this size (computed above)
            imagesx($img), imagesy($img)) //src w, h (the full size of the original)
        ) {
            //copy failed
            imagedestroy($new);
            return false;
        }

        //copy successful. replace original image.
        switch($ext){
            case 'jpg':
                imagejpeg($new,$image_path,100);
                break;
            case 'gif':
                imagegif($new,$image_path);
                break;
            case 'png':
                imagepng($new,$image_path);
                break;
        }

        return true;
    }
}// end class.
