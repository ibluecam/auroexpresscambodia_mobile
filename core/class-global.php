<?php
if(!isset($_SESSION)) @session_start();
require_once 'config.php';

class classglobal
{       
    protected $lang;
    protected $group = 'public';
    protected $slug = 'home';
    public $buttonAdvertise;    
    public $spare_data;

    //copyright
    public $copyright;

    //Contact
    public $contact;

    /*
    * Constructor -------------------------------------------------------------
    */
    public function __construct(){          
        $cnx = new Connect();
        $cnx->open();       
        global $total_num_row;
        global $total_used;
        global $total_usedSold;
        global $total_usedReserved;
        global $total_new;
        global $total_newSold;
        global $total_newReserved;
        
        $i=1;        
        $sql_total="SELECT ap.owner, SUM(ap.qty) AS total_count, 
                            rg.company_name, rg.user_id                                                     
                            FROM active_product AS ap                            
                            INNER JOIN register as rg on ap.owner=rg.user_id
                            INNER JOIN tbl_carfinder_sellers as cf_sellers on (ap.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                            ";
        $sql_used="SELECT ap_used.owner, SUM(ap_used.qty) AS total_used, 
                            rg.company_name, rg.user_id                                                     
                            FROM active_product AS ap_used                            
                            INNER JOIN register as rg on ap_used.owner=rg.user_id
                            INNER JOIN tbl_carfinder_sellers as cf_sellers on (ap_used.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                            WHERE ap_used.car_type='0'
                            ";              
        $sql_usedSold="SELECT ap_used.owner, SUM(ap_used.count_sold) AS total_usedSold, 
                            rg.company_name, rg.user_id                                                     
                            FROM active_product AS ap_used                            
                            INNER JOIN register as rg on ap_used.owner=rg.user_id
                            INNER JOIN tbl_carfinder_sellers as cf_sellers on (ap_used.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                            WHERE ap_used.car_type='0' AND count_sold>'0'
                            ";      
        $sql_usedReserved="SELECT ap_used.owner, SUM(ap_used.count_reserved) AS total_usedReserved,
                            rg.company_name, rg.user_id                                                     
                            FROM active_product AS ap_used                            
                            INNER JOIN register as rg on ap_used.owner=rg.user_id
                            INNER JOIN tbl_carfinder_sellers as cf_sellers on (ap_used.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                            WHERE ap_used.car_type='0' AND count_reserved>'0'
                            ";
        
        $sql_new="SELECT ap_new.owner, SUM(ap_new.qty) AS total_new,
                            rg.company_name, rg.user_id                                                     
                            FROM active_product AS ap_new                            
                            INNER JOIN register as rg on ap_new.owner=rg.user_id
                            INNER JOIN tbl_carfinder_sellers as cf_sellers on (ap_new.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                            WHERE ap_new.car_type='1'
                            ";
        $sql_newSold="SELECT ap_new.owner, SUM(ap_new.count_sold) AS total_newSold, 
                            rg.company_name, rg.user_id                                                     
                            FROM active_product AS ap_new                            
                            INNER JOIN register as rg on ap_new.owner=rg.user_id
                            INNER JOIN tbl_carfinder_sellers as cf_sellers on (ap_new.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                            WHERE ap_new.car_type='1' AND count_sold>'0'
                            ";
        $sql_newReserved="SELECT ap_new.owner, SUM(ap_new.count_reserved) AS total_newReserved, 
                            rg.company_name, rg.user_id                                                     
                            FROM active_product AS ap_new                            
                            INNER JOIN register as rg on ap_new.owner=rg.user_id
                            INNER JOIN tbl_carfinder_sellers as cf_sellers on (ap_new.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
                            WHERE ap_new.car_type='1' AND count_reserved>'0'
                            ";                      
        /* (SELECT COUNT(ap_used.qty) FROM active_product AS ap_used WHERE ap_used.car_type='0' AND ap_used.owner=rg.user_id) 
                            AS total_used,
                            (SELECT COUNT(ap_used.owner) FROM active_product AS ap_used WHERE car_type='0' AND `status`='Sold' AND ap_used.owner=rg.user_id) 
                            AS total_usedSold,
                            (SELECT COUNT(ap_used.owner) FROM active_product AS ap_used WHERE car_type='0' AND `status`='Reserved' AND ap_used.owner=rg.user_id) 
                            AS total_usedReserved,
                            
                            (SELECT COUNT(ap_new.owner) FROM active_product AS ap_new WHERE ap_new.car_type='1' AND ap_new.owner=rg.user_id) 
                            AS total_new,
                            (SELECT COUNT(ap_new.owner) FROM active_product AS ap_new WHERE ap_new.car_type='1' AND `status`='Sold' AND ap_new.owner=rg.user_id) 
                            AS total_newSold,
                            (SELECT COUNT(ap_new.owner) FROM active_product AS ap_new WHERE ap_new.car_type='1' AND `status`='Reserved' AND ap_new.owner=rg.user_id) 
                            AS total_newReserved             */     
                    
        
        // sql_total
        if( !$sql_total = @mysql_query($sql_total) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }               
        
        if( @mysql_num_rows($sql_total) < 1 ){
            $cnx->close();
        }else {         
            while( $r = @mysql_fetch_assoc($sql_total) ){               
                $total_num_row=$r['total_count']+$total_num_row;    
            }               
        }   

        // sql_used
        if( !$sql_used = @mysql_query($sql_used) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql_used) < 1 ){
            $cnx->close();
        }else {         
            while( $r = @mysql_fetch_assoc($sql_used) ){
                $total_used=$r['total_used']+$total_used;               
            }               
        }   
        
        // sql_usedSold
        if( !$sql_usedSold = @mysql_query($sql_usedSold) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql_usedSold) < 1 ){
            $cnx->close();
        }else {         
            while( $r = @mysql_fetch_assoc($sql_usedSold) ){                
                $total_usedSold=$r['total_usedSold']+$total_usedSold;               
            }               
        }   
        
        // sql_usedReserved
        if( !$sql_usedReserved = @mysql_query($sql_usedReserved) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql_usedReserved) < 1 ){
            $cnx->close();
        }else {         
            while( $r = @mysql_fetch_assoc($sql_usedReserved) ){                
                $total_usedReserved=$r['total_usedReserved']+$total_usedReserved;               
            }               
        }
        
        // sql_new
        if( !$sql_new = @mysql_query($sql_new) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql_new) < 1 ){
            $cnx->close();
        }else {         
            while( $r = @mysql_fetch_assoc($sql_new) ){                             
                $total_new=$r['total_new']+$total_new;              
            }               
        }
        
        // sql_newSold
        if( !$sql_newSold = @mysql_query($sql_newSold) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql_newSold) < 1 ){
            $cnx->close();
        }else {         
            while( $r = @mysql_fetch_assoc($sql_newSold) ){         
                $total_newSold=$r['total_newSold']+$total_newSold;              
            }               
        }
        
        // sql_newReserved
        if( !$sql_newReserved = @mysql_query($sql_newReserved) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load Recent Product list due to a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql_newReserved) < 1 ){
            $cnx->close();
        }else {         
            while( $r = @mysql_fetch_assoc($sql_newReserved) ){             
                $total_newReserved=$r['total_newReserved']+$total_newReserved;              
            }               
        }
        
        @mysql_free_result($sql_total);
        @mysql_free_result($sql_used);
        @mysql_free_result($sql_usedSold);
        @mysql_free_result($sql_usedReserved);
        @mysql_free_result($sql_new);
        @mysql_free_result($sql_newSold);
        @mysql_free_result($sql_newReserved);
        $cnx->close();
        // end sql
        
        // load function
        $this->loadbuttomAdver();       
        $this->loadSpareList();
        $this->loadCopyright();
    }   
    
    // Advertising
    public function loadbuttomAdver(){                      
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        
        global $start;
        
        // get page     
        $array_url=array();
        $current_url = strtok($_SERVER["REQUEST_URI"],'?');
        $array_url = (explode("/",$current_url,3));             
        
        if($_SERVER['SERVER_NAME']=='localhost'){
            $array_url = $array_url[2];
            $current_page = current(explode("/",$array_url));           
        }else{
            $array_url = $array_url[1];
            $current_page = current(explode("/",$array_url));           
        }

        if($current_page==""){
            $current_page='home';
        }
        
        $totalsql = "select count(promotion_id) as count from tb_promotion where status='1' and promo_type='4' and (promo_page='undefined' or promo_page='$current_page') ";
        $totalquery = @mysql_query($totalsql);
        if (@mysql_num_rows($totalquery)>0){
            
            $newrow = @mysql_fetch_assoc($totalquery);
            $total = $newrow['count'];
            }
        else {
            $this->buttonAdvertise= "";
            return ;
            }       
        
        if ($total<4){
            $myinfo = array();
        $sql = "select * from tb_promotion where status='1' and promo_type='4' and (promo_page='undefined' or promo_page='$current_page') ";
        $query = @mysql_query($sql);
            while($row = @mysql_fetch_assoc($query)){
                
                array_push( $myinfo, $row);
                
                }
            $this->buttonAdvertise= $myinfo ;
            
            
            }
        else{
        
        $myinfo = array();
        
        $sql = "select * from tb_promotion where status='1' and promo_type='4' and (promo_page='undefined' or promo_page='$current_page') ORDER BY RAND() ";
        $query = @mysql_query($sql);
            while($row = @mysql_fetch_assoc($query)){
                
                array_push( $myinfo, $row);
                
                }
            $this->buttonAdvertise= $myinfo ;
            
            }
        
        
    
        $cnx->close();              
    }           

    // get promotion scroll text
    private function loadSpareList(){
        
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $sql = "select * from tb_sparepart where status='0' ";          
        
        $query = @mysql_query($sql);
        if (@mysql_num_rows($query)<1) {
            $cnx->close();
            
            
            return ;
            
            }
        $promotion = array();
        while ($r = @mysql_fetch_assoc($query)){
            
            array_push($promotion, $r); 
            
            }
            $cnx->close();
        $this->spare_data = $promotion;
       
    }

    // copyright
    private function loadCopyright(){
        
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $sql_copyright = "SELECT * FROM copyright";         
        
        $query_copyright = @mysql_query($sql_copyright);
        if (@mysql_num_rows($query_copyright)<1) {
            $cnx->close();
            
            
            return ;
            
            }
        $copyright_text = array();
        while ($r = @mysql_fetch_assoc($query_copyright)){
            
            array_push($copyright_text, $r);    
            
            }
            $cnx->close();
        $this->copyright = $copyright_text;
       
    }

    
    /*
     * Public Function -------------------------------------------------------------
     */
    
    // get advertise
    public function showbuttonAdvertise(){
        return $this->buttonAdvertise;
    }
    
    // get promotion scroll text
    public function getSpareData(){
        return $this->spare_data;
    }

    // Copyright
    public function getCopyright(){
        return $this->copyright;
    }
    
}