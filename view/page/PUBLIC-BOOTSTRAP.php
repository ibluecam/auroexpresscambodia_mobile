<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <!-- scroll to top button -->
            <div id="top-page" onclick="javascript: $('html, body').animate({scrollTop: '0px'}, 300);"></div>
            <!-- end scroll to top button -->
            <div id="inner-content">
                <!-- START CONTENT -->
                <H1>H1 Heading</H1>
                <h2>H2 Heading</h2>
                <h3>H3 Heading</h3>
                <h4>H4 Heading</h4>
                <h5>H5 Heading</h5>
                <h6>H6 Heading</h6>
                <hr>
                <p>This is a <strong>bold/strong</strong> text.</p>
                <p>This is a <em>italic</em> text.</p>
                <p>This is a <small>small</small> text tag.</p>
                <hr>
                <h1>Emphasis</h1>
                <p class="muted">This is the <strong>muted</strong> class.</p>
                <p class="text-warning">This is the <strong>text-warning</strong> class.</p>
                <p class="text-error">This is the <strong>text-error</strong> class.</p>
                <p class="text-info">This is the <strong>text-info</strong> class.</p>
                <p class="text-success">This is the <strong>text-success</strong> class.</p>
                <hr>
                <h1>Abbreviations</h1>
                <abbr title="abbreviation attribute">Abr. attr</abbr>
                <hr>
                <h1>Addresses</h1>
                <address>
                    <strong>Company name</strong><br/>
                    795 Folsom Ave, Suite 600<br />
                    San Francisco, CA 94107<br/>
                    (123) 456-7890                    
                </address>
                <h1>Block quotes</h1>
                <blockquote>
                    <p>THis is a blockquote left align.</p>
                </blockquote>
                <blockquote class="pull-right">
                    <p>This is a blockquote tag right align.</p>
                </blockquote>
                <hr>
                <h1>Lists</h1>
                <ul>
                    <li>Unordered list</li>
                </ul>
                <ol>
                    <li>Ordered list.</li>
                </ol>
                <ul class="unstyled">
                    <li>Unordered list unstyled.</li>
                </ul>
                <hr>
                <h1>Description</h1>
                <dl>
                    <dt>Description list title.</dt>
                    <dd>Description list description.</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Horizontal description list title.</dt>
                    <dd>Horizontal description</dd>
                </dl>
                <hr>
                <h1>Code</h1>
                <code>This is a code block.</code>
                <hr>
                <h1>Basic block</h1>
                <pre>&lt;p&gt;Sample text&lt;/p&gt;</pre>
                <pre class="pre-scrollable">&lt;p&gt;Sample text&lt;/p&gt;<br> with scroll bar and 350px height.</pre>
                <hr>
                <h1>Tables</h1>
                <p>Let's keep it away from here ;)</p>
                <hr>
                <h1>Forms</h1>
                <form>
                    <h2>Simple form layout</h2>
                    <legend>Legend</legend>
                    <label>Label</label>
                    <input type="text" placeholder="Type something...."/>
                    <span class="help-block">Example help-block class</span>
                    <label class="checkbox"> Check me out
                        <input type="checkbox" />
                    </label>
                    <input type="submit" class="btn" value="submit" />
                </form>
                <hr>
                <h2>Search form layout</h2>
                <form class="form-search">
                    <input type="text" class="input-medium search-query"/>
                    <input type="submit" class="btn" value="search" />
                </form>
                
                <form class="form-search">
                    <div class="input-append">
                        <input class="span2 search-query" type="text" />
                        <input class="btn" type="submit" value="Go" />
                    </div>
                </form>
                <hr>
                <h2>Horizontal form</h2>
                <form class="form-horizontal">
                    
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Email</label>
                        <div class="controls">
                            <input type="text" id="inputEmail" placeholder="Email" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputPassword">Password</label>
                        <div class="controls">
                            <input type="password" id="inputPassword" placeholder="Password" />
                        </div>
                    </div>
                </form>
                <hr>
                <h2>Select</h2>
                <select>
                    <option>1</option>
                    <option>1</option>
                    <option>1</option>
                    <option>1</option>
                    <option>1</option>
                </select>
                <hr>
                <h1>Extending form elements</h1>
                <div class="input-prepend">
                    <span class="add-on">@</span>
                    <input type="text" class="span2" id="prependedInput" size="16" placeholder="Username" />
                </div>
                <div class="input-append">
                    <input type="text" class="span2" id="appendInput" size="16" />
                    <span class="add-on">.00</span>
                </div>
                <p><strong>Combined</strong></p>
                <div class="input-prepend input-append">
                    <span class="add-on">$</span>
                    <input type="text" class="span2" id="appendedPrependedInput" size="16"/>
                    <span class="add-on">.00</span>
                </div>
                <hr>
                <p>For more samples of how to use bootstrap, see README.txt.</p>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
