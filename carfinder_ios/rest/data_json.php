<?php

  require '../controllers/ControllerRest.php';
  $controllerRest = new ControllerRest('../application/DB_Connect.php');

  $resultPhotoCar = $controllerRest->getPhotoCarsResult();
  $resultPhotoDealers = $controllerRest->getPhotoDealersResult();
  $resultMake = $controllerRest->getMakeResult();
  $resultSellers = $controllerRest->getSellersResult();
  $resultCars = $controllerRest->getCarsResult();
  $resultDealers = $controllerRest->getDealersResult();

  

  header ("content-type: text/json");
  // header("Content-Type: application/text; charset=ISO-8859-1");
  echo "{";

            // PHOTO CARS
            echo "\"photo_cars\" : [";
            $no_of_rows = $resultPhotoCar->rowCount();
            $ind = 0;
            $count = $resultPhotoCar->columnCount();
            foreach ($resultPhotoCar as $row) 
            {
                echo "{";
                $inner_ind = 0;
                foreach ($row as $columnName => $field) 
                {

                    $val = trim(strip_tags($field));
                    if(!is_numeric($columnName)) {
                        echo "\"$columnName\" : \"$val\"";

                        if($inner_ind < $count - 1)
                          echo ",";

                        ++$inner_ind;
                    }
                }
                echo "}";

                if($ind < $no_of_rows - 1)
                  echo ",";

                ++$ind;
            }
            echo "], ";

            // PHOTO DEALERS
            echo "\"photo_dealers\" : [";
            $no_of_rows = $resultPhotoDealers->rowCount();
            $ind = 0;
            $count = $resultPhotoDealers->columnCount();
            foreach ($resultPhotoDealers as $row) 
            {
                echo "{";
                $inner_ind = 0;
                foreach ($row as $columnName => $field) 
                {

                    $val = trim(strip_tags($field));
                    if(!is_numeric($columnName)) {
                        echo "\"$columnName\" : \"$val\"";

                        if($inner_ind < $count - 1)
                          echo ",";

                        ++$inner_ind;
                    }
                }
                echo "}";

                if($ind < $no_of_rows - 1)
                  echo ",";

                ++$ind;
            }
            echo "], ";


            // MAKE
            echo "\"make\" : [";
            $no_of_rows = $resultMake->rowCount();
            $ind = 0;
            $count = $resultMake->columnCount();
            foreach ($resultMake as $row) 
            {
                echo "{";
                $inner_ind = 0;
                foreach ($row as $columnName => $field) 
                {

                    $val = trim(strip_tags($field));
                    if(!is_numeric($columnName)) {
                        echo "\"$columnName\" : \"$val\"";

                        if($inner_ind < $count - 1)
                          echo ",";

                        ++$inner_ind;
                    }
                }
                echo "}";

                if($ind < $no_of_rows - 1)
                  echo ",";

                ++$ind;
            }
            echo "], ";


            // SELLERS
            echo "\"sellers\" : [";
            $no_of_rows = $resultSellers->rowCount();
            $ind = 0;
            $count = $resultSellers->columnCount();
            foreach ($resultSellers as $row) 
            {
                echo "{";
                $inner_ind = 0;
                foreach ($row as $columnName => $field) 
                {

                    $val = trim(strip_tags($field));
                    if(!is_numeric($columnName)) {
                        echo "\"$columnName\" : \"$val\"";

                        if($inner_ind < $count - 1)
                          echo ",";

                        ++$inner_ind;
                    }
                }
                echo "}";

                if($ind < $no_of_rows - 1)
                  echo ",";

                ++$ind;
            }
            echo "], ";


            // CARS
            echo "\"cars\" : [";
            $no_of_rows = $resultCars->rowCount();
            $ind = 0;
            $count = $resultCars->columnCount();
            foreach ($resultCars as $row) 
            {
                echo "{";
                $inner_ind = 0;
                foreach ($row as $columnName => $field) 
                {

                    $val = trim(strip_tags($field));
                    if(!is_numeric($columnName)) {
                        echo "\"$columnName\" : \"$val\"";

                        if($inner_ind < $count - 1)
                          echo ",";

                        ++$inner_ind;
                    }
                }
                echo "}";

                if($ind < $no_of_rows - 1)
                  echo ",";

                ++$ind;
            }
            echo "], ";


            // DEALERS
            echo "\"dealers\" : [";
            $no_of_rows = $resultDealers->rowCount();
            $ind = 0;
            $count = $resultDealers->columnCount();
            foreach ($resultDealers as $row) 
            {
                echo "{";
                $inner_ind = 0;
                foreach ($row as $columnName => $field) 
                {

                    $val = trim(strip_tags($field));
                    if(!is_numeric($columnName)) {
                        echo "\"$columnName\" : \"$val\"";

                        if($inner_ind < $count - 1)
                          echo ",";

                        ++$inner_ind;
                    }
                }
                echo "}";

                if($ind < $no_of_rows - 1)
                  echo ",";

                ++$ind;
            }
            echo "]";
       
  echo "}";
?>