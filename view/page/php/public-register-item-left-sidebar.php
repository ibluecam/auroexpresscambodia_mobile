<div class="left-sidebar-wrapper">
    <div class="nav-wrapper"><a href="#" class="linkfade">Register Item</a> &gt; <span>Place an AD</span></div>
    <div class="left-menu-wrapper">
        <ul class="left-menu">
             <li><a href="place-ads">Place and AD</a></li>
            <li><a href="membership-guide">Membership Guide</a></li>
            <li><a href="edit-car">Register Car</a></li>
            <li><a href="edit-truck">Register Truck</a></li>
            <li><a href="edit-bus">Register Bus</a></li>
            <li><a href="edit-equipment">Rgister Heavy Machine</a></li>
            <li><a href="edit-part">Register Part</a></li>
            <li><a href="edit-motorbike">Register Motorbike</a></li>
            <li><a href="edit-aircraft">Register Aircraft</a></li>
            <li><a href="edit-watercraft">Register Watercraft</a></li>
            
        </ul>
    </div>
    <div class="left-info-wrapper">
        <div class="info-title">Contact</div>
        
        <table>
            <tr>
                <td id="com-title" colspan="3">BLAUDA CO., LTD.</td>
                
            </tr>
            <tr>
                <td>ADDRESS</td><td>:</td>
                <td>M-2602, 32, Songdo Gwahak-ro, Yeonsu-gu, Incheon, Korea.</td>
            </tr>
            <tr>
                <td>TEL</td><td>:</td>
                <td>+82-32-715-7098</td>
            </tr>
            <tr>
                <td>FAX</td><td>:</td>
                <td>+82-32-712-4885</td>
            </tr>
            <tr>
                <td>WEBSITE</td><td>:</td>
                <td><a href="http://www.blauda.com">www.blauda.com</a></td>
            </tr>
        </table>
    </div>
</div>