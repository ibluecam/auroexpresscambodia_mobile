<?php
if(!isset($_SESSION)) @session_start();
define('LOCATION_ROOT', dirname(dirname(dirname(dirname(__FILE__)))) . '/');
require_once LOCATION_ROOT . 'config.php';
require_once BASE_CLASS . 'class-connect.php';
require_once BASE_CLASS . 'class-file-system.php';
include_once("resize-class.php");


function loadThumbImage()
{
    $cnx = new Connect();
    $cnx->open();
    $user_id = $_SESSION['log_id'];

    if(!$result = mysql_query("SELECT * FROM more_image WHERE `owner` = {$user_id}"))
    {
        require_once BASE_CLASS . 'class-log.php';
        LogReport::write('Unable to save image for more_image table due a query error at ' . __FILE__ . ':' . __LINE__);
    }

    $images = array();
    while($r = mysql_fetch_assoc($result))
    {
        array_push($images, $r);
    }
    $cnx->close();

    return $images;
}

// Delete Image.
function deleteImage()
{
    if(isset($_POST['select_img']))
    {
        $cnx = new Connect();
        $cnx->open();
        $user_id = $_SESSION['log_id'];

        foreach($_POST['select_img'] as $file_id)
        {
            if($result = mysql_query("SELECT * FROM more_image WHERE `id` = {$file_id} AND `owner` = {$user_id}"))
            {
                if(mysql_num_rows($result) > 0)
                {
                    $img_info = mysql_fetch_assoc($result);
                    if(mysql_query("DELETE FROM more_image WHERE `id` = {$file_id} AND `owner` = {$user_id}")){
                        if(file_exists(LOCATION_ROOT . $img_info['m_image'])){
                            unlink(LOCATION_ROOT . $img_info['m_image']);
                        }
                        if(file_exists(LOCATION_ROOT . $img_info['m_thumb'])){
                            unlink(LOCATION_ROOT . $img_info['m_thumb']);
                        }
                    }
                }
            }
        }

        $cnx->close();
    }
}

// File upload.
function uploadImage()
{
    if(isset($_FILES['files']))
    {
        $files = $_FILES['files'];

        foreach ($files['name'] as $key => $file_name)
        {
            if($files['error'][$key] > 0)
            {
                continue;
            }

            $m_image = 'upload/myinfo/';
            $m_thumb = 'upload/thumb/';

            // Upload image.
            $src_img_upload = LOCATION_ROOT . $m_image;
            FileSystem::createDir($src_img_upload);

            $file_name =  explode('.', $file_name);
            $extension = end($file_name);
            array_pop($file_name);
            $file_name = md5(implode('.', $file_name) . time());
            $src_img_upload .= "{$file_name}.{$extension}";

            if(!move_uploaded_file($files['tmp_name'][$key], $src_img_upload))
            {
                continue;
            }

            // Create thumb image.
            $src_thumb_upload = LOCATION_ROOT . $m_thumb;
            FileSystem::createDir($src_thumb_upload);
            $src_thumb_upload .= "{$file_name}.{$extension}";

            $thumb_img = new resize($src_img_upload);
            $thumb_img->resizeImage(160, 116, 'crop');
            $thumb_img->saveImage($src_thumb_upload, 100);

            // Insert into Database.
            $cnx = new Connect();
            $cnx->open();
            $user_id = $_SESSION['log_id'];

            if(!mysql_query("INSERT INTO more_image (`owner`, `m_thumb`, `m_image`)
                                            VALUES ({$user_id}, '{$m_thumb}{$file_name}.{$extension}', '{$m_image}{$file_name}.{$extension}')"))
            {
                require_once BASE_CLASS . 'class-log.php';
                LogReport::write('Unable to save image for more_image table due a query error at ' . __FILE__ . ':' . __LINE__);
                $form_error = true;
                $form_message = $_LANG['NEWCAR_UPLOAD_QUERY_ERROR'];

                // Remove Image file
                unlink($src_img_upload);
                unlink($src_thumb_upload);
            }

            $cnx->close();
        }
    }
}

// Do operation.
if(isset($_SESSION['log_id']))
{
    deleteImage();
    uploadImage();
    $imagelist = loadThumbImage();
}
?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>File uploader</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../bootstrap/css/bootstrap-responsive.min.css">
        <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
        <script src="../jquery/jquery.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script>
        $(function()
        {
            var win = $(window),
                pic_wrapper_select = $('#picturewrap_select');
            win.bind(
                'resize',
                function()
                {
                    if(pic_wrapper_select.outerHeight() >= 400){
                        pic_wrapper_select.css('overflow-y', 'scroll');
                        $('.pic_select').css({width : '118px', height : '83px'});
                        $('.check_box').css({top: '-26px', left: '100px'});
                    }

                    $(this)[0].frameElement.style.height = $(document.body).height() + 'px';
                }
            ).trigger('resize');
        });
        </script>

        <style>
        body{
            padding: 0;
        }
        #file_detailwrap{
            width: 100%;
            height: 30px;
            margin-top: 20px;
        }
        #file_detailwrap input{
            margin-left: 5px;
            float: right;
        }
        .pic_select{
            width: 122px;
            height: 88px;
            border: 1px solid #CCC;
            margin: 10px 10px 0px 0px;
            position: relative;
            float: left;
            overflow: hidden;
        }
        #picturewrap_select{
            max-height: 400px;
            overflow-x: hidden;
        }
        .check_box {
            position: relative;
            top: -26px;
            left: 104px;
        }
        .hidden{
            opacity: 0
        }
        </style>
    </head>
    <body>
            <!-- The file upload form used as target for the file upload widget -->
            <form id="form_upload_images" method="POST"  enctype="multipart/form-data" style="margin:0;" >
                <div id="picturewrap_select">
                    <div class="clearfix" style="height: auto;">
                        <?php
                        if(isset($imagelist) && $imagelist != null):
                            foreach($imagelist as $image): ?>
                            <div class="pic_select" style="margin:4px;">
                                <img src="<?php echo (file_exists(LOCATION_ROOT . $image['m_thumb']) ? "../../../{$image['m_thumb']}" : '../../../images/no_img/no_img_160x116.png'); ?>" />
                                <div class="check_box hidden">
                                   <input type="checkbox" name="select_img[]" id="checkbox_input" value="<?php echo $image['id']; ?>" />
                                   <label for="checkbox_input"></label>
                               </div>
                            </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p id="no_image_upload" style="text-align: center; margin: 14px 0;">* No any uploaded images.</p>
                            <style>
                            #select-all-pic{
                                display: none;
                            }
                            </style>
                        <?php endif; ?>
                    </div>
                </div>
                <div id="select-all-pic" style="padding: 5px 0 0 5px; float: left;">
                    <input type="checkbox"  style="margin: 0;"/>
                    <label for="select-all-pic" style="display: inline-block; margin: 0;">Select all</label>
                </div>
                <div id="file_detailwrap">
                    <input type="submit" name="btn_delete_images" id="btn_delete_images" class="btn btn-success file_upload" value="Delete">
                    <input type="file" name="files[]" id="image-upload-choosing" style="display: none;" multiple/>
                    <input type="button" class="btn btn-success file_upload" id="btn-image-upload" value="Upload" onclick=""/>
                </div>
            </form>
        <script>
        $('#btn-image-upload').on('click',function(){
            $('#image-upload-choosing').click();
        });

        $('#btn_delete_images').on('click',function(e){
            $('#form_upload_images').find('input[type=checkbox]').each(function(index, element){
                if(element.checked){
                    $('#form_upload_images').submit();
                }
            });

            e.preventDefault();
        });

        $('#image-upload-choosing').on('change',function(){
            $('#form_upload_images').submit();
        });

        $('.pic_select').on('mouseover', function(){
             $(this).find('.check_box').removeClass('hidden');
        });
        $('.pic_select').on('mouseout', function(){
            var element = $(this).find('.check_box input[type=checkbox]')[0];

            if(element.checked){
                return;
            }

            $(this).find('.check_box').addClass('hidden');
        });

        $('#select-all-pic input[type=checkbox]').on('click', function(){
            if(this.checked){
                $('.check_box input[type=checkbox]').each(function(){
                    this.checked = true;
                });

                $('.check_box').removeClass('hidden');
            }
            else{
                $('.check_box input[type=checkbox]').each(function(){
                    this.checked = false;
                });

                $('.check_box').addClass('hidden');
            }
        });
        </script>
    </body>
</html>
