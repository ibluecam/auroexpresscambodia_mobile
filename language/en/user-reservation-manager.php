<?php
$_LANG = array(
    'RESERV_HEADER' => 'Vehicle reservation manager',
    'RESERV_LOAD_QUERY_ERROR' => 'Unable to load reservation list due an internal error. See log for details.',
    'RESERV_NORESULT_LABEL' => 'There is no reservation to be managed at the moment.',
    'RESERV_REMOVE_CONFIRM_LABEL' => 'Are you sure you want to remove this vehicle from the reservation list?',
    'RESERV_DATE_LABEL' => 'Reservation date',
    'RESERV_VEHICLE_LABEL' => 'Vehicle',
    'RESERV_PRICE_LABEL' => 'Vehicle price',
    'RESERV_RESERVED_FOR_LABEL' => 'Reserved for',
    'RESERV_EMAIL_LABEL' => 'E-Mail address',
    'RESERV_PHONE_LABEL' => 'Phone',
    'RESERV_ADDRESS_LABEL' => 'Address',
    'RESERV_CITY_LABEL' => 'City',
    'RESERV_ZIP_LABEL' => 'Zip Code',
    'RESERV_COUNTRY_LABEL' => 'Country',
    'RESERV_CODE_LABEL' => 'Reservation code',
    'RESERV_IP_LABEL' => 'Under IP',
    'RESERV_REMOVE_BUTTON' => 'Remove reservation',
    'RESERV_WRITE_EMAIL_BUTTON' => 'Write user an e-mail',
    'RESERV_REMOVE_QUERY_ERROR' => 'Unable to remove reserved vehicle due an internal error. See log for more details.',
    'RESERV_REMOVE_WARNING_ERROR' => 'WARNING: Vehicle removed from user list but an error occurred when updating the vehicle status. CHECK THE LOG FILE FOR DETAILS.',
    'RESERV_REMOVE_SUCCESS' => 'Vehicle removed from reservation list successfully!'
);