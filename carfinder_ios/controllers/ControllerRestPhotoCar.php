<?php

class ControllerRestPhotoCar
{
 
    private $db;
    private $pdo;
    function __construct($db_path) 
    {
        require_once $db_path;

        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }

    function setClassPath($class_path) {
        require_once $class_path;
    }
 
    function __destruct() { }
 
    public function updatePhotoCar($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE car_media 
                                        SET 
                                            photo_url = :photo_url, 
                                            thumb_url = :thumb_url, 
                                            car_id = :car_id  

                                            WHERE photo_id = :photo_id');

        $result = $stmt->execute(
                            array('photo_url' => $itm->photo_url,
                                    'thumb_url' => $itm->thumb_url,
                                    'car_id' => $itm->car_id, 
                                    'photo_id' => $itm->photo_id) );
        
        return $result ? true : false;
    }


    public function insertPhotoCar($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO car_media( 
                                            photo_url, 
                                            thumb_url, 
                                            car_id ) 

                                        VALUES(
                                            :photo_url, 
                                            :thumb_url, 
                                            :car_id )');

        $result = $stmt->execute(
                            array('photo_url' => $itm->photo_url,
                                    'thumb_url' => $itm->thumb_url,
                                    'car_id' => $itm->car_id ) );
        
        return $result ? true : false;
    }
 
    public function deletePhotoCar($photo_id, $is_deleted) 
    {
        $stmt = $this->pdo->prepare('UPDATE car_media 
                                
                                        SET 
                                            is_deleted = :is_deleted 

                                        WHERE photo_id = :photo_id');
        
        $result = $stmt->execute(
                            array('photo_id' => $photo_id,
                                    'is_deleted' => $is_deleted ) );
        
        return $result ? true : false;
    }

    public function getPhotoCarByPhotoId($photo_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM car_media 
                                        WHERE photo_id = :photo_id');

        $result = $stmt->execute(
                            array('photo_id' => $photo_id) );

        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoCar();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->car_id = $row['car_id'];

            return $itm;
        }
        return null;
    }

    public function getLastInsertedPhotoId() {

        return $this->pdo->lastInsertId(); 
    }
 
}
 
?>