<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>




 <div id="sectionContenWrapper">
    <!--sectionSidebar--> 
    <div id="topMenu">
        <p><a href="#" class="linkfade">Help</a> > Blacklist Report/Search</p>
    </div>
    <?php include("php/sidebar/help.php");?>
    
    </div>           
    <div id="sectionContent"> 
        <div id="help_title">
            <p>Blacklist Report/Search</p>
        </div>
        <div class="contentText">
        	<p class="blacklistText">
            	Are you struggling with spammers/fake sellers or buyers? Please report this issue on blacklisting. Reporting an issue 
will be done anonymously. We will handle this issue immediately by confirmation of blacklisting or cancelling the 
reports. Please keep in mind that you may get in trouble for filling a false charge report We will do our best to find a 
quick and effective solution for our customers
            </p>
        </div>
        <div id="blacklistForm">
            <table width="705" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="tdl"><p>Report ID</p></td>
                    <td class="tdr">
                    	<input type="text" name="search" class="searchBox" />
                    	<input type="submit" name="btnSearch" class="btnSearch linkfade" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><p>Country</p></td>
                    <td class="tdr"><input type="text" name="country" /></td>
                </tr>
                <tr>
                    <td class="tdl"><p>Name</p></td>
                    <td class="tdr"><input type="text" name="name" /></td>
                </tr>
                <tr>
                    <td class="tdl"><p>Reason</p></td>
                    <td class="tdr">
                    	<select name="reason">
                        	<option>---Select---</option>
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                            <option>Option 4</option>
                            <option>Option 5</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tdA-r" colspan="2"><input type="submit" name="submit" class="linkfade"/></td>
                </tr>   
            </table>
        </div>
        	
        
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
  
  
  