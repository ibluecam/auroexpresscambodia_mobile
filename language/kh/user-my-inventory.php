<?php
	$_LANG = array(
	'INVENTORY_DEFAULT'=>'ដូចដើម',
	'INVENTORY_RESERVED'=>'ចរចា',
	'INVENTORY_SOLD_OUT'=>'លក់ដាច់',
	'INVENTORY_DELETE'=>'លុប',
	'INVENTORY_ADD_NEW'=>'បន្ថែមថ្មី',
	'INVENTORY_HIT'=>'ចូលមើល',
	'INVENTORY_COUNTRY'=>'ប្រទេស',
	'INVENTORY_CHASSISNO'=>'លេខកូដ',
	'INVENTORY_SELLER'=>'អ្នកលក់',
	'INVENTORY_FOB'=>'តម្លៃ',
	'INVENTORY_DETAIL'=>'លំអិត',
	'INVENTORY_EDIT'=>'កែប្រែ',
	'INVENTORY_STATUS'=>'ស្តុករថយន្ត',
	
	'INVENTORY_STEERING'=>'ដៃចង្កូត',
	'INVENTORY_FUEL_TYPE'=>'ប្រភេទប្រេង',
	'INVENTORY_MILEAGE'=>'ចំងាយ'
	
);
?>