<?php

	if( !isset($_SESSION['log_group']) ){

		header('Location: '.BASE_RELATIVE.'login/');
	}
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
// $group = 'user';
// $slug  = 'message-box';
// $inbox = 0;
// $message_count = "";
// load page content.
//$pg_details = $_CLASS->getPageHTML();

// Whether which tab is active.
//$box_flag = $_CLASS->getBoxFlag();

// if form is submit, get registration status to display feed
// back message.
//$del_status = $_CLASS->getDeleteStatus();
//$restore_status = $_CLASS->getRestoreStatus();


?>



 <div id="sectionContenWrapper">
        	<!--sectionSidebar-->
            <!-- <div id="topMenu">
            	<p><a href="#" class="linkfade">My iBlue</a> > My Message Box</p>
            </div>

        	<?php /*include("php/sidebar/my-wini.php")*/;?>
            </div> -->
            <div id="sectionContent">

                <div id="my_item">
                	<ul class="clearfix myitem">
		                <li><a href="<?php echo BASE_RELATIVE; ?>my-info"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_infonew.png" class="linkfade" /></a></li>
		                <li><a href="<?php echo BASE_RELATIVE; ?>messages"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/message_boxover.png" class="linkfade" /></a></li>
		                <li><a href="<?php echo BASE_RELATIVE; ?>my-inventory"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_inventorynew.png" class="linkfade" /></a></li>
		                <li><a href="<?php echo BASE_RELATIVE; ?>edit-car"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/register_productnew.png" class="linkfade" /></a></li>
		            </ul>
                </div>

				
                <div id="tab_wrap" class="clearfix">                	
                	<div style="position:absolute; right:56px; top:0px; width:12px; height:6px; z-index:99;">
						<img src="<?php echo BASE_RELATIVE; ?>images/mywini/messagebox/message_box.png" class="message" />
					</div>
					<div style="position:absolute; right:50px; top:25px; width:12px; height:6px; z-index:99; display:none;">
						<img src="<?php echo BASE_RELATIVE; ?>images/mywini/messagebox/up_arrow.png" />
					</div>                  
                    <div id="tab_trash" class="tab_msg trash" style="top:90px;">
                    	<p>Trash <span id="trashCount"></span></p>
                    </div>
                    <div id="tab_sent" class="tab_msg history" style="top:60px;">
                    	<p>Sent <span id="sentCount"></span></p>
                    </div>

                    <div id="tab_inbox" class="tab_msg awaiting currentTab" style="top:30px;">
					
                    	<p class="inbox" style="padding-left:45px;"> Inbox (<span id="inboxCount">0</span>)</p>
                    </div>


                    <div id="check_all">
                    	<span style="float:left; padding-right:10px;">
                    		<img src="images/common/register_item/title.png" /><a href="#" class="compose linkfade" style="color:#2a6ac1; margin-left:10px">Message Box </a>
                    	</span>
                        
                    	<!--<input type="button" value="Delete Selected" id="delete_selected"/>-->
						
                    </div>

                   
                </div>

                <div id="tab_detail">

                    <div id="message_container" class="message-box">
	                    
	                    	               	
                    </div>
                    
                    <div id="msg_pagination">
	                    	
		            </div>	
                </div>
				                             
            </div><!-- end div id="sectionContent"-->
            <div style="clear:both"></div>

        </div><!-- end div id="sectionContentWraper" -->
