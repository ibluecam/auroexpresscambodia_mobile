<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

$maker = trim($_POST['maker_filter']);
$model = trim($_POST['model_filter']);


if( !isset($model) || !isset($maker) ){
    exit;
}

require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

if( !$sql = mysql_query("SELECT `maker`,`model`,`body_type`,`status` FROM `car` WHERE `maker`='$maker' AND `model`='$model' AND `status`='0'") ){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to load car model list due a query error at ' . __FILE__ . ':' . __LINE__);
    
    $cnx->close();
    echo '';
    exit;
}

if( @mysql_num_rows($sql) < 1 ){
    $cnx->close();
    echo '';
    exit;
}

$model_arr = array();

$language_path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
$do_translation = false;

if( file_exists($language_path) ){
    require_once $language_path;
    $do_translation = true;
}

while( $r = @mysql_fetch_assoc($sql) ){
    ( $do_translation ? $label = $_CAR_BODY[$r['body_type']] : $label = $r['body_type'] );
        
    array_push($model_arr, $label);
}

@mysql_free_result($sql);
$cnx->close();

$model_arr = array_unique($model_arr);
$model_arr = array_values($model_arr);
sort($model_arr);

$result = '';

for( $i=0; $i < count($model_arr); $i++ ){
    $result .= $model_arr[$i];
    
    if( ($i+1) < count($model_arr) ){
        $result .= ',';
    }
}

echo $result;
exit;