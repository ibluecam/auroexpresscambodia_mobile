<?php
	require_once BASE_CLASS . 'php_cofig.php';
	/* require_once BASE_CLASS . 'class-global.php'; 
	$global = new classglobal(); */
	
	// set session time out for user		
	if(isset($_SESSION['log_email'])){			 
		// set timeout period in seconds
		$session_timeout = 3600; // time out 1h
		
		if (!isset($_SESSION['last_visit'])) { 
			$_SESSION['last_visit'] = time(); 
		} 			
	
		if((time() - $_SESSION['last_visit']) > $session_timeout) { 
		  session_destroy(); 				  		  
		  echo '<script type="text/javascript">window.location="'.BASE_RELATIVE.'login"</script>';
		}
		$_SESSION['last_visit'] = time();
	}	
	

?>



<!-- GoogleAnalytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58272743-1', 'auto');
  ga('send', 'pageview');

</script>

<div id="header">	
    <div class="container menuHeader">	
        <div id="contaninerHead" class="navbar">
            <div class="navbar-brand logo">
                <a href="<?php echo BASE_RELATIVE; ?>" id="logo">
                    <img src="<?php echo BASE_RELATIVE.'asset/img/express_auto_dealer_logo.png'; ?>" alt="Logo">
                </a>
            </div>                       
        	<div id="country-select">

					<?php $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
					$link=explode("?", $link);
					if(isset($_GET['lang'])=="en"){
						$get_language="en";
					}else{
					  $get_language="kh";					  					 				 
					}

					?>
					
                    <form action="server-side-script.php">
                        <select id="country-options" name="country-options">
                            <?php
        
                                $id = $_GET['owner'];
                                $cid = $_GET['cid'];
                                $com_id  = $_GET['id'];
                                $pro_type = $_GET['product_type']; 
								if(isset($_GET["member_type3"])){
									$reg_type="&member_type3=seller&signup=Next";
								}else{
									$reg_type="";
								}
                            ?>
                    
							<?php 
                                if($_SESSION['log_language_iso']== "en"){
                            ?>
                                    <option selected="selected" title="<?php echo  $link[0];?>?lang=en<?php echo $reg_type ?>" value="uk">English</option>
                                    <option title="<?php echo  $link[0];?>?lang=kh<?php echo $reg_type ?>" value="km">ខ្មែរ</option>
                                    
                            <?php
                                    
                                }elseif($_SESSION['log_language_iso']== "kh"){
                            ?>
                                    <option  title="<?php echo  $link[0];?>?lang=en<?php echo $reg_type ?>" value="uk">English</option>
                                    <option selected="selected" title="<?php echo  $link[0];?>?lang=kh<?php echo $reg_type ?>" value="km">ខ្មែរ</option>
                                                   

                            
                            <?php } ?>
                        </select>
                        <input value="Select" type="submit" />
                    </form>

					</div><!-- #country-select -->
			<!-- Menu Nav -->
			<div id="menuRes">
				<img src="<?php echo BASE_RELATIVE; ?>asset/img/menu_res.png" />
			</div><!-- menuRes -->
		</div><!-- .navbar -->	
		

		<div id="menuNav">
			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul id="dropMenu" class="nav navbar-nav mainmenu">
					<li><a  href="<?php echo BASE_RELATIVE;?>" title="Vehicles of Express Auto Cambodia" ><?php echo $_GLANG['PUBLIC_TOPMENU_ALL_VEHICLE'];?></a></li>
                	<li><a href="<?php echo BASE_RELATIVE.'?lc=kh'?>" title="Current stock of Express Auto Cambodia"><?php echo $_GLANG['PUBLIC_TOPMENU_CURRENT_STOCK'];?></a></li>
                	
					<li><a href="<?php echo BASE_RELATIVE.'?lc=us'?>" title="USA stock of Express Auto Cambodia"><?php echo $_GLANG['PUBLIC_TOPMENU_USA_STOCK'];?></a></li>
					
					<li><a href="<?php echo BASE_RELATIVE.'dealer'?>" title="Dealers of Express Auto Cambodia"><?php echo $_GLANG['PUBLIC_TOPMENU_FIND_LOCAL_DEALER'];?></a></li>
                   
                    <li><a href="<?php echo BASE_RELATIVE.''?>" title="Other services of Express Auto Cambodia"><?php echo $_GLANG['PUBLIC_TOPMENU_OTHER_SERVICE'];?></a></li>
                   
				</ul> 										
			</div>
		</div><!-- #menuNav -->
		
    </div><!-- .menuHeader -->
</div><!-- #header -->