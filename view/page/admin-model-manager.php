<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'admin';
$slug  = 'car';
// load page content.

?>
<script type="text/javascript">
    var make="<?php if(isset($_POST['makeSelect'])) echo htmlspecialchars($_POST['makeSelect']);?>";
    var product_type="<?php if(isset($_POST['product_type'])) echo htmlspecialchars($_POST['product_type']); else echo 'Car';?>";
</script>



 <div id="sectionContenWrapper">              
    <div id="topMenu">
        <p><a href="#" class="linkfade">Site Manager</a></p>

    </div>
    <?php include("php/sidebar/community.php");?>
    </div>
    <div id="sectionContent">
        <div class="page_title">Model Manager</div> 
        <div id="admin_tab" class="clearfix">
            <ul class="clearfix">
                <li data-value="Car" class="top_tab current">Cars</li>
                <li data-value="Truck" class="top_tab">Trucks</li>
                <li data-value="Bus" class="top_tab">Buses</li>
                <li data-value="Part" class="top_tab">Auto Parts</li>
                <li data-value="Accessories" class="top_tab">Auto Accessories</li>
                <li data-value="Equipment" class="top_tab">Heavy Machines</li>
                <li data-value="Watercraft" class="top_tab">Watercrafts</li>
                <li data-value="Aircraft" class="top_tab">Aircrafts</li>
                <li data-value="Motorbike" class="top_tab">Motor Bikes</li>
            </ul>
        </div>
        
        
        <?php
        ///////////Show saving result
            if( $fstatus ){
        ?>
        <div class="alert <?php echo $fstyle;?>">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <?php echo $fmessage;?>
        </div>
        <?php
            }
        ///////////End Show saving result
        ?>
        <!-- Content body -->

        <div class="tab_content" id="seller_members_wrapper">
            <form method="post">
                <div>
                    <table class="make_model_table">
                        <tr>
                            <td>
                                Make
                            </td><td>
                                <select id="makeSelect" name="makeSelect">
                                    <option value="- Make -">- Make -</option>
                                </select>
                            </td>
                            
                        </tr><tr>
                            <td>New Model:</td>
                            <td>
                                <input type="text" id="newModelInput" name="newModelInput"/>
                                <input type="submit" id="submitSave" name="submitSave" class='hover' value="Add">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="selected_button_wrapper">
                    <input type="hidden" id="product_type" name="product_type" value="Car"/>
                    <input type="submit" name="save_changes" class="selected_button" id="save_changes" value="SAVE CHANGES">
                    <input type="submit" name="delete_selected" class="selected_button" id="delete_selected" value="DELETE SELECTED">
                </div>
                
                
                <div id="model_table_wrapper">
                        

                </div>
            </form>
        </div>
        <!-- End Content body -->
        
       
        <!-- end register Tab -->
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
<div style="clear:both"></div>
  
  