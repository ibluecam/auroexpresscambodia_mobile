<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminmanager {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $news_list;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminmanager($lang=array()){
        $this->lang = $lang;
        
        // handle remove news.
        if( isset($_POST['removebtn']) ){
            $this->handleForm();
        }
        
        // load news list.
        //$this->loadNewsList();
    }
    
    /**
     * Private method: handle remove news form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    public function handleForm(){
        $id = (int)$_POST['idInput'];
        
        if( $id < 1 ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query("DELETE FROM `news` WHERE `id`='$id' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to remove news due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['MANAGE_NEWS_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['MANAGE_NEWS_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: load news list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadNewsList(){
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        
        $cnx = new Connect();
        $cnx->open();
                
        // load news.
        if( !$sql = @mysql_query("SELECT * FROM `news` ORDER BY `id` DESC") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            
            LogReport::write('Unable to load news list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->news_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $day = explode('-',$r['date']);
            $date = Utilities::checkDateFormat($day[0],$day[1],$day[2],$format);
            
            $obj = array(
                'id' => $r['id'],
                'title' => stripslashes($r['title']),
                'html' => stripslashes($r['html']),
                'date' => $date,
                'time' => $r['time'],
                'author' => stripslashes($r['author'])
            );
            array_push($this->news_list,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    
    /**
     * Public method: get news list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getNewsList(){
        return $this->news_list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}