<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class userreservationmanager {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $list;
    protected $car_list;

    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function userreservationmanager($lang=array()){
        $this->lang = $lang;
        
        // handle remove reservation form.
        if( isset($_POST['removebtn']) ){
            $this->removeReservationFormHandler();
        }
        
        // load reservation list.
        $this->loadReservationList();
    }
    
    /**
     * Private method: handle remove reservation form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function removeReservationFormHandler(){
        $car_id = trim($_POST['cidInput']);
        $reservation_id = trim($_POST['ridInput']);
        $author=(int)$_SESSION['log_id'];
		 
        if( empty($car_id) || empty($reservation_id) ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if(!$this->isAuthorOfCar($car_id) || !@mysql_query("DELETE FROM `reservation` WHERE `reservation`.`id`='$reservation_id' AND `car_id`='$car_id' LIMIT 1;")){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to remove reserved vehicle due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['RESERV_REMOVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
			
        }
        
        // update car status.
        if( !@mysql_query("UPDATE `car` SET `status`='0' WHERE `id`='$car_id' AND `author`='$author' LIMIT 1;")){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('WARNING:URGENT::: Unable to update car id [] back to status 0. Vehicle will NOT be listed on front-end until the status is updated. Error occurred due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['RESERV_REMOVE_WARNING_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        
        $this->form_message = $this->lang['RESERV_REMOVE_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
     /**
     * Private method: check if user is author or not
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function isAuthorOfCar($car_id){
        require_once BASE_CLASS . 'class-connect.php';
        $author=(int)$_SESSION['log_id'];
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `car` WHERE `id`='$car_id' AND `author`='$author'") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to load reservation list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['RESERV_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if(mysql_num_rows($sql)>0){
			return true;
			
		}
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    
    /**
     * Private method: load reservation list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadReservationList(){
        require_once BASE_CLASS . 'class-connect.php';
        $author=(int)$_SESSION['log_id'];
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `reservation` r
INNER JOIN car c
ON r.car_id = c.id
WHERE c.author='$author'
		ORDER BY `date` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to load reservation list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['RESERV_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id'        => $r['id'],
                'car_id'    => $r['car_id'],
                'user_id'   => $r['user_id'],
                'name'      => $r['name'],
                'email'     => $r['email'],
                'phone'     => $r['phone'],
                'address'   => $r['address'],
                'city'      => $r['city'],
                'zip'       => $r['zip'],
                'country'   => $r['country'],
                'code'      => $r['code'],
                'browser'   => $r['browser'],
                'ip'        => $r['ip'],
                'date'      => $r['date']
            );
            array_push($this->list, $obj);
        }
        
        @mysql_free_result($sql);
        
        // load car list.
        if( !$sql = @mysql_query("SELECT `id`,`maker`,`model`,`price`,`year` FROM `car` WHERE `status`='1' AND author='$author'") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to load reservation list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['RESERV_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $this->list = array();
            return;
        }
        
        $this->car_list = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id'    => $r['id'],
                'maker' => $r['maker'],
                'model' => $r['model'],
                'price' => $r['price'],
                'year'  => $r['year']
            );
            array_push($this->car_list,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    
    /**
     * Public method: get car by id
     * <br>---------------------------------------------------------------------
     * @param $car_id The car id.
     * @return array
     */
    public function getCarById($car_id){
        if( empty($car_id) ){
            return array();
        }
        
        $result = array();
        
        for( $i=0; $i < count($this->car_list); $i++ ){
            if( $this->car_list[$i]['id'] == $car_id ){
                $result = array(
                    'id' => $this->car_list[$i]['id'],
                    'maker' => $this->car_list[$i]['maker'],
                    'model' => $this->car_list[$i]['model'],
                    'price' => $this->car_list[$i]['price'],
                    'year' => $this->car_list[$i]['year']
                );
                break;
            }
        }
        
        return $result;
    }
    
    /**
     * Public method: get reservation list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getReservationList(){
        return $this->list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
