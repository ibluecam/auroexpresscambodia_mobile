    <link href="<?php echo BASE_RELATIVE;?>css/blog/blog-main.css" rel="stylesheet" type="text/css" />
    
    <div id="sectionSlide">
        <img src="images/slide.jpg" />
    </div>
 <div id="sectionContent" class="clearfix">    	
	<?php include('layout/sidebar.php'); ?>
    <div id="sectionFeature" class="clearfix">
        <div id="sectionListVihicle" class="cleafix">
            <div class="reviewVihicle">
                <p><strong>Products :</strong> Car (14) / Truck (1) / Bus (1) / Part/Acc. (32) </p>
            </div>
            <div class="vihicle clearfix">
                <div class="vihicleHeader clearfix"><p class="type">Car (14)</p><p class="seeMore"><a href="">See more</a></p></div>
                <div class="listVihicle clearfix">                        
                    <?php
                        $count =1;
                        for($i=1; $i <=5; $i++){
                    ?>
                            <div class="item <?php if(($count%5) == 0){ echo $count;?> itemLast <?php }?>">
                                <p><img src="images/img1.jpg" width="120" height="90" /></p>
                                <p><a href="">2001 Hyundai Terracan Terracan</a></p>
                                <p class="price">Ask for Price</p>
                                
                            </div>
                    <?
                            $count++;
                        }
                    ?>
                </div>
            </div>
            
            <div class="vihicle clearfix">
                <div class="vihicleHeader clearfix"><p class="type">Track (14)</p><p class="seeMore"><a href="">See more</a></p></div>
                <div class="listVihicle clearfix">
                     <?php
                        $count =1;
                        for($i=1; $i <=5; $i++){
                    ?>
                            <div class="item <?php if(($count%5) == 0){?> itemLast <?php }?>">
                                <p><img src="images/img2.jpg" width="120" height="90" /></p>
                                <p><a href="">2001 Hyundai Terracan Terracan</a></p>
                                <p class="price">USD 36,000 </p>
                                
                            </div>
                    <?
                            $count++;
                        }
                    ?>
                    
                </div>
            </div>
            
            <div class="vihicle clearfix">
                <div class="vihicleHeader clearfix"><p class="type">Bus (14)</p><p class="seeMore"><a href="">See more</a></p></div>
                <div class="listVihicle clearfix">
                    <?php
                        $count =1;
                        for($i=1; $i <=5; $i++){
                    ?>
                            <div class="item <?php if(($count%5) == 0){?> itemLast <?php }?>">
                                <p><img src="images/img3.jpg" width="120" height="90" /></p>
                                <p><a href="">2001 Hyundai Terracan Terracan</a></p>
                                <p class="price">USD 36,000 </p>
                                
                            </div>
                    <?
                            $count++;
                        }
                    ?>
                </div>
            </div>                
        </div>
        
        <div id="sectionPhoto">
            <div class="photohead">
                <p><strong>Photo</strong></p>
            </div>
            <div class="photoGallery clearfix">
                <div class="photoGalleryHeader clearfix"><p class="type">Car (14)</p><p class="seeMore"><a href="">See more</a></p></div>
                <div class="listphotoGallery clearfix">                    
                    <?php
                        $count =1;
                        for($i=1; $i <=5; $i++){
                    ?>
                            <div class="item <?php if(($count%5) == 0){?> itemLast <?php }?>">
                                <p><img src="images/img4.jpg" width="120" height="90" /></p>
                                <p><a href="">Our Staff</a></p>
                                
                            </div>
                    <?
                            $count++;
                        }
                    ?>
                </div>
            </div>
        </div>
        
        <div id="sectionContact">
            <p>
                <b>Contact Us</b><br />
                <b>To. Wan Sik Lee (ID: kncar)</b><br />
                Test Add [+] Del [x]
            </p>
            <table width="640" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                    <textarea rows="5"></textarea>
                </td>
              </tr>
              <tr>
                <td>
                    <!--<input type="file" name="fileUpload" />
                    <input type="submit" class="btndelete" value="delete" /><input type="submit" class="btnadd" value="add" />-->
                    <div class='file_upload' id='f1'><input name='file[]' type='file'/></div>
                    <div id='file_tools'>                        	
                        <img src='images/btnadd.gif' id='add_file' title='Add new input'/>
                        <img src='images/btndelete.gif' id='del_file' title='Delete'/>
                    </div>
                    
                </td>
              </tr>
              <tr>
                <td>
                    <p>
                        * Available File Formats : XLS, DOC, PPT, .JPG, GIF, BMP, ZIP, PDF, PNG, HWP, TXT*<br />
                                                                             Max file upload size : 10MB
                    </p>
                </td>
              </tr>
              <tr>
                <td>
                    <input type="submit" value="send" class="btnsend linkfade" />
                </td>
              </tr>
            </table>        
        </div>
    </div>
</div>