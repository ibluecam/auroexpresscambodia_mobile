<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();

// define login url
if( !empty($_GET['cid']) ){
    $cid = (int)$_GET['cid'];
    
    if( $cid > 0 ){
        $login_url = BASE_RELATIVE . 'login/?pg=reserve-car&cid=' . $cid;
    }
    else {
        $login_url = BASE_RELATIVE . 'login/';
    }
}
else {
    $login_url = BASE_RELATIVE . 'login/';
}
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['RESERVE_CAR_HEADER'];?></h1>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $_LANG['RESERVE_CAR_LOGIN_REQUIRED_LABEL'];?>
                </div>
                <?php echo $_LANG['RESERVE_CAR_LOGIN_ACCOUNT_LABEL'];?>
                <div class="clearfix"></div>
                <br>
                <a href="<?php echo $login_url;?>" class="btn btn-success"><i class="icon-user" style="margin-right:10px;"></i> <?php echo $_LANG['RESERVE_CAR_LOGIN_BUTTON'];?></a>&nbsp;&nbsp;
                <a href="<?php echo BASE_RELATIVE;?>register/" class="btn"><i class="icon-cog" style="margin-right:10px;"></i> <?php echo $_LANG['RESERVE_CAR_REGISTER_BUTTON'];?></a>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
