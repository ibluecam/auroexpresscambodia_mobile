<?php
/**
 * IMPORTANT
 * 
 * The list below defines the car's drive type which can be translated to your 
 * language. DO NOT CHANGE THE ID ORDER OF EACH ROW!!!!! IF YOU DO, YOU WILL
 * SCREW IT UP!
 * 
 * ID(never change this) => drive type name(you can change this)
 * 
 * If you need more types, just add another row. IE. '31' => 'New type'
 */
$_CAR_DRIVE = array(
    '0' => 'Not defined',
    '1' => '2WD',
    '2' => '4WD'
);
