<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/**
 * Generates sitemap based on the car list update status.
 */
class SiteMap {
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function SiteMap(){
        
    }
    
    /**
     * Public method: generate site map
     * <br>---------------------------------------------------------------------
     * @return boolean
     */
    public function generate(){
        $path = 'config.php';

        if( !file_exists($path) ){
            return false;
        }

        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        // do the query: select only vehicles which are available for selling.
        $query = "SELECT * FROM `car` WHERE `status`='0'";

        if( !$sql = @mysql_query($query) ){
            $cnx->close();
            return false;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return true;
        }

        $list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => stripslashes($r['id']),
                'maker' => stripslashes($r['maker']),
                'model' => stripslashes($r['model']),
                'year' => stripslashes($r['year'])
            );
            array_push($list, $obj);
        }

        unset($obj);
        unset($sql);
        unset($r);

        @mysql_free_result($sql);
        $cnx->close();

        # generate list header.
        $xml  = '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n";
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\r\n";# generate static pages list.$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>1.00</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/login/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/register/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/support/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/search/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/sell-your-car/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/financing/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/find-us/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";$xml .= '<url>' . "\r\n";$xml .= '  <loc>http://jtcarframework.intersofts.com/news/</loc>' . "\r\n";$xml .= '  <changefreq>daily</changefreq>' . "\r\n";$xml .= '  <priority>0.80</priority>' . "\r\n";$xml .= '</url>' . "\r\n";
        
        # add static pages.
        $xml .= '<url>' . "\r\n";    
        $xml .= '  <loc>'.BASE_RELATIVE .'</loc>' . "\r\n";    
        $xml .= '  <changefreq>daily</changefreq>' . "\r\n";    
        $xml .= '  <priority>1</priority>' . "\r\n";    
        $xml .= '</url>' . "\r\n";
        
        # build slug
        require_once BASE_CLASS . 'class-utilities.php';

        for( $i=0; $i < count($list); $i++ ){
            $slug = Utilities::genetateSlug($list[$i]['maker'].' '.$list[$i]['model'].' '.$list[$i]['year']);	
            $slug = BASE_RELATIVE . $slug . '/car-detail/?cid=' . $list[$i]['id'];
            
            $xml .= '<url>' . "\r\n";    
            $xml .= '  <loc>'. $slug .'</loc>' . "\r\n";    
            $xml .= '  <changefreq>daily</changefreq>' . "\r\n";    
            $xml .= '  <priority>0.64</priority>' . "\r\n";    
            $xml .= '</url>' . "\r\n";
        }

        # close tag.
        $xml .= '</urlset>' . "\r\n";

        # rewrite current map.
        $error = true;

        if( $fo = @fopen( BASE_ROOT . 'sitemap.xml','w' ) ){
                if( !@fwrite($fo, $xml) ){
                        $error = false;	
                }

                @fclose($fo);
        } else {	
                $error = false;
        }

        return $error;
    }
}
