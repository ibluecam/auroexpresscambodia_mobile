<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}

/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
//$_CKEDITOR_ID = 'sellerCommentText';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $invalidProduct = false;
    // get lists.
    $maker_unique_list = $_CLASS->getMakerList();
    $model_unique_list = $_CLASS->getModelList();
    $body_type_list    = $_CLASS->getBodyList();
	$drive_type_list   = $_CLASS->getDriveList();

	$product_option_list = $_CLASS->getProductOptionList();
    $country_list = $_CLASS->getCountryList();
	$vehicle_type_list = $_CLASS->getVehicleTypeList();
	$car_maker_list = $_CLASS->getCarMakerList();
	$model_list=$_CLASS->getModelList();
	$category_list = $_CLASS->getCategoryList();
	$fuel_type_list = $_CLASS->getFuelTypeList();
	$transmission_list = $_CLASS->getTransmissionList();

    // set input size for maker.
    ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );
    ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );
    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );
    if( CURRENCY_CODE == 'USD' ){
        $decimal = '.';
        $thousand= ',';
    }
    else {
        $decimal = ',';
        $thousand= '.';
    }
    if(!isset($_GET['cid'])) $getId='';
	else $getId=$_GET['cid'];
	$product=array();

    // get generated car id.
	if(empty($getId)){
    	$carid = $_CLASS->getCarId();
	}else{
		$carid = $getId;
		$product = $_CLASS->getAllProducts($carid);
        $invalidProduct=$_CLASS->invalidProduct;

	}
	(isset($product['payment_terms']) ? $payment_terms = explode('^',$product['payment_terms']) : $payment_terms=array());
	(isset($product['other_options']) ? $other_options = explode('^',$product['other_options']) : $other_options=array());
	(isset($product['safety_device_options']) ? $safety_device_options = explode('^',$product['safety_device_options']) : $safety_device_options=array());
	(isset($product['exterior_options']) ? $exterior_options = explode('^',$product['exterior_options']) : $exterior_options=array());
	(isset($product['interior_options']) ? $interior_options = explode('^',$product['interior_options']) : $interior_options=array());
	(isset($product['fitting_make']) ? $fitting_make_row = explode(';',$product['fitting_make']) : $fitting_make_row=array());
	$fitting_make=array();
	$fitting_make_tmp=array();
	foreach($fitting_make_row as $fr){
		$fitting_make_tmp=explode('^',$fr);
		$tmp_maker=$fitting_make_tmp[0];
		unset($fitting_make_tmp[0]);
		$fitting_make[$tmp_maker]=$fitting_make_tmp;
	}


?>
<script>
var condition="<?php if(isset($product['condition'])) echo $product['condition']; ?>";
var vessel_type="<?php if(isset($product['vessel_type'])) echo $product['vessel_type']; ?>";
var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
var year="<?php if(isset($product['model_year'])) echo $product['model_year']; ?>";
var built_year="<?php if(isset($product['built_year'])) echo $product['built_year']; ?>";
var made_in="<?php if(isset($product['made_in'])) echo $product['made_in']; ?>";
var country="<?php if(isset($product['location'])) echo $product['location']; ?>";

var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
var flight_rule="<?php if(isset($product['flight_rule'])) echo $product['flight_rule']; ?>";
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
        <div id="content-wrapper">
              <div id="topMenu">
            	<p><a href="#" class="linkfade">Register Item</a> > Jet Ski</p>
            </div>
            <?php include("php/sidebar/my-wini.php");?>
            </div>
                <div id="inner-content">
                <!-- START CONTENT -->
                <div class="page-title">
                	<?php
					if(empty($getId)){
						echo "<p>".$_LANG['PRODUCT_HEADER']."</p>";
					}else{
						echo "<p>".$_LANG['PRODUCT_EDIT_HEADER']."</p>";
					}
					?>
                </div>

                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <form id="form-wrap" method="post">
                    <!----------------- FORM CONTAINER -------------------->
                    <?php if(!$invalidProduct){?>
                    <input type="hidden" name="caridInput" value="<?php echo $carid;?>" />

                    <div id="content1">
                       <br/>

						<div class="input-prepend input-append inputap">

                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_CONDITION_LABEL'];?> *</span>
                            <label><input type="radio" checked id="conditionInput" name="conditionInput" value="New"/><?php echo $_LANG['PRODUCT_CONDITION_NEW'];?></label>
                           	<label><input type="radio" name="conditionInput" value="Used"/><?php echo $_LANG['PRODUCT_CONDITION_USED'];?></label>

                        </div>
                       
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_UNIT_WEIGHT_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['unit_weight'])) echo $product['unit_weight'];?>" maxlength="20" name="unitWeightInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_UNIT_WEIGHT_PLACEHOLDER'];?>" onkeypress=""/>

                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_LENGTH_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['length'])) echo $product['length'];?>" id="lengthInput" name="lengthInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_LENGTH_PLACEHOLDER'];?>" />

                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_HEIGHT_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['height'])) echo $product['height'];?>" name="heightInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_HEIGHT_PLACEHOLDER'];?>" />

                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_WIDTH_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['width'])) echo $product['width'];?>" name="widthInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_WIDTH_PLACEHOLDER'];?>" />

                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_MAKER_LABEL'];?> *</span>
                            <select required name="makerInputSelect" id="makerInputSelect" class="content-label" >
                                <?php
                                    $_GET['product_type']="Jet Ski";
                                    include('ajax/load_saved_make.php');
                                ?>
                            </select>
                            <input type="text" style="display:none;width:250px;" value="" maxlength="50" name="makerInput" id="makerInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_MAKER_PLACEHOLDER'];?>" />

                        </div>

                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_MODEL_LABEL'];?> *</span>
                            <select required name="modelInputSelect" id="modelInputSelect" class="content-label" >
                                <option value="">- Model -</option>
                            </select>
                            <input type="text" style="display:none;width:250px;" value="" maxlength="50" name="modelInput" id="modelInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_MODEL_PLACEHOLDER'];?>" />

                        </div>
                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_YEAR_LABEL'];?> *</span>
                            <select id="yearInput" required name="yearInput" style="width:280px;">
                                <option value="">- Select -</option>
                                <?php
                                for( $i=date('Y'); $i > 1900; $i-- ){
                                ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_ENGINE_POWER_LABEL'];?></span>
                            <input type="text" value="<?php if(isset($product['engine_power'])) echo $product['engine_power'];?>" name="enginePowerInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_ENGINE_POWER_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" />
                        </div>
						<div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_COUNTRY_LIST_LABEL'];?> *</span>
                            <?php

                            ?>

                            <select id="countryInputSelect" required name="countryInputSelect" style="width:280px;">
                                <option value="">- Select -</option>
                                <?php
                                    if( count($country_list['cc']) > 0 ){

                                        for( $i=0; $i < count($country_list['cc']); $i++ ){
                                            if( !empty($country_list['country_name'][$i]) ){
                                    ?>
                                    <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                <?php
                                            }
                                        }
                                    }
                                    else {
                                ?>
                                    <option value="0"><?php echo $_LANG['PRODUCT_NOCOUNTRY_LABEL'];?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_PRICE_LABEL'];?></span>

                            <input type="text" value="<?php if(isset($product['price'])) echo $product['price'];?>" name="priceInput" class="content-label" placeholder="<?php echo $_LANG['PRODUCT_PRICE_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)"/>
                            <select name="currencySelect" id="currencySelect">
                                <?php include("ajax/load_currency_list.php");?>
                            </select>
                        </div>


                        <div class="input-prepend inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_PAYMENT_TERM_LABEL'];?></span>

                            <label><input <?php if (in_array("T/T", $payment_terms)) echo "checked";?> type="checkbox" name="paymentTermCheck[]" value="T/T" /> <span>T/T</span></label>
                            <label><input <?php if (in_array("L/C", $payment_terms)) echo "checked";?> type="checkbox" name="paymentTermCheck[]" value="L/C" /> <span>L/C</span></label>
                            <label><input <?php if (in_array("Other", $payment_terms)) echo "checked";?> type="checkbox" name="paymentTermCheck[]" value="Other" /> <span>Other</span></label>


                        </div>


                        <div class="input-prepend inputap clearfix accordion">

                            <span style="width:100%;float:left;" class="add-on title-label"><?php echo $_LANG['PRODUCT_SELLER_COMMENT_HEADER'];?></span>
                            <div class="accordion_item">
                                <textarea id="sellerCommentText" name="sellerCommentText">
                                    <?php if(isset($product['seller_comment'])) echo $product['seller_comment'];?>
                                </textarea>
                            </div>

                        </div>
                        <!-------------- UPLOAD MEDIA BLOCK ---------->

                        <div id="video-wrap" style="display:none;" class="input-prepend input-append inputap">
                            <span class="add-on title-label"><?php echo $_LANG['PRODUCT_YOUTUBE_URL_LABEL'];?></span>
                            <input id="videoUrlInput" type="text" class="content-label" style="width:435px;" placeholder="<?php echo $_LANG['PRODUCT_YOUTUBE_URL_PLACEHOLDER'];?>" />
                            <input id="vidBtn" onclick="saveVideoHandler('<?php echo $carid;?>');return false;" type="submit" class="btn btn-info" style="width:100px;" value="<?php echo $_LANG['PRODUCT_SAVE_BUTTON'];?>" />
                        </div>
                        <div id="upload-wrap" style="display:none;" class="input-prepend input-append inputap">
                            <h4><?php echo $_LANG['PRODUCT_UPLOAD_LABEL'];?></h4>
                            <iframe id="iupload" width="700" height="100" style="border:0;"></iframe>
                        </div>
                    </div>
                    <br>
                    <div id="outputwrap"></div>
                    <div id="outoutimg" class="clearfix"></div>
                    <div class="clearfix"></div>
                    <br>
                    <a id="prevbtn" href="#" onclick="return previousFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['PRODUCT_PREVIOUS_BUTTON_LABEL'];?>
                    </a>
                    <a id="nextbtn" href="#" onclick="return nextFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <?php echo $_LANG['PRODUCT_NEXT_BUTTON_LABEL'];?><i class="icon-circle-arrow-right" style="margin-left:10px;"></i>
                    </a>
                    <input style="margin:0px 0px 20px 10px;" id="submitbtn" style="" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['PRODUCT_SAVE_BUTTON'];?>" />

                    <!-------------- END UPLOAD MEDIA BLOCK ---------->
                <!----------------- END FORM CONTAINER -------------------->
                <?php
                }
                else{
                    echo "<font color='#C1272D'>Product with this ID is does not exist!</font>";
                }


                ?>
                </form>
                <!-- END CONTENT -->



                    </div>


                    <div class="clearfix"></div>
                    <div id="content2">

            </div>
            <div class="clearfix"></div>
        </div>
        <script language="javascript" type="text/javascript">
            /* preview uploaded image */
            $(document).ready(function(e){
                displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>');
            });
            function basename(path) {
               return path.split('/').reverse()[0];
            }
			
			
			

			 function carImageUploadedNew_Photo(url,mode,fname,i){

                        var did = 'img' + Math.round((Math.random()*1000));
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(i==1){

							 html +="<input type='radio' name='primary_photo[]' value="+fname+"  checked='checked'  />Set as Primary Photo";
						}
                        else{
							 html +="<input type='radio' name='primary_photo[]' value="+fname+"  />Set as Primary Photo";
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+basename(url)+'\',\''+did+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }




            function carImageUploaded(url,mode,fname,primary_photo){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(primary_photo==1){

							 html +="<input type='radio' name='primary_photo[]' value="+fname+"  checked='checked'  />Set as Primary Photo";
						}
                        else{
							 html +="<input type='radio' name='primary_photo[]' value="+fname+"  />Set as Primary Photo";
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+basename(url)+'\',\''+did+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}
