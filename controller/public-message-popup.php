<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicMessagePopup
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'buyer-directory';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    var $traderList;
    private $car;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;
    var $register_type="";

    protected $seller;
    protected $countries;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicMessagePopup($lang=array())
    {
        $this->lang = $lang;
        $this->loadSeller($_GET['cid']); 
        $this->loadCountries(); 

        if(isset($_POST) && !empty($_POST))
        {
            $this->processOrderForm();
        }        
    }

    private function processOrderForm()
    {
        if(!isset($_GET['cid']))
        {
            return;
        }

        // Send order mail        
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        mysql_query("SET Names utf8");

        $product_id = $_GET['cid'];
        $sent_id = $_SESSION['log_email'];
        $respose_id = $this->seller['email'];
        $duration = date('Y-m-d H:i:s');
        $country = isset($_POST['countryLoadingSelect']) ? $_POST['countryLoadingSelect'] : '';
        $port_name = isset($_POST['portLoadingSelect']) ? $_POST['portLoadingSelect'] : '';
        $payment_term = isset($_POST['payment_term']) ? implode(',', $_POST['payment_term']) : '';
        $price_term = isset($_POST['price_term']) ? implode(',', $_POST['price_term']) : '';        
        $buyer_id = $_SESSION['log_id'];
        $ip = $this->getIpAddresses()['user'];


        $message = isset($_POST['description']) ? $_POST['description'] : '';
        $message = mysql_real_escape_string($message);        
        $message = htmlentities($message);

        $id = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) as message_count FROM message"))['message_count'];
        $id++;

        $query = "INSERT INTO message ( id, message, sent_id, response_id, duration, seen, country, port_name, 
                                        payment_term, price_term, product_id, buyer_id, ip) 
                              VALUES  ( $id, '{$message}', '{$sent_id}', '{$respose_id}', '{$duration}', 0, '{$country}', '{$port_name}', 
                                        '{$payment_term}', '{$price_term}', '{$product_id}', '{$buyer_id}', '{$ip}')";

        
        if(!mysql_query($query))
        {
            require_once BASE_ROOT . 'core/class-log.php';
            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
        }

        $cnx->close();
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        //echo $this->slug;
        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
    
    /*
     * Public method: get load trader data ------------------------------------
     * @return array | false.
     */
    private function loadTraderList(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $traderList=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if(isset($_GET['keyword'])) $keyword=mysql_real_escape_string(stripcslashes($_GET['keyword'])); else $keyword="";
        $query = "SELECT rg.*, 
        cl.country_name as country_name
        FROM register as `rg` 
        INNER JOIN country_list AS cl
        ON rg.country=cl.cc 
        LEFT JOIN product as `pd`
        ON pd.owner=rg.id
        WHERE register_type LIKE '%$this->register_type%' $where  
        GROUP BY `rg`.`id`
        ";
        $this->traderList=$traderList;
        $cnx->close();
    }
    
    /*
     * Public method: get number by condition data ------------------------------------
     * @return array | false.
     */
    private function loadCountries(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();  
        mysql_query("SET NAMES utf8"); // Allow data from mysql as UTF-8.
        
        $countries = array();
        if(!$result = mysql_query("SELECT * FROM country_list "))
        {
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load countries due a query error at ' . __FILE__ . ':' . __LINE__);
        }
        else
        {
            while($row = mysql_fetch_assoc($result))
            {
                array_push($countries, $row);
            }
        }
        $cnx->close();

        $this->countries = $countries;
    }

    private function loadSeller($cc){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();  
        mysql_query("SET NAMES utf8");

        if(!$result =  mysql_query("SELECT  r.name as name, 
                                            r.email as email
                                    FROM register r
                                        INNER JOIN product p ON r.id = p.owner
                                    WHERE p.id = '{$cc}'"))
        {
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load countries due a query error at ' . __FILE__ . ':' . __LINE__);
        }
        else
        {
            $this->seller =  mysql_fetch_assoc($result);
        }
        $cnx->close();
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getTraderList()
    {
        return $this->traderList;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    /*
     * Public method: get vehicle data -------------------------------------------
     * @return string.
     */

    public function getIpAddresses()
    {
       $ipAddresses = array();
       if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {

          $ipAddresses['proxy'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
          $ipAddresses['user'] = $_SERVER["HTTP_X_FORWARDED_FOR"];

       } else {
          $ipAddresses['user'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
       }
       return $ipAddresses;
    }

    public function getSeller()
    {
        return $this->seller;
    }

    public function getCountries()
    {
        return $this->countries;
    }
}
?>
