<?php
$_LANG = array(
	'REGISTER_USER_ID'=>'ឈ្មោះសំគាល់',
	'REGISTER_SIGNUP'=>'ចុះឈ្មោះ',
	'REGISTER_BUSINESS_TYPE_STATUS' => 'ចុះឈ្មោះ',
	'REGISTER_BUSINESS_TYPE_PERSONAL' => 'ចុះឈ្មោះជាឯកជន',
	'REGISTER_BUSINESS_TYPE_DEALER' => 'Dealer Sign up',
	'REGISTER_BUSINESS_TYPE_SELLER' => 'Seller Sign up',
	'REGISTER_LASTNAME'=>'នាមត្រកូល',
	'REGISTER_EMAIL'=>'អ៊ីម៉ែល',
	'REGISTER_SEX'=>'ភេទ',
	'REGISTER_membership_type'=>'ប្រភេទសមាជិក',
	'REGISTER_CITY/PROVINCE'=>'ក្រុង / ខេត្ត',
	'REGISTER_ADDRESS'=>'អាស័យដ្ធាន',
	'REGISTER_COMPANY_DES'=>'ពិពណ៌នាអំពីរក្រុមហ៊ុន',
	'REGISTER_PASSWORD'=>'លេខសំងាត់',
	'REGISTER_FIRSTNAME'=>'នាមខ្លួន',
	'REGISTER_FAX'=>'ទូរសារ',
	'REGISTER_WEBSITE'=>'គេហទំព័រ',
	'REGISTER_CONFIRM_PASSWORD'=>'បញ្ជាក់លេខសំងាត់',
	'REGISTER_TERM'=>'ខ្ញុំយល់ព្រមជាមួយ',
	'REGISTER_COMPANY_NAME'=>'ឈ្មោះក្រុមហ៊ុន',
	'REGISTER_CONTACT_NAME'=>'ឈ្មោះទំនាក់ទំនង',
	'REGISTER_PHONE_NUMBER'=>'លេខទូរសព្ទ័',
	'REGISTER_SECURITY_QUESTION'=>'លេខសុវត្តិភាព:',
	'REGISTER_SECURITY_NUMBER'=>'វាយចំលើយ?',
	'REGISTER_SUBMIT'=>'បង្កើត',
	//Login Translate
	'LOGIN_STATUS'=>'ចូលក្នុង',
	'LOGIN_ID'=>'អត្តលេខ',
	'LOGIN_PASSWORD'=>'លេខសំងាត់',
	'LOGIN_KEEP_ME_SIGNIN'=>'ចងចាំខ្ញុំ',
	'LOGIN_FORGOT_ID'=>'ភ្លេចអត្តលេខ',
	'LOGIN_FORGOT_PASSWORD'=>'ភ្លេចលេខសំងាត់',
	
	
	'REGISTER_BUSINESS_FIELD_STATUS' => 'Please check your business field.',
	'REGISTER_CARD_ID_STATUS' => 'The field Card ID is required.',
    'REGISTER_COMPLETE_STATUS' => 'Account registered successfully! Please check your e-mail box to confirm the registration.',
    'REGISTER_COMPLETE_LABEL' => 'Your account has been registered successfully. Please check your e-mail box to authenticate your account registration.',
	'REGISTER_CONFIRMEMAIL_STATUS' => 'Your E-mail does not match.',
    'REGISTER_CONFIRMPASSWORD_STATUS' => 'Your password does not match.',
    'REGISTER_CONFIRMPASSWORD_LABEL' => 'Confirm Password',
	'REGISTER_COUNTRY_STATUS' => 'Please select country.',
    'REGISTER_DESC_LABEL' => 'The fields marked with the * symbol are required.',
    'REGISTER_ERROR_STATUS' => 'Unable to register account due an internal error. Please notify the system administrator.',
    'REGISTER_ERROR_LABEL' => 'Unable to register account due an internal error. Please notify the system administrator.',
    'REGISTER_EMAIL_ERROR_LABEL' => 'The e-mail address you are trying to use is already registered.',
	'REGISTER_EMAIL_ERROR_STATUS' => 'The e-mail address you are trying to use is already registered.',
    'REGISTER_EMAIL_STATUS' => 'The field e-mail is required.',
    'REGISTER_EMAIL_LABEL' => 'E-Mail Address',
    'REGISTER_FIELDALLFIELDS_LABEL' => 'Please fill all fields before submitting the form.',
    'REGISTER_FILLALLFIELDS_STATUS' => 'Please fill all fields before submitting the form.',
	'REGISTER_ID_STATUS' => 'The field ID is required.',
	'REGISTER_ID_LENGTH_STATUS' => 'The field ID is 4-12 characters',
	'REGISTER_ID_EXIST_STATUS' => 'The field ID existed',
	'REGISTER_ID_EXIST_LABEL' => 'The field ID existed',
	'REGISTER_INVALID_EMAIL_STATUS' => 'The E-mail is not valid.',
	'REGISTER_INVALID_EMAIL_LABEL' => 'The E-mail is not valid.',
	'REGISTER_INVALID_MOBILE_STATUS' => 'Invalid mobile numbers.',
	'REGISTER_INVALID_NAME_STATUS' => 'Your name must be English characters only.',
	'REGISTER_INVALID_NAME_LABEL' => 'Your name must be English characters only.',
	'REGISTER_INVALID_TEL_STATUS' => 'Invalid phone numbers.',
	'REGISTER_MEMBER_STATUS' => 'Please check your member type.',
	'REGISTER_MOBILE_STATUS' => 'The field mobile is required.',
    'REGISTER_NAME_LABEL' => 'Your Name',
    'REGISTER_NAME_STATUS' => 'The field name is required.',
    'REGISTER_PASSWORD_LABEL' => 'Your Password',
    'REGISTER_PASSWORD_STATUS' => 'Your password must have between 5 to 20 characters.',
	'REGISTER_TAB_TITLE' => 'Register a new account',
	'REGISTER_ADDRESS_STATUS' => 'Address is required.',
	'REGISTER_TEL_STATUS' => 'Telephone numbers is required.',
    'REGISTER_TERMSOFUSE_LABEL' => 'Terms of Use',
    'REGISTER_TERMSOFUSE_STATUS' => 'You must accept the terms of use to register your account.',
    'REGISTER_TERMSOFUSE_AGREEMENT_LABEL' => 'I have read and accept the <a href="'.BASE_RELATIVE.'view/lib/terms-of-use.html?iframe=true&width=800&height=600" rel="prettyPhoto">Terms of Use</a>.',
    'REGISTER_SECURITYQUESTION_LABEL' => 'Security Question',
    'REGISTER_SECURITYQUESTION_QUESTION' => 'How much is',
    'REGISTER_SECURITYQUESTION_STATUS' => 'Invalid answer.',
	'REGISTER_UPLOAD_ERROR_STATUS' => 'Cannot upload the picture. Make sure the extension is JPEG or PNG and its size is less than 350Kb',
	'REGISTER_UPLOAD_ERROR_LABEL' => 'Cannot upload the picture. Make sure the extension is JPEG or PNG and its size is less than 350Kb',
    'REGISTER_FORM_SUBMIT_BUTTON_LABEL' => 'Register Now',
    'SENDEMAIL_SUBJECT_LABEL' => 'Account Activation',
    'SENDEMAIL_ACTVATE_NOW' => 'Activate now',
    'SENDEMAIL_MESSAGE_REGISTRATION' => 'Thank you for registering your account with us!<br>Please click the link below to activate your account:<br>'
	
);