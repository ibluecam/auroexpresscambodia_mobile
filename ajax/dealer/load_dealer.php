<?php

if(!isset($_SESSION)) @session_start();

include('../../view/3rdparty/pagination-ajax/pagination.php');

$pagination_html="";

// assign webroot
$array_local=array();
$protocol = "http://".$_SERVER['SERVER_NAME'];
$request_uri = $_SERVER['REQUEST_URI'];
$array_local=(explode("/",$request_uri,2));
$array_local = $array_local[1];
$local_url = current(explode("/",$array_local));
if($_SERVER['SERVER_NAME']=='localhost'){
	$web_root = $protocol.'/'.$local_url.'/';	
}else {
	$web_root = "http://".$_SERVER['SERVER_NAME']."/";
}

function loadDealer(){
	require_once '../../config.php';
	require_once BASE_CLASS . 'class-connect.php';	

	$cnx = new Connect();
	$cnx->open();
	
	global $vehicle;
	global $pagination_html;        
	global $total_dealer;	

	// get dealer search	
	if(isset($_GET['s_dealer'])){
		$s_dealer=mysql_real_escape_string($_GET['s_dealer']);
		$s_dealer =str_replace('-',' ',$s_dealer);
	}else {
		$s_dealer="";
	}
			
	if(isset($s_dealer)){
		$where="WHERE (rg.company_name LIKE '%".$s_dealer."%' OR cs.seller_name LIKE '%".$s_dealer."%') AND rg.`activated` = '1' AND rg.`group` = 'user'";
	}else {
		$where="WHERE rg.`activated` = '1' AND rg.`group` = 'user'";
	}	
	
	$sql_search=("SELECT rg.user_id, rg.username, rg.email, rg.company_name, rg.mobile, rg.image, cs.user_id, cs.seller_name, cs.contact_no,
				(SELECT count(pd.owner) FROM product AS pd WHERE pd.is_deleted='0' AND pd.owner=rg.user_id) 
				AS total,
				(SELECT count(pd.owner) FROM product AS pd WHERE pd.is_deleted='0' AND pd.status='Sold' AND pd.owner=rg.user_id)
				AS total_sold,
				(SELECT count(pd.owner) FROM product AS pd WHERE pd.is_deleted='0' AND pd.status='Reserved' AND pd.owner=rg.user_id)
				AS total_reserved
				FROM register AS rg
				LEFT JOIN tbl_carfinder_sellers AS cs ON cs.user_id=rg.user_id		
				".$where." GROUP BY rg.user_id ORDER BY total DESC");		
	
	/////PAGINATION PROCESS//////

	$itemPerPage=15;
	$sql_count = @mysql_query($sql_search);
	$total_dealer= @mysql_num_rows($sql_count);

	$links = new Pagination ($total_dealer, $itemPerPage);
	$limit=$links->start_display;
	$pagination_html.= $links->display();

	///////PAGINATION PROCESS///////
	$sql_search.=$limit;
	// echo $sql_search;
	if( !$sql = @mysql_query($sql_search) ){
		require_once BASE_CLASS . 'class-log.php';
		LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
		$cnx->close();
		return;
	}

	echo "<script>";
	echo "var itemCountThisPage=".@mysql_num_rows($sql).";";
	echo "var itemPerPage=".$itemPerPage.";";
	echo "var currentOffset=".$links->start.";";
	echo "</script>";
	
	if( @mysql_num_rows($sql) < 1 ){
		$cnx->close();
	}

	$vehicle = array();
	
	while( $r = @mysql_fetch_assoc($sql) ){
		$total_count = $r['total'];		
		
		$mysql = "select * from more_image where owner ='".$r['user_id']."' and primary_photo=1";
		$myquery = @mysql_query($mysql);						
		
		if (@mysql_num_rows($myquery)>0){
			$myimage = @mysql_fetch_assoc($myquery);
			$vehicle[]=array('user_id'=>$r['user_id'],'username'=>$r['username'],'company_name'=>$r['company_name'],'email'=>$r['email'],
			'mobile'=>$r['mobile'],
			'seller_name'=>$r['seller_name'],
			'contact_no'=>$r['contact_no'],
			'count'=>$total_count,
			'total_sold'=>$r['total_sold'],
			'total_reserved'=>$r['total_reserved'],
			'image'=>$myimage['m_image']
			);	
		}else{
			$vehicle[]=array('user_id'=>$r['user_id'],'username'=>$r['username'],'email'=>$r['email'],
			'mobile'=>$r['mobile'],
			'company_name'=>$r['company_name'],
			'seller_name'=>$r['seller_name'],
			'contact_no'=>$r['contact_no'],
			'count'=>$total_count,
			'total_sold'=>$r['total_sold'],
			'total_reserved'=>$r['total_reserved'],
			'image'=>""
			);			
		}	
	}		
	
	@mysql_free_result($sql);
	$cnx->close();		
}

loadDealer();
?>

<?php $i=1; if(count($vehicle)>0){ ?>
	<form id="load_form" method="get" action="dealer" >
		<input type="hidden" class="formSelection" name="s_dealer" value="<?php if(isset($_GET['s_dealer'])) echo htmlspecialchars($_GET['s_dealer']);?>" />
		<input type="hidden" class="formSelection" name="s" id="offset" value="<?php if(isset($_GET['s'])) echo htmlspecialchars($_GET['s']);?>"/>

		<ul class="clearfix">
			<?php
				$i=1;
				if(count($vehicle)>0){
				foreach($vehicle as $dealer){				
			?>

			<li class="linkimg"><a href="company-detail/<?php echo $dealer['user_id'];?>" title="<?php echo $dealer['company_name'];?>">
				<table class='dealer-block'>
					<tr>                
						<td width="20%">
							<?php
								if($dealer['image'] == ""){
									echo '<img width="110px" height="110px" style="border:1px solid #333" src="'.$web_root.'images/dealer_sample.png" />';
							?>
							<?php }else{ ?>
					        <?php 
								$furl=explode('/',$dealer['image']);
								$fname=end($furl);
								$furl= BASE_ROOT . '/upload/profile/'.$fname;
								
						        if(file_exists($furl)){
								
						   ?>
					    
						        <img src="<?php echo $dealer['image'] ; ?>" width="110px" height="110px" style="border:none" alt="<?php if(!empty($dealer['company_name'])) echo $dealer['company_name']; else echo $dealer['seller_name'];?>" />
							<?php } else {?>
							    <img width="110px" height="110px" style="border:1px solid #333" src="<?php echo $web_root.'images/dealer_sample.png' ;?>" />
							<?php 
								} 
							}
							?>
																						
					    </td>

						<td id="wrap-detail">
						<?php if(!empty($dealer['company_name'])){ ?>
						<p id="c_name"> 
							<span>
								<?php 
									$count_len = strlen($dealer['company_name']);
									
									if($count_len <= 20){
										echo $dealer['company_name'];
									}else{
										echo substr($dealer['company_name'],0,17)."..."; 
									}
								?>
							</span>
						</p>
						
						<?php }else if(!empty($dealer['seller_name'])){ ?> 
						<p id="c_name"> 
							<span>
								<?php 
									$count_len = strlen($dealer['seller_name']);
									
									if($count_len <= 20){
										echo $dealer['seller_name'];
									}else{
										echo substr($dealer['seller_name'],0,17)."..."; 
									}
								?>
							</span>
						</p>
						<?php } ?>
					
						<div class='line'></div>										
						
						<p class='tel'>
						<?php 
							if(!empty($dealer['mobile'])){
								echo "Tel : ". "<a href='tel:".$dealer['mobile']."'>".$dealer['mobile']."</a>". "<br/>";
							}else if(!empty($dealer['contact_no'])){
								echo "Tel : ". "<a href='tel:".$dealer['contact_no']."'>".$dealer['contact_no']."</a>". "<br/>";
							}  
						?></p>
						
						<p class='email'>
						<?php if(!empty($dealer['email'])){
							echo "Email : ".$dealer['email'];} 
						?></p>
						
						<p class='p total'>
							<?php 
								if(!empty($dealer['count'])){
							echo "Total Vehicle : ".$dealer['count']." Cars";
							}else{
								echo "Total Vehicle : 0 Cars";
								} 
							?>
						</p>
						<p class="statusCar">
							<span class="reserved">Reserved : <?php echo $dealer['total_reserved'];?></span><span class="soldOut">Sold Out : <?php echo $dealer['total_sold'];?></span>
						</p>
						</td>
					</tr>
				</table>		
			</a></li>
				
			
			<?php } } ?>
		</ul>
	</form>	
	
	<div id="pagegination">		
		<?php
			echo "<p style='clear:both;'>$pagination_html</p>";
		?>
	</div>

<?php }else{
	echo "<div class='warning-label'>No items found!</div>";
} ?>

</div>

<script>
	// link images
	$('.linkimg').hover(function(){
		$(this).stop().animate({'opacity':0.7}, 500);
	}, function(){
		$(this).stop().animate({'opacity':1}, 500);
	});
</script>				