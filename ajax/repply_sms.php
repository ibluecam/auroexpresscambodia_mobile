<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

 $old_msg=$_POST['old_msg'];
 $m_id=$_POST['m_id'];

// connect to database.
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();


if( !@mysql_query("UPDATE message SET message='{$old_msg}' WHERE `id`=$m_id ;") ){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to remove car image due a query error at ' . __FILE__ . ':' . __LINE__);
    $cnx->close();
}



