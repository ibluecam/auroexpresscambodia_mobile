<?php

        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if( !$sql = @mysql_query("SELECT * FROM `country_list` INNER JOIN product on product.location=country_list.cc
        GROUP BY cc ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->country_list = array();

            return;
        }


        echo "<option value=''>--Country--</option>";
        while( $r = @mysql_fetch_assoc($sql) ){
            $select="";
            if($country==$r['cc']) $select="Selected";
                  echo "<option style='background-image:url(images/flag/{$r['cc']}.png); background-repeat: no-repeat;padding-left:18px;' $select value='{$r['cc']}'>{$r['country_name']}</option>";
        }

        @mysql_free_result($sql);
        $cnx->close();

    ?>
