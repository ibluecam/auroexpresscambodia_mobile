<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class userMyInquiry
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'my-inquiry';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	protected $listitem;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;
    /*
	
     * Constructor -------------------------------------------------------------
     */
    public function userMyInquiry($lang=array())
    {
        $this->lang = $lang;
		$this->listinquiry();
		if(isset($_GET['btnsearch'])){
           $this->listinquiry(); 
		}
        if(isset($_GET['action'])){
            $this->delete_inquiry(); 
        }
    }

    public function delete_inquiry()
    {
        if( $_GET['action']=='delete')
        {
            if(isset($_GET['inquery_id'])){
                $id=$_GET['inquery_id'];
				$owner_id=$_SESSION['log_id'];
                require_once BASE_ROOT . 'core/class-connect.php'; 
                $cnx = new Connect();
                $cnx->open();
                $query="DELETE FROM `inquiry` WHERE `id`='$id' and owner='$owner_id'";
                if( !$sql = @mysql_query($query) )
                {
                    $cnx->close(); 
                    require_once BASE_ROOT . 'core/class-log.php'; 
                    LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());        
                    $this->form_message = 'Unable to load page content due an internal error.';
                    $this->form_status = true;
                    $this->form_style = 'alert-error'; 
                    return;
                }
                $cnx->close();
                //header('Location:'.BASE_RELATIVE.'my-inquiry');
            }
        }
    }
	public function listinquiry()
	{
		 require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $id="";
        if(isset($_SESSION['log_id']))
        {
            $id=$_SESSION['log_id'];
        }
		if(isset($_GET['btnsearch'])){
			
			$type="";
            $make="";
			if(isset($_GET['radio']))
            {$type=$_GET['radio'];
				 }
            if(isset($_GET['select_left']))
            {$make=$_GET['select_left'];
			 }
            $query="";
			if($type=="all" && $make==""){
				$query="SELECT * FROM `inquiry` WHERE `owner`='$id' ";
			
			}
			elseif($type=="all" && $make!=""){
				$query="SELECT * FROM `inquiry` WHERE `make`='$make' AND `owner`='$id'";
				}
		
			else{
				$query="SELECT * FROM `inquiry` WHERE (`make`='$make' OR `vehicle_type`='$type') AND `owner`='$id'";
				}

            /////PAGINATION PROCESS//////

            $sql_count_str=$query;
            $sql_count = @mysql_query($sql_count_str);
            $this->total_num_row= @mysql_num_rows($sql_count);
            
            $links = new Pagination ($this->total_num_row);
            $limit=$links->start_display;
            $this->pagination_html= $links->display(); 
            $this->current_page = $links->currentPage();
            $this->total_page= $links->numPages();
            ///////PAGINATION PROCESS///////
            $query.=$limit;  

			if( !$sql = @mysql_query($query) )
				{
					$cnx->close();
		
					require_once BASE_ROOT . 'core/class-log.php';
		
					LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
		
					$this->form_message = 'Unable to load page content due an internal error.';
					$this->form_status = true;
					$this->form_style = 'alert-error';
		
					return;
				}
				
		 }
		 else{
			 
            $query= "SELECT * FROM `inquiry` WHERE `owner`='$id'";
            /////PAGINATION PROCESS//////

            $sql_count_str=$query;
            $sql_count = @mysql_query($sql_count_str);
            $this->total_num_row= @mysql_num_rows($sql_count);
            
            $links = new Pagination ($this->total_num_row);
            $limit=$links->start_display;
            $this->pagination_html= $links->display(); 
            $this->current_page = $links->currentPage();
            $this->total_page= $links->numPages();
            ///////PAGINATION PROCESS///////
            $query.=$limit;  


			 if( !$sql = @mysql_query($query) )
				{
					$cnx->close();
		
					require_once BASE_ROOT . 'core/class-log.php';
		
					LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
		
					$this->form_message = 'Unable to load page content due an internal error.';
					$this->form_status = true;
					$this->form_style = 'alert-error';
		
					return;
				}
			 }
		

         $this->listitem = array();
        while( $r = @mysql_fetch_assoc($sql) ){

            array_push($this->listitem, $r);
     
        }
        //var_dump($this->listarray);
        return $this->listitem;
	}
	

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
