<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$deal_week = $_CLASS->loadDealweek();

?>




<div id="sectionContenWrapper">
        	<div id="carMenu">
            	<ul class="clearfix">
                	<li class="first_child"><a href="#" class="linkfade">Find Trader</a> > </li>
                    <li class="menu">
                    	Buyer's Directory
                  </li>
                </ul>
            </div>
        	<!--sectionSidebar--> 
            
            <?php 				
				include("php/sidebar/findtrader.php");	 	
				include("php/sidebar/bottom.php");	 				 						 			
			?>           
        	<!--end sectionSidebar-->
            
            <div id="sectionContent" class="clearfix">            	
                <div id="buyerTitle">
					<p>Buyer's Directory</p>                	
                </div>
                <div id="member">
					<p>32,504 Members</p>                	
                </div>
                <div id="flagCountWrap" class="clearfix">
                	
                	<div class="flagCount">
                    	<ul>
                        <?php for($i=0;$i<14;$i++){?>  
                        	<li>
                            	<span><img src="<?php echo BASE_RELATIVE;?>images/common/flag/russia.png" /></span>
                                <p><a href="#" class="linkfade">Russia (4,836)</a></p>                                
                            </li>
                        <?php }?>
                        </ul>    
                    </div>                
                    
                </div>
                <div id="moreCountry" class="clearfix">
                	<div id="dropdown">
                    	<p><a href="#" class="linkfade allCountry">More Countries</a></p>
                    </div>
                </div>
				<div id="allCountry" class="clearfix">
                	
                	<div class="flagCount">
                    	<ul>
                            <?php for($i=0;$i<14;$i++){?>  
                            <li>
                                <span><img src="<?php echo BASE_RELATIVE;?>images/common/flag/russia.png" /></span>
                                <p><a href="#" class="linkfade">Russia (4,836)</a></p>                                
                            </li>
                        <?php }?>
                        </ul>
                    </div>                
                    
                </div>
                <div id="itemCounter">
                	<ul>
                    	<?php for($i=0;$i<15;$i++){
						?>
                    	<li><a href="#" class="linkfade">Car (22,029)</a> |</li>
                        
                        <?php }?>
                    </ul>
                </div>
                <div id="searchBoxWrap" class="clearfix">
                	<form>
                    	<input type="text" class="searchBox"  placeholder=" Keywords..." />
                    </form>
                    <div id="searchButton">
                    	<input type="submit" value="" class="seach_btn">
                    </div>
                </div>
                <div id="buyerWrap" class="clearfix">
                	<?php 
							for($i=1;$i<12;$i++){
						?>
                	<div class="buyDetailWrap clearfix <?php if(($i % 2) == 0){ echo ' buyDetailWrapRight';}?>">
                    	<div class="buyerDetail">
                        	<p class="title">DAE SHIN SOLUTION</p>
                            <p><span><img src="<?php echo BASE_RELATIVE;?>images/common/flag/korea.png" /></span> S.Korea</p>
                            <p>ID : asiagrand</p>
                            <p>Name : PAK SERGEY </p>
                        </div>
                        <div class="buyerLogo">
                        	<img src="<?php echo BASE_RELATIVE;?>images/buyerdirectory/buyerlogo.png" />
                            <p><a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade">Visit Buyer's Homepage</a></p>
                        </div>
                        <div class="buyAddress">
                            <p>
                                Уважаемые, клиенты! Компания "Азия Гранд" рада представить Вашему вниманию полный перечень услуг в сфере прио... 
                            </p>
	                    </div>
                        <div class="moreBuyer">
                        	<p>
                            	<a href="#" class="linkfade"><< More >></a>
                            </p>
                        </div>
                    </div>
                    <?php }?>
                </div>
                
                
                <div id="pagernation">
                	Pager
                </div>
                      
        	</div><!-- end div id="sectionContent" -->  
        </div>      