
    $(document).ready(function(e) {


    		if(search!=''){
				$('#search_title p a#ad_search').addClass("current");
		        $('#search_title p a#quick_search').removeClass("current");
		        $('#second_search #search_wrap').css('display','none');
		        $('#second_search #advance_search').css('display','block');
			}
		    if( $('#make').length ){
		    	$('#make').change(function(e) {
					make=$('#make option:selected').val();
					model="";
	                loadModel();
	            });
	            $('#make_adv').change(function(e) {
					make=$('#make_adv option:selected').val();
					model="";
	                loadModel();
	            });
		    }
		    $('.listCar').mouseover(function() {
		    	$(this).find('.modeButton').css("display", "block");
		    }).mouseout(function(){
		    	$(this).find('.modeButton').css("display", "none");

		   	});
		   	$('#model_adv').change(function(e) {
					model=$(this).val();
					$('#model').val(model);
	            });
		   	$('#model').change(function(e) {
					model=$(this).val();
					$('#model_adv').val(model);
	            });
		    if( $('#category').length ){
				$('#category').change(function(e) {
					category=$('#category option:selected').val();
					loadSubCategory();
	            });
			}
			if(transmission!=""){
				$("#transmission_adv").val(transmission);
			}
			if(vehicle_type!=""){
				$("#vehicle_type_adv").val(vehicle_type);
			}
			if(country!=""){
				$("#country_adv").val(country);
			}
			if( $('#condition').length ){
				if(condition!="") $("#condition").val(condition);
				if(condition!="") $("#condition_adv").val(condition);
			}

			if(fuel_type!="") {
				$("#fuel_type").val(fuel_type);
				$("#fuel_type_adv").val(fuel_type);
			}
			if(engine_volume_from!=""){
				$("#engine_volume_from_adv").val(engine_volume_from);
			}
			if(engine_volume_to!=""){
				$("#engine_volume_to_adv").val(engine_volume_to);
			}
			if( $('#category').length ){
				loadSubCategory();

			}

			if( $('#make').length ){
				loadModel();
			}

			if( $('#steering').length ){
				if(steering!="") $("#steering").val(steering);
				if(steering!="") $("#steering_adv").val(steering);
			}
			if( $('#country').length ){
				if(country!="") $('#country').val(country);
				if(country!="") $('#country_adv').val(country);
			}
			if( $('#year_to').length ){
				if(year_to!="") $('#year_to').val(year_to);
				if(year_to!="") $('#year_to_adv').val(year_to);
			}

			if( $('#year_from').length ){
				if(year_from!="") $('#year_from').val(year_from);
				if(year_from!="") $('#year_from_adv').val(year_from);
			}
				if(month_from!="") $('#month_from_adv').val(month_from);


				if(month_to!="") $('#month_to_adv').val(month_to);

			if( $('#car_id').length ){
				if(car_id!="") $('#car_id').val(car_id);
			}
			if( $('#customSearch').length ){
				if(customSearch!="") $('#customSearch').val(customSearch);
				if(customSearch!="") $('#customSearch_adv').val(customSearch);
			}

    });

	function loadSubCategory(){
		if(category!=""){
			$("#category").val(category);
			$("#category_adv").val(category);

		}
	}
	function loadModel(){
			$("#make").val(make);
			$("#make_adv").val(make);
			if( $('#model').length ){
				$('#model, #model_adv').load('ajax/load_car_model.php?make='+encodeURIComponent(make)+'&product_type='+encodeURIComponent(product_type), function(e){
					if(model!="") $("#model").val(model);
					if(model!="") $("#model_adv").val(model);
				});

			}
	}
