<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class usermyfavorites {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $favorites;
    protected $car;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function usermyfavorites($lang=array()){
        $this->lang = $lang;
        
        // handle remove form button.
        if( isset($_POST['rInput']) ){
            $this->handleForm();
        }
        
        // load favorites list.
        $this->loadFavorites();
    }
    
    /**
     * Private method: handle remove item from favorites
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleForm(){
        $cid = trim($_POST['cID']);
        
        if( empty($cid) ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query("DELETE FROM `user_favorites` WHERE `user_id`='".$_SESSION['log_id']."' AND `car_id`='$cid' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            
            LogReport::write('Unable to remove item from favorite list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['FAV_REMOVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['FAV_REMOVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
    /**
     * Private method: load favorites list and car list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadFavorites(){
        $this->favorites = array();
        $this->car = array();
        
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `user_favorites` WHERE `user_id`='".$_SESSION['log_id']."'") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            
            LogReport::write('Unable to load favorite list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['FAV_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $day = explode('-',$r['date']);
            $year = $day[0];
            $month = $day[1];
            $day = $day[2];
            
            $obj = array(
                'id'        => $r['id'],
                'user_id'   => $r['user_id'],
                'car_id'    => $r['car_id'],
                'date'      => Utilities::checkDateFormat($year, $month, $day, $format)
            );
            
            array_push($this->favorites, $obj);
        }
        
        @mysql_free_result($sql);
        
        // load car list.
        if( !$sql = @mysql_query("SELECT `id`,`maker`,`model`,`year`,`status` FROM `car` WHERE `status`='0'") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            $this->favorites = array();
            
            LogReport::write('Unable to load car list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['FAV_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id'    => $r['id'],
                'maker' => stripslashes($r['maker']),
                'model' => stripslashes($r['model']),
                'year'  => $r['year']
            );
            
            array_push($this->car,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        
        return;
    }
    
    /**
     * Public method: get car by id
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarByID($id){
        $id = trim($id);
        
        if( empty($id) ){
            return array();
        }
        
        if( count($this->car) < 1 ){
            return array();
        }
        
        $result = array();
        
        for( $i=0; $i < count($this->car); $i++ ){
            if( $id == $this->car[$i]['id'] ){
                $result = array(
                    'id' => $this->car[$i]['id'],
                    'maker' => $this->car[$i]['maker'],
                    'model' => $this->car[$i]['model'],
                    'year' => $this->car[$i]['year']
                );

                break;
            }
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        // get first image from media.
        if( !$sql = @mysql_query("SELECT `car_id`,`mode`,`source` FROM `car_media` WHERE `car_id`='".$result['id']."' AND `mode`='exterior' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            $cnx->close();
            LogReport::write('Unable to load car media due a query error at ' . __FILE__ . ':' . __LINE__);
            $result['img'] = BASE_RELATIVE . 'image/default_main_image.jpg';
            
            return $result;
        }
        
        if( @mysql_num_rows($sql) != 1 ){
            $cnx->close();
            $result['img'] = BASE_RELATIVE . 'image/default_main_image.jpg';
            return $result;
        }
        
        $r = @mysql_fetch_assoc($sql);
        @mysql_free_result($sql);
        $cnx->close();
        
        $result['img'] = BASE_RELATIVE . $r['source'];
        
        return $result;
    }
    
    /**
     * Public method: get favorites list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getFavList(){
        return $this->favorites;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
