<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.

$pg_details = $_CLASS->getPageHTML();
$status = $_CLASS->getstatus();

 

if( $status == 'query_dealweek' || $status =='query_media' )
    {
      FeedbackMessage::displayOnPage('Submit Sucessfully', 'success');
?>
<p class="alert alert-success" style="text-align:center;"><strong><?php echo  "SUBMIT COMPLETE";?></strong></p>

<?php
    }

if($status=='error'){
    ?>    
    <p class="alert alert-error" style="text-align:center;"><strong><?php echo 'Please fill all fields before submitting the form.';?></strong></p>
 
 <?php }


?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > <a href="#" class="linkfade">Deal of the Week</a> > Register</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Deal of the Week</p>
                </div>
                <div id="deal_register" class="clearfix">
                	<form action="<?php echo BASE_RELATIVE;?>register-dealoftheweek" method="Post" enctype="multipart/form-data">
                	<div id="title"><p>Title</p></div>
                   
                    <input type="text" class="text_box" name="rd_title"  />
                 
                    <div id="message">
                    	<p>Message</p>
                    </div>
                    <div id="text_editor">
                    	<textarea id="_ebody" name="rd_htmlInput"></textarea>
                    </div>
                    <div id="attach" class="clearfix">                    	                     
                        <div class='file_upload' id='f1'><input name='myfile' type='file'  /></div>
                        <div id='file_tools' style="padding:10px; float:left;">                        	
                            <p> Select your file</p>
                        </div>
                    </div>
                    <div id="button_wrap">
                    	<div id="upload">
                        	<input type="submit" class="upload" value="" name="deal_upload" />
                            <div id="back">
                            	<a href="<?php echo BASE_RELATIVE;?>dealof-theweek"><img src="<?php echo BASE_RELATIVE;?>images/community/back.png" /></a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
               
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  