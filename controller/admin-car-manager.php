<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class admincarmanager {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $car_list;
    private $media_list;
    private $total_entries;
    private $results_per_page = 20;  // set number of results per page.
    private $body_list;
    private $total_pages;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function admincarmanager($lang=array()){
        $this->lang = $lang;
        
        // handle update form.
        if( isset($_POST['updatebtn']) ){
            $this->handleUpdateForm();
        }
        
        // handle delete form.
        if( isset($_POST['removebtn']) ){
            $this->handleRemoveVehicleForm();
        }
        
        // handle mark as sold form.
        if( isset($_POST['soldbtn']) ){
            $this->handleMarkAsSoldForm();
        }
        
        // handle reset sold status.
        if( isset($_POST['resetbtn']) ){
            $this->handleResetSoldStatus();
        }
        
        // load car list.
        $this->loadCarList();
        
        // load body list.
        $this->loadBodyList();
    }
    
    /**
     * Private method: handle reset sold status button
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleResetSoldStatus(){
        $car_id = trim($_POST['caridInput']);
        
        if( empty($car_id) ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !@mysql_query("UPDATE `car` SET `status`='0' WHERE `id`='$car_id' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to update vehicle status due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['CAR_MAN_SOLD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['CAR_MAN_RESET_SOLD_QUERY'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        // update sitemap.
        require_once BASE_CLASS . 'class-sitemap.php';
        $sitemap = new SiteMap();
        
        if( !$sitemap->generate() ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to generate sitemap due an error at ' . __FILE__ . ':' . __LINE__);
        }
        
        return;
    }
    
    /**
     * Private method: handle mark as sold form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleMarkAsSoldForm(){
        $car_id = trim($_POST['caridInput']);
        
        if( empty($car_id) ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $car_id = @mysql_real_escape_string($car_id);
        
        if( !@mysql_query("UPDATE `car` SET `status`='2' WHERE `id`='$car_id' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to change vehicle status due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['CAR_MAN_SOLD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['CAR_MAN_SOLD_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        // update sitemap.
        require_once BASE_CLASS . 'class-sitemap.php';
        $sitemap = new SiteMap();
        
        if( !$sitemap->generate() ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to generate sitemap due an error at ' . __FILE__ . ':' . __LINE__);
        }
        
        return;
    }
    
    /**
     * Private method: handle remove vehicle form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleRemoveVehicleForm(){
        $car_id = trim($_POST['caridInput']);
        
        if( empty($car_id) ){
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $car_id = @mysql_real_escape_string($car_id);
        
        // remove from car table.
        if( !@mysql_query("DELETE FROM `car` WHERE `id`='$car_id' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to remove vehicle due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['CAR_MAN_REMOVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $cnx->close();
            return;
        }
        
        // remove from car details table.
        if( !@mysql_query("DELETE FROM `car_details` WHERE `car_id`='$car_id' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to remove vehicle details due a query error at ' . __FILE__ . ':' . __LINE__ . ' Remove car_id ' . $car_id . ' from car_details table manually!');
        }
        
        // remove from car feature table.
        if( !@mysql_query("DELETE FROM `car_features` WHERE `car_id`='$car_id'") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to remove vehicle features due a query error at ' . __FILE__ . ':' . __LINE__ . ' Remove car_id ' . $car_id . ' from car_features table manually!');
        }
        
        // load media list from media table to remove images after query.
        $img = array();
        $remove = false;
        
        if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE `car_id`='$car_id'") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load image list to remove vehicle media due a query error at ' . __FILE__ . ':' . __LINE__ . ' Remove car_id ' . $car_id . ' from car_media table and images manually!');
        }
        else {
            if( @mysql_num_rows($sql) > 0 ){
                while( $r = @mysql_fetch_assoc($sql) ){
                    if( $r['type'] == 'image' ){
                        array_push($img, $r['source']);
                    }
                }
                
                @mysql_free_result($sql);
            }
            
            $remove = true;
        }
        
        if( $remove ){
            // remove from car media table.
            if( !@mysql_query("DELETE FROM `car_media` WHERE `car_id`='$car_id'") ){
                require_once BASE_CLASS . 'class-log.php';
                LogReport::write('Unable to remove vehicle media due a query error at ' . __FILE__ . ':' . __LINE__ . ' Remove car_id ' . $car_id . ' from car_media table manually!');
            }
        }
        
        $cnx->close();
        
        // remove images.
        if( count($img) > 0 ){
            for( $i=0; $i < count($img); $i++ ){
                if( file_exists(BASE_ROOT . $img[$i]) ){
                    @unlink(BASE_ROOT . $img[$i]);
                }
            }
        }
        
        $this->form_message = $this->lang['CAR_MAN_REMOVE_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        // update sitemap.
        require_once BASE_CLASS . 'class-sitemap.php';
        $sitemap = new SiteMap();
        
        if( !$sitemap->generate() ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to generate sitemap due an error at ' . __FILE__ . ':' . __LINE__);
        }
        
        return;
    }
    
    /**
     * Private method: handle update form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleUpdateForm(){
        $car_id = trim($_POST['caridInput']);
        $maker  = trim($_POST['makerInput']);
        $model  = trim($_POST['modelInput']);
        $bodyId = (int)$_POST['bodyInput'];
        $year   = (int)$_POST['yearInput'];
        $measure= trim($_POST['measureInput']);
        $miles  = (int)$_POST['milesInput'];
        $door   = (int)$_POST['doorInput'];
        $feat   = (bool)$_POST['featuredInput'];
        $priceThousand  = (int)$_POST['priceThousandInput'];
        $priceHundred   = (int)$_POST['priceHundredInput'];
        $priceDecimal   = (int)$_POST['priceCentInput'];
        $energy = trim($_POST['energyInput']);
        
        // validate.
        if( empty($car_id) ){
            $this->form_message = $this->lang['CAR_MAN_INVALID_CARID_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( empty($maker) ){
            $this->form_message = $this->lang['CAR_MAN_INVALID_MAKER_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( empty($model) ){
            $this->form_message = $this->lang['CAR_MAN_INVALID_MODEL_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( $bodyId < 1 ){ $bodyId = '0'; }
        
        if( $year < 1900 ){
            $year = 1900;
        }
        
        switch($measure){
            case 'km':
            case 'miles':
                break;
            default:
                $measure = 'miles';
        }
        
        if( $miles < 1 ){ $miles = '0'; }
        
        if( $door < 1 ){ $door = 1; }
        
        ( $feat ? $feat = '1' : $feat = '0' );
        
        if( empty($priceThousand) && empty($priceHundred) && empty($priceDecimal) ){
            $priceDecimal = 99;
        }
        
        if( empty($priceThousand) || $priceThousand < 1 ){
            $priceThousand = '';
        }
        if( $priceThousand > 999 ){
            $priceThousand = 999;
        }
        
        if( empty($priceHundred) ){
            $priceHundred = '000';
        }
        else if( $priceHundred < 10 ){
            $priceHundred = '00' . (string)$priceHundred;
        }
        else if( $priceHundred < 100 ){
            $priceHundred = '0' . (string)$priceHundred;
        }
        else if( $priceHundred > 999 ){
            $priceHundred = 999;
        }
        
        if( empty($priceDecimal) ){
            $priceDecimal = '00';
        }
        else if( $priceDecimal < 10 ){
            $priceDecimal = '0' . (string)$priceDecimal;
        }
        else if( $priceDecimal > 99 ){
            $priceDecimal = 99;
        }
        
        switch($energy){
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
                break;
            default:
                $energy = 'A';
        }
        
        $price = $priceThousand . $priceHundred . '.' . $priceDecimal;
        
        // update database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $maker = @mysql_real_escape_string($maker);
        $model = @mysql_real_escape_string($model);
        $car_id= @mysql_real_escape_string($car_id);
        
        if( !@mysql_query("UPDATE `car` SET `maker`='$maker',
                                            `model`='$model',
                                            `body_type`='$bodyId',
                                            `price`='$price',
                                            `year`='$year',
                                            `miles`='$miles',
                                            `measure_type`='$measure',
                                            `doors`='$door',
                                            `featured`='$feat',
                                            `eco`='$energy' WHERE `id`='$car_id' LIMIT 1;") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to update vehicle information due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['CAR_MAN_UPDATE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $cnx->close();
        $this->form_message = $this->lang['CAR_MAN_UPDATE_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        // update sitemap.
        require_once BASE_CLASS . 'class-sitemap.php';
        $sitemap = new SiteMap();
        
        if( !$sitemap->generate() ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to generate sitemap due an error at ' . __FILE__ . ':' . __LINE__);
        }
        
        return;
    }
    
    /**
     * Private method: load body list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadBodyList(){
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( !file_exists($path) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Body type translation requested at ' . __FILE__ . ':' . __LINE__ . '. File ['.$path.'] could not be found!');
            return $this->lang['CAR_MAN_UNDEFINED_LABEL'];
        }
                
        require_once $path;
        $this->body_list = array();
        
        foreach($_CAR_BODY as $k => $v){
            $obj = array(
                $k => $v
            );
            array_push($this->body_list, $obj);
        }
        
        if( !is_array($this->body_list) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Body type translation requested at ' . __FILE__ . ':' . __LINE__ . '. File ['.$path.'] could not be found!');
            return $this->lang['CAR_MAN_UNDEFINED_LABEL'];
        }
    }
    
    /**
     * Private method: load car list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadCarList(){
        // get pagination current page.
        ( isset($_GET['pg']) ? $pg = (int)$_GET['pg'] : $pg = 1 );
        if( $pg < 1 ){ $pg = 1; }
        
        // get number of entries.
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $this->car_list = array();
        
        if( !$sql = @mysql_query("SELECT `id`,`status` FROM `car`") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to load car list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['CAR_MAN_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->total_entries = @mysql_num_rows($sql);
        
        if( $this->total_entries < 1 ){
            $cnx->close();
            return;
        }
        
        $pageNumber = ceil($this->total_entries/$this->results_per_page);
        $this->total_pages = $pageNumber;
        if( $this->total_pages < 1 ){ $this->total_pages = 1; }
        
        if( $pg > $this->total_entries ){ $pg = $this->total_entries; }
        
        // set query limit.
        $LIMIT = ' LIMIT ' . ($pg - 1) * $this->results_per_page . ',' . $this->results_per_page;
        
        // load list.
        if( !$sql = @mysql_query("SELECT * FROM `car` ORDER BY `status` ASC $LIMIT;") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to load car list due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['CAR_MAN_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        //  set car id array.
        $carid_arr = array();
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id'            => $r['id'],
                'maker'         => stripslashes($r['maker']),
                'model'         => stripslashes($r['model']),
                'body_type'     => $r['body_type'],
                'price'         => $r['price'],
                'year'          => $r['year'],
                'miles'         => $r['miles'],
                'measure_type'  => $r['measure_type'],
                'doors'         => $r['doors'],
                'status'        => (int)$r['status'],
                'featured'      => (bool)$r['featured'],
                'eco'           => $r['eco']
            );
            array_push($this->car_list,$obj);
            array_push($carid_arr,$r['id']);
        }
        
        @mysql_free_result($sql);
        
        // load first car image list from car_media table.
        $this->media_list = array();
        
        for( $i=0; $i < count($carid_arr); $i++ ){
            if( !$sql = @mysql_query("SELECT `car_id`,`mode`,`source` FROM `car_media` WHERE `car_id`='".$carid_arr[$i]."' AND `mode`='exterior' LIMIT 1;") ){
                // fail silently.
                require_once BASE_CLASS . 'class-log.php';
                $cnx->close();
                LogReport::write('Unable to load car media list due a query error at ' . __FILE__ . ':' . __LINE__);
                break;
            }
            
            if( @mysql_num_rows($sql) == 1 ){
                $r = @mysql_fetch_assoc($sql);
                @mysql_free_result($sql);
                
                $obj = array(
                    'car_id' => $r['car_id'],
                    'source' => $r['source']
                );
                
                array_push($this->media_list,$obj);
            }
        }
        
        $cnx->close();
        return;
    }
    
    /**
     * Public method: get body type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getBodyTypeList(){
        return $this->body_list;
    }
    
    /**
     * Public method: get body type translation by body-type id
     * <br>---------------------------------------------------------------------
     * @param $bid The body type id.
     * @return string The body type name
     */
    public function getBodytypeById($bid){
        $res;
        for( $i=0; $i < count($this->body_list); $i++ ){
            if( $i == $bid ){
                $res = $this->body_list[$i][$i];
                break;
            }
        }
        
        if( empty($res) ){
            $res = $this->lang['CAR_MAN_UNDEFINED_LABEL'];
        }
        
        return $res;
    }
    
    /**
     * Public method: get car image by car id
     * <br>---------------------------------------------------------------------
     * @param $cid The car id.
     * @return string The image relative URI.
     */
    public function getCarMediaById($cid){
        $result = 'image/default_main_image.jpg';
        
        if( count($this->media_list) < 1 ){
            return $result;
        }
        
        for( $i=0; $i < count($this->media_list); $i++ ){
            if( $this->media_list[$i]['car_id'] == $cid ){
                $result = $this->media_list[$i]['source'];
                break;
            }
        }
        
        if( empty($result) ){
            $result = 'image/default_main_image.jpg';
        }
        
        return $result;
    }
    
    /**
     * Public method: get car list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarList(){
        return $this->car_list;
    }
    
    /**
     * Public method: get total entries
     * <br>---------------------------------------------------------------------
     * @return int
     */
    public function getTotalEntries(){
        return $this->total_entries;
    }
    
    /**
     * Public method: get number of pages
     * <br>---------------------------------------------------------------------
     * @return int
     */
    public function getTotalNumberOfPages(){
        return $this->total_pages;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}