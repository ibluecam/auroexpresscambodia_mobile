<?php

/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
//$pg_details = $_CLASS->getPageHTML();
if(isset($_GET['product_type'])){
	$menuSelected = $_GET['product_type'];
}
else $menuSelected="All";

$vehicle = $_CLASS->getVehicle();


$car_count=$_CLASS->call_loaddatacounter();




$pagination=$_CLASS->pagination_html;

if(isset($_GET['price_to'])) $price_to = htmlspecialchars($_GET['price_to']); else $price_to="";
if(isset($_GET['price_from'])) $price_from = htmlspecialchars($_GET['price_from']); else $price_from="";
if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id']; else $log_id="";
?>


<script type="text/javascript">
var search="<?php if(isset($_GET['search'])) echo htmlspecialchars($_GET['search']);?>";
var product_type="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>";
var condition="<?php if(isset($_GET['condition'])) echo htmlspecialchars($_GET['condition']);?>";
var fuel_type="<?php if(isset($_GET['fuel_type'])) echo htmlspecialchars($_GET['fuel_type']);?>";
var engine_volume_from="<?php if(isset($_GET['engine_volume_from'])) echo htmlspecialchars($_GET['engine_volume_from']);?>";
var engine_volume_to="<?php if(isset($_GET['engine_volume_to'])) echo htmlspecialchars($_GET['engine_volume_to']);?>";
var country="<?php if(isset($_GET['country'])) echo htmlspecialchars($_GET['country']);?>";
var transmission="<?php if(isset($_GET['transmission'])) echo htmlspecialchars($_GET['transmission']);?>";
var vehicle_type="<?php if(isset($_GET['vehicle_type'])) echo htmlspecialchars($_GET['vehicle_type']);?>";

var category="<?php if(isset($_GET['category'])) echo htmlspecialchars($_GET['category']);?>";
var category2="<?php if(isset($_GET['category2'])) echo htmlspecialchars($_GET['category2']);?>";
var category3="<?php if(isset($_GET['category3'])) echo htmlspecialchars($_GET['category3']);?>";
var make="<?php if(isset($_GET['make'])) echo htmlspecialchars($_GET['make']);?>";
var model="<?php if(isset($_GET['model'])) echo htmlspecialchars($_GET['model']);?>";
var steering="<?php if(isset($_GET['steering'])) echo htmlspecialchars($_GET['steering']);?>";
var country="<?php if(isset($_GET['country'])) echo htmlspecialchars($_GET['country']);?>";
var year_from="<?php if(isset($_GET['year_from'])) echo htmlspecialchars($_GET['year_from']); elseif(isset($_GET['man_year_from'])) echo htmlspecialchars($_GET['man_year_from']);?>";
var year_to="<?php if(isset($_GET['year_to'])) echo htmlspecialchars($_GET['year_to']); elseif(isset($_GET['man_year_to'])) echo htmlspecialchars($_GET['man_year_to']);?>";
var month_from="<?php if(isset($_GET['month_from'])) echo htmlspecialchars($_GET['month_from']); elseif(isset($_GET['man_month_from'])) echo htmlspecialchars($_GET['man_month_from']);?>";
var month_to="<?php if(isset($_GET['month_to'])) echo htmlspecialchars($_GET['month_to']); elseif(isset($_GET['man_month_to'])) echo htmlspecialchars($_GET['man_month_to']);?>";
var car_id="<?php if(isset($_GET['car_id'])) echo htmlspecialchars($_GET['car_id']);?>";
var customSearch = "<?php if(isset($_GET['customSearch'])) echo htmlspecialchars($_GET['customSearch']);?>";
var owner="<?php if(isset($_GET['owner'])) echo htmlspecialchars($_GET['owner']);?>";
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
<script>
    function hit_counter(id){
       $.post('<?php echo BASE_RELATIVE;?>counter',{myid:id});

    }

</script>





<div id="sectionWrapper">
    <?php 
			//Create SESSION for change language on page load_section.
			
			$_SESSION['default']=$_LANG['INVENTORY_DEFAULT'];
			$_SESSION['reserved']=$_LANG['INVENTORY_RESERVED'];
			$_SESSION['sold_out']=$_LANG['INVENTORY_SOLD_OUT'];
			$_SESSION['delete']=$_LANG['INVENTORY_DELETE'];
			$_SESSION['add_new']=$_LANG['INVENTORY_ADD_NEW'];
			
			$_SESSION['hit']=$_LANG['INVENTORY_HIT'];
			$_SESSION['country']=$_LANG['INVENTORY_COUNTRY'];
			$_SESSION['chassisno']=$_LANG['INVENTORY_CHASSISNO'];
			$_SESSION['seller']=$_LANG['INVENTORY_SELLER'];
			$_SESSION['fob']=$_LANG['INVENTORY_FOB'];
			$_SESSION['detail']=$_LANG['INVENTORY_DETAIL'];
			$_SESSION['edit']=$_LANG['INVENTORY_EDIT'];
			
			$_SESSION['STEERING']=$_LANG['INVENTORY_STEERING'];
			$_SESSION['FUEL_TYPE']=$_LANG['INVENTORY_FUEL_TYPE'];
			$_SESSION['MILEAGE']=$_LANG['INVENTORY_MILEAGE'];
			
	?>
	
    <div id="sectionContenWrapper" style="width:1170px">


    <div id="sectionContent" class="clearfix divBore">

		<div class="titleTop">
				<img class="img-responsive" src="<?php echo BASE_RELATIVE.'img/public_inventory/img_01.png'; ?>" />
				<span><?php echo $_LANG['INVENTORY_STATUS'];?></span>
		</div><br/>
		
		
	   
		<!-- SEARCH SECTION -->
		<div id="search_section">

		</div>

		<div id="carData" class="clearfix" style="width:94%; margin-left:3%; margin-right:3%">

			<!--DYNAMIC SEARCH RESULT -->

			<div id="carSourceWraper" style="width:100%">

			</div>

			<img id="loading_image" src="images/loading_75.gif" style="position:absolute;left:0;right:0;margin:auto;z-index:999;display:none;margin-top:20px;"/>
		</div>
		<div id="pagernation" class="clearfix">
			<?php
				 echo $pagination;
			?>
		</div>
    </div>

 </div>
</div>
<div style='clear:both;'></div>