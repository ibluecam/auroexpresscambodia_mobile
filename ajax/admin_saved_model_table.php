<?php
if(!isset($_SESSION)) @session_start();
$log_group=$_SESSION['log_group'];
if($log_group!='admin'){
    return;
}
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();

if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
if(isset($_GET['make'])) $make=mysql_real_escape_string(stripcslashes($_GET['make'])); else $make="";
//if(isset($_GET['offset'])) $offset=htmlspecialchars($_GET['offset']); else $offset=0;
$where="WHERE pm.maker='$make' ";
$sql="SELECT pm.id, pm.model, pm.maker from product_model pm $where ORDER BY pm.model";

if( !$result = @mysql_query($sql) ){
    require_once BASE_CLASS . 'class-log.php';
    $cnx->close();
    LogReport::write('Unable to load make list due a query error at ' . __FILE__ . ':' . __LINE__);
    return;
}
$i=0;
echo "<table class='model_table' id='model_table'>
        <tr>
            <th></th>
            <th>N<sup>o</sup></th>
            <th>Model</th>
            <th></th>
        </tr>";
if(mysql_num_rows($result)>0){
    while( $r = @mysql_fetch_assoc($result) ){
        //array_push($this->city_list,$r);
        $i++;
        $id=$r['id'];                 
        echo 
        "<tr>
            <td>
                <input type='checkbox' class='model_check' value='{$r['model']}' name='modelCheck[$id]' id='model_check_$id'>
                <input type='hidden' value='{$r['model']}' name='old_model[$id]' id='old_model_$id'>
            </td>
            <td>$i</td>
            <td id='model_$id'>{$r['model']}</td>
            <td class='button_td'>
                <a edit_id='$id' class='edit_button' href='#'><img src='images/admin/write.png'></a>
                <a delete_id='$id' class='delete_button' href='#'><img src='images/admin/x.png'></a>
            </td>
        </tr>";
    }
}else{
    echo "<tr><td style='color:#A00000;text-align:center;' colspan='5'>No models found</td></tr>";
}
echo "<table>";
@mysql_free_result($sql);
$cnx->close();
?>