<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}
	
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
// $_CKEDITOR_ENABLED = true;
// $_CKEDITOR_ID = '_ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $invalidProduct = false;
    // get lists.
 //    $maker_unique_list = $_CLASS->getMakerList();
 //    $model_unique_list = $_CLASS->getModelList();
 //    $body_type_list    = $_CLASS->getBodyList();
	// $drive_type_list   = $_CLASS->getDriveList();
    
	// $product_option_list = $_CLASS->getProductOptionList();
     $country_list = $_CLASS->getCountryList();
     $vessel_type_list = $_CLASS->getVesselTypeList();
     $line_list = $_CLASS->getLineList();

	// $vehicle_type_list = $_CLASS->getVehicleTypeList();
	// $car_maker_list = $_CLASS->getCarMakerList();
	// $category_list = $_CLASS->getCategoryList();
	// $fuel_type_list = $_CLASS->getFuelTypeList();
	// $transmission_list = $_CLASS->getTransmissionList();
    // set input size for maker.
 //    ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );
 //    ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );
 //    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );
 //    if( CURRENCY_CODE == 'USD' ){
 //        $decimal = '.';
 //        $thousand= ',';
 //    }
 //    else {
 //        $decimal = ',';
 //        $thousand= '.';
 //    }
    if(isset($_GET['cid'])) $cid=(int)$_GET['cid'];	else $cid='';
	$shippingInfo=array();
	$invalidShippingInfo = false;
 //    // get generated car id.
    
	if(!empty($cid)){

		
		$shippingInfo = $_CLASS->getShippingInfo($cid);
        $invalidShippingInfo=$_CLASS->invalidShippingInfo;
	}
	// (isset($product['payment_terms']) ? $payment_terms = explode('^',$product['payment_terms']) : $payment_terms=array());
	// (isset($product['other_options']) ? $other_options = explode('^',$product['other_options']) : $other_options=array());
	// (isset($product['safety_device_options']) ? $safety_device_options = explode('^',$product['safety_device_options']) : $safety_device_options=array());
	// (isset($product['exterior_options']) ? $exterior_options = explode('^',$product['exterior_options']) : $exterior_options=array());
	// (isset($product['interior_options']) ? $interior_options = explode('^',$product['interior_options']) : $interior_options=array());
	
	
		
?>
<script>
    var countryLoading="<?php if(isset($shippingInfo['country_loading'])) echo $shippingInfo['country_loading'];?>";
    var countryDest="<?php if(isset($shippingInfo['country_dest'])) echo $shippingInfo['country_dest'];?>";
    var portLoading="<?php if(isset($shippingInfo['port_loading'])) echo $shippingInfo['port_loading'];?>";
    var portDest="<?php if(isset($shippingInfo['port_dest'])) echo $shippingInfo['port_dest'];?>";
    var vesselType="<?php if(isset($shippingInfo['vessel_type'])) echo $shippingInfo['vessel_type'];?>";
    var size="<?php if(isset($shippingInfo['size'])) echo $shippingInfo['size'];?>";
    var transitTime="<?php if(isset($shippingInfo['transit_time'])) echo $shippingInfo['transit_time'];?>";
    var line="<?php if(isset($shippingInfo['line'])) echo $shippingInfo['line'];?>";
</script>

        <div id="content-wrapper">
            <link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/mywini/mywini.css" type="text/css"/>
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Help</a> > Shipping Estimate</p>
            </div>
                <?php include("php/sidebar/my-wini.php");?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                 <div class="page-title">
                	<?php 
					if(empty($getId)){
						echo "<p>".$_LANG['PAGE_HEADER']."</p>";
					}else{
						echo "<p>".$_LANG['PAGE_EDIT_HEADER']."</p>";
					}
					?>
                </div>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                
                <form id="form-wrap" method="post">
                <!----------------- FORM CONTAINER -------------------->
                    <?php if(!$invalidShippingInfo){?>
                    
                    <div id="content1" class="shipping-info-container">
                        <br/>
                        
						<table>
                        	<tr>
                                <td class="input-td"><?php echo $_LANG['PAGE_PORT_LOADING_LABEL'];?> *</td>
                                <td class="input-td">
                                    <select required style="width:250px" id="countryLoadingSelect" name="countryLoadingSelect">
                                        <option value="">- Select -</option>
                                        <?php
                                            if( count($country_list['cc']) > 0 ){
                                            
                                                for( $i=0; $i < count($country_list['cc']); $i++ ){
                                                    if( !empty($country_list['country_name'][$i]) ){
                                            ?>
                                            <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                        <?php
                                                    }
                                                }
                                            }
                                            else {
                                        ?>
                                            <option value="0"><?php echo $_LANG['PAGE_NOCOUNTRY_LABEL'];?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td class="input-td">
                                    <select style="width:250px" id="portLoadingSelect" name="portLoadingSelect">
                                        <option value="">- Select -</option>
                                    </select>
                                    <br/>
                                    <input placeholder="<?php echo $_LANG['PAGE_PORT_PLACEHOLDER'];?>" id="portLoadingInput" name="portLoadingInput" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="input-td"><?php echo $_LANG['PAGE_PORT_DESTINATION_LABEL'];?> *</td>
                                <td class="input-td">
                                    <select required style="width:250px" id="countryDestSelect" name="countryDestSelect">
                                        <option value="">- Select -</option>
                                        <?php
                                            if( count($country_list['cc']) > 0 ){
                                            
                                                for( $i=0; $i < count($country_list['cc']); $i++ ){
                                                    if( !empty($country_list['country_name'][$i]) ){
                                            ?>
                                            <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                        <?php
                                                    }
                                                }
                                            }
                                            else {
                                        ?>
                                            <option value=""><?php echo $_LANG['PAGE_NOCOUNTRY_LABEL'];?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td class="input-td">
                                    <select style="width:250px" id="portDestSelect" name="portDestSelect">
                                        <option value="">- Select -</option>
                                    </select>
                                    <br/>
                                    <input placeholder="<?php echo $_LANG['PAGE_PORT_PLACEHOLDER'];?>" id="portDestInput" name="portDestInput" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="input-td"><?php echo $_LANG['PAGE_VESSEL_TYPE_LABEL'];?> *</td>
                                <td class="input-td">
                                    <select required style="width:250px" id="vesselTypeSelect" name="vesselTypeSelect">
                                        <option value="">- Select -</option>
                                        <?php 
                                            foreach($vessel_type_list as $vessel_type){
                                                echo "<option value='$vessel_type'>$vessel_type</option>";
                                            }
                                        ?>
                                        <option value="Other">Other</option>
                                    </select>
                                    <br/>
                                    <input placeholder="<?php echo $_LANG['PAGE_VESSEL_TYPE_PLACEHOLDER'];?>" id="vesselTypeInput" name="vesselTypeInput" type="text"/>
                                </td>
                                
                                <td class="input-td">
                                    <select required style="width:250px" id="sizeSelect" name="sizeSelect">
                                        <option value="">- Select -</option>
                                    </select>
                                    <br/>
                                    <input placeholder="<?php echo $_LANG['PAGE_SIZE_PLACEHOLDER'];?>" id="sizeInput" name="sizeInput" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="input-td"><?php echo $_LANG['PAGE_OCEAN_FREIGHT_LABEL'];?> *</td>
                                <td class="input-td" colspan="3">
                                   
                                    <input value="<?php if(isset($shippingInfo['ocean_freight_price'])) echo $shippingInfo['ocean_freight_price'];?>" required id="oceanFreightInput" name="oceanFreightInput" type="text"/> <span>USD</span>
                                </td>
                                
                            </tr>
                            
                            <tr>
                                <td class="input-td"><?php echo $_LANG['PAGE_TRANSIT_TIME_LABEL'];?></td>
                                <td class="input-td" colspan="3">
                                    <select style="width:250px" id="transitTimeSelect" name="transitTimeSelect">
                                      <option value="0">- Select -</option>
                                      <?php 
                                        for($i=1;$i<=365;$i++){
                                            echo "<option value='$i'>$i</option>";
                                        }

                                      ?>
                                    </select> <span>Days</span>
                                    
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="input-td"><?php echo $_LANG['PAGE_LINE_LABEL'];?></td>
                                <td colspan="2" class="input-td">
                                    <select style="width:250px" id="lineSelect" name="lineSelect">
                                        <option value="">- Select -</option>
                                        <?php 
                                            foreach($line_list as $line){
                                                echo "<option value='$line'>$line</option>";
                                            }
                                        ?>
                                        <option value="Other">Other</option>
                                    </select>
                                    <br/>
                                    <input id="lineInput" name="lineInput" placeholder="<?php echo $_LANG['PAGE_LINE_PLACEHOLDER'];?>" type="text"/>
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="input-td"><?php echo $_LANG['PAGE_REMARK_LABEL'];?></td>
                                <td colspan="2" class="input-td">
                                    
                                    <textarea id="remarkInput" name="remarkInput"><?php if(isset($shippingInfo['remark'])) echo $shippingInfo['remark'];?></textarea>
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="input-td"></td>
                                <td colspan="2" class="input-td">
                                    <input style="margin:0px 0px 20px 10px;" id="submitbtn" style="" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['PAGE_SAVE_BUTTON'];?>" />
                                   
                                </td>
                                
                            </tr>
                        </table>
                        
                        
                    </div>
                    
                    
                    <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    <!----------------- END FORM CONTAINER -------------------->
                    <?php 
                    } 
                    else{
                        echo "<font color='#C1272D'>Shipping info with this ID is invalid or does not exist!</font>";
                    }


                    ?>
                </form>
                <!-- END CONTENT -->
                        
                        
                        <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    </div>
                  
                    <div class="clearfix"></div>
                    <div id="content2">
                        
            </div>
            <div class="clearfix"></div>
        </div>
 
        
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}