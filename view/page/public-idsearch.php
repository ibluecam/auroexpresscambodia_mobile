<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$countryList=$_CLASS->getCountryList();

?>


 <div id="sectionContenWrapper">
        	<!--sectionSidebar-->
            <div id="topMenu">
            	<p><a href="#">My Wini</a> > ID SEARCH</p>
            </div>
        	<div id="sectionSidebar">
            	<ul>
                	<li class="top"><a href="<?php echo BASE_RELATIVE;?>register" class="linkfade">Register</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>login" class="linkfade">Sign In</a></li>
                    <li><a href="<?php echo BASE_RELATIVE;?>idsearch" class="linkfade">ID Search</a></li>
                    <li><a href="pwssearch" class="linkfade">Password Search</a></li>

                </ul>
            </div><!-- end div id="sectionSidebar"-->
            <div id="sectionContent">
            	<div id="myInfoTitle">
                	<p>ID SEARCH</p>
                </div>
                <div id="info" class="clearfix">

                <?php
					if(isset($_POST['idsearch'])){
						$_CLASS->getUserID();
					}
				?>
                 <form action="#" method="post">
                    <p class="font_intro"> Please fill in your  email in form below to find your user ID</p>
                    <select id="country" name="country">
						<option value="<?php echo ( isset($_SESSION['reg_country']) ? $_SESSION['reg_country'] : '' ); ?>"><?php echo ( isset($_SESSION['reg_country']) ? $_SESSION['reg_country'] : '-- Select --' ); ?></option>
						<?php foreach($countryList as $country){
							echo "<option value='{$country['cc']}'>{$country['country_name']}</option>";
						}?>
					</select>
					<span class="error" id="country_error"><?php echo $_LOCAL['REGISTER_COUNTRY_STATUS']; ?></span>
                    <input class="inputText" type="text" name="email_id" placeholder="someone@example.com" /><br />
                    <input class="inquiry-button" name="idsearch" type="submit" value="Submit" />
                 </form>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->

