<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();

// get car info.
$car = $_CLASS->getCarInformation();
$car_detail = $_CLASS->getCarDetails();
$car_features = $_CLASS->getCarFeatures();
$car_media = $_CLASS->getCarMedia();
$extras_list = $_CLASS->getExtrasList();

require_once BASE_CLASS . 'class-utilities.php';
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
                <!-- extras -->
                <?php
                    // display only if there is more than 1 entry.
                    if( count($extras_list) > 0 ){
                ?>
                <div id="extra-main-wrap">
                <?php
                        for( $i=0; $i < count($extras_list); $i++ ){
                            $pslug = $extras_list[$i]['maker'] . ' ' . $extras_list[$i]['model'] . ' ' . $extras_list[$i]['year'];
                            $pslug = Utilities::genetateSlug($pslug);
                ?>
                    <div class="extra-wrap">
                        <h5><?php echo $extras_list[$i]['maker'] . ' ' . $extras_list[$i]['model'] . ' ' . $extras_list[$i]['year'];?></h5>
                        <a href="<?php echo BASE_RELATIVE . $pslug . '/car-detail/?cid=' . $extras_list[$i]['id'];?>&o=&ot=&r=&pg=1#.UeB_pUFmib_" style="display:block;margin-bottom:5px;">
                            <img src="<?php echo BASE_RELATIVE . $extras_list[$i]['image'];?>" class="img-polaroid" width="240" height="183" alt="<?php echo $extras_list[$i]['maker'] . ' ' . $extras_list[$i]['model'];?>"/>
                        </a>
                        <a href="<?php echo BASE_RELATIVE . $pslug . '/car-detail/?cid=' . $extras_list[$i]['id'];?>&o=&ot=&r=&pg=1#.UeB_pUFmib_" class="btn btn-small"><i class="icon-search" style="margin-right:10px;"></i>More details</a>
                    </div>
                <?php
                        }
                ?>
                </div>    
                <?php
                    }
                ?>
                <!-- end extras -->                
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <?php
                    if( $fstatus && $ftype == 'top' ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // if car id is not declared, display error message.
                    $cid = $_GET['cid'];
                    $cid = addslashes($cid);
                    
                    if( empty($cid) ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['CAR_DETAIL_INVALID_CAR_ID_ERROR'];?>
                </div>
                <?php
                    }
                    else {
                        // check if car info has been load.
                        if( count($car) < 1 ){
                ?>
                <p><?php echo $_LANG['CAR_DETAIL_NO_CAR_AVAILABLE_LABEL'];?></p>
                <?php
                        }
                        else {
                            // display page.
                            // define images.
                            $img_arr = $car_media;
                            
                            // initialize image/video arrays.
                            $interior_arr = array();
                            $exterior_arr = array();
                            $video_arr = array();
                            
                            if( count($img_arr) > 0 ){
                                
                                for( $a=0; $a < count($img_arr); $a++ ){
                                    // handle images.
                                    if( $img_arr[$a]['type'] == 'image' ){
                                        
                                        if($img_arr[$a]['mode'] == 'interior') {
                                            array_push($interior_arr, $img_arr[$a]['source']);
                                        }
                                        else {
                                            array_push($exterior_arr, $img_arr[$a]['source']);
                                        }
                                        
                                        $main_img = BASE_RELATIVE . $img_arr[0]['source'];
                                    }
                                    // handle video.
                                    else {
                                        array_push($video_arr, $img_arr[$a]['source']);
                                    }
                                }
                            }
                            else {
                                array_push($interior_arr, 'image/default_main_image.jpg');
                                array_push($exterior_arr, 'image/default_main_image.jpg');
                                $main_img = BASE_RELATIVE . 'image/default_main_image.jpg';
                            }
                            
                            
                            // set reserve car link.
                            if( $_SESSION['log_group'] == 'public' || !isset($_SESSION['log_id']) ){
                                $reserve_car_link = '#';
                                $reserve_car_onclick = 'onclick="bootbox.alert(\''.addslashes($_LANG['CAR_DETAIL_RESERVECAR_LOGIN_REQUIRED_LABEL']).'\');return false"';
                            }
                            else {
                                $reserve_car_link = BASE_RELATIVE . 'reserve-car/?cid=' . $car['id'];
                                $reserve_car_onclick = '';
                            }

                            // update 2013.05.28: add eco icon.
                            $ico = BASE_RELATIVE . 'image/eco_' . strtolower($car['eco']) . '.png';
                ?>
                <h1><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $_CLASS->getBodyTypeByID($car['body_type']) . ' ' . $car['year'];?> 
                    <a class="btn btn-small" href="<?php echo BASE_RELATIVE;?>home/search/?o=<?php echo (!empty($_GET['o']) ? trim($_GET['o']) : 'featured');?><?php echo (!empty($_GET['ot']) ? '&ot='.$_GET['ot'] : '');?>&r=<?php echo trim($_GET['r']);?>&pg=<?php echo trim($_GET['pg']);?>" style="float:right;"><i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['CAR_DETAIL_GOBACK_LABEL'];?></a>
                </h1>
                <br/>
                <div class="clearfix" style="position:relative;">
                    <img class="ecoIcon" src="<?php echo $ico;?>" width="32" height="24" alt="Energy class <?php echo $car['eco'];?>" title="Energy class <?php echo $car['eco'];?>" />
                </div>
                
                <div id="car-list-wrapper">
                    <div class="car-item-wrapper">
                        <div class="handle-wrapper">
                            <span class="label-wrap"><?php echo CURRENCY_SYMBOL;?> <?php echo Utilities::formatPrice($car['price'],CURRENCY_CODE);?></span>
                            <div class="toolbar-wrapper">
                                <a href="#" onclick="addToFavorites('<?php echo $car['id'];?>'); return false;" class="btn btn-small btn-warning" style="margin-right:10px;"><i class="icon-star-empty" style="margin-right:10px;"></i> <?php echo $_LANG['CAR_DETAIL_ADD_TO_FAV_BUTTON'];?> </a>
                                <?php
                                    if( $_SETTING->getAllowReservation() ){
                                ?>
                                <a href="<?php echo $reserve_car_link;?>" <?php echo $reserve_car_onclick;?> class="btn btn-small btn-info" style="margin-right:10px;"><i class="icon-inbox" style="margin-right:10px;"></i> <?php echo $_LANG['CAR_DETAIL_RESERVE_CAR_BUTTON'];?> </a>
                                <?php
                                    }
                                ?>
                                <a id="printbtn" onclick="printThisPage('<?php echo $car['id'];?>');return false;" class="btn btn-small"><i class="icon-print" style="margin-right:10px;"></i><?php echo $_LANG['CAR_DETAILS_PRINT_PAGE_LABEL'];?></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <a id="mainImgAnchor" class="main-image" href="<?php echo $main_img;?>" rel="prettyPhoto[gallery]"><img id="mainImg" class="img-polaroid img-item" src="<?php echo $main_img;?>" width="300" height="200" alt="<?php echo $car['maker'];?> <?php echo $car['model'];?>" /></a>
                        <?php
                            // update 2013.07.11: add all images to gallery.
                            if( count($exterior_arr) > 0 ){
                                for( $i=0; $i < count($exterior_arr); $i++ ){
                                    // skip first image coz is already on the list (index 0 is the main image)
                                    if( $i > 0 ){
                        ?>
                        <a href="<?php echo BASE_RELATIVE . $exterior_arr[$i];?>" rel="prettyPhoto[gallery]" style="display:none;"></a>
                        <?php
                                    }
                                }
                            }
                            
                            if( count($interior_arr) > 0 ){
                                for( $i=0; $i < count($interior_arr); $i++ ){
                        ?>
                        <a href="<?php echo BASE_RELATIVE . $interior_arr[$i];?>" rel="prettyPhoto[gallery]" style="display:none;"></a>
                        <?php
                                }
                            }
                        ?>
                        
                        <div class="item-gallery">
                            <ul class="nav nav-tabs" id="imgTab">
                                <?php
                                    // hide interior if there are no images to be displayed: update July 9th 2013.
                                    if( count($interior_arr) > 0 ){
                                ?>
                                <li class="active"><a href="#interior"><?php echo $_LANG['CAR_DETAIL_INTERIOR_TAB_LABEL'];?></a></li>
                                <?php
                                    }
                                ?>
                                <li><a href="#exterior"><?php echo $_LANG['CAR_DETAIL_EXTERIOR_TAB_LABEL'];?></a></li>
                                <?php
                                    // hide video tab if no results are available.
                                    if( count($video_arr) > 0 ){
                                ?>
                                <li><a href="#video"><?php echo $_LANG['CAR_DETAIL_VIDEO_TAB_LABEL'];?></a></li>
                                <?php
                                    }
                                ?>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="interior">
                                    <?php
                                        // display interior images.
                                        if( count($interior_arr) < 1 ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg','<?php echo BASE_RELATIVE . 'image/default_main_image.jpg';?>','mainImgAnchor'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . 'image/default_main_image.jpg';?>" width="74" height="50" alt="<?php echo $car['maker'];?> <?php echo $car['model'];?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                        } else {
                                            for( $k=0; $k < count($interior_arr); $k++ ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg','<?php echo BASE_RELATIVE . $interior_arr[$k];?>','mainImgAnchor'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . $interior_arr[$k];?>" width="74" height="50" alt="<?php echo $car['maker'];?> <?php echo $car['model'];?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="tab-pane" id="exterior">
                                    <?php
                                        // display exterior images.
                                        if( count($exterior_arr) < 1 ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg','<?php echo BASE_RELATIVE . 'image/default_main_image.jpg';?>','mainImgAnchor'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . 'image/default_main_image.jpg';?>" width="74" height="50" alt="<?php echo $car['maker'];?> <?php echo $car['model'];?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                        } else {
                                            for( $k=0; $k < count($exterior_arr); $k++ ){
                                    ?>
                                    <a href="#" onclick="displayMainImage('mainImg','<?php echo BASE_RELATIVE . $exterior_arr[$k];?>','mainImgAnchor'); return false;">
                                        <img src="<?php echo BASE_RELATIVE . $exterior_arr[$k];?>" width="74" height="50" alt="<?php echo $car['maker'];?> <?php echo $car['model'];?>" class="img-thumb-item img-polaroid" />
                                    </a>
                                    <?php
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="tab-pane" id="video">
                                    <?php
                                        // display videos.
                                        if( count($video_arr) < 1 ){
                                    ?>
                                    <p><?php echo $_LANG['CAR_DETAIL_VIDEO_TAB_NOVIDEO_LABEL'];?></p>
                                    <?php
                                        }
                                        for( $k=0; $k < count($video_arr); $k++ ){
                                    ?>
                                    <a href="<?php echo BASE_RELATIVE . $video_arr[$k];?>" rel="prettyPhoto">
                                        <img src="<?php echo BASE_RELATIVE;?>image/default_video.jpg" width="74" height="50" alt="<?php echo $car['maker'];?> <?php echo $car['model'];?>" class="img-thumb-item" />
                                    </a>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="social-wrapper" style="padding:15px;">
                            <?php
                                // if social bar is enabled, show it (genius! lol)
                                if( $_SETTING->getSocialStatus() ){
                                    
                                    // note: to replace AddThis by any other social sharing tool,
                                    // just remove the below code and replace it by whatever you
                                    // want to use.
                            ?>
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                <a class="addthis_button_preferred_1"></a>
                                <a class="addthis_button_preferred_2"></a>
                                <a class="addthis_button_preferred_3"></a>
                                <a class="addthis_button_preferred_4"></a>
                                <a class="addthis_button_compact"></a>
                                <a class="addthis_counter addthis_bubble_style"></a>
                            </div>
                            <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
                            <!-- AddThis Button END -->
                            <?php
                                }
                            ?>
                        </div>
                        <?php
                            // display feature icons with tooltip.
                            if( count($car_features) < 1 ){
                                // do nothing;
                            }
                            else {
                        ?>
                        <div class="features-wrapper">
                            <h4><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $_CLASS->getBodyTypeByID($car['body_type']) . ' ' . $car['year'];?> <?php echo $_LANG['CAR_DETAIL_FEATURES_HEADER'];?></h4>
                            <div>
                                <ul class="nav nav-tabs" id="featTab">
                                    <li><a href="#spec" class="active"><?php echo $_LANG['CAR_DETAIL_SPECIFICATION_TAB_LABEL'];?></a></li>
                                    <li><a href="#featicon"><?php echo $_LANG['CAR_DETAIL_FEATURES_ICON_TAB_LABEL'];?></a></li>
                                    <li><a href="#featmore"><?php echo $_LANG['CAR_DETAIL_MORE_DETAILS_TAB_LABEL'];?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="featicon">
                                        <?php
                                            for( $k=0; $k < count($car_features); $k++ ){
                                        ?>
                                        <p class="feat-item">
                                            <img class="feature-icon" src="<?php echo BASE_RELATIVE . $car_features[$k]['feat_icon'];?>" title="<?php echo $car_features[$k]['feat_name'];?>" width="50" height="50" alt="<?php echo $car_features[$k]['feat_name'];?>" />
                                            <span class="feat-item-span"><?php echo stripslashes($car_features[$k]['feat_name']);?></span>
                                        </p>
                                        <?php
                                            }
                                        ?>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="tab-pane" id="spec">
                                        <p class="feat-row feat-row-item" style="background:#FFF;">
                                            <span class="feat-item-title"><?php echo strtoupper($car['measure_type']);?></span>
                                            <span class="feat-item-content"><?php echo $car['miles'] . ' ' . $car['measure_type'];?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_ENGINE_SIZE_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['engine_size']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['engine_size']) );?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_TRIM_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['trim']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['trim']) );?></span>
                                        </p>
                                        <p class="feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_TYPE_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo $_CLASS->getBodyTypeByID($car['body_type']) . ' ' . $car['doors'] . ' ' . $_LANG['CAR_DETAIL_DOORS_LABEL'];?></span>
                                        </p>
                                        <p class="feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_GEAR_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['gear']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['gear']) );?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_FUEL_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['fuel']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['fuel']) );?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_COLOR_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['color']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['color']) );?></span>
                                        </p>
                                        <p class="feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_PREV_OWNER_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['prev_owners']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['prev_owners']) );?></span>
                                        </p>
                                        <p class="feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_LAST_SERVICE_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['last_service']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['last_service']) );?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_MOT_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['mot']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['mot']) );?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_TAB_BAND_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['tax_band']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['tax_band']) );?></span>
                                        </p>
                                        <p class="feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_TOP_SPEED_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['top_speed']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['top_speed']) );?></span>
                                        </p>
                                        <p class="feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_ENGINE_RPM'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['engine_torque_rpm']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['engine_torque_rpm']) );?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_ENGINE_KW'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['engine_power_kw']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['engine_power_kw']) );?></span>
                                        </p>
                                        <p class="feat-row feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAIL_TRANSMISSION_TYPE'];?></span>
                                            <span class="feat-item-content"><?php echo ( empty($car_detail['transmission_type']) ? $_LANG['CAR_DETAIL_UNSPECIFIED_LABEL'] : stripslashes($car_detail['transmission_type']) );?></span>
                                        </p>
                                        <p class="feat-row-item">
                                            <span class="feat-item-title"><?php echo $_LANG['CAR_DETAILS_ECO_LABEL'];?></span>
                                            <span class="feat-item-content"><?php echo $car['eco'];?></span>
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="tab-pane" id="featmore">
                                        <?php echo ( empty($car_detail['html']) ? $_LANG['CAR_DETAIL_NODETAILS_LABEL'] : stripslashes($car_detail['html']) );?>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                            }
                        ?>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <script language="javascript" type="text/javascript">
                    /* add car to favorites */
                    function addToFavorites(carid){
                        // validate car id.
                        if( carid == undefined || carid.length < 1 ){
                            bootbox.alert('<?php echo addslashes($_LANG['CAR_DETAIL_ADD_TO_FAV_ERROR']);?>');
                        }
                        else {
                            // send add request.
                            $.ajax({
                                url: '<?php echo BASE_RELATIVE;?>ajax/save_to_favorite.php',
                                type: "POST",
                                data: {car: carid},
                                success: function(data, textStatus, xhr){
                                    if( data.length > 20 ){
                                        bootbox.alert('<?php echo addslashes($_LANG['CAR_DETAIL_ADD_TO_FAV_ERROR']);?>');
                                    }
                                    else {
                                        // handle responses.
                                        if( data == 'ok' ){
                                            bootbox.alert('<?php echo addslashes($_LANG['CAR_DETAIL_ADD_TO_FAV_SUCCESS']);?>');
                                        }
                                        else if( data == 'error' ){
                                            bootbox.alert('<?php echo addslashes($_LANG['CAR_DETAIL_ADD_TO_FAV_ERROR']);?>');
                                        }
                                        else if( data == 'login' ){
                                            bootbox.alert('<?php echo addslashes($_LANG['CAR_DETAIL_ADD_TO_FAV_LOGIN_ERROR']);?>');
                                        }
                                        else {
                                            bootbox.alert('<?php echo addslashes($_LANG['CAR_DETAIL_ADD_TO_FAV_ERROR']);?>');
                                        }
                                    }
                                    
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown){
                                    bootbox.alert('<?php echo $_LANG['CAR_DETAIL_ADD_TO_FAV_ERROR'];?>');
                                }
                            });
                        }
                    } // end add car to favorites.
                    
                    /**
                     * Print page button handler.
                     */
                    function printThisPage(cid){
                        if( cid == undefined ){
                            return false;
                        }
                        
                        var pop = window.open('<?php echo BASE_RELATIVE;?>print-page.php?cid=' + cid, 'Print page', 'height=600,width=800');
                        
                        if( window.focus ){
                            pop.focus();
                        }
                        
                        return false;
                    }
                </script>
                <?php            
                        }
                
                    } // end car id validation (else)
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
