<?php
// assign webroot
$array_local=array();
$protocol = "http://".$_SERVER['SERVER_NAME'];
$request_uri = $_SERVER['REQUEST_URI'];
$array_local=(explode("/",$request_uri,2));
$array_local = $array_local[1];
$local_url = current(explode("/",$array_local));
if($_SERVER['SERVER_NAME']=='localhost'){
	$web_root = $protocol.'/'.$local_url.'/';	
}else {
	$web_root = "http://".$_SERVER['SERVER_NAME']."/";
}

require_once '../../config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();
	if($_POST){
		$key_search = mysql_real_escape_string($_POST['search']);

		$sql_search="SELECT p.*, rg.company_name, rg.user_id as comid, rg.mobile,
							cf_sellers.seller_name, cf_sellers.contact_no
                            FROM product AS p                       
                            INNER JOIN register as rg on p.owner=rg.user_id
							INNER JOIN tbl_carfinder_sellers as cf_sellers on (p.owner=cf_sellers.user_id AND cf_sellers.is_deleted=0)
					";
		
		// Free Search
		if(!empty($key_search)){
			$where_keysearch = "";
			$key_search=mysql_real_escape_string($key_search);			
			$key_search=strtolower($key_search);
			
			$arr_keysearch=array();			
			$arr_keysearch=explode(" ", $key_search);
			$where_keysearch.= " WHERE p.is_deleted = 0 AND `p`.`product_type`='Car' AND (p.count_sold=0 AND p.count_reserved=0) AND ";			
			foreach($arr_keysearch as $val){
				$where_keysearch.=" CONCAT(LOWER(p.make), LOWER(p.model), LOWER(p.year), LOWER(p.vin_id)) LIKE '%".$val."%' AND";
			}
			$where_keysearch = trim($where_keysearch, 'AND');	
			$sql_search.=$where_keysearch." GROUP BY p.make, p.model, p.year ORDER BY p.make, p.model, p.year ";	
		}
					
		if( !$sql = @mysql_query($sql_search) ){
			require_once BASE_CLASS . 'class-log.php';
			LogReport::write('Unable to load category list due a query error at ' . __FILE__ . ':' . __LINE__);			
			return;
		}else if(mysql_num_rows($sql)>0){
			$i=0;
			
			while( $r = @mysql_fetch_assoc($sql) ){
				$i++;			
				
				if(!empty($r['make'])) {
					$make=$r['make'];
				}else{
					$make="";
				}
				
				if(!empty($r['model'])) {
					$model=$r['model'];
				}else{
					$model="";
				}
				
				if(!empty($r['year'])) {
					$year=$r['year'];
				}else{
					$year="";
				}

				if(!empty($r['vin_id'])) {
					$vin_id=$r['vin_id'];
				}else{
					$vin_id="";
				}
				
				$vehicle=$make. " ".$model. " ".$year. " ".$vin_id;
				
				$b_vehicle='<strong>'.$key_search.'</strong>';
				$final_dealer=str_ireplace($key_search, $b_vehicle, $vehicle);								
				
				?>					
					<div class="show" align="left" onClick="return true">				
						<span class="name"><?php echo $final_dealer; ?></span>				
					</div>					
				<?php
			}
		}			
	}

$cnx->close();

?>