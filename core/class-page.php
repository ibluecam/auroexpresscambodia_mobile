<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Handles page requests -------------------------------------------------------
 */
if(!isset($_SESSION)) @session_start();

class Page
{
    protected $meta_keyword;
    protected $meta_description;
    protected $meta_robots;
    protected $page_title;
    protected $page_group;
    protected $page_slug;
    protected $page_css_uri;
    protected $page_js_uri;
    protected $page_controller_uri;
    protected $page_top_header_uri;
    protected $page_menu_header_uri;
    protected $page_banner_header_uri;
    protected $page_foot_header_uri;
    protected $page_content_uri;
    protected $page_id;
    protected $page_author;
    protected $page_creation;
    protected $user_group;
    protected $local_language_uri;
    protected $global_language_uri;
    protected $controller_classname;
    protected $theme_name;
    var $version=VERSION;
    /*
     * Constructor -------------------------------------------------------------
     * - get current slug and define page variabled.
     * @param $current_group - current user logged group.
     * @return void.
     */
    public function Page($current_group, $theme)
    {
        $this->version=time();
        if( empty($current_group) )
        {
            $current_group = 'public';
        }
        
        if( empty($theme) ){
            $theme = 'jtheme';
        }
        
        $this->theme_name = $theme;
        $this->user_group = $current_group;
        
        ( isset($_GET['url']) ? $slug = trim($_GET['url']) : $slug = '' );
        // if slug is empty, set it to default home slug.
        if( empty($slug) )
        {
            $slug = DEFAULT_HOME_SLUG;
        }
		 if($slug > 0 or $slug < 0)
        {
            $slug = DEFAULT_HOME_SLUG;
        } 
		// array fix car_type
		$car_type=array('Hatchback','SUV','Sedan','PickUp','Estate Saloons','Compact');
		foreach($car_type as $car_value){
			if($slug==$car_value){
				$slug = DEFAULT_HOME_SLUG;
			}
		}
        // check if slug is composed by more than 1 part. if so, get the last
        // non-empty part.
        $slug = explode('/', $slug);
        if( count($slug) > 1 )
        {
            if( !empty($slug[count($slug)]) )
            {
                $slug = $slug[count($slug)];
            }
            else
            {
                for( $i=count($slug); $i > -1; $i-- )
                {
                    if(!empty($slug[$i]) )
                    {
                        $slug = trim($slug[$i]);
                        break;
                    }
                }
            }
            
            $slug = strtolower($slug);
            
        }
        else
        {
            $slug = $slug[0];
        }
        
        //set pages that allow admin to access
        if($_SESSION['log_group']=='admin'){
            if($slug=='edit-myinfo'
                ||$slug=='my-inventory'
                ||$slug=='edit-car'
                ||$slug=='edit-truck'
                ||$slug=='edit-bus'
                ||$slug=='edit-part'
                ||$slug=='edit-accessories'
                ||$slug=='edit-equipment'
                ||$slug=='edit-watercraft'
                ||$slug=='edit-aircraft'
                ||$slug=='edit-motorbike'
                ||$slug=='my-info'
                ||$slug=='messages'
                ){
                $current_group='user';
            }
        }
        $page_patch = BASE_ROOT . 'view/page/' . $current_group . '-' . $slug . '.php';
        // check if physical page exists.
        if( !file_exists($page_patch) )
        {

            if((file_exists(BASE_ROOT.'view/page/user-'.$slug.'.php')||file_exists(BASE_ROOT.'view/page/admin-'.$slug.'.php'))&& $current_group=='public'){
                //if(!empty($slug)) $pg="pg=".$slug;
                $pg=urlencode($this->getCurrentURL());
                //header('Location: '.BASE_RELATIVE.'login?pg='.$pg);
				header('Location: '.BASE_RELATIVE.'register?member_type3=seller&signup=Next');
            }
            //try public pages.
            $current_group = 'public';
			 
            $page_patch = BASE_ROOT . 'view/page/' . $current_group . '-' . $slug . '.php';
            
            if( !file_exists($page_patch) )
            {
                // try 404 page.
                $slug = '404';  // defines the default 404 page.
                $page_patch = BASE_ROOT . 'view/page/' . $current_group . '-' . $slug . '.php';
                if( !file_exists($page_patch) )
                {
                    require_once BASE_CLASS . 'class-log.php';
                    
                    LogReport::write('Unable to load 404 page. The page does not exists. Error thrown at ' . __FILE__ . ':' . __LINE__);
                    LogReport::fatalError('Unable to load page due an internal error. Please notify the system administrator.');
                }
            }
        }
        
        // define attribute values for the page.
        $this->page_slug = $slug;
        $this->page_group = $current_group;
        $this->page_content_uri = BASE_ROOT . 'view/page/' . $this->page_group . '-' . $this->page_slug . '.php';
        $this->local_language_uri = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/' . $this->page_group . '-' . $this->page_slug . '.php';
        $this->global_language_uri = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/global.php';
        $this->controller_classname = $this->page_group.$this->page_slug;
        
        // define css URI.
        $css_patch = BASE_ROOT . 'view/theme/'.$this->theme_name.'/css/' . $this->page_group . '-' . $this->page_slug . '.css';
        
        if( !file_exists($css_patch) ){
            // try file from default theme.
            $css_patch = $css_patch = BASE_ROOT . 'view/theme/jtheme/css/' . $this->page_group . '-' . $this->page_slug . '.css';
        }
        
        if( file_exists($css_patch) )
        {
            $this->page_css_uri = BASE_RELATIVE . 'view/theme/'.$this->theme_name.'/css/' . $this->page_group . '-' . $this->page_slug . '.css?v='.$this->version;
        }
        
        // define js URI.
        $js_patch = BASE_ROOT . 'view/theme/'.$this->theme_name.'/js/' . $this->page_group . '-' . $this->page_slug . '.js';
        
        if( !file_exists($js_patch) ){
            // try js from default theme.
            $js_patch = BASE_ROOT . 'view/theme/jtheme/js/' . $this->page_group . '-' . $this->page_slug . '.js';
        }
        
        if( file_exists($js_patch) )
        {
            $this->page_js_uri = BASE_RELATIVE . 'view/theme/'.$this->theme_name.'/js/' . $this->page_group . '-' . $this->page_slug . '.js?v='.$this->version;
        }
        
        // define controller URL.
        $controller_url = BASE_ROOT . 'controller/' . $this->page_group . '-' . $this->page_slug . '.php';
        
        if( file_exists($controller_url) )
        {
            $this->page_controller_uri = $controller_url;
        }
        
        // define language URL.
        if( !file_exists($this->local_language_uri) )
        {
            $this->local_language_uri = '';
        }
        
        // about headers: if a header has specific group, it will be loaded to
        // replace the public header. This gives you the hability to display different
        // headers for each group. The fallback header will always be the public header.
        // define top header URI.
		if(isset($_GET['blog']))
			$top_header = BASE_ROOT . 'view/header/' . $this->user_group . '-blog-top-header.php';
//			$top_header =BASE_ROOT . 'view/header/' . $this->user_group . '-popup-header.php';
		else if	(isset($_GET['popup']))
			$top_header =BASE_ROOT . 'view/header/' . $this->user_group . '-popup-header.php';
		else 
			
			$top_header = BASE_ROOT . 'view/header/' . $this->user_group . '-top-header.php';
        if( !file_exists($top_header) )
        {
            // try public header.
            //$top_header = BASE_ROOT . 'view/header/public-top-header.php';
			
			if(isset($_GET['blog']))
				$top_header = BASE_ROOT . 'view/header/public-blog-top-header.php';
			else if(isset($_GET['popup']))
				$top_header = BASE_ROOT . 'view/header/public-popup-header.php';
			else				
				$top_header = BASE_ROOT . 'view/header/public-top-header.php';
            
            if( file_exists($top_header) )
            {
                $this->page_top_header_uri = $top_header;
            }
            else
            {
                $this->page_top_header_uri = '';
            }
        }
        else
        {
            $this->page_top_header_uri = $top_header;
        }
        
        $menu_header = BASE_ROOT . 'view/header/' . $this->page_group . '-menu-header.php';
        
        if( !file_exists($menu_header) )
        {
            // try public header.
            $menu_header = BASE_ROOT . 'view/header/public-menu-header.php';
            
            if( file_exists($menu_header) )
            {
                $this->page_menu_header_uri = $menu_header;
            }
            else
            {
                $this->page_menu_header_uri = '';
            }
        }
        else
        {
            $this->page_menu_header_uri = $menu_header;
        }
        
        // display banner only for default home page under public group.
        if( $this->page_slug == DEFAULT_HOME_SLUG && $this->user_group == 'public' )
        {
            $banner_header = BASE_ROOT . 'view/header/' . $this->page_banner_header_uri . '-banner-header.php';
            
            if( !file_exists($banner_header) )
            {
                // try public header.
                $banner_header = BASE_ROOT . 'view/header/public-banner-header.php';
                
                if( file_exists($banner_header) )
                {
                    $this->page_banner_header_uri = $banner_header;
                }
                else
                {
                    $this->page_banner_header_uri = '';
                }
            }
            else
            {
                $this->page_banner_header_uri = $banner_header;
            }
        }
        else
        {
            $this->page_banner_header_uri = '';
        }
        if(isset($_GET['blog']))
			$foot_header = BASE_ROOT . 'view/header/' . $this->user_group . '-blog-foot-header.php';
		else if(isset($_GET['popup']))
			$foot_header = BASE_ROOT . 'view/header/' . $this->user_group . '-popup-foot-header.php';
		else
			$foot_header = BASE_ROOT . 'view/header/' . $this->user_group . '-foot-header.php';
        
        if( !file_exists($foot_header) )
        {
            // try public header.
            //$foot_header = BASE_ROOT . 'view/header/public-foot-header.php';
			if(isset($_GET['blog']))
				
				$foot_header = BASE_ROOT . 'view/header/public-blog-foot-header.php';
			elseif(isset($_GET['popup']))
				$foot_header = BASE_ROOT . 'view/header/public-popup-foot-header.php';
			else
				$foot_header = BASE_ROOT . 'view/header/public-foot-header.php';
            
            if( file_exists($foot_header) )
            {
                $this->page_foot_header_uri = $foot_header;
            }
            else
            {
                $this->page_foot_header_uri = '';
            }
        }
        else
        {
            $this->page_foot_header_uri = $foot_header;
        }
        
        // load page attributes from database. If page is not registered, set
        // default attribute values.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        $slug = mysql_real_escape_string($slug);
        
        if( !$sql = mysql_query("SELECT * FROM `page` WHERE `page_slug`='$slug' AND `page_publish`='1' AND `editable`='1'") )
        {
            $db->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to query page attributes at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            LogReport::fatalError('Unable to load page due an internal error. Please notify the system administrator.');
        }
        
        if( mysql_num_rows($sql) != 1 )
        {
            $db->close();
            $this->setAttributesDefaultValues();
            
            return;
        }
        else
        {
            $r = @mysql_fetch_assoc($sql);
            @mysql_free_result($sql);
            $db->close();
            
            $this->page_id = $r['id'];
            $this->meta_keyword = $r['meta_keywords'];
            $this->meta_description = $r['meta_description'];
            $this->meta_robots = $r['meta_robots'];
            $this->page_title = $r['page_title'];
            $this->page_author = $r['page_author'];
            $this->page_creation = $r['creation'];
        }
    }
    //Stand alone function to get full url
    function getCurrentURL(){
        $currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
        $currentURL .= $_SERVER["SERVER_NAME"];
     
        if($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443")
        {
            $currentURL .= ":".$_SERVER["SERVER_PORT"];
        }
     
            $currentURL .= $_SERVER["REQUEST_URI"];
        return $currentURL;
    }
    /*
     * Page is not registered on database. Set default meta values -------------
     * @return void.
     */
    protected function setAttributesDefaultValues()
    {
        $this->page_id = -1;
        $this->meta_keyword = '';
        $this->meta_description = '';
        $this->meta_robots = 'noindex,nofollow';
        $this->page_title = FRAMEWORK_NAME;
        $this->page_author = FRAMEWORK_NAME;
        $this->page_creation = date('Y.m.d');
        
        // If page is one of the static pages, include meta file ------
        switch($this->page_slug){
            case '':
            case DEFAULT_HOME_SLUG:
            case 'search':
            case 'financing':
            case 'sell':
            case 'find-us':
            case 'news':
            case 'login':
            case 'register':
            case 'support':
            case 'advanced-search':
                
                $p = BASE_ROOT . 'view/page/static_meta/' . $this->page_group . '-' . $this->page_slug . '.php';
                
                if(file_exists($p)){
                    require_once $p;
                    $this->meta_keyword = $meta_keyword;
                    $this->meta_description = $meta_description;
                    $this->meta_robots = $meta_robots;
                }
                
                break;
        }
    }
    
    /*
     * Get page meta keywords --------------------------------------------------
     * @return string.
     */
    public function getMetaKeywords()
    {
        return $this->meta_keyword;
    }
    
    /*
     * Get meta description ----------------------------------------------------
     * @return string.
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }
    
    /*
     * Get meta robots ---------------------------------------------------------
     * @return string.
     */
    public function getMetaRobots()
    {
        return $this->meta_robots;
    }
    
    /*
     * Get meta robots ---------------------------------------------------------
     * @return string.
     */
    public function getPageTitle()
    {
        return $this->page_title;
    }
    
    /*
     * Get page group ----------------------------------------------------------
     * @return string.
     */
    public function getPageGroup()
    {
        return $this->page_group;
    }
    
    /*
     * Get page slug -----------------------------------------------------------
     * @return string.
     */
    public function getPageSlug()
    {
        return $this->page_slug;
    }
    
    /*
     * Get page css uri --------------------------------------------------------
     * @return string.
     */
    public function getCSSURL()
    {
        return $this->page_css_uri;
    }
    
    /*
     * get page js uri ---------------------------------------------------------
     * @return string.
     */
    public function getJSURL()
    {
        return $this->page_js_uri;
    }
    
    /*
     * Get page controller uri -------------------------------------------------
     * @return string.
     */
    public function getControllerURL()
    {
        return $this->page_controller_uri;
    }
    
    /*
     * Get top header uri ------------------------------------------------------
     * @return string.
     */
    public function getTopHeaderURL()
    {
        return $this->page_top_header_uri;
    }
    
    /*
     * Get menu header uri -----------------------------------------------------
     * @return string.
     */
    public function getMenuHeaderURL()
    {
        return $this->page_menu_header_uri;
    }
    
    /*
     * Get banner header uri ---------------------------------------------------
     * @return string.
     */
    public function getBannerHeaderURL()
    {
        return $this->page_banner_header_uri;
    }
    
    /*
     * Get foot header uri -----------------------------------------------------
     * @return string.
     */
    public function getFootHeaderURL()
    {
        return $this->page_foot_header_uri;
    }
    
    /*
     * Get content uri ---------------------------------------------------------
     * @return string.
     */
    public function getContentURL()
    {
        return $this->page_content_uri;
    }
    
    /*
     * Get page id -------------------------------------------------------------
     * @return int.
     */
    public function getPageID()
    {
        return $this->page_id;
    }
    
    /*
     * Get page author ---------------------------------------------------------
     * @return string.
     */
    public function getPageAuthor()
    {
        return $this->page_author;
    }
    
    /*
     * Get page language url ---------------------------------------------------
     * @return string.
     */
    public function getPageLanguageURL()
    {
        return $this->local_language_uri;
    }
    
    /*
     * Get global language url -------------------------------------------------
     * @return string.
     */
    public function getGlobalLanguageURL()
    {
        return $this->global_language_uri;
    }
    
    /*
     * Get controller classname ------------------------------------------------
     * @return string.
     */
    public function getControllerClassname()
    {
        return str_replace('-','',$this->controller_classname);
    }
}