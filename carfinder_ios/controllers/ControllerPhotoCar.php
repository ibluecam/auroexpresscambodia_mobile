<?php

require_once './models/PhotoCar.php';
 
class ControllerPhotoCar
{
 
    private $db;
    private $pdo;
    function __construct() 
    {
        require_once './application/DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }
 
    function __destruct() { }
 
    public function updatePhotoCar($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE car_media 
                                        SET 
                                            photo_url = :photo_url, 
                                            thumb_url = :thumb_url, 
                                            car_id = :car_id  

                                            WHERE photo_id = :photo_id');

        $result = $stmt->execute(
                            array('photo_url' => $itm->photo_url,
                                    'thumb_url' => $itm->thumb_url,
                                    'car_id' => $itm->car_id, 
                                    'photo_id' => $itm->photo_id) );
        
        return $result ? true : false;
    }


    public function insertPhotoCar($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO car_media( 
                                            photo_url, 
                                            thumb_url, 
                                            car_id ) 

                                        VALUES(
                                            :photo_url, 
                                            :thumb_url, 
                                            :car_id )');

        $result = $stmt->execute(
                            array('photo_url' => $itm->photo_url,
                                    'thumb_url' => $itm->thumb_url,
                                    'car_id' => $itm->car_id ) );
        
        return $result ? true : false;
    }
 
    public function deletePhotoCar($photo_id, $is_deleted) 
    {
        $stmt = $this->pdo->prepare('UPDATE car_media 
                                
                                        SET 
                                            is_deleted = :is_deleted 

                                        WHERE photo_id = :photo_id');
        
        $result = $stmt->execute(
                            array('photo_id' => $photo_id,
                                    'is_deleted' => $is_deleted ) );
        
        return $result ? true : false;
    }

    
    public function getPhotoCars() 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM car_media 
                                        WHERE is_deleted = 0');

        $stmt->execute();

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoCar();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->car_id = $row['car_id'];

            $array[$ind] = $itm;
            $ind++;
        }
        return $array;
    }

    public function getPhotoCarsByCarId($car_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM car_media 
                                        WHERE car_id = :car_id AND is_deleted = 0');

        $result = $stmt->execute(
                            array('car_id' => $car_id) );

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoCar();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->car_id = $row['car_id'];

            $array[$ind] = $itm;
            $ind++;
        }
        return $array;
    }


    public function getPhotoCarByPhotoId($photo_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM car_media 
                                        WHERE photo_id = :photo_id');

        $result = $stmt->execute(
                            array('photo_id' => $photo_id) );

        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoCar();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->car_id = $row['car_id'];

            return $itm;
        }
        return null;
    }


    public function getNoOfPhotosByCarId($car_id) 
    {
       $stmt = $this->pdo->prepare('SELECT * 

                                        FROM car_media 
                                        WHERE car_id = :car_id AND is_deleted = 0');

        $result = $stmt->execute(
                            array('car_id' => $car_id) );

        $no_of_rows = $stmt->rowCount();

       return $no_of_rows;
    }

    public function getPhotoCarByCarId($car_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 

                                        FROM car_media 
                                        WHERE car_id = :car_id');

        $result = $stmt->execute(
                            array('car_id' => $car_id) );

        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new PhotoCar();
            $itm->photo_id = $row['photo_id'];
            $itm->photo_url = $row['photo_url'];
            $itm->thumb_url = $row['thumb_url'];
            $itm->car_id = $row['car_id'];

            return $itm;
        }
        return null;
    }

    

}
 
?>