<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <h1><?php echo $_LOCAL['ACTIVATE_HEADER_TITLE'];?></h1>
                <?php
                    $email = trim($_GET['account']);
                    
                    require_once BASE_CLASS . 'class-utilities.php';
                    
                    if( !Utilities::checkEmail($email) )
                    {
                ?>
                <p class="text-error"><strong><?php echo $_LOCAL['ACTIVATE_LABELA'];?></strong></p>
                <?php
                    }
                    else
                    {
                        // check if account is aready validated.
                        require_once BASE_CLASS . 'class-connect.php';
                        
                        $db = new Connect();
                        $db->open();
                        
                        $email = mysql_real_escape_string($email);
                        
                        if( !$sql = @mysql_query("SELECT `email`,`activated` FROM `register` WHERE `email`='$email'") )
                        {
                            $db->close();
                            
                            require_once BASE_CLASS . 'class-log.php';
                            
                            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
                            
                ?>
                <p class="text-error"><strong><?php echo $_LOCAL['ACTIVATE_LABELB'];?></strong></p>
                <?php
                        }
                        else
                        {
                            if( @mysql_num_rows($sql) != 1 )
                            {
                                $db->close();
                ?>
                <p class="text-error"><strong><?php echo $_LOCAL['ACTIVATE_LABELC'];?></strong></p>
                <?php
                            }
                            else
                            {
                                $r = @mysql_fetch_assoc($sql);
                                @mysql_free_result($sql);
                                
                                if( (bool)$r['activated'] )
                                {
                                    $db->close();
                ?>
                <p class="text-success"><strong><?php echo $_LOCAL['ACTIVATE_LABELD'];?></strong></p>
                <?php
                                }
                                else
                                {
                                    // activate user account.
                                    if( !$sql = mysql_query("UPDATE `register` SET `activated`='1' WHERE `email`='$email' LIMIT 1;") )
                                    {
                                        $db->close();
                            
                                        require_once BASE_CLASS . 'class-log.php';

                                        LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
                ?>
                <p class="text-error"><strong><?php echo $_LOCAL['ACTIVATE_LABELE'];?></strong></p>
                <?php
                                    }
                                    else
                                    {
                ?>
                <p class="text-success"><strong><?php echo $_LOCAL['ACTIVATE_LABELF'];?></strong></p>
                <?php
                                    }
                                }
                            }
                        }
                    }
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
