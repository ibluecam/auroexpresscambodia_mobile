<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminreplymessage
{
    protected $lang;
    protected $to;
    protected $name;
    protected $messageId;
    protected $template_metadata;
    protected $form_status = false;
    protected $form_message;
    protected $form_style;


    /*
     * Constructor -------------------------------------------------------------
     */
    public function adminreplymessage($lang=array())
    {
        $this->lang = $lang;
        
        // check if required params. has been passed and are valid.
        $email = trim($_GET['email']);
        $name  = trim($_GET['name']);
        $messageid = trim($_GET['messageid']);
        
        $name = str_replace('%20', ' ', $name);
        
        require_once BASE_CLASS . 'class-utilities.php';
        
        if( empty($name) || empty($messageid) || !Utilities::checkEmail($email) )
        {
            header("Location: " . BASE_RELATIVE . 'home/inbox/');
            exit;
        }
        
        $this->to = $email;
        $this->name = $name;
        $this->messageId = $messageid;
        
        // handle send message form.
        if( isset($_POST['sendMsg']) && !empty($_POST['sendMsg']) )
        {
            $this->sendEmail();
        }
        
        // load template file.
        $this->loadSendMailTemplate();
    }
    
    /*
     * Protected method: send email form handler -------------------------------
     * @return void.
     */
    protected function sendEmail()
    {
        $message = stripslashes($_POST['sendMsg']);
        $subject = trim($_POST['subject']);
        $subject = stripslashes($subject);
        $subject = htmlentities($subject);
        $to = trim($_POST['destination']);
        $name = trim($_POST['destname']);
        $name = stripslashes($name);
        $name = htmlentities($name);
        $msgID = trim($_POST['msgid']);
        
        // add HTML tags.
        $html = '<html><head><title>' . FRAMEWORK_NAME . '</title></head><body>';
        $html .= $message;
        $html .= '</body></html>';
        
        // validate.
        require_once BASE_CLASS . 'class-utilities.php';
        
        if( !Utilities::checkEmail($to) )
        {
            $this->form_status = true;
            $this->form_style = 'alert-error';
            $this->form_message = $this->lang['REPLAY_INVALID_USER_EMAIL'];
            return;
        }
        
        if( empty($subject) )
        {
            $this->form_status = true;
            $this->form_style = 'alert-error';
            $this->form_message = $this->lang['REPLAY_SUBJECT_REQUIRED'];
            return;
        }
        
        // send message.
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: '.$name.' <'.$to.'>' . "\r\n";
        $headers .= 'From: '.FRAMEWORK_NAME.' <'.ADMIN_CONTACT_EMAIL.'>' . "\r\n";
        $headers .= 'X-Mailer: ' . FRAMEWORK_NAME;
        
        if( !@mail($to, $subject, $html, $headers) )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to send reply email at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_status = true;
            $this->form_style = 'alert-error';
            $this->form_message = $this->lang['REPLAY_MAIL_FAILURE'];
            
            return;
        }
        
        // update message status.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        if( !@mysql_query("UPDATE `contact` SET `replied`='1', `read`='1' WHERE `id`='$msgID' LIMIT 1;") )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to update message status at ' . __FILE__ . ':' . __LINE__ .'. ' . mysql_error());
            
            $db->close();
            
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $this->form_message = $this->lang['REPLAY_STATUS_UPDATE_FAILURE'];
            
            return;
        }
        
        $db->close();
        
        $this->form_status = true;
        $this->form_style = 'alert-success';
        $this->form_message = $this->lang['REPLAY_SUCCESS'];
        
        return;
    }
    
    /*
     * Protected method: load sendmail template --------------------------------
     * @return void.
     */
    protected function loadSendMailTemplate()
    {
        $path = BASE_ROOT . 'view/email/template.html';
        
        if( !file_exists($path) )
        {
            $this->template_metadata = '';
        }
        else
        {
            if( $fo = @fopen($path, 'r') )
            {
                if( !$data = @fread($fo, filesize($path)) )
                {
                    require_once BASE_CLASS . 'class-log.php';
                
                    LogReport::write('Unable to read sendmail template at ' . __FILE__ . ':' . __LINE__ . '. Probable cause: access denied (read).');

                    $this->template_metadata = '';
                }
                else
                {
                    $this->template_metadata = $data;
                }
                
                @fclose($fo);
            }
            else
            {
                require_once BASE_CLASS . 'class-log.php';
                
                LogReport::write('Unable to load sendmail template at ' . __FILE__ . ':' . __LINE__ . '.');
                
                $this->template_metadata = '';
            }
        }
    }
    
    /*
     * Public method: get template metadata ------------------------------------
     * @return string (HTML)
     */
    public function getTemplateMetadata()
    {
        $this->template_metadata = str_replace('[:::MESSAGE:::]', '', $this->template_metadata);
        return $this->template_metadata;
    }

    /*
     * Public method: get email destination (TO) -------------------------------
     * @return string.
     */
    public function getTOEmail()
    {
        return $this->to;
    }
    
    /*
     * Public method: get destination name -------------------------------------
     * @return string.
     */
    public function getName()
    {
        return $this->name;
    }
    
    /*
     * Public method: get message id (the ID from contact table) ---------------
     * @return int.
     */
    public function getMessageID()
    {
        return $this->messageId;
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /*
     * Public method: get form style -------------------------------------------
     * @return string (alert-error | alert-warning | alert-success)
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    
    /*
     * Public method: get form message -----------------------------------------
     * @return string
     */
    public function getFormStatusMessage()
    {
        return $this->form_message;
    }
}