<?php
if(!isset($_SESSION)) @session_start();
require_once '../../../config.php';
require_once BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/admin-new-car.php';
include('resize-class.php');

// calculate max. upload filesize.
$max_upload     = (int)(ini_get('upload_max_filesize'));
$max_post       = (int)(ini_get('post_max_size'));
$memory_limit   = (int)(ini_get('memory_limit'));
$upload_mb      = min($max_upload, $max_post, $memory_limit);
$cid            = trim($_GET['cid']);
$mode           = trim($_GET['mode']);


$product_media  = array();

function existedProduct(){
    global $cid;
    global $product_media;
    require_once BASE_CLASS . 'class-connect.php';

                        $cnx = new Connect();
                        $cnx->open();
    if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE author='".(int)$_SESSION['log_id']."' AND `car_id`='$cid'") ){
        require_once BASE_CLASS . 'class-log.php';
        LogReport::write('Unable to load image from car_media table due a query error at ' . __FILE__ . ':' . __LINE__);

    }else{
        if(mysql_num_rows($sql)>0){
            while( $r = @mysql_fetch_assoc($sql) ){
            foreach($r as $column=>$value) {
                    $product_media[$column][]=$value;
                }
            }
            return true;
        }
    }
}

/**
 * Handle file upload teset kean
 * <br>-------------------------------------------------------------------------
 */


if( isset($_POST['savebtn']) ){
    $primary_photo=1;
    $id = trim($_POST['caridInput']);
    echo $id;

    //$files[]= $_FILES['fileInput'];

    //echo "No. files uploaded : ".count($_FILES['fileInput']['name'])."<br>";

  //Count number of files in array, loop through each file

  for($j=0; $j<count($_FILES['fileInput']['name']); $j++) {

    if( empty($cid) || !isset($_FILES['fileInput']['name'][$j]) ){
        $form_error = true;
        $form_message = $_LANG['NEWCAR_INVALID_REQUEST_ID_LABEL'];
    }
    else {

        // get file extension.
        $ext =$_FILES['fileInput']['name'][$j];
        $ext = explode('.', $ext);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);

        switch($ext){
            case 'jpg':
            case 'gif':
            case 'png':
                $nogood = false;
                break;
            default:
                $nogood = true;
        }

        if( $nogood ){
            $form_error = true;
            $form_message = $_LANG['NEWCAR_INVALID_FILE_EXTENSION'];
        }
        else {
            // upload file.
            $path  = BASE_ROOT . 'image/upload/cars/';
            $fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;

            $preview_gallery_name="preview_gallery-".$fname;
            $special_offer_name="special_offer-".$fname;
            $list_car_name="list_car-".$fname;
            $thumb_gallery_name="thumb_gallery-".$fname;

            // upload failed.
            if( $_FILES['fileInput']['size'][$j] < 1 ){
                $form_error = true;
                $form_message = $_LANG['NEWCAR_INVALID_IMAGE_SIZE_LABEL'];
            }
            else {
                 // save to database.
                        require_once BASE_CLASS . 'class-connect.php';

                        $cnx = new Connect();
                        $cnx->open();



                        $query = "SELECT COUNT(car_media.product_id) AS pro_count, register_types.max_image
                                    FROM car_media
                                    INNER JOIN register ON register.id=car_media.author
                                    INNER JOIN register_types ON register.register_type = `register_types`.`code`
                                    WHERE car_media.product_id='".$cid."' AND register.id='".$_SESSION['log_id']."' GROUP BY car_media.product_id";



                        $result = mysql_query($query);
                        $max_image = 1;
                        $pro_count = 0;

                        while($row = mysql_fetch_array($result)){
                            $max_image = $row['max_image'];
                            $pro_count = $row['pro_count'];
                        }

                        if(($pro_count < $max_image) ){ ?>
                        <script type="text/javascript">
                        $('#sal').hide();
                        </script>


                <?php
                // upload the image.
                if( !@move_uploaded_file($_FILES['fileInput']['tmp_name'][$j], $path.$fname) ){
                    $form_error = true;
                    $form_message = $_LANG['NEWCAR_UPLOAD_FAILURE_LABEL'];
                    require_once BASE_CLASS . 'class-log.php';
                    LogReport::write('Unable to upload image at ' . __FILE__ . ':' . __LINE__);
                }
                else {
                    // resize image.
                    //copy($path.$fname,$path.$medium_name);
                    //copy($path.$fname,$path.$small_name);


                    // *** 1) Initialise / load image
                    $main_image=new resize($path.$fname);
                    $preview_gallery = new resize($path.$fname);
                    //$special_offer = new resize($path.$fname);
                   //   $list_car = new resize($path.$fname);
                    $thumb_gallery = new resize($path.$fname);
                    // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
                    $main_image->resizeImage(500, 400, 'crop');
                    $preview_gallery->resizeImage(365, 210, 'crop');
                   // $special_offer->resizeImage(156, 90, 'crop');
                   // $list_car->resizeImage(315, 225, 'crop');
                    $thumb_gallery->resizeImage(145,112, 'crop');
                    // *** 3) Save image
                    $main_image->saveImage($path.$fname, 100);
                    $preview_gallery->saveImage($path.$preview_gallery_name, 100);
                    //$special_offer->saveImage($path.$special_offer_name, 100);
                //  $list_car->saveImage($path.$list_car_name, 100);
                    $thumb_gallery->saveImage($path.$thumb_gallery_name, 100);


                    // check if thumb was created successfully.




                            //Define path upload to database
                            $image_path='/image/upload/cars/';
                            $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http://' : 'https://';
                            $host = $_SERVER['HTTP_HOST'];
                            $full_url=$protocol.$host.$_SERVER['REQUEST_URI'];
                            //Condition for debug in local
                            if($host=='localhost'){
                                $start=strlen($protocol.$host);
                                $end = strpos($full_url, '/', $start + 1);
                                $length=$end-$start;
                                $local_host=substr($full_url, $start +1, $length-1);
                                $host="localhost/{$local_host}";
                            }

                            //Reset Primary photo and Set it Again
                            //generate full destination
                            $destination = $protocol.$host.$image_path.$fname;
                            $thumb=$protocol.$host.$image_path."thumb_gallery-".$fname;
                            if( !@mysql_query("INSERT INTO `car_media` (
                                                                `car_id`,
                                                                `type`,
                                                                `author`,
                                                                `mode`,
                                                                `photo_url`,
                                                                `thumb_url`,
                                                                `primary_photo`
                                                                ) VALUES (
                                                                '$cid',
                                                                'image',
                                                                '".(int)$_SESSION['log_id']."',
                                                                '$mode',
                                                                '$destination',
                                                                '$thumb',
                                                                '$primary_photo'
                                                                )") ){
                                require_once BASE_CLASS . 'class-log.php';
                                LogReport::write('Unable to save image for car_media table due a query error at ' . __FILE__ . ':' . __LINE__);
                                $form_error = true;
                                $form_message = $_LANG['NEWCAR_UPLOAD_QUERY_ERROR'];
                                $cnx->close();
                                @unlink($path.$fname);
                            }
                            else {
                                $primary_photo=0;
                                $last_id=mysql_insert_id();


                                // notify parent window to preview image.
                            ?>
                            <script language="javascript">

                                parent.carImageUploadedNew_Photo(<?php echo $last_id;?>,'<?php echo $cid;?>','image/upload/cars/<?php echo $fname;?>','<?php echo $mode;?>','<?php echo $fname;?>',<?php echo $j; ?>);
                            </script>
                            <?php
                            }


                    }
                }else{

                            echo "<p id='sal' style='color:red;font-size: 10px;margin-top: 35px;position: absolute;'>Please Upgrade Your Account to Upload More Then ".$max_image." Images</p>";
                            $cnx->close();
                }

                }

            }

        }

    }
?>
<script type="text/javascript">
    //parent.save_data_form();
</script>
<?php
 }else{
    if(existedProduct()){
        for($i=0;$i<count($product_media['product_id']);$i++){
            echo "<script language='javascript'>parent.carImageUploaded('{$product_media['id'][$i]}','{$cid}','{$product_media['source'][$i]}','{$product_media['mode'][$i]}','{$product_media['source'][$i]}','{$product_media['primary_photo'][$i]}');</script>";
        }

    }
 }

// end file uploader handler dd---------------------------------------------------
$form_error = false;

// internals -------------------------------------------------------------------
function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);
    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow];
}

// end internals ---------------------------------------------------------------
?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!-- Force latest IE rendering engine or ChromeFrame if installed -->
        <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
        <meta charset="utf-8">
        <title>File uploader</title>
        <!-- Bootstrap CSS Toolkit styles -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap-responsive.min.css">
        <!-- Bootstrap CSS fixes for IE6 -->
        <!--[if lt IE 7]><link rel="stylesheet" href="../bootstrap/css/bootstrap-ie6.min.css"><![endif]-->
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
        <!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script src="../jquery/jquery.js"></script>
        <!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">

                $(document).ready(function(e) {
                    var select_file=0;
                   $('form').submit(function(e) {
                   var max_image_upload='<?php echo $_SESSION['user_max_image_upload']; ?>';

                                        // if(parent.check_vehicle_data()==false){

                        //  alert('Please Insert Data in First');
                        //  e.preventDefault();
                        //    return false;
                        // }
                      if(select_file==0){


                            alert('Please select image to upload!');
                            e.preventDefault();
                           return false;

                      }

                    });


                     // Check for file upload limit
                    $('#product_images').change(function(){
                        select_file=this.files.length;
                       $('#btn_upload_photo').click();

                        <?php
                        $cnx = new Connect();
                        $cnx->open();

                            $max_image = 0;
                            $max_pro = 0;
                            $pro_count = 0;
                            $register_type=$_SESSION['register_type'];
                            //$query = "SELECT * FROM register_types WHERE code='".$register_type."'";
                            /*$query="SELECT COUNT(car_media.product_id) AS pro_count, register_types.max_image
FROM car_media
INNER JOIN register ON register.id=car_media.author
INNER JOIN register_types ON register.register_type = `register_types`.`code`
WHERE car_media.product_id='".$cid."' AND register.id='".$_SESSION['log_id']."' GROUP BY car_media.product_id";*/


                            //Count all products user upload

                            $query = "SELECT COUNT(product.id) AS owner_pro_count
                                        FROM product
                                        WHERE product.owner='".$_SESSION['log_id']."'";
                            $result = mysql_query($query);
                            $owner_pro_count = '';

                            $row = mysql_fetch_array($result);
                            $owner_pro_count = $row['owner_pro_count'];

                            /*if($owner_pro_count == $_SESSION['user_max_image_upload']){

                            }else{

                            }*/


                            //Count all products that have image or not
                            $query="SELECT COUNT(tmp.product_id) AS pro_count, tmp.max_image, tmp.max_product
                                    FROM (SELECT cm.product_id, rg.id, rgt.max_image, rgt.max_product
                                                FROM car_media cm
                                                INNER JOIN register rg ON rg.id=cm.author
                                                INNER JOIN register_types rgt ON `rgt`.`code` = rg.register_type
                                                WHERE rg.id='".$_SESSION['log_id']."') tmp
                                    WHERE tmp.product_id='$cid'";

                            $result = mysql_query($query);

                            while($row = mysql_fetch_array($result)){
                                if($row['max_image'] == ''){

                                    $max_image = $_SESSION['user_max_image_upload'];
                                }else{
                                    $max_image = $row['max_image'];

                                }

                                if($row['max_product'] == ''){

                                    $max_pro = $_SESSION['user_max_pro'];
                                }else{
                                    $max_pro = $row['max_product'];

                                }



                                $max_pro = $row['max_product'];
                                $pro_count = $row['pro_count'];
                            }

                        ?>



                        var max_image = <?php echo $max_image; ?>;
                        var pro_count = <?php echo $pro_count; ?>;
                        var owner_pro_count = <?php echo $owner_pro_count;?>;
                        //alert(this.files.length + " Max_image: " + max_image +" Product_count: "+pro_count);


                    });
                        // Prevent submission if limit is exceeded.

                        // End Check for file upload limit
                });
        </script>
    </head>
    <body>

        <?php
            /**
             * Make sure the article id is declared.
             */
            if(  $_SESSION['log_group'] != 'user' && $_SESSION['log_group'] != 'admin' )
            {
                die('<p class="alert alert-error">' . $_LANG['NEWCAR_INVALID_UPLOAD_REQUEST_LABEL'] . '</p>');
            }

            // get car id.
            if( empty($_GET['cid']) ){
                die('<p class="alert alert-error">' . $_LANG['NEWCAR_INVALID_REQUEST_ID_LABEL'] . '</p><p><a href="admin-addpic.php?cid='.$cid.'&mode='.$mode.'">'.$_LANG['NEWCAR_GOBACK_LABEL'].'</a></p>');
            }

            // form check
            if( $form_error ){
                die('<p class="alert alert-error">' . $form_message . '</p><p><a href="admin-addpic.php?cid='.$cid.'&mode='.$mode.'">'.$_LANG['NEWCAR_GOBACK_LABEL'].'</a></p>');
            }
            else {
        ?>
        <!-- <div style="width:100%;overflow:hidden;">

            <!-- The file upload form used as target for the file upload widget -->
            <form method="POST" enctype="multipart/form-data" style="margin:0;" action="?cid=<?php echo $cid;?>&mode=<?php echo $mode;?>">
                <div class="input-append">
                    <input type="file" id="product_images" multiple name="fileInput[]" style="border:1px solid #CCC;width:100%;box-sizing:border-box;" />
                    <input type="hidden" name="caridInput" value="<?php echo $cid;?>" />
                    <input type="submit" id="btn_upload_photo" name="savebtn" class="btn btn-success" value="<?php echo $_LANG['NEWCAR_UPLOAD_BUTTON'];?>" style="display:none;" />

                </div>
            </form>
            <br>
        </div> -->
        <?php
            }
        ?>
    </body>
</html>
