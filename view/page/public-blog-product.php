
<link href="<?php echo BASE_RELATIVE;?>css/blog/blog-product.css" rel="stylesheet" type="text/css" />
    
<div id="sectionContent" class="clearfix">    	
	<?php include('layout/sidebar.php'); ?>
    <div id="sectionFeature" class="clearfix">
    	<div id="proSearch" class="clearfix">
        	<p>Products : (14)</p>
            <select>
            	<option>Part/Accessary</option>
            </select>
        </div>
        <div id="searchForm" class="clearfix">
        	<select>
            	<option>Category 1</option>
            </select>
            <select>
            	<option>Category 1</option>
            </select>
            <select>
            	<option>Category 1</option>
            </select>
            <select>
            	<option>---Any---</option>
            </select>
            <select>
            	<option>---Any---</option>
            </select>
            <input type="text" />
            <input type="reset" value="clear" />
            <input type="submit" value="search" />
        	
        </div>
        <div id="displayOption" class="clearfix">
        	<p>Search Result: 32 Units</p>
            <div class="display">
            	<select>
                    <option>For Sale</option>
                </select>
                <p>Sort by :</p>
                <p>
                	<a href=""><img src="images/blog/product/disThumb.gif" /></a>
                    <a href=""><img src="images/blog/product/disList.gif" /></a>
                    <img src="images/blog/product/new.gif" />
                </p>
                <p>
                	
                	Listed within 3 days
                </p>
            </div>
        </div>
        <div id="listProduct">
            <table width="660" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                    <th>Photos</th>
                    <th>Information</th>
                    <th>Price</th>
                    <th>Seller</th>
                </tr>
                <?php
					for($i=1; $i <= 10; $i++){
				?>
                <tr>
                    <td class="col1">
                    	<img src="images/blog/product/product.jpg" width="120" height="90" />
                    </td>
                    <td class="col2">
                    	<p><b>Batteries 100R</b></p>
                        <p>#IP500357 l Used |</p>
                        <p><img src="images/blog/product/flag.gif" />S.Korea</p>
                    </td>
                    <td class="col3">
                    	<p>Ask for Price</p>
                    </td>
                    <td class="col4">
                    	<p>Kyum Nam Sky Corporation</p>
                        <p><img src="images/blog/product/expert-company.gif" /></p>
                    </td>
                </tr>
                <?php
					}
				?>
            </table>
        </div>
        <div id="paging">
        	<div class="perPage">
            	<p>Listing per page</p>
                <select>
                	<option>10</option>
                    <option>20</option>
                    <option>30</option>
                    <option>40</option>
                    <option>50</option>
                    <option>60</option>
                    <option>70</option>
                </select>
            </div>
            <div class="page">
            	<p><< 1 2 3 >></p>
            </div>
            <div class="goToPage">
            	<p>Go to page</p>
                <input type="text" />
                <input type="submit" value="Go"/>          	
            </div>
            
        </div>
    </div>
</div>