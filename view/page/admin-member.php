<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */

$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'admin';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$members=$_CLASS->getMembers();
//Pagination
$pagination=$_CLASS->pagination_html;
$total_row=$_CLASS->total_num_row;
$current_page=$_CLASS->current_page;
$total_page=$_CLASS->total_page;
if(isset($_GET['member_type1'])) $member_type1=htmlspecialchars($_GET['member_type1']); else $member_type1=""; 
if(isset($_GET['custom_search'])) $custom_search=htmlspecialchars($_GET['custom_search']); else $custom_search=""; 
?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.fancybox').fancybox();
		// Change title type, overlay closing speed
			$(".fancybox-pop").fancybox({
				helpers: {
					title : {					
						type : 'outside'					
					},
					overlay : {
						speedOut : 0
					}
				}
			});	
		$('.fancybox-inner').css('width','auto');
	})

    var member_type1="<?php echo $member_type1;?>";
    var show="<?php if(isset($_GET['show'])) echo htmlspecialchars($_GET['show']);?>";
    
	// Deleted confirm user list page
	function myDeleteUsuerlist(id){		
	var r=confirm("Are you sure want to delete?");			
	if (r==true)
		window.location = 'member?listId='+id;
	}
</script>



 <div id="sectionContenWrapper" class="clearfix">              
   <div id="topMenu">
        <p><a href="#" class="linkfade">Site Manager</a></p>

    </div>
    <?php include("php/sidebar/community.php");?>
    </div>
    <div id="sectionContent"> 
        <div id="admin_tab" class="clearfix">
            <a href="member?member_type1=Seller">
                <div class="tabs" id="tab_1" title="Seller">
                    <p>Seller</p>
                </div>
            </a>
            <a href="member?member_type1=Buyer">
                <div class="tabs" id="tab_2" title="Buyer">
                    <p>Buyer</p>
                </div>
            </a>
            <a href="member">
                <div class="tabs" id="tab_3" title="Recent Members">
                    <p>Recent Members</p>
                </div>
            </a>
        </div>
        <!-- Seller Tab -->
        
       <div id="seller" class="clearfix admin_tab">
            <div class="search_wrapper">
                <form>
                    <input type="hidden" name="member_type1" value="<?php echo $member_type1;?>">
                    <select name="country" class="search country">
                        <?php 
                            include('ajax/register/load_saved_country.php');
                        ?>
                    </select>
                    <select name="register_type" class="search">
                        <?php include('ajax/load_register_type.php'); ?>
                    </select>
                    <select name="product_type" class="search">
                       <?php include('ajax/load_product_types.php'); ?>
                    </select>
                    <input value="<?php echo $custom_search;?>" type="text" placeholder="Company Name..." name="custom_search" class="text_search">
                    <input type="submit" name="submit_search" value="" class="search_button linkfade">
                </form>
            </div>
        </div>
        <!-- end Seller Tab -->
        <!-- Seller Tab -->
        
       <div id="buyer" class="clearfix admin_tab">
            <div class="search_wrapper">
                <form>
                    <input type="hidden" name="member_type1" value="<?php echo $member_type1;?>">
                    <select name="country" class="search country">
                        <?php 
                            include('ajax/register/load_saved_country.php');
                        ?>
                    </select>
                    <select name="register_type" class="search">
                        <?php include('ajax/load_register_type.php'); ?>
                    </select>
                    <select name="product_type" class="search">
                       <?php include('ajax/load_product_types.php'); ?>
                    </select>
                    <input value="<?php echo $custom_search;?>" type="text" placeholder="Company Name..." name="custom_search" class="text_search">
                    <input type="submit" name="submit_search" value="" class="search_button linkfade">
                </form>
            </div>
        </div>
        <!-- end Seller Tab -->
        <!-- Seller Tab -->
        
        <div id="register" class="clearfix admin_tab">
            <div class="search_wrapper">
                <form>
                    <!--<input type="hidden" name="member_type1" value="<?php //echo $member_type1;?>">
                    <select name="country" class="search country">
                        <?php 
                            //include('ajax/register/load_saved_country.php');
                        ?>
                    </select>-->
                    <select name="register_type" class="search">
                        <?php include('ajax/load_register_type.php'); ?>
                    </select>
                    <!--<select name="product_type" class="search">
                       <?php //include('ajax/load_product_types.php'); ?>
                    </select>-->
                    <input value="<?php echo $custom_search;?>" type="text" placeholder="Company Name..." name="custom_search" class="text_search" />
                    <input type="submit" name="submit_search" value="" class="search_button linkfade" />
                </form>
            </div>
        </div>
        <!-- end Seller Tab -->
        
        <?php
        ///////////Show saving result
            if( $fstatus ){
        ?>
        <div class="alert <?php echo $fstyle;?>">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <?php echo $fmessage;?>
        </div>
        <?php
            }
        ///////////End Show saving result
        ?>
        <!-- Content body -->
        <div class="tab_content" id="seller_members_wrapper">
            <form method="post">
                <div class="mode_button_wrapper">
                <input type="submit" name="submit" id="submit" class="update_button" value="">
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title idAuto">ID</td>
						<td class="title middle">Login ID</td>
                        <td class="title middle">Company Name</td>
                        <td class="title large">Email</td>
						<td class="title small">Password</td>
						<td class="title tel">Tel</td>
                        <td class="title memberType">Member Type</td>
                        <td class="title small">Activation</td>
                        <td class="title small">Products</td>
						<td class="title small">Action</td>
                        
                    </tr>
                    <?php 		
						$autoId=$_GET['s']+1;
						foreach($members as $member){	
							$company_name=$member['company_name'];
							$password=$member['temp_password'];
							$mobile=str_replace(",", "\n", $member['mobile']);
							$email=$member['email'];
							$username=$member['username'];
							if (strlen($email) > 25){
								$email = mb_substr($email, 0, 22,'UTF-8') . '...';
							}	
							if (strlen($username) > 19){
								$username = mb_substr($username, 0, 16,'UTF-8') . '...';
							}	
							
							echo "<tr>";	
								echo "
									<td>".$autoId."</td>
									<td title='".$member['username']."' alt='".$member['username']."'>".$username."</td>
									<td>".$company_name."</td>
									<td title='".$member['email']."' alt='".$member['email']."'>".$email."</td>
									<td><a href='#popPassword".$member['user_id']."' class='fancybox-pop pwd'>Password</a></td>
									<td>".$mobile."</td>
									<td>
								";
								echo "<select class='member_type' name='register_type[{$member['user_id']}]'>";
								//auto select member type after the record loaded
								if(!empty($member['register_type'])) $_GET['register_type']=$member['register_type'];
								include('ajax/load_register_type.php');    
								echo "</select>";
								echo "</td><td>";
								if($member['activated']>0){
									$offcheck="";
									$color="";
									$oncheck="selected";
								}else{
									$offcheck="selected";
									$color="style='color:#ee0000'";
									$oncheck="style='color:#000000'";
								}
								echo "<select class='activateSelect' $color name='activated[{$member['user_id']}]'>
										<option $offcheck value='0'>OFF</option>
										<option $oncheck value='1'>ON</option>
								</select>";
								echo "</td>";
								echo "<td width='150'><a style='color:#3765DD;' href='my-inventory?owner={$member['user_id']}'>Total : ".$member['total']."</a></td>";				
								echo "<td><a class='del' href='#' onclick='myDeleteUsuerlist({$member['user_id']})'>Delete</a></td>";	
							echo '</tr>';								
							
							if($password>0){
								$password;
							}else{
								$password="No Password";
							}
							echo '
								<div id="showPop">
									<div id="popPassword'.$member['user_id'].'" class="popPassword">
										<p>Password <br/><font>'.$password.'</font></p>
									</div>
								</div>								
							';
							
							$autoId++;
						}	
						
                    ?>
                    
                   
                </table>								
            </form>
        </div>
        <!-- End Content body -->
        <?php
        echo "<div>";
       	//$kreusna = "<script> alert('sdfsdf'); <script> ";
        echo "<div style='float:right'>Total Users: ".$total_row."</div>";
  
        echo "</div>";
        ?>
        <div id="pagernation">

            <?php 

                 echo $pagination;
                                    
            ?>
              
        </div>
        <!-- end register Tab -->
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
  <div style="clear:both"></div>
  
  