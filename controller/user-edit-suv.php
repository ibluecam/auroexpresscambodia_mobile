<?php

if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class usereditsuv {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $maker_list;
    protected $model_list;
    protected $body_list;
    //protected $feature_icon;
	protected $country_list;
    protected $car_id;
	protected $drive_list;
	protected $vehicle_type_list;
	protected $car_maker_list;
	protected $fuel_type_list;
	protected $transmission_list;
	protected $category_list;
	protected $product_option;
	protected $product_type='Suv';
	var $invalidProduct=false;
	var $tooltip=array();

    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function usereditsuv($lang=array()){
        $this->lang = $lang;

        // define car id.
        require_once BASE_CLASS . 'class-utilities.php';
        $this->car_id = Utilities::generateRandomString(6);

        // load maker list.
        $this->loadMakerList();

        // load body type list.
        $this->loadBodyTypeList();

        // load Drive type list.
        $this->loadDriveTypeList();

        // load featured list.
       // $this->loadFeaturedList();
		// load product option list.
		$this->loadProductOptionList();
         // load country list.
		$this->loadCountryList();
		  // load car Maker list.
		$this->loadCarMakerList();
		// load car Maker list.
		$this->loadCategoryList();

		 // load car Maker list.
		$this->loadFuelTypeList();
		// load car type list.
		$this->loadCarTypeList();
		// load car type list.
		$this->loadTransmissionList();
        // handle save form.
		$user_detail=$this->getUserDetail();
        if(($user_detail['member_type1']!='Seller'&&$user_detail['member_type1']!='Both')
            ||$user_detail['email']==''
            ||$user_detail['country']==''
            ||$user_detail['company_name']==''
        ){
            //Invalid Seller
            header("Location: edit-myinfo?msg=incomplete_seller");
        }
        if( isset($_POST['savebtn']) ){



			// include("ajax/data_count.php");


			$this->handleSaveForm();

			// header("Location:".BASE_RELATIVE."my-inventory?product_type=suv");


        }
    }
    private function getUserDetail(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $user_id=$_SESSION['log_id'];
        $user_detail=array();
        $sql="SELECT member_type1, email, country, company_name FROM register WHERE `id`='$user_id' ";
        $result=mysql_query($sql);
        while($row=mysql_fetch_assoc($result)){
            $user_detail=$row;
        }
        $cnx->close();
        return $user_detail;
    }

    /**
     * Private method: handle save form
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleSaveForm(){
        // car id.
        $car_id             = trim($_POST['caridInput']);
		//$primary_photo=$_POST['primary_photo'];

		/// Enable only one product type
        $productType		= "Suv";
		//$product_type		= "Truck";
		//$product_type		= "Bus";
		//$product_type		= "Equipment";
		//$product_type		= "Part";
		require_once BASE_CLASS . 'class-connect.php';
		$cnx = new Connect();
		$cnx->open();



		( isset($_POST['categorySelect'])       	? 	$categorySelect = trim($_POST['categorySelect']) : $categorySelect = '' );
		( isset($_POST['subCategoryGroupSelect'])   ? 	$subCategoryGroupSelect = trim($_POST['subCategoryGroupSelect']) : $subCategoryGroupSelect = '' );
		( isset($_POST['subCategorySelect'])       	? 	$subCategorySelect = trim($_POST['subCategorySelect']) : $subCategorySelect = '' );
		( isset($_POST['partNameInput'])       		? 	$partNameInput = trim($_POST['partNameInput'])  : $partNameInput = '' );
		( isset($_POST['conditionInput'])       	? 	$conditionInput = trim($_POST['conditionInput']) : $conditionInput = 'New' );
		( isset($_POST['unitWeightInput'])       	? 	$unitWeightInput = trim($_POST['unitWeightInput']) : $unitWeightInput = '' );
		( isset($_POST['partNumberInput'])       	? 	$partNumberInput = (int)($_POST['partNumberInput'])  : $partNumberInput = 0 );
		( isset($_POST['manufacturerInput'])       	? 	$manufacturerInput = trim($_POST['manufacturerInput']) : $manufacturerInput = '' );
		( isset($_POST['madeInInputSelect'])       	? 	$madeInInputSelect = trim($_POST['madeInInputSelect']) : $madeInInputSelect = '' );
		( isset($_POST['fittingMakeInput'])       	? 	$fittingMakeInput = trim($_POST['fittingMakeInput']) : $fittingMakeInput = '' );
		( isset($_POST['colorInput'])       		? 	$colorInput = trim($_POST['colorInput']) : $colorInput = '' );
		( isset($_POST['vehicleTypeSelect'])       	? 	$vehicleTypeSelect = trim($_POST['vehicleTypeSelect']) : $vehicleTypeSelect = '' );
		( isset($_POST['makerInputSelect'])       	? 	$makerInputSelect = trim($_POST['makerInputSelect'])  : $makerInputSelect = '' );
		( isset($_POST['makerInput'])       	? 	$makerInput = trim($_POST['makerInput'])  : $makerInput = '' );
		( isset($_POST['modelInputSelect'])     	? 	$modelInputSelect = trim($_POST['modelInputSelect'])  : $modelInputSelect = '' );
		( isset($_POST['modelInput'])     	? 	$modelInput = trim($_POST['modelInput'])  : $modelInput = '' );
		( isset($_POST['classInput'])       		? 	$classInput = trim($_POST['classInput'])  : $classInput = '' );
		( isset($_POST['chassisNoInput'])       		? 	$chassisNoInput = trim($_POST['chassisNoInput'])  : $chassisNoInput = '' );
		( isset($_POST['engineModelInput'])       		? 	$engineModelInput = trim($_POST['engineModelInput'])  : $engineModelInput = '' );


		( isset($_POST['manDateInput'])       		? 	$manDateInput = trim($_POST['manDateInput'])   : $manDateInput = '' );
		( isset($_POST['yearInput'])       			? 	$yearInput = (int)($_POST['yearInput']) : $yearInput = 0 );
		( isset($_POST['milesInput'])       		? 	$milesInput = (int)($_POST['milesInput'])  : $milesInput = 0 );
		( isset($_POST['measureTypeInput'])       	? 	$measureTypeInput = trim($_POST['measureTypeInput'])  : $measureTypeInput = '' );
		( isset($_POST['engineVolumeInput'])       	? 	$engineVolumeInput = (int)($_POST['engineVolumeInput'])  : $engineVolumeInput = 0 );
		( isset($_POST['axleQtyInput'])       		? 	$axleQtyInput = (int)($_POST['axleQtyInput'])  : $axleQtyInput = 0 );
		( isset($_POST['steeringInput'])       		? 	$steeringInput = trim($_POST['steeringInput'])  : $steeringInput = 'LHD' );
		( isset($_POST['transmissionInputSelect'])  ? 	$transmissionInputSelect = trim($_POST['transmissionInputSelect'])               : $transmissionInputSelect = '' );
		( isset($_POST['carFuelInputSelect'])       ? 	$carFuelInputSelect = trim($_POST['carFuelInputSelect'])  : $carFuelInputSelect = '' );
		( isset($_POST['citySelect'])       ? 	$citySelect = trim($_POST['citySelect'])  : $citySelect = '' );
		( isset($_POST['cityInput'])       ? 	$cityInput = trim($_POST['cityInput'])  : $cityInput = '' );
		( isset($_POST['numberCargoInput'])       	? 	$numberCargoInput = (int)($_POST['numberCargoInput']) : $numberCargoInput = 0 );
		( isset($_POST['cargoBoxSizeInput'])       	? 	$cargoBoxSizeInput = trim($_POST['cargoBoxSizeInput']) : $cargoBoxSizeInput = '' );
		( isset($_POST['lengthInput'])       		? 	$lengthInput = trim($_POST['lengthInput']) : $lengthInput = '' );
		( isset($_POST['heightInput'])       		? 	$heightInput = trim($_POST['heightInput']) : $heightInput = '' );
		( isset($_POST['widthInput'])       		? 	$widthInput = trim($_POST['widthInput']) : $widthInput = '' );
		( isset($_POST['totalWeightInput'])       	? 	$totalWeightInput = trim($_POST['totalWeightInput']) : $totalWeightInput = '' );
		// ( isset($_POST['countryInputSelect'])       ? 	$countryInputSelect = trim($_POST['countryInputSelect']) : $countryInputSelect = '' );
		( isset($_POST['priceInput'])       		? 	$priceInput = (float)($_POST['priceInput']) : $priceInput = 0 );
		( isset($_POST['currencySelect'])       	? 	$currencySelect = strtoupper ($_POST['currencySelect']) : $currencySelect = 'USD' );
		( isset($_POST['loadingWeightInput'])       ? 	$loadingWeightInput = trim($_POST['loadingWeightInput']) : $loadingWeightInput = '' );
		//( isset($_POST['vinInput'])       			? 	$vinInput = trim($_POST['vinInput'])  : $vinInput = '' );
		( isset($_POST['numberPassengerInput'])     ? 	$numberPassengerInput = (int)($_POST['numberPassengerInput']) : $numberPassengerInput = 0 );

		( isset($_POST['doorInput'])     ? 	$doorInput = (int)($_POST['doorInput']) : $doorInput = 0 );
		( isset($_POST['bodyColorInput'])       	? 	$bodyColorInput = trim($_POST['bodyColorInput']) : $bodyColorInput = '' );
		( isset($_POST['enginePowerInput'])       	? 	$enginePowerInput = (int)($_POST['enginePowerInput']) : $enginePowerInput = 0 );
		( isset($_POST['numberCylinderInput'])      ? 	$numberCylinderInput = (int)($_POST['numberCylinderInput'])               : $numberCylinderInput = 0 );
		( isset($_POST['driveAvail'])       		? 	$driveAvail = trim($_POST['driveAvail']) : $driveAvail = 0 );
		( isset($_POST['driveInputSelect'])       	? 	$driveInputSelect = trim($_POST['driveInputSelect']) : $driveInputSelect = '' );
		( isset($_POST['paymentTermCheck'])       	? 	$paymentTermCheck = $this->getAllCheckbox('paymentTermCheck') : $paymentTermCheck = '' );
		( isset($_POST['otherOptionsCheck'])       	? 	$otherOptionsCheck = $this->getAllCheckbox('otherOptionsCheck') : $otherOptionsCheck = '' );
		( isset($_POST['safetyDeviceOptionsCheck'])       	? 	$safetyDeviceOptionsCheck = $this->getAllCheckbox('safetyDeviceOptionsCheck') : $safetyDeviceOptionsCheck = '' );
		( isset($_POST['exOptionsCheck'])       	? 	$exOptionsCheck = $this->getAllCheckbox('exOptionsCheck') : $exOptionsCheck = '' );
		( isset($_POST['inOptionsCheck'])       	? 	$inOptionsCheck = $this->getAllCheckbox('inOptionsCheck') : $inOptionsCheck = '' );

       	( isset($_POST['sellerCommentText'])       	? 	$sellerCommentText = trim($_POST['sellerCommentText'])  : $sellerCommentText = '' );
        // strip all slashes.

		$categorySelect          	= stripslashes($categorySelect);
		$subCategoryGroupSelect     = stripslashes($subCategoryGroupSelect);
		$subCategorySelect          = stripslashes($subCategorySelect);
		$partNameInput          	= stripslashes($partNameInput);
		$conditionInput          	= stripslashes($conditionInput);
		$unitWeightInput          	= stripslashes($unitWeightInput);
		$partNumberInput          	= stripslashes($partNumberInput);
		$manufacturerInput          = stripslashes($manufacturerInput);
		$madeInInputSelect          = stripslashes($madeInInputSelect);
		$fittingMakeInput          	= stripslashes($fittingMakeInput);
		$colorInput          		= stripslashes($colorInput);
		$vehicleTypeSelect          = stripslashes($vehicleTypeSelect);
		$makerInputSelect         	= stripslashes($makerInputSelect);
		$modelInputSelect        	= stripslashes($modelInputSelect);
		$classInput          		= stripslashes($classInput);
		$chassisNoInput          	= stripslashes($chassisNoInput);
		$engineModelInput          	= stripslashes($engineModelInput);

		$manDateInput          		= stripslashes($manDateInput);
		$yearInput          		= stripslashes($yearInput);
		$milesInput          		= stripslashes($milesInput);
		$measureTypeInput          	= stripslashes($measureTypeInput);
		$engineVolumeInput          = stripslashes($engineVolumeInput);
		$axleQtyInput         		= stripslashes($axleQtyInput);
		$steeringInput          	= stripslashes($steeringInput);
		$transmissionInputSelect    = stripslashes($transmissionInputSelect);
		$carFuelInputSelect         = stripslashes($carFuelInputSelect);
		$citySelect					= stripslashes($citySelect);
		$cityInput					= stripslashes($cityInput);
		$numberCargoInput          	= stripslashes($numberCargoInput);
		$cargoBoxSizeInput          = stripslashes($cargoBoxSizeInput);
		$lengthInput          		= stripslashes($lengthInput);
		$heightInput          		= stripslashes($heightInput);
		$widthInput          		= stripslashes($widthInput);
		$totalWeightInput          	= stripslashes($totalWeightInput);
		// $countryInputSelect        	= stripslashes($countryInputSelect);
		$priceInput          		= stripslashes($priceInput);
		$currencySelect          	= stripslashes($currencySelect);
		$loadingWeightInput         = stripslashes($loadingWeightInput);
		//$vinInput          			= stripslashes($vinInput);
		$numberPassengerInput       = stripslashes($numberPassengerInput);
		$doorInput          		= stripslashes($doorInput);
		$bodyColorInput          	= stripslashes($bodyColorInput);
		$enginePowerInput          	= stripslashes($enginePowerInput);
		$numberCylinderInput        = stripslashes($numberCylinderInput);
		$driveAvail          		= stripslashes($driveAvail);
		$driveInputSelect          	= stripslashes($driveInputSelect);
		$paymentTermCheck          	= stripslashes($paymentTermCheck);
		$otherOptionsCheck          = stripslashes($otherOptionsCheck);
		$safetyDeviceOptionsCheck   = stripslashes($safetyDeviceOptionsCheck);
		$exOptionsCheck          	= stripslashes($exOptionsCheck);
		$inOptionsCheck          	= stripslashes($inOptionsCheck);
		$sellerCommentText          = stripslashes($sellerCommentText);

		$owner					= (int)$_SESSION['log_id'];
		if($_SESSION['log_group']=='admin'){
			$productOwner=$this->getOwnerByProductId($car_id);
			if(!empty($productOwner)){
				$owner=$productOwner;
			}
		}

        if( empty($measure_type) ){ $measure_type = 'Km'; }


        $categorySelect          	= @mysql_real_escape_string(htmlspecialchars($categorySelect));
		$subCategoryGroupSelect     = @mysql_real_escape_string(htmlspecialchars($subCategoryGroupSelect));
		$subCategorySelect          = @mysql_real_escape_string(htmlspecialchars($subCategorySelect));
		$partNameInput          	= @mysql_real_escape_string(htmlspecialchars($partNameInput));
		$conditionInput          	= @mysql_real_escape_string(htmlspecialchars($conditionInput));
		$unitWeightInput          	= @mysql_real_escape_string(htmlspecialchars($unitWeightInput));
		$partNumberInput          	= @mysql_real_escape_string(htmlspecialchars($partNumberInput));
		$manufacturerInput          = @mysql_real_escape_string(htmlspecialchars($manufacturerInput));
		$madeInInputSelect          = @mysql_real_escape_string(htmlspecialchars($madeInInputSelect));
		$fittingMakeInput          	= @mysql_real_escape_string(htmlspecialchars($fittingMakeInput));
		$colorInput          		= @mysql_real_escape_string(htmlspecialchars($colorInput));
		$vehicleTypeSelect          = @mysql_real_escape_string(htmlspecialchars($vehicleTypeSelect));
		$makerInputSelect         	= @mysql_real_escape_string(htmlspecialchars($makerInputSelect));
		$makerInput         		= @mysql_real_escape_string(htmlspecialchars($makerInput));
		$modelInputSelect        	= @mysql_real_escape_string(htmlspecialchars($modelInputSelect));
		$modelInput        			= @mysql_real_escape_string(htmlspecialchars($modelInput));
		$classInput          		= @mysql_real_escape_string(htmlspecialchars($classInput));
		$manDateInput          		= @mysql_real_escape_string(htmlspecialchars($manDateInput));
		$yearInput          		= @mysql_real_escape_string(htmlspecialchars($yearInput));
		$milesInput          		= @mysql_real_escape_string(htmlspecialchars($milesInput));
		$measureTypeInput          	= @mysql_real_escape_string(htmlspecialchars($measureTypeInput));
		$engineVolumeInput          = @mysql_real_escape_string(htmlspecialchars($engineVolumeInput));
		$axleQtyInput         		= @mysql_real_escape_string(htmlspecialchars($axleQtyInput));
		$steeringInput          	= @mysql_real_escape_string(htmlspecialchars($steeringInput));
		$transmissionInputSelect    = @mysql_real_escape_string(htmlspecialchars($transmissionInputSelect));
		$carFuelInputSelect         = @mysql_real_escape_string(htmlspecialchars($carFuelInputSelect));
		$citySelect					= @mysql_real_escape_string(htmlspecialchars($citySelect));
		$cityInput					= @mysql_real_escape_string(htmlspecialchars($cityInput));
		$numberCargoInput          	= @mysql_real_escape_string(htmlspecialchars($numberCargoInput));
		$cargoBoxSizeInput          = @mysql_real_escape_string(htmlspecialchars($cargoBoxSizeInput));
		$lengthInput          		= @mysql_real_escape_string(htmlspecialchars($lengthInput));
		$heightInput          		= @mysql_real_escape_string(htmlspecialchars($heightInput));
		$widthInput          		= @mysql_real_escape_string(htmlspecialchars($widthInput));
		$totalWeightInput          	= @mysql_real_escape_string(htmlspecialchars($totalWeightInput));
		// $countryInputSelect        	= @mysql_real_escape_string(htmlspecialchars($countryInputSelect));
		$citySelect					= @mysql_real_escape_string(htmlspecialchars($citySelect));
		$cityInput					= @mysql_real_escape_string(htmlspecialchars($cityInput));
		$priceInput          		= @mysql_real_escape_string(htmlspecialchars($priceInput));
		$currencySelect        		= @mysql_real_escape_string(htmlspecialchars($currencySelect));
		$loadingWeightInput         = @mysql_real_escape_string(htmlspecialchars($loadingWeightInput));
		//$vinInput          			= @mysql_real_escape_string(htmlspecialchars($vinInput));
		$numberPassengerInput       = @mysql_real_escape_string(htmlspecialchars($numberPassengerInput));
		$doorInput          		= @mysql_real_escape_string(htmlspecialchars($doorInput));
		$bodyColorInput          	= @mysql_real_escape_string(htmlspecialchars($bodyColorInput));
		$enginePowerInput          	= @mysql_real_escape_string(htmlspecialchars($enginePowerInput));
		$numberCylinderInput        = @mysql_real_escape_string(htmlspecialchars($numberCylinderInput));
		$driveAvail          		= @mysql_real_escape_string(htmlspecialchars($driveAvail));
		$driveInputSelect          	= @mysql_real_escape_string(htmlspecialchars($driveInputSelect));
		$paymentTermCheck          	= @mysql_real_escape_string(htmlspecialchars($paymentTermCheck));
		$otherOptionsCheck          = @mysql_real_escape_string(htmlspecialchars($otherOptionsCheck));
		$safetyDeviceOptionsCheck   = @mysql_real_escape_string(htmlspecialchars($safetyDeviceOptionsCheck));
		$exOptionsCheck          	= @mysql_real_escape_string(htmlspecialchars($exOptionsCheck));
		$inOptionsCheck          	= @mysql_real_escape_string(htmlspecialchars($inOptionsCheck));
		$sellerCommentText          = @mysql_real_escape_string($sellerCommentText);
		$chassisNoInput          	= @mysql_real_escape_string(htmlspecialchars($chassisNoInput));
		$engineModelInput          	= @mysql_real_escape_string(htmlspecialchars($engineModelInput));

        //GET maker name by giving make id parameter
		$makerInputSelect=$this->getMaker($makerInputSelect);
		if(!empty($makerInput)){
			$makerInputSelect=$makerInput;
		}
		if(!empty($modelInput)){
			$modelInputSelect=$modelInput;
		}
		if(!empty($cityInput)){
			$citySelect=$cityInput;
		}

		if(empty($makerInputSelect) || empty($modelInputSelect)){

            $cnx->close();

            $this->form_message = "Unable to save car. Please select or fill in the required fields.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            //$this->removeMedia($car_id);
            return;
		}



		$citySelect=ucwords(strtolower($citySelect));
		$countryInputSelect=$this->getUserCountry();
		if(!$this->validateCountry($countryInputSelect)||!$this->validateCurrency($currencySelect)){
			require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            LogReport::write('Unable to save to car table due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = "Unable to save car due to invalid currency or country.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            //$this->removeMedia($car_id);
            return;
		}


		$sqldatacountUpdate="UPDATE `data_count` SET `car_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Car' AND del_flag=0 and `owner`='$owner'),
			`truck_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Truck' AND del_flag=0 AND `owner`='$owner'),
			`bus_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Bus' AND del_flag=0 AND `owner`='$owner'),
			 `part_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Part' AND del_flag=0 AND `owner`='$owner'),
			`accessories_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Accessories' AND del_flag=0 AND `owner`='$owner'),
			`equipment_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Equipment' AND del_flag=0 and `owner`='$owner'),
			`motorbike_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Motorbike' AND del_flag=0 and `owner`='$owner'),
			`aircraft_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Aircraft' AND del_flag=0 and `owner`='$owner'),
			`watercraft_count`= (SELECT COUNT(product_type) FROM product WHERE product_type='Watercraft' AND del_flag=0 AND `owner`='$owner'),
			`suv_count`=(SELECT COUNT(product_type) FROM product WHERE product_type='Suv' AND del_flag=0 and `owner`='$owner')
			 WHERE register_id='$owner';
		  ";

       $sqlUpdate="UPDATE `product` SET `category` = '$categorySelect',
            `sub_category_group`  = '$subCategoryGroupSelect',
            `sub_category`  = '$subCategorySelect',
            `part_name`  = '$partNameInput',
            `condition`  = '$conditionInput',
            `unit_weight`  = '$unitWeightInput',
            `part_number` = '$partNumberInput',
            `manufacturer` = '$manufacturerInput',
            `made_in` = '$madeInInputSelect',
            `fitting_make` = '$fittingMakeInput',
            `color` = '$colorInput',
            `vehicle_type` = '$vehicleTypeSelect',
            `make` = '$makerInputSelect',
            `model` = '$modelInputSelect',
            `class` = '$classInput',
			`chassis_no` = '$chassisNoInput',
			`engine_model` = '$engineModelInput',
            `manufacturer_date` = '$manDateInput',
            `model_year` = '$yearInput',
            `mileage` = '$milesInput',
            `mileage_type` = '$measureTypeInput',
            `engine_volume` = '$engineVolumeInput',
            `axle_qty` = '$axleQtyInput',
            `steering` = '$steeringInput',
            `transmission` = '$transmissionInputSelect',
            `fuel_type` = '$carFuelInputSelect',
		   `number_cargo` = '$numberCargoInput',
		   `cargo_box_size` = '$cargoBoxSizeInput',
		   `length` =  '$lengthInput',
		   `height` = '$heightInput',
		   `width` =  '$widthInput',
		   `total_weight` = '$totalWeightInput',
		   `location` = '$countryInputSelect',
		   `city`='$citySelect',
		   `price` = '$priceInput',
		   `currency` = '$currencySelect',
		   `loading_weight` = '$loadingWeightInput',
		   `number_passenger` = '$numberPassengerInput',
		   `door` = '$doorInput',
		   `exterior_color` = '$bodyColorInput',
		   `engine_power` = '$enginePowerInput',
		   `number_cylinder` = '$numberCylinderInput',
		   `drive_availability` = '$driveAvail',
		   `drive_type` = '$driveInputSelect',
			   `payment_terms` = '$paymentTermCheck',
		   `created_date` = NOW(),
           `other_options` = '$otherOptionsCheck',
		   `safety_device_options` = '$safetyDeviceOptionsCheck',
		   `exterior_options` = '$exOptionsCheck',
		   `interior_options` = '$inOptionsCheck',
		   `seller_comment`= '$sellerCommentText'
			WHERE `id`='$car_id' AND `owner`='$owner' AND `product_type`='$this->product_type';
       ";

		$sqlInsert="INSERT INTO `product` (
                                                `id`,
												`owner`,
												`category`,
                                                `sub_category_group`,
                                                `sub_category`,
                                                `part_name`,
                                                `condition`,
                                                `unit_weight`,
                                                `part_number`,
                                                `manufacturer`,
                                                `made_in`,
                                                `fitting_make`,
                                                `color`,
                                                `vehicle_type`,
                                                `make`,
                                                `model`,
                                                `class`,
												`chassis_no`,
												`engine_model`,
                                                `manufacturer_date`,
                                                `model_year`,
                                                `mileage`,
                                                `mileage_type`,
                                                `engine_volume`,
                                                `axle_qty`,
                                                `steering`,
                                                `transmission`,
                                                `fuel_type`,
											   `number_cargo`,
											   `cargo_box_size`,
											   `length`,
											   `height`,
											   `width`,
											   `total_weight`,
											   `location`,
											   `city`,
											   `price`,
											   `currency`,
											   `loading_weight`,
											   `number_passenger`,
											   `door`,
											   `exterior_color`,
											   `engine_power`,
											   `number_cylinder`,
											   `drive_availability`,
											   `drive_type`,
               								   `payment_terms`,
											   `created_date`,
                                               `other_options`,
											   `safety_device_options`,
											   `exterior_options`,
											   `interior_options`,
											   `seller_comment`,
											   `product_type`
                                             ) VALUES (
                                                '$car_id',
												'$owner',
												'$categorySelect',
												'$subCategoryGroupSelect',
												'$subCategorySelect',
												'$partNameInput',
												'$conditionInput',
												'$unitWeightInput',
												'$partNumberInput',
												'$manufacturerInput',
												'$madeInInputSelect',
												'$fittingMakeInput',
												'$colorInput',
												'$vehicleTypeSelect',
												'$makerInputSelect',
												'$modelInputSelect',
												'$classInput',
												'$chassisNoInput',
												'$engineModelInput',
												'$manDateInput',
												'$yearInput',
												'$milesInput',
												'$measureTypeInput',
												'$engineVolumeInput',
												'$axleQtyInput',
												'$steeringInput',
												'$transmissionInputSelect',
												'$carFuelInputSelect',
												'$numberCargoInput',
                                                '$cargoBoxSizeInput',
                                                '$lengthInput',
                                                '$heightInput',
                                                '$widthInput',
                                                '$totalWeightInput',
                                                '$countryInputSelect',
                                                '$citySelect',
                                                '$priceInput',
                                                '$currencySelect',
                                                '$loadingWeightInput',
                                                '$numberPassengerInput',
                                                '$doorInput',
                                                '$bodyColorInput',
                                                '$enginePowerInput',
                                                '$numberCylinderInput',
                                                '$driveAvail',
                                                '$driveInputSelect',
                                                '$paymentTermCheck',
												NOW(),
												'$otherOptionsCheck',
												'$safetyDeviceOptionsCheck',
												'$exOptionsCheck',
												'$inOptionsCheck',
												'$sellerCommentText',
												'$this->product_type'
                                             )";

		$sqldatacountInsert="INSERT INTO `data_count` (`register_id`,`car_count`,`truck_count`,`bus_count`, `part_count`,`accessories_count`,`equipment_count`,`motorbike_count`,`aircraft_count`,`watercraft_count`,`suv_count`,`van_count`)
		VALUES (
		'$owner',
		(SELECT COUNT(product_type) FROM product WHERE product_type='Car' AND del_flag=0 and `owner`='$owner'),
		(SELECT COUNT(product_type) FROM product WHERE product_type='Truck' AND del_flag=0 AND `owner`='$owner'),
		(SELECT COUNT(product_type) FROM product WHERE product_type='Bus' AND del_flag=0 AND `owner`='$owner'),
		(SELECT COUNT(product_type) FROM product WHERE product_type='Part' AND del_flag=0 AND `owner`='$owner'),
		 (SELECT COUNT(product_type) FROM product WHERE product_type='Accessories' AND del_flag=0 AND `owner`='$owner'),

		 (SELECT COUNT(product_type) FROM product WHERE product_type='Equipment' AND del_flag=0 and `owner`='$owner'),
		 (SELECT COUNT(product_type) FROM product WHERE product_type='Motorbike' AND del_flag=0 and `owner`='$owner'),
		 (SELECT COUNT(product_type) FROM product WHERE product_type='Aircraft' AND del_flag=0 and `owner`='$owner'),
		 (SELECT COUNT(product_type) FROM product WHERE product_type='Watercraft' AND del_flag=0 AND `owner`='$owner'),
		 (SELECT COUNT(product_type) FROM product WHERE product_type='Suv' AND del_flag=0 and `owner`='$owner'),
		 (SELECT COUNT(product_type) FROM product WHERE product_type='Van' AND del_flag=0 and `owner`='$owner')

		)";

		//$sql=$sqlInsert;
		if(!$this->existedProduct($car_id, $owner)){
			if($this->countSavedProduct()<$this->getProductLimit()){
				$sql=$sqlInsert;
			}else{
	            $cnx->close();
	            $this->form_message = "Unable to add more products! You have to upgrade your member type to upload more products";
	            $this->form_status = true;
	            $this->form_style = 'alert-warning';
	            return false;
			}
		}else{
			$sql=$sqlUpdate;
		}

     	if(!$this->existedData_count($owner)){
	       	$sqldata=$sqldatacountInsert;
	       	// echo $sqldata;

       }else{
	       	$sqldata=$sqldatacountUpdate;
	       	// echo $sqldata;

        }


        $cnx = new Connect();
		$cnx->open();
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();

            LogReport::write('Unable to save to car table due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = "Unable to save car due an internal error. See log for details.";
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return false;
        }
        //save to product_maker and product_model table
       	$insertedMakeId=$this->getMakeId($makerInputSelect);
       	if(empty($insertedMakeId)){
       		$insertedMakeId=$this->insertMake($makerInputSelect);
       	}

        if(!$this->existModel($modelInputSelect, $insertedMakeId)){
        	$this->insertModel($modelInputSelect, $insertedMakeId);
        }
        if( !@mysql_query($sqldata) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
        }

        $image_id="";
		if(isset($_POST['primary_photo']) && isset($_GET['cid'])){
			$imge_primary=$_POST['primary_photo'];
			foreach($imge_primary as $img){
				if($img<>""){
					$image_id=$img;
				}
			}

				$sql_primary_photo="UPDATE car_media set primary_photo=0 where car_media.product_id='".$car_id."'";

								if(mysql_query($sql_primary_photo)){
									//echo "Success";
								}
								else{
									//echo $sql_primary_photo."<br>-=====--";
									//echo mysql_error();
								}
								 if(mysql_query("UPDATE car_media set primary_photo=1 where car_media.source='".$image_id."'")){
									// echo "Success Set Primary";
								 }
									else{

									//echo mysql_error();
								}
				// $cnx->close();
				// $this->form_message =  $this->lang['PRODUCT_SAVE_QUERY_SUCCESS'];
				// $this->form_status = true;
				// $this->form_style = 'alert-success';
		}
		//Save to product_make and model

		// executeDataCount();

		$cnx->close();
		 header("Location:".BASE_RELATIVE."my-inventory?product_type=suv");

        $this->form_message =  $this->lang['PRODUCT_SAVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return true;

 }
 //funnctions for validate max product limit
private function countSavedProduct(){
	require_once BASE_CLASS . 'class-connect.php';
	$cnx = new Connect();
	$cnx->open();
	$owner=$_SESSION['log_id'];
	$sql="SELECT COUNT(*) as `number` FROM product WHERE owner='$owner'";
	$result=mysql_query($sql);
    while($row=mysql_fetch_assoc($result)){
    	return $row['number'];
    }
}
private function getProductLimit(){
	require_once BASE_CLASS . 'class-connect.php';
	$cnx = new Connect();
	$cnx->open();
	$register_type=$_SESSION['register_type'];
	$sql="SELECT * FROM register_types WHERE code='$register_type'";
	$result=mysql_query($sql);
    while($row=mysql_fetch_assoc($result)){
    	return $row['max_product'];
    }
}
 private function insertMake($make){
 	require_once BASE_CLASS . 'class-connect.php';
	$cnx = new Connect();
	$cnx->open();
	$sql="INSERT INTO product_maker (`maker`, `product_type`) VALUES ('$make', '{$this->product_type}')";
    $result=mysql_query($sql);
    return mysql_insert_id();
    $cnx->close();
 }
 private function getMakeId($make){
 	require_once BASE_CLASS . 'class-connect.php';
	$cnx = new Connect();
	$cnx->open();
	$sql="SELECT `id` FROM product_maker WHERE maker='{$make}' AND product_type='{$this->product_type}'";
    $result=mysql_query($sql);
    while($row=mysql_fetch_assoc($result)){
    	$cnx->close();
    	return $row['id'];
    }
    $cnx->close();
 }
 private function insertModel($model, $make_id){
	require_once BASE_CLASS . 'class-connect.php';
	$cnx = new Connect();
	$cnx->open();
	$sql="INSERT INTO product_model (`model`, `maker`) VALUES ('$model', '$make_id')";
    $result=mysql_query($sql);
    return mysql_insert_id();
    $cnx->close();
 }
 private function existModel($model, $make_id){
	require_once BASE_CLASS . 'class-connect.php';
	$cnx = new Connect();
	$cnx->open();
	$sql="SELECT `id` FROM product_model WHERE `maker`='{$make_id}' AND `model`='{$model}'";
    $result=mysql_query($sql);
    if(mysql_num_rows($result)>0){
    	$cnx->close();
    	return true;
    }
    $cnx->close();
}
 private function saveCount(){

		require_once BASE_CLASS . 'class-connect.php';
		$cnx = new Connect();
		$cnx->open();

		$owner = (int)$_SESSION['log_id'];

        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();

            LogReport::write('Unable to save to save count table due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PRODUCT_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $cnx->close();
            return;
        }


		$cnx->close();

        $this->form_message =  $this->lang['PRODUCT_SAVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';

 }
  private function validateCurrency($currency_iso){
  	require_once BASE_CLASS . 'class-connect.php';
    $cnx = new Connect();
    $cnx->open();
 	$sql="SELECT * FROM currency_list WHERE `currency_iso`='$currency_iso'";
 	$result=mysql_query($sql);
 	if(!$result){
 		return false;
 	}
 	if(mysql_num_rows($result)>0){
 		return true;
 	}
 	@mysql_free_result($sql);
    $cnx->close();
 }
 private function validateCountry($cc){
 	require_once BASE_CLASS . 'class-connect.php';

    $cnx = new Connect();
    $cnx->open();
 	$sql="SELECT * FROM country_list WHERE cc='$cc'";
 	$result=mysql_query($sql);
 	if(!$result){
 		return false;
 	}
 	if(mysql_num_rows($result)>0){
 		return true;
 	}
 	@mysql_free_result($sql);
    $cnx->close();
 }
 public function getUserCountry(){
 	require_once BASE_CLASS . 'class-connect.php';

    $cnx = new Connect();
    $cnx->open();
    $id=$_SESSION['log_id'];
 	$sql="SELECT country FROM register WHERE id='$id'";
 	$result=@mysql_query($sql);
 	while($row=@mysql_fetch_assoc($result)){
 		return $row['country'];
 	}
 	@mysql_free_result($sql);
    $cnx->close();
 }
    public function getAllProducts($cid){
		$product=array();

		require_once BASE_CLASS . 'class-connect.php';
		$owner					= (int)$_SESSION['log_id'];
        if($_SESSION['log_group']!='admin'){
        	$and=" AND `owner`='$owner' ";
		}
        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `product` WHERE `id`='$cid' $and AND `product_type`='$this->product_type';") ){
            require_once BASE_CLASS . 'class-log.php';

            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
        	$this->invalidProduct=true;
            $cnx->close();
            return;
        }


        while( $r = @mysql_fetch_assoc($sql) ){
           	foreach($r as $column=>$value) {
				$product[$column]=$value;
			}
        }
        return $product;
        @mysql_free_result($sql);
        $cnx->close();


	}
    /**
     * Private method: remove current vehicle's media
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function removeMedia($carid){
        if( empty($carid) ){
            return;
        }
        $author = (int)$_SESSION['log_id'];
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        if( $sql = @mysql_query("SELECT `car_id`,`source` FROM `car_media` WHERE `car_id`='$carid' AND `author`=$author") ){
            if( @mysql_num_rows($sql) > 0 ){
                while( $r=@mysql_fetch_assoc($sql) ){
                    @unlink(BASE_ROOT . $r['source']);
                }
                @mysql_free_result($sql);
            }
        }

        @mysql_query("DELETE FROM `car_media` WHERE `car_id`='$carid' AND `author`=$author");

        $cnx->close();
    }

    /**
     * Private method: load feature icons
     * @return array
     */
    /*private function loadFeaturedList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `feature_list` ORDER BY `label` ASC") ){
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to load feature icon list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->feature_icon = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->feature_icon = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'label' => stripslashes($r['label']),
                'source' => $r['source']
            );

            array_push($this->feature_icon,$obj);
        }

        @mysql_free_result($sql);
        $cnx->close();
    }*/
     /**
     * Private method: load option list
     * @return array
     */
	private function existedProduct($cid, $owner){

		require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `product` WHERE `id`='$cid' AND `owner`='$owner';") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            return false;
        }

        if( @mysql_num_rows($sql) >0  ){
            return true;

        }

        @mysql_free_result($sql);


	}
	/**
     * Private method: load option list
     * @return array
     */
	private function getOwnerByProductId($cid){

		require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT owner FROM `product` WHERE `id`='$cid';") ){
            $cnx->close();
            return false;
        }
        while($row=mysql_fetch_assoc($sql)){
        	return $row['owner'];
        }
        return '';
        @mysql_free_result($sql);
        $cnx->close();

	}
	private function existedData_count($owner){

		require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `data_count` WHERE `register_id`='$owner';") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            return false;
        }

        if( @mysql_num_rows($sql) >0  ){
            return true;
        }

        @mysql_free_result($sql);


	}
    private function loadProductOptionList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `product_option` ORDER BY `option_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to load option list due a query error at ' . __FILE__ . ':' . __LINE__);


            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }


        $this->product_option=array();
        while( $r = @mysql_fetch_assoc($sql) ){
           $this->product_option[$r['option_type']][] = $r['option_name'];
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load body type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadBodyTypeList(){
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';

        if( file_exists($path) ){
            require_once $path;
            $this->body_list = $_CAR_BODY;
        }
        else {
            $this->body_list = array();
        }
    }
	/**
     * Private method: load drive type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    private function loadDriveTypeList(){
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-drive-types.php';

        if( file_exists($path) ){
            require_once $path;
            $this->drive_list = $_CAR_DRIVE;
        }
        else {
            $this->drive_list = array();
        }
    }
    /**
     * Private method: load country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->country_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->country_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['cc'][] = $r['cc'];
            $obj['country_name'][] = stripslashes($r['country_name']);


            $this->country_list=$obj;
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load car maker list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCarMakerList(){
        require_once BASE_CLASS . 'class-connect.php';
        $product_type='Car';
        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `product_maker` WHERE product_type='$product_type' ORDER BY `id` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->car_maker_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->car_maker_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['maker'][] = stripslashes($r['maker']);

            $this->car_maker_list=$obj;
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load fuel type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadFuelTypeList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `fuel_list` ORDER BY `id` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load fuel type list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->fuel_type_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->fuel_type_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['fuel_name'][] = stripslashes($r['fuel_name']);

            $this->fuel_type_list=$obj;
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load transmission list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadTransmissionList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `transmission_list` ORDER BY `id` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load transmission list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->transmission_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->transmission_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['transmission_name'][] = stripslashes($r['transmission_name']);

            $this->transmission_list=$obj;
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
	/**
     * Private method: load car type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCarTypeList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `vehicle_types` ORDER BY `type_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car type list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->vehicle_type_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->vehicle_type_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $obj['id'][] = $r['id'];
            $obj['type_name'][] = stripslashes($r['type_name']);
            $this->vehicle_type_list=$obj;
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Private method: load maker unique values list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadMakerList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT `maker`,`model` FROM `car` ORDER BY `maker` ASC") ){
            require_once BASE_CLASS . 'class-log.php';

            $cnx->close();

            LogReport::write('Unable to load maker and model list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->form_message = $this->lang['PRODUCT_MAKER_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }

        $this->maker_list = array();
        $this->model_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->maker_list, stripslashes($r['maker']));
            array_push($this->model_list, stripslashes($r['model']));
        }

        @mysql_free_result($sql);
        $cnx->close();

        $this->maker_list = array_unique($this->maker_list);
        $this->maker_list = array_values($this->maker_list);
        $this->model_list = array_unique($this->model_list);
        $this->model_list = array_values($this->model_list);
    }

	/**
     * Private method: load category list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	private function loadCategoryList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT DISTINCT `category_name`, `product_type` FROM `category` ORDER BY `category_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load category list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->category_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->category_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            $this->category_list[$r['product_type']][] = $r['category_name'];
            //echo $r['category_name'];
        }

        @mysql_free_result($sql);
        $cnx->close();
    }
    /**
     * Public method: get car id
     * <br>---------------------------------------------------------------------
     * @return string The generated car id
     */
    public function getCarId(){
        return $this->car_id;
    }
	//get all checkbox data

    public function getAllCheckbox($chkboxName, $joiner='^'){
		$chkbox = $_POST[$chkboxName];
		for( $i = 0; $i < sizeof( $chkbox ); $i++ ){
			$chkbox[$i] = substr( $chkbox[$i], 0, 50 );
		}
		$string = implode("^", $chkbox);

		return $string;
	}
	//get car make name

    public function getMaker($maker_id){
		require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT `maker` FROM `product_maker` WHERE `id`='$maker_id';") ){
            require_once BASE_CLASS . 'class-log.php';

            $cnx->close();
            return $maker_id;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }

        while( $r = @mysql_fetch_assoc($sql) ){
           	return $r['maker'];
        }

        @mysql_free_result($sql);
        $cnx->close();

	}
    /**
     * Public method: get feature list
     * <br>---------------------------------------------------------------------
     * @return array
     */
   /* public function getFeatureIconList(){
        return $this->feature_icon;
    }*/
	/**
     * Public method: get option list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getProductOptionList(){
        return $this->product_option;
    }
	/**
     * Public method: get car maker list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCarMakerList(){
        return $this->car_maker_list;
    }
	/**
     * Public method: get category list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCategoryList(){
        return $this->category_list;
    }
    /**
     * Public method: get country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCountryList(){
        return $this->country_list;
    }
	/**
     * Public method: get car type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getVehicleTypeList(){
        return $this->vehicle_type_list;
    }
    /**
     * Public method: get body type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getBodyList(){
        return $this->body_list;
    }
    /**
     * Public method: get body type list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getDriveList(){
        return $this->drive_list;
    }
    /**
     * Public method: get maker unique values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getMakerList(){
        return $this->maker_list;
    }
    /**
     * Public method: get fuel unique values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getFuelTypeList(){
        return $this->fuel_type_list;
    }
    /**
     * Public method: get transmission values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getTransmissionList(){
        return $this->transmission_list;
    }
    /**
     * Public method: get model unique values list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getModelList(){
        return $this->model_list;
    }

    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
