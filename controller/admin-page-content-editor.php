<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminpagecontenteditor 
{
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    
    /*
     * Constructor -------------------------------------------------------------
     */
    public function adminpagecontenteditor($lang=array())
    {
        $this->lang = $lang;
        
        // handle update form.
        if( isset($_POST['updateBtn']) )
        {
            $this->updateForm();
        }
        
        // handle css update form.
        if( isset($_POST['updatecssbtn']) )
        {
            $this->handleCSSForm();
        }
        
        // handle js update form.
        if( isset($_POST['updatejsbtn']) )
        {
            $this->handleJSForm();
        }
    }
    
    /*
     * Private method: update css form -----------------------------------------
     * @return void.
     */
    private function handleCSSForm()
    {
        $content = trim($_POST['cssInput']);
        $content = stripslashes($content);
        $slug = trim($_POST['cssslug']);
        $slug .= '.css';
        $slug = BASE_ROOT . 'view/css/' . $slug;
        
        if( empty($content) ){ $content = '/* css stylesheet */'; }
        
        if( !file_exists($slug) )
        {
            return;
        }
        
        $write = true;
        
        if( $fo = @fopen($slug,'w') )
        {
            if( !@fwrite($fo,$content) )
            {
                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to open ['.$slug.'] for writting. Access denied at ' . __FILE__ . ':' . __LINE__);
                $write = false;
            }
            
            @fclose($fo);
        }
        else
        {
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to open ['.$slug.'] for writting. Access denied at ' . __FILE__ . ':' . __LINE__);
            $write = false;
        }
        
        if( !$write )
        {
            $this->form_message = $this->lang['CMS_EDIT_CSS_WRITE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        else
        {
            $this->form_message = $this->lang['CMS_EDIT_CSS_WRITE_SUCCESS'];
            $this->form_status = true;
            $this->form_style = 'alert-success';
            return;
        }
    }
    
    /*
     * Private method: update js form ------------------------------------------
     * @return void.
     */
    private function handleJSForm()
    {
        $content = trim($_POST['jsInput']);
        $content = stripslashes($content);
        $slug = trim($_POST['jsslug']);
        $slug .= '.js';
        $slug = BASE_ROOT . 'view/js/' . $slug;
        
        if( empty($content) ){ $content = '/* javascript sheet */'; }
        
        if( !file_exists($slug) )
        {
            return;
        }
        
        $write = true;
        
        if( $fo = @fopen($slug,'w') )
        {
            if( !@fwrite($fo,$content) )
            {
                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to open ['.$slug.'] for writting. Access denied at ' . __FILE__ . ':' . __LINE__);
                $write = false;
            }
            
            @fclose($fo);
        }
        else
        {
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to open ['.$slug.'] for writting. Access denied at ' . __FILE__ . ':' . __LINE__);
            $write = false;
        }
        
        if( !$write )
        {
            $this->form_message = $this->lang['CMS_EDIT_JS_WRITE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        else
        {
            $this->form_message = $this->lang['CMS_EDIT_JS_WRITE_SUCCESS'];
            $this->form_status = true;
            $this->form_style = 'alert-success';
            return;
        }
    }
    
    /*
     * Private method: handle update form --------------------------------------
     */
    private function updateForm()
    {
        $pid = trim($_POST['pid']);
        $content = trim($_POST['phtml']);
        $content = stripslashes($content);
        
        // connect to database.
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        // filter.
        $content = mysql_real_escape_string($content);
        
        if( !mysql_query("UPDATE `page` SET `html`='$content' WHERE `id`='$pid' LIMIT 1;") )
        {
            $cnx->close();
            
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to update page due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['CMS_EDIT_CONTENT_UPDATE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        $cnx->close();
        
        $this->form_message = $this->lang['CMS_EDIT_CONTENT_UPDATE_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /*
     * Public method: get css content ------------------------------------------
     * @return string.
     */
    public function getCSSContent($slug,$group)
    {
        $path = BASE_ROOT . 'view/css/' . $group . '-' . $slug . '.css';
        
        $result = '';
        
        if( file_exists($path) )
        {
            if( $fo = @fopen($path,'r') )
            {
                $result = @fread($fo, filesize($path));
                @fclose($fo);
            }
        }
        
        return $result;
    }
    
    /*
     * Public method: get css content ------------------------------------------
     * @return string.
     */
    public function getJSContent($slug,$group)
    {
        $path = BASE_ROOT . 'view/js/' . $group . '-' . $slug . '.js';
        
        $result = '';
        
        if( file_exists($path) )
        {
            if( $fo = @fopen($path,'r') )
            {
                $result = @fread($fo, filesize($path));
                @fclose($fo);
            }
        }
        
        return $result;
    }

    /*
     * Public method: get page content -----------------------------------------
     * @return array | bool.
     */
    public function getPageContent($page_id)
    {
        // validate.
        if( empty($page_id) || !is_numeric($page_id) )
        {
            $this->form_message = $this->lang['CMS_EDIT_INVALID_PAGE_REQUEST'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return;
        }
        
        // connect.
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $page_id = mysql_real_escape_string($page_id);
        
        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `id`='$page_id' LIMIT 1;") )
        {
            $cnx->close();
            
            require_once BASE_ROOT . 'core/class-log.php';
            
            LogReport::write('Unable to query page table due a query error at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            
            $this->form_message = $this->lang['CMS_EDIT_PAGE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            
            return false;
        }
        
        if( @mysql_num_rows($sql) != 1 )
        {
            $this->form_message = $this->lang['CMS_EDIT_NO_CONTENT_RESULT'];
            $this->form_style = 'alert-error';
            $this->form_status = true;
            
            $cnx->close();
            
            return array();
        }
        
        $r = @mysql_fetch_assoc($sql);
        
        $result = array();
        $result['meta_keywords'] = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots'] = $r['meta_robots'];
        $result['page_title'] = $r['page_title'];
        $result['page_slug'] = $r['page_slug'];
        $result['page_publish'] = $r['page_publish'];
        $result['page_author'] = $r['page_author'];
        $result['creation'] = $r['creation'];
        $result['editable'] = $r['editable'];
        $result['page_group'] = $r['page_group'];
        $result['html'] = $r['html'];
        
        @mysql_free_result($sql);
        
        $cnx->close();
        
        return $result;
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
?>
