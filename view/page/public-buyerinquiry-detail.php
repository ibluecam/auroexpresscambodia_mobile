<?php
if(!isset($_GET['id']) || $_GET['id'] === ''){
?>
    <script>
        window.location.href = "<?php echo BASE_RELATIVE . 'buyerinquiry'; ?>";
    </script>
<?php
    exit;
}
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$expert_company = $_CLASS->loadExpertCompany();
$listinquiry=$_CLASS->listinquiry();
$listuser=$_CLASS->listuser();
$message_templates = $_CLASS->loadMsgTemplate();
$receiver="";
?>

<?php
 
    if( !empty($fstatus) )
    {
        if( $fstatus == 'complete' )
        {
            FeedbackMessage::displayOnPage( 'Message has been sent successfully!' , 'success');
?>
        <p class="alert alert-success" style="text-align: center"><strong> REGISTER</strong></p>
<?php
        } else
        {
            FeedbackMessage::displayOnPage( 'Failed to sent this message.');
?>
        <p class="alert alert-error" style="text-align: center"><strong>  ERROR </strong></p>
<?php           
        }
    }
?>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/mywini/mywini.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/register_item.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/buyer_inquiry.css" />
<?php
//Test
$c_id = "";
$m_id = "";
$reader = "";
$inbox = "";

?>
 
<div id="sectionContenWrapper" style="margin-top:0px;">
   <!--sectionSidebar-->   
   <?php include("php/public-inquiry-detail-left-sidebar.php");?>

<div id="sectionContent" style="margin-top:35px;">
   <div id="payment_list" style="height:52px;" >
       <p>Buyer's Inquiry Detail</p>
   </div>

   <div id="message-detail">
      
   <table width="705" cellspacing="0" cellpadding="0">
    <tr>
        <th colspan="3"><p>Item Infomation</p></th>
    </tr>
    <?php 
        foreach ($listinquiry as $lst) {
                         
     ?>
    <tr>
        <td class="borR0 tdl"><p>Vehicle Type</p></td>
        <td class="borL0 tdr"><p><?php echo $lst['vehicle_type'];?></p></td>
        <td class="borR0 borB0 tdpic" rowspan="9">
           <p class="pic"><img src="<?php echo BASE_RELATIVE.$lst['fileurl'];?>" /></p>
           <p>Item No : <span><?php echo $lst['id'];?></span></p>
       </td>
   </tr>
   <tr>
    <td class="borR0 tdl"><p>Condition</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['condition'];?></p></td>
</tr> 
<tr>
    <td class="borR0 tdl"><p>Make</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['make'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Model</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['model'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Year</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['year_from'].' - '.$lst['year_to'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Price($)</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['price_from'].' - '.$lst['price_to'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Steering</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['steering'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Transmission</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['transmission'];?></p></td>
</tr>
</table>

<table width="705" cellspacing="0" cellpadding="0">
    <tr>
        <th colspan="2"><p>Buyer's Message</p></th>
    </tr>
    <tr>
        <td class="borR0 tdl"><p>Country</p></td>
        <td class="borL0 tdr490"><p><?php echo $lst['country'];?></p></td>
    </tr>
    <tr>
        <td class="borR0 tdl"><p>Port</p></td>
        <td class="borL0 tdr490"><p><?php echo $lst['port'];?></p></td>
    </tr>
    <tr>
        <td class="borR0 tdl"><p>Payment terms</p></td>
        <td class="borL0 tdr490"><p><?php echo $lst['payment_terms'];?></p></td>
    </tr>
    <tr>
        <td class="borR0 tdl"><p>Price terms</p></td>
        <td class="borL0 tdr490"><p><p><?php echo $lst['price_term'];?></p></td>
    </tr>
    <tr>
        <td class="borR0 tdl vTop"><p style="padding-top:10px;">Requirements</p></td>
        <td class="borL0 tdr490">
           <p class="message">
            <?php echo $lst['message'];?>
           </p>
      </td>
  </tr>
</table>

<table width="705" cellspacing="0" cellpadding="0">
    <tr>
        <th colspan="3"><p>Buyer’s Information</p></th>
    </tr>
    <tr>
        <td class="borR0 tdl"><p>Company Name</p></td>
        <td class="borL0 tdr"><p><?php echo $lst['company_name'];?></p></td>
        <td class="borR0 borB0 tdpic" rowspan="9">
           <p class="pic"><img src="<?php if($lst['image']!=""){
                             echo BASE_RELATIVE.'upload/thumb/'.$lst['image'];
                            }
                             else{
                                echo BASE_RELATIVE.'images/register/profile-pic.jpg';
                             };?>" /></p>
       </td>
   </tr>
   <tr>
    <td class="borR0 tdl"><p>Name</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['name'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>ID</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['card_id'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Tel</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['tel'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Fax</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['fax'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Country</p></td>
    <td class="borL0 tdr"><p><?php echo $lst['country'];?></p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Address</p></td>
    <td class="borL0 tdr">
       <p>
        <?php echo $lst['address'];?>
    </p>
</td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Mini Homepage</p></td>
    <td class="borL0 tdr"><p> </p></td>
</tr>
<tr>
    <td class="borR0 tdl"><p>Buyer access IP</p></td>
    <td class="borL0 tdr"><p>Manual</p></td>
</tr>
<?php
    }
?>
</table>

<?php
if( $_SESSION['log_group']!= 'public') 
{

?>
<div class="" id="popForm2"><form method="post" enctype="multipart/form-data" >
    <div class="form_mr">

        <!-- top Layout -->
        <div class="form_tLay">
            <div class="tl"><img src="<?php echo BASE_RELATIVE;?>images/mywini/poplay_tl.gif"></div>
            <div class="tr"><p><a href="javascript:Display_Propose('0');"><img src="<?php echo BASE_RELATIVE;?>images/mywini/btn_pop_close.gif"></a></p></div>
        </div>
        <!-- top Layout end -->
        <div class="popBody">
            <dl class="poptitle">
                <dt><strong>Reply</strong></dt>
                <dd>www.iblue.com</dd>
            </dl>
            <div id="messPop">
                <ul style="width:100%;">


                    <input type="hidden" value="" name="i_sUserCd">
                    <ul style="overflow:hidden;margin:0 5px 0px 5px;">
                        <li style="float:left;padding-right:7px;font-size:12px;">Text   </li>   <li style="float:left;padding-right:7px;"><a style="font-size:10px;" onclick="addMessageTemplate()" href="javascript:this onclick">Add [+]</a></li>
                        <li style="float:left;padding-right:7px;"><a style="font-size:10px;" onclick="editMessageTemplate()" href="javascript:this onclick">Edit [-]</a></li>
                        <li style="float:left;">    <a style="font-size:10px;" onclick="deleteMessageTemplate()" href="javascript:fnMessageDel();">Del [x]</a></li>
                        <li style="float:left;margin-left:10px">
                            <select style="width: 100px; visibility: visible;" onchange="selectMsgTemplate();" id="select_message_template" name="select_message_template">
                                <option value="">Select</option>
                                <?php 
                                foreach($message_templates as $message_template){                                  
                                    echo '<option value="'. $message_template['id'] .'">'. $message_template['title'] .'</option>';
                                }
                                ?>
                            </select>
                        </li>
                        <li style="float:right;"><a style="font-size:12px;" href="javascript:fnMsgSample();"><div id="msgopenbtn">[Multilingual sample answers▼]</div></a></li>
                    </ul>


                </ul>
                <!-- 다국어 샘플 메시지 s -->
                <!-- <ul style="width:650px;">
                <li style="float:right;">
                <a href="javascript:fnMsgSample();" style="font-size:10px;">[다국어샘플답변]</a></li>
            </ul> -->
            <table width="100%" border="0" cellspacing="0" cellpadding="2" style="display: none;" id="msgSample">
                <tbody><tr>
                    <td height="4">
                        <div class="tabAll">
                            <ul style="width:663px;" class="renTab">
                                <li class="type on"><span><a href="javascript:fnChangeTab('C010');">Price inquiries</a></span></li>
                                <li class="type"><span><a href="javascript:fnChangeTab('C020');">Status inquiries</a></span></li>
                                <li class="type"><span><a href="javascript:fnChangeTab('C030');">Inventory inquiries</a></span></li>
                                <li class="type"><span><a href="javascript:fnChangeTab('C040');">Sold</a></span></li>
                                <li class="type"><span><a href="javascript:fnChangeTab('C050');">Greetings</a></span></li>
                                <select style="width: 80px; float: right; visibility: visible;" class="selectForm" onchange="fnMsgLangChange();" name="select_msg_sample_language">
                                    <option value="en">English</option>
                                    <option value="es">Espanol</option>
                                    <option value="ru">русский</option>
                                    <!-- <option value="ar">العربية</option> -->
                                </select>
                            </ul>
                        </div>
                        <div style="overflow-y:scroll;width:660px;height:200px;border:1px solid #e0e0e0;" id="content" class="">
                            <table>
                                    <tbody>
                                            <tr>
                                                    <td style="width:590px;">
                                                            <a onmouseout="this.style.textDecoration='none'" onmouseover="this.style.textDecoration='underline'" href="javascript:msg_submit(5)">I think I can give you a little discount and make the final price (    ) USD. If you are interested do not hesitate to contact me. </a>
                                                    </td>
                                                    <td style="width:20px;">
                                                            <img cursor:pointer;'="" float:right;="" onclick="msg_submit(5)" title="Add" alt="Add" src="<?php echo BASE_RELATIVE;?>images/mywini/btn_attach_add.gif" id="sample_button5">
                                                    </td>
                                            </tr>
                                    </tbody>
                            </table>
                            <input type="hidden" value="I think I can give you a little discount and make the final price (    ) USD. If you are interested do not hesitate to contact me.
                        " id="sample_text5">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width:590px;">
                                        <a onmouseout="this.style.textDecoration='none'" onmouseover="this.style.textDecoration='underline'" href="javascript:msg_submit(4)">The price given on this web site is a final price. No discounts available, sorry.</a>
                                    </td>
                                    <td style="width:20px;">
                                        <img cursor:pointer;'="" float:right;="" onclick="msg_submit(4)" title="Add" alt="Add" src="<?php echo BASE_RELATIVE;?>images/mywini/btn_attach_add.gif" id="sample_button4">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="The price given on this web site is a final price. No discounts available, sorry. " id="sample_text4">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width:590px;">
                                        <a onmouseout="this.style.textDecoration='none'" onmouseover="this.style.textDecoration='underline'" href="javascript:msg_submit(3)">I am sorry but your price is too low. My best price is (     ) USD. If you are interested do not hesitate to contact me. </a>
                                    </td>
                                    <td style="width:20px;">
                                        <img cursor:pointer;'="" float:right;="" onclick="msg_submit(3)" title="Add" alt="Add" src="<?php echo BASE_RELATIVE;?>images/mywini/btn_attach_add.gif" id="sample_button3">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="I am sorry but your price is too low. My best price is (     ) USD. If you are interested do not hesitate to contact me. " id="sample_text3">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width:590px;">
                                        <a onmouseout="this.style.textDecoration='none'" onmouseover="this.style.textDecoration='underline'" href="javascript:msg_submit(2)">Thank you for the inquiry. The price of this item is USD (    ) includes shipping. And the condition of this item is (    ). Please give me a response if you want more detail information on this item. I’m looking forward to hearing from you soon.</a>
                                    </td>
                                    <td style="width:20px;">
                                        <img cursor:pointer;'="" float:right;="" onclick="msg_submit(2)" title="Add" alt="Add" src="http://image.autowini.com/IMG/EN/MW/btn_attach_add.gif" id="sample_button2">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="Thank you for the inquiry. The price of this item is USD (    ) includes shipping. And the condition of this item is (    ). Please give me a response if you want more detail information on this item. I’m looking forward to hearing from you soon. " id="sample_text2">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width:590px;">
                                        <a onmouseout="this.style.textDecoration='none'" onmouseover="this.style.textDecoration='underline'" href="javascript:msg_submit(1)">The CFR price for this stock is (    ) USD. If you are interested do not hesitate to contact me. </a>
                                    </td>
                                    <td style="width:20px;">
                                        <img cursor:pointer;'="" float:right;="" onclick="msg_submit(1)" title="Add" alt="Add" src="http://image.autowini.com/IMG/EN/MW/btn_attach_add.gif" id="sample_button1">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="The CFR price for this stock is (    ) USD. If you are interested do not hesitate to contact me. " id="sample_text1">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width:590px;">
                                        <a onmouseout="this.style.textDecoration='none'" onmouseover="this.style.textDecoration='underline'" href="javascript:msg_submit(0)">The FOB price for this stock is (    ) USD. If you are interested do not hesitate to contact me.</a>
                                    </td>
                                    <td style="width:20px;">
                                        <img cursor:pointer;'="" float:right;="" onclick="msg_submit(0)" title="Add" alt="Add" src="http://image.autowini.com/IMG/EN/MW/btn_attach_add.gif" id="sample_button0">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="The FOB price for this stock is (    ) USD. If you are interested do not hesitate to contact me. " id="sample_text0">
                    </div>
    <ul style="width:650px;">
        <li style="float:right;">
            <img style="cursor:pointer; margin-top:7px;" onclick="javascript:fnMsgClear();" class="midd" title="Delete" alt="Delete" src="http://image.autowini.com/IMG/EN/MW/btn_delete.gif">
            <!-- <a href="javascript:fnMsgClear();" style="font-size:10px;">[Clear]</a></li> -->
        </li></ul>
    </td>
</tr>
</tbody></table>

<!-- 다국어 샘플 메시지 e -->
<div class="textarea">
    <textarea onkeyup="fnByteCheck();" rows="" class="inputLine" cols="" style="width:100%; height: 200px;" id="i_sMessage" name="i_sMessage">
 

---------- [ Original Message ] ----------
<?php
    if(sizeof($listinquiry)!=0){
        foreach ($listinquiry as $lst) {
   
?>
Date : <?php echo date('M d Y', strtotime($lst['dates'])); ?>

From : <?php echo $lst['email']; ?> 

<?php echo str_replace("<br />","", $lst['message']); ?></textarea>
<?php 
    $receiver=$lst['email'];
    }
}
?>
</div>
<div style="width:100%;">
    <p class="byte"><span id="span_txt">0 Byte</span> / 2,600 Byte <br></p>
    <br>

    <input type="hidden" value="" name="i_sRecordId">
    <div id="div_main">

        <div name="div_attach" id="div_attach">
            <table style="width:400px; margin: 0;">
                <tbody><tr>
                    <td style="border:0px;padding:2px 2px 2px 2px;">
                        <input type="text" readonly="" style="width: 535px" class="inputLine" name="TX_TMP_FILENAME">
                        <div style="width:460px;" name="div_fileName" id="div_fileName"></div>
                    </td>
                    <td style="border:0px;padding:2px 2px 2px 2px;">
                        <div style="width: 63px; height: 20px; overflow: hidden; background: url(&quot;<?php echo BASE_RELATIVE;?>images/mywini/btn_attach_file.gif&quot;) no-repeat scroll 0% 0% transparent;">
                            <?php $upload_path = mysql_real_escape_string(dirname(dirname(dirname(__FILE__))). '/upload/message-attachment/' .  $lst['email'] . '/'); ?>
                            <input type="file" style="cursor: pointer; opacity: 0; margin-top: -3px; height: 30px; margin-left: -134px;" onchange="chooseAttachFile(this, document.getElementById('max_allowed_file_size').getAttribute('data-field'), '<?php echo $upload_path; ?>')" id="att_file_upload" name="att_file_upload[]" accept="image/*, application/pdf, application/zip, application/rar, application/msword" multiple>                             
                        </div>

                    </td>
                    <td style="border:0px;padding:2px 2px 2px 2px;">
                        <img align="absb" style="cursor:pointer;display:none;" onclick="fnFileDel(this)" title="delete" alt="delete" name="TMP_THUB_DEL" src="<?php echo BASE_RELATIVE;?>images/mywini/btn_attach_del.gif">
                    </td>
                    <td style="border:0px;padding:2px 2px 2px 2px;">

                        <img border="0" align="absmiddle" onclick="fnAddAttach();" style="cursor:pointer;display:none;" src="<?php echo BASE_RELATIVE;?>images/mywini/btn_attach_add.gif">

                    </td>
                </tr>
            </tbody></table>

            <input type="hidden" value="" name="i_arrAttachStatus">
            <input type="hidden" value="" name="i_arrAttachId">
            <input type="hidden" value="" name="i_arrAttachNm">
            <input type="hidden" value="" name="i_arrAttachExt">
            <input type="hidden" value="" name="i_arrAttachPath">
            <input type="hidden" value="" name="i_arrAttachSize">
        </div>


    </div>

    <p style="font-size: x-small;">* Available File Formats :  <span id="allowed_file_format">XLS, DOC, DOCX, PPT, PPS, JPG, GIF, BMP, ZIP, PDF, PNG, HWP, TXT</span><br>* Max file upload size : <span id="max_allowed_file_size" data-field="">10MB</span></p>
     <script>
    (function(o){
        var unit = o.text().substr(-2),
            _byte =  parseInt(o.text().substr(0, o.text().indexOf(unit)));

        unit = unit.toLowerCase();

        switch (unit) {
            case 'gb':
                o.attr('data-field', _byte * Math.pow(1024, 3));
                break;
            case 'mb':
                o.attr('data-field', _byte * Math.pow(1024, 2));
                break;
            case 'kb':
                o.attr('data-field', _byte * 1024);
                break;
            default:
                o.attr('data-field', _byte);
        }
    }($('#max_allowed_file_size')));

    (function(o){
        var allowed = $('#allowed_file_format').text().toLowerCase().split(',');        
        allowed.forEach(function(item, index){
            allowed[index] = '.' + item.trim();
        });
        o.attr('accept', allowed.toString());
    }($('#att_file_upload')));
    </script>
    <div id="att_file_list"></div>
    <style type="text/css">
    #att_file_list{
        display: none;
        padding: 10px 5px;
        background-color: #fcfcfc;
    }
    #att_file_list .attachment_file{ 
        padding-bottom: 5px;       
        font-weight: bold;
    }

    #att_file_list .btn_att_file_remove{
        font-size: 0.8em;
        margin-left: 10px;
    }
    </style>
    <p class="tt2">
        Profile Signature
        <input type="radio" checked="" value="Y" name="i_sFlagSignature">&nbsp;Show
        <input type="radio" value="N" name="i_sFlagSignature">&nbsp;Hide
    </p>
    <p class="tt5">
        <span>You can edit your profile signature at 'My iblue &gt; Update my Information'</span>
    </p>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <div style="width:500px;background-color:#ffffff;">
        <?php 
            if(sizeof($listuser)!=0)
            {
                foreach($listuser as $user)
                {   
        ?>
        <table width="502" cellspacing="0" cellpadding="0" style="border-collapse:collapse; border-spacing:0; empty-cells:show; border-top:5px solid #CCC; border-bottom:5px solid #CCC;border-right:1px solid #CCC;border-left:1px solid #CCC;font-family:Verdana, Geneva, sans-serif; font-size:12px;">
            <tbody>
                <tr height="30">
                    <td style="text-align:left;" colspan="2">
                        <!--img width="500" height="21" alt="logo" src="<?php echo BASE_RELATIVE;?>images/mywini/autowinilogo_email.gif"-->
                    </td>
                </tr>
                <tr>
                    <td width="110" valign="top" align="left" style="text-align:left;padding-bottom:5px;border-collapse:collapse; border-spacing:0; empty-cells:show;">
                        <img width="100" height="75" style="padding-top:0px;border:1px #999 solid" alt="companylogo" src="<?php if($user['image']!=""){
                             echo BASE_RELATIVE.'upload/thumb/'.$user['image'];
                            }
                             else{
                                echo BASE_RELATIVE.'images/register/profile-pic.jpg';
                             };?>">
                    </td>
                    <td width="390" valign="top" align="left" style="text-align:left;">
                        <table width="100%" style="padding-bottom:5px">
                            <tbody>
                                <tr>
                                    <td valign="top" align="left" style="text-align:left;line-height:1.5em">
                                        <span style="font-weight:bold;"><?php echo $user['name'];?></span>
                                        <br>ID :  <?php echo $user['card_id']?>
                                        <!--img width="102" height="15" style="vertical-align:middle" alt="powermember" src="http://autowini.com/IMG/EN/expert_badge.png"> 
                                        <img width="52" height="15" style="vertical-align:middle" alt="powermember" src="http://image.autowini.com/IMG/COMMON/4years.png"-->
                                        <br>Company name : <?php echo $user['company_name'];?>
                                        <br>Country : 
                                        <img width="20" height="12" style="vertical-align:middle" alt="<?php echo $user['country_name'];?>" src="<?php echo BASE_RELATIVE.'images/flag/'.$user['cc'];?>.png"> <?php echo $user['country_name'];?>
                                        <br>TEL : <?php echo $user['tel'];?><br>Homepage : 
                                        <a href="http://<?php echo $user['web'];?>">http://<?php echo $user['web'];?></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
                }
            }
        ?>
    </div>
</div>
</div>
<div class="clear:both;"></div>
<div class="btnCenter">
    <span class="button4">
        <input type="submit" id="submit" name="submit" value=""/>
        <input type="hidden" name="sender" value="<?php echo $_SESSION['log_email']; ?>" />
        <input type="hidden" name="receiver" value="<?php echo $receiver; ?>" />
        <input type="hidden" name="product" value="<?php echo $c_id; ?>" />
    </span>
</div>
</div>
<!-- bottom Layout -->
<div class="form_bLay">
    <div class="bl"><img src="<?php echo BASE_RELATIVE;?>images/mywini/poplay_bl.gif"></div>
    <div class="br"><img src="http://image.autowini.com/IMG/EN/sp.gif"></div>
</div>
<!-- bottom Layout end -->
</div>
</form>
</div>
<?php
}
?>

<script src="<?php echo BASE_RELATIVE;?>js/msg_template_reply.js"></script>
</div>
</div><!-- end div id="sectionContent"-->

</div><!-- end div id="sectionContentWraper" -->

 
