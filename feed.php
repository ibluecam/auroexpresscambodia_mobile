<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
// README
// By calling feed.php, it will generate a list of the last 10 newest vehicles.
// -----------------------------------------------------------------------------
// RSS FEED BUILDER
// CONFIGURATION
// - Change the number of cars that are add to the RSS
$limit = 10;
// END CONFIGURATION

$path = 'config.php';

if( !file_exists($path) ){
    return;
}

require_once 'config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

// do the query.
$query = "SELECT * FROM `car` WHERE `status`='0' ORDER BY RAND()  LIMIT $limit;";

if( !$sql = @mysql_query($query) ){
    require_once BASE_CLASS . 'class-log.php';
    
    $cnx->close();
    LogReport::write('Unable to load car list for RSS creation due a query error at ' . __FILE__ . ':' . __LINE__);
    
    die('Error.');
}

if( @mysql_num_rows($sql) < 1 ){
    $cnx->close();
    die('<?xml version="1.0" encoding="utf-8"?><channel></channel>');
}

$list = array();

while( $r = @mysql_fetch_assoc($sql) ){
    $obj = array(
        'id' => stripslashes($r['id']),
        'maker' => stripslashes($r['maker']),
        'model' => stripslashes($r['model']),
        'price' => stripslashes($r['price']),
        'year' => stripslashes($r['year']),
        'miles' => stripslashes($r['miles'])
    );
    
    array_push($list, $obj);
}

unset($obj);
unset($sql);

@mysql_free_result($sql);

// get vehicle picture.
for( $i=0; $i < count($list); $i++ ){
    $query = "SELECT `car_id`,`source` FROM `car_media` WHERE `car_id`='".$list[$i]['id']."' LIMIT 1;";
    
    if( !$sql = @mysql_query($query) ){
        break;
    }
    else {
        if( @mysql_num_rows($sql) > 0 ){
            $r = @mysql_fetch_assoc($sql);
            @mysql_free_result($sql);
            
            $list[$i]['thumb'] = stripslashes($r['source']);
        }
    }
}

// get currency symbol and website name.
if( !$sql = @mysql_query("SELECT `currency_symbol`,`currency_code` FROM `setting` LIMIT 1;") ){
    $cnx->close();
    require_once BASE_CLASS . 'class-log.php';
    
    LogReport::write('Unable to load site information for RSS due a query error at ' . __FILE__ . ':' . __LINE__);
    
    die('<?xml version="1.0" encoding="utf-8"?><channel></channel>');
}

if( @mysql_num_rows($sql) != 1 ){
    $cnx->close();
    die('<?xml version="1.0" encoding="utf-8"?><channel></channel>');
}

$r = @mysql_fetch_assoc($sql);
@mysql_free_result($sql);

$currency_symbol = stripslashes($r['currency_symbol']);
$currency_code   = stripslashes($r['currency_code']);

unset($sql);
unset($r);

// get company details.
if( !$sql = @mysql_query("SELECT * FROM `company` LIMIT 1;") ){
    $cnx->close();
    require_once BASE_CLASS . 'class-log.php';
    
    LogReport::write('Unable to load site information for RSS due a query error at ' . __FILE__ . ':' . __LINE__);
    
    die('<?xml version="1.0" encoding="utf-8"?><channel></channel>');
}

if( @mysql_num_rows($sql) != 1 ){
    $cnx->close();
    die('<?xml version="1.0" encoding="utf-8"?><channel></channel>');
}

$r = @mysql_fetch_assoc($sql);
@mysql_free_result($sql);

$company_name = stripslashes($r['company_name']);
$company_pic   = stripslashes($r['company_photo']);

unset($sql);
unset($r);

$cnx->close();

# generate feed.
$xml = '<rss version="2.0">' . "\r\n";

$xml .= '<channel>' . "\r\n";
$xml .= '   <title>'.$company_name.'</title>' . "\r\n";
$xml .= '   <link>'.BASE_RELATIVE.'</link>' . "\r\n";
$xml .= '   <image>' . "\r\n";
$xml .= '       <url>'.BASE_RELATIVE . $company_pic.'</url>' . "\r\n";
$xml .= '       <title>'.$company_name.'</title>' . "\r\n";
$xml .= '       <link>'.BASE_RELATIVE.'</link>' . "\r\n";
$xml .= '   </image>' . "\r\n";
$xml .= '   <description>RSS List</description>' . "\r\n";

# build slug
require_once BASE_CLASS . 'class-utilities.php';

for( $i=0; $i < count($list); $i++ ){
    $slug = Utilities::genetateSlug($list[$i]['maker'].' '.$list[$i]['model'].' '.$list[$i]['year']);
    
    $xml .= '   <item>' . "\r\n";
    $xml .= '       <title>'.$list[$i]['maker'].' '.$list[$i]['model'].' '.$list[$i]['year'].'</title>' . "\r\n";
    $xml .= '       <link>'.BASE_RELATIVE . $slug . '/car-detail/?cid=' . $list[$i]['id'].'</link>' . "\r\n";
    $xml .= '       <description>'. $currency_symbol . ' ' . $list[$i]['price'].' ' . $currency_code . ' </description>' . "\r\n";
    $xml .= '   </item>' . "\r\n";
}
$xml .= '</channel>' . "\r\n";
$xml .= '</rss>' . "\r\n";

die($xml);