function filterModelList() {
    var e = document.getElementById("lightbox");
    e.style.display = "block";
    var t = document.getElementById("maker");
    var n = document.getElementById("body");
    t = t.options[t.selectedIndex].value;
    $.ajax({
        url: "ajax/car_model_list.php",
        data: {
            maker_filter: t
        },
        type: "post",
        success: function (r) {
            var i = r.split(",");
            if (i.length < 1) {
                document.getElementById("loading").style.display = "none";
                document.getElementById("erroricon").style.display = "inline-block";
                return
            }
            var s = document.getElementById("model");
            var o;
            var u = s.getElementsByTagName("option");
            for (var a = u.length - 1; a >= 1; a--) {
                s.removeChild(u[a])
            }
            var f = n.getElementsByTagName("option");
            for (a = f.length - 1; a >= 1; a--) {
                n.removeChild(f[a])
            }
            n.options.selectedIndex = 0;
            n.disabled = true;
            if (t == "all") {
                e.style.display = "none";
                s.disabled = true;
                return
            }
            for (var a = 0; a < i.length; a++) {
                o = document.createElement("option");
                o.text = i[a];
                o.value = i[a];
                s.add(o, s.options[null])
            }
            car_maker = t;
            e.style.display = "none";
            s.disabled = false
        },
        error: function (e) {
            document.getElementById("loading").style.display = "none";
            document.getElementById("erroricon").style.display = "inline-block";
            console.log("Unable to load model list due an internal error.")
        }
    })
}
function filterCarTypeList() {
    if (car_maker == undefined || car_maker.length < 2) {
        return
    }
    var e = document.getElementById("lightbox");
    e.style.display = "block";
    var t = document.getElementById("model");
    t = t.options[t.selectedIndex].value;
    $.ajax({
        url: "ajax/car_type_list.php",
        data: {
            maker_filter: car_maker,
            model_filter: t
        },
        type: "post",
        success: function (n) {
            var r = n.split(",");
            if (r.length < 1) {
                document.getElementById("loading").style.display = "none";
                document.getElementById("erroricon").style.display = "inline-block";
                return
            }
            var i = document.getElementById("body");
            var s;
            var o = i.getElementsByTagName("option");
            for (var u = o.length - 1; u >= 1; u--) {
                i.removeChild(o[u])
            }
            if (t == "all") {
                e.style.display = "none";
                i.disabled = true;
                return
            }
            for (var u = 0; u < r.length; u++) {
                s = document.createElement("option");
                s.text = r[u];
                s.value = r[u];
                i.add(s, i.options[null])
            }
            car_model = t;
            e.style.display = "none";
            i.disabled = false
        },
        error: function (e) {
            document.getElementById("loading").style.display = "none";
            document.getElementById("erroricon").style.display = "inline-block";
            console.log("Unable to load type list due an internal error.")
        }
    })
}
function validateEmail() {
    var e;
    var t = document.getElementById("subsc");
    var n = t.value;
    var r = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (n.length < 1) {
        e = false
    }
    if (!r.test(n)) {
        e = false
    }
    if (!e) {
        t.style.border = "1px solid #C00";
        document.getElementById("subscbtn").className = "btn btn-danger inputBtn"
    }
    return e
}
function loadFeaturedVehicles(e, t, n) {
    var r = document.getElementById("latest-wrapper");
    var i = document.getElementById("list-wrapper");
    $.post("ajax/load_home_latest_list.php", {
        current_code: e,
        currency_symbol: t,
        base: n
    }, function (r, s) {
        var o = r.split("?");
        if (h == undefined || i == undefined) {
            h = document.getElementById("latest-wrapper");
            i = document.getElementById("list-wrapper")
        }
        if (o[0] != "ok") {
            var u = '<p class="alert alert-error">Unable to load vehicle list. Please reload the page.</p>';
            h.innerHTML += u;
            return
        } else {
            var a = o[1];
            var f;
            var l;
            var c;
            a = a.split("[]");
            if (a.length < 1 || a[0].length < 1) {
                var h = document.getElementById("inner-content");
                var p = document.getElementById("latest-wrapper");
                if (h != undefined && p != undefined) {
                    h.removeChild(p);
                    console.log("removed");
                    return
                }
            }
            for (var d = 0; d < a.length; d++) {
                f = a[d].split(";");
                f[6] == 1 ? c = "image/featured.png" : c = "image/transparent_pixel.png";
                if (f[0] != undefined && f[1] != undefined && f[2] != undefined && f[3] != undefined) {
                    l = '<li class="span3" style="position:relative;margin-left:30px;">';
                    l += '<img src="' + c + '" alt="Featured" class="carFeatImg"/>';
                    l += '<a href="' + f[1].replace(" ", "") + "-" + f[2].replace(" ", "") + "-" + f[4].replace(" ", "") + "/car-detail/?cid=" + f[0] + '" class="thumbnail">';
                    l += '<img src="' + f[5] + '" alt="' + f[1] + " " + f[2] + '"/>';
                    l += "</a>";
                    l += '<span style="font-weight:bold;font-size:12px;text-align:center;display:block;margin-bottom:5px;">' + f[1] + " " + f[2] + " " + f[4] + "</span>";
                    l += "<div>";
                    l += '<span style="font-weight:bold;font-size:14px;text-align:center;display:block;color:green;float:left;padding-top:5px;"><?php echo CURRENCY_SYMBOL;?>' + f[3] + "</span>";
                    l += '<a href="' + f[1].replace(" ", "") + "-" + f[2].replace(" ", "") + "-" + f[4].replace(" ", "") + "/car-detail/?cid=" + f[0] + '" class="btn btn-small" style="float:right;"><i class="icon-road" style="margin-right:5px;"></i>More details</a>';
                    l += "</div>";
                    l += '<div class="clearfix"></div>';
                    l += "</li>";
                    i.innerHTML += l;
                    var v = document.getElementById("loadListLoader");
                    if (v != undefined && h != undefined) {
                        h.removeChild(v)
                    }
                }
            }
            setTimeout(function () {
                if (i == undefined) {
                    return
                }
                $(i).fadeOut(500, function () {
                    i.innerHTML = "";
                    loadFeaturedVehicles(e, t, n);
                    $(i).fadeIn(500)
                })
            }, rotation_speed)
        }
    })
}
$(document).ready(function (e) {
   /*  e("#slide").cycle({
        fx: "fade",
        timeout: 8e3,
        speed: "slow",
        easingIn: 20,
        easingOut: 40,
        pager: "#nav",
        after: function (e, t, n) {
            slideIndex = n.currSlide;
            nextIndex = slideIndex + 1;
            prevIndex = slideIndex - 1;
            if (slideIndex == n.slideCount - 1) {
                nextIndex = 0
            }
            if (slideIndex == 0) {
                prevIndex = n.slideCount - 1
            }
        },
        pagerAnchorBuilder: function (e, t) {
            return '<a href="#"> </a>'
        }
    });
    e("div#next a").click(function () {
        e("#slide").cycle(nextIndex, "scrollRight")
    });
    e("div#prev a").click(function () {
        e("#slide").cycle(prevIndex, "scrollLeft")
    }) */
});
var car_maker;
var car_model;
var car_type;
var rotation_speed = 1e4;
$(document).ready(function () {
    $("#tabs a:first").tab("show");
    $("#tab_all ul").load("ajax/load_inquiry.php");
    $("#tabs a").click(function (e) {
        e.preventDefault();
        $(this).tab("show")
    });
    $("#tabAll, #tabAll a").click(function (e) {
        $("#tab_all ul").load("ajax/load_inquiry.php")
    });
    $("#tabCar, #tabCar a").click(function (e) {
        $("#tab_car ul").load("ajax/load_inquiry.php?vehicle_type=Car")
    });
    $("#tabTruck, #tabTruck a").click(function (e) {
        $("#tab_truck ul").load("ajax/load_inquiry.php?vehicle_type=Truck")
    });
    $("#tabBus, #tabBus a").click(function (e) {
        $("#tab_bus ul").load("ajax/load_inquiry.php?vehicle_type=Bus")
    });
    $("#tabEquipment, #tabEquipment a").click(function (e) {
        $("#tab_equipment ul").load("ajax/load_inquiry.php?vehicle_type=Equipment")
    });
    $("#tabPart, #tabPart a").click(function (e) {
        $("#tab_part ul").load("ajax/load_inquiry.php?vehicle_type=Part")
    });
    $("#tabMotorbike, #tabMotorbike a").click(function (e) {
        $("#tab_motorbike ul").load("ajax/load_inquiry.php?vehicle_type=Motorbike")
    });
    $("#tabAircraft, #tabAircraft a").click(function (e) {
        $("#tab_aircraft ul").load("ajax/load_inquiry.php?vehicle_type=Aircraft")
    });
    $("#tabShip, #tabShip a").click(function (e) {
        $("#tab_ship ul").load("ajax/load_inquiry.php?vehicle_type=Ship")
    })
});