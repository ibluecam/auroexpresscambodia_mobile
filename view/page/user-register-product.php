<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}

/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE. test For CK
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '_ebody';

/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */

?>
        <div id="content-wrapper">
        <?php if ($_SESSION['mobile']==false) {?>
            <div id="my_item">
           
                <ul class="clearfix myitem" >
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-info"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_infonew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>messages"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/message_boxnew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-inventory"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_inventorynew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>register-product"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/register_productover.png" class="linkfade" /></a></li>
                </ul>
                <p>Categories</p><br/>
                <h1 id="sms">First step Choose the category from the following.</h1>
                 
    <div id="amazingcarousel-1" style="display:block;position:relative;margin-top:20px; margin-bottom:30px; width:87.8%">
        <div class="amazingcarousel-list-container" >
                  <ul class="clearfix item amazingcarousel-list">
                    <li title="car" class="amazingcarousel-item">
                       <a id="cars" href="<?php echo BASE_RELATIVE; ?>edit-car" >
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/car_over.png" />
                             <img src="<?php echo BASE_RELATIVE; ?>images/mywini/car.png" class="linkfade car" />
                        </a> 
                    </li>
                   <li title="car" class="amazingcarousel-item">
                       <a href="<?php echo BASE_RELATIVE; ?>edit-suv" >
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/suv_hover.png" />
                             <img src="<?php echo BASE_RELATIVE; ?>images/mywini/suv.png" class="linkfade suv" />
                        </a>
                    </li>
                    <li title="van" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-van" >
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/van_hover.png" />
                             <img src="<?php echo BASE_RELATIVE; ?>images/mywini/van.png" class="linkfade van" />
                        </a>
                    </li>
                    <li title="pick up" class="amazingcarousel-item">
                       <a href="<?php echo BASE_RELATIVE; ?>edit-pickup" >
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/pickup_hover.png" />
                             <img src="<?php echo BASE_RELATIVE; ?>images/mywini/pickup.png" class="linkfade pickup" />
                        </a>
                    </li>
                    <li title="truck" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-truck">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/truck_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/truck.png" class="linkfade truck " />

                        </a>  
                    </li>
                    <li title="bus" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-bus">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/bus_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/bus.png" class="linkfade bus" />

                        </a>
                    </li>
                    <li title="part" class="amazingcarousel-item">  
                        <a href="<?php echo BASE_RELATIVE; ?>edit-part">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/part_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/part.png" class="linkfade part" />

                        </a> 
                    </li>
                   
                    <li title="equipment" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-equipment">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/equipment_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/equipment.png" class="linkfade equipment" />

                        </a>
                    </li>
                    <li title="watercraft" class="amazingcarousel-item">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-watercraft">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/watercraft_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/watercraft.png" class="linkfade watercraft" />
                        </a> 
                    </li>
                    <li title="aircraft" class="amazingcarousel-item" >
                        <a href="<?php echo BASE_RELATIVE; ?>edit-aircraft">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/aircraft_over.png"  />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/aircraft.png" class="linkfade aircraft" />
                        </a>
                    </li>
                    <li title="motorbike" class="motor amazingcarousel-item">
                       <a href="<?php echo BASE_RELATIVE; ?>edit-motorbike">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/motorbike_mobileover.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/motorbike-mobile.png" class="linkfade motorbike" />

                       </a>
                   </li>
                </ul>
                </div>
                
                 <div class="amazingcarousel-prev"></div>
                <div class="amazingcarousel-next"></div>
                <div class="amazingcarousel-nav"></div>
                
                </div>
                </div>
               
            <div id="inner-content">

 				<!--<h1 id="post">RECENT POST HISTORY</h1>-->
                     <div id="carData" class="clearfix">
                

				</div>
                <!-- END CONTENT -->
                
                        <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    </div>
                 <?php } else{?>
                 <div id="my_item">
                 <ul class="clearfix myitem">
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-info"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_info-mobile.jpg" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>messages"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/message_box-mobile.jpg" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-inventory"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_inventory-mobile.jpg" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>register-product"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/register_product-mobileover.jpg" class="linkfade" /></a></li>
                </ul>
                <p>Categories</p><br/>
                 <h1 id="sms">First step Choose the category from the following.</h1>
               
                <ul class="clearfix item">
                    <li title="car">
                        <a id="cars" href="<?php echo BASE_RELATIVE; ?>edit-car" >
                             <img src="<?php echo BASE_RELATIVE; ?>images/mywini/car-mobile.jpg" class="linkfade car" />
                        </a>
                    </li>
                    <li title="suv">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-suv" >
                             <img src="<?php echo BASE_RELATIVE; ?>images/mywini/suv-mobile.jpg" class="linkfade suv " />
                        </a>
                    </li>
                    <li title="van">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-van" >
                              <img src="<?php echo BASE_RELATIVE; ?>images/mywini/van-mobile.jpg" class="linkfade van " />
                        </a>
                    </li>
                    <li title="pickup">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-pickup" >
                             <img src="<?php echo BASE_RELATIVE; ?>images/mywini/pickup-mobile.jpg" class="linkfade pickup " />
                        </a>
                    </li>
                    <li title="truck">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-truck">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/truck-mobile.jpg" class="linkfade truck " />
                        </a>
                    </li>
                    <li title="bus">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-bus"> 
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/bus-mobile.jpg" class="linkfade bus" />
                        </a>
                    </li>
                    <li title="part">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-part">          
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/part-mobile.jpg" class="linkfade part" />
                        </a>
                    </li>
                    <li title="accessory">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-accessories">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/accessory-mobile.jpg" class="linkfade accessory" />
                        </a>
                    </li>
                    <li title="equipment">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-equipment">
                          
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/equipment-mobile.jpg" class="linkfade equipment" />
                        </a>
                    </li>
                    <li title="watercraft">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-watercraft">
                         
                          <img src="<?php echo BASE_RELATIVE; ?>images/mywini/watercraft-mobile.jpg" class="linkfade watercraft" />
                        </a>
                    </li>
                    <li title="aircraft">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-aircraft">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/aircraft-mobile.jpg" class="linkfade aircraft" />
                        </a>
                    </li>
                    <li title="motorbike">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-motorbike">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/motorbike-mobile.jpg" class="linkfade motorbike" />
                        </a>
                    </li>
                </ul>
                  
            </div>
            <div id="inner-content">
 <h1 id="post">YOUR POST HISTORY</h1>
                     <div id="carData" class="clearfix">
               

				</div>
                
                <!-- END CONTENT -->
                        <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    </div>
                    <?php }?>

                    <div class="clearfix"></div>
                    <div id="content2">

            </div>

            <div class="clearfix"></div>
        </div>

        <script language="javascript" type="text/javascript">
        $("#cars").click(function(event){
       $("#war-paper").removeAttr("style");
       $("#car_over").remove();
                 });
            /* preview uploaded image */

            $(document).ready(function(e){

                displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>');

            });


			$("#submitbtn").submit(function(event){
                    if($('#makerInputSelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" ){
                    alert("Please Input Data");

                    return false;
                   }
                 });

            function basename(path) {
               return path.split('/').reverse()[0];
            }

			function save_data_form(){

			 	$('#submitbtn').trigger('click');


			}
		    function check_vehicle_data(){
				 var data_return=false;
				 if($('#makerInputSelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" ){
                    alert("Please Input Data");
					data_return= false;
                    return false;
				 }
				 else{
					 data_return= true;
				 }
				 return data_return;


			}

			 function carImageUploadedNew_Photo(id,proid,url,mode,fname,i){

                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;

                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(i==0){

						   html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
            		}
                        else{
					       html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
            			}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;

                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
            function carImageUploaded(id,proid,url,mode,fname,primary_photo){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

						if(primary_photo==1){

							 html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
						}
                        else{
							 html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
						}
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>