<?php
$_LANG = array(
    'NEWSLETTER_HEADER' => 'Newsletter',
    'NEWSLETTER_HEADER_LABEL' => 'Your message will be sent to all users registered to newsletter.',
    'NEWSLETTER_LOAD_QUERY_ERROR' => 'Unable to load newsletter list due an internal error. See log for details.',
    'NEWSLETTER_NOUSER_LABEL' => 'There are no users registered for newsletter.',
    'NEWSLETTER_SEND_BUTTON' => 'Send newsletter',
    'NEWSLETTER_SUBJECT_LABEL' => 'Subject',
    'NEWSLETTER_NEWSLETTER_DEFAULT_SUBJECT' => 'Newsletter',
    'NEWSLETTER_MAIL_ERROR' => 'Unable to send newsletter due an internal error. See log for details.',
    'NEWSLETTER_MAIL_SUCCESS' => 'Newsletter sent successfully! A copy has been sent to your e-mail box.'
);