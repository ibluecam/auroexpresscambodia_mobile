<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Handles page requests -------------------------------------------------------
 */

if(!isset($_SESSION)) @session_start();

require_once( "social_login/hybridauth/Hybrid/Auth.php" );


class socialnetwork
{
    var $error = "";
    var $config = 'social_login/hybridauth/config.php';
    var $redirect_url;
    var $logout_redirect_url="http://demo.angkorauto.com/";
    var $user_data=NULL;
    /*
     * Constructor -------------------------------------------------------------
     * - get current slug and define page variabled.
     * @param $current_group - current user logged group.
     * @return void.
     */
    public function socialnetwork()
    {
        //Call login page by social network name
        $this->handleLoginPage($provider);
        
        $this->redirect_url=$this->getCurrentURL();
    }

    //Stand alone function to get full url
    function getCurrentURL(){
        $currentURL = (@$_SERVER["HTTP"] == "on") ? "http://" : "https://";
        $currentURL .= $_SERVER["SERVER_NAME"];
     
        if($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443")
        {
            $currentURL .= ":".$_SERVER["SERVER_PORT"];
        }
     
            $currentURL .= $_SERVER["REQUEST_URI"];
        return $currentURL;
    }
    public function handleLoginPage($provider){
        $error='';
        if( isset( $_GET["error"] ) ){
            $error = '<b style="color:red">' . trim( strip_tags(  $_GET["error"] ) ) . '</b><br /><br />';
        }
        // if user select a provider to login with
        // then inlcude hybridauth config and main class
        // then try to authenticate te current user
        // finally redirect him to his profile page
        if( !empty($provider)):
            try{
                // create an instance for Hybridauth with the configuration file path as parameter
                $hybridauth = new Hybrid_Auth( $this->config );
                
                // set selected provider name 
                $provider = @ trim( strip_tags( $provider ) );

                // try to authenticate the selected $provider
                $adapter = $hybridauth->authenticate( $provider );
                
                // if okey, we will redirect to user profile page 
                $hybridauth->redirect( $this->redirect_url, "JS");
            }
            catch( Exception $e ){
                // In case we have errors 6 or 7, then we have to use Hybrid_Provider_Adapter::logout() to 
                // let hybridauth forget all about the user so we can try to authenticate again.

                // Display the recived error, 
                // to know more please refer to Exceptions handling section on the userguide
                switch( $e->getCode() ){ 
                    case 0 : $error = "Unspecified error."; break;
                    case 1 : $error = "Hybriauth configuration error."; break;
                    case 2 : $error = "Provider not properly configured."; break;
                    case 3 : $error = "Unknown or disabled provider."; break;
                    case 4 : $error = "Missing provider application credentials."; break;
                    case 5 : $error = "Authentication failed. The user has canceled the authentication or the provider refused the connection."; break;
                    case 6 : $error = "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again."; 
                             $adapter->logout(); 
                             break;
                    case 7 : $error = "User not connected to the provider."; 
                             $adapter->logout(); 
                             break;
                } 

                // well, basically your should not display this to the end user, just give him a hint and move on..
                //$error .= "<br /><br /><b>Original error message:</b> " . $e->getMessage(); 
                //$error .= "<hr /><pre>Trace:<br />" . $e->getTraceAsString() . "</pre>";
            }
        endif;
        $this->error=$error;
    }
    //return false if not connected to social network , return true if logged in
    public function loggedInStatus($provider){
        try{
            $hybridauth = new Hybrid_Auth( $this->config );

            // selected provider name 
            $provider = @ trim( strip_tags($provider) );

            // check if the user is currently connected to the selected provider
            if( !  $hybridauth->isConnectedWith( $provider ) ){ 
                // redirect him back to login page
                //header( "Location: login.php?error=Your are not connected to $provider or your session has expired" );
                return false;
            }

            // call back the requested provider adapter instance (no need to use authenticate() as we already did on login page)
            $adapter = $hybridauth->getAdapter( $provider );

            // grab the user profile
            $user_data = $adapter->getUserProfile();
        }
        catch( Exception $e ){  
            // In case we have errors 6 or 7, then we have to use Hybrid_Provider_Adapter::logout() to 
            // let hybridauth forget all about the user so we can try to authenticate again.

            // Display the recived error, 
            // to know more please refer to Exceptions handling section on the userguide
            switch( $e->getCode() ){ 
                case 0 : $error = "Unspecified error."; break;
                case 1 : $error = "Hybriauth configuration error."; break;
                case 2 : $error = "Provider not properly configured."; break;
                case 3 : $error = "Unknown or disabled provider."; break;
                case 4 : $error = "Missing provider application credentials."; break;
                case 5 : $error = "Authentication failed. The user has canceled the authentication or the provider refused the connection."; break;
                case 6 : $error = "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again."; 
                         $adapter->logout(); 
                         break;
                case 7 : $error = "User not connected to the provider."; 
                         $adapter->logout(); 
                         break;
            } 
            return false;
            // well, basically your should not display this to the end user, just give him a hint and move on..
            //$error .= "<br /><br /><b>Original error message:</b> " . $e->getMessage(); 
            //$error .= "<hr /><pre>Trace:<br />" . $e->getTraceAsString() . "</pre>";  
        }
        $this->error=$error;
        $this->user_data=$user_data;
        return true;
    }
    public function getSocialLoginGroup(){
        return '<fieldset>
        <legend>Sign-in with one of these providers</legend>
            &nbsp;&nbsp;<a href="?provider=Google">Sign-in with Google</a><br /> 
            &nbsp;&nbsp;<a href="?provider=Yahoo">Sign-in with Yahoo</a><br /> 
            &nbsp;&nbsp;<a href="?provider=Facebook">Sign-in with Facebook</a><br />
            &nbsp;&nbsp;<a href="?provider=Twitter">Sign-in with Twitter</a><br />
            &nbsp;&nbsp;<a href="?provider=MySpace">Sign-in with MySpace</a><br />  
            &nbsp;&nbsp;<a href="?provider=Live">Sign-in with Windows Live</a><br />  
            &nbsp;&nbsp;<a href="?provider=LinkedIn">Sign-in with LinkedIn</a><br /> 
            &nbsp;&nbsp;<a href="?provider=Foursquare">Sign-in with Foursquare</a><br /> 
            &nbsp;&nbsp;<a href="?provider=AOL">Sign-in with AOL</a><br />  
        </fieldset>';
    }

    public function getSocialProfileLink(){
        // try{
            $profile_link='';
            $hybridauth = new Hybrid_Auth( $this->config );

            $connected_adapters_list = $hybridauth->getConnectedProviders(); 

            if( count( $connected_adapters_list ) ){
                foreach( $connected_adapters_list as $adapter_id ){
                    $profile_link= '&nbsp;&nbsp;<a href="social-network-profile?provider=' . $adapter_id . '">Switch to <b>' . $adapter_id . '</b>  account</a><br />'; 
                }
            }
            return $profile_link;
        // }catch( Exception $e ){
        //     echo "Ooophs, we got an error: " . $e->getMessage();

        //     echo " Error code: " . $e->getCode();

        //     echo "<br /><br />Please try again.";

        //     echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>"; 
        // }
    }

    //This function should not be showed to public
    public function getErrorMessage(){
        if( $this->error ){
            echo '<p><h3 style="color:red">Error!</h3>' . $this->error . '</p>';
            echo "<pre>Session:<br />" . print_r( $_SESSION, true ) . "</pre><hr />";
        }
    }
    //Modify default redirect url
    public function setRedirectUrl($url){
        $this->redirect_url=$url;
    }
    //Modify default redirect url
    public function getUserProfile(){
        return $this->user_data;
    }

    public function logoutAllProvider(){
        $hybridauth = new Hybrid_Auth( $this->config );
        // logout the user from $provider
        $hybridauth->logoutAllProviders(); 
        // return to login page
        //$hybridauth->redirect($this->logout_redirect_url);
    }
}