<?php

	require_once '../models/Dealer.php';
    require '../controllers/ControllerRestDealer.php';
    require_once '../models/User.php';
    require '../controllers/ControllerUser.php';
    
    $controllerRestDealer = new ControllerRestDealer('../application/DB_Connect.php');
    $controllerUser = new ControllerUser('../application/DB_Connect.php');

    $user_id = 0;
    if( !empty($_POST['user_id']) )
        $user_id = $_POST['user_id'];

    $login_hash = "";
    if( !empty($_POST['login_hash']) )
        $login_hash = $_POST['login_hash'];

    $lat = "";
    if( !empty($_POST['lat']) )
        $lat = $_POST['lat'];

    $lon = "";
    if( !empty($_POST['lon']) )
        $lon = $_POST['lon'];

    $contact_no = "";
    if( !empty($_POST['contact_no']) )
        $contact_no = trim(strip_tags($_POST['contact_no']));

    $email = "";
    if( !empty($_POST['email']) )
        $email = trim(strip_tags($_POST['email']));

    $fb = "";
    if( !empty($_POST['fb']) )
        $fb = trim(strip_tags($_POST['fb']));

    $twitter = "";
    if( !empty($_POST['twitter']) )
        $twitter = trim(strip_tags($_POST['twitter']));

    $website = "";
    if( !empty($_POST['website']) )
        $website = trim(strip_tags($_POST['website']));

    $address = "";
    if( !empty($_POST['address']) )
        $address = trim(strip_tags($_POST['address']));

    $dealer_name = "";
    if( !empty($_POST['dealer_name']) )
        $dealer_name = trim(strip_tags($_POST['dealer_name']));

    $details = "";
    if( !empty($_POST['details']) )
        $details = trim(strip_tags($_POST['details']));

    $sales_hours_monday_friday = "";
    if( !empty($_POST['sales_hours_monday_friday']) )
        $sales_hours_monday_friday = trim(strip_tags($_POST['sales_hours_monday_friday']));

    $sales_hours_saturday = "";
    if( !empty($_POST['sales_hours_saturday']) )
        $sales_hours_saturday = trim(strip_tags($_POST['sales_hours_saturday']));

    $sales_hours_sunday = "";
    if( !empty($_POST['sales_hours_sunday']) )
        $sales_hours_sunday = trim(strip_tags($_POST['sales_hours_sunday']));

    $service_hours_monday_friday = "";
    if( !empty($_POST['service_hours_monday_friday']) )
        $service_hours_monday_friday = trim(strip_tags($_POST['service_hours_monday_friday']));

    $service_hours_saturday = "";
    if( !empty($_POST['service_hours_saturday']) )
        $service_hours_saturday = trim(strip_tags($_POST['service_hours_saturday']));

    $service_hours_sunday = "";
    if( !empty($_POST['service_hours_sunday']) )
        $service_hours_sunday = trim(strip_tags($_POST['service_hours_sunday']));

    if( !empty($user_id) && !empty($lat) && !empty($lon) && 
        !empty($contact_no) && !empty($email) /*&& !empty($fb) && !empty($twitter) && 
        !empty($website)*/ && !empty($address) && !empty($dealer_name) /*&& 
        !empty($details) && !empty($sales_hours_monday_friday) && !empty($sales_hours_saturday) && 
        !empty($sales_hours_sunday) && !empty($service_hours_monday_friday) && !empty($service_hours_saturday) && 
        !empty($service_hours_sunday)*/ ) {
          
        $user = $controllerUser->getUserByUserId($user_id);

        $login_hash = str_replace(" ", "+", $login_hash);

        if($user != null) {
            
            if($user->login_hash == $login_hash) {

            	$dealer = $controllerRestDealer->getUserDealerByUserId($user_id);


                $itm = new Dealer();
  	            $itm->lat = $lat;
  	            $itm->lon = $lon;
  	            $itm->contact_no = $contact_no;
  	            $itm->email = $email;
  	            $itm->fb = $fb;
  	            $itm->twitter = $twitter;
  	            $itm->website = $website;
  	            $itm->address = $address;
  	            $itm->dealer_name = $dealer_name;
  	            $itm->details = $details;
  	            $itm->sales_hours_monday_friday = $sales_hours_monday_friday;
  	            $itm->sales_hours_saturday = $sales_hours_saturday;
  	            $itm->sales_hours_sunday = $sales_hours_sunday;
  	            $itm->service_hours_monday_friday = $service_hours_monday_friday;
  	            $itm->service_hours_saturday = $service_hours_saturday;
  	            $itm->service_hours_sunday = $service_hours_sunday;
  	            $itm->user_id = $user_id;

                if($dealer != null) {
                    $itm->dealer_id = $dealer->dealer_id;
                    $controllerRestDealer->updateDealer($itm);
                }
                else {
                    $controllerRestDealer->insertDealer($itm);
                }

                $itm = $controllerRestDealer->getUserDealerByUserId($user_id);

                $json = "{
                        \"dealer_info\" : {
                                      \"dealer_id\" : \"$itm->dealer_id\",
                                      \"lat\" : \"$itm->lat\",
                                      \"lon\" : \"$itm->lon\",
                                      \"contact_no\" : \"$itm->contact_no\",
                                      \"email\" : \"$itm->email\",
                                      \"fb\" : \"$itm->fb\",
                                      \"twitter\" : \"$itm->twitter\",
                                      \"website\" : \"$itm->website\",
                                      \"address\" : \"$itm->address\",
                                      \"dealer_name\" : \"$itm->dealer_name\",
                                      \"details\" : \"$itm->details\",
                                      \"sales_hours_monday_friday\" : \"$itm->sales_hours_monday_friday\",
                                      \"sales_hours_saturday\" : \"$itm->sales_hours_saturday\",
                                      \"sales_hours_sunday\" : \"$itm->sales_hours_sunday\",
                                      \"service_hours_monday_friday\" : \"$itm->service_hours_monday_friday\",
                                      \"service_hours_saturday\" : \"$itm->service_hours_saturday\",
                                      \"service_hours_sunday\" : \"$itm->service_hours_sunday\",
                                      \"user_id\" : \"$itm->user_id\"
                                      },
                        \"status\" : {
                                      \"status_code\" : \"-1\",
                                      \"status_text\" : \"Success.\"
                                    }
                        }";
            }
            else {

                $json = "{
                        \"status\" : {
                                      \"status_code\" : \"5\",
                                      \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                    }
                        }";
            }

        }
        else {
        	$json = "{
                        \"status\" : {
                                      \"status_code\" : \"5\",
                                      \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                    }
                        }";
        }
    }

    else {

        $json = "{
                  \"status\" : {
                                \"status_code\" : \"3\",
                                \"status_text\" : \"Invalid Access.\"
                              }
                  }";

        
    }

    echo $json;
?>