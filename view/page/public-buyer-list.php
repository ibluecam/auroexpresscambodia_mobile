
<?php
	// get buyer
	$listbuyer = $_CLASS->load_buyer();
?>

<div id="wrapper-buyer">
  <div id="search-buyer">
	<table>
		  <tr>
			  <td><p class='find-buyer'>Find Car Wanted : </p></td>
			  <form action="" method="post">
			  <td><input type="text" name="search" class='txt-search' placeholder="Keywords"/></td>
			  <td><input type="submit" value='' name='cmdsearch' class='btn-search linkimg' /></td>
			  </form>
		  </tr>
	</table>
  </div>
  
  <div id="container-items">
		<ul class="clearfix">
		<?php 
		    $i=0;
			if(count($listbuyer) > 0){
			foreach($listbuyer as $buyer){
		?>
			<li>
				<div class='dealer-block'>
					<p class='buyer-name'><?php echo $buyer['full_name']; ?></p>
					<div class='line'></div>
					<p class='gender'>Gender : <font><?php echo $buyer['gender'];?></font></p>
					<p class='tel'><font>Tel : </font><?php echo $buyer['tel'];?></p>
					<p class='email'><font>Email : </font><?php echo $buyer['email'];?></p>

					<div class='block-left'>
						<p class='make'>Make : <font><?php echo $buyer['make'];?></font></p>
						<p class='year'>Year : <font><?php echo $buyer['year_car'];?></font></p>
						<p class='model'>Model : <font><?php echo $buyer['model'];?></font></p>
					</div>
					<div class='block-left block-right'>
						<p class='color'>Color : <font><?php echo $buyer['color'];?></font></p>
						<p class='condition'>Condition : <font><?php echo $buyer['condition_car'];?></font></p>
						<p class='price'>Price : <font class='colo'>
						<?php if($buyer['price']=="" or $buyer['price']=="0"){
						        echo"";
							 } else {
							   echo $buyer['price'].'$';
							 }
						?>
						</font></p>
					</div>
					<p class='description'>
						<?php echo $buyer['description'];?>
					</p>
				</div>
			</li>
        <?php 
		 }
		}
		?>			
		</ul>
  </div>
    <div id="pagegination">
		<p class='total-record'>Total:</p>
		<p class='record'><?php echo $total_buyer; ?></p>
		<p class='label-record'>Record</p>
		<?php
			echo "<p style='clear:both;'>$pagination_html</p>";
		?>
	</div>
	
	
</div>