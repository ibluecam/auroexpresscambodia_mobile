<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'user';
$slug  = 'car';
$get_dealweek = $_CLASS->loadDealweek($_GET['wid']);

$str = $get_dealweek['date'];  
$i = strrpos($str, " ");
$l = strlen($str) - $i;
$str = substr($str, $i, $l);
$date = str_ireplace($str, "", $get_dealweek['date']);

// load page content.

$pg_details = $_CLASS->getPageHTML();

?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
 
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
 


 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > <a href="#" class="linkfade">Deal of the Week</a> > View</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Trade Information Detail</p>
                </div>
                <div id="community_view" class="clearfix">
                	<div id="logo">
                        <?php                                       
                            if(!file_exists('upload/'.$get_dealweek['image']) || $get_dealweek['image']==""){                                         
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="40" height="40" />
                        <?php                                           
                            }else{
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>upload/<?php echo $get_dealweek['image'];?>"  width="40" height="40" />
                        <?php
                            }
                        ?>
                    	
                    </div>
                    <div id="detail">
                    	<p class="title">
                        Category: <?php echo $get_dealweek['category']?>
                        </p>
                        <p>Written by : <span class="writer"><?php echo $get_dealweek['name']?></span>	Date : <?php echo date("F. j. Y",strtotime($get_dealweek['date']));?>	<span> </span></p>
                    </div>
                    <div id="img_wrap" class="clearfix">
                        <div id="text_editor" style="width:100%;height:100%;word-wrap:break-word;border-width:3px;border-style:solid;border-color:#d1d5fa;padding:10px;border-radius:5px;">
                             <?php echo $get_dealweek['description'];?> 
                        </div>                    	
                    </div>
                             
                </div>
                <div id="button_wrap">
                    	<form>
                        	<!--input type="submit" class="write" value=""/-->
                            <a href="<?php echo BASE_RELATIVE;?>trade-info"><img src="<?php echo BASE_RELATIVE;?>images/community/back_to_list.png" /></a>
                        </form>
                        <div id="back">
                        	
                        </div>
                    </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  