<?php

################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');

class publicvehicle
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'vehicle';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    private $vehicle;
    private $news_list;
    private $special_offer;
    protected $total_num_row;
    var $pagination_html;
    var $slug_detail='';
	
	// count steering
	var $count_left;
	var $count_right;
	
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicvehicle($lang=array())
    {

        $this->lang = $lang;
        $this->loadVehicleData();
        //$this->loadSpecialOffer();
        //$this->loadNewsList();
		
		$this->loadsteering();
    }

	// public function loadReserve($product_id){
 //        require_once BASE_ROOT . 'core/class-connect.php';
 //        $cnx = new Connect();
 //        $cnx->open();

 //        $sql_query = "SELECT * FROM product WHERE status='Reserved' AND id='$product_id'";

 //        if( !$sql = mysql_query($sql_query) ){
 //            die('Unable to load reserve. '. __FILE__ .':'. __LINE__);
	// 		$cnx->close();
 //        }

 //        $cnx->close();

	// 	return mysql_num_rows($sql);
 //    }

	// public function loadSold($product_id){
 //        require_once BASE_ROOT . 'core/class-connect.php';
 //        $cnx = new Connect();
 //        $cnx->open();

 //        $sql_query = "SELECT * FROM product WHERE status='Sold' AND id='$product_id'";

 //        if( !$sql = mysql_query($sql_query) ){
 //            die('Unable to load sold. '. __FILE__ .':'. __LINE__);
	// 		$cnx->close();
 //        }

 //        $cnx->close();

	// 	return mysql_num_rows($sql);
 //    }
	/*
     * private method: get name and price of vehicle ------------------------------------------
     * @return bool.
     */
    private function loadVehicleData(){

        echo "Hello"; exit();
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        //$cnx->open();
        $cnx->pdoOpen();

        $where=" WHERE p.del_flag = 0 ";
        $select_sql="SELECT SQL_CACHE
                p.car_id,
				p.owner,
                p.desc1,
				p.status,
                p.hit_count,
                p.price,
				p.category,
                p.currency,
                p.make,
				p.category,
				p.color,
				p.chassis_no,
                p.model,
                p.model_year,
                p.price,
                p.location,
                p.part_name,
				p.exterior_color,
                p.product_type, c.source as carImg, c.thumb as thumb, cl.country_name as country,
                cl.cc as flag ,cl.country_name,rg.user_id as comid, rg.company_name, rg.name";
        $sql_search="FROM product as p
                    LEFT JOIN car_media as c on p.car_id=c.product_id AND c.primary_photo='1' OR c.primary_photo='2'
                    INNER JOIN country_list as cl on p.location=cl.cc
                    LEFT JOIN register as rg on p.owner=rg.user_id
        ";

        if(isset($_GET['page'])) $page =$_GET['page']; else $page="";
        if(isset($_SESSION['log_id'])) $log_id =$_SESSION['log_id']; else $log_id="";


        if(isset($_GET['product_type'])) $product_type =strtolower($_GET['product_type']); else $product_type="Car";

        if(isset($_GET['vehicle_type'])) $vehicle_type =$_GET['vehicle_type']; else $vehicle_type="";
        if(isset($_GET['condition'])) $condition =$_GET['condition']; else $condition="";
		 if(isset($_GET['own'])) $own =$_GET['own']; else $own="";
        if(isset($_GET['fuel_type'])) $fuel_type =$_GET['fuel_type']; else $fuel_type="";
        if(isset($_GET['transmission'])) $transmission =$_GET['transmission']; else $transmission="";
        if(isset($_GET['engine_volume'])) $engine_volume =$_GET['engine_volume']; else $engine_volume="";
        if(isset($_GET['engine_volume_to'])) $engine_volume_to =$_GET['engine_volume_to']; else $engine_volume_to="";
        if(isset($_GET['engine_volume_from'])) $engine_volume_from =$_GET['engine_volume_from']; else $engine_volume_from="";
        if(isset($_GET['price_to'])) $price_to =preg_replace("/[^0-9]/","",$_GET['price_to']); else $price_to="";
        if(isset($_GET['price_from'])) $price_from =preg_replace("/[^0-9]/","",$_GET['price_from']); else $price_from="";
        if(isset($_GET['category'])) $category =$_GET['category']; else $category="";
        if(isset($_GET['category2'])) $category2 =$_GET['category2']; else $category2="";
        if(isset($_GET['category3'])) $category3 =$_GET['category3']; else $category3="";
        if(isset($_GET['make'])) $make =$_GET['make']; else $make="";
        if(isset($_GET['model'])) $model =$_GET['model']; else $model="";
        if(isset($_GET['steering'])) $steering =$_GET['steering']; else $steering="";
        if(isset($_GET['country'])) $country =$_GET['country']; else $country="";
        if(isset($_GET['year_from'])) $year_from =$_GET['year_from']; else $year_from=0;
        if(isset($_GET['year_to'])) $year_to =$_GET['year_to']; else $year_to=0;
        if(isset($_GET['man_year_from'])) $man_year_from =$_GET['man_year_from']; else $man_year_from=0;
        if(isset($_GET['man_year_to'])) $man_year_to =$_GET['man_year_to']; else $man_year_to=0;
        if(isset($_GET['drive_type'])) $drive_type =$_GET['drive_type']; else $drive_type="";

        if(isset($_GET['man_month_from'])) $man_month_from =$_GET['man_month_from']; else $man_month_from=0;
        if(isset($_GET['man_month_to'])) $man_month_to =$_GET['man_month_to']; else $man_month_to=0;
        if(isset($_GET['customSearch'])) $customSearch =$_GET['customSearch']; else $customSearch="";
        if(isset($_GET['owner'])) $owner =$_GET['owner']; else $owner="";


        $where=" WHERE 1 ";
        if(empty($product_type)){
            $table="active_product";
        }else{
            $table="active_product_".$product_type;
        }
        $select_sql="SELECT SQL_CACHE
                rg.user_id as comid,
				p.car_id,
				p.owner,
                p.desc1,
				p.status,
                p.hit_count,
                p.price,
                p.currency,
                p.make,
				p.color,
				p.chassis_no,
                p.model,
                p.model_year,
                p.price,
                p.location,
                p.part_name,
				p.exterior_color,
                p.created_date,
                p.seller_comment,
                p.product_type, c.source as carImg, c.thumb as thumb, cl.country_name as country,
                cl.cc as flag ,cl.country_name, rg.company_name, rg.name ";
        $sql_search="FROM $table as p
                    LEFT JOIN product_primary_photo AS c ON c.product_id=p.car_id
                    INNER JOIN country_list as cl on p.location=cl.cc
                    LEFT JOIN activated_seller as rg on p.owner=rg.user_id
        ";


  //       $page = mysql_real_escape_string(stripcslashes($page));
  //       $log_id = mysql_real_escape_string(stripcslashes($log_id));
  //       $product_type = mysql_real_escape_string(stripcslashes($product_type));
		// $vehicle_type = mysql_real_escape_string(stripcslashes($vehicle_type));
  //       $condition = mysql_real_escape_string(stripcslashes($condition));
  //       $fuel_type = mysql_real_escape_string(stripcslashes($fuel_type));
  //       $transmission = mysql_real_escape_string(stripcslashes($transmission));
  //       $engine_volume= mysql_real_escape_string(stripcslashes($engine_volume));
  //       $engine_volume_to= mysql_real_escape_string(stripcslashes($engine_volume_to));
  //       $engine_volume_from= mysql_real_escape_string(stripcslashes($engine_volume_from));
  //       $price_to= mysql_real_escape_string(stripcslashes($price_to));
  //       $price_from= mysql_real_escape_string(stripcslashes($price_from));

  //       $category = mysql_real_escape_string(stripcslashes($category));
  //       $category2 = mysql_real_escape_string(stripcslashes($category2));
  //       $category3 = mysql_real_escape_string(stripcslashes($category3));
  //       $make = mysql_real_escape_string(stripcslashes($make));
  //       $model = mysql_real_escape_string(stripcslashes($model));
  //       $steering = mysql_real_escape_string(stripcslashes($steering));
  //       $country = mysql_real_escape_string(stripcslashes($country));
  //       $year_from = (int)(stripcslashes($year_from));
  //       $year_to = (int)(stripcslashes($year_to));
  //       $man_year_from = (int)(stripcslashes($man_year_from));
  //       $man_year_to = (int)(stripcslashes($man_year_to));
  //       $man_month_from = (int)(stripcslashes($man_month_from));
  //       $man_month_to = (int)(stripcslashes($man_month_to));
  //       $customSearch = mysql_real_escape_string(stripcslashes($customSearch));
  //       $owner = mysql_real_escape_string(stripcslashes($owner));
        if($page=='inventory'){
            $where.=" AND `p`.`owner`='$log_id'";
        }
		if($page=='vehicle'){
            $where.=" AND `p`.`owner`='$own'";
        }
        // if(!empty($product_type)){
        //     $where.=" AND `p`.`product_type`='$product_type'";
        // }
		if(!empty($vehicle_type)){
            $where.=" AND `p`.`body_style`='$vehicle_type'";
        }
        if(!empty($condition)){
            $where.=" AND `p`.`car_type`='$condition'";
        }
        if(!empty($fuel_type)){
            $where.=" AND `p`.`fuel_type`='$fuel_type'";
        }
        if(!empty($transmission)){
            $where.=" AND `p`.`transmission`='$transmission'";
        }
        if(!empty($drive_type)){
            $where.=" AND `p`.`drivetrain`='$drive_type'";
        }
        if(!empty($engine_volume)){
            $where.=" AND `p`.`engine`=$engine_volume";
        }
        if(!empty($engine_volume_to)){
            $where.=" AND `p`.`engine`<=$engine_volume_to";
        }
        if(!empty($engine_volume_from)){
            $where.=" AND `p`.`engine`>=$engine_volume_from";
        }
        if(!empty($price_to)){
            $where.=" AND `p`.`price`<=$price_to";
        }
        if(!empty($price_from)){
            $where.=" AND `p`.`price`>=$price_from";
        }
        if(!empty($category)){
            $where.=" AND `p`.`category`='$category'";
        }
        if(!empty($category2)){
            $where.=" AND `p`.`sub_category_group`='$category2'";
        }
        if(!empty($category3)){
            $where.=" AND `p`.`sub_category`='$category3'";
        }
        if(!empty($make)){
            $where.=" AND `p`.`make`='$make'";
        }
        if(!empty($model)){
            $where.=" AND `p`.`model`='$model'";
        }
        if(!empty($steering)){
            $where.=" AND `p`.`desc1`='$steering'";
        }
        if(!empty($country)){
            $where.=" AND `p`.`location`='$country'";
        }
        if($year_from>0){
            $where.=" AND (`p`.`year`>=$year_from OR `p`.`built_year`>=$year_from )";
        }
        if($year_to>0){
            $where.=" AND (`p`.`year`<=$year_to OR `p`.`built_year`<=$year_to )";
        }
        if($man_year_from>0){
            $where.=" AND LEFT(`p`.manufacturer_date, 4)>=$man_year_from";
        }
        if($man_year_to>0){
            $where.=" AND LEFT(`p`.manufacturer_date, 4)<=$man_year_to";
        }
        if($man_month_from>0){
            $where.=" AND RIGHT(`p`.manufacturer_date, 2)>=$man_month_from";
        }
        if($man_month_to>0){
            $where.=" AND RIGHT(`p`.manufacturer_date, 2)<=$man_month_to";
        }
        if(!empty($customSearch)){
            $where.=" AND (`p`.`vin_number` LIKE '%$customSearch%'
            OR `p`.`make` LIKE '%$customSearch%'
            OR `p`.`category` LIKE '%$customSearch%'
            OR `p`.`model` LIKE '%$customSearch%'
            OR `p`.`id` LIKE '%$customSearch%'
            OR `p`.`chassis_no` LIKE '%$customSearch%'
            OR `p`.`vin_number` LIKE '%$customSearch%'
            OR `p`.`year` LIKE '%$customSearch%'
            OR `p`.`fitting_make` LIKE '%$customSearch%'
            OR `p`.`part_name` LIKE '%$customSearch%'
            OR `rg`.`company_name` LIKE '%$customSearch%'
            OR `rg`.`name` LIKE '%$customSearch%'
            )
            ";
        }
        if(!empty($owner)){
            $where.=" AND `p`.`owner`='$owner'";
        }

        $this->slug_detail= ucfirst($product_type);


        // Group by product ID.
        //$sql_search.=$where."GROUP BY p.car_id ";
        //$sql_search.=$where." GROUP BY p.car_id ORDER BY created_date DESC ";
        // echo $sql_search;
        /////PAGINATION PROCESS//////
        $select_sql_count="SELECT SQL_CACHE COUNT(DISTINCT p.car_id) as num_row ";
        $sql_count=$select_sql_count.$sql_search.$where." ORDER BY created_date DESC ";

        $result_count = $cnx->pdoExecuteQuery($sql_count);
        $row_count=$cnx->getRecords();

        $this->total_num_row= $row_count[0]['num_row'];
        //echo $sql_count;
        $links = new Pagination ($this->total_num_row,20);
        $limit=$links->start_display;
        $this->pagination_html.= $links->display();

        ///////PAGINATION PROCESS///////
        $sql_search.=$where." GROUP BY p.car_id ORDER BY created_date DESC ".$limit;
		 // echo $sql_search;
        $sql_product=$select_sql.$sql_search;
        // echo $sql_product;

        $cnx->pdoExecuteQuery($sql_product);
        // if( !$sql = $cnx->pdoExecuteQuery($sql_product) ){
        //     require_once BASE_CLASS . 'class-log.php';
        //     LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
        //     $cnx->close();
        //     return;
        // }

        if( count($cnx->getRecords()) < 1 ){
            //$cnx->close();
        }

        //$this->vehicle = array();

        // while( $r = @mysql_fetch_assoc($sql) ){
        //     array_push($this->vehicle, $r);
        // }
        // var_dump($this->vehicle);
        $this->vehicle = $cnx->getRecords();
        @mysql_free_result($sql);
        $cnx->close();

    }

    public function getTotalItems(){
        return $this->total_num_row;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
	/*
     * Public method: get vehicle data -------------------------------------------
     * @return string.
     */
	public function getVehicle(){
        return $this->vehicle;
    }

	public function loadcategory(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $category=array();
        $sql_str="
                SELECT name,title
                FROM product_type_list
                ORDER BY name ASC

                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($category, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $category;

    }

	// Vehicle type search
	public function load_make(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $vehicle_type=array();
        $sql_str="
               SELECT  active_product.make , COUNT(active_product.make) total_make from active_product GROUP BY make ORDER BY total_make DESC

                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($vehicle_type, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $vehicle_type;

    }
	
	// count steering
	public function loadsteering(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        //$category=array();
        $sql_right="
                SELECT ap.desc1,ap.product_type,rg.user_id
                FROM active_product as ap
				INNER JOIN register as rg ON ap.owner=rg.user_id
				WHERE ap.desc1='1' AND ap.product_type='Car'
                ";

		$sql_left="
                SELECT ap.desc1,ap.product_type,rg.user_id
                FROM active_product as ap
				INNER JOIN register as rg ON ap.owner=rg.user_id
				WHERE ap.desc1='0' AND ap.product_type='Car'
                ";
		
        if( !$sql = @mysql_query($sql_right) ){
            return 0;
        }
		if( !$sql = @mysql_query($sql_left) ){
            return 0;
        }
        $sql_count_right = @mysql_query($sql_right);
		$this->count_right = @mysql_num_rows($sql_count_right);
			
		$sql_count_left = @mysql_query($sql_left);
		$this->count_left = @mysql_num_rows($sql_count_left);

        @mysql_free_result($sql);
        $cnx->close();  		
    }		
	
}
?>
