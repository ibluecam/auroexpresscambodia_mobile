<?php

require_once './models/Dealer.php';
 
class ControllerDealer
{
 
    private $db;
    private $pdo;
    function __construct() 
    {
        require_once './application/DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }
 
    function __destruct() { }
 
    public function updateDealer($itm) 
    {
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_dealers 
                                        SET 
                                            lat = :lat, 
                                            lon = :lon, 
                                            contact_no = :contact_no, 
                                            email = :email, 
                                            fb = :fb, 
                                            twitter = :twitter, 
                                            website = :website, 
                                            address = :address, 
                                            dealer_name = :dealer_name, 
                                            details = :details, 
                                            sales_hours_monday_friday = :sales_hours_monday_friday, 
                                            sales_hours_saturday = :sales_hours_saturday, 
                                            sales_hours_sunday = :sales_hours_sunday, 
                                            service_hours_monday_friday = :service_hours_monday_friday, 
                                            service_hours_saturday = :service_hours_saturday, 
                                            service_hours_sunday = :service_hours_sunday 

                                        WHERE dealer_id = :dealer_id');

        $result = $stmt->execute(
                            array('lat' => $itm->lat,
                                    'lon' => $itm->lon,
                                    'contact_no' => $itm->contact_no,
                                    'email' => $itm->email,
                                    'fb' => $itm->fb,
                                    'twitter' => $itm->twitter,
                                    'website' => $itm->website,
                                    'address' => $itm->address,
                                    'dealer_name' => $itm->dealer_name,
                                    'details' => $itm->details,
                                    'sales_hours_monday_friday' => $itm->sales_hours_monday_friday, 
                                    'sales_hours_saturday' => $itm->sales_hours_saturday,
                                    'sales_hours_sunday' => $itm->sales_hours_sunday,
                                    'service_hours_monday_friday' => $itm->service_hours_monday_friday,
                                    'service_hours_saturday' => $itm->service_hours_saturday,
                                    'service_hours_sunday' => $itm->service_hours_sunday,
                                    'dealer_id' => $itm->dealer_id) );
        
        return $result ? true : false;
    }

    public function deleteDealer($dealer_id, $is_deleted) 
    {
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_dealers 
                                        SET is_deleted = :is_deleted
                                        WHERE dealer_id = :dealer_id');


        $result = $stmt->execute(
                            array('is_deleted' => $is_deleted, 
                                    'dealer_id' => $dealer_id) );
        
        return $result ? true : false;
    }


    public function insertDealer($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO tbl_carfinder_dealers( 
                                    lat, 
                                    lon, 
                                    contact_no, 
                                    email, 
                                    fb, 
                                    twitter, 
                                    website, 
                                    address, 
                                    dealer_name, 
                                    details, 
                                    sales_hours_monday_friday, 
                                    sales_hours_saturday, 
                                    sales_hours_sunday, 
                                    service_hours_monday_friday, 
                                    service_hours_saturday, 
                                    service_hours_sunday ) 

                                VALUES(
                                    :lat, 
                                    :lon, 
                                    :contact_no, 
                                    :email, 
                                    :fb, 
                                    :twitter, 
                                    :website, 
                                    :address, 
                                    :dealer_name, 
                                    :details, 
                                    :sales_hours_monday_friday, 
                                    :sales_hours_saturday, 
                                    :sales_hours_sunday, 
                                    :service_hours_monday_friday, 
                                    :service_hours_saturday, 
                                    :service_hours_sunday 
                                 )');

        $result = $stmt->execute(
                            array('lat' => $itm->lat,
                                    'lon' => $itm->lon,
                                    'contact_no' => $itm->contact_no,
                                    'email' => $itm->email,
                                    'fb' => $itm->fb,
                                    'twitter' => $itm->twitter,
                                    'website' => $itm->website,
                                    'address' => $itm->address,
                                    'dealer_name' => $itm->dealer_name,
                                    'details' => $itm->details,
                                    'sales_hours_monday_friday' => $itm->sales_hours_monday_friday, 
                                    'sales_hours_saturday' => $itm->sales_hours_saturday,
                                    'sales_hours_sunday' => $itm->sales_hours_sunday,
                                    'service_hours_monday_friday' => $itm->service_hours_monday_friday,
                                    'service_hours_saturday' => $itm->service_hours_saturday,
                                    'service_hours_sunday' => $itm->service_hours_sunday) );
    
        return $result ? true : false;
    }
 
    
    public function getDealers() 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM tbl_carfinder_dealers 
                                WHERE is_deleted = 0 ORDER BY dealer_name ASC');

        $stmt->execute( );

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Dealer();
            $itm->dealer_id = $row['dealer_id'];
            $itm->lat = $row['lat'];
            $itm->lon = $row['lon'];
            $itm->contact_no = $row['contact_no'];
            $itm->email = $row['email'];
            $itm->fb = $row['fb'];
            $itm->twitter = $row['twitter'];
            $itm->website = $row['website'];
            $itm->address = $row['address'];
            $itm->dealer_name = $row['dealer_name'];
            $itm->details = $row['details'];
            $itm->sales_hours_monday_friday = $row['sales_hours_monday_friday'];
            $itm->sales_hours_saturday = $row['sales_hours_saturday'];
            $itm->sales_hours_sunday = $row['sales_hours_sunday'];
            $itm->service_hours_monday_friday = $row['service_hours_monday_friday'];
            $itm->service_hours_saturday = $row['service_hours_saturday'];
            $itm->service_hours_sunday = $row['service_hours_sunday'];

            $array[$ind] = $itm;
            $ind++;
        }

        return $array;
    }


    public function getDealersBySearching($search) 
    {

        $stmt = $this->pdo->prepare('SELECT * 
                                FROM tbl_carfinder_dealers 
                                WHERE is_deleted = 0 AND dealer_name LIKE :search ORDER BY dealer_name ASC');

        $stmt->execute( array('search' => '%'.$search.'%'));

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Dealer();
            $itm->dealer_id = $row['dealer_id'];
            $itm->lat = $row['lat'];
            $itm->lon = $row['lon'];
            $itm->contact_no = $row['contact_no'];
            $itm->email = $row['email'];
            $itm->fb = $row['fb'];
            $itm->twitter = $row['twitter'];
            $itm->website = $row['website'];
            $itm->address = $row['address'];
            $itm->dealer_name = $row['dealer_name'];
            $itm->details = $row['details'];
            $itm->sales_hours_monday_friday = $row['sales_hours_monday_friday'];
            $itm->sales_hours_saturday = $row['sales_hours_saturday'];
            $itm->sales_hours_sunday = $row['sales_hours_sunday'];
            $itm->service_hours_monday_friday = $row['service_hours_monday_friday'];
            $itm->service_hours_saturday = $row['service_hours_saturday'];
            $itm->service_hours_sunday = $row['service_hours_sunday'];

            $array[$ind] = $itm;
            $ind++;
        }

        return $array;
    }


    public function getDealerByDealerId($dealer_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM tbl_carfinder_dealers 
                                WHERE dealer_id = :dealer_id');

        $stmt->execute( array('dealer_id' => $dealer_id));
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Dealer();
            $itm->dealer_id = $row['dealer_id'];
            $itm->lat = $row['lat'];
            $itm->lon = $row['lon'];
            $itm->contact_no = $row['contact_no'];
            $itm->email = $row['email'];
            $itm->fb = $row['fb'];
            $itm->twitter = $row['twitter'];
            $itm->website = $row['website'];
            $itm->address = $row['address'];
            $itm->dealer_name = $row['dealer_name'];
            $itm->details = $row['details'];
            $itm->sales_hours_monday_friday = $row['sales_hours_monday_friday'];
            $itm->sales_hours_saturday = $row['sales_hours_saturday'];
            $itm->sales_hours_sunday = $row['sales_hours_sunday'];
            $itm->service_hours_monday_friday = $row['service_hours_monday_friday'];
            $itm->service_hours_saturday = $row['service_hours_saturday'];
            $itm->service_hours_sunday = $row['service_hours_sunday'];

            return $itm;
        }
        return null;
    }


}
 
?>