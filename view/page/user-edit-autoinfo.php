<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$status = $_CLASS->getstatus();
$product_maker = $_CLASS->loadProduct();
$get_autoinfo = $_CLASS->loadAutoInfo($_GET['id']);



if($get_autoinfo['owner']!=$_SESSION['log_id']){    
    header("location:auto-info");
}
else{
    if(isset($_POST['update_autoinfo'])){
        $_CLASS->updateAutoinfo($_SESSION['log_id'],$get_autoinfo['owner']);
        $status = $_CLASS->getstatus();

        if( $status == 'ok')
            {
              FeedbackMessage::displayOnPage('Submit Sucessfully', 'success');
        ?>
        <p class="alert alert-success" style="text-align:center;"><strong><?php echo  "SUBMIT COMPLETE";?></strong></p>

        <?php
            }

        if($status=='error'){
            ?>    
            <p class="alert alert-error" style="text-align:center;"><strong><?php echo 'Please fill all fields before submitting the form.';?></strong></p>
         
         <?php }

        if($status=='no'){
            ?>    
            <p class="alert alert-error" style="text-align:center;"><strong><?php echo 'You are not the owner of this product.';?></strong></p>
         
         <?php }
                
            }

}



?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > Auto Infomation</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Auto Informatino</p>
                </div>
                <div id="register_info">
                	<form action="<?php echo BASE_RELATIVE;?>edit-autoinfo?id=<?php echo $_GET['id'];?>" method ="post" enctype="multipart/form-data">
					<div id="radio_wraper" class="clearfix">                    
                        <div id="category">Category</div>                        
                        <div id="specification"><input type="radio" name="category" value="specification" <?php echo ($get_autoinfo['category'] == "specification" ? 'checked="checked"': ''); ?>/> Specification</div>
                        <div id="news"><input type="radio" name="category" value="auto_new" <?php echo ($get_autoinfo['category'] == "auto_new" ? 'checked="checked"': ''); ?> /> Auto News</div>
                        <div id="photo"><input type="radio" name="category" value="photo" <?php echo ($get_autoinfo['category'] == "photo" ? 'checked="checked"': ''); ?>/>Photo</div>
                    </div>
                    <div id="table_wrap">
                    	<table border="0" cellpadding="0" cellspacing="0">
                        	<tr>
                            	<td class="label">Item</td>
                                <td>
                                	<select class="text_box" name="item">
                                        <option><?php echo $get_autoinfo['item'];?></option>
                                        <option>Car</option>
                                        <option>Truck</option>
                                        <option>Bus</option>                                        
                                        <option>Heavy Machine</option>
                                        <option>Part</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td class="label">Maker</td>
                                <td>
                                	<select class="text_box" name="make">
                                        <option><?php echo $get_autoinfo['make'];?></option>      
                                        <?php foreach($product_maker as $value){?>
                                            <option><?php echo $value['make'];?></option>
                                        <?php }?>                                 
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td class="label">Model</td>
                                <td>
                                	<input type="text" class="text_box" name="model" value="<?php echo $get_autoinfo['model'];?>">
                                </td>
                            </tr>
                             <tr>
                            	<td class="label">Title</td>
                                <td>
                                	<input type="text" class="text_box" name="topic" value="<?php echo $get_autoinfo['topic'];?>" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="text_editor">
                    	<p>Message</p>
                        <textarea id="_ebody" name="message" ><?php echo $get_autoinfo['message'];?></textarea>
                        
                    </div>
                    <div id="attach" class="clearfix">                                           
                        <div class='file_upload' id='f1'><input name='myfile' type='file'/></div>
                        <div style="float:left; padding:10px 0px 0px 10px;">
                            <p> Select your files</p>
                        </div>
                    </div>
                    
                    <div id="button_wrap">
                    	<div id="upload">
                        	<input type="submit" class="upload" value="" name="update_autoinfo" />
                            <div id="back">
                            	<a href="<?php echo BASE_RELATIVE;?>auto-info"><img src="<?php echo BASE_RELATIVE;?>images/community/back.png" /></a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  