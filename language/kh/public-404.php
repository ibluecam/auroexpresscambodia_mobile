<?php
$_LANG = array(
    'p404_HEADER' => '404 Page not found',
    'p404_LABEL' => 'It seems the page you are trying to access does not exists or has been removed. If you believe there is a problem with the page don\'t hesitate in contacting us!'
);