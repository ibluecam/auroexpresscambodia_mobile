
        <div id="content-wrapper">
            <div id="my_item">
                <ul class="clearfix myitem">
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-info"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_infonew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>messages"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/message_boxnew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>my-inventory"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/my_inventorynew.png" class="linkfade" /></a></li>
                    <li><a href="<?php echo BASE_RELATIVE; ?>edit-car"><img src="<?php echo BASE_RELATIVE; ?>images/mywini/register_productover.png" class="linkfade" /></a></li>
                </ul>
                <p>Categories</p>
                <ul class="clearfix item">
                    <li title="car">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-car" >
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/car_over.png" />
                        </a>
                    </li>
                    <li title="truck">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-truck">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/truck_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/truck.png" class="linkfade truck " />

                        </a>
                    </li>
                    <li title="bus">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-bus">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/bus_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/bus.png" class="linkfade bus" />

                        </a>
                    </li>
                    <li title="part">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-part">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/part_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/part.png" class="linkfade part" />

                        </a>
                    </li>
                    <li title="accessory">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-accessories">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/accessory_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/accessory.png" class="linkfade accessory" />

                        </a>
                    </li>
                    <li title="equipment">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-equipment">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/equipment_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/equipment.png" class="linkfade equipment" />

                        </a>
                    </li>
                    <li title="watercraft">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-watercraft">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/watercraft_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/watercraft.png" class="linkfade watercraft" />

                        </a>
                    </li>
                    <li title="aircraft">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-aircraft">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/aircraft_over.png"  />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/aircraft.png" class="linkfade aircraft" />
                        </a>
                    </li>
                    <li title="motorbike" class="motor">
                        <a href="<?php echo BASE_RELATIVE; ?>edit-motorbike">
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/motorbike_over.png" />
                            <img src="<?php echo BASE_RELATIVE; ?>images/mywini/motorbike.png" class="linkfade motorbike" />

                        </a>
                    </li>

                </ul>
            </div>
           <!-- <div id="topMenu">
                <p><a href="#" class="linkfade">Register Item</a> > Car</p>
            </div>
                <?php include("php/sidebar/my-wini.php");?>
            </div> -->
            <div id="inner-content">
                <!-- START CONTENT -->
                 <!-- <div class="page-title">
                    <?php
                    if(empty($getId)){
                        echo "<p>".$_LANG['PRODUCT_HEADER']."</p>";
                    }else{
                        echo "<p>".$_LANG['PRODUCT_EDIT_HEADER']."</p>";
                    }
                    ?>
                </div> -->



                <!-- END CONTENT -->


                        <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    </div>

                    <div class="clearfix"></div>
                    <div id="content2">

            </div>

            <div class="clearfix"></div>
        </div>

        <script language="javascript" type="text/javascript">
            /* preview uploaded image */

            $(document).ready(function(e){

                displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>');

            });

            $("#submitbtn").submit(function(event){
                    if($('#makerInputSelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" ){
                    alert("Please Input Data");

                    return false;
                   }
                 });

            function basename(path) {
               return path.split('/').reverse()[0];
            }

            function save_data_form(){

                $('#submitbtn').trigger('click');


            }
            function check_vehicle_data(){
                 var data_return=false;
                 if($('#makerInputSelect').val()=="" || $('#modelInputSelect').val()=="" || $('#countryInputSelect').val()=="" ){
                    alert("Please Input Data");
                    data_return= false;
                    return false;
                 }
                 else{
                     data_return= true;
                 }
                 return data_return;


            }

             function carImageUploadedNew_Photo(id,proid,url,mode,fname,i){

                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;

                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

                        if(i==0){

                           html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
                    }
                        else{
                           html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
                        }
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;

                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }




            function carImageUploaded(id,proid,url,mode,fname,primary_photo){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

                        if(primary_photo==1){

                             html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
                        }
                        else{
                             html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
                        }
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>
