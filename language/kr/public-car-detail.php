<?php
$_LANG = array(
    'CAR_DETAIL_INVALID_CAR_ID_ERROR' => 'Invalid car detail request. Please go back and try again.',
    'CAR_DETAIL_HEADER' => 'Car information',
    'CAR_DETAIL_CAR_QUERY_ERROR' => 'Unable to load car information due an internal error. Please notify the support.',
    'CAR_DETAIL_NO_CAR_AVAILABLE_LABEL' => 'The car you are trying to access is not registered or has been sold. Please go back and try again.',
    'CAR_DETAIL_GOBACK_LABEL' => 'Go back',
    'CAR_DETAIL_INTERIOR_TAB_LABEL' => 'Interior',
    'CAR_DETAIL_EXTERIOR_TAB_LABEL' => 'Exterior',
    'CAR_DETAIL_VIDEO_TAB_LABEL' => 'Videos',
    'CAR_DETAIL_VIDEO_TAB_NOVIDEO_LABEL' => 'There is no video available to this vehicle.',
    'CAR_DETAIL_FEATURES_HEADER' => 'specifications',
    'CAR_DETAIL_RESERVECAR_LOGIN_REQUIRED_LABEL' => 'You must login before you can reserve this vehicle.',
    'CAR_DETAIL_ADD_TO_FAV_ERROR' => 'Unable to add vehicle to favorite list due an internal error.',
    'CAR_DETAIL_ADD_TO_FAV_SUCCESS' => 'This vehicle has been add to your favorite list successfully!',
    'CAR_DETAIL_ADD_TO_FAV_LOGIN_ERROR' => 'You must login before adding vehicles to your favorite list.',
    'CAR_DETAIL_ADD_TO_FAV_BUTTON' => 'Add to favorites',
    'CAR_DETAIL_RESERVE_CAR_BUTTON' => 'Reserve this car',
    'CAR_DETAIL_FEATURES_ICON_TAB_LABEL' => 'Features list',
    'CAR_DETAIL_MORE_DETAILS_TAB_LABEL' => 'More details',
    'CAR_DETAIL_SPECIFICATION_TAB_LABEL' => 'Specifications',
    'CAR_DETAIL_BODY_TYPE_UNKNOWN' => '(body type undefined)',
    'CAR_DETAIL_NODETAILS_LABEL' => 'There are no specifications available at the moment.',
    'CAR_DETAIL_UNSPECIFIED_LABEL' => 'Not specified',
    'CAR_DETAIL_ENGINE_SIZE_LABEL' => 'Engine size',
    'CAR_DETAIL_TRIM_LABEL' => 'Trim',
    'CAR_DETAIL_TYPE_LABEL' => 'Type',
    'CAR_DETAIL_GEAR_LABEL' => 'Gear box',
    'CAR_DETAIL_FUEL_LABEL' => 'Fuel',
    'CAR_DETAIL_COLOR_LABEL' => 'Color',
    'CAR_DETAIL_PREV_OWNER_LABEL' => 'Previous Owner(s)',
    'CAR_DETAIL_LAST_SERVICE_LABEL' => 'Last service',
    'CAR_DETAIL_MOT_LABEL' => 'MOT',
    'CAR_DETAIL_TAB_BAND_LABEL' => 'Tax band',
    'CAR_DETAIL_TOP_SPEED_LABEL' => 'Top speed',
    'CAR_DETAIL_ENGINE_RPM' => 'Engine torque (rpm)',
    'CAR_DETAIL_ENGINE_KW' => 'KW',
    'CAR_DETAIL_TRANSMISSION_TYPE' => 'Transmission type',
    'CAR_DETAIL_DOORS_LABEL' => 'door(s)',
    'CAR_DETAILS_ECO_LABEL' => 'Energy class',
    'CAR_DETAILS_PRINT_PAGE_LABEL' => 'Print this page'  
);