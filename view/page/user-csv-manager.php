<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}
	
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE. test For CK
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '_ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    //$invalidProduct = false;
    // get lists.
    
 //    if(!isset($_GET['cid'])) $getId='';
	// else $getId=$_GET['cid'];
	// $product=array();
	
    // get generated car id.
	// if(empty($getId)){
 //    	$carid = $_CLASS->getCarId();
	// }else{
	// 	$carid = htmlspecialchars($getId);
		
 //        //$invalidProduct=$_CLASS->invalidProduct;
	// }
	
	
		
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/datepicker.css" />
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/bootstrap-datepicker.js"></script>

        <div id="content-wrapper">
            <link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/mywini/mywini.css" type="text/css"/>
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Register Item</a> > Car</p>
            </div>
                <?php include("php/sidebar/my-wini.php");?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                 <div class="page-title">
                	<p><?php echo $_LANG['PAGE_TITLE'];?>
                </div>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                
                <form id="form-wrap" method="post">
                <!----------------- FORM CONTAINER -------------------->
                    
                    <br>
                   
                    <br>
                    <div class="body_wrapper">
                        <table>
                            <tr>
                                <td class="label-td"><label><?php echo $_LANG['PAGE_PRODUCT_TYPE_LABEL'];?></label></td>
                                
                                <td class="input-td">
                                    <select name="productTypeInputSelect">
                                        <option value="Car">Car</option>
                                        <option value="Truck">Truck</option>
                                        <option value="Bus">Bus</option>
                                        <option value="Equipment">Heavy Machine</option>
                                        <option value="Part">Part</option>

                                        <option value="Aircraft">Aircraft</option>
                                        <option value="Ship">Ship</option>
                                        <option value="Motorbike">Motorbike</option>
                                        

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="label-td"><label><?php echo $_LANG['PAGE_CSV_FILE_LABEL'];?></label></td>
                                
                                <td class="input-td">
                                   <input class="user-input" type="file" name="file" id="file"/>
                                </td>
                            </tr>
                            <tr><td colspan="2"></td></tr>
                        </table>
                        
                        <input style="margin:0px 0px 20px 10px;" id="submitbtn" style="" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['PAGE_IMPORT_BUTTON'];?>" />
                        <!-------------- END UPLOAD MEDIA BLOCK ---------->
                        <!----------------- END FORM CONTAINER -------------------->
                    </div>
                </form>
                <!-- END CONTENT -->
                        
                        
                        <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    </div>
                  
                    <div class="clearfix"></div>
                    <div id="content2">
                        
            </div>
            <div class="clearfix"></div>
        </div>
 
        
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}