<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feeds.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // initialize lists.
    require_once BASE_CLASS . 'class-language.php';
    $langclass = new LanguageFormat();
    
    $country_list = $langclass->getCountryList();
    $setting_list = $_CLASS->getSettingList();
    $installed_language_packs = $langclass->getInstalledLanguagePacks();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <form method="post" enctype="multipart/form-data">
                    <h1><?php echo $_LOCAL['ADMINSETTING_FRAMEWORK_HEADER'];?></h1>
                    <?php
                            // display from feed.
                       if( $fstatus ){
                   ?>
                   <div class="alert <?php echo $fstyle;?>">
                       <button type="button" class="close" data-dismiss="alert">×</button>
                       <?php echo $fmessage;?>
                   </div>
                   <?php
                       }
                    ?>
                    <h2><?php echo $_LOCAL['ADMINSETTING_SITEOPTIONS_HEADER'];?></h2>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_WEBSITE_NAME_LABEL'];?></span>
                        <input type="text" name="fwNameInput" style="width:555px;" placeholder="<?php echo $_LOCAL['ADMINSETTING_WEBSITE_NAME_PLACEHOLDER'];?>" value="<?php echo $setting_list['fwname'];?>"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_DEFAULT_LANGUAGE_LABEL'];?></span>
                        <select name="languageInput" style="width:568px;">
                            <optgroup label="<?php echo $_LOCAL['ADMINSETTING_OPTION_SELECTED'];?>">
                                <option value="<?php echo $setting_list['language'];?>"><?php echo ucfirst($setting_list['language']);?></option>
                            </optgroup>
                            <optgroup label="<?php echo $_LOCAL['ADMINSETTING_OPTION_OPTIONS'];?>">
                                <?php
                                    for( $i=0; $i < count($installed_language_packs); $i++ )
                                    {
                                        $lan = $langclass->getLanguageNameByISO($installed_language_packs[$i]);
                                        $lan = strtolower($lan);
                                ?>
                                <option value="<?php echo $lan;?>"><?php echo $langclass->getLanguageNameByISO($installed_language_packs[$i]);?></option>
                                <?php
                                    }
                                ?>
                            </optgroup>
                        </select>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_DEFAULT_HOMESLUG_LABEL'];?></span>
                        <select name="defaultHomeInput" style="width:568px;">
                            <optgroup label="<?php echo $_LOCAL['ADMINSETTING_OPTION_SELECTED'];?>">
                                <option value="<?php echo $setting_list['default_home_slug'];?>"><?php echo ucfirst($setting_list['default_home_slug']);?></option>
                            </optgroup>
                            <optgroup label="<?php echo $_LOCAL['ADMINSETTING_OPTION_OPTIONS'];?>">
                                <option value="home">Theme Home</option>
                                <?php
                                    $page_list = $_CLASS->getPageList();

                                    for( $i=0; $i < count($page_list); $i++ )
                                    {
                                ?>
                                <option value="<?php echo $page_list[$i]['page_slug'];?>"><?php echo $page_list[$i]['page_title'];?></option>
                                <?php
                                    }
                                ?>
                            </optgroup>
                        </select>
                    </div>
                    <br>
                    <input type="submit" name="siteOptionInput" value="<?php echo $_LOCAL['ADMINSETTING_SITEOPTIONS_BUTTON_LABEL'];?>" class="btn btn-info btn-small" />
                    <br>
                    <h2><?php echo $_LOCAL['ADMINSETTING_CURRENCY_HEADER'];?></h2>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_DEFAULT_CURRENCY'];?></span>
                        <select name="currencyInput" style="width:568px;">
                            <optgroup label="<?php echo $_LOCAL['ADMINSETTING_OPTION_SELECTED'];?>">
                                <option value="<?php echo $_SESSION['CURRENCY_SYMBOL'];?>"><?php echo $_SESSION['CURRENCY_CODE'];?> <?php echo $_SESSION['CURRENCY_SYMBOL'];?></option>
                            </optgroup>
                            <optgroup label="<?php echo $_LOCAL['ADMINSETTING_OPTION_OPTIONS'];?>">
                                <?php
                                    // display currency options.
                                    $currency_option = $_CLASS->getCurrencyOptionList();
                                    
                                    foreach( $currency_option as $k => $v ){
                                        $country = explode('-',$k);
                                        $code    = $country[1];
                                        $country = $country[0];
                                ?>
                                <option value="<?php echo $code;?>,<?php echo $v;?>"><?php echo $code . ' - ' . $v . ' (' . $country .')';?></option>
                                <?php
                                    }
                                ?>
                            </optgroup>
                        </select>
                    </div>
                    <br>
                    <input type="submit" name="currencyBtn" value="<?php echo $_LOCAL['ADMINSETTING_CURRENCY_BUTTON'];?>" class="btn btn-info btn-small"/>
                    <br>
                    <h2><?php echo $_LOCAL['ADMINSETTING_META_HEADER'];?></h2>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_META_COPYRIGHTS_LABEL'];?>*</span>
                        <input type="text" name="copyrightsInput" style="width:555px;" value="<?php echo $setting_list['meta_copyrights'];?>"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_META_CANONIC_LABEL'];?>*</span>
                        <input type="text" name="canonicInput" style="width:555px;" value="<?php echo $setting_list['meta_canonic'];?>"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_META_FAVICON_LABEL'];?></span>
                        <input type="file" name="faviconInput" style="width:555px;"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_GOOGLE_ANALYTICS_LABEL'];?></span>
                        <textarea name="googleInput" style="width:555px;height:90px;"><?php echo $setting_list['analytics'];?></textarea>
                    </div>
                    <input type="submit" name="metaInput" value="<?php echo $_LOCAL['ADMINSETTING_META_BUTTON_LABEL'];?>" class="btn btn-info btn-small" />
                    <br>
                    <h2><?php echo $_LANG['ADMINSETTING_HEADER_RESERVATION_HEADER'];?></h2>
                    <?php
                        ( $setting_list['allow_car_reservation'] ? $rdcheck = 'checked="checked"' : $rdcheck = '' );
                    ?>
                    <label class="checkbox">
                    <input type="checkbox" name="reservationInput" <?php echo $rdcheck;?> value="1" />
                        <?php echo $_LOCAL['ADMINSETTING_HEADER_ALLOW_RESERVATION_LABEL'];?>
                    </label>
                    <br>
                    <input type="submit" name="reservationbtn" class="btn btn-info btn-small btn-small" value="<?php echo $_LANG['ADMINSETTING_HEADER_RESERVATION_BUTTON'];?>" />
                    <br>
                    <h2><?php echo $_LOCAL['ADMINSETTING_ADMIN_HEADER'];?></h2>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_ADMIN_EMAIL_LABEL'];?>*</span>
                        <input type="text" name="adminEmailInput" style="width:555px;" value="<?php echo $setting_list['admin_email'];?>"/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LOCAL['ADMINSETTING_ADMIN_PASSWORD_LABEL'];?>*</span>
                        <input type="password" name="adminPassInput" style="width:555px;" placeholder="<?php echo $_LOCAL['ADMINSETTING_ADMIN_PASSWORD_PLACEHOLDER'];?>"/>
                    </div>
                    <br>
                    <input type="submit" name="adminBtnInput" value="<?php echo $_LOCAL['ADMINSETTING_ADMIN_BUTTON_LABEL'];?>" class="btn btn-info btn-small" />
                    <br>
                    <h2><?php echo $_LOCAL['ADMINSETTING_LIVE_SUPPORT_HEADER'];?></h2>
                    <?php
                        // set checked boxes.
                        ( $setting_list['live_chat'] ? $liveChat = 'checked="checked"' : $liveChat = '' );
                    ?>
                    <label class="checkbox">
                        <input type="checkbox" name="liveChatInput" <?php echo $liveChat;?> value="yes" />
                        <?php echo $_LOCAL['ADMINSETTING_LIVE_SUPPORT_PLACEHOLDER'];?>
                    </label>
                    <br/>
                    <input type="submit" name="livechatBtnInput" value="<?php echo $_LOCAL['ADMINSETTING_LIVE_SUPPORT_BUTTON'];?>" class="btn btn-info btn-small" />
                    <br/>
                    <h2><?php echo $_LOCAL['ADMINSETTING_SOCIAL_HEADER'];?></h2>
                    <p><?php echo $_LOCAL['ADMINSETTING_SOCIAL_PLACEHOLDER'];?></p>
                    <label class="checkbox">
                        <?php
                            // get social bar status.
                            ( $setting_list['allow_social'] ? $social = 'checked="checked"' : $social = '' );
                        ?>
                        <input type="checkbox" name="socialbarInput" <?php echo $social;?> value="yes" />
                        <?php echo $_LOCAL['ADMINSETTING_SOCIAL_CHECKBOX_LABEL'];?>
                    </label>  
                    <br/>          
                    <input type="submit" name="socialbtn" value="<?php echo $_LOCAL['ADMINSETTING_SOCIAL_UPDATE_BUTTON'];?>" class="btn btn-small btn-info"/>
                    <br/>
                    <h2><?php echo $_LOCAL['ADMINSETTING_HEADER_HEADER'];?></h2>
                    <?php
                        // set checked boxes.
                        ( $setting_list['enable_top_header'] ? $th = 'checked="checked"' : $th = '' );
                        ( $setting_list['enable_menu_header'] ? $tm = 'checked="checked"' : $tm = '' );
                        ( $setting_list['enable_banner_header'] ? $tb = 'checked="checked"' : $tb = '' );
                        ( $setting_list['enable_foot_header'] ? $tf = 'checked="checked"' : $tf = '' );
                    ?>
                    <label class="checkbox">
                        <input type="checkbox" name="topHeaderInput" <?php echo $th;?> value="yes" />
                        <?php echo $_LOCAL['ADMINSETTING_HEADER_TOP_LABEL'];?>
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="menuHeaderInput" <?php echo $tm;?> value="yes" />
                        <?php echo $_LOCAL['ADMINSETTING_HEADER_MENU_LABEL'];?>
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="bannerHeaderInput" <?php echo $tb;?> value="yes" />
                        <?php echo $_LOCAL['ADMINSETTING_HEADER_BANNER_LABEL'];?>
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="footHeaderInput" <?php echo $tf;?> value="yes" />
                        <?php echo $_LOCAL['ADMINSETTING_HEADER_FOOTER_LABEL'];?>
                    </label>
                    <br>
                    <input type="submit" name="headerInput" value="<?php echo $_LOCAL['ADMINSETTING_HEADER_BUTTON_LABEL'];?>" class="btn btn-info btn-small" />
                    <!-- END CONTENT -->
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}