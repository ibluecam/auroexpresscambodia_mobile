<?php
    /*
     * Public menu header
     * Initialize variables to avoid notice warnings...
     */
     $_home = '';
     $_search = '';
     $_sell = '';
     $_finance = '';
     $_find = '';
     $_news = '';
     
     // set selected link.
     switch ($_PAGE->getPageSlug() )
     {
         case '':
         case DEFAULT_HOME_SLUG:
             $_home = 'class="selected-menu"';
             break;
         case 'search':
             $_search = 'class="selected-menu"';
             break;
         case 'sell-your-car':
             $_sell = 'class="selected-menu"';
             break;
         case 'financing':
             $_finance = 'class="selected-menu"';
             break;
         case 'find-us':
             $_find = 'class="selected-menu"';
             break;
         case 'news':
             $_news = 'class="selected-menu"';
             break;             
     }
?>
        
        <!--<div id="menu-header">
            <ul>
                <li>
                    <a href="<?php echo BASE_RELATIVE;?>" <?php echo $_home;?>><?php echo $_GLANG['PUBLIC_TOPMENU_HOME_LABEL'];?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_RELATIVE;?>search/" <?php echo $_search;?>><?php echo $_GLANG['PUBLIC_TOPMENU_BROWSE_LABEL'];?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_RELATIVE;?>sell-your-car/" <?php echo $_sell;?>><?php echo $_GLANG['PUBLIC_TOPMENU_SELL_CAR_LABEL'];?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_RELATIVE;?>financing/" <?php echo $_finance;?>><?php echo $_GLANG['PUBLIC_TOPMENU_FINANCING_LABEL'];?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_RELATIVE;?>find-us/" <?php echo $_find;?>><?php echo $_GLANG['PUBLIC_TOPMENU_FINDUS_LABEL'];?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_RELATIVE;?>news/" <?php echo $_news;?>><?php echo $_GLANG['PUBLIC_TOPMENU_NEWS_LABEL'];?></a>
                </li>
            </ul>
        </div>-->
