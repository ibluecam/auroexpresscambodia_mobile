<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Send email using pre-formatted template.
 */
class UserSendEmail
{
    protected $template_data;
    protected $has_data;
    protected $from;
    protected $to;
    protected $subject;
    protected $to_company_name;
    protected $model_year;
    protected $model;
    protected $make;
    protected $part_name;
    protected $id;
    protected $message;
    protected $product_image;
	protected $message_attachment_container;

    protected $email_body="";

    protected $from_company_name;
    protected $from_address;
    protected $from_profile_image;
    protected $from_profile_link;
    protected $from_profile_container;

    /*
     * Constructor -------------------------------------------------------------
     */
    public function __construct()
    {
        // load template source.
        $path = BASE_ROOT . 'view/email/template_product_inquiry.html';

        $this->template_data = '';
        $this->has_data = false;

        if(file_exists($path) )
        {
            if( $fo = @fopen($path, 'r') )
            {
                if( $this->template_data = @fread($fo, filesize($path)) )
                {
                    $this->has_data = true;
                }

                @fclose($fo);
            }
        }
    }

    /*
     * Public method: send email -----------------------------------------------
     * @return bool.
     */
    public function send()
    {
        if(empty($this->to) || empty($this->from))
        {
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Email not send. Required parameter email was not set. Error thrown at ' . __FILE__ . ':' . __LINE__ .'.');

            return false;
        }
        else
        {
            $email_body = '';

            // if template exists, use it.
            if( !empty($this->template_data) )
            {
                $email_body = '<html><head><title>MotorBB</title></head><body>';
                $email_body .= $this->template_data;
                /* Product container */
                if(!empty($this->id)){
                    $email_body = str_replace('[:::PRODUCT_CONTAINER:::]', $this->getProductContainer(), $email_body);
                    if(!empty($this->model_year))
                        $email_body = str_replace('[:::MODEL_YEAR:::]', $this->model_year, $email_body);
                    else
                        $email_body = str_replace('[:::MODEL_YEAR:::]', '', $email_body);
                    $email_body = str_replace('[:::PART_NAME:::]', $this->part_name, $email_body);
                    $email_body = str_replace('[:::MODEL:::]', $this->model, $email_body);
                    $email_body = str_replace('[:::MAKE:::]', $this->make, $email_body);
                    $email_body = str_replace('[:::PRODUCT_IMAGE:::]', $this->product_image, $email_body);
                    $email_body = str_replace('[:::ID:::]', $this->id, $email_body);
                }else{
                    $email_body = str_replace('[:::PRODUCT_CONTAINER:::]', '', $email_body);
                }
                /* Message body */
                $email_body = str_replace('[:::TO_COMPANY_NAME:::]', $this->to_company_name, $email_body);
                $email_body = str_replace('[:::EMAIL_FROM:::]', $this->from, $email_body);
                $email_body = str_replace('[:::SUBJECT:::]', $this->subject, $email_body);
                $email_body = str_replace('[:::MESSAGE:::]', $this->message, $email_body);
                if(!empty($this->message_attachment_container)){
                    $email_body = str_replace('[:::ATTACHMENT_CONTAINER:::]', $this->getAttachmentContainer(), $email_body);
                    $email_body = str_replace('[:::ATTACHMENT:::]', $this->message_attachment_container, $email_body);
                }else{
                    $email_body = str_replace('[:::ATTACHMENT_CONTAINER:::]', '', $email_body);
                }
                /* From detail */
                $email_body = str_replace('[:::FROM_PROFILE_CONTAINER:::]', $this->from_profile_container, $email_body);

                if(!empty($this->from_profile_image)){
                    $email_body = str_replace('[:::FROM_PROFILE_IMAGE_CONTAINER:::]', $this->getFromProfileImageContainer(), $email_body);
                    $email_body = str_replace('[:::FROM_PROFILE_IMAGE:::]', $this->from_profile_image, $email_body);
                }else{
                    $email_body = str_replace('[:::FROM_PROFILE_IMAGE_CONTAINER:::]', '', $email_body);

                }
                $email_body = str_replace('[:::FROM_PROFILE_LINK:::]', $this->from_profile_link, $email_body);
                $email_body .= '</body></html>';

            }
            // no template loaded.
            // else
            // {
            //     $email_body = '<html><head><title>MotorBB</title></head><body>';
            //     $email_body .= $this->message;
            //     $email_body .= '</body></html>';
            // }

            // set email headers.
            $this->email_body=$email_body;
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

            $headers .= 'From: Motorbb <noreply@motorbb.com>' . "\r\n";
            $headers .= 'Reply-To:'.$this->from_company_name.' - Motorbb.com<'.$this->from.'>' . "\r\n";
            $headers .= 'X-Mailer:Welcome to Mototbb.com';

            if(!mail($this->to, $this->subject, $email_body, $headers))
            {
                require_once BASE_CLASS . 'class-log.php';
                LogReport::write('Unable to send email to ['.$this->to.']. Error thrown at ' . __FILE__ . ':' . __LINE__ .'.');

                return false;
            }
            else
            {
                return true;
            }
        }
    }

    private function getAttachmentContainer(){
        return '<hr style="margin-bottom:5px;" color="#ddd" size="1"/><div>[:::ATTACHMENT:::]</div>';
    }
    /**
    * Get product container html if product id is set
    ** @return string
    */
    private function getProductContainer(){
        return '<td rowspan="2" style="width:150px;background:white;padding:10px;border:1px solid #ddd;;vertical-align:top;">
                    <a href="https://motorbb.com/vehicle-detail?cid=[:::ID:::]">
                        [:::PART_NAME:::]
                        [:::MODEL_YEAR:::]
                        [:::MODEL:::]<br/>
                        [:::MAKE:::]
                        <hr size="2" color="#0664B0"/>
                        <img src="[:::PRODUCT_IMAGE:::]"/>
                        <hr size="1" color="#ccc"/>
                        Stock ID: [:::ID:::]
                    </a>
                </td>';
    }
    /**
    * Get product container html if photo url is not empty
    ** @return string
    */
    private function getFromProfileImageContainer(){
        return '<div style="float:left;">
                    <a href="[:::FROM_PROFILE_LINK:::]"><img style="width:100%;border:1px solid #ddd;" src="[:::FROM_PROFILE_IMAGE:::]"/></a>
                </div>';
    }
    /**
    * SET message Attachment HTML
    */
    public function setMessageAttachmentContainer($html){
        $this->message_attachment_container=$html;
    }
    /**
    * Get profile detail if each detail is not empty
    */
    public function setFromProfileContainer($fields = array()){
        $container_html="<table>";
        foreach($fields as $key=>$value){
            if(!empty($value)){
                $container_html.="<tr>";
                $container_html.="<td style='padding:2px'><small>$key </small></td>";
                $container_html.="<td style='padding:2px'><small>: $value</small></td>";
                $container_html.="</tr>";
            }
        }
        $container_html.="</table>";
        $this->from_profile_container=$container_html;
    }
    /**
    * Get output email body (debug only)
    ** @return string
    */
    public function viewMessage(){
        return $this->email_body;
    }
    /*
     * Set message -------------------------------------------------------------
     * @param $message - the message to be sent.
     * @return void.
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /*
     * Set subject -------------------------------------------------------------
     * @param $subject - the subject string.
     * @return void.
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /*
     * Set email destination ---------------------------------------------------
     * @param $email - email address to be sent.
     * @return void.
     */
    public function setEmail($email)
    {
        $this->to = $email;
    }
    /* Receiver's Company Name */
    public function setToCompanyName($to_company_name)
    {
        $this->to_company_name = $to_company_name;
    }
    /* Sender's email */
    public function setEmailFrom($from)
    {
        $this->from = $from;
    }

    public function setModelYear($model_year)
    {
        $this->model_year = $model_year;
    }
    public function setModel($model)
    {
        $this->model = $model;
    }
    public function setMake($make)
    {
        $this->make = $make;
    }
    public function setPartName($part_name)
    {
        $this->part_name = $part_name;
    }
    /* ID of product */
    public function setId($id)
    {
        $this->id = $id;
    }
    public function setProductImage($product_image)
    {
        $this->product_image = $product_image;
    }

	/* Sender's company name */
    public function setFromCompanyName($from_company_name){
        $this->from_company_name=$from_company_name;
    }

    // public function setFromAddress($from_address){
    //     $this->from_address=$from_address;
    // }

    /* Sender's company image  */
    public function setFromProfileImage($from_profile_image){
        $this->from_profile_image = $from_profile_image;
    }
    /* Sender's company detail url  */
    public function setFromProfileLink($from_profile_link){
        $this->from_profile_link = $from_profile_link;
    }

}
