var current_product_type="";
var current_offset="";

    $(document).ready(function(e) {

    		//Button product type row
		//set selected category
		    function clear_select(){
  				$(".current").removeClass('current');
				$('.remove_silver').css('z-index','2');
				$('.remove_color').css('z-index','0');
			}
		   clear_select();
		    $("#search_section").load("ajax/my-inventory/quick_search/all.php", function() {

             		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type="+product_type+"&owner="+owner, function() {
		     			hideLoading();
		     			$("img.lazy").lazyload({
				               effect : "fadeIn"
				         });
		     		});
            });





			if(product_type=='car'){
				clear_select();
  				$("ul li[title=car]").addClass('current');
				$("ul li[title=car]").find('p').addClass('current');
			    $("ul li[title=car]").find('.car_img_silver').css('z-index','0');
				$("ul li[title=car]").find('.car_img_color').css('z-index','2');
			}
			if(product_type=='suv'){
				clear_select();
  				$("ul li[title=suv]").addClass('current');
				$("ul li[title=suv]").find('p').addClass('current');
			    $("ul li[title=suv]").find('.suv_img_silver').css('opacity','0');
				$("ul li[title=suv]").find('.suv_img_color').css('z-index','2');
			}
			if(product_type=='van'){
				clear_select();
  				$("ul li[title=van]").addClass('current');
				$("ul li[title=van]").find('p').addClass('current');
			    $("ul li[title=van]").find('.van_img_silver').css('opacity','0');
				$("ul li[title=van]").find('.van_img_color').css('z-index','2');
			}
			if(product_type=='pickup'){
				clear_select();
  				$("ul li[title=pickup]").addClass('current');
				$("ul li[title=pickup]").find('p').addClass('current');
			    $("ul li[title=pickup]").find('.pickup_img_silver').css('opacity','0');
				$("ul li[title=pickup]").find('.pickup_img_color').css('z-index','2');
			}
			if(product_type=='truck'){
				clear_select();
  				$("ul li[title=truck]").addClass('current');
				$("ul li[title=truck]").find('p').addClass('current');
			    $("ul li[title=truck]").find('.truck_img_silver').css('opacity','0');
				$("ul li[title=truck]").find('.truck_img_color').css('z-index','2');
			}
			if(product_type=='bus'){
				clear_select();
  				$("ul li[title=bus]").addClass('current');
				$("ul li[title=bus]").find('p').addClass('current');
			    $("ul li[title=bus]").find('.bus_img_silver').css('opacity','0');
				$("ul li[title=bus]").find('.bus_img_color').css('z-index','2');
			}
			if(product_type=='part'){
				clear_select();
  				$("ul li[title=part]").addClass('current');
				$("ul li[title=part]").find('p').addClass('current');
			    $("ul li[title=part]").find('.part_img_silver').css('opacity','0');
				$("ul li[title=part]").find('.part_img_color').css('z-index','2');
			}
			if(product_type=='accessories'){
				clear_select();
  				$("ul li[title=accessory]").addClass('current');
				$("ul li[title=accessory]").find('p').addClass('current');
			    $("ul li[title=accessory]").find('.accessories_img_silver').css('opacity','0');
				$("ul li[title=accessory]").find('.accessories_img_color').css('z-index','2');
			}
			if(product_type=='equipment'){
				clear_select();
  				$("ul li[title=equipment]").addClass('current');
				$("ul li[title=equipment]").find('p').addClass('current');
			    $("ul li[title=equipment]").find('.equipment_img_silver').css('opacity','0');
				$("ul li[title=equipment]").find('.equipment_img_color').css('z-index','2');
			}
			if(product_type=='watercraft'){
				clear_select();
  				$("ul li[title=watercraft]").addClass('current');
				$("ul li[title=watercraft]").find('p').addClass('current');
			    $("ul li[title=watercraft]").find('.watercraft_img_silver').css('opacity','0');
				$("ul li[title=watercraft]").find('.watercraft_img_color').css('z-index','2');
			}
			if(product_type=='aircraft'){
				clear_select();
  				$("ul li[title=aircraft]").addClass('current');
				$("ul li[title=aircraft]").find('p').addClass('current');
			    $("ul li[title=aircraft]").find('.aircraft_img_silver').css('opacity','0');
				$("ul li[title=aircraft]").find('.aircraft_img_color').css('z-index','2');
			}
			if(product_type=='motorbike'){
				clear_select();
  				$("ul li[title=motorbike]").addClass('current');
				$("ul li[title=motorbike]").find('p').addClass('current');
			    $("ul li[title=motorbike]").find('.motorbike_img_silver').css('opacity','0');
				$("ul li[title=motorbike]").find('.motorbike_img_color').css('z-index','2');
			}

		// End set selected category
    		$("#search_section").load("ajax/my-inventory/quick_search/all.php", function() {
        		current_offset='0';
        		$("#search_wrap form #owner").val(owner);
		        $("#search_wrap form #offset").val(current_offset);
        		functionsInSearch();

        	});


    		$('#my_item ul.item li').click(function(){
				var title = $(this).attr('title');
				$('#my_item ul.item li img').css('z-index','1');
				$('#my_item ul.item li img.'+title).css('z-index','1');
				$('#my_item ul.item li').removeClass('current');
				$('#my_item ul.item li p').removeClass('current');
				$(this).addClass('current');
				$(this).find('p').addClass('current');

			});


			//Start up search result with all product types
			showLoading();

    		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?owner="+owner, function() {
    			hideLoading();
    			$("img.lazy").lazyload({
		              effect : "fadeIn"
		        });
		        //count checkbox on startup
    			var countCheckedDelete = $('input.deleteCheck:checked').length;
				if(countCheckedDelete<=0) $('#delete_selected').attr("disabled", "disabled"); else $('#delete_selected').removeAttr('disabled');
    		});
    		//Event checkbox change
    		$(document.body).on("change",".deleteCheck", function(e){
    			var countCheckedDelete = $('input.deleteCheck:checked').length;
    			if(countCheckedDelete<=0) $('#delete_selected').attr("disabled", "disabled"); else $('#delete_selected').removeAttr('disabled');
    		});
    		//Ajax events for deleteButton click
    		$(document.body).on('click', ".deleteButton", function(e){
    			//reset all checks if individual deletion

				if (confirm('Are you sure want to delete this?')) {
					$(".deleteCheck").prop('checked', false);
					var delete_id=$(this).attr('delete_id');
					$("#item_check_"+delete_id).prop('checked', true);
	    			processDelete();
	    		}
				return false;
    		});
    		//Ajax events for delete selected button click
    		$(document.body).on('click', "#delete_selected", function(e){
    			//reset all checks if individual deletion
				if (confirm('Are you sure want to delete the selected items?')) {
	    			processDelete();
	    		}
    		});
    		//delete_selected
    		//Ajax events for pagination click
    		$(document.body).on('click', "#pagernation a", function(e){
		        current_offset=$(this).attr('offset');
		        reloadMyInventory(current_offset);
		        return false;
    		});
    		//Reservd select
    		$(document.body).on('click', "#Reserved", function(e){
    			//reset all checks if individual deletion

	    			processupdate();



    		});
    		//Sold select
    		$(document.body).on('click', "#Sold", function(e){
    			//reset all checks if individual deletion
	    			processsold();
    		});
    			//Default select
    		$(document.body).on('click', "#Default", function(e){
    			//reset all checks if individual deletion

	    			processDefault();


    		});

    		//Ajax events for search click
    		$(document.body).on('click', "#submit_search", function(e){
		        //alert();
		        reloadMyInventory('0');
		       	return false;
    		});
    		//If user click tab car it will load car data
            $('#btn_car').click(function(event) {
            	showLoading();
				clear_select();
  				$("ul li[title=car]").addClass('current');
				$("ul li[title=car]").find('p').addClass('current');
			    $("ul li[title=car]").find('.car_img_silver').css('z-index','0');
				$("ul li[title=car]").find('.car_img_color').css('z-index','2');

            	$("#search_section").load("ajax/my-inventory/quick_search/car.php", function() {

            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Car&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
			$('#btn_suv').click(function(event) {
            	showLoading();
				clear_select();
  				$("ul li[title=suv]").addClass('current');
				$("ul li[title=suv]").find('p').addClass('current');
			    $("ul li[title=suv]").find('.suv_img_silver').css('z-index','0');
				$("ul li[title=suv]").find('.suv_img_color').css('z-index','2');

            	$("#search_section").load("ajax/my-inventory/quick_search/suv.php", function() {

            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Suv&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
			$('#btn_van').click(function(event) {
            	showLoading();
				clear_select();
  				$("ul li[title=van]").addClass('current');
				$("ul li[title=van]").find('p').addClass('current');
			    $("ul li[title=van]").find('.van_img_silver').css('z-index','0');
				$("ul li[title=van]").find('.van_img_color').css('z-index','2');

            	$("#search_section").load("ajax/my-inventory/quick_search/van.php", function() {

            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Van&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
			$('#btn_pickup').click(function(event) {
            	showLoading();
				clear_select();
  				$("ul li[title=pickup]").addClass('current');
				$("ul li[title=pickup]").find('p').addClass('current');
			    $("ul li[title=pickup]").find('.pickup_img_silver').css('z-index','0');
				$("ul li[title=pickup]").find('.pickup_img_color').css('z-index','2');

            	$("#search_section").load("ajax/my-inventory/quick_search/pickup.php", function() {

            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Pickup&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
            $('#btn_truck').click(function(event) {

				clear_select();
  				$("ul li[title=truck]").addClass('current');
				$("ul li[title=truck]").find('p').addClass('current');
			    $("ul li[title=truck]").find('.truck_img_silver').css('z-index','0');
				$("ul li[title=truck]").find('.truck_img_color').css('z-index','2');

            	showLoading();
            	current_product_type="Truck";
            	$("#search_section").load("ajax/my-inventory/quick_search/truck.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Truck&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
            $('#btn_bus').click(function(event) {

				clear_select();
  				$("ul li[title=bus]").addClass('current');
				$("ul li[title=bus]").find('p').addClass('current');
			    $("ul li[title=bus]").find('.bus_img_silver').css('z-index','0');
				$("ul li[title=bus]").find('.bus_img_color').css('z-index','2');

            	showLoading();
            	current_product_type="Bus";
            	$("#search_section").load("ajax/my-inventory/quick_search/bus.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Bus&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
            $('#btn_auto_part').click(function(event) {

				clear_select();
  				$("ul li[title=part]").addClass('current');
				$("ul li[title=part]").find('p').addClass('current');
			    $("ul li[title=part]").find('.part_img_silver').css('z-index','0');
				$("ul li[title=part]").find('.part_img_color').css('z-index','2');

            	showLoading();
            	current_product_type="Part";
            	$("#search_section").load("ajax/my-inventory/quick_search/part.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Part&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
            $('#btn_auto_accessories').click(function(event) {

				clear_select();
  				$("ul li[title=accessory]").addClass('current');
				$("ul li[title=accessory]").find('p').addClass('current');
			    $("ul li[title=accessory]").find('.accessories_img_silver').css('z-index','0');
				$("ul li[title=accessory]").find('.accessories_img_color').css('z-index','2');

            	showLoading();
            	current_product_type="Accessories";
            	$("#search_section").load("ajax/my-inventory/quick_search/accessories.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Accessories&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
            $('#btn_equipment').click(function(event) {

				clear_select();
  				$("ul li[title=equipment]").addClass('current');
				$("ul li[title=equipment]").find('p').addClass('current');
			    $("ul li[title=equipment]").find('.equipment_img_silver').css('z-index','0');
				$("ul li[title=equipment]").find('.equipment_img_color').css('z-index','2');

            	showLoading();
            	current_product_type="Equipment";
            	$("#search_section").load("ajax/my-inventory/quick_search/equipment.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Equipment&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });

            $('#btn_watercraft').click(function(event) {

				clear_select();
  				$("ul li[title=watercraft]").addClass('current');
				$("ul li[title=watercraft]").find('p').addClass('current');
			    $("ul li[title=watercraft]").find('.watercraft_img_silver').css('z-index','0');
				$("ul li[title=watercraft]").find('.watercraft_img_color').css('z-index','2');

            	showLoading();
            	current_product_type="Watercraft";
            	$("#search_section").load("ajax/my-inventory/quick_search/watercraft.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Watercraft&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
            $('#btn_aircraft').click(function(event) {

				clear_select();
  				$("ul li[title=aircraft]").addClass('current');
				$("ul li[title=aircraft]").find('p').addClass('current');
			    $("ul li[title=aircraft]").find('.aircraft_img_silver').css('z-index','0');
				$("ul li[title=aircraft]").find('.aircraft_img_color').css('z-index','2');

            	showLoading();
            	current_product_type="Aircraft";
            	$("#search_section").load("ajax/my-inventory/quick_search/aircraft.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Aircraft&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });
            $('#btn_motorbike').click(function(event) {

				clear_select();
  				$("ul li[title=motorbike]").addClass('current');
				$("ul li[title=motorbike]").find('p').addClass('current');
			    $("ul li[title=motorbike]").find('.motorbike_img_silver').css('z-index','0');
				$("ul li[title=motorbike]").find('.motorbike_img_color').css('z-index','2').css('opacity','1');

            	showLoading();
            	current_product_type="Motorbike";
            	$("#search_section").load("ajax/my-inventory/quick_search/motorbike.php", function() {
            		/* Act on the event */
            		functionsInSearch();
            		$("#carSourceWraper").load("ajax/my-inventory/load_section.php?product_type=Motorbike&owner="+owner, function() {
		    			hideLoading();
		    			$("img.lazy").lazyload({
				              effect : "fadeIn"
				        });
		    		});
            	});
				return false;
            });

		   	$('#model').change(function(e) {
					model=$(this).val();
	            });

			if( $('#condition').length ){
				if(condition!="") $("#condition").val(condition);
			}

			if(fuel_type!="") {
				$("#fuel_type").val(fuel_type);
			}

			if( $('#category').length ){
				loadSubCategory();
			}

			if( $('#make').length ){
				loadModel();
			}

			if( $('#steering').length ){
				if(steering!="") $("#steering").val(steering);
			}

			if( $('#country').length ){
				if(country!="") $('#country').val(country);
			}

			if( $('#year_to').length ){
				if(year_to!="") $('#year_to').val(year_to);
			}

			if( $('#year_from').length ){
				if(year_from!="") $('#year_from').val(year_from);
			}

			if( $('#car_id').length ){
				if(car_id!="") $('#car_id').val(car_id);
			}

			if( $('#customSearch').length ){
				if(customSearch!="") $('#customSearch').val(customSearch);
			}
    });
	//process Items Deletion
	function processDelete(){
		//convert all post element in form to ajax object
		var delete_p=$('#load_form').serializeArray();
	    $.ajax({
            type:"post",
            url: "ajax/my-inventory/delete.php",
            data: $.param(delete_p),
            success: function(data){
            	//do stuff after the AJAX calls successfully completes
            	 alert(data);
            	//count number of items that were checked to be deleted
    			var countCheckedDelete = $('input.deleteCheck:checked').length;

    			if((itemCountThisPage-countCheckedDelete<=0)&&current_offset>0){
    				current_offset-=itemPerPage;
    				reloadMyInventory(current_offset);
    			}else{
    				reloadMyInventory(current_offset);
    			}
            }
        });
	}
	function processupdate(){
		//convert all post element in form to ajax object
		var delete_p=$('#load_form').serializeArray();
	    $.ajax({
            type:"post",
            url: "ajax/my-inventory/update.php",
            data: $.param(delete_p),
            success: function(data){
            	reloadMyInventory($("#offset").val());
            	//do stuff after the AJAX calls successfully completes
            	// alert(data);
            	//count number of items that were checked to be deleted

            }
        });
	}
function processsold(){
		//convert all post element in form to ajax object
		var delete_p=$('#load_form').serializeArray();
	    $.ajax({
            type:"post",
            url: "ajax/my-inventory/sold.php",
            data: $.param(delete_p),
            success: function(data){
            		reloadMyInventory($("#offset").val());
            	//do stuff after the AJAX calls successfully completes
            	// alert(data);
            	//count number of items that were checked to be deleted

            }
        });
	}
	function processDefault(){
		//convert all post element in form to ajax object
		var delete_p=$('#load_form').serializeArray();
	    $.ajax({
            type:"post",
            url: "ajax/my-inventory/Default.php",
            data: $.param(delete_p),
            success: function(data){
            	reloadMyInventory($("#offset").val());
            	//do stuff after the AJAX calls successfully completes
            	// alert(data);
            	//count number of items that were checked to be deleted

            }
        });
	}


	//reload my inventory body function
	function reloadMyInventory(setOffset){
		//ajax loading item list
		showLoading();

        $("#search_wrap form #offset").val(setOffset);
		$("#search_wrap form #owner").val(owner);

        var search_filter=$("#search_wrap form").serializeArray();

        $.ajax({
            type:"get",
            url: "ajax/my-inventory/load_section.php",
            data: $.param(search_filter),
            success: function(data){
            //do stuff after the AJAX calls successfully completes
            	hideLoading();
            	$('#carSourceWraper').html(data);
            	$("img.lazy").lazyload({
		              effect : "fadeIn"
		        });

            }
        });

	}
	//all events will load within this function when product type buttons are clicked
	function functionsInSearch(){

		$('#make').change(function(e) {
			make=$('#make option:selected').val();
			model="";
            loadModel();
        });
        if( $('#category').length ){
				$('#category').change(function(e) {
					category=$('#category option:selected').val();
					loadSubCategory();
	            });
			}

		if( $('#category2').length ){
			$('#category2').change(function(e) {
				category2=$('#category2 option:selected').val();
				loadSubCategory();
            });
		}
		if( $('#category3').length ){
			$('#category3').change(function(e) {
				category3=$('#category3 option:selected').val();
				loadSubCategory();
            });
		}
	}

	function loadSubCategory(){
		if(category!=""){
			$("#category").val(category);

			if( $('#category2').length ){
				$('#category2').load('ajax/load_category2.php?category='+encodeURIComponent(category), function(e){
					if(category2!=""){
						$("#category2").val(category2);
						if( $('#category3').length ){
							$('#category3').load('ajax/load_category3.php?category2='+encodeURIComponent(category2), function(e){
								if(category3!="") $("#category3").val(category3);
							});
						}
					}
				});

			}
		}
	}
	function loadModel(){
		//if(make!=""){
			$("#make").val(make);

			if( $('#model').length ){
				$('#model').load('ajax/load_car_model.php?make='+encodeURIComponent(make)+'&product_type='+encodeURIComponent(product_type), function(e){
					if(model!="") $("#model").val(model);
				});

			}
		//}
	}


	function showLoading(){
		$("#loading_image").css("display", "block");
	}
	function hideLoading(){
		$("#loading_image").css("display", "none");
		//Show or hide mode button
		$('.listCar').mouseover(function() {
	    	$(this).find('.modeButton').css("display", "block");
	    }).mouseout(function(){
	    	$(this).find('.modeButton').css("display", "none");

	   	});
	}
