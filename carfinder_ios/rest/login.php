<?php

  require_once '../models/User.php';
  require '../controllers/ControllerUser.php';
  $controllerUser = new ControllerUser('../application/DB_Connect.php');



  if( !empty($_POST['username']) )
      $username = $_POST['username'];

  if( !empty($_POST['password']) )
      $password = md5($_POST['password']);

  if( !empty($_POST['facebook_id']) )
      $facebook_id = $_POST['facebook_id'];

  if( !empty($_POST['twitter_id']) )
      $twitter_id = $_POST['twitter_id'];

  if( !empty($username) && !empty($password) ) {
      
      $user = $controllerUser->loginUser($username, $password);
      if($user != null) {
          // update the hash
          $controllerUser->updateUserHash($user);
          $json = translateJSON($user);
      }
      else {

          $json = "{
                      \"status\" : {
                                    \"status_code\" : \"1\",
                                    \"status_text\" : \"Username/Password Invalid or you are being denied to access. Please try again.\"
                                  }
                  }";

      }

      echo $json;
  }

  else if( !empty($facebook_id) ) {

      $user = $controllerUser->loginFacebook($facebook_id);
      if($user != null) {
          // update the hash
          $controllerUser->updateUserHash($user);
          $json = translateJSON($user);
      }
      else {

          $json = "{
                      \"status\" : {
                                    \"status_code\" : \"2\",
                                    \"status_text\" : \"Invalid Login.\"
                                  }
                  }";

      }

      echo $json;
  }

  else if( !empty($twitter_id) ) {

      $user = $controllerUser->loginTwitter($twitter_id);
      if($user != null) {
          // update the hash
          $controllerUser->updateUserHash($user);
          $json = translateJSON($user);
      }
      else {

          $json = "{
                      \"status\" : {
                                    \"status_code\" : \"2\",
                                    \"status_text\" : \"Invalid Login.\"
                                  }
                  }";
      }

      echo $json;
  }
  else {

      $json = "{
                \"status\" : {
                              \"status_code\" : \"3\",
                              \"status_text\" : \"Invalid Access.\"
                            }
                }";

      echo $json;
  }



  function translateJSON($itm) {

      require_once '../models/Seller.php';
      require '../controllers/ControllerRestSeller.php';
      $controllerRestSeller = new ControllerRestSeller('../application/DB_Connect.php');

      require_once '../models/Dealer.php';
      require '../controllers/ControllerRestDealer.php';
      $controllerRestDealer = new ControllerRestDealer('../application/DB_Connect.php');

      $dealer = $controllerRestDealer->getUserDealerByUserId($itm->user_id);
      $seller = $controllerRestSeller->getUserSellerIfExist($itm->user_id);

      $dealerJSON = "\"dealer_info\" : null";
      if($dealer != null) {
          $dealerJSON = "
                          \"dealer_info\" : {
                                      \"dealer_id\" : \"$dealer->dealer_id\",
                                      \"lat\" : \"$dealer->lat\",
                                      \"lon\" : \"$dealer->lon\",
                                      \"contact_no\" : \"$dealer->contact_no\",
                                      \"email\" : \"$dealer->email\",
                                      \"fb\" : \"$dealer->fb\",
                                      \"twitter\" : \"$dealer->twitter\",
                                      \"website\" : \"$dealer->website\",
                                      \"address\" : \"$dealer->address\",
                                      \"dealer_name\" : \"$dealer->dealer_name\",
                                      \"details\" : \"$dealer->details\",
                                      \"sales_hours_monday_friday\" : \"$dealer->sales_hours_monday_friday\",
                                      \"sales_hours_saturday\" : \"$dealer->sales_hours_saturday\",
                                      \"sales_hours_sunday\" : \"$dealer->sales_hours_sunday\",
                                      \"service_hours_monday_friday\" : \"$dealer->service_hours_monday_friday\",
                                      \"service_hours_saturday\" : \"$dealer->service_hours_saturday\",
                                      \"service_hours_sunday\" : \"$dealer->service_hours_sunday\",
                                      \"user_id\" : \"$dealer->user_id\"
                                      }
                        ";
      }

      $sellerJSON = "\"seller_info\" : null";
      if($seller != null) {
          $sellerJSON = "
                      \"seller_info\" : {
                                    \"seller_id\" : \"$seller->seller_id\",
                                    \"seller_name\" : \"$seller->seller_name\",
                                    \"contact_no\" : \"$seller->contact_no\",
                                    \"email\" : \"$seller->email\",
                                    \"lat\" : \"$seller->lat\",
                                    \"lon\" : \"$seller->lon\",
                                    \"sms\" : \"$seller->sms\",
                                    \"address\" : \"$seller->address\",
                                    \"user_id\" : \"$seller->user_id\",
                                    \"profile_pic\" : \"$seller->profile_pic\"
                                    }
                    ";
      }



      $json = "{
                      \"user_info\" : {
                                    \"user_id\" : \"$itm->user_id\",
                                    \"username\" : \"$itm->username\",
                                    \"login_hash\" : \"$itm->login_hash\",
                                    \"facebook_id\" : \"$itm->facebook_id\",
                                    \"twitter_id\" : \"$itm->twitter_id\",
                                    \"full_name\" : \"$itm->full_name\",
                                    \"email\" : \"$itm->email\",
                                    },
                                    $dealerJSON,
                                    $sellerJSON,

                      \"status\" : {
                                    \"status_code\" : \"-1\",
                                    \"status_text\" : \"Success.\"
                                  }
                  }";

      return $json;
  }

?>