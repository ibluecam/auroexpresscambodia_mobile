<?php
/**
 * IMPORTANT
 * 
 * The list below defines the car's body type which can be translated to your 
 * language. DO NOT CHANGE THE ID ORDER OF EACH ROW!!!!! IF YOU DO, YOU WILL
 * SCREW IT UP!
 * 
 * ID(never change this) => body type name(you can change this)
 * 
 * If you need more types, just add another row. IE. '31' => 'New type'
 */
$_CAR_BODY = array(
    '0' => 'Not defined',
    '1' => 'Hatchback',
    '2' => 'Cabriolet',
    '3' => 'MPV',
    '4' => '4x4 Crossover',
    '5' => 'Coupe',
    '6' => 'Estate',
    '7' => 'Saloon',
    '8' => 'Van',
    '9' => 'Sedan',
    '10' => 'Station Wagon 2WD',
    '11' => 'SUV',
    '12' => 'Minivan',
    '13' => 'Convertible',
    '14' => 'Sport',
    '15' => 'Campervan',
    '16' => 'Coupe Quattro',
    '17' => 'Hatchback 4WD',
    '18' => 'Liftback',
    '19' => 'Pickup 4WD',
    '20' => 'Pickup 2WD',
    '21' => 'Truck 2WD',
    '22' => 'Truck 4WD',
    '23' => 'Roadster',
    '24' => 'Sedan AWD',
    '25' => 'Sedan Quattro',
    '26' => 'Station Wagon 4WD',
    '27' => 'Wagon AWD',
    '28' => 'Wagon 2WD',
    '29' => 'Wagon 4WD',
    '30' => 'Electric car',
    '31' => 'Crew Cab Pickup',
    '32' => 'Extended Cab Pickup',
    '33' => '4dr Car',
    '34' => 'Mini-van',
    '35' => 'Station Wagon',
    '36' => 'Sport Utility',
    '37' => '2dr Car',
    '38' => 'Mini-van, Passenger',
    '39' => 'Regular Cab Pickup',
    '40' => 'Mini-van, Cargo'
);
