<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class adminmembersettings {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $register_type_list;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminmembersettings($lang=array()){
        $this->lang = $lang;
        
        // handle remove news.
        if( isset($_POST['removebtn']) ){
            $this->handleForm();
        }
        
        //$this->handleDelete();

        // load news list.
        $this->loadRegisterTypeList();
    }
    
    
    // private function handleDelete(){
    //     require_once BASE_CLASS . 'class-connect.php';
        
    //     $cnx = new Connect();
    //     $cnx->open();
    //     if(isset($_POST['delete'])) $delete=mysql_real_escape_string(stripslashes($_POST['delete'])); else $delete='';
    //     if(!empty($delete)) {
    //         if(!$this->existMemberSetting($delete)){
    //             require_once BASE_CLASS . 'class-log.php'; 
    //             $this->form_message = $this->lang['PAGE_NOT_FOUND_QUERY_ERROR'];
    //             $this->form_status = true;
    //             $this->form_style = 'alert-warning';
    //             $cnx->close();
    //             return false;
    //         }
    //         if($delete=='1') {
    //             require_once BASE_CLASS . 'class-log.php'; 
    //             $this->form_message = "The default member type cannot be deleted. You may edit it only.";
    //             $this->form_status = true;
    //             $this->form_style = 'alert-warning';
    //             $cnx->close();
    //             return false;
    //         }
    //         if( !$sql = @mysql_query("DELETE FROM `register_types` WHERE `id`='$delete';") ){
    //             require_once BASE_CLASS . 'class-log.php'; 
    //             $this->form_message = $this->lang['PAGE_DELETE_QUERY_ERROR'];
    //             $this->form_status = true;
    //             $this->form_style = 'alert-warning';
    //             $cnx->close();
    //             return false;
    //         }
    //         $this->form_message =  $this->lang['PAGE_DELETE_QUERY_SUCCESS'];
    //         $this->form_status = true;
    //         $this->form_style = 'alert-success';
    //         @mysql_free_result($sql);
    //         $cnx->close();
    //     }
        

    //     @mysql_free_result($sql);
        
        
    // }
    private function existMemberSetting($id){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `register_types` WHERE `id`='$id' ") ){
            require_once BASE_CLASS . 'class-log.php'; 
            $cnx->close();
            return false;
        }
        
        if( @mysql_num_rows($sql) >0  ){
            return true;
        }

        @mysql_free_result($sql);
        $cnx->close();
        
    }
    /**
     * Private method: load news list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadRegisterTypeList(){
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        
        $cnx = new Connect();
        $cnx->open();
                
        // load news.
        if( !$sql = @mysql_query("SELECT * FROM `register_types` ORDER BY `code`") ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            
            LogReport::write('Unable to load register type list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->register_type_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
       
        while( $r = @mysql_fetch_assoc($sql) ){
            
            array_push($this->register_type_list,$r);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    
    /**
     * Public method: get news list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getRegisterTypeList(){
        return $this->register_type_list;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}