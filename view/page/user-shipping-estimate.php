<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$shippingInfoList=$_CLASS->getShippingInfoList();
$country_list = $_CLASS->getCountryList();
$vessel_type_list = $_CLASS->getVesselTypeList();
$line_list = $_CLASS->getLineList();
$pagination=$_CLASS->pagination_html;
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

if(isset($_SESSION['log_group'])) $log_group=$_SESSION['log_group']; else $log_group="";
if(isset($_SESSION['log_id'])) $log_id=$_SESSION['log_id']; else $log_id="";
$log_group;

?>


<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/mywini/mywini.css" />
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/pagination.css" />
<script>
    var countryLoading="<?php if(isset($_GET['countryLoadingSelect'])) echo $_GET['countryLoadingSelect'];?>";
    var countryDest="<?php if(isset($_GET['countryDestSelect'])) echo $_GET['countryDestSelect'];?>";
    var portLoading="<?php if(isset($_GET['portLoadingSelect'])) echo $_GET['portLoadingSelect'];?>";
    var portDest="<?php if(isset($_GET['portDestSelect'])) echo $_GET['portDestSelect'];?>";
    var vesselType="<?php if(isset($_GET['vesselTypeSelect'])) echo $_GET['vesselTypeSelect'];?>";
    var size="<?php if(isset($_GET['sizeSelect'])) echo $_GET['sizeSelect'];?>";
    var transitTime="<?php if(isset($_GET['transitTimeSelect'])) echo $_GET['transitTimeSelect'];?>";
    var line="<?php if(isset($_GET['transitTimeSelect'])) echo $_GET['transitTimeSelect'];?>";
</script>

 <div id="sectionContenWrapper">
        	<!--sectionSidebar-->
            <div id="topMenu">
            	<p><a href="#" class="linkfade">My Wini</a> > My Payment List</p>
            </div>
        	<?php include("php/sidebar/help.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="payment_list">
                	<p>My Freight Cost</p>
                </div>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <div id="checking">
                	<p>Here are shipping estimates submitted by Global iBlue members.Please search for shipping estimates.</p>
                </div>
                <div id="form_search">
                	<form action="" method="get">
                	<table border="0" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td>Country of Loading</td>
                            <td >
                                <select class="select_box" name="countryLoadingSelect" id="countryLoadingSelect">
                                    <option value="">- Select -</option>
                                    <?php
                                        if( count($country_list['cc']) > 0 ){
                                        
                                            for( $i=0; $i < count($country_list['cc']); $i++ ){
                                                if( !empty($country_list['country_name'][$i]) ){
                                        ?>
                                        <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                    <?php
                                                }
                                            }
                                        }
                                        else {
                                    ?>
                                        <option value="0"><?php echo $_LANG['PAGE_NOCOUNTRY_LABEL'];?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </td>
                            <td class="right_form">Country of Destination</td>
                            <td >
                                <select class="select_box" name="countryDestSelect" id="countryDestSelect">
                                    <option value="">- Select -</option>
                                    <?php
                                        if( count($country_list['cc']) > 0 ){
                                        
                                            for( $i=0; $i < count($country_list['cc']); $i++ ){
                                                if( !empty($country_list['country_name'][$i]) ){
                                        ?>
                                        <option value="<?php echo $country_list['cc'][$i];?>"><?php echo $country_list['country_name'][$i];?></option>
                                    <?php
                                                }
                                            }
                                        }
                                        else {
                                    ?>
                                        <option value="0"><?php echo $_LANG['PAGE_NOCOUNTRY_LABEL'];?></option>
                                    <?php
                                        }
                                    ?>
                                </select></td>
                        </tr>
                        <tr>
                        	<td>Port of Loading</td>
                            <td>
                                <select class="select_box" name="portLoadingSelect" id="portLoadingSelect">
                                    <option value="">- Select -</option>
                                </select>
                            </td>
                            <td class="right_form">Port of Destination</td>
                            <td>
                                <select class="select_box" name="portDestSelect" id="portDestSelect">
                                    <option value="">- Select -</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td>Vessel Type</td>
                            <td>
                                <select class="select_box" name="vesselTypeSelect" id="vesselTypeSelect">
                                    <option value="">- Select -</option>
                                    <?php 
                                        foreach($vessel_type_list as $vessel_type){
                                            echo "<option value='$vessel_type'>$vessel_type</option>";
                                        }
                                    ?>
                                    <option value="Other">Other</option>
                                </select>
                            </td>
                            <td class="right_form">Size</td>
                            <td><select  class="select_box" name="sizeSelect" id="sizeSelect"><option value="">- Select -</option></select></td>
                        </tr>
                    </table>
                    <div id="search_btn">
                    	<input type="submit" class="submit" value="" />
                    </div>
                    </form>
                </div>
                <div id="result">  
                    <div class="top_button_container" >

                        <?php if($log_group=="user"){?>
                            <a href="<?php echo BASE_RELATIVE;?>edit-shipping/"><img src="images/btnadd.gif"/></a>
                        <?php }?>
                    </div>              
                	<table border="0" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td class="loading blue_bg">Port of<br/>Loading</td>
                            <td class="destination blue_bg">Port of<br/>Destination</td>
                            <td class="type blue_bg">Vessel<br/>Type</td>
                            <td class="size blue_bg">Size</td>
                            <td class="freight blue_bg">Ocean Freight<br/>(USD)</td>
                            <td class="time blue_bg">Transit Time</td>
                            <td class="line blue_bg">Line</td>
                            <td class="written blue_bg">Written by</td>
                        </tr>
                        <?php 
                        if(count($shippingInfoList)>0){
                            foreach($shippingInfoList as $shippingInfo) {
                        
                        ?>
                        <tr>
                        	<td class="loading">
                            	<p><img src="images/flag/<?php echo $shippingInfo['country_loading'];?>.png" /> <?php echo $shippingInfo['cl_name'];?></p>
                                <span><?php echo $shippingInfo['port_loading'];?></span>
                            </td>
                            <td class="destination">
                            	<p><img src="images/flag/<?php echo $shippingInfo['country_dest'];?>.png" /> <?php echo $shippingInfo['cd_name'];?></p>
                                <span><?php echo $shippingInfo['port_dest'];?></span>
                            </td>
                            <td class="type"><?php echo $shippingInfo['vessel_type'];?></td>
                            <td class="size"><?php echo $shippingInfo['size'];?></td>
                            <td class="freight red_text"><?php echo number_format($shippingInfo['ocean_freight_price']);?></td>
                            <td class="time"><?php if($shippingInfo['transit_time']>0) echo $shippingInfo['transit_time'];?> days</td>
                            <td class="line"><?php echo $shippingInfo['line'];?></td>
                            <td class="written">
                                <?php 
                                    $written_date = $shippingInfo['written_date'];
                                    $written_date = new DateTime($written_date);
                                    echo "<a href='#'>";
                                    echo $shippingInfo['author']."</a><br/>";
                                    echo $written_date->format('M d, Y');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" style="text-align: left; padding: 5px;">
                                <?php if($log_id==$shippingInfo['owner']){?>   
                                    <a href="<?php echo BASE_RELATIVE.'edit-shipping/?cid='.$shippingInfo['id'];?>" style="font-weight: bold; padding: 3px 10px; background-color: #808080; color: white; margin-top: -3px; float:right;">Edit</a>
                                    <a href="<?php echo BASE_RELATIVE.'shipping-estimate?delete='.$shippingInfo['id'];?>" style="font-weight: bold; padding: 3px 10px; background-color: #B20000; color: white; margin-top: -3px; margin-right:5px; float:right;">Delete</a>
                                <?php } ?>
                                <?php if(!empty($shippingInfo['remark'])){ ?>
                                    <?php echo "<font color='#AD0000'>Remark</font> : ".$shippingInfo['remark'];?>
                                <?php } else echo "<br/>";?>
                                
                            </td></tr>
                        <tr><td colspan="8" style="padding: 5px; background-color: rgb(221, 221, 221);"></td></tr>
                        <?php 
                            }
                        }
                        else{
                        ?>
                            <tr>
                            
                            <td colspan="8" class="written">
                                <font color="#8E0000">Sorry! No shipping found at the moment.</font>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </table>
                    <?php 
                        echo $pagination;
                        
                    ?>
                </div>
                
               
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  