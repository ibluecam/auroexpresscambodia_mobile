<?php
$_LANG = array(
    'MY_ACCOUNT_LOADER_QUERY_ERROR' => 'Unable to load registration information due an internal error. Please notify the system administrator.',
    'MY_ACCOUNT_UPDATE_HEADER_LABEL' => 'My account',
    'MY_ACCOUNT_NOTICE_LABEL' => 'NOTICE',
    'MY_ACCOUNT_NOTICE_TXT_LABEL' => 'Your account is binded to your email address. You may not change the e-mail address binded to your account.',
    'MY_ACCOUNT_EMAIL_LABEL' => 'E-Mail address',
    'MY_ACCOUNT_PASSWORD_LABEL' => 'Password',
    'MY_ACCOUNT_PASSWORD_PLACEHOLDER' => 'Leave empty to keep the same password.',
    'MY_ACCOUNT_FULL_NAME_LABEL' => 'Full name',
    'MY_ACCOUNT_FULL_NAME_PLACEHOLDER' => 'Enter your full name.',
    'MY_ACCOUNT_UPDATE_BUTTON_LABEL' => 'Update',
    'MY_ACCOUNT_REMOVE_HEADER_LABEL' => 'Remove account',
    'MY_ACCOUNT_IMPORTANT_LABEL' => 'IMPORTANT',
    'MY_ACCOUNT_IMPORTANT_TXT_LABEL' => 'If you choose to remove your account, all your customized templates will be permanently removed. This action cannot be reverted and your work cannot be recovered.',
    'MY_ACCOUNT_REMOVE_BUTTON_LABEL' => 'Remove my account',
    'MY_ACCOUNT_CONFIRM_REMOVE_LABEL' => 'Are you sure you wish to permanently remove your account?',
    'MY_ACCOUNT_INVALID_NAME' => 'Please enter your first and lastname.',
    'MY_ACCOUNT_UPDATE_QUERY_ERROR' => 'Unable to update your account information due an internal error. Please notify the system administrator.',
    'MY_ACCOUNT_UPDATE_QUERY_SUCCESS' => 'Account information updated successfully!',
    'MY_ACCOUNT_REMOVE_QUERY_ERROR' => 'Unable to remove account due an internal error. Please notify the system administrator.'
);