<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>


<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/thank/thank.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar-->
            <div id="topMenu">
            	<p><a href="#">Join Free</a> > Register</p>
            </div>
        	<div id="sectionSidebar"> 
            	<ul>
                	<li class="top"><a href="#" class="linkfade">Register</a></li>
                    <li><a href="login" class="linkfade">Sign In</a></li>
                    <li><a href="idsearch" class="linkfade">ID Search</a></li>
                    <li><a href="pwssearch" class="linkfade">Password Search</a></li>
                </ul>         
            </div><!-- end div id="sectionSidebar"-->            
            <div id="sectionContent"> 
            	<div id="thankTitle">
                	<p>Register</p>
                </div> 
                <div id="tickImg">
                	<div id="imgMain">
                    	<img src="<?php echo BASE_RELATIVE;?>images/thank/tick1.png" />
                    </div>
                </div>
                <div id="congrate">
                	<p>
                    	<span class="congratulation">Congratultion!</span> 
                        <span class="nowMember">You're now a member of</span>
                        <span><a href="#" class="linkfade">Autowini.com</a></span>
                    </p>
                    <p  class="membership">Your membership has been successfully registered!</p>
                </div>
                <div class="firstItem">
                	<div class="itemImg"><img src="<?php echo BASE_RELATIVE;?>images/thank/car.png" /></div>
                    <div class="firstItem"><a href="#"><img src="<?php echo BASE_RELATIVE;?>images/thank/register_car.png" class="linkfade" /></a></div>
                </div>
                <div class="otherItem">
                	<div class="itemImg"><img src="<?php echo BASE_RELATIVE;?>images/thank/truck.png" /></div>
                    <div class="firstItem"><a href="#"><img src="<?php echo BASE_RELATIVE;?>images/thank/register_truck.png" class="linkfade" /></a></div>
                </div>
                <div class="otherItem">
                	<div class="itemImg"><img src="<?php echo BASE_RELATIVE;?>images/thank/bus.png" /></div>
                    <div class="firstItem"><a href="#"><img src="<?php echo BASE_RELATIVE;?>images/thank/register_bus.png" class="linkfade" /></a></div>
                </div>
                <div class="otherItem">
                	<div class="itemImg"><img src="<?php echo BASE_RELATIVE;?>images/thank/equipment.png" /></div>
                    <div class="firstItem"><a href="#"><img src="<?php echo BASE_RELATIVE;?>images/thank/register_equipment.png" class="linkfade" /></a></div>
                </div>
                <div class="otherItem">
                	<div class="itemImg"><img src="<?php echo BASE_RELATIVE;?>images/thank/accessory.png" /></div>
                    <div class="firstItem"><a href="#"><img src="<?php echo BASE_RELATIVE;?>images/thank/register_part.png" class="linkfade" /></a></div>
                </div>
                <div id="note">
                	<p>
                    	Note : First Time on Autowini.com? Register your items now! 2 units FOR FREE!!! if you want to advertise more than 2 items, <br/>
join our <a href="#" class="linkfade">premium</a> members!
                    </p>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  