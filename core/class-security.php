<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * Sets and validates current session ------------------------------------------
 */
if(!isset($_SESSION)) @session_start();

class Security
{
    /*
     * Constructor -------------------------------------------------------------
     * - initialize / validate current session.
     */
    public function Security()
    {
        if( !isset($_SESSION['session']) )
        {

            $_SESSION['session'] = md5($_SERVER['REMOTE_ADDR']);
        }else
        {
            if( $_SESSION['session'] != md5($_SERVER['REMOTE_ADDR']) )
            {
                session_destroy();
                header("Location: " . BASE_SITE);
                exit;
            }
        }
        
        // initialize group session.
        if( !isset($_SESSION['log_group']) )
        {
            $_SESSION['log_group'] = 'public';
        }

        if(isset($_GET['logout'])) $logout=htmlspecialchars($_GET['logout']); else $logout='';

        if(isset($_COOKIE["ussid"])){
            $session=$_COOKIE["ussid"];
            if($logout!=$session){
                if(!isset($_SESSION['log_id'])){
                    $user_detail=$this->getUserBySession($session);
                    if(count($user_detail)>0){
                        $_SESSION['log_group'] = $user_detail['group'];
                        $_SESSION['log_name']  = $user_detail['name'];
						$_SESSION['log_fullname']  = $user_detail['fullname'];
                        $_SESSION['company_name']=$user_detail['company_name'];
                        $_SESSION['log_id']    = $user_detail['id'];
                        $_SESSION['userlog_id']    = $user_detail['log_id'];
                        $_SESSION['log_email'] = $user_detail['email'];
                        $_SESSION['register_type'] = $user_detail['register_type'];

                        $_SESSION['user_max_image_upload'] = $user_detail['max_image'];
                        $_SESSION['user_max_pro'] = $user_detail['max_product'];
                    }
                }
            }else{
                setcookie("ussid","",time()-3600);
            }
        }
    }
    protected function getUserBySession($session){
        require_once BASE_CLASS . 'class-connect.php';
        $db = new Connect();
        $db->open();
        $session=mysql_real_escape_string(stripslashes($session));
        $sql="SELECT `rg`.`id`, `group`, `fullname`, `name`, `company_name`, `log_id`, `email`, `register_type`, `rgt`.`max_image`, `rgt`.`max_product` 
            FROM register rg 
            INNER JOIN register_types rgt ON rgt.`code`=rg.register_type
            WHERE `session`='$session'";
        $result=mysql_query($sql);
		if(mysql_num_rows($sql)>0){
        while($row=mysql_fetch_assoc($result)){
            return $row;
         }
		}
        $db->close();
    }
    /*
     * Get current session -----------------------------------------------------
     * @return string.
     */
    public function getSession()
    {
        return $_SESSION['session'];
    }
    
    /*
     * Get current user logged group status ------------------------------------
     * @return string.
     */
    public function getUserGroup()
    {
        return $_SESSION['log_group'];
    }
}