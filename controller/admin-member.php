<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');

class adminMember
{
    protected $lang;
    protected $group = 'admin';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $members;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;

    var $member_type1="Seller";
    /*
     * Constructor -------------------------------------------------------------
     */
    public function adminmember($lang=array())
    {
        $this->lang = $lang;
        if(isset($_POST['submit'])){
            $this->handleSaveForm();
        }
		$this->loadMembers();
		$this->loadmemberDeleted();				
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
    /**
     * Private method: handle save form
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleSaveForm(){        
        

        ( isset($_POST['register_type'])  ?   $register_type = $_POST['register_type'] : $register_type = array() );
        ( isset($_POST['activated'])  ?   $activated = $_POST['activated'] : $activated = array() );
        // strip all slashes.
        $owner                  = (int)$_SESSION['log_id'];
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        $sql="";
        
        $ids = implode("', '", array_keys($register_type));
        $sql= "UPDATE `register` SET `register_type` = CASE `user_id` ";
        foreach($register_type as $key=>$value){
            $sql.="WHEN '{$key}' THEN '{$value}' ";
        }
        $sql .= "END, ";
        $sql.= "activated = CASE `user_id` ";
        foreach($activated as $key=>$value){
            $sql.="WHEN '{$key}' THEN '{$value}' ";
        }
        $sql .= "END ";
        $sql .= "WHERE user_id IN ('$ids')";
        // foreach($register_type as $key => $value){
        //     $sql="UPDATE `register` SET `register_type`='$value' WHERE `id`='$key'; \n";
            // if(!$this->validRegisterType($value)){
            //     require_once BASE_CLASS . 'class-log.php';
            //     echo mysql_error();
            //     $cnx->close();
                
            //     LogReport::write('Unable to save register table due a query error at ' . __FILE__ . ':' . __LINE__);
            //     $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            //     $this->form_status = true;
            //     $this->form_style = 'alert-warning';

            //     return;
            // }
        //echo $sql;
        if( !@mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            echo mysql_error();
            $cnx->close();
            
            LogReport::write('Unable to save register table due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        // }
       // echo $sql;
        
        
        $cnx->close();
        
        $this->form_message =  $this->lang['PAGE_SAVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }

    /*
     * Private method: validate register type------------------------------------
     * @return array | false.
     */
    private function validRegisterType($register_type){
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
       
        $query = "SELECT * FROM register_types WHERE code='{$register_type}'";
        
        $result=mysql_query($query);
        if(mysql_num_rows($result)>0){return true;}
        
        $cnx->close();
    }
	/*
     * Private method: load members ------------------------------------
     * @return array.
     */
    private function loadMembers(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $members=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";
        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if(isset($_GET['member_type1'])) $member_type1=mysql_real_escape_string(stripcslashes($_GET['member_type1'])); else $member_type1="";
        if(isset($_GET['register_type'])) $register_type=mysql_real_escape_string(stripcslashes($_GET['register_type'])); else $register_type="";
        if(isset($_GET['sort'])) $sort=mysql_real_escape_string(stripcslashes($_GET['sort'])); else $sort="";
        if(isset($_GET['sortby'])) $sortby=mysql_real_escape_string(stripcslashes($_GET['sortby'])); else $sortby="ASC";
        if(isset($_GET['business_type'])) $business_type=mysql_real_escape_string(stripcslashes($_GET['business_type'])); else $business_type="";
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(isset($_GET['condition'])) $condition=mysql_real_escape_string(stripcslashes($_GET['condition'])); else $condition="";
        if(isset($_GET['steering'])) $steering=mysql_real_escape_string(stripcslashes($_GET['steering'])); else $steering="";
        if(isset($_GET['fuel_type'])) $fuel_type=mysql_real_escape_string(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
        if(isset($_GET['min_price'])) $min_price=mysql_real_escape_string(stripcslashes($_GET['min_price'])); else $min_price="";
        if(isset($_GET['max_price'])) $max_price=mysql_real_escape_string(stripcslashes($_GET['max_price'])); else $max_price="";
        if(isset($_GET['make'])) $make=mysql_real_escape_string(stripcslashes($_GET['make'])); else $make="";
        if(isset($_GET['category'])) $category=mysql_real_escape_string(stripcslashes($_GET['category'])); else $category="";
        if(isset($_GET['custom_search'])) $custom_search=mysql_real_escape_string(stripcslashes($_GET['custom_search'])); else $custom_search="";     
		
        if(!empty($member_type1)){
            $where.=" AND `rg`.`member_type1` LIKE '%$member_type1%' ";
            $order=" ORDER BY `rg`.`company_name` $sortby";
        }else{
            $order=" ORDER BY `rg`.`creation` DESC";
        }

        if(!empty($register_type)){
            $where.=" AND `rg`.`register_type` = '$register_type' ";
        }
        
        if(!empty($sort)){
            $order=" ORDER BY `rg`.`$sort` $sortby";
        }
        if(!empty($country)){
            $where.=" AND `rg`.`country`='$country' ";
        }
        if(!empty($business_type)){
            $where.=" AND `rg`.`business_type` LIKE '%$business_type%' ";
        }
        if(!empty($product_type)){
            $where.=" AND `rg`.`business_field` LIKE '%$product_type%' ";
        }
        if(!empty($condition)){
            $where.=" AND `pd`.`condition` = '$condition' ";
        }
        if(!empty($steering)){
            $where.=" AND `pd`.`steering` = '$steering' ";
        }
        if(!empty($fuel_type)){
            $where.=" AND `pd`.`fuel_type` = '$fuel_type' ";
        }
        if(!empty($min_price)){
            $where.=" AND `pd`.`price` > '$min_price' ";
        }
        if(!empty($max_price)){
            $where.=" AND `pd`.`price` < '$max_price' ";
        }
        if(!empty($make)){
            $where.=" AND `pd`.`make` = '$make' ";
        }
        if(!empty($category)){
            $where.=" AND `pd`.`category` = '$category' ";
        }
        if(!empty($custom_search)){
            $where.=" AND `rg`.`company_name` LIKE '%$custom_search%' ";
        }
        $query = "SELECT rg.*, 
        cl.country_name as country_name,
		count(pd.owner) as total,
        (SELECT COALESCE(SUM(car_count + 
            truck_count +
            bus_count +
            equipment_count +
            part_count +
            accessories_count +
            motorbike_count +
            aircraft_count +
            watercraft_count), 0) FROM data_count WHERE register_id=rg.user_id) as product_number 
        FROM register as `rg`   
        INNER JOIN country_list AS cl ON rg.country=cl.cc 
        LEFT JOIN active_product as pd ON pd.owner=rg.user_id
        WHERE 1 $where
        GROUP BY `rg`.`user_id`
        ORDER BY total DESC
        ";
		
        //echo $query;
        /////PAGINATION PROCESS//////
		$itemPerPage=10;
        $sql_count_str=$query;
        $sql_count = @mysql_query($sql_count_str);
		
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row, $itemPerPage);
        $limit=$links->start_display;
        
		$this->pagination_html= $links->display(); 
        $this->current_page = $links->currentPage();
        $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $query.=$limit;
       
        $result = mysql_query($query);
        
        
        //echo $query;
        $this->members = array();
		
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($members, $r);						
        }
		
        //echo mysql_error();
        //var_dump($this->userInfo);
        $this->members=$members;
        $cnx->close();
    }
    
	// Deleted Member
	
	private function loadmemberDeleted(){
	    require_once BASE_CLASS . 'class-connect.php';

		$cnx = new Connect();
		$cnx->open();				
		
		@mysql_query("DELETE FROM register WHERE user_id = '".$_GET['listId']."'");
		@mysql_query("DELETE FROM tbl_carfinder_sellers WHERE user_id = '".$_GET['listId']."'");
		@mysql_query("DELETE FROM more_image WHERE owner = '".$_GET['listId']."'");
		@mysql_query("DELETE FROM product WHERE owner = '".$_GET['listId']."'");
		@mysql_query("DELETE FROM car_media WHERE author = '".$_GET['listId']."'");

		if(isset($_GET['listId'])){
			header("Location: " .BASE_RELATIVE ."member");
		}			
		
		$cnx->close();

    }	
		
	
	/*
	* Public method: get members ------------------------------------------
	* @return bool.
	*/
    public function getMembers()
    {
        return $this->members;
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
