<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class userTradeInfoedit
{
    protected $lang;
    protected $group = 'user';
    protected $slug = 'trade-info-write';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    Protected $trad_list;
    Protected $pic_list;
    /*
     */
    public function userTradeInfoedit($lang=array())
    {
        $this->lang = $lang;
		$this->saveTradeInfo();
        $this->get_tradlist();
        $this->get_piclist();
         
    }
	




	public function saveTradeInfo(){
	
		require_once BASE_CLASS . 'class-connect.php';
		if(isset($_POST['saveTrade'])){
			$category='';
			$topic='';
			$rd_htmlInput='';
			extract($_POST);
            $trade_info_id=$_GET['wid'];

			$date = date('y-m-d');
			$writer = $_SESSION['log_id'];
			$cnx = new Connect();
			$writer_id=$_SESSION['log_id'];
			$cnx->open();
			
			$query = "UPDATE trade_info SET category='".$category."', topic='".$topic."', description='".$rd_htmlInput."', date='".$date."', writer='".$writer."'
                      WHERE `trade_info_id`='$trade_info_id' and writer='$writer_id';";
             
			$resutl = mysql_query($query);
			if(!$resutl){
				require_once BASE_CLASS . 'class-log.php';
				LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
				$cnx->close();
				return;
			}else{
				
				//$trade_info_id = mysql_insert_id();	
				$path = 'images/trade_info/';
					
				$target='images/trade_info/';

				if($target[strlen($target)-1]!='/')
						$target=$target.'/';
					$count=0;
                     $mypic=$_FILES['myfile']['name'];
                      
                    if(sizeof($mypic)>1)
                    { 
                        echo "HELLO";
                        $query="DELETE FROM trade_info_media WHERE trade_info_id='".$trade_info_id."'";
                        $resutl = mysql_query($query);
                    }
					foreach ($_FILES['myfile']['name'] as $filename) 
					{
						$temp=$target;
						$tmp=$_FILES['myfile']['tmp_name'][$count];
						$count=$count + 1;
						$temp=$temp.basename($filename);
						if(move_uploaded_file($tmp,$temp)){
							$query = "INSERT INTO trade_info_media SET trade_info_id='".$trade_info_id."', source='".$filename."'";
							$resutl = mysql_query($query);
						}
						$temp='';
						$tmp='';
					}
				
			}
		}
	}
	
    public function get_piclist()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $id=$_GET['wid'];

        $query="SELECT * FROM trade_info_media  
        WHERE `trade_info_id`='$id'
        ORDER BY 1 DESC ";

        if( !$sql = @mysql_query($query.";" ) )
            {
                $cnx->close();

                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $this->form_message = 'Unable to load page content due an internal error.';
                $this->form_status = true;
                $this->form_style = 'alert-error';

                return;
            }
             
             
        

        $this->pic_list = array();
        while( $r = @mysql_fetch_assoc($sql) ){

                array_push($this->pic_list, $r);
         
            }
        //var_dump($this->trad_list);
        //@mysql_free_result($rows_count);
        @mysql_free_result($sql);
        return $this->pic_list;

    }

    public function get_tradlist()
    {   
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $id=$_GET['wid'];
         
          
            /////PAGINATION PROCESS////
        $query="SELECT tr.*,r.`name` FROM trade_info tr 
        LEFT JOIN register r ON tr.`writer`=r.`id` 
        WHERE `trade_info_id`='$id'
        ORDER BY 1 DESC ";


        /*$sql_count_str= $query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row,10);
        $limit=$links->start_display;
        $this->pagination_html= $links->display(); 
        $this->total_page=$links->numPages();
        $this->current_page=$links->currentPage();
        ///////PAGINATION PROCESS///////
        //$sql_str=$sql_count_str.$limit;  
        $query.=$limit;*/
         

            if( !$sql = @mysql_query($query.";" ) )
            {
                $cnx->close();

                require_once BASE_ROOT . 'core/class-log.php';

                LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $this->form_message = 'Unable to load page content due an internal error.';
                $this->form_status = true;
                $this->form_style = 'alert-error';

                return;
            }
             
             
        

        $this->trad_list = array();
        while( $r = @mysql_fetch_assoc($sql) ){

                array_push($this->trad_list, $r);
         
            }
        //var_dump($this->trad_list);
        //@mysql_free_result($rows_count);
        @mysql_free_result($sql);
        return $this->trad_list;


    }

	/*public function saveTradeInfoPic(){
		$path = 'images/trade_info/';
		$file = $_FILES['myfile']['name'];
		foreach($_FILES['myfile']['name'] as $filename){
			echo $filename.'<br>';
		}
		
	}*/

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	
	
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
