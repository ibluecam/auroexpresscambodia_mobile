<?php
$_LANG = array(
    'FAV_HEADER_LABEL' => 'My favorites',
    'FAV_QUERY_ERROR' => 'Unable to load favorite list due an internal error. Please contact the support.',
    'FAV_LIST_EMPTY_LABEL' => 'You have no vehicles in your favorite list.',
    'FAV_REMOVE_BUTTON' => 'Remove from favorites',
    'FAV_REMOVE_CONFIRM' => 'Are you sure you wish to remove this vehicle from your favorite list?',
    'FAV_REMOVE_QUERY_ERROR' => 'Unable to remove vehicle from favorite list due an internal error. Please contact the support.',
    'FAV_REMOVE_QUERY_SUCCESS' => 'Vehicle successfully removed from favorite list!'
);