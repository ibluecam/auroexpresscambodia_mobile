<?php
$_LANG = array(
    'PAGE_SAVE_QUERY_SUCCESS' => 'Member setting was saved!.',
    'PAGE_SAVE_QUERY_ERROR' => 'Error! Could not save member setting. See the log file for more info.',
    'PAGE_NOT_FOUND_QUERY_ERROR' => 'Record could not be found!'
);