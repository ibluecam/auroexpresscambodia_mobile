<?php


/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$userInfo = $_CLASS->getUserInfo();
$listinquiryimg=$_CLASS->listinquiryimg();

?>

 <div id="sectionContenWrapper">
 
            <div id="sectionContent">
             
            	

                <!--my info content-->
            
                <div id="info" class="clearfix">
                <p id="title" style="margin-bottom:35px"><b><?php echo $_LANG['USER_MY_INFO_ACCOUNT'];?></b></p>
                <p style="margin-bottom:15px"></p>
                    <?php
                        foreach($userInfo as $row){
                    ?>
                    <div id="infoDetail" class="clearfix">
                        <table border="0" cellpadding="0" cellspacing="0" style="width:55%;margin-left:5px;">
							
							<tr>
								<td class="left"><b><?php echo $_LANG['USER_ID'];?></b></td>
								<td>:<b style='padding-left:5px;'><?php echo $row['username'];?></b></td>
							</tr>
							<tr>
                            	<td class="left"><b><?php echo $_LANG['USER_MY_INFO_COMPANY_NAME'];?></b></td>
                                <td>: 
										
								<?php echo"<strong>". $row['firstname']." ".$row['lastname']."</strong>";?>
										
								</td>
                            </tr>
						
                            <tr>
                           		<td class="left"><b><?php echo $_LANG['USER_MY_INFO_EMAIL'];?></b></td>
                                <td>:<b style='padding-left:5px;'><?php echo $row['email'];?></b></td>
                            </tr>
                            
                            <!--<tr>
                            	<td class="left"><b><?php echo $_LANG['USER_MY_INFO_CONTACT_PERSON'];?></b></td>
                                <td>: <?php echo"<strong>". $row['name']."</strong>";?></td>
                            </tr>
							-->
                            <!--<tr>
                            	<td class="left"><b><?php echo $_LANG['USER_MY_INFO_COUNTRY'];?></b></td>
                                <td>: <img src="<?php echo BASE_RELATIVE;?>images/flag/<?php echo $row['country'];?>.png " /> <?php echo $row['country_name'];?></td> 
                            </tr>-->
							
                           
							
                            <tr>
                                <td class="left"><b><?php echo $_LANG['USER_MY_INFO_TEL'];?></b></td>
                                
                                <?php if ($row['mobile'] == null) {
									
                                	echo "<td>: <td/>";
                                } else {
								 //echo "<td>: "; echo"<strong>+".$row['phonecode']."-</strong>"; echo "<strong>".$row['mobile']."</strong>";
								 echo "<td>: <strong>".$row['mobile']."</strong></td>";
								};?>
                            </tr>
							 <tr>
                            	<td class="left"><b><?php echo $_LANG['USER_MY_INFO_ADDRESS'];?></b></td>
                                <td>: <?php echo"<strong>". $row['address']."</strong>";?></td>
                            </tr>

                        </table>
                    	
                        <div id="edit_profile"><p><a href="<?php echo BASE_RELATIVE.'edit-myinfo?id='.$_SESSION['log_id'];?>" class="linkfade linkimg"><?php echo $_LANG['USER_MY_INFO_EDIT_PROFILE'];?></a></p></div>

                    </div>
                    <?php
                                    }
                                ?>
                </div>

				<div id="info-right"><img id="infoImg" src="<?php echo BASE_RELATIVE?>images/myinfo/angkor ads.jpg" /></div>

            </div><!-- end div id="sectionContent"-->
            
            <div id="partner"><!-- start div id="partner"-->
            <p id="title-part">Our Partner</p>
                <ul>
                    <li><img src="<?php echo BASE_RELATIVE?>images/myinfo/blauda.png"/></li>
                    <li><img src="<?php echo BASE_RELATIVE?>images/myinfo/ligmotor.jpg"/></li>
                    <li><img src="<?php echo BASE_RELATIVE?>images/myinfo/aml.png"/></li>
                    <li><img src="<?php echo BASE_RELATIVE?>images/myinfo/advertising_car3.png"/></li>
                    <li><img style="height:145px;"src="<?php echo BASE_RELATIVE?>images/myinfo/logo_softbloom.jpg"/></li>
           		</ul>
            </div><!-- end div id="partner"-->
        </div><!-- end div id="sectionContentWraper" -->

