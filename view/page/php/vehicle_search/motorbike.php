




                    <?php
                        if ($_SESSION['mobile']==false) {
                    ?>

                    <div id="second_search">
                        <div id="search_title" class="clearfix">
                            <p><a href="#" class="current" id="quick_search">Quick Search</a> <span>&nbsp;&nbsp; | &nbsp;&nbsp;</span> <a href="#" id="ad_search">Advance Search</a></p>
                        </div>
                        <div id="search_wrap" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>

                                <input type="hidden" class="formSelection" name="page" id="page" value="<?php if(isset($_GET['page'])) echo htmlspecialchars($_GET['page']);?>"/>

                                <select class="make formSelection moto" name="make" id="make" style="width:230px">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>


                                <select class="model formSelection moto" name="model" id="model" style="width:230px">
                                    <option value="">- Model -</option>
                                </select>
                                <!--<select class="condition formSelection moto" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>

                                </select>

                                <select class=" formSelection " name="country" id="country_adv">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>
                                <select class="year formSelection" name="year_from" id="year_from">
                                        <option value="">Year From</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>

                                    <select class="year toSelection" name="year_to" id="year_to">
                                        <option value="">Year To</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>-->
                                    <input type="text" class="textBox" name="customSearch" placeholder="Model name, Item No..." id="customSearch" />
                                    <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div>
                        
                        <div id="advance_search" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" name="search" value="advance"/>
                                <select class="make formSelection" name="make" id="make_adv">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>


                                <select class="model formSelection" name="model" id="model_adv">
                                    <option value="">- Model -</option>
                                </select>
                                <select class="condition formSelection " name="condition" id="condition_adv">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                </select>

                                <select class=" formSelection " name="country" id="country_adv">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>
                                <!--<span class="displacement"> Displacement </span>-->
                                
                                <select class="yearmotoad year formSelection" name="year_from" id="year_from">
                                        <option value="">-- From year --</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>

                                    <select class="yearmotoad year toSelection" name="year_to" id="year_to">

                                        <option value="">-- To year --</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>


                                
                                <input type="text" class="price" name="price_from" value="<?php echo $price_from;?>" placeholder="Min Price" id="price_from_adv" onkeypress="return isNumberKey(event)" />
                                
                                <input type="text" class="price" name="price_to" value="<?php echo $price_to;?>" placeholder="Max Price" id="price_to_adv" onkeypress="return isNumberKey(event)" />
						
                        		<input type="text" class="moto_search" name="customSearch" placeholder="keywords" id="customSearch_adv" style="margin-left:3px; " />
                                <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div>
                    </div>

                    <!-- <table border="0" cellspacing="0" cellpadding="0">
                        <tr>

                            <td class="condition top">

                                <select class="condition formSelection" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    <option value="Salvage">Salvage</option>
                                </select>

                            </td>
                            <td class="make">
                                <select class="make formSelection" name="make" id="make">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                            </td>
                            <td class="model">
                                <select class="model formSelection" name="model" id="model">
                                    <option value="">- Model -</option>
                                </select>
                            </td>
                            <td class="steering">
                                <select class="steering formSelection" name="steering" id="steering">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>
                            </td>

                            <td class="country">
                                <select class="country formSelection" name="country" id="country">
                                    <?php
                                        include('ajax/load_country.php');
                                    ?>
                                </select>
                            </td>

                            <td >
                                <select class="ful_type formSelection" name="fuel_type" id="fuel_type">
                                    <?php
                                        include('ajax/load_saved_fuel_type.php');
                                    ?>
                                </select>
                            </td>

                            <td class="year">
                                <div id="divYear">

                                    <select class="year formSelection" name="year_from" id="year_from">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                         ~
                                    <select class="year toSelection" name="year_to" id="year_to">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                </div>

                            </td>
                            <td class="search">
                                <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                <input type="submit" value="" class="search" />

                            </td>
                          </tr>

                    </table> -->
                    <?php }
                    else{
                        ?>
                    <div class="new_searchwrap">
                        <div class="newsearch">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class=" top">

                                        <select class="condition formSelection" name="condition" id="condition">
                                            <option value="">- Condition -</option>
                                            <option value="New">New</option>
                                            <option value="Used">Used</option>

                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="make formSelection" name="make" id="make">
                                            <?php
                                                include('ajax/load_car_make.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="model formSelection" name="model" id="model">
                                            <option value="">- Model -</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <select class="steering formSelection" name="steering" id="steering">
                                            <option selected value="">- Steering -</option>
                                            <option value="LHD">LHD</option>
                                            <option value="RHD">RHD</option>
                                        </select>
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td >
                                        <select class="country formSelection" name="country" id="country">
                                            <?php
                                                include('ajax/load_country.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                -->
                                <tr>
                                    <td >
                                        <select class="ful_type formSelection" name="fuel_type" id="fuel_type">
                                            <?php
                                                include('load_saved_fuel_type.php');
                                            ?>
                                        </select>
                                    </td>
                                </tr>


                                <tr>
                                    <td >
                                        <div id="divYear">
                                            <!-- <div id="year"> <p>Year :</p></div> -->
                                            <select class="year formSelection" name="year_from" id="year_from">
                                                <option value="">From</option>
                                                 <?php
                                                 for($i=1990;$i<=2014;$i++){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                                 ?>
                                            </select>

                                            <select class="year_end toSelection" name="year_to" id="year_to">
                                                <option value="">To</option>
                                                 <?php
                                                 for($i=1990;$i<=2014;$i++){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                                 ?>
                                            </select>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="search">
                                        <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <input type="submit" value="" class="search" />
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </div>
                    </div>
                        <?php
                    }
                    ?>






