<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
if(isset($_GET["deleteresult"])) $deleteresult=htmlspecialchars($_GET["deleteresult"]); else $deleteresult='';

if(!empty($deleteresult)){
    if($deleteresult=='fail'){
       $fstatus = true;
        $fmessage = "Cannot deleted this deal info. You may not have permission to delete it or it doesn'exist";
        $fstyle = 'alert-warning';
    }
}
$group = 'user';
$slug  = 'car';

$owner = (int)$_SESSION['log_id'];
if(isset($_GET['id'])) $get_id=htmlspecialchars($_GET['id']); else $get_id="";
if(isset($_GET['owner'])) $get_owner=htmlspecialchars($_GET['owner']); else $get_owner="";
$get_dealweek = $_CLASS->loadDealweek($get_id);
$get_register = $_CLASS->loadUserName($get_owner );

$str = $get_dealweek['date'];  
$i = strrpos($str, " ");
$l = strlen($str) - $i;
$str = substr($str, $i, $l);
$date = str_ireplace($str, "", $get_dealweek['date']);

// load page content.

$pg_details = $_CLASS->getPageHTML();


/*if($get_register['id']!=$_SESSION['log_id']){    
    header("location:dealof-theweek");
}
else{
    if(isset($_POST['update_dealweek'])){
        $_CLASS->updateDealWeek($_SESSION['log_name'],$get_register['name']);
        $status = $_CLASS->getstatus();
        if( $status == 'ok' )
        {
          FeedbackMessage::displayOnPage('Submit Sucessfully', 'success');
        ?>
        <p class="alert alert-success" style="text-align:center;"><strong><?php echo  "SUBMIT COMPLETE";?></strong></p>

        <?php
            }

        if($status=='error'){
            ?>    
            <p class="alert alert-error" style="text-align:center;"><strong><?php echo 'You are not the owner of this product.';?></strong></p>

                <?php
        }
    }    
        
}*/




?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > <a href="#" class="linkfade">Deal of the Week</a> > View</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Deal of the Week</p>
                </div>
                <?php
                    if( $fstatus ){
						//foreach($get_register as $_get){
							//if ($_get['owner']!=$_SESSION['log_id']){
							  // die();
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <div id="community_view" class="clearfix">
                	<div id="logo">
                        <?php                                       
                            if(!file_exists('upload/'.$get_register['image']) || $get_register['image']==""){                                         
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="40" height="40" />
                        <?php                                           
                            }else{
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>upload/<?php echo $get_register['image'];?>"  width="40" height="40" />
                        <?php
                            }
                        ?>
                    	
                    </div>
                    <div id="detail">
                    	<p class="title">
                            <?php echo $get_dealweek['title']?>
                        </p>
                        <p>Written by : <span class="writer"><?php echo $get_register['name']?></span>	Date : <?php echo $date?>	<span>Click : 10</span></p>
                    </div>
                    <div id="img_wrap" class="clearfix">
                        <div id="text_editor">                            
                            <?php echo $get_dealweek['message'];?>
                        </div>                    	
                    </div>
                             
                </div>
                <?php if ($get_dealweek['owner']==$owner){?>
                    <div id="button_wrap" style="width:320px;">
                        <div id="write_article">
                            <a href="<?php echo BASE_RELATIVE;?>community-dealview?delete=<?php echo $_GET['id'];?>"><img src="<?php echo BASE_RELATIVE;?>images/community/delete_com.png" /></a>
                        </div>
                    	<div id="write_article" style="margin-left:10px;">
                            <a href="<?php echo BASE_RELATIVE;?>community-view-edit?id=<?php echo $_GET['id'].'&owner='.$_GET['owner'];?>"><img src="<?php echo BASE_RELATIVE;?>images/community/edit_article.png" /></a>
                        </div>
                        <div id="back">
                        	<a href="<?php echo BASE_RELATIVE;?>dealof-theweek"><img src="<?php echo BASE_RELATIVE;?>images/community/back_to_list.png" /></a>
                        </div>
                    </div>
                <?php  } ?>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  