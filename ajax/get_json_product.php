<?php 
header('Content-type: application/json');
require_once '../config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->pdoOpen();

$sql_str="SELECT pd.*, rg.company_name, c.thumb FROM product pd
	INNER JOIN activated_seller rg ON pd.owner = rg.id
	LEFT JOIN product_primary_photo c ON c.product_id=pd.id
 WHERE product_type='Truck' LIMIT 10";

$cnx->pdoExecuteQuery($sql_str);
$products=$cnx->getRecords();

@mysql_free_result($sql);
$cnx->close();
$encoded_product= json_encode($products, true);
//$arr=json_decode($encode_product, true);
echo $encoded_product;
?>