<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicsearch {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    private   $car_result;
    private   $car_media;
    private   $car_features;
    private   $car_pagination_array;
    private   $total_results;
    private   $body_type_list;
    private   $maker_list;
    private   $maker_logo_list;
    
    /**
     * Get total results
     * <br>---------------------------------------------------------------------
     * @return int
     */
    public function getTotalResults(){
        return $this->total_results;
    }
    
    /**
     * Get pagination
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getPaginationList(){
        return $this->car_pagination_array;
    }
    
    /**
     * Get car features
     * <br>---------------------------------------------------------------------
     * @param $car_id The car id
     * @return array
     */
    public function getCarFeatures($car_id){
        if( count($this->car_features) < 1 ){
            return array();
        }
        
        $result =  array();
        
        for( $i=0; $i < count($this->car_features); $i++ ){
            if( $this->car_features[$i]['car_id'] == $car_id ){
                array_push($result,$this->car_features[$i]);
            }
        }
        
        return $result;
    }
    
    /**
     * Get car media
     * <br>---------------------------------------------------------------------
     * @param $car_id The car id
     * @return array
     */
    public function getCarMedia($car_id){
        if( count($this->car_media) < 1 ){
            return array();
        }
        
        $result = array();
        
        for( $i=0; $i < count($this->car_media); $i++ ){
            
            if( $this->car_media[$i]['car_id'] == $car_id ){
                array_push($result,$this->car_media[$i]);
            }
        }
                
        return $result;
    }
    
    /**
     * Get car result list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getCarList(){
        return $this->car_result;
    }
    
    /**
     * Get maker list [update:2013.07.13]
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getMakerList(){
        return $this->maker_list;
    }
    
    /**
     * Constructor.
     * <br>---------------------------------------------------------------------
     */
    public function publicsearch($lang=array()){
        $this->lang = $lang;
        
        // include body type list.
        $path = BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/car-body-types.php';
        
        if( file_exists($path) ){
            require_once $path;
            $this->body_type_list = $_CAR_BODY;
        }
        else {
            $this->body_type_list = array();
        }
        
        // there are 3 possible requests made to this page:
        // ?q       : contains maker, model and body encoded in base64
        // ?body    : body types
        // ?maker   : maker name
        $is_set = false;
        
        if( isset($_REQUEST['body']) ){
            $is_set = true;
            $this->handleSearchByBody();
        }
        
        if( isset($_REQUEST['maker']) ){
            $is_set = true;
            $this->handleSearchByMaker();
        }
        
        if( isset($_REQUEST['q']) ){
            $is_set = true;
            $this->handleSearchMMT();
        }
        
        // if none is set, the page has been directly accessed. display all
        // cars.
        if( !$is_set ){
            $this->handleSearchMMT(true);
        }
        
        // load maker list.
        $this->loadMakerList();
    }
    
    /**
     * Private method: load maker list
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function loadMakerList(){
        $this->maker_list = array();
        
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT `maker` FROM `car`") ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $cnx->close();
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->maker_list, stripslashes($r['maker']));
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        
        unset($sql);
        unset($r);
        
        $this->maker_list = array_unique($this->maker_list);
        $this->maker_list = array_values($this->maker_list);
        
        return;
    }
    
    /**
     * Private method: handler search by maker, model and body.
     * <br>---------------------------------------------------------------------
     * @param $override Override the REQUEST method.
     * @return void.
     */
    private function handleSearchMMT($override=false){
        if( $override ){
            $q = 'YWxse186X31hbGx7XzpffWFsbA==';
        }
        else {
            $q = trim($_REQUEST['q']);
        }
        
        $q = base64_decode($q);
        $q = explode('{_:_}',$q);
        
        $maker = $q[0];
        $model = $q[1];
        $body  = $q[2];
        
        // validate entries.
        if( empty($maker) ){
            $maker = 'all';
            $model = 'all';
            $body  = 'all';
        }
        
        if( empty($model) ){
            $model = 'all';
            $body  = 'all';
        }
        
        if( empty($body) ){
            $body = 'all';
        }
        
        // strip GET %20 (space)
        $maker = str_replace('%20', ' ', $maker);
        $model = str_replace('%20', ' ', $model);
        $body  = str_replace('%20', ' ', $body);
        
        // if body is not 'all', get it's ID.
        if( count($this->body_type_list) < 1 ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to locate car body type list at language directory. This file MUST exist! Error throw at ' . __FILE__ . ':' . __LINE__ .' for path: ' . $path);
            
            $body = 'all';
        }
        else {
            foreach( $this->body_type_list as $k => $v ){
                if( strtolower($v) == $body ){
                    $body = $k;
                    break;
                }
            }
        }
        
        // define search queries. status: 0=available | 1=reserved | 2=sold
        // query by maker, model and body.
        if( $maker != 'all' && $model != 'all' && $body != 'all' ){
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `maker`='$maker' AND `model`='$model' AND `body_type`='$body' AND `status`='0' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        
        // query by maker and model
        else if( $maker != 'all' && $model != 'all' && $body == 'all' ){
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `maker`='$maker' AND `model`='$model' AND `status`='0' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        
        // query by maker only.
        else if( $maker != 'all' && $model == 'all' && $body == 'all' ){
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `maker`='$maker' AND `status`='0' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        
        // query all entries.
        else {
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `status`='0' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        
        $this->doQuery($query);
    }
    
    /**
     * Private method: handle search by maker.
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleSearchByMaker(){
        $maker = trim($_REQUEST['maker']);
        
        // validate.
        if( empty($maker) ){
            $maker = 'all';
        }
        
        $maker = str_replace('%20', ' ', $maker);
        
        // build query.
        if( $maker == 'all' ){
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `status`='0' `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        else {
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `maker`='$maker' AND `status`='0' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        
        $this->doQuery($query);
    }
    
    /**
     * Private method: handle search by body.
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function handleSearchByBody(){
        $body = trim($_REQUEST['body']);
        
        // validate.
        if( empty($body) ){
            $body = 'all';
        }
        
        $body = str_replace('%20', ' ', $body);
        
        // build query.
        if( $body == 'all' ){
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `status`='0' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        else {
            $query = "SELECT [::SELECT_TYPE::] FROM `car` WHERE `body_type`='$body' AND `status`='0' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]";
        }
        
        $this->doQuery($query);
    }
    
    /**
     * Private method: do query with pagination
     * <br>---------------------------------------------------------------------
     * @param $query The query to be performed.
     * @return void
     */
    private function doQuery($query){
        // validate.
        if( empty($query) ){
            return;
        }
        
        // initialize result.
        $this->car_result = array();
        
        // get order.
        ( isset($_GET['o']) ? $attr = trim($_GET['o']) : $attr = '' );
        ( isset($_GET['ot']) ? $ord  = trim($_GET['ot']) : $ord = '' );
        
        // validate order.
        switch($attr){
            case 'maker':
            case 'model':
            case 'body_type':
            case 'price':
            case 'year':
            case 'miles':
            case 'door':
            case 'featured':
                break;
            default: $attr = 'featured';
        }
        
        switch ($ord){
            case 'a':
                $ord = 'ASC';
                break;
            case 'd':
                $ord = 'DESC';
                break;
            default:
                $ord = 'ASC';
        }
        
        // get max. results per page. min (5) max (40).
        ( isset($_GET['r']) ? $max_per_page = (int)$_GET['r'] : $max_per_page = 20 );
        
        if( $max_per_page < 20 ){ 
            $max_per_page = 20;
        }
        if( $max_per_page > 40 ){
            $max_per_page = 40;
        }
        
        // get current selected page.
        ( isset($_GET['pg']) ? $pageno = (int)$_GET['pg'] : $pageno = 1 );
        
        if( $pageno < 1 ){
            $pageno = 1;
        }
        
        // get number of results.
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        $tquery = $query;
        $tquery = str_replace(' ORDER BY `[::ORDER_ATTR::]` [::ORDER_TYPE::] [::LIMIT::]', '', $tquery);
        $tquery = str_replace('[::SELECT_TYPE::]', '`id`', $tquery);
                
        if( !$sql = @mysql_query($tquery) ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to get search results due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['SEARCH_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            $cnx->close();
            
            return;
        }
        
        $rows = @mysql_num_rows($sql);
        $this->total_results = $rows;
        
        // check if there is at least 1 result.
        if( $rows < 1 ){
            $cnx->close();
            return;
        }
        
        $lastpage = ceil($rows / $max_per_page);
        
        if( $pageno > $lastpage ){
            $pageno = $lastpage;
        }
        
        $limit = 'LIMIT ' . ( ($pageno-1) * $max_per_page ) . ', ' . $max_per_page . ';';
        
        // replace key tags.
        $query = str_replace('[::LIMIT::]', $limit, $query);
        $query = str_replace('[::ORDER_ATTR::]', $attr, $query);
        $query = str_replace('[::ORDER_TYPE::]', $ord, $query);
        $query = str_replace('[::SELECT_TYPE::]', '*', $query);
                
        // do query.
        if( !$sql = @mysql_query($query) ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to get search results due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['SEARCH_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            $cnx->close();
            
            return;
        }
        
        // if no entry is found.
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        // set pagination array.
        $this->car_pagination_array = array();
        
        if( $lastpage == 1 ){
            array_push($this->car_pagination_array, 1);
        } 
        else {
            for( $i=1; $i <= $lastpage; $i++ ){
                array_push($this->car_pagination_array, $i);
            }
        }
        
        // load car object.
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'maker' => $r['maker'],
                'model' => $r['model'],
                'body_type' => $r['body_type'],
                'price' => $r['price'],
                'year' => $r['year'],
                'miles' => $r['miles'],
                'measure_type' => $r['measure_type'],
                'doors' => $r['doors'],
                'status' => $r['status'],
                'featured' => $r['featured'],
                'eco' => $r['eco']
            );
            
            array_push($this->car_result, $obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        
        // load each result's details.
        $this->loadCarDetails();
    }
    
    /**
     * Private method: load car features and media.
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function loadCarDetails(){
        $this->car_features = array();
        $this->car_media = array();

        // if there are no cars, load nothing.
        if( count($this->car_result) < 1 ){
            
            return;
        }
        
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-log.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        for( $i=0; $i < count($this->car_result); $i++ ){
            $cid = $this->car_result[$i]['id'];
            
            // load details data.
            if( !$sql = @mysql_query("SELECT * FROM `car_features` WHERE `car_id`='$cid' ORDER BY `feat_name` ASC") ){
                LogReport::write('Unable to load car features due a query error at ' . __FILE__ . ':' . __LINE__);
            }
            else {
                if( @mysql_num_rows($sql) > 0 ){
                    while( $r = @mysql_fetch_assoc($sql) ){
                        $obj = array(
                            'id' => $r['id'],
                            'car_id' => $r['car_id'],
                            'feat_name' => stripslashes($r['feat_name']),
                            'feat_icon' => $r['feat_icon']
                        );
                        
                        array_push($this->car_features, $obj);
                    }
                    
                    @mysql_free_result($sql);
                }
            }
            
            // load media data.
            if( !$sql = @mysql_query("SELECT * FROM `car_media` WHERE `car_id`='$cid' ORDER BY `mode` ASC") ){
                LogReport::write('Unable to load car media due a query error at ' . __FILE__ . ':' . __LINE__);
            }
            else {
                if( @mysql_num_rows($sql) > 0 ){
                    while( $r = @mysql_fetch_assoc($sql) ){
                        $obj = array(
                            'id' => $r['id'],
                            'car_id' => $r['car_id'],
                            'type' => $r['type'],
                            'mode' => $r['mode'],
                            'source' => $r['source']
                        );
                        
                        array_push($this->car_media, $obj);
                    }
                    
                    @mysql_free_result($sql);
                }
            }            
        }// end for.
        
        $cnx->close();
    }
    
    /**
     * Public method: get logo list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getMakerLogoList(){
        $this->maker_logo_list = array();
        
        // set maker directory.
        $dirp = BASE_ROOT . 'image/maker/';
        
        if( !file_exists($dirp) ){
            return array();
        }
        
        if( !$dir = @opendir($dirp) ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load maker image list at ' . __FILE__ . ':' . __LINE__);
            return array();
        }
        
        while( false !== ($file = @readdir($dir)) ){
            if( $file != '.' && $file != '..' && $file != '.htaccess' ){
                // get maker name.
                $maker = strtolower($file);
                $maker = str_replace('.jpg','',$maker);
                
                $obj = array(
                    'maker' => $maker,
                    'src' => BASE_RELATIVE . 'image/maker/' . $file
                );
                
                array_push($this->maker_logo_list, $obj);
            }
        }
        
        @closedir($dir);
        
        unset($maker);
        unset($dirp);
        unset($dir);
        unset($file);
        
        return $this->maker_logo_list;
    }
    
    /**
     * Get body type by id
     * <br>---------------------------------------------------------------------
     * @return string
     */
    public function getBodyTypeByID($id){
        if( count($this->body_type_list) < 1 ){
            return $this->lang['SEARCH_UNKNOWN_BODY_TYPE_LABEL'];
        }
        
        $result = '';
        
        foreach($this->body_type_list as $k => $v ){
            if( $k == $id ){
                $result = $v;
                break;
            }
        }
        
        return $result;
    }

    /**
     * Public method: get form status.
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message.
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style.
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
