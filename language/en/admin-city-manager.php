<?php
$_LANG = array(
    'PAGE_DELETE_QUERY_SUCCESS' => 'Member setting was removed successfully.',
    'PAGE_DELETE_QUERY_ERROR' => 'Error! Could not delete. See the log file for more info.',
    'PAGE_NOT_FOUND_QUERY_ERROR' => 'Record could not be found!'
);