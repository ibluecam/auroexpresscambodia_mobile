<?php
$_LANG = array(
    'LOGIN_STATUS'=>'Sign in',
	'LOGIN_EMAIL'=>'User ID',
	'LOGIN_PASSWORD'=>'Password',
	'LOGIN_REMEMBER_ME'=>'Remember Me',
	'LOGIN_FORGOT_ID'=>'Forgot ID',
	'LOGIN_FORGOT_PASSWORD'=>'Forgot Password',
	'LOGIN_REMEMBER'=>'Not a member',
	'LOGIN_REGISTER'=>'Sign Up'
	
);