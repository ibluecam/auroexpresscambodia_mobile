<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class userUpgradeMember {
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $news;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function userUpgradeMember($lang=array()){
        $this->lang = $lang;
        
        // this load news.
        $this->loadNews();
		$this->loadUserData();
    }
    
    /**
     * Private method: load last 10 news
     * <br>---------------------------------------------------------------------
     * @return void
     */
	 
	 private function loadUserData(){
		require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();
		
		$email = $_SESSION['log_email'];
		$query = "SELECT * FROM register WHERE email='$email' ";
		$result = mysql_query($query);
		
		
		//echo $query;
		$this->userInfo = array();
		while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($this->userInfo, $r);
		}
		//echo mysql_error();
		//var_dump($this->userInfo);
		
		$cnx->close();
	}
    private function loadNews(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $this->news = array();
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `news` ORDER BY `id` DESC LIMIT 10;") ){
            $cnx->close();
            
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to load news due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['NEWS_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            
            return;
        }
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $obj = array(
                'id' => $r['id'],
                'title' => stripslashes($r['title']),
                'html' => stripslashes($r['html']),
                'date' => $r['date'],
                'time' => $r['time'],
                'author' => stripslashes($r['author'])
            );
            
            array_push($this->news,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
    
    /**
     * Public method: get news list
     * <br>---------------------------------------------------------------------
     * @return array
     */
    public function getNews(){
        return $this->news;
    }
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /**
     * Public method: get form message
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
	
	public function getUserInfo(){
        return $this->userInfo;
    }
	
	public function upgradeMember(){
		if(isset($_POST['upgrade-member'])){
			extract($_POST);
			$log_mail = $_SESSION['log_email'];
			$avalible_service = "";
			if($car != ''){
				$avalible_service .= $car.',';
			}
			if($track != ''){
				$avalible_service .= $track.',';
			}
			if($bus != ''){
				$avalible_service .= $bus.',';
			}
			if($construction_equipment != ''){
				$avalible_service .= $construction_equipment.',';
			}
			if($attachment != ''){
				$avalible_service .= $attachment.',';
			}
			if($equipment_part != ''){
				$avalible_service .= $equipment_part.',';
			}
			if($special_vehivle != ''){
				$avalible_service .= $special_vehivle.',';
			}
			if($auto_part != ''){
				$avalible_service .= $auto_part.',';
			}
			if($auto_accessory != ''){
				$avalible_service .= $auto_accessory.',';
			}
			if($automobile_recycling != ''){
				$avalible_service .= $automobile_recycling.',';
			}
			if($engine_oil != ''){
				$avalible_service .= $engine_oil.',';
			}
			if($shipping_shoring != ''){
				$avalible_service .= $shipping_shoring.',';
			}
			
			
			$cnx = new Connect();
        	$cnx->open();
			//echo $member_type;
			//header('location:'.BASE_RELATIVE.'sent-success?popup');
			$update = "UPDATE register SET member_type1='$member_type',
										avalible_service='$avalible_service',
										member_status='0',
										message='$message'
										WHERE email='$log_mail'";
			//echo $update;
			if(mysql_query($update)){
					echo "Sucess";
				//header('location:'.BASE_RELATIVE.'sent-success?popup');
			}
			
			$cnx->close();
			
			
		
			
		}
	}
}