<?php 
    if(!isset($_SESSION['mobile'])) $mobile=false; else $mobile=$_SESSION['mobile'];
    $_GET['product_type']="Equipment";
?>

                    <?php
                    
                        if ($mobile==false) {
                    ?>
                    <div id="second_search">
                        <!-- <div id="search_title" class="clearfix">
                            <p><a href="#" class="current" id="quick_search">Quick Search</a> <span>&nbsp;&nbsp; | &nbsp;&nbsp;</span> <a href="#" id="ad_search">Advance Search</a></p>
                        </div> -->
                        <div id="search_wrap" class="clearfix">
                            <form method="get" action="my-inventory">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" class="formSelection" name="owner" id="owner" value="<?php if(isset($_GET['owner'])) echo htmlspecialchars($_GET['owner']);?>"/>
                                <input type="hidden" class="formSelection" name="s" id="offset" value="<?php if(isset($_GET['s'])) echo htmlspecialchars($_GET['s']);?>"/>

                                <select class="equipment_first category formSelection" name="category" id="category">
                                    <?php 
                                        include('../../../ajax/load_saved_category.php');
                                    ?>
                                </select>
                                <select class="formSelection equipment" name="category2" id="category2">
                                    <option value="">- Sub Category -</option>
                                </select> 
                                <select class="equipment make formSelection" name="make" id="make">
                                    <?php
                                        include('../../../ajax/load_car_make.php');
                                    ?>
                                </select>
                                
                                
                                <select class="equipment model formSelection" name="model" id="model">
                                    <option value="">- Model -</option>
                                </select>
                                <select class="equipment condition formSelection " name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>
                                <select class="equipment steering formSelection" name="steering" id="steering">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>                                
                                <select class="country formSelection" name="country" id="country">
                                    <?php 
                                        include('../../../ajax/load_country.php');
                                    ?>
                                </select>
                                <select class="year formSelection" name="year_from" id="year_from">
                                        <option value="">From...</option>
                                         <?php
                                             for($i=date('Y');$i>=date('Y')-20;$i--){
                                                     echo "<option value='$i'>$i</option>";
                                                 }
                                        ?>
                                    </select>
                                         
                                    <select class="year toSelection" name="year_to" id="year_to">
                                        <option value="">To...</option>
                                         <?php
                                         for($i=date('Y');$i>=date('Y')-20;$i--){
                                                 echo "<option value='$i'>$i</option>";
                                             }
                                         ?>
                                    </select>
                                    <input type="text" class="equipment_search" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                    <input type="submit" id="submit_search"  value="" class="search linkfade" />
                            </form>
                        </div>
                        <!--======================== ADVANCE SEARCH =====================================-->
                                       
                        
                    </div>
                    
                    <?php }
                    else {
                        ?>
                        <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                        <input type="hidden" class="formSelection" name="s" id="offset" value="<?php if(isset($_GET['s'])) echo htmlspecialchars($_GET['s']);?>"/>
                        <table border="0" cellspacing="0" cellpadding="0">
                        
                        <tr>
                        <!-------------------------- BLOCK 1 ------------------------------>
                            <td class="top condition">                                
                                <select class="formSelection condition" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="top category">                                
                                <select class="formSelection category" name="category" id="category">
                                    <?php 
                                        include('../../../ajax/load_category.php');
                                    ?>
                                </select>                                
                            </td>
                        </tr>
                        <tr>
                            <td class="top category2">                                
                                <select class="formSelection" name="category2" id="category2">
                                    <option value="">- Category 2 -</option>
                                    
                                </select>                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select class="formSelection category2" name="make" id="make">
                                    <?php 
                                        include('../../../ajax/load_car_make.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="model">
                                <select class="formSelection model"  name="model" id="model">
                                    <option value="">- Model -</option>                                    
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="country">
                                <select class="formSelection country" name="country" id="country">
                                    <?php 
                                        include('../../../ajax/load_country.php');
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="year_truck">
                                <div id="divYear">
                                    
                                    <select class="fromSelection year" name="year_from" id="year_from">
                                        <option value="">Year</option>
                                        <?php
                                         for($i=date('Y');$i>=date('Y')-20;$i--){
                                                 echo "<option value='$i'>$i</option>";
                                             }
                                         ?>
                                    </select>
                                    <select class="toSelection year_end" name="year_to" id="year_to">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=date('Y');$i>=date('Y')-20;$i--){
                                                 echo "<option value='$i'>$i</option>";
                                             }
                                         ?>
                                    </select>
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="search_truck">
                                <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />                      
                                
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" id="submit_search"  value="" class="search" />
                            </td>
                        </tr>                        
                    </table>
                        <?php

                    }
                    ?>
                    </form>
                