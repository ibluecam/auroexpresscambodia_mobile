
<link type="text/css" href="<?php BASE_RELATIVE."view/theme/jtheme/css/public-support.css"?>"
<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['SUPPORT_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <p><?php echo $_LANG['SUPPORT_HEADER_PLACEHOLDER'];?>:</p>
                <form method="post">
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['SUPPORT_NAME_LABEL'];?><span class="required-field">*</span></span>
                        <input type="text" name="nameInput" placeholder="<?php echo $_LANG['SUPPORT_NAME_PLACEHOLDER'];?>" value="<?php echo (isset($_SESSION['support_name']) ? stripslashes($_SESSION['support_name']) : '' );?>" style="width:550px;" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['SUPPORT_EMAIL_LABEL'];?><span class="required-field">*</span></span>
                        <input type="text" name="emailInput" placeholder="<?php echo $_LANG['SUPPORT_EMAIL_PLACEHOLDER'];?>" value="<?php echo (isset($_SESSION['support_email']) ? stripslashes($_SESSION['support_email']) : '');?>" style="width:550px;" />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['SUPPORT_DEPARTMENT_LABEL'];?><span class="required-field">*</span></span>
                        <select name="deptInput" style="width:564px;">
                            <?php
                                if( isset($_SESSION['support_dept']) )
                                {
                            ?>
                            <optgroup label="<?php echo $_LANG['SUPPORT_SELECTED_OPTION'];?>">
                                <option value="<?php echo stripslashes($_SESSION['support_dept']);?>"><?php echo stripslashes($_SESSION['support_dept']);?></option>
                            </optgroup>
                            <?php
                                }
                            ?>
                            <option value="<?php echo $_LANG['SUPPORT_QUESTION_LABEL'];?>"><?php echo $_LANG['SUPPORT_QUESTION_LABEL'];?></option>
                            <option value="<?php echo $_LANG['SUPPORT_SUGGESTION_LABEL'];?>"><?php echo $_LANG['SUPPORT_SUGGESTION_LABEL'];?></option>
                            <option value="<?php echo $_LANG['SUPPORT_TECH_LABEL'];?>"><?php echo $_LANG['SUPPORT_TECH_LABEL'];?></option>
                            <option value="<?php echo $_LANG['SUPPORT_MARKETING_LABEL'];?>"><?php echo $_LANG['SUPPORT_MARKETING_LABEL'];?></option>
                            <option value="<?php echo $_LANG['SUPPORT_BILLING_LABEL'];?>"><?php echo $_LANG['SUPPORT_BILLING_LABEL'];?></option>
                            <option value="<?php echo $_LANG['SUPPORT_OTHER_LABEL'];?>"><?php echo $_LANG['SUPPORT_OTHER_LABEL'];?></option>
                        </select>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on" style="width:150px;"><?php echo $_LANG['SUPPORT_SUBJECT_LABEL'];?><span class="required-field">*</span></span>
                        <input type="text" name="subjectInput" placeholder="<?php echo $_LANG['SUPPORT_SUBJECT_PLACEHOLDER'];?>" value="<?php echo ( isset($_SESSION['support_subject']) ? stripslashes($_SESSION['support_subject']) : '');?>" style="width:550px;" />
                    </div>
                    <br/>
                    <p><b><?php echo $_LANG['SUPPORT_MESSAGE_LABEL'];?></b></p>
                    <textarea name="messageInput" class="support-ta"><?php echo ( isset($_SESSION['support_message']) ? stripslashes($_SESSION['support_message']) : '');?></textarea>
                    <br/>
                    <label>
                        <input type="checkbox" name="acptInput" value="yes" style="float:left;margin-right:20px;" /><?php echo $_LANG['SUPPORT_READ_A'];?> <a href="<?php echo BASE_RELATIVE;?>view/lib/terms-of-use.html?iframe=true" rel="prettyPhoto"><?php echo $_LANG['SUPPORT_READ_B'];?></a>.
                    </label>
                    <div class="clearfix" style="height:20px;"></div>
                    <input type="submit" name="sendbtn" value="<?php echo $_LANG['SUPPORT_SEND_BUTTON'];?>" class="btn btn-small" />
                </form>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
