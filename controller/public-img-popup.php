<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicImgPopup
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'buyer-directory';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    var $traderList;
    private $car;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;
    var $register_type="";
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicImgPopup($lang=array())
    {
        $this->lang = $lang;
        $this->loadTraderList();
    }

    function loadDealweek(){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT * from deal_week  ORDER BY id DESC limit 5";
        
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
       $this->deal_week = array();
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($this->deal_week, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $this->deal_week; 
        
    }


    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        //echo $this->slug;
        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
    
    /*
     * Public method: get load trader data ------------------------------------
     * @return array | false.
     */
    private function loadTraderList(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $traderList=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if(isset($_GET['keyword'])) $keyword=mysql_real_escape_string(stripcslashes($_GET['keyword'])); else $keyword="";
        //if(isset($_GET['business_type'])) $business_type=mysql_real_escape_string(stripcslashes($_GET['business_type'])); else $business_type="";
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(isset($_GET['register_type'])) $register_type=mysql_real_escape_string(stripcslashes($_GET['register_type'])); else $register_type="";
        // if(isset($_GET['condition'])) $condition=mysql_real_escape_string(stripcslashes($_GET['condition'])); else $condition="";
        // if(isset($_GET['steering'])) $steering=mysql_real_escape_string(stripcslashes($_GET['steering'])); else $steering="";
        // if(isset($_GET['fuel_type'])) $fuel_type=mysql_real_escape_string(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
        // if(isset($_GET['min_price'])) $min_price=mysql_real_escape_string(stripcslashes($_GET['min_price'])); else $min_price="";
        // if(isset($_GET['max_price'])) $max_price=mysql_real_escape_string(stripcslashes($_GET['max_price'])); else $max_price="";
        // if(isset($_GET['make'])) $make=mysql_real_escape_string(stripcslashes($_GET['make'])); else $make="";
        // if(isset($_GET['category'])) $category=mysql_real_escape_string(stripcslashes($_GET['category'])); else $category="";

        if(!empty($country)){
            $where.=" AND `rg`.`country`='$country' ";
        }
        if(!empty($keyword)){
            $where.=" AND (`rg`.`card_id` LIKE '%$keyword%' OR `rg`.`name` LIKE '%$keyword%' OR `rg`.`company_name` LIKE '%$keyword%' OR `rg`.`introduction` LIKE '%$keyword%') ";
        }
        // if(!empty($business_type)){
        //     $where.=" AND `rg`.`business_type` LIKE '%$business_type%' ";
        // }
        if(!empty($product_type)){
            $where.=" AND `pd`.`product_type` LIKE '%$product_type%' ";
        }
        if(!empty($register_type)){
            //$where.=" AND `rg`.`register_type` = '$register_type' ";
            $this->register_type=$register_type;
        }
        // if(!empty($condition)){
        //     $where.=" AND `pd`.`condition` = '$condition' ";
        // }
        // if(!empty($steering)){
        //     $where.=" AND `pd`.`steering` = '$steering' ";
        // }
        // if(!empty($fuel_type)){
        //     $where.=" AND `pd`.`fuel_type` = '$fuel_type' ";
        // }
        // if(!empty($min_price)){
        //     $where.=" AND `pd`.`price` > '$min_price' ";
        // }
        // if(!empty($max_price)){
        //     $where.=" AND `pd`.`price` < '$max_price' ";
        // }
        // if(!empty($make)){
        //     $where.=" AND `pd`.`make` = '$make' ";
        // }
        // if(!empty($category)){
        //     $where.=" AND `pd`.`category` = '$category' ";
        // }
        $query = "SELECT rg.*, 
        cl.country_name as country_name
        FROM register as `rg` 
        INNER JOIN country_list AS cl
        ON rg.country=cl.cc 
        LEFT JOIN product as `pd`
        ON pd.owner=rg.id
        WHERE register_type LIKE '%$this->register_type%' $where  
        GROUP BY `rg`.`id`
        ";
        //echo $query;
        //echo $query;
        /////PAGINATION PROCESS//////

        $sql_count_str=$query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row);
        $limit=$links->start_display;
        $this->pagination_html= $links->display(); 
        $this->current_page = $links->currentPage();
        $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $query.=$limit;  
       //echo $query;
        $result = mysql_query($query);
        
        
        //echo $query;
        $this->traderList = array();
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($traderList, $r);
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        $this->traderList=$traderList;
        $cnx->close();
    }
    
    /*
     * Public method: get number by condition data ------------------------------------
     * @return array | false.
     */
    public function getExpertCompanyNumberByCountry(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();  
        $where="";
        $count="";
        $join="";

        $query = "SELECT `country`, COUNT(rg.id) AS `number` FROM register as rg 
                WHERE rg.register_type LIKE '%$this->register_type%' GROUP BY rg.`country` ";
        $result = mysql_query($query);
        //echo $query;
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            $sellerNumber[$r['country']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        return $sellerNumber;
        $cnx->close();
    }
    
    

    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProductType(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();  

       
        $query = "SELECT pd.product_type, COUNT(DISTINCT(owner)) AS number  
                FROM product as pd
                INNER JOIN register as rg ON pd.owner=rg.id 
                WHERE rg.register_type LIKE '%$this->register_type%' 
                GROUP BY pd.product_type"
        ;

       
        //echo $query;
        $result = mysql_query($query);
        
        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r['product_type']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;
        
    }
    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProduct($column){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open(); 
        $where_product_type=""; 
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(!empty($product_type)){
             $where_product_type=" AND product_type='$product_type'";
        }
        $query = "SELECT pd.`$column`, (SELECT COUNT(DISTINCT(owner)) FROM product WHERE `$column`=pd.`$column`$where_product_type) AS number 
                FROM register as rg 
                LEFT JOIN product as pd ON pd.owner=rg.id 
                WHERE rg.register_type LIKE '%$this->register_type%'
                GROUP BY pd.`$column`  "
        ;
        //echo $query."<br/>";
        $result = mysql_query($query);
        
        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r[$column]]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;
        
    }
    
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getTraderList()
    {
        return $this->traderList;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    /*
     * Public method: get vehicle data -------------------------------------------
     * @return string.
     */

}
?>
