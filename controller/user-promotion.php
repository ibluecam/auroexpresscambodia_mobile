<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class userpromotion
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	
    /*
     * Constructor -------------------------------------------------------------
     */
    public function userpromotion($lang=array())
    {
        $this->lang = $lang;				
		
		if(isset($_POST['btn_add'])){
		   $this->add_promotion();
		}
    }
	
	public function add_promotion(){
		$user_id = $_SESSION['log_id'];
		$title=trim($_POST['txt_title']);		
		
		// Check file upload
		if($_FILES['file']['name']!=""){
			if ($_FILES['file']['size']<10485760){				
				$ext=strtolower(substr($_FILES['file']['name'],-3));
				$fname=date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;
				move_uploaded_file($_FILES['file']['tmp_name'] ,BASE_ROOT."/img/user_promotion/upload/".$fname);	
			}
			$fname = BASE_RELATIVE."img/user_promotion/upload/".$fname;
		}		
		
		require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

		$sql_insert="INSERT INTO tb_promotion (title, photo_url, create_date, user_id, status)
		VALUES ('$title', '$fname', '".date('Y-m-d')."', '$user_id', '0')";
		$result=@mysql_query($sql_insert);
		/* if($result){
			echo "<script> alert('Submit Success')</script>";
		} */

	}
	
	
}
?>