<?php
$_LANG = array(
	'FORGOT_USER_ID_AND_PASSWORD'=>'Forgot User ID or Password',
	'FORGOT_FILL_EMAIL'=>'Please fill in your email in form below.',
	'FORGOT_USER_ID'=>'I forgot my user ID',
	'FORGOT_PASSWORD'=>'I forgot my password',
	'INPUT_EMAIL'=>'Enter your email address',
	'BTN_SEND'=>'SEND'
);
?>