/* handle next form (btn click) */
var cur_step = 1;
var cur_img;
var tab;
var next_btn;
var prev_btn;
var img_st1;

$(document).ready(function(){
    tab = document.getElementById('content1');
    next_btn = document.getElementById('nextbtn');
    prev_btn = document.getElementById('prevbtn');
    img_st1 = document.getElementById('step1');
    
    opt1 = document.getElementById('opt1');
    opt2 = document.getElementById('opt2');
    vid  = document.getElementById('video-wrap');
    upl  = document.getElementById('upload-wrap');
    
    cur_img = img_st1;
	
});

function nextFormHandler(){
    if( tab == undefined || next_btn == undefined || prev_btn == undefined ){
        console.log('undefined objects cannot be set!');
        return false;
    }
    
    cur_step += 1;
    
    // display image step tracker.
    cur_img.style.display = 'none';
    
    var iname = 'step' + cur_step;
    cur_img = document.getElementById(iname);
    cur_img.style.display = 'block';
    
    // display previous button.
    document.getElementById('prevbtn').style.display = 'inline-block';
    
    // disable next button if reaches step 4 and submit button.
    if( cur_step >= 4 ){
        document.getElementById('nextbtn').style.display = 'none';
        document.getElementById('submitbtn').style.display = 'inline-block';
    }
    
    var aname = 'content' + cur_step;
    
    $(tab).fadeOut();
    
    tab = document.getElementById(aname);
    
    $(tab).fadeIn();
    
    return false;
}

/* handle previous form */
function previousFormHandler(){
    if( tab == undefined || next_btn == undefined || prev_btn == undefined ){
        console.log('undefined objects cannot be set!');
        return false;
    }
    
    // hide submit button.
    console.log('current step: ' + cur_step);
    
    if( cur_step >= 3 ){
        document.getElementById('submitbtn').style.display = 'none';
    }
    
    cur_step -= 1;
    
    // display image step tracker.
    cur_img.style.display = 'none';
    
    var iname = 'step' + cur_step;
    cur_img = document.getElementById(iname);
    cur_img.style.display = 'block';
    
    // display next button.
    document.getElementById('nextbtn').style.display = 'inline-block';
    
    // hide previous if first page.
    if( cur_step <= 1 ){
        document.getElementById('prevbtn').style.display = 'none';
    }
    
    var aname = 'content' + cur_step;
    
    $(tab).fadeOut();
    
    tab = document.getElementById(aname);
    
    $(tab).fadeIn();
    
    return false;
}

/* redefine wysiwyg */
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
});

/* handle step4 selectors */
var opt1;
var opt2;
var vid;
var upl;
function validateImageType(){
    if( opt1 == undefined || opt2 == undefined || vid == undefined || upl == undefined ){
        opt1 = document.getElementById('opt1');
        opt2 = document.getElementById('opt2');
        vid  = document.getElementById('video-wrap');
        upl  = document.getElementById('upload-wrap');
    }
    
    var val = opt1.options[opt1.selectedIndex].value;
    
    // disable and hide all controllers.
    if( val == 'none' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
    
    // display video controller.
    else if( val == 'video' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'inline-block';
        upl.style.display = 'none';
    }
    
    // display option 2 but hide controllers.
    else {
        opt2.disabled = false;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
}

function displayController(url){
    var val = opt2.options[opt2.selectedIndex].value;
    
    // hide controllers if none.
    if( val == 'none' ){
        
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
    
    // display image uploader.
    else if( val == 'interior' || val == 'exterior' ){
        vid.style.display = 'none';
        upl.style.display = 'inline-block';
    }
    
    // display video field.
    else {
        vid.style.display = 'inline-block';
        upl.style.display = 'none';
    }
    
    // add interior/exterior to iframe src.
    document.getElementById('iupload').src = url + '&mode=' + val;
    
}

/* handle save video url form */
function saveVideoHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }
    
    var value = document.getElementById('videoUrlInput').value;
    
    if( value == undefined || value.length < 1 ){
        return false;
    }
    
    // disable save button.
    document.getElementById('vidBtn').disabled = true;
    
    // send ajax request.
    $.post(
        '../../ajax/save_video_url.php',
        {
            carid: cid,
            url: value
        },
        function(data,status){
            var result = data.split(',');
            
            document.getElementById('videoUrlInput').value = ''; // enpty video url input field.
            document.getElementById('vidBtn').disabled = false;   // enable button.
            
            if( result[0] == 'ir' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var itemid = 'link' + Math.floor((Math.random()*1000)+1);
                div.innerHTML += '<p id="'+itemid+'"><b>Video URL:</b> ' + result[1] + '<a href="#" onclick="removeVideo(\''+result[1]+'\',\''+cid+'\',\''+itemid+'\'); return false;" class="btn btn-small btn-danger" style="float:right;">×</a></p>';
                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}

/* handle upload image form */
function uploadImageHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }
    
    
}

/* remove video link */
function removeVideo(url,carid,itemid){
    if( url == undefined || carid == undefined || itemid == undefined ){
        return false;
    }
    
    $.post(
        '../../ajax/remove_vid_url.php',
        {
            cid: carid,
            dest: url
        },
        function(data,status){
            var result = data.split(',');
                        
            if( result[0] == 'ng' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var p = document.getElementById(itemid);
                
                if(  p != undefined ){
                    div.removeChild(p);
                }
                
                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}

/* handle remove image */
function removeImageHandler(url,divid){
    // validate.
    if( url == undefined || url == null ){
        console.log('URL is undefined.');
        return false;
    }
    
    var d = document.getElementById(divid);
    
    if( d == undefined ){
        console.log('Image divider is undefined.');
        return false;
    }
    
    // send ajax request.
    $.post(
        '../../ajax/remove_car_image.php',
        {
            loc: url
        },
        function(data,status){
            var result = data.split(',');
            
            if( result[0] == 'ng' ){
                alert('Unable to remove image due an internal error.');
                console.log(result[0] + ':' + result[1]);
                return false;
            }
            else if( result[0] == 'ok' ){
                // remove image.
                var wrap = document.getElementById('outoutimg');
                wrap.removeChild(d);
                return false;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}