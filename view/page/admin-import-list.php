<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['IMPORT_HEADER'];?></h1>
                <p>
                    <span class="label label-important"><?php echo $_LANG['IMPORT_IMPORTANT_LABEL'];?></span>
                    <?php echo $_LANG['IMPORT_IMPORTANT_MESSAGE'];?>
                </p>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>
                <form method="post" enctype="multipart/form-data">
                    <div class="input-append">
                        <input type="file" name="csv_file" style="width:450px;border: 1px solid #CCC;height:28px;"/>
                        <input type="submit" name="importbtn" value="<?php echo $_LANG['IMPORT_BUTTON_LABEL'];?>" class="btn"/>
                    </div>
                </form>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}