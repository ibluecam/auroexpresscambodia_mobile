<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';

// load page content.
$pg_details = $_CLASS->getPageHTML();
$deal_week = $_CLASS->loadDealweek();


if(isset($_GET['country'])) $country=htmlspecialchars(stripcslashes($_GET['country'])); else $country="";
//if(isset($_GET['business_type'])) $business_type=htmlspecialchars(stripcslashes($_GET['business_type'])); else $business_type="";
if(isset($_GET['product_type'])) $product_type=htmlspecialchars(stripcslashes($_GET['product_type'])); else $product_type="";
if(isset($_GET['keyword'])) $keyword=htmlspecialchars($_GET['keyword']); else $keyword="";
// if(isset($_GET['condition'])) $condition=htmlspecialchars(stripcslashes($_GET['condition'])); else $condition="";
// if(isset($_GET['steering'])) $steering=htmlspecialchars(stripcslashes($_GET['steering'])); else $steering="";
// if(isset($_GET['fuel_type'])) $fuel_type=htmlspecialchars(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
// if(isset($_GET['min_price'])) $min_price=htmlspecialchars(stripcslashes($_GET['min_price'])); else $min_price="";
// if(isset($_GET['max_price'])) $max_price=htmlspecialchars(stripcslashes($_GET['max_price'])); else $max_price="";
// if(isset($_GET['make'])) $make=htmlspecialchars(stripcslashes($_GET['make'])); else $make="";
// if(isset($_GET['category'])) $category=htmlspecialchars(stripcslashes($_GET['category'])); else $category="";


$traderList= $_CLASS->getTraderList();
$pagination=$_CLASS->pagination_html;
$total_row=$_CLASS->total_num_row;
$current_page=$_CLASS->current_page;
$total_page=$_CLASS->total_page;
$countryNumber=$_CLASS->getForwarderNumberByCountry();

//$numberByProductType = $_CLASS->getNumberByProductType();
// $numberByCondition = $_CLASS->getNumberByProduct("condition");
// $numberBySteering = $_CLASS->getNumberByProduct("steering");
// $numberByFuelType = $_CLASS->getNumberByProduct("fuel_type");

// $numberByMaxPrice['3000']=$_CLASS->getNumberByPrice('3000', '<');
// $numberByMaxPrice['5000']=$_CLASS->getNumberByPrice('5000', '<');
// $numberByMaxPrice['10000']=$_CLASS->getNumberByPrice('10000', '<');
// $numberByMinPrice['15000']=$_CLASS->getNumberByPrice('15000', '>');
// $numberByMake=$_CLASS->getNumberByProduct('make');
// $numberByCategory=$_CLASS->getNumberByProduct('category');
//if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";

?>



<div id="sectionContenWrapper">
        	<div id="carMenu">
            	<ul class="clearfix">
                	<li class="first_child"><a href="#" class="linkfade">Find Trader</a> > </li>
                    <li class="menu">
                    	Fowarder's Directory
                  </li>
                </ul>
            </div>
        	<!--sectionSidebar--> 
            
            <?php 				
				include("php/sidebar/findtrader.php");	 	
				include("php/sidebar/bottom.php");	 				 						 			
			?>    

        	<!--end sectionSidebar-->
            
            <div id="sectionContent" class="clearfix">            	
                <div class="findtrader_title">
                    
                        <p>Fowarder's Directory</p>                	
                </div>       
                    <p class="detail">
                    The total of 8,987 buyers have been currently registered at Global iBlue.<br/>
Buyers who are on the list of buyer's directory  are Global iBlue's premium memebers as well as the experts in import & export business.<br/>
Please search by categories and find out the best sutiable supplier for your business here!
                    </p>             
            	

				          
           		<div id="tab_all" style="display:block">
                    <form id="forwarder_search" action="forwarder-directory" method="get">
                    	<table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="label">Country</td>
                                <td class="list" colspan="3">
                                    <select name="country">
                                        <?php 
                                            include('ajax/get_country_list.php');
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        	<tr>
                            	<td class="label"></td>
                                <td class="list" colspan="3">
                                	<ul>
                                    	<li><img src="<?php echo BASE_RELATIVE;?>images/flag/kr.png" /> <a <?php if($country=='kr') echo "class='selected_filter'";?> href="forwarder-directory?country=kr">S. Korea (<?php if(isset($countryNumber['kr'])) echo $countryNumber['kr']; else echo "0";?>)</a></li>
                                        <li><img src="<?php echo BASE_RELATIVE;?>images/flag/jp.png" /> <a <?php if($country=='jp') echo "class='selected_filter'";?> href="forwarder-directory?country=jp">Japan (<?php if(isset($countryNumber['jp'])) echo $countryNumber['jp']; else echo "0";?>)</a></li>
                                        <li><img src="<?php echo BASE_RELATIVE;?>images/flag/us.png" /> <a <?php if($country=='us') echo "class='selected_filter'";?> href="forwarder-directory?country=us">U.S.A (<?php if(isset($countryNumber['us'])) echo $countryNumber['us']; else echo "0";?>)</a></li>
                                        <li><img src="<?php echo BASE_RELATIVE;?>images/flag/tr.png" /> <a <?php if($country=='tr') echo "class='selected_filter'";?> href="forwarder-directory?country=tr">Turkey (<?php if(isset($countryNumber['tr'])) echo $countryNumber['tr'];  else echo "0";?>)</a></li>
                                        <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cl.png" /> <a <?php if($country=='cl') echo "class='selected_filter'";?> href="forwarder-directory?country=cl">Chile (<?php if(isset($countryNumber['cl'])) echo $countryNumber['cl'];  else echo "0";?>)</a></li>
                                        <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cn.png" /> <a <?php if($country=='cn') echo "class='selected_filter'";?> href="forwarder-directory?country=cn">China(<?php if(isset($countryNumber['cn'])) echo $countryNumber['cn'];  else echo "0";?>)</a></li>
                                        
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">Keyword:</td>
                                <td class="list" colspan="3">
                                    <input type="text" id="keywordInput" value="<?php echo $keyword;?>" name="keyword"/><input type="submit" name="submit" id="submit" value="Search">
                                </td>
                            </tr>

                        </table>
                    </form>
                </div>
                
                
                <?php 
                if(count($traderList)>0){
                foreach($traderList as $trader){
                ?>
                <div class="seller_wrap clearfix">
                	<div class="left_item">
                    <?php 
                        $image_src="images/seller-directory/empty.jpg";
                        if(!empty($trader['image'])&&file_exists("upload/thumb/".$trader['image'])){
                            $image_src="upload/thumb/".$trader['image'];
                        }
                    ?>
                    	<a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade"><img src="<?php echo $image_src;?>" /></a>
                        <p><a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade">Visit Buyer's Homepage</a></p>
                    </div>
                    <div class="right_item">
                    	<p class="title"><a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade"><?php echo $trader['company_name'];?></a></p>
                        <ul>
                        	<li><img src="<?php echo BASE_RELATIVE;?>images/flag/<?php echo $trader['country'];?>.png" /> <?php echo $trader['country_name'];?></li>
                            <li>ID : <?php echo $trader['card_id'];?></li>
                            <li>Name : <?php echo $trader['name'];?></li>
                            <?php
								if($trader['messengerID'] != ''){
							?>
									<li>Skype : <a href="skype:<?php echo $trader['messengerID'];?>?add"><?php echo $trader['messengerID'];?></a></li>
							<?php
								}
							?>
                            <li>Email : <?php echo $trader['email'];?></li>
                            
                        </ul>
                        <div class="data_detail">
                        	<p>
                            	<?php echo $trader['introduction'];?>
                            </p>
                            <br/>
                            <p class="more"><a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade"><< More >> </a></p>
                        </div>
                    </div>
                </div>
                <?php }}else{
                    echo "<div style='padding-top:20px;text-align:center;'><font color='red'>..:: Sorry there is no sellers found for your inquiry at the moment! ::..</font></div>";
                }

                ?>

                <div id="pagernation">
                     <?php 
      
                         echo $pagination;
                                            
                    ?>
                      
                </div>
                      
        	</div><!-- end div id="sectionContent" -->  
        </div>      
        