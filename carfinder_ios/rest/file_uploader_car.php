<?php

    require_once '../models/PhotoCar.php';
    require '../controllers/ControllerRestPhotoCar.php';
    require_once '../models/User.php';
    require '../controllers/ControllerUser.php';
    
    $controllerRestPhotoCar = new ControllerRestPhotoCar('../application/DB_Connect.php');
    $controllerUser = new ControllerUser('../application/DB_Connect.php');

    if( !empty($_POST['user_id']) )
        $user_id = $_POST['user_id'];

    if( !empty($_POST['login_hash']) )
        $login_hash = $_POST['login_hash'];

    $car_id = 0;
    if( !empty($_POST['car_id']) )
        $car_id = $_POST['car_id'];

    $photo_id = 0;
    if( !empty($_POST['photo_id']) )
        $photo_id = $_POST['photo_id'];

    $photo_url = "";
    if( !empty($_POST['photo_url']) )
        $photo_url = trim(strip_tags($_POST['photo_url']));

    $thumb_url = "";
    if( !empty($_POST['thumb_url']) )
        $thumb_url = trim(strip_tags($_POST['thumb_url']));

    $is_deleted = 0;
    if( !empty($_POST['is_deleted']) )
        $is_deleted = $_POST['is_deleted'];

    if( !empty($car_id) && !empty($photo_id) > 0 && !empty($login_hash) && !empty($user_id) && $is_deleted == 1 ) {
        $user = $controllerUser->getUserByUserId($user_id);

        $login_hash = str_replace(" ", "+", $login_hash);
        if($user != null) {
            
            if($user->login_hash == $login_hash) {
                $controllerRestPhotoCar->deletePhotoCar($photo_id, 1);
                $json = "{
                    \"photo_car_info\" : {
                                  \"photo_id\" : \"$photo_id\",
                                  \"is_deleted\" : \"1\"
                                  },
                    \"status\" : {
                                  \"status_code\" : \"-1\",
                                  \"status_text\" : \"Success.\"
                                }
                    }";
            }
            else {
                $json = "{
                        \"status\" : {
                                      \"status_code\" : \"5\",
                                      \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                    }
                        }";
            }
        }
        else {
            $json = "{
                  \"status\" : {
                                \"status_code\" : \"5\",
                                \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                              }
                  }";
        }
    }
    else if( !empty($car_id) && !empty($photo_id) >= 0 && !empty($login_hash) && !empty($user_id) ) {
          
        $user = $controllerUser->getUserByUserId($user_id);

        $login_hash = str_replace(" ", "+", $login_hash);
        if($user != null) {
            
            if($user->login_hash == $login_hash) {

                $photoCar = $controllerRestPhotoCar->getPhotoCarByPhotoId($photo_id);

                $itm = new PhotoCar();
                $itm->photo_id = $photo_id;
                $itm->photo_url = $photo_url;
                $itm->thumb_url = $thumb_url;
                $itm->car_id = $car_id;

                if( !empty($_FILES["thumb_file"]["name"]) && !empty($_FILES["photo_file"]["name"]) ) {

                    $desired_dir = IMAGE_UPLOAD_DIR;
                    $desired_dir_path = "../".IMAGE_UPLOAD_DIR;

                    if(is_dir($desired_dir_path)==false) {
                        // Create directory if it does not exist
                        mkdir("$desired_dir_path", 0700);        
                    }

                    $id =  uniqid();
                    $temp = explode(".", $_FILES["thumb_file"]["name"]);
                    $extension = end($temp);
                    $thumb_new_file_name = $desired_dir."/"."thumb_".$id.".".$extension;
                    $thumb_new_file_name_path = $desired_dir_path."/"."thumb_".$id.".".$extension;
                    move_uploaded_file($_FILES['thumb_file']['tmp_name'], $thumb_new_file_name_path);
                    $itm->thumb_url = ROOT_URL.$thumb_new_file_name;


                    $id =  uniqid();
                    $temp = explode(".", $_FILES["photo_file"]["name"]);
                    $extension = end($temp);
                    $photo_new_file_name = $desired_dir."/"."photo_".$id.".".$extension;
                    $photo_new_file_name_path = $desired_dir_path."/"."photo_".$id.".".$extension;
                    move_uploaded_file($_FILES['photo_file']['tmp_name'], $photo_new_file_name_path);
                    $itm->photo_url = ROOT_URL.$photo_new_file_name;



                    if($photoCar != null) {
                        $controllerRestPhotoCar->updatePhotoCar($itm);
                        $photo_id = $itm->photo_id;
                    }
                    else {
                        $controllerRestPhotoCar->insertPhotoCar($itm); 
                        $photo_id = $controllerRestPhotoCar->getLastInsertedPhotoId();
                    }

                    $itm = $controllerRestPhotoCar->getPhotoCarByPhotoId($photo_id);

                    $json = "{
                            \"photo_car_info\" : {
                                          \"photo_id\" : \"$itm->photo_id\",
                                          \"photo_url\" : \"$itm->photo_url\",
                                          \"thumb_url\" : \"$itm->thumb_url\",
                                          \"car_id\" : \"$itm->car_id\",
                                          \"is_deleted\" : \"0\"
                                          },
                            \"status\" : {
                                          \"status_code\" : \"-1\",
                                          \"status_text\" : \"Success.\"
                                        }
                            }";

                }
                else {
                    if($photoCar != null) {
                        $controllerRestPhotoCar->updatePhotoCar($itm);
                        $photo_id = $itm->photo_id;
                    }
                    else {
                        $controllerRestPhotoCar->insertPhotoCar($itm); 
                        $photo_id = $controllerRestPhotoCar->getLastInsertedPhotoId();
                    }

                    $itm = $controllerRestPhotoCar->getPhotoCarByPhotoId($photo_id);

                    $json = "{
                            \"photo_car_info\" : {
                                          \"photo_id\" : \"$itm->photo_id\",
                                          \"photo_url\" : \"$itm->photo_url\",
                                          \"thumb_url\" : \"$itm->thumb_url\",
                                          \"car_id\" : \"$itm->car_id\",
                                          \"is_deleted\" : \"0\"
                                          },
                            \"status\" : {
                                          \"status_code\" : \"-1\",
                                          \"status_text\" : \"Success.\"
                                        }
                            }";
                }

            }
            else {
                $json = "{
                          \"status\" : {
                                        \"status_code\" : \"5\",
                                        \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                      }
                          }";
            }
                
        }
        else {
            $json = "{
                          \"status\" : {
                                        \"status_code\" : \"5\",
                                        \"status_text\" : \"It seems you are out of sync. Please relogin again.\"
                                      }
                          }";
        }
        
    }
    else {

        $json = "{
                  \"status\" : {
                                \"status_code\" : \"3\",
                                \"status_text\" : \"Invalid Access.\"
                              }
                  }";

        
    }

    echo $json;


?>