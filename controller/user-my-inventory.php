<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class userMyInventory
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'vehicle';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    private $vehicle;
    private $news_list; 
    private $special_offer;
    protected $total_num_row;
    var $pagination_html;
	private $getdatacounter;

    /*
     * Constructor -------------------------------------------------------------
     */
    public function userMyInventory($lang=array())
    {
       $this->lang = $lang;
      //  $this->loadVehicleData();
       // $this->loadSpecialOffer();
     //   $this->loadNewsList();
       $user_detail=$this->getUserDetail();
       /*if(($user_detail['member_type1']!='Seller'&&$user_detail['member_type1']!='Both')
            ||$user_detail['email']==''
            ||$user_detail['country']==''
            ||$user_detail['company_name']==''
        ){
            //Invalid Seller
            header("Location: edit-myinfo?msg=incomplete_seller");
       }*/
	   
	   /* if($user_detail['email']==''||$user_detail['company_name']==''){
            //Invalid Seller
            header("Location: edit-myinfo?msg=incomplete_seller");
			
		
        } */
	   
	   
	   
	   
	   $this->loaddatacounter();
        
    }
	private function getUserDetail(){
        require_once BASE_CLASS . 'class-connect.php';        
        $cnx = new Connect();
        $cnx->open();
        $user_id=$_SESSION['log_id'];
        $user_detail=array();
        $sql="SELECT member_type1, email, province, company_name FROM register WHERE `user_id`='$user_id' ";
        $result=mysql_query($sql);
        while($row=mysql_fetch_assoc($result)){
            $user_detail=$row;
        }
        $cnx->close();
        return $user_detail;
    }
	public function loadReserve($product_id){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        $sql_query = "SELECT * FROM product WHERE status='Reserved' AND car_id='$product_id'";

        if( !$sql = mysql_query($sql_query) ){
            die('Unable to load reserve. '. __FILE__ .':'. __LINE__);
			$cnx->close();
        }
		
        $cnx->close();
		
		return mysql_num_rows($sql);
    }
	
	public function loadSold($product_id){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        $sql_query = "SELECT * FROM product WHERE status='Sold' AND car_id='$product_id'";

        if( !$sql = mysql_query($sql_query) ){
            die('Unable to load sold. '. __FILE__ .':'. __LINE__);
			$cnx->close();
        }
		
        $cnx->close();
		
		return mysql_num_rows($sql);
    }
	
	public function loadNew($product_id){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        $sql_query = "SELECT * FROM product WHERE created_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND id='$product_id'";

        if( !$sql = mysql_query($sql_query) ){
            die('Unable to load New Car. '. __FILE__ .':'. __LINE__);
			$cnx->close();
        }
		
        $cnx->close();
		
		return mysql_num_rows($sql);
    }

    function loadDealweek(){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT * from deal_week  ORDER BY id DESC limit 5";
        
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
       $this->deal_week = array();
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($this->deal_week, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $this->deal_week; 
        
    }
    
    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	/*
     * private method: get name and price of vehicle ------------------------------------------
     * @return bool.
     */
    private function loadVehicleData(){


    }
    function countCountry($product_type=""){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $and="";
        $countcountry=array();
        $and=" AND (`member_type1` LIKE '%seller%' OR `member_type1` LIKE '%forwarder%')";
        if(!empty($product_type)){
            $and.= " AND `pd`.`product_type` = '$product_type'";
        }
        

        //if(!empty($vehicle_type)){
        //  $and.= " AND `vehicle_type`='$vehicle_type'";
        //}

        
        $sql_str="
                SELECT country, cl.country_name, COUNT(DISTINCT(rg.user_id)) as `number`
                FROM `register` as `rg`
                LEFT JOIN `product` as `pd` ON `pd`.`owner`=`rg`.`user_id`
                INNER JOIN `country_list` as cl ON `cl`.`cc`=`rg`.`country`
                WHERE 1$and

                GROUP BY `rg`.`country`
                ORDER BY `number` DESC
                LIMIT 0, 10
                ";

        
        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }
        
        while($row=@mysql_fetch_assoc($sql)){
            array_push($countcountry, $row);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
        return $countcountry;

    }
    private function loadSpecialOffer(){
        if(isset($_GET['product_type'])){
            require_once BASE_CLASS . 'class-connect.php';
            $cnx = new Connect();
            $cnx->open();

            $query = "SELECT p.*, c.source as carImg, cl.country_name as country,
                            cl.cc as flag ,cl.country_name, rg.company_name
                            FROM (product as p LEFT JOIN car_media as c on p.id=c.product_id AND c.primary_photo=1 OR c.primary_photo=2)
                            INNER JOIN country_list as cl on p.location=cl.cc 
                            INNER JOIN register as rg on p.owner=rg.user_id
                        WHERE product_type = '{$_GET['product_type']}' AND special_offer = 1 AND del_flag = 0";

            if( !$result = @mysql_query($query) ){
                require_once BASE_CLASS . 'class-log.php';
                LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
                $cnx->close();
                return;
            }

            if( @mysql_num_rows($result) > 1 ){
                $this->special_offer = array();
                while( $r = @mysql_fetch_assoc($result) ){
                    array_push($this->special_offer, $r);
                }
                @mysql_free_result($result);
            }

            $cnx->close();
        }
    }

    private function loadNewsList(){
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';
        
        $cnx = new Connect();
        $cnx->open();
                
        // load news.
        if( !$sql = @mysql_query("SELECT * FROM `news` ORDER BY `id` DESC LIMIT 6" ) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
            
            LogReport::write('Unable to load news list due a query error at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->news_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }
        
        ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );
        
        while( $r = @mysql_fetch_assoc($sql) ){
            $day = explode('-',$r['date']);
            $date = Utilities::checkDateFormat($day[0],$day[1],$day[2],$format);
            $date=date("F j, Y",strtotime($r['date']));
            $obj = array(
                'id' => $r['id'],
                'title' => stripslashes($r['title']),
                'type' => stripslashes($r['product_type']),
                'html' => stripslashes($r['html']),
                'date' => $date,
                'time' => $r['time'],
                'author' => stripslashes($r['author'])
            );
            array_push($this->news_list,$obj);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }

	
	 private function loaddatacounter(){	 
		//Connection
        require_once BASE_CLASS . 'class-connect.php';        
        $cnx = new Connect();
        $cnx->open();
        $and="";
        if(isset($_GET['owner'])) $owner=mysql_real_escape_string(stripslashes($_GET['owner'])); else $owner='';
        if($_SESSION['log_group']=='user'){
            $owner=mysql_real_escape_string(stripslashes($_SESSION['log_id']));
            $and="AND register_id='$owner'";
        }else{
            if(empty($owner)){
                $and="";
            }else{
                $and="AND register_id='$owner'";
            }
        }
        // load Data.
        
        $query_str="SELECT 
                        SUM(car_count) as car_count,
						SUM(suv_count) as suv_count,
						SUM(van_count) as van_count,
						SUM(pickup_count) as pickup_count, 
                        SUM(truck_count) as truck_count,
                        SUM(bus_count) as bus_count,
                        SUM(part_count) as part_count,
                        SUM(accessories_count) as accessories_count,
                        SUM(equipment_count) as equipment_count,
                        SUM(watercraft_count) as watercraft_count,
                        SUM(aircraft_count) as aircraft_count,
                        SUM(motorbike_count) as motorbike_count
                    FROM data_count
                    WHERE 1 $and" ;
        //echo $query_str;
        if( !$sql = @mysql_query($query_str) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();
			//For Error Message
            LogReport::write('Unable to load news list due a query error at ' . __FILE__ . ':' . __LINE__);            
          //  $this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        $this->news_list = array();
        
        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }

        $this->getdatacounter=array();
        while( $r = @mysql_fetch_assoc($sql) ){   
          
        
            array_push($this->getdatacounter,$r);
        }
        
        @mysql_free_result($sql);
        $cnx->close();
    }
	
    
	
    public function getTotalItems(){
        return $this->total_num_row;
    }
    public function getNewsList(){
        return $this->news_list;
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
	/*
     * Public method: get vehicle data -------------------------------------------
     * @return string.
     */
	public function getVehicle(){
        return $this->vehicle;
    }

    public function getSpecialOffer(){
        return $this->special_offer;
    }
	
	public function call_loaddatacounter(){
        return $this->getdatacounter;
    }

}
?>
