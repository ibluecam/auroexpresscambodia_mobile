var loading_html="<table class='model_table' id='model_table'><tr><th></th><th>N<sup>o</sup></th><th>Make</th><th></th></tr></table>";
loading_html+='<img id="loading_image" src="images/loading_75.gif" style="position:absolute;left:0;right:0;margin:auto;z-index:999;margin-top:20px;"/>';
$(document).ready(function($) {

	//showLoading();

    $(".top_tab").each(function(e){
        if($(this).attr("value")==product_type){
            $('#admin_tab ul li').removeClass('current');
            $(this).addClass('current');
            $("#product_type").val(product_type);
        }
    });
	$("#makeSelect").load("ajax/admin_saved_make_options.php?product_type="+product_type, function(e){
   		if(make!=''){
            $("#makeSelect").val(make);
            $("#model_table_wrapper").load("ajax/admin_saved_model_table.php?product_type="+product_type+"&make="+make, function(e){
        
            });
        }
	});
    $("#model_table_wrapper").load("ajax/admin_saved_model_table.php?product_type="+product_type, function(e){
        
    });
	$('#admin_tab ul li').click(function(){
		$('#admin_tab ul li').removeClass('current');
		$(this).addClass('current');
		product_type=$(this).attr("data-value");
		$("#product_type").val(product_type);
		showLoading();
        $("#makeSelect").load("ajax/admin_saved_make_options.php?product_type="+product_type, function(e){
            //alert($(this).val());
            make=$(this).val();
            $("#model_table_wrapper").load("ajax/admin_saved_model_table.php?product_type="+product_type+"&make="+make, function(e){
        
            });
        });

		
	});
    $(document.body).on('change', "#makeSelect", function(e){
        make=$(this).val();
        showLoading();
        $("#model_table_wrapper").load("ajax/admin_saved_model_table.php?product_type="+product_type+"&make="+make, function(e){
        
        });
    });
	$(document.body).on('click', "#delete_selected", function(e){
        if(confirm("Are you sure want to delete all products with the selected cities?")==false) return false;
    	
    });
    $(document.body).on('click', ".edit_button", function(e){
        var current_id=$(this).attr('edit_id');
        var current_model=$("#model_"+current_id).text();

        if(!$("#new_model_"+current_id).length){
            $("#model_"+current_id).html(function(e){
                return "<input type='text' class='model_text' value='"+current_model+"' id='new_model_"+current_id+"' name='new_model["+current_id+"]'/>";
            });
        }
        return false;
    });
    
    $(document.body).on('click', ".delete_button", function(e){
        
        $(".model_check").prop('checked', false);
        var delete_id=$(this).attr('delete_id');
        $("#model_check_"+delete_id).prop('checked', true);
        $("#delete_selected").click();
    });
});

function load_ready(){   
    
   
}

function showLoading(){    
    $("#model_table_wrapper").html(loading_html);
}