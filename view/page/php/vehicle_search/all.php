


                
                    
                    <?php
                        if ($_SESSION['mobile']==false) {
                    ?>
                    <div id="second_search">
                        <div id="search_title" class="clearfix">
                            <p><a href="#" class="current" id="quick_search">Quick Search</a></p>
                        </div>
                        <div id="search_wrap" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                
                                <select class="make formSelection" name="make" id="make" style="width:225px;">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>


                                <select class="model formSelection" name="model" id="model">
                                    <option value="" class="notranslate">- Model -</option>
                                </select>
                                
                                    <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />
                                    <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div>
                        <!--======================== ADVANCE SEARCH =====================================-->
                        <!-- <div id="advance_search" class="clearfix">
                            <form method="get" action="vehicle">
                                <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>
                                <input type="hidden" name="search" value="advance"/>
                                <select class="truck_first category formSelection" name="category" id="category_adv">
                                    <?php 
                                        include('ajax/load_saved_category.php');
                                    ?>
                                </select>
                                <select class="truck make formSelection" name="make" id="make_adv">
                                    <?php
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                                
                                
                                <select class="truck model formSelection" name="model" id="model_adv">
                                    <option value="">-- Model --</option>
                                </select>
                                <select class="truck condition formSelection " name="condition" id="condition_adv">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                    
                                </select>
                                <select class="truck_last steering formSelection" name="steering" id="steering_adv">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>
                                <select style="width:130px;" class=" formSelection " name="country" id="country_adv">
                                    <?php include('ajax/load_country.php'); ?>
                                </select>
                                <select class=" formSelection " style="width:150px;" name="transmission" id="transmission_adv">
                                    <?php include('ajax/load_saved_transmission.php'); ?>
                                </select>
                                
                                <select style="width:150px;" class=" formSelection " name="fuel_type" id="fuel_type_adv">
                                    <?php
                                        include('ajax/load_saved_fuel_type.php');
                                    ?>
                                </select>
                                
                                <span class="displacement"> Displacement </span>
                                <select style="width:103px;" class="truck_any formSelection " name="engine_volume_from" id="engine_volume_from_adv">
                                    <option value="">--Any--</option>
                                    <?php 
                                        for($i=500;$i<=6000;$i+=100){
                                            echo "<option value='{$i}'>{$i}</option>";
                                        }
                                    ?>
                                </select>
                                <select style="width:103px;" class="truck_any formSelection " name="engine_volume_to" id="engine_volume_to_adv">
                                    <option value="">--Any--</option>
                                    <?php 
                                        for($i=500;$i<=6000;$i+=100){
                                            echo "<option value='{$i}'>{$i}</option>";
                                        }
                                    ?>
                                </select>
                                
                                <input type="submit" value="" class="search linkfade" />
                            </form>
                        </div> -->                  
                    </div>
                    <!-- <table border="0" cellspacing="0" cellpadding="0">                    
                      <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>                                                          
                        <tr>
                            <td class="top condition">                                
                                <select class="condition formSelection" name="condition" id="condition">
                                    <option value="">- Condition -</option>
                                    <option value="New">New</option>
                                    <option value="Used">Used</option>
                                   
                                </select>                                
                            </td>
                            <td class="top category">                                
                                <select class="category formSelection" name="category" id="category">
                                    <?php 
                                        include('ajax/load_category.php');
                                    ?>
                                </select>                                
                            </td>
                            <td class="top category2">                                
                                <select class="category2 formSelection" name="category2" id="category2">
                                    <option value="">- Category 2 -</option>
                                    
                                </select>                                
                            </td>
                            <td class="make">
                                <select class="make formSelection" name="make" id="make">
                                    <?php 
                                        include('ajax/load_car_make.php');
                                    ?>
                                </select>
                            </td>
                            <td class="model">
                                <select class="model formSelection" name="model" id="model">
                                    <option value="">-- Model --</option>                                    
                                </select>
                            </td>
                            <td>
                                <select class="formSelection" name="steering" id="steering">
                                    <option selected value="">- Steering -</option>
                                    <option value="LHD">LHD</option>
                                    <option value="RHD">RHD</option>
                                </select>
                            </td>
                            <td class="country">
                                <select class="country formSelection" name="country" id="country">
                                    <?php 
                                        include('ajax/load_country.php');
                                    ?>
                                </select>
                            </td>
                            <td class="year_truck">
                                <div id="divYear">
                                    
                                    <select class="fromSelection year" name="year_from" id="year_from">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                    ~
                                    <select class="toSelection year" name="year_to" id="year_to">
                                        <option value="">Year</option>
                                         <?php
                                         for($i=1990;$i<=2014;$i++){
                                             echo "<option value='$i'>$i</option>";
                                         }
                                         ?>
                                    </select>
                                </div>
                                
                            </td>
                            <td class="search_truck">
                                <input type="text" class="textBox_truck" name="customSearch" placeholder="Model, name..." id="customSearch" />                      
                                <input type="submit" value="" class="search" />
                                
                            </td>
                            
                          </tr>
                          <tr>
                        
                    </table> -->
                    <?php }
                    else{
                        ?>
                        
                    <div class="new_searchwrap">
                        <div class="newsearch">
                            <table border="0" cellspacing="0" cellpadding="0">                    
                              <input type="hidden" class="formSelection" name="product_type" id="product_type" value="<?php if(isset($_GET['product_type'])) echo htmlspecialchars($_GET['product_type']);?>"/>                                                          
                            <tr>
                                <td class="top ">                                
                                    <select class="condition formSelection" name="condition" id="condition">
                                        <option value="">- Condition -</option>
                                        <option value="New">New</option>
                                        <option value="Used">Used</option>
                                       
                                    </select>                                
                                </td>
                            </tr>
                            <tr>
                                <td class="top ">                                
                                    <select class="category formSelection" name="category" id="category">
                                        <?php 
                                            include('ajax/load_category.php');
                                        ?>
                                    </select>                                
                                </td>
                            </tr>
                            <tr>
                                <td class="top ">                                
                                    <select class="category2 formSelection" name="category2" id="category2">
                                        <option value="">- Category 2 -</option>
                                        
                                    </select>                                
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <select class="make formSelection" name="make" id="make">
                                        <?php 
                                            include('ajax/load_car_make.php');
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <select class="model formSelection" name="model" id="model">
                                        <option value="">-- Model --</option>                                    
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <!-- <td>
                                    <select class="formSelection" name="steering" id="steering">
                                        <option selected value="">- Steering -</option>
                                        <option value="LHD">LHD</option>
                                        <option value="RHD">RHD</option>
                                    </select>
                                </td> -->
                                <td >
                                    <select class="country formSelection" name="country" id="country">
                                        <?php 
                                            include('ajax/load_country.php');
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <div id="divYear">
                                        
                                        <select class="fromSelection year" name="year_from" id="year_from">
                                            <option value="">From</option>
                                             <?php
                                             for($i=1990;$i<=2014;$i++){
                                                 echo "<option value='$i'>$i</option>";
                                             }
                                             ?>
                                        </select>
                                        
                                        <select class="toSelection year_end" name="year_to" id="year_to">
                                            <option value="">To</option>
                                             <?php
                                             for($i=1990;$i<=2014;$i++){
                                                 echo "<option value='$i'>$i</option>";
                                             }
                                             ?>
                                        </select>
                                    </div>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <input type="text" class="textBox" name="customSearch" placeholder="Model, name..." id="customSearch" />                      
                                    
                                    
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="" class="search" />
                                </td>
                            </tr>
                              
                            
                        </table>
                    </div>
                </div>
                            <?php 
                        }
                        ?>
                      

            </form>
                