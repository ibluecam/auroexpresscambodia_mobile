<?php
if(!isset($_SESSION)) @session_start();
require_once '../../../config.php';
require_once BASE_ROOT . 'language/' . $_SESSION['log_language_iso'] . '/admin-new-car.php';

// calculate max. upload filesize.
$max_upload     = (int)(ini_get('upload_max_filesize'));
$max_post       = (int)(ini_get('post_max_size'));
$memory_limit   = (int)(ini_get('memory_limit'));
$upload_mb      = min($max_upload, $max_post, $memory_limit);
$cid            = trim($_GET['cid']);
$mode           = trim($_GET['mode']);

/**
 * Handle file upload
 * <br>-------------------------------------------------------------------------
 */

if( isset($_POST['savebtn']) ){
    $cid = trim($_POST['caridInput']);
    //$files[]= $_FILES['fileInput'];
	
	//echo "No. files uploaded : ".count($_FILES['fileInput']['name'])."<br>";
 
  //Count number of files in array, loop through each file
  for($j=0; $j<count($_FILES['fileInput']['name']); $j++) {

 
    if( empty($cid) || !isset($_FILES['fileInput']['name'][$j]) ){
        $form_error = true;
        $form_message = $_LANG['NEWCAR_INVALID_REQUEST_ID_LABEL'];
    }
    else {
        // get file extension.
        $ext =$_FILES['fileInput']['name'][$j];
        $ext = explode('.', $ext);
        $ext = $ext[(count($ext)-1)];
        $ext = strtolower($ext);
        
        switch($ext){
            case 'jpg':
            case 'gif':
            case 'png':
                $nogood = false;
                break;
            default:
                $nogood = true;
        }
        
        if( $nogood ){
            $form_error = true;
            $form_message = $_LANG['NEWCAR_INVALID_FILE_EXTENSION'];
        }
        else {
            // upload file.
            $path  = BASE_ROOT . 'image/upload/cars/';
            $fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;
			$medium_name="medium-".$fname;
            $small_name="small-".$fname;
            // upload failed.
            if( $_FILES['fileInput']['size'][$j] < 1 ){
                $form_error = true;
                $form_message = $_LANG['NEWCAR_INVALID_IMAGE_SIZE_LABEL'];
            }
            else {
                // upload the image.
                if( !@move_uploaded_file($_FILES['fileInput']['tmp_name'][$j], $path.$fname) ){
                    $form_error = true;
                    $form_message = $_LANG['NEWCAR_UPLOAD_FAILURE_LABEL'];
                    require_once BASE_CLASS . 'class-log.php';
                    LogReport::write('Unable to upload image at ' . __FILE__ . ':' . __LINE__);
                }
                else {
                    // resize image.
					copy($path.$fname,$path.$medium_name);
					copy($path.$fname,$path.$small_name);
                    switch($ext){
                        case 'jpg':
                            $i = imagecreatefromjpeg($path.$fname);
							$medium_size=imagecreatefromjpeg($path.$medium_name);
							$small_size=imagecreatefromjpeg($path.$small_name);
                            break;
							
                        case 'gif':
                            $i = imagecreatefromgif($path.$fname);
							$medium_size=imagecreatefromgif($path.$medium_name);
							$small_size=imagecreatefromgif($path.$small_name);
							
                            break;
                        case 'png':
                            $i = imagecreatefrompng($path.$fname);
							$medium_size=imagecreatefrompng($path.$medium_name);
							$small_size=imagecreatefrompng($path.$small_name);
                            break;
                    }
                    $resized_image = thumbnail_box($i,365,210);
					$resize_medium_image=thumbnail_box($medium_size,157,89);
					$resize_small_size=thumbnail_box($small_size,65,40);
                    
                    // destroy original image.
                    imagedestroy($i);
					imagedestroy($medium_size);
					imagedestroy($small_size);
                    
                    // check if thumb was created successfully.
                    if(is_null($resized_image) ){
                        $form_error = true;
                        $form_message = $_LANG['NEWCAR_UPLOAD_FAILURE_LABEL'];
                        require_once BASE_CLASS . 'class-log.php';
                        LogReport::write('Unable to upload image at ' . __FILE__ . ':' . __LINE__);
                        @unlink($path.$fname);
                    }
                    else {
                        // save new resized image.
                        switch($ext){
                            case 'jpg':
                                imagejpeg($resized_image,$path.$fname,100);
								imagejpeg($resize_medium_image,$path.$medium_name,100);
								imagejpeg($resize_small_size,$path.$small_name,100);
                                break;
                            case 'gif':
                                 imagegif($resized_image,$path.$fname);
								 imagegif($resize_medium_image,$path.$medium_name);
								 imagegif($resize_small_size,$path.$small_name);
                                break;
                            case 'png':
                                imagepng($resized_image,$path.$fname);
							    imagepng($resize_medium_image,$path.$medium_name);
							    imagepng($resize_small_size,$path.$small_name);
                                break;
                        }
                        
                        // save to database.
                        require_once BASE_CLASS . 'class-connect.php';
                        
                        $cnx = new Connect();
                        $cnx->open();
                        
                        $destination = 'image/upload/cars/' . $fname;
                        
                        if( !@mysql_query("INSERT INTO `car_media` (
                                                                    `product_id`,
                                                                    `type`,
                                                                    `mode`,
                                                                    `source`
                                                                    ) VALUES (
                                                                    '$cid',
                                                                    'image',
                                                                    '$mode',
                                                                    '$destination'
                                                                    )") ){
                            require_once BASE_CLASS . 'class-log.php';
                            LogReport::write('Unable to save image for car_media table due a query error at ' . __FILE__ . ':' . __LINE__);
                            $form_error = true;
                            $form_message = $_LANG['NEWCAR_UPLOAD_QUERY_ERROR'];
                            $cnx->close();
                            @unlink($path.$fname);
                        }
                        else {
                            $cnx->close();

                            // notify parent window to preview image.
?>
<script language="javascript">parent.carImageUploaded('image/upload/cars/<?php echo $fname;?>','<?php echo $mode;?>');</script>
<?php
                        }
                    }
                }
            }
        }
    }
 }
 }
// end file uploader handler ---------------------------------------------------
$form_error = false;

// internals -------------------------------------------------------------------
function formatBytes($bytes, $precision = 2) { 
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1);
    $bytes /= pow(1024, $pow); 

    return round($bytes, $precision) . ' ' . $units[$pow]; 
}
function thumbnail_box($img, $box_w, $box_h) {
    //create the image, of the required size
    $new = imagecreatetruecolor($box_w, $box_h);
    if($new === false) {
        //creation failed -- probably not enough memory
        return null;
    }


    //Fill the image with a light grey color
    //(this will be visible in the padding around the image,
    //if the aspect ratios of the image and the thumbnail do not match)
    //Replace this with any color you want, or comment it out for black.
    //I used grey for testing =)
    $fill = imagecolorallocate($new, 200, 200, 205);
    imagefill($new, 0, 0, $fill);

    //compute resize ratio
    $hratio = $box_h / imagesy($img);
    $wratio = $box_w / imagesx($img);
    $ratio = min($hratio, $wratio);

    //if the source is smaller than the thumbnail size, 
    //don't resize -- add a margin instead
    //(that is, dont magnify images)
    if($ratio > 1.0)
        $ratio = 1.0;

    //compute sizes
    $sy = floor(imagesy($img) * $ratio);
    $sx = floor(imagesx($img) * $ratio);

    //compute margins
    //Using these margins centers the image in the thumbnail.
    //If you always want the image to the top left, 
    //set both of these to 0
    $m_y = floor(($box_h - $sy) / 2);
    $m_x = floor(($box_w - $sx) / 2);

    //Copy the image data, and resample
    //
    //If you want a fast and ugly thumbnail,
    //replace imagecopyresampled with imagecopyresized
    if(!imagecopyresampled($new, $img,
        $m_x, $m_y, //dest x, y (margins)
        0, 0, //src x, y (0,0 means top left)
        $sx, $sy,//dest w, h (resample to this size (computed above)
        imagesx($img), imagesy($img)) //src w, h (the full size of the original)
    ) {
        //copy failed
        imagedestroy($new);
        return null;
    }
    //copy successful
    return $new;
}
// end internals ---------------------------------------------------------------
?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!-- Force latest IE rendering engine or ChromeFrame if installed -->
        <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
        <meta charset="utf-8">
        <title>File uploader</title>
        <!-- Bootstrap CSS Toolkit styles -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap-responsive.min.css">
        <!-- Bootstrap CSS fixes for IE6 -->
        <!--[if lt IE 7]><link rel="stylesheet" href="../bootstrap/css/bootstrap-ie6.min.css"><![endif]-->
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
        <!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script src="../jquery/jquery.js"></script>
        <!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
            /**
             * Make sure the article id is declared.
             */
            if(  $_SESSION['log_group'] != 'admin' )
            {
                die('<p class="alert alert-error">' . $_LANG['NEWCAR_INVALID_UPLOAD_REQUEST_LABEL'] . '</p>');
            }
            
            // get car id.
            if( empty($_GET['cid']) ){
                die('<p class="alert alert-error">' . $_LANG['NEWCAR_INVALID_REQUEST_ID_LABEL'] . '</p><p><a href="admin-addpic.php?cid='.$cid.'&mode='.$mode.'">'.$_LANG['NEWCAR_GOBACK_LABEL'].'</a></p>');
            }
            
            // form check
            if( $form_error ){
                die('<p class="alert alert-error">' . $form_message . '</p><p><a href="admin-addpic.php?cid='.$cid.'&mode='.$mode.'">'.$_LANG['NEWCAR_GOBACK_LABEL'].'</a></p>');
            }
            else {
        ?>
        <div style="width:700px;overflow:hidden;">

            <!-- The file upload form used as target for the file upload widget -->
            <form method="POST"  enctype="multipart/form-data" style="margin:0;" action="?cid=<?php echo $cid;?>&mode=<?php echo $mode;?>">
                <p><b style="color:#006600;"><?php echo $_LANG['NEWCAR_MAX_UPLOAD_FILESIZE_LABEL'];?>: <?php echo $upload_mb;?>MB</b></p>
                <div class="input-append">
                    <input type="file" multiple name="fileInput[]" style="border:1px solid #CCC;width:594px;height:30px;" />
                    <input type="hidden" name="caridInput" value="<?php echo $cid;?>" />
                    <input type="submit" name="savebtn" class="btn btn-success" value="<?php echo $_LANG['NEWCAR_UPLOAD_BUTTON'];?>" />
                </div>
            </form>
            <br>
        </div>
        <?php
            }
        ?>
    </body> 
</html>
