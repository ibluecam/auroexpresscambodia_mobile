<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get car list based on pagination.
    $list = $_CLASS->getCarList();
    $body_list = $_CLASS->getBodyTypeList();
    $country_list=$_CLASS->getCountryList();
    // set current page.
    if( isset($_GET['pg']) ? $current_page = (int)$_GET['pg'] : $current_page = 1 );
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="top-page" onclick="javascript: $('html, body').animate({scrollTop: '0px'}, 300);"></div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['CAR_MAN_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display list.
                    if( count($list) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['CAR_MAN_NORESULT_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                ?>
                <!-- pagination controls -->
                <form method="post" style="margin:0;">
                    <div class="input-prepend input-append" style="text-align: right;margin-right:3px;">
                        <span class="add-on" style="width:90px;"> <?php echo $_LANG['CAR_MAN_PAGE_LABEL'];?> </span>
                        <select id="pageInput" onchange="loadPage();" style="width:90px;">
                            <?php
                                for( $i=0; $i < $_CLASS->getTotalNumberOfPages(); $i++ ){
                                    $selected = '';
                                    
                                    // fix bug: set current page as selected [2013.07.10]
                                    ( $current_page == ($i+1) ? $selected = 'selected="selected"' : $selected = '' );
                            ?>
                            <option value="<?php echo ($i+1);?>" <?php echo $selected;?>><?php echo ($i+1);?></option>
                            <?php
                                }
                            ?>
                        </select>
                        <span class="add-on" style="background:#FFF;width:90px;"> <?php echo $_LANG['CAR_MAN_OF_LABEL'];?> <?php echo $_CLASS->getTotalNumberOfPages();?>.</span>
                    </div>
                </form>
                <?php
                        require_once BASE_CLASS . 'class-utilities.php';
                        
                        // defined price ponctuation
                        if( $_SESSION['log_language_iso'] != 'en' ){
                            $tseparator = '.';
                            $dseparator = ',';
                        }
                        else {
                            $tseparator = ',';
                            $dseparator = '.';
                        }
                        
                        // display items.
                        for( $i=0; $i < count($list); $i++ ){
                            $thumb = $_CLASS->getCarMediaById($list[$i]['id']);
                            
                            $pr = explode('.',$list[$i]['price']);
                            $cents = $pr[1];
                            $thousand = substr($pr[0],0,-3);
                            $hundred = substr($pr[0], -3,strlen($pr[0]));
                            if( empty($cents) ){
                                $cents = '00';
                            }
                            
                            // defined feature image url.
                            ( $list[$i]['featured'] ? $featURL = BASE_RELATIVE . 'image/featured.png' : $featURL = '' );
                            
                ?>
                <form method="post" class="form-wrap" action="?pg=<?php echo $current_page;?>#t<?php echo $i;?>">
                    <a name="t<?php echo $i;?>"></a>
                    <a href="<?php echo BASE_RELATIVE . $thumb;?>" rel="prettyPhoto" class="list-img">
                        <img src="<?php echo BASE_RELATIVE . $thumb;?>" width="200" height="150" class="img-polaroid" />
                        <img class="carFeatImg" src="<?php echo $featURL;?>"/>
                    </a>
                    <div class="list-content">
                        <h3><?php echo $list[$i]['maker'] . ' ' . $list[$i]['model'] . ' ' . $list[$i]['year'];?></h3>
                        <div class="input-prepend">
                            <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_MAKER_LABEL'];?></span>
                            <input type="text" name="makerInput" value="<?php echo $list[$i]['maker'];?>" style="width:322px;" />
                        </div>
                        <div class="input-prepend">
                            <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_MODEL_LABEL'];?></span>
                            <input type="text" name="modelInput" value="<?php echo $list[$i]['model'];?>" style="width:322px;" />
                        </div>
                        <div id="hiddenInput<?php echo $i;?>" style="display:none;">
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_BODY_TYPE_LABEL'];?></span>
                                <select name="bodyInput" style="width:336px;">
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_SELECTED'];?>">
                                        <option value="<?php echo $list[$i]['body_type'];?>"><?php echo $_CLASS->getBodytypeById($list[$i]['body_type']);?></option>
                                    </optgroup>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_OPTIONS'];?>">
                                        <?php
                                            for( $u=0; $u < count($body_list); $u++ ){
                                        ?>
                                        <option value="<?php echo $u;?>"><?php echo $body_list[$u][$u];?></option>
                                        <?php
                                            }
                                        ?>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_YEAR_LABEL'];?></span>
                                <select name="yearInput" style="width:336px;">
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_SELECTED'];?>">
                                        <option value="<?php echo $list[$i]['year'];?>"><?php echo $list[$i]['year'];?></option>
                                    </optgroup>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_OPTIONS'];?>">
                                        <?php
                                            for( $r=date('Y'); $r > (date('Y')-50); $r-- ){
                                        ?>
                                        <option value="<?php echo $r;?>"><?php echo $r;?></option>
                                        <?php
                                            }
                                        ?>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $list[$i]['measure_type'];?></span>
                                <input type="text" name="milesInput" value="<?php echo $list[$i]['miles'];?>" style="width:322px;" />
                            </div>
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_MEASURE_TYPE_LABEL'];?></span>
                                <select name="measureInput" style="width:336px;">
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_SELECTED'];?>">
                                        <option value="<?php echo $list[$i]['measure_type'];?>"><?php echo ucfirst($list[$i]['measure_type']);?></option>
                                    </optgroup>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_OPTIONS'];?>">
                                        <option value="km">Km</option>
                                        <option value="miles">Miles</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_DOORS_LABEL'];?></span>
                                <select name="doorInput" style="width:336px;">
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_SELECTED'];?>">
                                        <option value="<?php echo $list[$i]['doors'];?>"><?php echo ucfirst($list[$i]['doors']);?></option>
                                    </optgroup>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_OPTIONS'];?>">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_FEATURED_LABEL'];?></span>
                                <select name="featuredInput" style="width:336px;">
                                    <?php
                                        ( $list[$i]['featured'] ? $featured = $_LANG['CAR_MAN_FEATURED_YES_LABEL'] : $featured = $_LANG['CAR_MAN_FEATURED_NO_LABEL'] );
                                    ?>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_SELECTED'];?>">
                                        <option value="<?php echo $list[$i]['featured'];?>"><?php echo $featured;?></option>
                                    </optgroup>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_OPTIONS'];?>">
                                        <option value="0"><?php echo $_LANG['CAR_MAN_FEATURED_NO_LABEL'];?></option>
                                        <option value="1"><?php echo $_LANG['CAR_MAN_FEATURED_YES_LABEL'];?></option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_ENERGY_CLASS_LABEL'];?></span>
                                <select name="energyInput" style="width:336px;">
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_SELECTED'];?>">
                                        <option value="<?php echo $list[$i]['eco'];?>"><?php echo $list[$i]['eco'];?></option>
                                    </optgroup>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_OPTIONS'];?>">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>
                                        <option value="F">F</option>
                                    </optgroup>
                                </select>
                            </div>
							<div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_COUNTRY_LIST_LABEL'];?></span>
                                <select name="countryInput" style="width:336px;">
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_SELECTED'];?>">
                                        <option value="<?php echo $list[$i]['country'];?>"><?php echo $_CLASS->getCountryById($list[$i]['country']);?></option>
                                    </optgroup>
                                    <optgroup label="<?php echo $_LANG['CAR_MAN_OPTION_OPTIONS'];?>">
                                        <?php
                                            for( $u=0; $u < count($country_list['id']); $u++ ){
                                        ?>
                                        <option value="<?php echo $country_list['id'][$u];?>"><?php echo $country_list['country_name'][$u];?></option>
                                        <?php
                                            }
                                        ?>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="input-prepend input-append" style="margin-left:1px;">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_PRICE_LABEL'];?></span>
                                <span class="add-on" style="width:30px"><?php echo CURRENCY_SYMBOL;?></span>
                                <input type="text" name="priceThousandInput" maxlength="3" value="<?php echo $thousand;?>" style="width:70px;"/>
                                <span class="add-on" style="width:30px"><?php echo $tseparator;?></span>
                                <input type="text" name="priceHundredInput" maxlength="3" value="<?php echo $hundred;?>" style="width:70px;"/>
                                <span class="add-on" style="width:30px"><?php echo $dseparator;?></span>
                                <input type="text" name="priceCentInput" maxlength="2" value="<?php echo $cents;?>" style="width:35px;"/>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on" style="width:120px;"><?php echo $_LANG['CAR_MAN_MEDIA_DETAILS_LABEL'];?></span>
                                <a href="<?php echo BASE_RELATIVE;?>car-manager-edit-media/?cid=<?php echo $list[$i]['id'];?>" class="btn" style="width:70px;"><i class="icon-picture" style="margin-right:10px;"></i><?php echo $_LANG['CAR_MAN_MEDIA_LABEL'];?></a>
                                <a href="<?php echo BASE_RELATIVE;?>car-manager-edit-details/?cid=<?php echo $list[$i]['id'];?>" class="btn" style="width:80px;"><i class="icon-pencil" style="margin-right:10px;"></i><?php echo $_LANG['CAR_MAN_DETAILS_LABEL'];?></a>
                                <a href="<?php echo BASE_RELATIVE;?>car-manager-edit-features/?cid=<?php echo $list[$i]['id'];?>" class="btn" style="width:98px;"><i class="icon-check" style="margin-right:10px;"></i><?php echo $_LANG['CAR_MAN_FEATURES_LABEL'];?></a>
                            </div>
                        </div>
                        <input type="hidden" name="caridInput" value="<?php echo $list[$i]['id'];?>" />
                        <a href="#" onclick="displayManagerTab('hiddenInput<?php echo $i;?>','ico<?php echo $i;?>');return false;" class="btn btn-small" style="height:19px;"><i id="ico<?php echo $i;?>" class="icon-chevron-down"></i></a>
                        <input type="submit" name="updatebtn" class="btn btn-info btn-small" value="<?php echo $_LANG['CAR_MAN_UPDATE_BUTTON'];?>" />
                        <input type="submit" name="removebtn" class="btn btn-danger btn-small" value="<?php echo $_LANG['CAR_MAN_DELETE_BUTTON'];?>" onclick="return confirm('<?php echo $_LANG['CAR_MAN_DELETE_CONFIRM'];?>');" />
                        <?php
                            if( $list[$i]['status'] != 2 ){
                        ?>
                        <input type="submit" name="soldbtn" class="btn btn-warning btn-small" value="<?php echo $_LANG['CAR_MAN_SOLD_BUTTON'];?>" />
                        <?php
                            }
                            else {
                        ?>
                        <span class="label" style="padding:7px;"><?php echo $_LANG['CAR_MAN_VEHICLE_SOLD'];?></span>
                        <input type="submit" name="resetbtn" class="btn btn-success btn-small" value="<?php echo $_LANG['CAR_MAN_RESET_SOLD_STATUS'];?>" />
                        <?php
                            }
                        ?>
                    </div>
                </form>
                <?php
                        }
                    }
                ?>
                
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
            <?php
                // display tab if tid is set.
                if( isset($_GET['tid']) ){
            ?>
            <script language="javascript" type="text/javascript">
                var tid = document.getElementById('<?php echo trim($_GET['tid']);?>');
                
                if( tid != undefined && tid != null ){
                    $(tid).slideDown();
                    console.log('ok')
                }else {console.log('is null')}
            </script>
            <?php
                }
            ?>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}