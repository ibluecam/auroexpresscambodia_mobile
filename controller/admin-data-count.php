<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class admindatacount {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
    private $news_list;
    
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function admindatacount($lang=array()){
        $this->lang = $lang;
        $this->executeDataCount();
        // // handle remove news.
        // if( isset($_POST['removebtn']) ){
        //     $this->handleForm();
        // }
        
        // // load news list.
        // $this->loadNewsList();
    }
    
    /**
     * Private method: handle remove news form
     * <br>---------------------------------------------------------------------
     * @return void
     */
    public function executeDataCount(){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        $truncate_sql="TRUNCATE data_count";
        $insert_sql="INSERT INTO data_count (register_id, 
                        country, 
                        car_count, 
                        truck_count, 
                        bus_count, 
                        part_count, 
                        accessories_count, 
                        equipment_count, 
                        motorbike_count, 
                        aircraft_count, 
                        watercraft_count) 
                    SELECT `rg`.`id`,
                        `rg`.`country`,
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Car' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Truck' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Bus' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Part' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Accessories' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Equipment' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Motorbike' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Aircraft' AND del_flag='0'),
                        (SELECT COUNT(id) FROM product WHERE owner=`rg`.`id` AND product_type='Watercraft' AND del_flag='0')
                    FROM register rg
                    LEFT JOIN product pd ON pd.owner=rg.id AND rg.id=".$_SESSION['log_id']."
                    GROUP BY rg.id

        ";
        if( !@mysql_query($truncate_sql) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to truncate data_count due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['MANAGE_TRUNCATE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        //echo $insert_sql;
        if( !@mysql_query($insert_sql) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to save count to data_count due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            $this->form_message = $this->lang['MANAGE_SAVED_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }

        $cnx->close();
        $this->form_message = $this->lang['MANAGE_SAVED_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        return;
    }
    
  
    
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}