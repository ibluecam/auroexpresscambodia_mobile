
<link href="<?php echo BASE_RELATIVE;?>css/blog/blog-photo.css" rel="stylesheet" type="text/css" />
    
<div id="sectionContent" class="clearfix">    	
	<?php include('layout/sidebar.php'); ?>
    <div id="sectionFeature" class="clearfix">
		<div id="photoHead" class="clearfix">
        	<p><b>Photos : (6)</b></p>
            <p><a href="">All</a> <a href="">Group 1</a> | <a href="">Group 2</a></p>
        </div>
        <div id="listPhoto">
        	<?php
				$count = 1;
				for($i=1; $i <= 40; $i++){
			?>
            		<div class="photoAlbum<?php if(($count % 4) == 0){ echo ' photoAlbumLast';} ?>">
                        <img src="images/blog/photo/photo1.jpg" />
                        <p><b>Our staff (<span>62 View</span>)</b></p>
                        <p class="datePost">Aug 02 2013 kncar</p>
                    </div>
            <?php
					$count++;		
				}
			?>
            
        </div>
    </div>
</div>