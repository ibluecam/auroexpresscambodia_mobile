<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
/*
 * class-connect.php
 * Opens and closes connetction with database.
 */
class Connect
{
    protected $is_connected;
    protected $connection;
    protected $records;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function Connect()
    {
        $this->is_connected = false;
    }
    
    /*
     * Opens database connection -----------------------------------------------
     * @return void.
     */
    public function open()
    {
        if( $this->is_connected )
        {
            return;
        }
       
        if( !$this->connection = mysql_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS) )
        {
			require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to connect to database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            LogReport::fatalError('Unable to load page due an internal error. Please report the problem to the system administrator.');
        }
        
        if( !mysql_select_db(DATABASE_TABLE) )
        {
            require_once BASE_CLASS . 'class-log.php';
            
            @mysql_close($this->connection);
            
            LogReport::write('Unable to select database "'.DATABASE_TABLE.'" at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
            LogReport::fatalError('Unable to load page due an internal error. Please report the problem to the system administrator.');
        }
        mysql_query ("set character_set_client='utf8'"); 
        mysql_query ("set character_set_results='utf8'"); 

        mysql_query ("set collation_connection='utf8_general_ci'");  
        $this->is_connected = true;
    }
    
    public function pdoOpen(){
        try {
            $this->connection = new PDO('mysql:host=localhost;dbname='.DATABASE_TABLE, DATABASE_USER, DATABASE_PASS, array( PDO::ATTR_PERSISTENT => false));
            return true;
        } catch (PDOException $e) {
            //print "Error!: " . $e->getMessage() . "<br/>";
            return false;
        }
    }
    /* Execute pdo query return true if success */
    public function pdoExecuteQuery($sql_query){
        $stmt = $this->connection->prepare($sql_query);
        // call the stored procedure
        if($stmt->execute()){
            $this->records= $stmt->fetchAll(PDO::FETCH_ASSOC);
            return true;
        }
    }
    /*
     * Closes database connection ----------------------------------------------
     * @return void.
     */
    public function close()
    {
        if( !$this->is_connected )
        {
            return;
        }
        
        @mysql_close($this->connection);
        
        $this->connection = null;
        $this->is_connected = false;
    }
    public function getRecords(){
        return $this->records;
    }
}