<?php
if(!isset($_SESSION)) @session_start();
$log_group=$_SESSION['log_group'];
if($log_group!='admin'){
    return;
}
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();

if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
//if(isset($_GET['offset'])) $offset=htmlspecialchars($_GET['offset']); else $offset=0;
$where="WHERE pm.product_type='$product_type' ";
$sql="SELECT pm.id, pm.maker from product_maker pm $where ORDER BY pm.maker";

if( !$result = @mysql_query($sql) ){
    require_once BASE_CLASS . 'class-log.php';
    $cnx->close();
    LogReport::write('Unable to load make list due a query error at ' . __FILE__ . ':' . __LINE__);
    return;
}
$i=0;
echo "<option value=''>- Make -</option>";
if(mysql_num_rows($result)>0){
    while( $r = @mysql_fetch_assoc($result) ){
        echo "<option value='{$r['id']}'>{$r['maker']}</option>";
    }
}

@mysql_free_result($sql);
$cnx->close();
?>