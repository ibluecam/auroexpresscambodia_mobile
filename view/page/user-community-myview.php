<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'user';
$slug  = 'car';
$my_view = $_CLASS->loadDealweek($_GET['owner']);
$get_register = $_CLASS->loadUserName($_GET['owner']);




// load page content.

$pg_details = $_CLASS->getPageHTML();

?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > <a href="#" class="linkfade">Deal of the Week</a> > View</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Deal of the Week</p>
                </div>
                <div id="community_view" class="clearfix">
                	<div id="logo">
                        <?php                                       
                            if(!file_exists('upload/'.$get_register['image']) || $get_register['image']==""){                                         
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="40" height="40" />
                        <?php                                           
                            }else{
                        ?>
                                <img src="<?php echo BASE_RELATIVE;?>upload/<?php echo $get_register['image'];?>"  width="40" height="40" />
                        <?php
                            }
                        ?>
                    	
                    </div>
                    <div id="detail">
                        <p class="title">
                            <?php echo $get_register['company_name']?>
                        </p>                        
                    </div>
                   
                    
                             
                </div>

                <div id="my_view">
                    <div class="deal_detail clearfix">     
                        <?php $count=1; foreach($my_view as $row){

                            $str = $row['date'];  
                            $i = strrpos($str, " ");
                            $l = strlen($str) - $i;
                            $str = substr($str, $i, $l);
                            $date = str_ireplace($str, "", $row['date']);
                        ?>                                           
                        <div class="other_child <?php if($count%5==0) echo 'first_child';?> ">
                            <div class="car_img">
                               <?php                                       
                                if(!file_exists($row['source'])){                                                  
                            ?>
                                    <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="125" height="70" />
                            <?php                                           
                                }else{
                            ?>
                                    <img src="<?php echo BASE_RELATIVE.$row['source'];?>"  width="125" height="70" />
                            <?php
                                }
                            ?>                            
                            </div>
                            <p class="title"><a href="<?php echo BASE_RELATIVE;?>community-view" class="linkfade" style="color:#424242;"><?php echo $row['title']?></a></p>
                            <p><?php echo $date;?></p>
                            <p><a href="<?php echo BASE_RELATIVE;?>community-view?id=<?php echo $row['id'].'&owner='.$row['owner']?>" class="linkfade">Writer</a></p>
                        </div>

                        <?php $count++; }?>
                    </div>
                </div>
                <div id="button_wrap" style="width:100px;">                    	
                        <div id="back">
                        	<a href="<?php echo BASE_RELATIVE;?>dealof-theweek"><img src="<?php echo BASE_RELATIVE;?>images/community/back_to_list.png" /></a>
                        </div>
                    </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  