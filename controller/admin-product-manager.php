<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include(BASE_ROOT.'view/3rdparty/pagination/pagination.php');

class adminProductManager
{
    protected $lang;
    protected $group = 'admin';
    protected $slug = 'car';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $members;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;
    var $products;
    var $member_type1="Seller";
    var $row_per_page = 20;
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function adminProductManager($lang=array()){
        $this->lang = $lang;
        //if user submit press button save changes
        // if(isset($_POST['save_changes'])){
        //     $this->handleSaveChanges();
        // }
        //if user submit press button delete selected
        // if(isset($_POST['delete_selected'])){
        //     $this->handleDelete();
        // }
        // if(isset($_POST['submitSave'])){
        //     $this->handleSaveNew();
        // }
        $this->loadProductList();
        //$this->loadCityList();
    }
    
    // private function changeMakeInProduct(){
    //     if(isset($_POST['old_make'])) $old_makes=$_POST['old_make']; else $old_makes=array();
    //     if(isset($_POST['new_make'])) $new_makes=$_POST['new_make']; else $new_makes=array();
    //     if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
    //     //Delete existing make that match new makes first to prevent duplicate
    //     //$this->deletePreviousMake($new_makes, $product_type);
        
    //     require_once BASE_CLASS . 'class-connect.php';
    //     $cnx = new Connect();
    //     $cnx->open();

        
    //     $in_makes=array();
    //     $sql= "UPDATE `product` SET `make` = CASE `make` ";
    //     foreach($new_makes as $key=>$value){
    //         array_push($in_makes, $old_makes[$key]);
    //         $sql.="WHEN '{$old_makes[$key]}' THEN '{$value}' ";
    //     }
    //     //Sql of cities that were changed
    //     $in_old_makes = implode("', '", $in_makes);
    //     $sql .= "END ";
    //     //Change only cities that were edit
    //     $sql .= "WHERE make IN ('$in_old_makes') AND product_type='$product_type'";
    //     //echo $sql;
    //     if( !@mysql_query($sql) ){
    //         require_once BASE_CLASS . 'class-log.php';
    //         //echo mysql_error();
    //         $cnx->close();
            
    //         LogReport::write('Unable to change make due to a query error at ' . __FILE__ . ':' . __LINE__);
    //         $this->form_message = "Unable to save changes! Please make sure that you enter correct data.";
    //         $this->form_status = true;
    //         $this->form_style = 'alert-warning';
            
    //         return;
    //     }
    //     $cnx->close();
        
    //     $this->form_message =  "Makes were changed successfully!";
    //     $this->form_status = true;
    //     $this->form_style = 'alert-success';

    // }
    // private function handleDelete(){
    //     require_once BASE_CLASS . 'class-connect.php';
    //     $cnx = new Connect();
    //     $cnx->open();
    //     //code here
    //     if(isset($_POST['makeCheck'])) $makeCheck=$_POST['makeCheck']; else $makeCheck=array();
    //     if(isset($_POST['product_type'])) $product_type=$_POST['product_type']; else $product_type='';
    //     if(count($makeCheck)<=0){
    //         $this->form_message = "Please check the cities to be deleted!";
    //         $this->form_status = true;
    //         $this->form_style = 'alert-warning';
    //         return;
    //     }
    //     $checked_makes = implode("', '", array_keys($makeCheck));
    //     $sql="DELETE FROM product_maker WHERE id IN ('$checked_makes') AND product_type='$product_type'";
    //     //execute delete
    //     if( !@mysql_query($sql) ){
    //         require_once BASE_CLASS . 'class-log.php';
    //         $cnx->close();
    //         LogReport::write('Unable to delete makes in this category due to a query error at ' . __FILE__ . ':' . __LINE__);
    //         $this->form_message = 'Unable to delete makes with this category';
    //         $this->form_status = true;
    //         $this->form_style = 'alert-warning';
    //         return;
    //     }
    //     $cnx->close();
    //     $this->deleteFromModelTable();
    //     $this->form_message =  "Makes with the selected category were deleted successfully!";
    //     $this->form_status = true;
    //     $this->form_style = 'alert-success';
    // }
    
    private function loadProductList(){
        require_once BASE_CLASS . 'class-connect.php';
        $sql="";
        $where="WHERE 1";
        $from = "";

        if(isset($_GET['product_status'])) $del_flag=$_GET['product_status']; else $del_flag='2';
        if(isset($_GET['company_id'])) $owner=$_GET['company_id']; else $owner='';
        if(isset($_GET['categorySelect'])) $product_type=$_GET['categorySelect']; else $product_type='';
        if(isset($_GET['makeSelect'])) $make=$_GET['makeSelect']; else $make='';
        if(isset($_GET['modelSelect'])) $model=$_GET['modelSelect']; else $model='';
        if(isset($_GET['price_from'])) $price_from=$_GET['price_from']; else $price_from='';
        if(isset($_GET['price_to'])) $price_to=$_GET['price_to']; else $price_to='';
        if(isset($_GET['currencySelect'])) $currency=$_GET['currencySelect']; else $currency='';

        $cnx = new Connect();
        $cnx->pdoOpen();
        $where .= " AND del_flag=$del_flag";

        if(!empty($product_type)){
            $where .=" AND product_type='$product_type'";
        }
        if(!empty($owner)){
            $where .=" AND owner='$owner'";
        }
        if(!empty($make)){
            $where .=" AND make='$make'";
        }
        if(!empty($model)){
            $where .=" AND model='$model'";
        }
        if(!empty($price_from)){
            $where .=" AND price >='$price_from'";
        }
        if(!empty($price_to)){
            $where .=" AND price<='$price_to'";
        }
        if(!empty($currency)){
            $where .=" AND currency='$currency'";
        }

        $from.="
            FROM product p 
            INNER JOIN register rg ON p.owner = rg.id
            LEFT JOIN product_primary_photo pm ON p.id=pm.product_id
            ";

        /////PAGINATION PROCESS//////
        $sql_count="SELECT SQL_CACHE COUNT(p.id) as num_row 
            $from 
            $where
            ";
        $result_count = $cnx->pdoExecuteQuery($sql_count);
        $row_count=$cnx->getRecords();
        $this->total_num_row= $row_count[0]['num_row'];
        $links = new Pagination ($this->total_num_row, $this->row_per_page);
        $limit=$links->start_display;
        $this->pagination_html.= $links->display(); 
        ///////PAGINATION PROCESS///////

        $sql = "SELECT p.id as pid, p.make, p.model, p.model_year, p.price, p.currency, p.owner, p.del_flag, p.product_type, rg.company_name, pm.thumb
            $from
            $where
            $limit 
            ";
        
        $cnx->pdoExecuteQuery($sql);
        $this->products = $cnx->getRecords();
    }
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    public function getProductList(){
        return $this->products;
    }
}
?>
