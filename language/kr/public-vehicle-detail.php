<?php
$_LANG = array(
    'REGISTER_COMPLETE_STATUS' => 'Message has been sent successfully!',
    'REGISTER_COMPLETE_LABEL' => 'Message has been sent successfully!',
    'REGISTER_ERROR_STATUS' => 'Message sending failed!',
    'REGISTER_ERROR_LABEL' => 'Message sending failed!',
    'REGISTER_SESSION_STATUS' => 'Please login first!',
    'REGISTER_SESSION_LABEL' => 'Please login first!'
);