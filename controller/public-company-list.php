<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
 include('view/3rdparty/pagination/pagination.php');
class publicCompanyList
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'buyer-directory';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    var $traderList;
    var $membership;
    var $membershipgold;
    var $membershipSilver;
    var $membershipDiamon;
    private $car;
    var $total_num_row;
    var $pagination_html;
    var $current_page;
    var $total_page;
    var $register_type="";
    var $vehicle="";
    var $slug_detail='';
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicCompanyList($lang=array())
    {
         $this->lang = $lang;
          $this->loadTraderList();
          $this->loadVehicleData();
        // // $this->loadMembership();
        // // $this->loadMembershipSilver();
        // $this->loadMembershipgold();
        // $this->loadMembershipDiamon();
    }
    private function loadVehicleData(){
        $id="";
        require_once BASE_CLASS . 'class-connect.php';
        if(isset($_GET["id"]) ){
            $id = $_GET["id"];
        }
        $cnx = new Connect();
        $cnx->open();
        $where=" WHERE del_flag = 0 ";
        $sql_search="SELECT product.*,car_media.thumb,country_list.country_name,country_list.cc, register.company_name from product INNER JOIN register on product.`owner`=register.id LEFT JOIN car_media on product.id=car_media.product_id INNER JOIN country_list on product.location=country_list.cc
        WHERE product.`owner`=2
        GROUP BY product.id
        ORDER BY created_date DESC";


        // Group by product ID.
        //$sql_search.=$where."GROUP BY p.id ";
        // $sql_search.=" GROUP BY product.id ORDER BY ";
        //echo $sql_search;
        /////PAGINATION PROCESS//////

        // echo $sql_search;
        $sql_count = @mysql_query($sql_search);
        $this->total_num_row= @mysql_num_rows($sql_count);

        $links = new Pagination ($this->total_num_row,18);
        $limit=$links->start_display;
        $this->pagination_html.= $links->display();

        ///////PAGINATION PROCESS///////
        $sql_search.=$limit;
        //echo $sql_search;
        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }

        $this->vehicle = array();
        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->vehicle, $r);
        }
        //var_dump($this->vehicle);
        @mysql_free_result($sql);
        $cnx->close();

    }



    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        //echo $this->slug;
        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }

    /*
     * Public method: get load trader data ------------------------------------
     * @return array | false.
     */
    private function loadTraderList(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $traderList=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if(isset($_GET['keyword'])) $keyword=mysql_real_escape_string(stripcslashes($_GET['keyword'])); else $keyword="";
        //if(isset($_GET['business_type'])) $business_type=mysql_real_escape_string(stripcslashes($_GET['business_type'])); else $business_type="";
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(isset($_GET['register_type'])) $register_type=mysql_real_escape_string(stripcslashes($_GET['register_type'])); else $register_type="";
        // if(isset($_GET['condition'])) $condition=mysql_real_escape_string(stripcslashes($_GET['condition'])); else $condition="";
        // if(isset($_GET['steering'])) $steering=mysql_real_escape_string(stripcslashes($_GET['steering'])); else $steering="";
        // if(isset($_GET['fuel_type'])) $fuel_type=mysql_real_escape_string(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
        // if(isset($_GET['min_price'])) $min_price=mysql_real_escape_string(stripcslashes($_GET['min_price'])); else $min_price="";
        // if(isset($_GET['max_price'])) $max_price=mysql_real_escape_string(stripcslashes($_GET['max_price'])); else $max_price="";
        // if(isset($_GET['make'])) $make=mysql_real_escape_string(stripcslashes($_GET['make'])); else $make="";
        // if(isset($_GET['category'])) $category=mysql_real_escape_string(stripcslashes($_GET['category'])); else $category="";
        $where.=" AND (`rg`.`member_type1` LIKE '%seller%' OR `rg`.`member_type1` LIKE '%forwarder%')";
        if(!empty($country)){
            $where.=" AND `rg`.`country`='$country' ";
        }
        if(!empty($keyword)){
            $where.=" AND (`rg`.`card_id` LIKE '%$keyword%' OR `rg`.`name` LIKE '%$keyword%' OR `rg`.`company_name` LIKE '%$keyword%' OR `rg`.`introduction` LIKE '%$keyword%') ";
        }
        // if(!empty($business_type)){
        //     $where.=" AND `rg`.`business_type` LIKE '%$business_type%' ";
        // }
        if(!empty($product_type)){
            $where.=" AND `pd`.`product_type` = '$product_type' ";
        }
        if(!empty($register_type)){
            //$where.=" AND `rg`.`register_type` = '$register_type' ";
            $this->register_type=$register_type;
        }
        if(!empty($this->register_type)){
            $where.=" AND register_type LIKE '%$this->register_type%' ";
        }
        // if(!empty($condition)){
        //     $where.=" AND `pd`.`condition` = '$condition' ";
        // }
        // if(!empty($steering)){
        //     $where.=" AND `pd`.`steering` = '$steering' ";
        // }
        // if(!empty($fuel_type)){
        //     $where.=" AND `pd`.`fuel_type` = '$fuel_type' ";
        // }
        // if(!empty($min_price)){
        //     $where.=" AND `pd`.`price` > '$min_price' ";
        // }
        // if(!empty($max_price)){
        //     $where.=" AND `pd`.`price` < '$max_price' ";
        // }
        // if(!empty($make)){
        //     $where.=" AND `pd`.`make` = '$make' ";
        // }
        // if(!empty($category)){
        //     $where.=" AND `pd`.`category` = '$category' ";
        // }
        $query = "SELECT rg.id as id,
                                     rg.company_name as company_name, cl.country_name,
                                     (SELECT COUNT(*) FROM product WHERE `owner` = rg.id) as in_stock_product
                                     FROM register `rg`
                                    INNER JOIN country_list cl ON cl.cc=rg.country
                                    WHERE rg.company_name<>'' $where
                                    GROUP BY company_name
                                            ";

        //echo $query;
        /////PAGINATION PROCESS//////

        $sql_count_str=$query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);

        $links = new Pagination ($this->total_num_row, 12);
        $limit=$links->start_display;
        $this->pagination_html= $links->display();
        $this->current_page = $links->currentPage();
        $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $query.=$limit;
       //echo $query;
        $result = mysql_query($query);


        //echo $query;
        $this->traderList = array();
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($traderList, $r);
        }


       //$seller_country=implode(" ", $sellersByCountry);
        $str_slug="";
        $array_slug=array();
        foreach($traderList as $trader){

            if(!in_array($trader['country_name'], $array_slug))
                $array_slug[]=$trader['country_name'];

        }
        $str_slug.=implode(" " ,$array_slug);

        $this->slug_detail="Auto sellers from ".$str_slug;
        //echo mysql_error();
        //var_dump($this->userInfo);
        $this->traderList=$traderList;
        $cnx->close();
    }
    private function loadMembership(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $membership=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if(isset($_GET['keyword'])) $keyword=mysql_real_escape_string(stripcslashes($_GET['keyword'])); else $keyword="";
        //if(isset($_GET['business_type'])) $business_type=mysql_real_escape_string(stripcslashes($_GET['business_type'])); else $business_type="";
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(isset($_GET['register_type'])) $register_type=mysql_real_escape_string(stripcslashes($_GET['register_type'])); else $register_type="";
        // if(isset($_GET['condition'])) $condition=mysql_real_escape_string(stripcslashes($_GET['condition'])); else $condition="";
        // if(isset($_GET['steering'])) $steering=mysql_real_escape_string(stripcslashes($_GET['steering'])); else $steering="";
        // if(isset($_GET['fuel_type'])) $fuel_type=mysql_real_escape_string(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
        // if(isset($_GET['min_price'])) $min_price=mysql_real_escape_string(stripcslashes($_GET['min_price'])); else $min_price="";
        // if(isset($_GET['max_price'])) $max_price=mysql_real_escape_string(stripcslashes($_GET['max_price'])); else $max_price="";
        // if(isset($_GET['make'])) $make=mysql_real_escape_string(stripcslashes($_GET['make'])); else $make="";
        // if(isset($_GET['category'])) $category=mysql_real_escape_string(stripcslashes($_GET['category'])); else $category="";
        $where.=" AND (`rg`.`member_type1` LIKE '%seller%' OR `rg`.`member_type1` LIKE '%forwarder%')";
        if(!empty($country)){
            $where.=" AND `rg`.`country`='$country' ";
        }
        if(!empty($keyword)){
            $where.=" AND (`rg`.`card_id` LIKE '%$keyword%' OR `rg`.`name` LIKE '%$keyword%' OR `rg`.`company_name` LIKE '%$keyword%' OR `rg`.`introduction` LIKE '%$keyword%') ";
        }
        // if(!empty($business_type)){
        //     $where.=" AND `rg`.`business_type` LIKE '%$business_type%' ";
        // }
        if(!empty($product_type)){
            $where.=" AND `pd`.`product_type` = '$product_type' ";
        }
        if(!empty($register_type)){
            //$where.=" AND `rg`.`register_type` = '$register_type' ";
            $this->register_type=$register_type;
        }
        if(!empty($this->register_type)){
            $where.=" AND register_type LIKE '%$this->register_type%' ";
        }
        // if(!empty($condition)){
        //     $where.=" AND `pd`.`condition` = '$condition' ";
        // }
        // if(!empty($steering)){
        //     $where.=" AND `pd`.`steering` = '$steering' ";
        // }
        // if(!empty($fuel_type)){
        //     $where.=" AND `pd`.`fuel_type` = '$fuel_type' ";
        // }
        // if(!empty($min_price)){
        //     $where.=" AND `pd`.`price` > '$min_price' ";
        // }
        // if(!empty($max_price)){
        //     $where.=" AND `pd`.`price` < '$max_price' ";
        // }
        // if(!empty($make)){
        //     $where.=" AND `pd`.`make` = '$make' ";
        // }
        // if(!empty($category)){
        //     $where.=" AND `pd`.`category` = '$category' ";
        // }
        $query = "SELECT rg.id as id,rg.country,cl.country_name,rg.log_id,pd.product_type,rg.`name`,rg.message,rg.image,
                (SELECT COUNT(*) FROM product WHERE product_type='Car' AND del_flag=0 AND owner=rg.id) as car_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Truck' AND del_flag=0 AND owner=rg.id) as truck_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Bus' AND del_flag=0 AND owner=rg.id) as bus_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Equipment'AND del_flag=0  AND owner=rg.id) as equipment_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Part' AND del_flag=0 AND owner=rg.id) as part_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Motorbike' AND del_flag=0 AND owner=rg.id) as Motorbike_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Aircraft' AND del_flag=0 AND owner=rg.id) as Aircraft_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Watercraft' AND del_flag=0 AND owner=rg.id) as Watercraft_number
     ,
                                     rg.company_name as company_name,
                                     (SELECT COUNT(*) FROM product WHERE `owner` = rg.id) as in_stock_product
                                      FROM `register` as `rg`
                LEFT JOIN `product` as `pd` ON `pd`.`owner`=`rg`.`id`
                INNER JOIN `country_list` as cl ON `cl`.`cc`=`rg`.`country` INNER JOIN `register_types` as rgt ON rgt.code=`rg`.`register_type`
                WHERE rg.company_name<>'' and rgt.`code`='m5'
                GROUP BY company_name


                                            ";

        //echo $query;
        /////PAGINATION PROCESS//////

        $sql_count_str=$query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);

        $links = new Pagination ($this->total_num_row, 3);
        $limit=$links->start_display;
        $this->pagination_html= $links->display();
        $this->current_page = $links->currentPage();
        $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $query.=$limit;
       //echo $query;
        $result = mysql_query($query);


        //echo $query;
        $this->membership = array();
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($membership, $r);
        }

        //echo mysql_error();
        //var_dump($this->userInfo);
        $this->membership=$membership;
        $cnx->close();
    }
     function loadcategory(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $category=array();
        $sql_str="
                SELECT name,title
                FROM product_type_list
                ORDER BY name ASC

                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($category, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $category;

    }
     private function loadMembershipgold(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $membershipgold=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";

        if(isset($_GET['country'])) $country=mysql_real_escape_string(stripcslashes($_GET['country'])); else $country="";
        if(isset($_GET['keyword'])) $keyword=mysql_real_escape_string(stripcslashes($_GET['keyword'])); else $keyword="";
        //if(isset($_GET['business_type'])) $business_type=mysql_real_escape_string(stripcslashes($_GET['business_type'])); else $business_type="";
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(isset($_GET['register_type'])) $register_type=mysql_real_escape_string(stripcslashes($_GET['register_type'])); else $register_type="";
        // if(isset($_GET['condition'])) $condition=mysql_real_escape_string(stripcslashes($_GET['condition'])); else $condition="";
        // if(isset($_GET['steering'])) $steering=mysql_real_escape_string(stripcslashes($_GET['steering'])); else $steering="";
        // if(isset($_GET['fuel_type'])) $fuel_type=mysql_real_escape_string(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
        // if(isset($_GET['min_price'])) $min_price=mysql_real_escape_string(stripcslashes($_GET['min_price'])); else $min_price="";
        // if(isset($_GET['max_price'])) $max_price=mysql_real_escape_string(stripcslashes($_GET['max_price'])); else $max_price="";
        // if(isset($_GET['make'])) $make=mysql_real_escape_string(stripcslashes($_GET['make'])); else $make="";
        // if(isset($_GET['category'])) $category=mysql_real_escape_string(stripcslashes($_GET['category'])); else $category="";
        $where.=" AND (`rg`.`member_type1` LIKE '%seller%' OR `rg`.`member_type1` LIKE '%forwarder%')";
        if(!empty($country)){
            $where.=" AND `rg`.`country`='$country' ";
        }
        if(!empty($keyword)){
            $where.=" AND (`rg`.`card_id` LIKE '%$keyword%' OR `rg`.`name` LIKE '%$keyword%' OR `rg`.`company_name` LIKE '%$keyword%' OR `rg`.`introduction` LIKE '%$keyword%') ";
        }
        // if(!empty($business_type)){
        //     $where.=" AND `rg`.`business_type` LIKE '%$business_type%' ";
        // }
        if(!empty($product_type)){
            $where.=" AND `pd`.`product_type` = '$product_type' ";
        }
        if(!empty($register_type)){
            //$where.=" AND `rg`.`register_type` = '$register_type' ";
            $this->register_type=$register_type;
        }
        if(!empty($this->register_type)){
            $where.=" AND register_type LIKE '%$this->register_type%' ";
        }

        $query = "SELECT rg.id as id,rg.country,cl.country_name,rg.log_id,pd.product_type,rg.`name`,rg.message,rg.image,
                (SELECT COUNT(*) FROM product WHERE product_type='Car' AND del_flag=0 AND owner=rg.id) as car_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Truck' AND del_flag=0 AND owner=rg.id) as truck_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Bus' AND del_flag=0 AND owner=rg.id) as bus_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Equipment'AND del_flag=0  AND owner=rg.id) as equipment_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Part' AND del_flag=0 AND owner=rg.id) as part_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Motorbike' AND del_flag=0 AND owner=rg.id) as Motorbike_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Aircraft' AND del_flag=0 AND owner=rg.id) as Aircraft_number,
                (SELECT COUNT(*) FROM product WHERE product_type='Watercraft' AND del_flag=0 AND owner=rg.id) as Watercraft_number
     ,
                                     rg.company_name as company_name,
                                     (SELECT COUNT(*) FROM product WHERE `owner` = rg.id) as in_stock_product
                                      FROM `register` as `rg`
                LEFT JOIN `product` as `pd` ON `pd`.`owner`=`rg`.`id`
                INNER JOIN `country_list` as cl ON `cl`.`cc`=`rg`.`country` INNER JOIN `register_types` as rgt ON rgt.code=`rg`.`register_type`
                WHERE rg.company_name<>'' and rgt.`code`='m3'
                GROUP BY company_name


                                            ";

        //echo $query;
        /////PAGINATION PROCESS//////

        $sql_count_str=$query;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);

        $links = new Pagination ($this->total_num_row, 4);
        $limit=$links->start_display;
        $this->pagination_html= $links->display();
        $this->current_page = $links->currentPage();
        $this->total_page= $links->numPages();
        ///////PAGINATION PROCESS///////
        $query.=$limit;
       //echo $query;
        $result = mysql_query($query);


        //echo $query;
        $this->membershipgold = array();
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            array_push($membershipgold, $r);
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        $this->membershipgold=$membershipgold;
        $cnx->close();
    }


    function countCountry($product_type=""){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
       $countcountry=array();

       $sql_str="
                SELECT
                    dc.country,
                    cl.country_name,
                    COUNT(dc.register_id) as `number`,
                    SUM(dc.car_count) as car_count,
                    SUM(dc.truck_count) as truck_count,
                    SUM(dc.bus_count) as bus_count,
                    SUM(dc.equipment_count) as equipment_count,
                    SUM(dc.part_count) as part_count,
                    SUM(dc.accessories_count) as accessories_count,
                    SUM(dc.motorbike_count) as motorbike_count,
                    SUM(dc.aircraft_count) as aircraft_count,
                    SUM(dc.watercraft_count) as watercraft_count,
                    SUM(car_count +
                        truck_count +
                        bus_count +
                        equipment_count +
                        part_count +
                        accessories_count +
                        motorbike_count +
                        aircraft_count +
                        watercraft_count) as `product_number`
                FROM `data_count` as `dc`
                INNER JOIN `country_list` as cl ON `cl`.`cc`=`dc`.`country`
                INNER JOIN `register` as rg ON rg.id=dc.register_id

                WHERE 1 AND rg.activated='1' AND (rg.member_type1 LIKE '%seller%' OR rg.member_type1 LIKE '%both%') $and
                GROUP BY `dc`.`country`
                ORDER BY `number` DESC, `product_number` DESC

                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($countcountry, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $countcountry;

    }
    /*
     * Public method: get number by condition data ------------------------------------
     * @return array | false.
     */
     function loadMembershiptype(){
        require_once dirname(dirname(__FILE__)) . '/config.php';
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();
        $and="";
        $membershiptype=array();


        $sql_str="
                SELECT code,title
                FROM register_types
                ORDER BY code ASC

                ";


        if( !$sql = @mysql_query($sql_str) ){
            return 0;
        }

        while($row=@mysql_fetch_assoc($sql)){
            array_push($membershiptype, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $membershiptype;

    }

    public function getExpertCompanyNumberByCountry(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();
        $where="";
        $count="";
        $join="";

        $query = "SELECT `cogister_type LIKE untry`, COUNT(rg.id) AS `number` FROM register as rg
                WHERE rg.re'%$this->register_type%' GROUP BY rg.`country` ";
        $result = mysql_query($query);
        //echo $query;
        while( $r = @mysql_fetch_assoc($result) ){
          //echo 'ok';
            $sellerNumber[$r['country']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);
        return $sellerNumber;
        $cnx->close();
    }



    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProductType(){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();


         $query = "SELECT COUNT(owner) AS number, pd.owner
                FROM product as pd
                INNER JOIN register as rg ON pd.owner=rg.id
                WHERE rg.register_type LIKE '%$this->register_type%'
				GROUP BY pd.owner
                "
        ;


        //echo $query;
        $result = mysql_query($query);

        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r['owner']]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;

    }
    /*
     * Public method: get number by condition like data ------------------------------------
     * @return array | false.
     */
    public function getNumberByProduct($column){
        require_once BASE_ROOT . 'core/class-connect.php';
        $sellerNumber=array();
        $cnx = new Connect();
        $cnx->open();
        $where_product_type="";
        if(isset($_GET['product_type'])) $product_type=mysql_real_escape_string(stripcslashes($_GET['product_type'])); else $product_type="";
        if(!empty($product_type)){
             $where_product_type=" AND product_type='$product_type'";
        }
        $query = "SELECT pd.`$column`, (SELECT COUNT(DISTINCT(owner)) FROM product WHERE `$column`=pd.`$column`$where_product_type) AS number
                FROM register as rg
                LEFT JOIN product as pd ON pd.owner=rg.id
                WHERE rg.register_type LIKE '%$this->register_type%'
                GROUP BY pd.`$column`  "
        ;
        //echo $query."<br/>";
        $result = mysql_query($query);

        while( $r = @mysql_fetch_assoc($result) ){
            //echo 'ok';
            $sellerNumber[$r[$column]]=$r['number'];
        }
        //echo mysql_error();
        //var_dump($this->userInfo);

        $cnx->close();
        return $sellerNumber;

    }


    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getTraderList()
    {
        return $this->traderList;
    }
    public function getmembership()
    {
        return $this->membership;
    }

    public function getmembershipgold()
    {
        return $this->membershipgold;
    }
      public function getmembershipSilver()
    {
        return $this->membershipgold;
    }
     public function getmembershipDiamon()
    {
        return $this->membershipDiamon;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

    /*
     * Public method: get vehicle data -------------------------------------------
     * @return string.
     */

}
?>
