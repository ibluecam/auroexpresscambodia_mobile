<?php
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

// retrieve var.
$url = trim($_POST['loc']);
 $id=$_POST['imgid'];



// connect to database.
require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();
$source_path="../image/upload/cars/";
 $preview_path="../image/upload/cars/preview_gallery-";
 $thumb_path="../image/upload/cars/thumb_gallery-";


// $url = str_replace(stripslashes(BASE_ROOT), '', $url);
 // $url = BASE_ROOT.$url;
// $url = @mysql_real_escape_string($url);



if( !@mysql_query("DELETE FROM `car_media` WHERE `photo_id`=$id ;") ){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to remove car image due a query error at ' . __FILE__ . ':' . __LINE__);
    $cnx->close();

    die('ng,Unable to remove image due a query error.');
}

$cnx->close();

// remove the actual image.

 if(file_exists($source_path.$url)){
                unlink($source_path.$url);
            }
            if(file_exists($preview_path.$url)){
                unlink($preview_path.$url);
            }
            if(file_exists($thumb_path.$url)){
                unlink($thumb_path.$url);
            }

die('ok,File removed successfully!');

