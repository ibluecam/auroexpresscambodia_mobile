<?php

if(!isset($_SESSION)) @session_start();
require_once dirname(dirname(dirname(__FILE__))) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
$cnx = new Connect();
$cnx->open();
if(isset($_POST['delete'])) $delete=$_POST['delete']; else $delete=array();

$ids = implode("', '", $delete);
$owner=$_SESSION['log_id'];
if($_SESSION['log_group']!='admin'){
	$and=" AND owner='$owner'";
}else{
	$and='';
}
$sql="UPDATE `product` SET is_deleted = CASE `car_id` ";
foreach($delete as $del_id){
    $sql.="WHEN '{$del_id}' THEN '1' ";
}
$sql .= "END ";
$sql .= "WHERE car_id IN ('$ids') $and";
if(!@mysql_query($sql)){
    require_once BASE_CLASS . 'class-log.php';
    LogReport::write('Unable to delete product due a query error at ' . __FILE__ . ':' . __LINE__);            
    echo "Deleted Fail!";
}    
if(mysql_affected_rows()>0){
	echo "Deleted Success!";
}else{
	echo "Delete failed! No item with these IDs in your Inventory.";
}
$cnx->close();

