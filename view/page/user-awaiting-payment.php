<?php
	if( !isset($_SESSION['log_group']) ){
		header('Location: '.BASE_RELATIVE.'login/');
	}
	
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>


<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/mywini/mywini.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar-->
            <div id="topMenu">
            	<p><a href="#" class="linkfade">My iBlue</a> > My Payment</p>
            </div>
        	<?php include("php/sidebar/my-wini.php");?>
            <div id="contact_info">
            	<p>Contact</p>
            </div>
            <div id="contact_detail">
            	<p class="title">BLAUDA CO., LTD.</p>
                <p>ADDRESS : M-2602, 32, Songdo <br/>
                    <span>Gwahak-ro, Yeonsu-gu,</span> <br/>
                    <span>Incheon, Korea.</span> <br/>
                    TEL            : +82-32-715-7098    <br/>
                    FAX            : +82-32-712-4885<br/>
                    WEBSITE  : <a href="http://www.blauda.com" class="linkfade">www.blauda.com</a>
				</p>
            </div>
            </div>           
            <div id="sectionContent"> 
            	<div id="payment_list">
                	<p>My Payment List</p>
                </div>
                <div id="checking">
                	<p>Check contents in awaiting payment list below and please make a payment.</p>
                </div>
                <div id="tab_wrap">
                	<div id="tab_history" class="awaiting currentTab">
                    	<p>Awaiting Payment</p>
                    </div>
                    <div id="tab_history" class="history">
                    	<p>Payment History</p>
                    </div>
                </div>
                <div id="tab_detail">
                	<div id="tab_title">
                    	<ul class="clearfix">
                        	<li class="date">Date</li>
                            <li class="service">Service</li>
                            <li class="period">Service Period</li>
                            <li class="price">Price</li>
                        </ul>
                    </div>
                    <div id="awaiting">
                    <?php for($i=1;$i<=6;$i++){?>
                    <div id="tab_content_awaiting" <?php if($i%2==0) echo 'class ='.'add_bg'?>>
                    	<ul class="clearfix">
                        	<li class="date">27.Mar.2014</li>
                            <li class="service">Expert Company</li>
                            <li class="period">2013.05.29 ~ 2014.05.30</li>
                            <li class="price">$3000</li>
                        </ul>
                    </div>
                    <?php }?>
                    </div>
                    <div id="history">
                     <?php for($i=1;$i<=10;$i++){?>
                    <div id="tab_content_history" <?php if($i%2==0) echo 'class ='.'add_bg'?>>
                    	<ul class="clearfix">
                        	<li class="date">28.Mar.2014</li>
                            <li class="service">Expert Company</li>
                            <li class="period">2013.05.29 ~ 2014.05.30</li>
                            <li class="price">$3000</li>
                        </ul>
                    </div>
                    <?php }?>
                    </div>
                </div>
                <div id="paynow">
                	<a href="#"><img src="<?php echo BASE_RELATIVE;?>images/awaitingpayment/paynow.png" class="linkfade" /></a>
                </div>
                <div id="paynow_detail">
                	<p>
                    	- To pay by credit card, click the button '<span>Pay Now</span>'.<br/>
                        - To pay by bank transfer, please transfer the money into the bank account below.<br/>
                        - Please contact us for immediate confirmation.
                    </p>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  