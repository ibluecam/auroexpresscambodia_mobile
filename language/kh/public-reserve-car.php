<?php
$_LANG = array(
    'RESERVE_CAR_HEADER' => 'Authentication required',
    'RESERVE_CAR_LOGIN_REQUIRED_LABEL' => 'Please login into your account in order to reserve this vehicle.',
    'RESERVE_CAR_LOGIN_ACCOUNT_LABEL' => 'Login or register your free account in order to be able to make vehicle reservation.',
    'RESERVE_CAR_LOGIN_BUTTON' => 'Sign in now',
    'RESERVE_CAR_REGISTER_BUTTON' => 'Register a free account'
);