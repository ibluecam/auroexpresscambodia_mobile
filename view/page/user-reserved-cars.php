<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' )
{
    // get form feed.
    $fstatus        = $_CLASS->getFormStatus();
    $fmessage       = $_CLASS->getFormMessage();
    $fstyle         = $_CLASS->getFormStyle();
    
    // get reservation list.
    $list           = $_CLASS->getReservationList();
    $car            = $_CLASS->getCar();
    $car_detail     = $_CLASS->getCarDetails();
    $car_features   = $_CLASS->getCarFeatures();
    $car_media      = $_CLASS->getCarMedia();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['RESERVED_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display reserved car.
                    if( count($list) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['RESERVED_NORESERVE_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                ?>
                <p><?php echo $_LANG['RESERVED_HEADER_PLACEHOLDER'];?></p>
                <form method="post" class="tool-wrap">
                    <input type="hidden" name="carInput" value="<?php echo $car['id'];?>" />
                    <input type="hidden" name="reservationInput" value="<?php echo $list['id'];?>" />
                    <input type="submit" onclick="return confirm('<?php echo $_LANG['RESERVED_REMOVE_CONFIRM_LABEL'];?>');" class="btn btn-small btn-danger" name="removeInput" value="Remove vehicle reservation" />
                </form>
                <div class="two-rows">
                    <div class="tab">
                        <h4><?php echo $_LANG['RESERVED_RESERVATION_CODE_LABEL'];?></h4>
                        <p style="font-weight:bold;color:#CC0000;"><?php echo $list['code'];?></p>
                        <br>
                        <h4><?php echo $_LANG['RESERVED_VEHICLE_INFO_HEADER'];?></h4>
                        <p><?php echo $car['maker'] . ' ' . $car['model'] . ' ' . $car['body_type'] . ' ' . $car['year'] . ' ' . $car['doors'] . ' ' . $_LANG['RESERVED_DOORS_LABEL'];?></p>
                        <p><?php echo $_LANG['RESERVED_SELLPRICE_LABEL'] . ': ' . CURRENCY_SYMBOL . ' ' . $car['price'];?></p>
                        <p><?php echo $car['miles'] . ' ' . $car['measure_type'];?></p>
                    </div>
                    <div class="tab">
                        <h4><?php echo $_LANG['RESERVED_CONTACT_HEADER'];?></h4>
                        <p><?php echo $list['name'];?></p>
                        <p><?php echo $list['address'];?></p>
                        <p><?php echo $list['zip'];?> <?php echo $list['city'];?></p>
                        <p><?php echo $list['country'];?></p>
                        <br>
                        <p><?php echo $list['phone'];?></p>
                        <p><?php echo $list['email'];?></p>
                        <p>IP: <?php echo $list['ip'];?></p>
                        <p><?php echo $_LANG['RESERVED_REGISTER_DATE_LABEL'] . ' ' . $list['date'];?></p>
                    </div>
                </div>
                <br>
                <?php
                        // display car feature icons.
                        if( count($car_features) > 0 ){
                ?>
                <h4><?php echo $_LANG['RESERVED_FEATURES_HEADER'];?></h4>
                <div class="two-rows" style="border:0;">
                <?php
                                for( $i=0; $i < count($car_features); $i++ ){
                ?>
                    <div class="feat-wrap">
                        <img src="<?php echo BASE_RELATIVE . $car_features[$i]['feat_icon'];?>" width="50" height="50" alt="<?php echo $car_features[$i]['feat_name'];?>" />
                        <p class="feat-name"><?php echo $car_features[$i]['feat_name'];?></p>
                    </div>
                <?php
                                }
                ?>
                    <div class="clearfix"></div>
                </div>
                <br>
                <?php
                        }
                    
                        // display media images.
                        if( count($car_media) > 0 ){
                ?>
                <h4><?php echo $_LANG['RESERVED_CAR_MEDIA_HEADER'];?></h4>
                <?php
                            for( $i=0; $i < count($car_media); $i++ ){
                                if( $car_media[$i]['mode'] == 'interior' || $car_media[$i]['mode'] == 'exterior' ){
                ?>
                <a href="<?php echo BASE_RELATIVE . $car_media[$i]['source'];?>" rel="prettyPhoto" style="margin:3px;float:left;display:block;">
                    <img src="<?php echo BASE_RELATIVE . $car_media[$i]['source'];?>" width="74" height="50" class="img-polaroid" />
                </a>
                <?php
                                }
                            }
                ?>
                <div class="clearfix"></div>
                <br>
                <?php
                        }
                    
                        // display car details.
                        if( count($car_detail) > 0 ){
                ?>
                <h4><?php echo $_LANG['RESERVED_VEHICLE_DETAILS_HEADER'];?></h4>
                <div class="two-rows">
                    <div class="tab">
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_ENGINE_SIZE_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['engine_size'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_TRIM_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['trim'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_TYPE_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['type'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_GEAR_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['gear'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_FUEL_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['fuel'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_COLOR_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['color'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_PREV_OWNER_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['prev_owners'];?></span>
                        </p>
                    </div>
                    <div class="tab">
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_LAST_SERVICE_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['last_service'] . ' ' . $car['measure_type'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_MOT_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['mot'];?> (YYYY.MM.DD)</span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_TAXBAND_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['tax_band'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_TOP_SPEED_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['top_speed'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_RPM_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['engine_torque_rpm'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_KW_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['engine_power_kw'];?></span>
                        </p>
                        <p class="tab-wrap">
                            <span class="tab-title"><?php echo $_LANG['RESERVED_TRANSMISSION_LABEL'];?></span>
                            <span class="tab-content"><?php echo $car_detail['transmission_type'];?></span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <br>
                <?php
                            // html.
                            $html = $car_detail['html'];
                            $html = trim($html);
                            
                            if( !empty($html) ){
                ?>
                <h4><?php echo $_LANG['RESERVED_MORE_DETAILS_HEADER'];?></h4>
                <div>
                    <?php echo $html;?>
                </div>
                <div class="clearfix"></div>
                <br>
                <?php
                            }
                        }   
                    } // end display vehicle.
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}