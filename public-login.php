<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
?>
<html>
<head>
<title>Page Sign In</title>
<meta content="Car Stock, auto-cambodia.com, used car, T7, spare parts, used bus, used truck, used motorbike" name="keywords">
	<meta content="Auto Cambodia is the most largest second hand vehicle & Brand-new Spare parts sales company in Cambodia, who provides Hyundai, Kia, Daewoo, Ssangyong and Renault Samsung vehicles, and Top Quality Auto Parts to the Cambodia Market." name="description">
	<meta property="og:title" content="Auto Cambodia - Used Cars & Spare Parts Company">
	<meta property="og:description" content="Auto Cambodia is the most largest second hand vehicle & Brand-new Spare parts sales company in Cambodia, who provides Hyundai, Kia, Daewoo, Ssangyong and Renault Samsung vehicles, and Top Quality Auto Parts to the Cambodia Market.">
	<meta property="og:url" content="http://www.auto-cambodia.com/">
	<meta property="og:image" content="http://www.auto-cambodia.com/ogp.jpg">
	<meta property="og:type" content="website">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">		
	
	<link rel="stylesheet" type="text/css" href="asset/css/import.css"/>
    <link rel="stylesheet" type="text/css" href="css/autocam_sign_in/style.css"/>	
	
</head>	
	
	




  
	<div id="main">
		<div class="row">
			 <div id="label-sign-in">
			    <p>
				  Sign in
				</p>
			 </div>
		</div>
		
		<div class="row">
		  <p id="form-introduction">
		    Lorem ipsum dolor sit amet, conse ctetur Aenean pulvinar ligula eget quam
dolor sit amet, conse ctetur adipiscing elit.
		  </p>
		</div>
   	 
	 
		<div class="row">
		
		<div id="form-wrapper">
		   <table >
		    <tbody>
              <tr>
				<td><label style='font-size:12px'>Email</label></td>
				<td><input type="text" name="txtemail" class="form-control" id="txt-box-email"/></td>
			  </tr>	
			  <tr>
				<td><label style='font-size:12px'>Password</label></td>
				<td><input type="password" name="txtpassword" class="form-control" id="txt-box-password"/></td>
			  </tr>
			  <tr>
			    <td colspan="2"><input type="submit" value="Sign in" name="cmdsignin" class="btn btn-primary btn-sm" id="button-signin"/></td>
			  </tr>
			  <tr>
			    
			    <td colspan="2">
				   <label id="decorate-text-link-password"><a href="#" >Forget your password?</a></label> <br/>
				   <label style='font-size:12px'>Not a member yet? <a href="#">Sign up</a></label>
				</td>
			  </tr>
			</tbody>
		   </table>
		<!-- close form-->	
		
			
		
		</div> <!-- #form-wrapper-->
        </div>
		
		
		
   </div><!--#main--->


</html>

	   