<?php
if(!isset($_SESSION)) @session_start();
header('Content-Type: application/json');

require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';
require_once BASE_CLASS . 'class-file-system.php';
require_once BASE_CLASS . 'class-pagination.php';

$cnx = new Connect();
$cnx->open();

$owner_id = $_SESSION['log_id'];
extract($_POST);

function getQueryConditionAndQueryURI($product_type, $make, $model, $chassis_no)
{
	$from = "FROM ( product AS p LEFT JOIN car_media AS c ON p.car_id = c.car_id ) ".
             "LEFT JOIN country_list AS cl ON p.location = cl.cc ".
             "INNER JOIN register AS r ON p.`owner` = r.user_id ";
    $where = "WHERE r.email='".$_SESSION['log_email']."' AND is_deleted=0 ";

    $query_uri = "";
    if(isset($chassis_no) && !empty($chassis_no))
        {
            $where .= "AND p.chassis_no = '". $chassis_no ."' ";  
            $query_uri .= "chassis_no={$chassis_no}&";      
        }
    else
    {
		if(isset($product_type) && !empty($product_type))
		{
			$where .= " AND product_type = '{$product_type}' ";
			$query_uri .= "product_type={$product_type}&";
		}

		if(isset($make) && !empty($make))
		{
			$where .= "AND make = '{$make}' ";
			$query_uri .= "make={$make}&";
		}

		if(isset($model) && !empty($model))
		{
			$where .= "AND model = '{$model}' ";
			$query_uri .= "model={$model}&";
		}
	}

	$group_by = "GROUP BY p.car_id ";
	$order_by = "ORDER BY p.created_date DESC ";

	return array(
				'query_condition'	=> "{$from}{$where}{$group_by}{$order_by}",
				'query_uri'			=> $query_uri
			);
}

// Load vehicle.
function loadVehicleData($query_condition, $limit)
{				  
	if(!$result = mysql_query("SELECT  p.*,
				                (CASE WHEN p.created_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) THEN 'new' ELSE 'old' END) AS arrival,
				                (SELECT COUNT(*) FROM message WHERE response_id = r.email AND car_id = p.car_id AND seen = 0) AS unread_message,
				                (SELECT COUNT(*) FROM message WHERE response_id = r.email AND car_id = p.car_id) AS message,
				                r.email,
				                (SELECT source FROM car_media WHERE primary_photo = 1 OR primary_photo = 2 and car_id = p.car_id LIMIT 1) AS carImg,
				                cl.country_name AS country,
				                cl.cc AS flag
				                {$query_condition}{$limit}"))
	{
		return array();
	}

	$vehicles= array();
    if( @mysql_num_rows($result) < 1 )
    {
    	return array();
    }

    while( $r = @mysql_fetch_assoc($result) ){
        array_push($vehicles, $r);
    }

    mysql_free_result($result);

    return $vehicles;
}

if (isset($_POST['delete']))
{	
	$query_condition_and_query_uri = getQueryConditionAndQueryURI($product_type, $make, $model, $chassis_no);
	$query_condition = $query_condition_and_query_uri['query_condition'];
	$query_uri = $query_condition_and_query_uri['query_uri'];
	$status = 1;
	$pagination = array();
	$vehicle = array();

	if(is_array($p_id))
	{
		$index = min($index);

		foreach ($p_id as $id) {
			if(!mysql_query("UPDATE product SET is_deleted = 1 WHERE id = '{$id}' and owner='$owner_id'"))
			{
			    $status = -1;
			}
		}
	}
	else{
		if(!mysql_query("UPDATE product SET is_deleted = 1 WHERE id = '{$p_id}' and owner='$owner_id'"))
		{
		    $status = 0;
		}
	}
	
	// Load car inventory.
	if($found_rows = mysql_query("SELECT SQL_CALC_FOUND_ROWS * {$query_condition}"))
	{
		$rows_count = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows_count'));

		$pagination = new Pagination($rows_count['rows_count'], $current_page, 5);
		$pagination->setQueryString($query_uri);

		if($pagination->getCurrentPage() == $pagination->getNumberOfPage())
		{
			$record_in_page = $pagination->getTotalRecord() - $pagination->getStartIndex();
		}
		else
		{
			$record_in_page = $pagination->getNumberOfRecordPerPage();
		}
		
		$index_of_record = (($pagination->getCurrentPage() <= 0 ? 0 : ($pagination->getCurrentPage() - 1)) * $pagination->getNumberOfRecordPerPage()) + $index;
		$limit = "LIMIT ". $index_of_record . ", " . ($record_in_page - $index) . " ";			
		$vehicle = loadVehicleData($query_condition, $limit);

		
		$pagination	= array(
			"link"				=> $pagination->generateLink(),
			"total_car"			=> $pagination->getTotalRecord(),
			"current_page"		=> $pagination->getCurrentPage(),
			"total_page"		=> $pagination->getNumberOfPage(),
			"records_per_page" 	=> $pagination->getNumberOfRecordPerPage(),
			"record_in_page"	=> $record_in_page
		);
	}

	echo json_encode(
		array(
			"status" 		=> $status,
			"pagination"	=> $pagination,
			"vehicle"		=> $vehicle
		)
	);
}
elseif (isset($_POST['sold']))
{
    if(!mysql_query("UPDATE product SET status = 'Sold' WHERE id = '{$p_id}' and owner='$owner_id'"))
    {
		echo json_encode(array('status' => 0 ));
    }
    else{
    	echo json_encode(array('status' => 1 ));
    }
}
elseif(isset($_POST['reserved'])){
    $car_ids = $_POST['reserved'];
    if(!mysql_query("UPDATE product SET status = 'Reserved' WHERE id = '{$p_id}' and owner='$owner_id'"))
    {
		echo json_encode(array('status' => 0 ));
    }
    else{
    	echo json_encode(array('status' => 1 ));
    }
}

$cnx->close();
