<?php
$_LANG = array(
    'ADDNEWS_HEADER' => 'Add news',
    'ADDNEWS_TITLE_LABEL' => 'Title',
    'ADDNEWS_PRODUCT_TYPE_LABEL' => 'Item Type',
    'ADDNEWS_TITLE_PLACEHOLDER' => 'Enter the new\'s title.',
    'ADDNEWS_SAVE_BUTTON' => 'Save news',
    'ADDNEWS_FORM_ERROR' => 'Please fill all fields before submiting your news.',
    'ADDNEWS_QUERY_ERROR' => 'Unable to save news due an internal error. See log for details.',
    'ADDNEWS_QUERY_SUCCESS' => 'News saved successfully!'
);