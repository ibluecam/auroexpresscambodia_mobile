<?php

require_once './models/Make.php';
 
class ControllerMake
{
 
    private $db;
    private $pdo;
    function __construct() 
    {
        require_once './application/DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->pdo = $this->db->connect();
    }
 
    function __destruct() { }
 
    public function updateMake($itm) 
    {
        
        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_make 
                                        SET make_name = :make_name 
                                        WHERE make_id = :make_id');

        $result = $stmt->execute(
                            array('make_name' => $itm->make_name, 
                                    'make_id' => $itm->make_id) );
        
        return $result ? true : false;
    }

    public function deleteMake($make_id, $is_deleted) 
    {

        $stmt = $this->pdo->prepare('UPDATE tbl_carfinder_make 
                                        SET is_deleted = :is_deleted
                                        WHERE make_id = :make_id');


        $result = $stmt->execute(
                            array('is_deleted' => $is_deleted, 
                                    'make_id' => $make_id) );
        
        return $result ? true : false;
    }

    public function insertMake($itm) 
    {
        $stmt = $this->pdo->prepare('INSERT INTO tbl_carfinder_make( make_name ) 
                                        VALUES( :make_name )');
        
        $result = $stmt->execute(
                            array('make_name' => $itm->make_name) );
        
        return $result ? true : false;
    }
 
    
    public function getMake() 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                FROM tbl_carfinder_make 
                                 WHERE is_deleted = 0 ORDER BY make_name ASC');

        $stmt->execute();

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Make();
            $itm->make_id = $row['make_id'];
            $itm->make_name = $row['make_name'];

            $array[$ind] = $itm;
            $ind++;
        }
        return $array;
    }

    public function getMakeBySearching($search) 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                        FROM tbl_carfinder_make 
                                        WHERE is_deleted = 0 AND make_name LIKE :search ORDER BY make_name ASC');

        $stmt->execute( array('search' => '%'.$search.'%'));

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Make();
            $itm->make_id = $row['make_id'];
            $itm->make_name = $row['make_name'];

            $array[$ind] = $itm;
            $ind++;
        }
        return $array;
    }


    public function getMakeByMakeId($make_id) 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                        FROM tbl_carfinder_make WHERE make_id = :make_id');

        $stmt->execute( array('make_id' => $make_id));

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Make();
            $itm->make_id = $row['make_id'];
            $itm->make_name = $row['make_name'];

            return $itm;
        }
        return null;
    }


    public function getMakeByMake($make_name) 
    {
        $stmt = $this->pdo->prepare('SELECT * 
                                        FROM tbl_carfinder_make WHERE make_name = :make_name');

        $stmt->execute( array('make_name' => $make_name));

        $array = array();
        $ind = 0;
        foreach ($stmt as $row) 
        {
            // do something with $row
            $itm = new Make();
            $itm->make_id = $row['make_id'];
            $itm->make_name = $row['make_name'];

            return $itm;
        }
        return null;
    }


}
 
?>