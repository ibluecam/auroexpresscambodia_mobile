<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

/*
 * Set security question variables ---------------------------------------------------
 */



 include('php/ip2locationlite.class.php');
//Load the class
$ipLite = new ip2location_lite;
$ipLite->setKey('0ceddca8fc2527329d305fb922e7bbade230ca741a2a68055b649696326992be');
?>


<!--/END OF ORIGINAL CODE-->
	

       				
    <div id="wrapper-feedback" > 
     
		<div id="feedback"><!-- #feedback -->
        <div class="page-title">Feedback</div>
        
        <form id="defaultForm" class="form-horizontal form-feedback" method="post">
        <div class="status">Thank's for your's valuable feedback, We will value feedback and make it better</div>
        
        <div style="overflow:auto">
        <label>Name<span style="color:red">*</span></label>
        <div class="form-group" style="width:340px; padding-top:3px; float:left">
		<input value="" class="form-control border" type="text" name="name" id="user_id" style="width:320px"/>
		</div>
        </div>
        
         <div style="overflow:auto">
        <label>Company</label>
         <div class="form-group" style="width:340px; padding-top:3px; float:left">
		<input value="" class="form-control border" type="text" name="company_name" style="width:320px"/>
		</div>
        </div>
        
        
         <div style="overflow:auto">
        <label>Email<span style="color:red">*</span></label>
         <div class="form-group" style="width:340px; padding-top:3px; float:left">
		<input value="" class="form-control border" type="text" name="email" id="email" style="width:320px"/>
		</div>
        </div><br/>
        
        <p style=" font-size:14px">1. Rate our website on the following aspects:</p><br/>
        
        <table cellpadding="0" cellspacing="0" class="web-rate">
        <tr>
        <th></th>
        <th>Excellent</th>
        <th>Very good</th>
        <th>Good</th>
        <th>Average</th>
        <th>Poor</th>
        </tr>
        <tr>
        <td style="text-align:left; padding-left:15px;  border-right:1px solid #cacaca; width:200px">Home page</td>
        <td><input type="checkbox" value="excellent" name="homepage" class="homepage" /></td>
        <td><input type="checkbox" value="very good" name="homepage" class="homepage" /></td>
        <td><input type="checkbox" value="good" name="homepage" class="homepage" /></td>
        <td><input type="checkbox" value="average" name="homepage" class="homepage" /></td>
        <td><input type="checkbox" value="poor" name="homepage" class="homepage" /></td>
        </tr>
        <tr>
        <td style="text-align:left; padding-left:15px;  border-right:1px solid #cacaca;  width:200px">Technical support</td>
        <td><input type="checkbox" value="excellent" name="technical" class="technical"/></td>
        <td><input type="checkbox" value="very good" name="technical" class="technical"/></td>
        <td><input type="checkbox" value="good" name="technical" class="technical"/></td>
        <td><input type="checkbox" value="average" name="technical" class="technical"/></td>
        <td><input type="checkbox" value="poor" name="technical" class="technical"/></td>
        </tr>
        <tr>
        <td style="text-align:left; padding-left:15px;  border-right:1px solid #cacaca;  width:200px">Product Information</td>
        <td><input type="checkbox" value="excellent" name="product" class="product"/></td>
        <td><input type="checkbox" value="very good" name="product" class="product"/></td>
        <td><input type="checkbox" value="good" name="product" class="product"/></td>
        <td><input type="checkbox" value="average" name="product" class="product"/></td>
        <td><input type="checkbox" value="poor" name="product" class="product"/></td>
        </tr>
        <tr>
        <td style="text-align:left; padding-left:15px; border-right:1px solid #cacaca;  width:200px">Contacts</td>
        <td><input type="checkbox" value="excellent" name="contact" class="contact"/></td>
        <td><input type="checkbox" value="very good" name="contact" class="contact"/></td>
        <td><input type="checkbox" value="good" name="contact" class="contact"/></td>
        <td><input type="checkbox" value="average" name="contact" class="contact"/></td>
        <td><input type="checkbox" value="poor" name="contact" class="contact"/></td>
        </tr>
        
        </table><br/>
        
        <p style=" font-size:14px">2. How likely are you to:</p><br/>
        
         <table cellpadding="0" cellspacing="0" class="web-rate">
        <tr>
        <th></th>
        <th>Very likely</th>
        <th>Likely</th>
        <th>Unlikely</th>
        <th>Very unlikely</th>
        <th>Not sure</th>
        </tr>
        <tr>
        <td style="text-align:left; padding-left:15px;  border-right:1px solid #cacaca; width:200px">Revisit this site on a 
regular basis</td>
        <td><input type="checkbox" value="very likely" name="revisit" class="revisit" /></td>
        <td><input type="checkbox" value="likely" name="revisit" class="revisit"/></td>
        <td><input type="checkbox" value="unlikely" name="revisit" class="revisit"/></td>
        <td><input type="checkbox" value="very unlikely" name="revisit" class="revisit"/></td>
        <td><input type="checkbox" value="not sure" name="revisit" class="revisit"/></td>
        </tr>
        <tr>
        <td style="text-align:left; padding-left:15px;  border-right:1px solid #cacaca;  width:200px">Recommend our site</td>
        <td><input type="checkbox" value="very likely" name="recommend_site" class="recommend" /></td>
        <td><input type="checkbox" value="likely" name="recommend_site" class="recommend"/></td>
        <td><input type="checkbox" value="unlikely" name="recommend_site" class="recommend"/></td>
        <td><input type="checkbox" value="very unlikely" name="recommend_site" class="recommend"/></td>
        <td><input type="checkbox" value="not sure" name="recommend_site" class="recommend"/></td>
        </tr>
        </table><br/>
        
        <p style=" font-size:14px">3. How often do you visit our site?</p><br/>
        
         <div class="member">
       
	<div style="font-size:14px;"><input type="checkbox" value="Everyday" name="visit_our_site" class="visit"/><span style="margin-left:15px">Everyday</span></div>
    <div style="font-size:14px;"><input type="checkbox" value="Several Time a week" name="visit_our_site" class="visit"/><span style="margin-left:15px">Several Time a week</span></div>
    <div style="font-size:14px;"><input type="checkbox" value="About once a week" name="visit_our_site" class="visit"/><span style="margin-left:15px">About once a week</span></div>
    <div style="font-size:14px;"><input type="checkbox" value="Several times a month" name="visit_our_site" class="visit"/><span style="margin-left:15px">Several times a month</span></div>
    <div style="font-size:14px;"><input type="checkbox" value="About once a month" name="visit_our_site" class="visit"/><span style="margin-left:15px">About once a month</span></div>
    <div style="font-size:14px;"><input type="checkbox" value="Less than once a month" name="visit_our_site" class="visit"/><span style="margin-left:15px">Less than once a month</span></div>
    <div style="font-size:14px;"><input type="checkbox" value="This is my first visit here" name="visit_our_site" class="visit"/><span style="margin-left:15px">This is my first visit here</span></div>
                                    			
    	</div><br/>
        <p style=" font-size:14px">4. Suggestion</p><br/>
        
        <textarea value="" name="comment" class="form-control border" style="width:700px; height:200px"/></textarea><br/><br/>
        
        <input type="submit" name="feedback-send" id="feedback-send" class="linkimg" value="Submit"/>

            </form>
		</div><!-- #feedback -->
		
           				
	 <div class="clear"></div>
	</div>

<script type="text/javascript">
 
$(document).ready(function() {

    $('#defaultForm').bootstrapValidator({
	  

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and cannot be empty'
                    },
 
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: 'Name must be more than 2 and less than 30 characters'
                    },
                    regexp: {
                        regexp:/^[a-zA-Z_ ]*$/,
                        message: 'The Name must be exist only characters'
                    },
					
				}
 


            },
            email: {
                validators: {
					 notEmpty: {
                        message: 'The Email is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The Input is not a valid email address.'
                    },
                }
            },
 
        }
    });
	
	
});
</script> 
<script>


var $homepage = $('input.homepage');
var $technical = $('input.technical');
var $product = $('input.product');
var $contact = $('input.contact');
var $revisit = $('input.revisit');
var $recommend = $('input.recommend');
var $visit = $('input.visit');


//checkbox homepage
$homepage.click(function() {

 $checked = $(this).is(':checked') ; // check if that was clicked.
 $homepage.removeAttr('checked'); //clear all checkboxes
 $(this).attr('checked', $checked); // update that was clicked.

});

//checkbox technicle
$technical.click(function() {

 $checked = $(this).is(':checked') ; // check if that was clicked.
 $technical.removeAttr('checked'); //clear all checkboxes
 $(this).attr('checked', $checked); // update that was clicked.

});

//checkbox product
$product.click(function() {

 $checked = $(this).is(':checked') ; // check if that was clicked.
 $product.removeAttr('checked'); //clear all checkboxes
 $(this).attr('checked', $checked); // update that was clicked.

});

//checkbox contact
$contact.click(function() {

 $checked = $(this).is(':checked') ; // check if that was clicked.
 $contact.removeAttr('checked'); //clear all checkboxes
 $(this).attr('checked', $checked); // update that was clicked.

});

//checkbox revisit
$revisit.click(function() {

 $checked = $(this).is(':checked') ; // check if that was clicked.
 $revisit.removeAttr('checked'); //clear all checkboxes
 $(this).attr('checked', $checked); // update that was clicked.

});

//checkbox recommend
$recommend.click(function() {

 $checked = $(this).is(':checked') ; // check if that was clicked.
 $recommend.removeAttr('checked'); //clear all checkboxes
 $(this).attr('checked', $checked); // update that was clicked.

});

//checkbox visit
$visit.click(function() {

 $checked = $(this).is(':checked') ; // check if that was clicked.
 $visit.removeAttr('checked'); //clear all checkboxes
 $(this).attr('checked', $checked); // update that was clicked.

});

</script>

