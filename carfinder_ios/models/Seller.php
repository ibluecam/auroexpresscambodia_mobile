<?php
 
class Seller
{
	public $seller_id;
    public $seller_name;
    public $contact_no;
    public $email;
    public $lat;
    public $lon;
    public $sms;
    public $address;
    public $user_id;
    public $profile_pic;

    // constructor
    function __construct() 
    {

    }
 
    // destructor
    function __destruct() 
    {
         
    }
}
 
?>