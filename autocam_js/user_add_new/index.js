$(document).ready(function($) {	
	// Tab Category
	$("#menuTab ul li").click(function(){
		$("#menuTab ul li").removeClass("current");
		$(this).addClass("current");
	});
	
	// Text Editor
	tinymce.init({
		selector: "textarea",
		theme: "modern",
		height: "250",
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor colorpicker textpattern"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic strikethrough | fontsizeselect | fontselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		toolbar2: "print preview media | forecolor backcolor | charmap emoticons",
		image_advtab: true,
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		]
	});

	$('.form-herizontal').submit(function(){
	
		$('input[type="text"]').removeClass('text-box-err');
		$('.error').html('');
		
		//if you user didn't check to agree our term of use and policy			
		//if($('#agree').prop('checked') == false){
	//		$('.warming').html('You need to agree our term of use and policy!');
	//		$('.warming').fadeIn('slow');		
	//		return false;
	//	}else{
	//		$('.warming').fadeOut('slow');
	//	}
		
		$.post($('.form-herizontal').attr('action'), $('.form-herizontal').serialize(), function(data) {
			
			if (data.status == true) {
				$('.succed').html('You have successfully add new product!');
				$('.succed').fadeIn('slow');
				$('.form-herizontal').trigger('reset');
			}else{
				$.each(data.errors, function(key, val) {			
					$('#'+ key).addClass('text-box-err');
					$('#'+ key +'_err').html(val);
					$('.error').fadeIn('slow');
				})
			}
		}, 'json');
		
		return false;
	});

	//  upload image

	$("#file").change(function(){

		alert('ok');
		
		var src=$("#file").val();
		
		if(src!=""){
			
			formdata= new FormData();
			var numfiles=this.files.length;
			var i, file, progress, size;
			
			for(i=0;i<numfiles;i++){
				
				file = this.files[i];
				size = this.files[i].size;
				name = this.files[i].name;
				
				if (!!file.type.match(/image.*/)){
					
					if((Math.round(size))<=(1024*1024)){
						
						var reader = new FileReader();
						reader.readAsDataURL(file);
						$("#preview").show();
						$('#preview').html("");
						
						reader.onloadend = function(e){
							var image = $('<img>').attr('src',e.target.result);
							$(image).appendTo('#preview');
						};
						
						formdata.append("file[]", file);
						
						if(i==(numfiles-1)){
							
							$("#info").html("wait a moment to complete upload");
							$.ajax({
								url: "product/imageUploader",
								type: "POST",
								data: formdata,
								processData: false,
								contentType: false,
								success: function(res){
									if(res!="0")
										$("#info").html("Successfully Uploaded");
									else
										$("#info").html("Error in upload. Retry");
								}
							});
						}
					}else{
						
						$("#info").html(name+"Size limit exceeded");
						$("#preview").hide();
						
						return;
					}
				}else{
					
					$("#info").html(name+"Not image file");
					$("#preview").hide();
					
					return;
				}
			}
		}else{
			
			$("#info").html("Select an image file");
			$("#preview").hide();
			
			return;
		}
		
		return false;
		
	});
});