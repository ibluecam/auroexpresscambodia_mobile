<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
include('view/3rdparty/pagination/pagination.php');
class publicDealofTheweek
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'dealof-theweek';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $total_num_row;
    var $pagination_html;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicDealofTheweek($lang=array())
    {
        $this->lang = $lang;
		
    }


    function searchDealweek(){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $title = $_POST['deal_option'];
        $text = $_POST['deal_text'];

        $cnx = new Connect();
        $cnx->open();
        
        if($title=="Topic"){
            $sql_search="SELECT * from deal_week  WHERE title LIKE '%$text%' ORDER BY id DESC";
        }            
        
        else{
            $sql_search="SELECT * from deal_week  WHERE name LIKE '%$text%' ORDER BY id DESC";
        }
         
        
        /////PAGINATION PROCESS//////

        $sql_count_str=$sql_search;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row, 25);
        $limit=$links->start_display;
        $this->pagination_html.= $links->display(); 

        ///////PAGINATION PROCESS///////
        $sql_search.=$limit; 

        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
       $this->deal_week = array();
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($this->deal_week, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $this->deal_week; 
        
    }

    function loadDealweek(){
        
        require_once BASE_CLASS . 'class-connect.php';
      
        $cnx = new Connect();
        $cnx->open();
        
        $sql_search="SELECT * from deal_week  ORDER BY id DESC";
        /////PAGINATION PROCESS//////

        $sql_count_str=$sql_search;
        $sql_count = @mysql_query($sql_count_str);
        $this->total_num_row= @mysql_num_rows($sql_count);
        
        $links = new Pagination ($this->total_num_row, 25);
        $limit=$links->start_display;
        $this->pagination_html.= $links->display(); 

        ///////PAGINATION PROCESS///////
        $sql_search.=$limit; 

        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }
        
         
       $this->deal_week = array();
       
        while( $r = @mysql_fetch_assoc($sql)){
           
            array_push($this->deal_week, $r);
          
        }
         
  
        @mysql_free_result($sql);
        $cnx->close();
        return $this->deal_week; 
        
    }


    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    public function getPageHTML()
    {
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `page` WHERE `page_slug`='$this->slug' AND `page_group`='$this->group' LIMIT 1;") )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        if( @mysql_num_rows($sql) != 1 )
        {
            $cnx->close();

            require_once BASE_ROOT . 'core/class-log.php';

            LogReport::write('Unable to load page information at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $this->form_message = 'Unable to load page content due an internal error.';
            $this->form_status = true;
            $this->form_style = 'alert-error';

            return;
        }

        $r = @mysql_fetch_assoc($sql);

        $result                     = array();
        $result['id']               = $r['id'];
        $result['meta_keywords']    = $r['meta_keywords'];
        $result['meta_description'] = $r['meta_description'];
        $result['meta_robots']      = $r['meta_robots'];
        $result['page_title']       = $r['page_title'];
        $result['page_slug']        = $r['page_slug'];
        $result['page_publish']     = $r['page_publish'];
        $result['page_author']      = $r['page_author'];
        $result['creation']         = $r['creation'];
        $result['editable']         = $r['editable'];
        $result['page_group']       = $r['page_group'];
        $result['html']             = $r['html'];

        @mysql_free_result($sql);
        $cnx->close();

        return $result;
    }
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

}
?>
