// Slide Vehicle Detail
var src_array = Array();
$('a#thumbImage').each(function() {
	src_array.push($(this).attr('href'));
});
console.log(src_array);	

var thumb_arry=[];
for (i=0;i<src_array.length;i++){
	thumb_arry[i]={image:src_array[i]};
}
console.log(thumb_arry);	

jQuery.PictureSlides.set({
	// Switches to decide what features to use
	useFadingIn : false,
	useFadingOut : false,
	useFadeWhenNotSlideshow : true,
	useFadeForSlideshow : true,
	useDimBackgroundForSlideshow : true,
	loopSlideshow : false,
	usePreloading : true,
	useAltAsTooltip : true,
	useTextAsTooltip : false,
	
	// Fading settings
	fadeTime : 500, // Milliseconds	
	timeForSlideInSlideshow : 2000, // Milliseconds

	// At page load
	startIndex : 1,	
	startSlideShowFromBeginning : true,
	startSlideshowAtLoad : false,
	dimBackgroundAtLoad : false,

	// Large images to use and thumbnail settings		
	images :thumb_arry,				
	thumbnailActivationEvent : "click",

	// Classes of HTML elements to use
	mainImageClass : "picture-slides-image", // Mandatory
	mainImageFailedToLoadClass : "picture-slides-image-load-fail",
	imageLinkClass : "picture-slides-image-link",
	fadeContainerClass : "picture-slides-fade-container",
	imageTextContainerClass : "picture-slides-image-text",
	previousLinkClass : "picture-slides-previous-image",
	nextLinkClass : "picture-slides-next-image",
	imageCounterClass : "picture-slides-image-counter",
	startSlideShowClass : "picture-slides-start-slideshow",
	stopSlideShowClass : "picture-slides-stop-slideshow",
	thumbnailContainerClass: "picture-slides-thumbnails",
	dimBackgroundOverlayClass : "picture-slides-dim-overlay"
});

$(function(){
	
	$('#contact_popup').click(function(){
		$('#popbox').fadeToggle(1000);
	});
		
});