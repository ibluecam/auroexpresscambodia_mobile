<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>


<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/contact/contact.css" />

<div id="sectionContenWrapper">
	<div id="conInfo">
		<p>Contact Information</p>
	</div>
	<div id="textMap" class="clearfix">
		<div id="textInfo">
			<p>Angkor Auto is a global leading provider for specialized knowledge from car exporter to the buyer it is 
			on the basis of individual management services combined with international expertise and its 
			work around the world manily dealing with domestic market.</p>
			
			<div class="address">
				<p>Address : <span>#B26, St. North Bridge Sangkat Toek Thlar, Khann Sensok <br/>
				Phnom Penh, Cambodia.</span></p>
				<p>Email : <span>info@angkorauto.com</span></p>
				<p>Tel : <span>+ (855) 23-964-359</span></p>
				<p>Website : <span>www.angkorauto.com</span></p>
				<p>Work days : <span>Monday to Friday</span></p>
				<p>Work hours : <span>8:00 AM - 5:00 PM</span></p>
			</div>
		</div>
		<div id="mapInfo">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.8513810965997!2d104.87070899999996!3d11.562510000000009!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951b8ccbe8fe7%3A0x786256b0cef348c0!2sSoft+Bloom!5e0!3m2!1sen!2skh!4v1419993390598" width="600" height="300" frameborder="0" style="border:0"></iframe>
		</div>
	</div><!-- #textMap -->
	
	<div id="support" class="clearfix">
		<div class="leaderSupport border">
			<div class="marBorder">
				<div class="titleTop">
					<p>Membership and Business Support</p>
				</div>
				<ul>
					<li>
						<div class="divImg">
							<img src="<?php echo BASE_RELATIVE; ?>img/public_contact/sell_support.png" />
						</div>
						<div class="divText">
							<p><span>Soklin Khun </span>/ Marketing and Business Development Manager</p>
							<p>Langauge : ENG/KH</p>
							<p>Email : khunsoklin@blauda.com</p>
							<p>Tel : 015 668 867</p>
						</div>
					</li>
					<!--<li>
						<div class="divImg">
							<img src="<?php echo BASE_RELATIVE; ?>img/public_contact/sell_support.png" />
						</div>
						<div class="divText">
							<p><span>Soklin Khun </span>/ Marketing and Business Development Manager</p>
							<p>Langauge : ENG/KH</p>
							<p>Email : khunsoklin@blauda.com</p>
							<p>Tel : 015 668 867</p>
						</div>
					</li>
					<li class="clearBorder">
						<div class="divImg">
							<img src="<?php echo BASE_RELATIVE; ?>img/public_contact/sell_support.png" />
						</div>
						<div class="divText">
							<p><span>Soklin Khun </span>/ Marketing and Business Development Manager</p>
							<p>Langauge : ENG/KH</p>
							<p>Email : khunsoklin@blauda.com</p>
							<p>Tel : 015 668 867</p>
						</div>
					</li>-->
				</ul>
			</div>			
		</div>
		
		<div class="leaderSupport marLeft">
			<div class="titleTop">
				<p>Technical and Graphic Support</p>
			</div>
			<ul>
				<li>
					<div class="divImg">
						<img src="<?php echo BASE_RELATIVE; ?>img/public_contact/it_support.png" />
					</div>
					<div class="divText">
						<p><span>Tourn Vuthy </span>/ IT Manager</p>
						<p>Langauge : ENG/KH</p>
						<p>Email : support@angkorauto.com</p>
						<p>Tel : 097 461 5520</p>
					</div>
				</li>
				<!--<li>
					<div class="divImg">
						<img src="<?php echo BASE_RELATIVE; ?>img/public_contact/it_support.png" />
					</div>
					<div class="divText">
						<p><span>Tourn Vuthy </span>/ IT Manager</p>
						<p>Langauge : ENG/KH</p>
						<p>Email : support@angkorauto.com</p>
						<p>Tel : 097 461 5520</p>
					</div>
				</li>
				<li class="clearBorder">
					<div class="divImg">
						<img src="<?php echo BASE_RELATIVE; ?>img/public_contact/it_support.png" />
					</div>
					<div class="divText">
						<p><span>Tourn Vuthy </span>/ IT Manager</p>
						<p>Langauge : ENG/KH</p>
						<p>Email : support@angkorauto.com</p>
						<p>Tel : 097 461 5520</p>
					</div>
				</li>-->
			</ul>
		</div>
	</div><!-- #support -->
</div><!-- #sectionContenWrapper -->