<?php
/*
 * validate admin session ------------------------------------------------------
 */
if( $_SESSION['log_group'] == 'user' ) {
?>
        <div id="tab-wrapper">
            <a id="carId" href="#" style="color:#666;border:0" class="tab-title" onclick="displayMenuTab('car_tab');return false;"><?php echo $_GLANG['TOP_MENU_CAR_MANAGER_HEADER'];?></a>
            <div id="car_tab">
                <a href="<?php echo BASE_RELATIVE;?>home/new-car/"><?php echo $_GLANG['TOP_MENU_NEW_CAR_LABEL'];?></a>
                <a href="<?php echo BASE_RELATIVE;?>home/car-manager/"><?php echo $_GLANG['TOP_MENU_CAR_MANAGER_LABEL'];?></a>
                <a href="<?php echo BASE_RELATIVE;?>home/reservation-manager/"><?php echo $_GLANG['TOP_MENU_RESERVATION_MANAGER_LABEL'];?></a>

            </div>
            
            <div id="setting_tab">
                <a href="<?php echo BASE_RELATIVE;?>my-account/"><?php echo $_GLANG['TOP_MENU_MY_ACCOUNT_LABEL'];?></a>
                
            </div>
            
        </div>
<?php } ?>

