<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class admineditregistertype {
    private $lang;
    private $form_message;
    private $form_status;
    private $form_style;
   
    /**
     * Constructor
     * <br>---------------------------------------------------------------------
     */
    public function admineditregistertype($lang=array()){
        $this->lang = $lang;
        if(isset($_POST['submit'])){
            $this->handleSaveForm();
        }
        
       
    }
    
    
    private function handleSaveForm(){
        require_once BASE_CLASS . 'class-connect.php';
        $cnx = new Connect();
        $cnx->open();
        if(isset($_GET['id'])) $id=$_GET['id']; else $id="";
        if(isset($_POST['title'])) $title=$_POST['title']; else $title="";
        if(isset($_POST['note'])) $note=$_POST['note']; else $note="";
        if(isset($_POST['max_product'])) $max_product=$_POST['max_product']; else $max_product=0;
        if(isset($_POST['max_image'])) $max_image=$_POST['max_image']; else $max_image=0;

        //code, title, note, max_product, max_image
        $sql_insert="INSERT INTO register_types (`code`, title, note, `max_product`, max_image) 
                    SELECT CONCAT('m', MAX(id)+1),
                        '$title',
                        '$note',
                        '$max_product',
                        '$max_image'
                    FROM register_types
                    ";
        $sql_update="UPDATE register_types SET 
                        title='$title', 
                        note='$note', 
                        `max_product`='$max_product', 
                        max_image='$max_image' 
                    WHERE `id`='$id'
                    ";

        if(!empty($id)){
            $sql=$sql_update;
        }else{
            $sql=$sql_insert;
        }
        //mysql_affected_rows()
        //echo $sql_insert;
       
        if( !$sql = @mysql_query($sql) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to save member setting due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_SAVE_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $cnx->close();
            return;
        }
        if(!empty($id)&&mysql_affected_rows()<=0){
            require_once BASE_CLASS . 'class-log.php';
            //LogReport::write('Unable to save member setting due a query error at ' . __FILE__ . ':' . __LINE__);
            $this->form_message = $this->lang['PAGE_NOT_FOUND_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            $cnx->close();
            return;
        }
        $this->form_message =  $this->lang['PAGE_SAVE_QUERY_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        @mysql_free_result($sql);
        $cnx->close();
    }
   
    private function existMemberSetting($id){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        if( !$sql = @mysql_query("SELECT * FROM `register_types` WHERE `id`='$id' ") ){
            require_once BASE_CLASS . 'class-log.php'; 
            $cnx->close();
            return false;
        }
        
        if( @mysql_num_rows($sql) >0  ){
            return true;
        }

        @mysql_free_result($sql);
        $cnx->close();
        
    }


    public function getMemberSetting($id){
        require_once BASE_CLASS . 'class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        $memberSetting=array();
        if( !$sql = @mysql_query("SELECT * FROM `register_types` WHERE `id`='$id' ") ){
            require_once BASE_CLASS . 'class-log.php'; 
            $cnx->close();
            return false;
        }
        while($row = @mysql_fetch_assoc($sql)){
            array_push($memberSetting, $row);
        }

        @mysql_free_result($sql);
        $cnx->close();
        return $memberSetting;
    }
    /**
     * Public method: get form status
     * <br>---------------------------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /**
     * Public method: get form message 
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /**
     * Public method: get form style
     * <br>---------------------------------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}