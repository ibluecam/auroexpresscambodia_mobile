<div id="wrapper-term-condition">
    <h1>Terms and Condition</h1>
	<p>
		Angkorauto.com run the website as an online advertising and research service for car buyers, sellers. Personally recognized information collected by angkorauto.com through your use of the Site may be stored on servers or in a cookie on your computer and may be used by angkorauto.com for any of the following purposes:
	</p>
	<ul>
		<li>
			To confirm a request that you make (e.g., in order to send you a price quote or other information you have requested, angkorauto.com may share your contact information with a participating  angkorauto.com dealer and/or manufacturer).
		</li>
		<li>
			To follow up on any request that you have previously made (e.g., angkorauto.com may contact you to determine whether the dealer and/or manufacturer responded to your original request).
		</li>
		<li>
			To communicate with you regarding the angkorauto.com service (e.g., angkorauto.com may send you emails or call you to confirm a price quote request or to notify you that you have received a lead regarding a vehicle you have advertised or to request that you participate in a survey).
		</li>
		<li>
			To send promotional materials, newsletters or other content via email or otherwise. If you no longer wish to receive a particular email newsletter or other promotion from angkorauto.com, you must follow the opt-out instructions included in such email newsletters and promotions.
		</li>
		<li>
			To permit third parties (to whom angkorauto.com may disclose personally noticeable information specifically for direct marketing purposes) to send you promotional or informational messages, but only if you have first affirmatively agreed to receive such messages.
		</li>
		<li>
			To improve your user experience.
		</li>
		<li>
			To manage Angkorauto.com’s business and its relationships with Affiliates, customers, and other third parties to which angkorauto.com may disclose personally identifiable information pursuant to this Statement.
		</li>
		<li>
			Guarantee or ensure any vehicle or any transaction between a buyer and seller
		</li>
		<li>
			Collect or process payment or transfer of title on behalf of buyers or sellers.
		</li>
		<li>
		   Advertisers on angkorauto.com may include information about special offers, incentives, or pricing programs associated with a specific brand, model, or vehicle ("Offers").
		</li>
		<li>
		   Angkorauto.com is not responsible for the content of any such Offers, nor responsible for any errors or omissions in Offer contents or descriptions. Users of the site should contact the relevant advertiser for full details on any such Offers, including eligibility requirements, limitations and restrictions, and availability.
		</li>
	</ul>
</div>