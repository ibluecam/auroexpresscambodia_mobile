<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = '_ebody';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.

$pg_details = $_CLASS->getPageHTML();

?>
<script type="text/javascript" src="<?php echo BASE_RELATIVE;?>js/blog/common.js"></script>
<script type="text/javascript">
$('#ebody').ready(function(){
    CKEDITOR.config.height = 600;
    CKEDITOR.config.toolbar = 'Cms';
    CKEDITOR.config.toolbar_Cms =
    [
        { name: 'document', items : [ 'Source','DocProps','Print','-','Templates' ] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	'/',
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
    ];
	

});
</script>

<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />

 <div id="sectionContenWrapper">
        	<!--sectionSidebar--> 
            <div id="topMenu">
            	<p><a href="#" class="linkfade">Community</a> > <a href="<?php echo BASE_RELATIVE;?>trade-info" class="linkfade">Trade Information</a> > Register</p>
            </div>
        	<?php include("php/sidebar/community.php");?>
            
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>Trade Information</p>
                </div>
                <div id="deal_register" class="clearfix">
                	
                	<div id="title"><p>Title</p></div>
                    <form>
                    <input type="text" class="text_box" />

                	<div id="radio_wrap" class="clearfix">
                    	<div id="all"><input type="radio" name="auto" /> All</div>
                        <div id="customs"><input type="radio" name="auto" /> Customs</div>
                        <div id="shipping"><input type="radio" name="auto" /> Shipping</div>
                        <div id="photo"><input type="radio" name="auto" /> Import</div>
                        <div id="photo"><input type="radio" name="auto" /> Export</div>
                    </div>
                    </form>
                    <div id="message">
                    	<p>Message</p>
                    </div>
                    <div id="text_editor">
                    	<textarea id="_ebody" name="htmlInput"></textarea>
                    </div>
                    <div id="attach" class="clearfix">                    	                     
                        <div class='file_upload' id='f1'><input name='file[]' type='file'/></div>
                        <div id='file_tools'>                        	
                            <img class="button" src='images/btnadd.gif' id='add_file' title='Add new input'/>
                            <img class="button" src='images/btndelete.gif' id='del_file' title='Delete'/>
                        </div>
                    </div>
                    <div id="button_wrap">
                    	<div id="upload">
                        	<input type="submit" class="upload" value="" />
                            <div id="back">
                            	<a href="#"><img src="<?php echo BASE_RELATIVE;?>images/community/back.png" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="pagination">
                	<p>< Prev     1     2     3     4     5     Next ></p>
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->       
  
  
  