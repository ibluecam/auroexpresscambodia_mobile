<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
//$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();

?>
<script type="text/javascript">
    $(document).ready(function($) {
        $('#seller').css('display','block');
        $('.tabs').click(function(){   
            var title = $(this).attr('title');                   
            $('.tabs').removeClass('current');
            $(this).addClass('current');
            $('.admin_tab').css('display','none');
            $('#'+title).css('display','block');
        });
    });
</script>



 <div id="sectionContenWrapper">              
    <div id="sectionContent"> 
        <div id="admin_tab" class="clearfix">
            <div class="tabs current" title="seller">
                <p>Seller</p>
            </div>
            <div class="tabs" title="buyer">
                <p>Buyer</p>
            </div>
            <div class="tabs" title="register">
                <p>Register</p>
            </div>
        </div>
        <!-- Seller Tab -->
        <div id="seller" class="clearfix admin_tab">
            <select class="search country">
                <option>Country</option>
            </select>
            <select class="search">
                <option>Member Type</option>
            </select>
            <select class="search">
                <option>Category</option>
            </select>
            <input type="text" placeholder="Company Name..." class="text_search">
            <input type="submit" value="" class="search_button linkfade">
            <div class="tab_content">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title">Name</td>
                        <td class="title">Company Name</td>
                        <td class="title">Email</td>
                        <td class="title">Member Type</td>
                        <td class="title">Products</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>

                </table>
            </div>
        </div>
        <!-- end Buyer Tab -->
        <!-- Seller Tab -->
        <div id="buyer" class="clearfix admin_tab">
            <select class="search country">
                <option>Country</option>
            </select>
            <select class="search">
                <option>Member Type</option>
            </select>
            <select class="search">
                <option>Category</option>
            </select>
            <input type="text" placeholder="Company Name..." class="text_search">
            <input type="submit" value="" class="search_button linkfade">
            <div class="tab_content">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title">Name</td>
                        <td class="title">Company Name</td>
                        <td class="title">Email</td>
                        <td class="title">Member Type</td>
                        <td class="title">Products</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- end Buyer Tab -->
        <!-- register Tab -->
        <div id="register" class="clearfix admin_tab">
            <select class="search country">
                <option>Country</option>
            </select>
            <select class="search">
                <option>Member Type</option>
            </select>
            <select class="search">
                <option>Category</option>
            </select>
            <input type="text" placeholder="Company Name..." class="text_search">
            <input type="submit" value="" class="search_button linkfade">
            <div class="tab_content">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title">Name</td>
                        <td class="title">Company Name</td>
                        <td class="title">Email</td>
                        <td class="title">Member Type</td>
                        <td class="title">Products</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>Company Name</td>
                        <td>Email</td>
                        <td>
                            <select class="member_type">
                                <option>Member Type</option>
                            </select>
                        </td>
                        <td class="product">Total : 100</td>
                    </tr>                    
                </table>
            </div>
        </div>
        <!-- end register Tab -->
    </div><!-- end div id="sectionContent"-->
</div><!-- end div id="sectionContentWraper" -->       
  
  
  