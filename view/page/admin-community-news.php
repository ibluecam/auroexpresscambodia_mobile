<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get news list.
    $news = $_CLASS->getNewsList();
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
            <div id="sectionContenWrapper">
                <div id="topMenu">
                    <p><a href="#" class="linkfade">Site Manager</a> > News</p>

                </div>
                <?php include("php/sidebar/community.php");?>
            </div>
            <div id="sectionContent">
                <!-- START CONTENT -->
                <div id="community_title">
                    <p>Manage News</p>
                    <a href="add-news" style="float: right; padding: 15px 0px 0px 25px;color:#3765DD;">Add News</a>
                </div>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display news list.
                    if( count($news) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['MANAGE_NEWS_NORESULT_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                        
                        for( $i=0; $i < count($news); $i++ ){
                            ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );
                ?>
                <div class="item-wrap">
                    <form method="post" style="margin:0;">
                        <div class="input-prepend" style="border-bottom:1px solid #ccc;">
                            <span class="add-on" style="width:145px;"><?php echo $news[$i]['date'] . ' ' . $news[$i]['time'];?></span>
                            <span class="add-on" style="width:450px;background:#FFF;"><?php echo $news[$i]['title'];?></span>
                            <a href="#" onclick="displayTab('tab<?php echo $i;?>','btn<?php echo $i;?>');return false;" class="btn" style="height:20px;"><i id="btn<?php echo $i;?>" class="icon-chevron-down"></i></a>
                            <input type="hidden" name="idInput" value="<?php echo $news[$i]['id'];?>"/>
                            <input type="submit" class="btn btn-danger" name="removebtn" onclick="return confirm('<?php echo $_LANG['MANAGE_NEWS_REMOVE_CONFIRM'];?>');" value="×" />
                        </div>
                        <div class="clearfix"></div>
                    </form>
                    <div id="tab<?php echo $i;?>" class="html-content">
                        <?php echo $news[$i]['html'];?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php
                        }
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}