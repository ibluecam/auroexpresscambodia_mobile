/*
 * Facebox (for jQuery)
 * version: 1.2 (05/05/2008)
 * @requires jQuery v1.2 or later
 *
 * Examples at http://famspam.com/facebox/
 *
 * Licensed under the MIT:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2007, 2008 Chris Wanstrath [ chris@ozmm.org ]
 *
 * Usage:
 *
 *  jQuery(document).ready(function() {
 *    jQuery('a[rel*=facebox]').facebox()
 *  })
 *
 *  <a href="#terms" rel="facebox">Terms</a>
 *    Loads the #terms div in the box
 *
 *  <a href="terms.html" rel="facebox">Terms</a>
 *    Loads the terms.html page in the box
 *
 *  <a href="terms.png" rel="facebox">Terms</a>
 *    Loads the terms.png image in the box
 *
 *
 *  You can also use it programmatically:
 *
 *    jQuery.facebox('some html')
 *    jQuery.facebox('some html', 'my-groovy-style')
 *
 *  The above will open a facebox with "some html" as the content.
 *
 *    jQuery.facebox(function($) {
 *      $.get('blah.html', function(data) { $.facebox(data) })
 *    })
 *
 *  The above will show a loading screen before the passed function is called,
 *  allowing for a better ajaxy experience.
 *
 *  The facebox function can also display an ajax page, an image, or the contents of a div:
 *
 *    jQuery.facebox({ ajax: 'remote.html' })
 *    jQuery.facebox({ ajax: 'remote.html' }, 'my-groovy-style')
 *    jQuery.facebox({ image: 'stairs.jpg' })
 *    jQuery.facebox({ image: 'stairs.jpg' }, 'my-groovy-style')
 *    jQuery.facebox({ div: '#box' })
 *    jQuery.facebox({ div: '#box' }, 'my-groovy-style')
 *
 *  Want to close the facebox?  Trigger the 'close.facebox' document event:
 *
 *    jQuery(document).trigger('close.facebox')
 *
 *  Facebox also has a bunch of other hooks:
 *
 *    loading.facebox
 *    beforeReveal.facebox
 *    reveal.facebox (aliased as 'afterReveal.facebox')
 *    init.facebox
 *    afterClose.facebox
 *
 *  Simply bind a function to any of these hooks:
 *
 *   $(document).bind('reveal.facebox', function() { ...stuff to do after the facebox and contents are revealed... })
 *
 */
(function($) {
  $.facebox = function(data, klass) {
    $.facebox.loading()

    if (data.ajax) fillFaceboxFromAjax(data.ajax, klass)
    else if (data.image) fillFaceboxFromImage(data.image, klass)
    else if (data.div) fillFaceboxFromHref(data.div, klass)
    else if ($.isFunction(data)) data.call($)
    else $.facebox.reveal(data, klass)
  }

  /*
   * Public, $.facebox methods
   */

  $.extend($.facebox, {
    settings: {
      opacity      : 0.6,
      overlay      : true,
      loadingImage : 'images/contact-seller/loading.gif',
      closeImage   : 'images/contact-seller/closelabel.png',
      imageTypes   : [ 'png', 'jpg', 'jpeg', 'gif' ],
      faceboxHtml  : '\
    <div id="facebox" style="display:none;"> \
      <div class="popup"> \
        <div class="content"> \
        </div> \
        <a href="#" class="close"><img src="images/contact-seller/closelabel.png" title="close" class="close_image" /></a> \
      </div> \
    </div>'
    },

    loading: function() {
      init()
      if ($('#facebox .loading').length == 1) return true
      showOverlay()

      $('#facebox .content').empty()
      $('#facebox .body').children().hide().end().
        append('<div class="loading"><img src="'+$.facebox.settings.loadingImage+'"/></div>')

      $('#facebox').css({
        top:    getPageScroll()[1] + (getPageHeight() / 10),
        left:   $(window).width() / 2 - 205
      }).show()

      $(document).bind('keydown.facebox', function(e) {
        if (e.keyCode == 27) $.facebox.close()
        return true
      })
      $(document).trigger('loading.facebox')
    },

    reveal: function(data, klass) {
      $(document).trigger('beforeReveal.facebox')
      if (klass) $('#facebox .content').addClass(klass)
      $('#facebox .content').append(data)
      $('#facebox .loading').remove()
      $('#facebox .body').children().fadeIn('normal')
      $('#facebox').css('left', $(window).width() / 2 - ($('#facebox .popup').width() / 2))
      $(document).trigger('reveal.facebox').trigger('afterReveal.facebox')
    },

    close: function() {
      $(document).trigger('close.facebox')
      return false
    }
  })

  /*
   * Public, $.fn methods
   */

  $.fn.facebox = function(settings) {
    if ($(this).length == 0) return

    init(settings)

    function clickHandler() {
      $.facebox.loading(true)

      // support for rel="facebox.inline_popup" syntax, to add a class
      // also supports deprecated "facebox[.inline_popup]" syntax
      var klass = this.rel.match(/facebox\[?\.(\w+)\]?/)
      if (klass) klass = klass[1]

      fillFaceboxFromHref(this.href, klass)
      return false
    }

    return this.bind('click.facebox', clickHandler)
  }

  /*
   * Private methods
   */

  // called one time to setup facebox on this page
  function init(settings) {
    if ($.facebox.settings.inited) return true
    else $.facebox.settings.inited = true

    $(document).trigger('init.facebox')
    makeCompatible()

    var imageTypes = $.facebox.settings.imageTypes.join('|')
    $.facebox.settings.imageTypesRegexp = new RegExp('\.(' + imageTypes + ')$', 'i')

    if (settings) $.extend($.facebox.settings, settings)
    $('body').append($.facebox.settings.faceboxHtml)

    var preload = [ new Image(), new Image() ]
    preload[0].src = $.facebox.settings.closeImage
    preload[1].src = $.facebox.settings.loadingImage

    $('#facebox').find('.b:first, .bl').each(function() {
      preload.push(new Image())
      preload.slice(-1).src = $(this).css('background-image').replace(/url\((.+)\)/, '$1')
    })

    $('#facebox .close').click($.facebox.close)
    $('#facebox .close_image').attr('src', $.facebox.settings.closeImage)
  }

  // getPageScroll() by quirksmode.com
  function getPageScroll() {
    var xScroll, yScroll;
    if (self.pageYOffset) {
      yScroll = self.pageYOffset;
      xScroll = self.pageXOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {     // Explorer 6 Strict
      yScroll = document.documentElement.scrollTop;
      xScroll = document.documentElement.scrollLeft;
    } else if (document.body) {// all other Explorers
      yScroll = document.body.scrollTop;
      xScroll = document.body.scrollLeft;
    }
    return new Array(xScroll,yScroll)
  }

  // Adapted from getPageSize() by quirksmode.com
  function getPageHeight() {
    var windowHeight
    if (self.innerHeight) { // all except Explorer
      windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
      windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
      windowHeight = document.body.clientHeight;
    }
    return windowHeight
  }

  // Backwards compatibility
  function makeCompatible() {
    var $s = $.facebox.settings

    $s.loadingImage = $s.loading_image || $s.loadingImage
    $s.closeImage = $s.close_image || $s.closeImage
    $s.imageTypes = $s.image_types || $s.imageTypes
    $s.faceboxHtml = $s.facebox_html || $s.faceboxHtml
  }

  // Figures out what you want to display and displays it
  // formats are:
  //     div: #id
  //   image: blah.extension
  //    ajax: anything else
  function fillFaceboxFromHref(href, klass) {
    // div
    if (href.match(/#/)) {
      var url    = window.location.href.split('#')[0]
      var target = href.replace(url,'')
      if (target == '#') return
      $.facebox.reveal($(target).html(), klass)

    // image
    } else if (href.match($.facebox.settings.imageTypesRegexp)) {
      fillFaceboxFromImage(href, klass)
    // ajax
    } else {
      fillFaceboxFromAjax(href, klass)
    }
  }

  function fillFaceboxFromImage(href, klass) {
    var image = new Image()
    image.onload = function() {
      $.facebox.reveal('<div class="image"><img src="' + image.src + '" /></div>', klass)
    }
    image.src = href
  }

  function fillFaceboxFromAjax(href, klass) {
    $.get(href, function(data) { $.facebox.reveal(data, klass) })
  }

  function skipOverlay() {
    return $.facebox.settings.overlay == false || $.facebox.settings.opacity === null
  }

  function showOverlay() {
    if (skipOverlay()) return

/*     if ($('#facebox_overlay').length == 0)
      $("body").append('<div id="facebox_overlay" class="facebox_hide"></div>')

    $('#facebox_overlay').hide().addClass("facebox_overlayBG")
      .css('opacity', $.facebox.settings.opacity)
      .click(function() { $(document).trigger('close.facebox') })
      .fadeIn(200) */
    return false
  }

  function hideOverlay() {
    if (skipOverlay()) return

/*     $('#facebox_overlay').fadeOut(200, function(){
      $("#facebox_overlay").removeClass("facebox_overlayBG")
      $("#facebox_overlay").addClass("facebox_hide")
      $("#facebox_overlay").remove()
    }) */

    return false
  }

  /*
   * Bindings
   */

  $(document).bind('close.facebox', function() {
    $(document).unbind('keydown.facebox')
    $('#facebox').fadeOut(function() {
      $('#facebox .content').removeClass().addClass('content')
      $('#facebox .loading').remove()
      $(document).trigger('afterClose.facebox')
    })
    hideOverlay()
  })

})(jQuery);


jQuery(document).ready(function($) {
    // POP Up
    $('a[rel*=facebox]').facebox({
        loadingImage : 'images/contact-seller/loading.gif',
        closeImage   : 'images/contact-seller/closelabel.png'
    });
    //Save to session for future submit
    $(document.body).on('click', ".facebook_login", function(e){
          var form_data=$("#form_message").serializeArray();

          $.ajax({
              type:"post",
              url: "ajax/push_tmp_product_inquiry_session.php",
              data: $.param(form_data),
              success: function(data){
                  //alert(data);
              //do stuff after the AJAX calls successfully completes


              }
          });







    });
    $("#action_submit").click(function(e){
      $("#form_message").submit();
    });
    // TAB Content
    $("body").on("click", ".tabs-menu a", function(event){
        var val = $(this).closest('li').attr('id');
        $('.tabs-menu').css({'background': 'url("images/contact-seller/'+val+'.png") no-repeat'});
        $('.tab-content').css({'display': 'none'});
        $('.'+val).css({'display': 'block'});
    });

    /* TAB
     * User name
     */
	 $("body").on("focus", ".user_name", function(event){
        if(this.value == "ID...") {
            this.value = "";

        }
        $(this).css({"color": "#000000", "font-size": "12px"});
    });
    $("body").on("blur", ".user_name", function(event){
        if(this.value == "") {
            this.value = "ID...";
            $(this).css({"color": "#adadad", "font-size": "10px"});
        }
    });
	
	 $("body").on("focus", ".password", function(event){
        if(this.value == "Password") {
            this.value = "";

        }
         $(this).css({"color": "#000000", "font-size": "12px"});
    });
    $("body").on("blur", ".password", function(event){
        if(this.value == "") {
            this.value = "Password";
            $(this).css({"color": "#adadad", "font-size": "10px"});
        }
    });
	 
	 
    $("body").on("focus", ".tab-username", function(event){
        if(this.value == "4-12 characters") {
            this.value = "";
            $(this).css({"color": "#000000", "font-size": "12px"});
        }
    });
    $("body").on("blur", ".tab-username", function(event){
        if(this.value == "") {
            this.value = "4-12 characters";
            $(this).css({"color": "#adadad", "font-size": "10px"});
        }
    });

    /* TAB
     * User name
     */
    $("body").on("focus", ".tab-mail", function(event){
        if(this.value == "Enter valid E-mail address") {
            this.value = "";
            $(this).css({"color": "#000000", "font-size": "12px"});
        }
    });
    $("body").on("blur", ".tab-mail", function(event){
        if(this.value == "") {
            this.value = "Enter valid E-mail address";
            $(this).css({"color": "#adadad", "font-size": "10px"});
        }
    });

    /* TAB
     * Password
     */
    $("body").on("focus", ".tab-pass", function(event){
        if(this.value == "Password") {
            this.value = "";
            $(this).css({"color": "#000000", "font-size": "12px"});
        }
    });
    $("body").on("blur", ".tab-pass", function(event){
        if(this.value == "") {
            this.value = "Password";
            $(this).css({"color": "#adadad", "font-size": "10px"});
        }
    });

    /* TAB
     * Confirm password
     */
    $("body").on("focus", ".tab-cpass", function(event){
        if(this.value == "Enter your password again") {
            this.value = "";
            $(this).css({"color": "#000000", "font-size": "12px"});
        }
    });
    $("body").on("blur", ".tab-cpass", function(event){
        if(this.value == "") {
            this.value = "Enter your password again";
            $(this).css({"color": "#adadad", "font-size": "10px"});
        }
    });
});

function checkEmail() {
  var message =$('#txtarea').val().length;
  var email = document.getElementById('txtemail');

  var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 	if(message<20){
		$('#msg1').fadeIn(function(){
				$('#msg1').text('Message box must contain over 20 charaters');
			$('#txtarea').click(function(){
					$("#msg1").fadeOut();
				});
			});
	}

  	if (!filter.test(email.value)){

  	  $("#msg").fadeIn(function(){
	  		$("#msg").text('Email does not valid');
	  $("#txtemail").click(function(){
		    $("#msg").fadeOut();
		   });
	   });
    email.focus;
  return false;
  }

}
// $(document).ready(function() {
// $(document.body).on('click', "#sign_inbtn", function(e){

//    var show = {};
//    var txtarea=document.getElementById('txtarea').value;
//     show['txtarea'] = txtarea;
//        $.ajax({
//         url     : 'ajax/get_country_category.php',
//         type    : 'POST',
//         data    :  show,
//         success : function(data){
//              // console.log(data);

//         },
//         error: function(){
//             console.log('error');
//         }
//     });

//     });
// });




