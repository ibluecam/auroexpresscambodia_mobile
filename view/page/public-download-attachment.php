<?php 

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$errorMessages=$_CLASS->getErrorMessages();


?>	


<div id="content-wrapper">
    <?php
        if(count($errorMessages)){
    ?>
    <div class="alert <?php echo $fstyle;?>">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <?php 
            foreach($errorMessages as $message){
                echo $message."<br/>";
            }
        ?>
    </div>
    <?php
        }
    ?>
</div>
