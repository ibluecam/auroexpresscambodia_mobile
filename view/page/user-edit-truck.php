<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */

    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    $invalidProduct = false;
    // get lists.
    $maker_unique_list = $_CLASS->getMakerList();
    $model_unique_list = $_CLASS->getModelList();
    $body_type_list    = $_CLASS->getBodyList();
	$drive_type_list   = $_CLASS->getDriveList();

	$product_option_list = $_CLASS->getProductOptionList();
    $country_list = $_CLASS->getCountryList();
	$vehicle_type_list = $_CLASS->getVehicleTypeList();
	$car_maker_list = $_CLASS->getCarMakerList();
	$category_list = $_CLASS->getCategoryList();
	$fuel_type_list = $_CLASS->getFuelTypeList();
	$transmission_list = $_CLASS->getTransmissionList();
    // set input size for maker.
    ( count($maker_unique_list) > 0 ? $imaker = 'style="width:249px;margin-right:-1px;"' : $imaker = '' );
    ( count($model_unique_list) > 0 ? $imodel = 'style="width:249px;margin-right:-1px;"' : $imodel = '' );
    ( count($body_type_list) > 0 ? $ibody = 'style="width:249px;margin-right:-1px;"' : $ibody = '' );
    if( CURRENCY_CODE == 'USD' ){
        $decimal = '.';
        $thousand= ',';
    }
    else {
        $decimal = ',';
        $thousand= '.';
    }
    if(!isset($_GET['cid'])) $getId='';
	else $getId=$_GET['cid'];
	$product=array();

    // get generated car id.
	if(empty($getId)){
    	$carid = $_CLASS->getCarId();
	}else{
		$carid = $getId;
		$product = $_CLASS->getAllProducts($carid);
        $invalidProduct=$_CLASS->invalidProduct;
	}
	(isset($product['payment_terms']) ? $payment_terms = explode('^',$product['payment_terms']) : $payment_terms=array());
	(isset($product['other_options']) ? $other_options = explode('^',$product['other_options']) : $other_options=array());
	(isset($product['safety_device_options']) ? $safety_device_options = explode('^',$product['safety_device_options']) : $safety_device_options=array());
	(isset($product['exterior_options']) ? $exterior_options = explode('^',$product['exterior_options']) : $exterior_options=array());
	(isset($product['interior_options']) ? $interior_options = explode('^',$product['interior_options']) : $interior_options=array());



?>
<script>

var category="<?php if(isset($product['category'])) echo $product['category']; ?>";
var subCategoryGroup="<?php if(isset($product['sub_category_group'])) echo $product['sub_category_group']; ?>";
var subCategory="<?php if(isset($product['sub_category'])) echo $product['sub_category']; ?>";
var cabinType="<?php if(isset($product['cabin_type'])) echo $product['cabin_type']; ?>";
var madeIn="<?php if(isset($product['made_in'])) echo $product['made_in']; ?>";
var condition="<?php if(isset($product['condition'])) echo $product['condition']; ?>";
var vehicleType="<?php if(isset($product['vehicle_type'])) echo $product['vehicle_type']; ?>";
var make="<?php if(isset($product['make'])) echo $product['make']; ?>";
var model="<?php if(isset($product['model'])) echo $product['model']; ?>";
var year="<?php if(isset($product['model_year'])) echo $product['model_year']; ?>";
var exterior_color="<?php if(isset($product['exterior_color'])) echo $product['exterior_color']; ?>";
var measureType="<?php if(isset($product['mileage_type'])) echo $product['mileage_type']; ?>";
var transmission="<?php if(isset($product['transmission'])) echo $product['transmission']; ?>";
var fuelType="<?php if(isset($product['fuel_type'])) echo $product['fuel_type']; ?>";
var currency="<?php if(isset($product['currency'])) echo $product['currency']; ?>";
var country="<?php if(isset($product['location'])) echo $product['location']; ?>";
var city="<?php if(isset($product['city'])) echo $product['city']; ?>";
var door="<?php if(isset($product['door'])) echo $product['door']; ?>";
var driveType="<?php if(isset($product['drive_type'])) echo $product['drive_type']; ?>";
var driveAvail="<?php if(isset($product['drive_availability'])) echo $product['drive_availability']; ?>";
var steering="<?php if(isset($product['steering'])) echo $product['steering']; ?>";
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
        <div id="content-wrapper">

        <!--For PC Version-->


            <div id="my_item">

                <div class="titleTop">
                        <img class="img-responsive" src="<?php echo BASE_RELATIVE; ?>img/user_add_new/img_01.png" />
                        <span>Add new </span>
                    </div>

                 <div id="menuTab" class="clearfix">
                        <ul>
                            <li><a href="<?php echo BASE_RELATIVE; ?>edit-car" ><p>Car </p></a></li>
                            <li><a href="<?php echo BASE_RELATIVE; ?>edit-bus"><p> Bus </p></a></li>
                            <li><a href="#"><p>Track</p></a></li>
                            <li><a href="<?php echo BASE_RELATIVE; ?>edit-motorbike"><p>Motorbike</p></a></li>

                        </ul>
                    </div><!-- #menuTab -->


                </div>



            <div id="inner-content">
                <!-- START CONTENT -->
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                ?>


                <form id="form-wrap" method="post">
                    <!----------------- FORM CONTAINER -------------------->
                    <?php if(!$invalidProduct){?>
                    <input type="hidden" name="caridInput" value="<?php echo $carid;?>" />
                    <div id="content1">
                        <table class="edit_product_table">
                            <tr>
                                <td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_CONDITION_LABEL'];?> <font color="#C1272D">*</font></div>
                                    <label><input type="radio" name="conditionInput" value="Used"/><?php echo $_LANG['NEWCAR_CONDITION_USED'];?></label>
                                    <label><input type="radio" checked id="conditionInput" name="conditionInput" value="New"/><?php echo $_LANG['NEWCAR_CONDITION_NEW'];?></label>

                                </td>
                                </td><td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_STEERING_LABEL'];?></div>
                                    <label><input type="radio" checked name="steeringInput" value="LHD"/><?php echo $_LANG['NEWCAR_STEERING_LHD'];?></label>
                                    <label><input type="radio" name="steeringInput" value="RHD"/><?php echo $_LANG['NEWCAR_STEERING_RHD'];?></label>

                                </td>

                            </tr>
                            <tr>
                                <td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_CATEGORY_LABEL'];?> <font color="#C1272D">*</font></div>
                                    <select name="categorySelect" required id="categorySelect" style="">
                                        <option value="">- Select -</option>
                                    <?php
                                        if( count($category_list['Truck']) > 0 ){

                                            for( $i=0; $i < count($category_list['Truck']); $i++ ){
                                                if( !empty($category_list['Truck'][$i]) ){
                                        ?>
                                        <option value="<?php echo $category_list['Truck'][$i];?>"><?php echo $category_list['Truck'][$i];?></option>
                                    <?php
                                                }
                                            }
                                        }
                                        else {
                                    ?>
                                        <option value="">Other</option>
                                    <?php
                                        }
                                    ?>
                                    </select>
                                </td>
                                <td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_TRANSMISSION_LABEL'];?></div>
                                    <select id="transmissionInputSelect" name="transmissionInputSelect" style="">
                                        <option value="">- Select -</option>
                                        <?php
                                            if( count($transmission_list['transmission_name']) > 0 ){

                                                for( $i=0; $i < count($transmission_list['transmission_name']); $i++ ){
                                                    if( !empty($transmission_list['transmission_name'][$i]) ){
                                            ?>
                                            <option value="<?php echo $transmission_list['transmission_name'][$i];?>"><?php echo $transmission_list['transmission_name'][$i];?></option>
                                        <?php
                                                    }
                                                }
                                            }
                                            else {
                                        ?>
                                            <option value="0"></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>

                            </tr>
                            <tr>
                                <td class="edit_haft_row">

                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_CATEGORY_LABEL'];?> 2</div>
                                    <select name="subCategoryGroupSelect" id="subCategoryGroupSelect">

                                        <option value="">- Select -</option>

                                    </select>
                                    </td>
                                    </td><td class="edit_haft_row">
                                    <div class="add-on title-label">NO OF WHEEL BOLTS</div>
                                	<select name="numberPassengerInput" id="carFuelInputSelect">
                                    	<option value="">-Select-</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>8</option>
                                        <option>10</option>
                                        <option>12</option>
                                        <option>14</option>
                                    </select>
                                </td>
                            </tr><tr>
                                <td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_MAKER_LABEL'];?> <font color="#C1272D">*</font></div>
                                    <select required id="makerInputSelect" name="makerInputSelect" style="">
                                        <?php
                                            $_GET['product_type']="Truck";
                                            include('ajax/get_product_make_options.php');
                                        ?>
                                    </select>
                                    <!-- <div class="add-on title-label">&nbsp;</div> -->
                                    <input placeholder="Other Make" type="text" maxlength="30" id="makerInput" name="makerInput" class="content-label other_input"/>

                                </td>
                                <td class="edit_haft_row">
                                    <div style="font-size: 11px;" class="add-on title-label">Loading Capacity(TONS)</div>
                                    <input type="text" value="<?php if(isset($product['loading_weight'])) echo $product['loading_weight'];?>" name="loadingWeightInput" class="content-label" placeholder="3 tons" />
                                </td>
                            </tr><tr>
                                <td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_MODEL_LABEL'];?> <font color="#C1272D">*</font></div>
                                    <select required id="modelInputSelect" name="modelInputSelect" style="">
                                        <option value="">- Select -</option>
                                    </select>
                                    <!-- <div class="add-on title-label">&nbsp;</div> -->
                                    <input type="text" maxlength="50" id="modelInput" name="modelInput" class="content-label other_input" placeholder="Other Model" />

                                </td>
                                 <td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_FUEL_LABEL'];?></div>
                                    <select id="carFuelInputSelect" name="carFuelInputSelect" style="">

                                        <option value="">- Select -</option>
                                        <?php
                                            if( count($fuel_type_list['fuel_name']) > 0 ){

                                                for( $i=0; $i < count($fuel_type_list['fuel_name']); $i++ ){
                                                    if( !empty($fuel_type_list['fuel_name'][$i]) ){
                                            ?>
                                            <option value="<?php echo $fuel_type_list['fuel_name'][$i];?>"><?php echo $fuel_type_list['fuel_name'][$i];?></option>
                                        <?php
                                                    }
                                                }
                                            }
                                            else {
                                        ?>
                                            <option value="0"></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>

                        </tr>
                        <tr>

                                <td class="edit_haft_row">

                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_YEAR_LABEL'];?></div>
                                    <select id="yearInput" name="yearInput" style="">
                                        <option value="">- Select -</option>
                                        <?php
                                        for( $i=date('Y'); $i > 1900; $i-- ){
                                        ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        </td>
                         <td class="edit_haft_row">
                                    <div class="add-on title-label">Color </div>
                                    <select id="bodyColorInput" name="bodyColorInput">
                                        <option value="">- Select -</option>
                                        <option value="White">White</option>
                                        <option value="Black">Black</option>
                                        <option value="Silver">Silver</option>
                                        <option value="Gold">Gold</option>
                                        <option value="Brown">Brown</option>
                                        <option value="Red">Red</option>
                                        <option value="Orange">Orange</option>
                                        <option value="Blue">Blue</option>
                                        <option value="Yellow">Yellow</option>
                                        <option value="Green">Green</option>
                                        <option value="Other">Other</option>
                                    </select>

                                </td>

                        </tr>
                         <tr>
                          </td>

                                <td class="edit_haft_row">
                                    <div class="add-on title-label">Chassis N<font style="text-transform: lowercase;">o</font> / VIN</div>
                                    <input type="text" value="<?php if(isset($product['chassis_no'])) echo $product['chassis_no'];?>" maxlength="50" name="chassisNoInput" class="content-label" autocomplete="off" placeholder="<?php echo $_LANG['NEWCAR_CHASSIS_NO_PLACEHOLDER'];?>" />
                                </td>
                                <td  class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_CITY_LABEL'];?></div>
                                    <select id="citySelect" name="citySelect" style="">
                                        <?php
                                            $_GET['country']=$_CLASS->getUserCountry();
                                            include('ajax/load_saved_city.php');
                                        ?>
                                    </select><br/>
                                    <input type="text" maxlength="50" id="cityInput" name="cityInput" class="content-label other_input" placeholder="" />
                                </td>

                          </tr>

                        <tr>
                                <td class="edit_haft_row">
                                    <div class="add-on title-label">Cylinder Capacity</div>
                                    <input class="cc" type="text" id="engineVolumeInput" value="<?php if(isset($product['engine_volume'])) echo $product['engine_volume'];?>" name="engineVolumeInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_ENGINE_VOLUME_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)" /> (CC)
                                </td>
                                <td class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_PRICE_LABEL'];?></div>
                                    <input type="text" value="<?php if(isset($product['price'])) echo $product['price'];?>" id="priceInput" name="priceInput" class="content-label" placeholder="<?php echo $_LANG['NEWCAR_PRICE_PLACEHOLDER'];?>" onkeypress="return isNumberKey(event)"/>
                                    <select name="currencySelect" id="currencySelect">
                                        <?php include("ajax/load_currency_list.php");?>
                                    </select>
                                </td>

                          </tr>
                          <tr>
                               <td style="border-right:none;padding-left:10px" colspan="2" class="edit_haft_row">
                                    <div class="add-on title-label"><?php echo $_LANG['NEWCAR_MILES_LABEL'];?> <font color="#C1272D">*</font></div>
                                    <input required type="text" id="milesInput" value="<?php if(isset($product['mileage'])) echo $product['mileage'];?>" placeholder="<?php echo $_LANG['NEWCAR_MILES_PLACEHOLDER'];?>" name="milesInput" style="margin-right:-1px;" class="content-label" onkeypress="return isNumberKey(event)" />
                                    <label><input type="radio" checked value="Km" name="measureTypeInput">Km</label>
                                    <label><input type="radio" value="Mile" name="measureTypeInput">Mile</label>
                                </td>
                          </tr>
                            <tr>
                                <td style="border-right:none;padding-left:10px" colspan="2" class="edit_haft_row">
                                    <div class="add-on block-label"><b>Description</b></div>
                                    <textarea id="sellerCommentText" name="sellerCommentText" class="tinymce content-label"><?php if(isset($product['seller_comment'])) echo $product['seller_comment'];?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <div id="video-wrap" style="display:none;" class="input-prepend input-append inputap">
                                        <div class="add-on title-label"><?php echo $_LANG['NEWCAR_YOUTUBE_URL_LABEL'];?></div>
                                        <input id="videoUrlInput" type="text" class="content-label" style="width:435px;" placeholder="<?php echo $_LANG['NEWCAR_YOUTUBE_URL_PLACEHOLDER'];?>" />
                                        <input id="vidBtn" onclick="saveVideoHandler('<?php echo $carid;?>');return false;" type="submit" class="btn btn-info" style="width:100px;" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />
                                    </div>
                                    <div id="upload-wrap" style="display:none;float:left;width:100%;height:75px" >
                                        <h4 style="padding:10px;"><?php echo $_LANG['NEWCAR_UPLOAD_LABEL'];?></h4>
                                        <iframe id="iupload" width="90%" style="border:0;float:left"></iframe>
                                        <input style="float:left;" id="submitbtn" type="submit" name="savebtn" class="btn btn-small btn-info" value="<?php echo $_LANG['NEWCAR_SAVE_BUTTON'];?>" />

                                    </div>

                                </td>
                            </tr>

                        </table>
                    </div>
                    <br>
                    <div id="outputwrap"></div>
                    <div id="outoutimg" class="clearfix"></div>
                    <div class="clearfix"></div>
                    <br>
                    <a id="prevbtn" href="#" onclick="return previousFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <i class="icon-circle-arrow-left" style="margin-right:10px;"></i><?php echo $_LANG['NEWCAR_PREVIOUS_BUTTON_LABEL'];?>
                    </a>
                    <a id="nextbtn" href="#" onclick="return nextFormHandler()" class="btn btn-small" style="display:none;margin-right:10px;">
                        <?php echo $_LANG['NEWCAR_NEXT_BUTTON_LABEL'];?><i class="icon-circle-arrow-right" style="margin-left:10px;"></i>
                    </a>
                          <!-------------- END UPLOAD MEDIA BLOCK ---------->
                    <!----------------- END FORM CONTAINER -------------------->
                    <?php
                    }
                    else{
                        echo "<font color='#C1272D'>Product with this ID is does not exist!</font>";
                    }


                    ?>

                </form>
                <!-- END CONTENT -->

                    </div>


                    <div class="clearfix"></div>
                    <div id="content2">

            </div>
            <div class="clearfix"></div>
        </div>

		<script language="javascript" type="text/javascript">
            /* preview uploaded image */
            $(document).ready(function(e){
                displayController('<?php echo BASE_RELATIVE;?>view/3rdparty/file_uploader/user-addpic.php?cid=<?php echo $carid;?>');
            });

            function basename(path) {
               return path.split('/').reverse()[0];
            }

			function save_data_form(){

				$('#submitbtn').trigger('click');

			}
		    function check_vehicle_data(){
				 var data_return=false;
				 if($('#partname').val()=="" || $('#countryInputSelect').val()=="" || $('#categorySelect').val()==""){
					data_return= false;
				 }
				 else{
					 data_return= true;
				 }
				 return data_return;


			}

			 function carImageUploadedNew_Photo(id,proid,url,mode,fname,i){

                        var did = 'img' + Math.round((Math.random()*1000));

                        var rd='rd' + id;

                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

                        if(i==0){

							 html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
                        }
                        else{
							 html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
                        }
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';

                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;

                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }

            function carImageUploaded(id,proid,url,mode,fname,primary_photo){
                        var did = 'img' + Math.round((Math.random()*1000));
                        var rd='rd' + id;
                        var wrap = document.getElementById('outoutimg');
                        var html = '<div id="'+did+'" class="img-wrap">';
                        html += '<img src="'+url+'" width="200" height="150" style="margin:5px;" />';
                        html += '<p style="margin:0;padding-left:5px;">';

                        if(primary_photo==1){

                             html +='<input type="radio"  onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  checked="checked"  />Set as Primary Photo';
                        }
                        else{
                             html +='<input type="radio" onclick="checkimg(\''+id+'\',\''+proid+'\',\''+rd+'\');return true;" id='+rd+' name="primary_photo[]" value="'+fname+'"  />Set as Primary Photo';
                        }
                        html += '<a href="#" onclick="removeImageHandler(\''+id+'\',\''+basename(url)+'\',\''+did+'\',\''+rd+'\');return false;" class="btn btn-small btn-danger" style="float:right;margin-right:5px;">×</a>';
                        html += '</p>';
                        html += '</div>';

                        wrap.innerHTML += html;
                    }
                    function ucfirst (str) {
                        str += '';
                        var f = str.charAt(0).toUpperCase();
                        return f + str.substr(1);
                   }
        </script>
