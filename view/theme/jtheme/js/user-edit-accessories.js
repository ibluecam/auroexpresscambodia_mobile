/* handle next form (btn click) */
var cur_step = 1;
var cur_img;
var tab;
var next_btn;
var prev_btn;
var img_st1;
var currentMakeClick="";
var fittingModel = new Array();
var fittingModelBlock="";
Array.prototype.remove = function() {
					var what, a = arguments, L = a.length, ax;
					while (L && this.length) {
						what = a[--L];
						while ((ax = this.indexOf(what)) !== -1) {
							this.splice(ax, 1);
						}
					}
					return this;
				};
$(document).ready(function(){
	
    //General
    tab = document.getElementById('content1');
    opt1 = document.getElementById('opt1');
    opt2 = document.getElementById('opt2');
    vid  = document.getElementById('video-wrap');
    upl  = document.getElementById('upload-wrap');

    cur_img = img_st1;

	//Description field
	tinymce.init({
        selector: "textarea.tinymce",
        menubar:false,
        statusbar : false,
        width:665,
        height:250,
        plugins: [
            "advlist autolink lists link anchor",
            "searchreplace visualblocks",
            "insertdatetime table contextmenu paste moxiemanager"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | table"
    });
	
	//-----------ckeditor for mobile-------------
		tinymce.init({
        selector: "textarea.tinymce_mobile_accessery",
        menubar:false,
        statusbar : false,
        width:96+'%',
        height:250,
        plugins: [
            "advlist autolink lists link anchor",
            "searchreplace visualblocks",
            "insertdatetime table contextmenu paste moxiemanager"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | table"
    });
	
	//------------end ckeditor----------------

	//City field
	if(city!=''){
        $("#citySelect").val(city);
    }  
    if(currency!='') $("#currencySelect").val(currency);
    $("#citySelect").change(function(e){
        var currentCountry=$(this).val();
        if(currentCountry=="Other"){
            $("#cityInput").css('display', 'inline');
        }else{
            $("#cityInput").val('');
            $("#cityInput").css('display', 'none');
        }
    });
	
	//Make and model field
	$( "#makerInputSelect" ).change(function(e) {
        if($(this).val()=='Other')
            $("#makerInput").css('display','inline');
        else{
            $("#makerInput").css('display','none');
            $("#makerInput").val('');
        }
        var mid=$(this).val();

        $("#modelInputSelect").load('ajax/get_product_model_options.php?product_type=Acessories&mid='+encodeURIComponent(mid), function() {
            if($(this).val()=='Other')
                $("#modelInput").css('display','inline');
            else{
                $("#modelInput").css('display','none');
                $("#modelInput").val('');
            }
        });
    });
    $("#modelInputSelect" ).change(function(e) {
        if($(this).val()=='Other')
            $("#modelInput").css('display','inline');
        else{
            $("#modelInput").css('display','none');
            $("#modelInput").val('');
        }
    });

	//Product Name
	$("#partNameSelect" ).change(function(e) {
        if($(this).val()=='Other')
            $("#partNameInput").css('display','inline');
        else{
            $("#partNameInput").css('display','none');
            $("#partNameInput").val('');
        }
    });

    //General
	loadAllDropdown();

});

function loadAllDropdown(){
	//General
	var ptype='Acessories';
	
	//Made in field
	$("#madeInInputSelect").val(madeIn);
	if(condition!='') $("[name=conditionInput]").val([condition]);
	
	//Make and Model
	var foundInMakerSelect=false;
	if(make!=''){
        $("#makerInputSelect option").each(function() {
            if($(this).text() == make) {
                foundInMakerSelect = true;
                $(this).attr('selected', 'selected');
            }
        });
        if(foundInMakerSelect==false){
            $("#makerInputSelect").val("Other");
            $("#makerInput").css('display','inline');
            $("#makerInput").val(make);
        }
    }

	var mid=$("#makerInputSelect").val();
    if(mid!=''){

    	$("#modelInputSelect").load('ajax/get_product_model_options.php?product_type=Acessories&mid='+encodeURIComponent(mid), function() {
    		var foundInModelSelect=true;

    		$("#modelInputSelect").val(model);

            if($("#modelInputSelect").val()=='' || $("#modelInputSelect").val()== null){
                foundInModelSelect=false;
            }
            if(foundInModelSelect==false){
                $("#modelInputSelect").val('Other');
                $("#modelInput").css('display','inline');
                $("#modelInput").val(model);
            }
    	});
    }

    //Product Name field
    var foundInProductNameSelect=false;
    if(part_name!=''){
    	$("#partNameSelect").load('ajax/get_product_name_options.php?product_type=Acessories', function() {
    		var foundInProductNameSelect=true;

    		$("#partNameSelect").val(part_name);

            if($("#partNameSelect").val()=='' || $("#partNameSelect").val()== null){
                foundInProductNameSelect=false;
            }
            if(foundInProductNameSelect==false){
                $("#partNameSelect").val('Other');
                $("#partNameInput").css('display','inline');
                $("#partNameInput").val(part_name);
            }
    	});
    }

    //Year field
	$("#yearInput").val(year);

}


/* redefine wysiwyg */
/* handle step4 selectors */
var opt1;
var opt2;
var vid;
var upl;
function validateImageType(){
    if( opt1 == undefined || opt2 == undefined || vid == undefined || upl == undefined ){
        opt1 = document.getElementById('opt1');
        opt2 = document.getElementById('opt2');
        vid  = document.getElementById('video-wrap');
        upl  = document.getElementById('upload-wrap');
    }

    var val = opt1.options[opt1.selectedIndex].value;

    // disable and hide all controllers.
    if( val == 'none' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }

    // display video controller.
    else if( val == 'video' ){
        opt2.disabled = true;
        opt2.selectedIndex = 0;
        vid.style.display = 'inline-block';
        upl.style.display = 'none';
    }

    // display option 2 but hide controllers.
    else {
        opt2.disabled = false;
        vid.style.display = 'none';
        upl.style.display = 'none';
    }
}

function displayController(url){
    var val = "interior";

        vid.style.display = 'none';
        upl.style.display = 'inline-block';

    document.getElementById('iupload').src = url + '&mode=' + val;

}

/* handle save video url form */
function saveVideoHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }

    var value = document.getElementById('videoUrlInput').value;

    if( value == undefined || value.length < 1 ){
        return false;
    }

    // disable save button.
    document.getElementById('vidBtn').disabled = true;

    // send ajax request.
    $.post(
        'ajax/save_video_url.php',
        {
            carid: cid,
            url: value
        },
        function(data,status){
            var result = data.split(',');

            document.getElementById('videoUrlInput').value = ''; // enpty video url input field.
            document.getElementById('vidBtn').disabled = false;   // enable button.

            if( result[0] == 'ir' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var itemid = 'link' + Math.floor((Math.random()*1000)+1);
                div.innerHTML += '<p id="'+itemid+'"><b>Video URL:</b> ' + result[1] + '<a href="#" onclick="removeVideo(\''+result[1]+'\',\''+cid+'\',\''+itemid+'\'); return false;" class="btn btn-small btn-danger" style="float:right;">×</a></p>';
                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}

/* handle upload image form */
function uploadImageHandler(cid){
    // car id must be defined.
    if( cid == undefined || cid.length < 5 ){
        return false;
    }


}

/* remove video link */
function removeVideo(url,carid,itemid){
    if( url == undefined || carid == undefined || itemid == undefined ){
        return false;
    }

    $.post(
        'ajax/remove_vid_url.php',
        {
            cid: carid,
            dest: url
        },
        function(data,status){
            var result = data.split(',');

            if( result[0] == 'ng' ){
                alert(result[1]);
                return;
            }
            else if( result[0] == 'ok' ){
                // video url add successfully. display list.
                var div = document.getElementById('outputwrap');
                var p = document.getElementById(itemid);

                if(  p != undefined ){
                    div.removeChild(p);
                }

                return;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}


/* handle remove image */
function removeImageHandler(id,url,divid,rd){
    // validate.
    if( url == undefined || url == null ){
        console.log('URL is undefined.');
        return false;
    }
	if( id == undefined || id == null ){
        console.log('URL is undefined.');
        return false;
    }

    var d = document.getElementById(divid);


    if( d == undefined || d == null ){
        console.log('Image divider is undefined.');
        return false;
    }

    var r = document.getElementById(rd);

    if( r == undefined || r == null ){
        console.log('Image divider is undefined.');
        return false;
    }
    if(r.checked){
        alert('Can not Remove Primary Photo');
		return false;
	}
    // send ajax request.
    $.post(
        'ajax/remove_car_image.php',
        {

            loc: url,
			imgid: id

        },
        function(data,status){
		 console.log(data);
            var result = data.split(',');

            if( result[0] == 'ng' ){
                alert('Unable to remove image due an internal error.');
                console.log(result[0] + ':' + result[1]);
                return false;
            }
            else if( result[0] == 'ok' ){
                // remove image.
                var wrap = document.getElementById('outoutimg');
                wrap.removeChild(d);
                return false;
            }
            else {
                // unknown error.
                alert('Unknown error: ' + result[0] + ':' + result[1]);
                return;
            }
        }
    );
}
/* handle remove image */
function checkimg(id,proid,rd){
       // validate.


    if( id == undefined || id == null ){
        console.log('URL is undefined.');
        return false;
    }
    if( proid == undefined || proid == null ){
        console.log('URL is undefined.');
        return false;
    }
    $('#rd'+id).removeAttr('checked');
     $('#rd'+id).attr('checked',true);
      var r = document.getElementById(rd);
        r.checked = true;
    // send ajax request.
    $.post(
        'ajax/setprimaryimage.php',
        {

            imgid: id,
            pro:proid

        },
        function(data,status){
         console.log(data);
        }
    );
}

