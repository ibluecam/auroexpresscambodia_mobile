<?php 
if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################

class publicregister
{
	
	
    protected $register_form_status;    // tracks registration process (complete/error)
    protected $_LANG;
    protected $country_list;
	protected $lang;
    protected $error_status = false;
    protected $error_message;
    protected $error_type;
    protected $error_formname;
    protected $cdata; // holds the tid and ref passed to login page if accessed through
                      // the edit_ds page.
	
    var $redirect_url;
	var $invalid_log;
	
	var $store_temporary_userid;
	var $store_temporary_company_name;
	var $store_temporary_Email;
	var $store_temporary_Country;
	var $store_temporary_Password;
	var $store_temporary_Phone;
	var $store_temporary_Confirm_password;
	var $store_temporary_security_number;
	var $store_temporary_member_type;
	var $store_temporary_agree;
	var $store_temporary_firstname;
	var $store_temporary_lasttname;
	var $store_temporary_file_image;
	var $not_choose_image;
	var $store_tempory_address;
	var $store_temporary_contact_name;
	var $store_tempory_company_description;
	var $store_tempory_web;
	var $store_tempory_fax;
	var $address_null;
	var $secu_message_null;
	var $store_temporary_gender;
	var $store_temporary_province;
	var $secu_message_invalid;
	var $membership_type;
	var $invalid_security;
	
	
    protected $existedSocialAccount=false;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicregister($lang=array())
    {
		// define user id.
        require_once BASE_CLASS . 'class-utilities.php';
        $this->user_id = Utilities::generateRegisterID(6);
		$this->user_id;		
		
        // if group session is set, take user to the home page.
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] != 'public' )
        {
            header("Location: " . BASE_RELATIVE . DEFAULT_HOME_SLUG);
            exit;
        }
		
		
		 // if group session is set, take user to the home page. no double login!
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] != 'public'  && $_SESSION['log_group'] != 'admin' )
        {
            header("Location: " . BASE_RELATIVE);
            exit;
        }
        if( isset($_SESSION['log_group']) && $_SESSION['log_group'] == 'admin' )
        {
            header("Location: " . BASE_RELATIVE);
            exit;
        }
		
        
		
        //$this->loadCountryList();
		
        // initialize status.
        $this->register_form_status = '';

        // set language vars.
        $this->_LANG = $lang;

        // if register form Personal is submit, process it.
        if( isset($_POST['signup']) )
        {
			require_once BASE_CLASS .'math-security.php';
			$math = new BasicMathSecurity( 'math' );
			$secu_value=$_POST['reg_name'];
			//$select_country=$_POST['countries'];
			
			if(isset($_POST['term'])){
				
				//$this->store_temporary_id=$_POST['log_id'];
				//$this->store_temporary_company_name=$_POST['company_name'];
				$this->store_temporary_Email=$_POST['email'];
				//$this->store_temporary_Country=$_POST['countries'];
				$this->store_temporary_Password=$_POST['password'];
				$this->store_temporary_Phone=$_POST['mobile'];
				$this->store_temporary_firstname=$_POST['first_name'];
				$this->store_temporary_lasttname=$_POST['last_name'];
				$this->store_temporary_Confirm_password=$_POST['confirm_password'];
				$this->store_temporary_security_number=$_POST['reg_name'];
				$this->store_temporary_gender=$_POST['gender'];
				$this->store_temporary_province=$_POST['province_city'];
			    //$this->store_temporary_member_type=$_POST['member_type'];
			    $this->store_temporary_agree="checked";//checked for accept.
			
				
			}
			else{
				//$this->store_temporary_id=$_POST['log_id'];
				//$this->store_temporary_company_name=$_POST['company_name'];
				$this->store_temporary_Email=$_POST['email'];
				//$this->store_temporary_Country=$_POST['countries'];
				$this->store_temporary_Password=$_POST['password'];
				$this->store_temporary_Phone=$_POST['mobile'];
				$this->store_temporary_firstname=$_POST['first_name'];
				$this->store_temporary_lasttname=$_POST['last_name'];
				$this->store_temporary_Confirm_password=$_POST['confirm_password'];
				$this->store_temporary_security_number=$_POST['reg_name'];
				$this->store_temporary_gender=$_POST['gender'];
				$this->store_temporary_province=$_POST['province_city'];
			    //$this->store_temporary_member_type=$_POST['member_type'];
			    $this->store_temporary_agree="notcheck";	
				
			}
			
			
			
			if(isset($_SESSION['remember_sec_number'])){
			  $total=$_SESSION['remember_sec_number'];
			}
			
			
			if($secu_value==""){
			   $get_result="Invalid";
			   $this->secu_message_null="null";
			   
			   
			}
			if($secu_value==$total){
			   $get_result="Valid";
			}
			
			if($secu_value!=$total){
				$get_result="Invalid";
			   $this->secu_message_invalid="Invalid";
			}
			
			//////////////////////
			// I agree with
			
			if($this->store_temporary_agree=="checked"){
			    $agree='checked';
				 $this->store_temporary_agree="checked";
			}
			else{
				$agree='notchecked';
				$this->store_temporary_agree="notcheck";
			}
			
			
			if($get_result=="Valid" and $agree=='checked'){
				//$this->processRegisterForm($secu_value);
				$this->processRegisterForm_personal($secu_value);
				
			}
	}
		
		
		//Register for seller Process.
		
		if( isset($_POST['signup-seller']) )
        {
			require_once BASE_CLASS .'math-security.php';
			$math = new BasicMathSecurity( 'math' );
			$secu_value=$_POST['kreusna_reg_name'];
			//$select_country=$_POST['countries'];
			
			if(isset($_POST['term'])){
				
				$this->store_temporary_userid=$_POST['user_id'];
				$this->store_temporary_company_name=$_POST['company_name'];
				$this->store_temporary_Email=$_POST['email'];
				//$this->store_temporary_Country=$_POST['countries'];
				$this->store_temporary_Password=$_POST['password'];
				$this->store_temporary_Phone=$_POST['mobile'];
				$this->store_temporary_firstname=$_POST['first_name'];
				$this->store_temporary_lasttname=$_POST['last_name'];
				$this->store_temporary_Confirm_password=$_POST['confirm_password'];
				$this->store_temporary_security_number=$_POST['reg_name'];
				$this->store_temporary_gender=$_POST['gender'];
				$this->store_temporary_province=$_POST['province_city'];
				//$this->membership_type=$_POST['membership_type'];
			    //$this->store_temporary_member_type=$_POST['member_type'];
			    $this->store_temporary_agree="checked";//checked for accept.
				
				$this->store_tempory_address=$_POST['address'];
				
			
				
			}
			else{
				$this->store_temporary_userid=$_POST['user_id'];
				$this->store_temporary_company_name=$_POST['company_name'];
				$this->store_temporary_Email=$_POST['email'];
				//$this->store_temporary_Country=$_POST['countries'];
				$this->store_temporary_Password=$_POST['password'];
				$this->store_temporary_Phone=$_POST['mobile'];
				$this->store_temporary_firstname=$_POST['first_name'];
				$this->store_temporary_lasttname=$_POST['last_name'];
				$this->store_temporary_Confirm_password=$_POST['confirm_password'];
				$this->store_temporary_security_number=$_POST['reg_name'];
				$this->store_temporary_gender=$_POST['gender'];
				$this->store_temporary_province=$_POST['province_city'];
				$this->membership_type=$_POST['membership_type'];
			    //$this->store_temporary_member_type=$_POST['member_type'];
			    $this->store_temporary_agree="notcheck";	
				
				$this->store_tempory_address=$_POST['address'];
				
				
			}
			
			
			
			if(isset($_SESSION['remember_sec_number'])){
			  $total=$_SESSION['remember_sec_number'];
			}
			
			
			if($secu_value==""){
			   $get_result="Invalid";
			   $this->store_tempory_address=$_POST['address'];
			   $_SESSION['alert_security']=1;
			    $this->secu_message_null="null";
			   
			}
			if($secu_value==$total){
			   $get_result="Valid";
			}
			
			if($secu_value!=$total){
				$this->store_tempory_address=$_POST['address'];
				$get_result="Invalid";
				$_SESSION['alert_security']=1;
				$this->invalid_security="Invalid Value";
				$this->secu_message_invalid="Invalid";
			}
		/*	
			if($_FILES['file']['name']==""){
			 $filename="Invalid";
			}
			else{
			  $filename="Valid";
			}
			if($_POST['address']==""){
			 $address="Null";
			 $this->address_null="Please input address";
			}
			else{
			 $address="has";
			}
		*/	
			//////////////////////
			// I agree with
			
			if($this->store_temporary_agree=="checked"){
			    $agree='checked';
				 $this->store_temporary_agree="checked";
			}
			else{
				$agree='notchecked';
				$this->store_temporary_agree="notcheck";
			}
			
			
			if($get_result=="Valid" and $agree=='checked'){
				//$this->processRegisterForm($secu_value);
				$this->processRegisterForm_seller($secu_value);
				
			}
			
			
			
	}
	
	
	//Register for dealer Process.
		
		if( isset($_POST['signup-dealer']) )
        {
			require_once BASE_CLASS .'math-security.php';
			$math = new BasicMathSecurity( 'math' );
			$secu_value=$_POST['reg_name'];
			//$select_country=$_POST['countries'];
			
			if(isset($_POST['term'])){
				
				//$this->store_temporary_id=$_POST['log_id'];
				$this->store_temporary_company_name=$_POST['company_name'];
				$this->store_temporary_contact_name=$_POST['contact_name'];
				$this->store_temporary_Email=$_POST['email'];
				//$this->store_temporary_Country=$_POST['countries'];
				$this->store_temporary_Password=$_POST['password'];
				$this->store_temporary_Phone=$_POST['mobile'];
				//$this->store_temporary_firstname=$_POST['first_name'];
				//$this->store_temporary_lasttname=$_POST['last_name'];
				$this->store_temporary_Confirm_password=$_POST['confirm_password'];
				$this->store_temporary_security_number=$_POST['reg_name'];
				$this->store_temporary_gender=$_POST['gender'];
				$this->store_temporary_province=$_POST['province_city'];
				$this->membership_type=$_POST['membership_type'];
				
			    //$this->store_temporary_member_type=$_POST['member_type'];
			    $this->store_temporary_agree="checked";//checked for accept.
				
				$this->store_tempory_address=$_POST['address'];
				$this->store_tempory_company_description=$_POST['company_description'];
				$this->store_tempory_web=$_POST['website'];
	            $this->store_tempory_fax=$_POST['fax'];
				
			
				
			}
			else{
				//$this->store_temporary_id=$_POST['log_id'];
				$this->store_temporary_company_name=$_POST['company_name'];
				$this->store_temporary_contact_name=$_POST['contact_name'];
				$this->store_temporary_Email=$_POST['email'];
				//$this->store_temporary_Country=$_POST['countries'];
				$this->store_temporary_Password=$_POST['password'];
				$this->store_temporary_Phone=$_POST['mobile'];
				//$this->store_temporary_firstname=$_POST['first_name'];
				//$this->store_temporary_lasttname=$_POST['last_name'];
				$this->store_temporary_Confirm_password=$_POST['confirm_password'];
				$this->store_temporary_security_number=$_POST['reg_name'];
				$this->store_temporary_gender=$_POST['gender'];
				$this->store_temporary_province=$_POST['province_city'];
				$this->membership_type=$_POST['membership_type'];
				
			    //$this->store_temporary_member_type=$_POST['member_type'];
			    $this->store_temporary_agree="notcheck";	
				
				$this->store_tempory_address=$_POST['address'];
				$this->store_tempory_company_description=$_POST['company_description'];
				$this->store_tempory_web=$_POST['website'];
	            $this->store_tempory_fax=$_POST['fax'];
				
				
			}
			
			
			
			if(isset($_SESSION['remember_sec_number'])){
			  $total=$_SESSION['remember_sec_number'];
			}
			
			
			if($secu_value==""){
			   $get_result="Invalid";
			   $this->store_tempory_address=$_POST['address'];
				$this->store_tempory_company_description=$_POST['company_description'];
				$this->store_tempory_web=$_POST['website'];
	            $this->store_tempory_fax=$_POST['fax'];
				$this->store_temporary_contact_name=$_POST['contact_name'];
			   $_SESSION['alert_security']=1;
			   $this->secu_message_null="null";
			   
			}
			if($secu_value==$total){
			   $get_result="Valid";
			}
			
			if($secu_value!=$total){
				$this->store_tempory_address=$_POST['address'];
				$this->store_tempory_company_description=$_POST['company_description'];
				$this->store_temporary_contact_name=$_POST['contact_name'];
				$this->store_tempory_web=$_POST['website'];
	            $this->store_tempory_fax=$_POST['fax'];
				$get_result="Invalid";
				$_SESSION['alert_security']=1;
				$this->invalid_security="Invalid Value";
				$this->secu_message_invalid="Invalid";
			}
		/*	
			if($_FILES['file']['name']==""){
			 $filename="Invalid";
			}
			else{
			  $filename="Valid";
			}
			if($_POST['address']==""){
			 $address="Null";
			 $this->address_null="Please input address";
			}
			else{
			 $address="has";
			}
		*/
			//////////////////////
			// I agree with
			
			if($this->store_temporary_agree=="checked"){
			    $agree='checked';
				 $this->store_temporary_agree="checked";
			}
			else{
				$agree='notchecked';
				$this->store_temporary_agree="notcheck";
			}
			
			if($get_result=="Valid" and $agree=='checked' ){
				//$this->processRegisterForm($secu_value);
				$this->processRegisterForm_dealer($secu_value);
				
			}
			
	}
		
		
	
		
		// check if login form has been submitted ------------------------------
        if( isset($_POST['loginBtn']) )
        {
            $this->handleLoginSubmit();
			  //$this->user_login();
        }

    }
	
	//Select Province
	
	public function load_province(){
		require_once BASE_CLASS . 'class-connect.php';
		$db = new Connect();
        $db->open();
		$sql = @mysql_query("SELECT province_name FROM province");
		$this->province_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->province_list, $r);
        }
        @mysql_free_result($sql);
		return $this->province_list;
        $cnx->close();	
	}
	
	
	
	
	
	
	//Process Login Function.
		/*public function user_login(){
			$id_user = trim($_POST['emailInput']);
			$pass  = md5(trim($_POST['passwordInput']));	
			$this->validate_user($id_user,$pass);
			
			echo md5('81dc9bdb52d04dc20036dbd8313ed055');
	
		}
		
		public function validate_user( $id_user_input,$pass_user_input){
		
		    require_once BASE_CLASS . 'class-connect.php';
		    $db = new Connect();
            $db->open();
			$sql = @mysql_query("SELECT log_id,password,activated FROM register WHERE log_id = '$id_user_input' AND password='$pass_user_input' AND activated='1' ");
			$num=@mysql_num_rows($sql);
			if($num>0){
				echo"Welcome";
			}
			else{
				$_SESSION['validate']="Incorrect";
			}	
		}
	*/
		
	
	
	
	
	

    /*
     * Process registration form Personal -----------------------------------------------------
     * @return void.
     */
	protected function processRegisterForm_personal($secu_value){
		//condition for valid and invalid.
	   if($secu_value!=""){
		$valid_security=$secu_value;
	   }
	   else{
		$valid_security="";
	   }
	   
	   
		//require_once BASE_CLASS .'resize-class.php';
		// Initialize
		$member_type_1 = array();
		$member_type_1_string = "";
		$member_type_2 = array();
		$member_type_2_string = "";
		$business_type = array();
		$business_type_string = "";
		$business_field = array();
		$business_field_string = "";
		$log_id = "";
		$password = "";
		$fullname = "";
		
		$first_name="";
		$last_name="";
		$gender="";
		$province_city="";
		$member_type3="";
		
		$confirm_password = "";
		$email = "";
		$email_error = "";
		$confirm_email = "";
		$introduction = "";
		$msg = "";
		$company_name = "";
		$name = "";
		$card_id = "";
		$country = "";
		$address = "";
		$post_code = "";
		$tel = "";
		$fax = "";
		$phonecode = "";
		$reg_name="";
		$website = "";
		$available_services = array();
		$available_services_string = "";
		$term = "";
		$securityInput;
		extract($_POST);
		
		$passMD5 = md5($password);
		$email=trim($email);
					
					require BASE_CLASS . 'mail/PHPMailer.php';
					require BASE_CLASS . 'mail/PHPMailerPlugins/SMTP.class.php';
					
					$message = "";
					$subject = "";	
					$to_1 = "support@angkorauto.com";
					$to_2 = $email;											
					//$active_link = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';
					
					$subject = $this->_LANG['SENDEMAIL_SUBJECT_LABEL'];
					
					/* $message.="Thank you for registering with Angkorauto.<br/>";
					$message.="Please click on the vertification link below to activate your account.<br/>"; */
					$message.='
						<html>							
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								<title>Email confirm</title>

								<style>
									
									*{
										margin:0px;
										padding:0px;
									}
									body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td { 
									  margin:0;
									  padding:0;  
									}

									div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td {
									  font-size: 100%;
									}
									p,pre{
										margin: 0;
										letter-spacing:0;
									}
									table {
									  border-collapse:collapse;
									  border-spacing:0;
									}
									fieldset,img { 
									  border:0;
									}
									address,caption,cite,code,dfn,em,strong,th,var {
									  font-style:normal;
									  font-weight:normal;
									}
									ol,ul {
									  list-style:none;
									  margin: 0;
									  padding: 0;
									}
									caption,th {
									  text-align:left;
									}
									h1,h2,h3,h4,h5,h6 {
									  font-weight:normal;
									}
									q:before,q:after {
									  content:"";
									}
									abbr,acronym {
									  border:0;
									}

									img,
									input[type="image"] {
									  vertical-align: bottom;	  
									}
									
									img {
										display:block;
									}

									a {
									  color: blue;
									  text-decoration: underline;
									}

									input[type="submit"],
									input[type="button"],
									input[type="text"],
									input[type="password"],
									textarea {
									  margin:0;
									  padding:0;
									}
									input,
									select,
									textarea {
									  font-size: 100%; /* for iPhone */
									}
									input[type="text"],
									input[type="password"] {
									  vertical-align: baseline;
									}
								</style>

								</head>

								<body style="margin:0 auto; padding:0;">
									<div style="margin:0 auto;line-height:10px;padding:0;width:100%;">
										<div style="width:650px; margin:0 auto; padding:0 auto; background:#eeeeee;">
											<h2 style="padding:20px 0;text-align:center;color:#2c65b6;font-size:24px;font-weight:bold;">Thank You for Your Registration</h2>
											<p style="font-size:14px;font-weight:bold;text-align:center;padding-bottom:15px;">Thank you for registering with Angkorauto.</p>
											<p style="text-align:center;font-size:14px;padding-bottom:15px;">Please click on the vertification link below to activate your account.</p>
											<p style="text-align:center;color:blue;text-decoration:underline;padding-bottom:15px;">
												<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>
											</p>
											
											<div style="border-top:1px dashed #CCC;border-bottom:1px dashed #CCC;width:550px;margin:0 auto;padding-bottom:15px;">
												<p style="font-size:14px;font-weight:bold;text-align:center;padding-top:15px;">Your account will not be activated until you verify your email address.</p>
												<p style="text-align:center;font-size:14px;padding-top:15px;"><span style="font-size:14px;font-weight:bold;text-align:center;">Having trouble accessing your account?</span> <span>Please contact</span> 
												<span><a style="text-decoration:underline;color:blue;" class="notification" href="mailto:support@angkorauto.com">Online Customer Support.</a></span></p>
											</div>
											<div style="border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
												<div style="float:left;padding:10px 0;"><img src="http://demo.angkorauto.com/images/common/header/softbloom.png" /></div>
												<div style="float:left;padding:10px 0;margin-left:160px;width:75px;">
													<table> 
														<tr>
															<td colspan="4" style="padding-bottom:10px;" >Email:info@angkorauto.com</td>
														</tr>
														<tr><td></td></tr>
														<tr>
															<td><img title="Angkorauto Facebook" alt="Angkorauto Facebook" src="http://demo.angkorauto.com/images/social_icons/fb.png" /></td>
															<td><img title="Angkorauto Twitter" alt="Angkorauto Twitter" src="http://demo.angkorauto.com/images/social_icons/tt.png" /></td>
															<td><img title="Angkorauto Google+ " alt="Angkorauto Google+ " src="http://demo.angkorauto.com/images/social_icons/gplus.png" /></td>
															<td><img title="Angkorauto LinkedIn " alt="Angkorauto LinkedIn " src="http://demo.angkorauto.com/images/social_icons/in.png" /></td>
														</tr>
													</table>
												</div>
												<p style="clear:both"></p>
											</div>
											
											<p style="text-align:center;font-size:14px;padding:15px 0 20px 0;">Copyright © 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>
										</div>
									</div>
								</body>
						</html>
					';					
					
					$mail_send = new PHPMailer();
					$mail_send->IsSMTP();
					$mail_send->Host     = "mail.motorbb.com";
					$mail_send->Port     = "26";
					$mail_send->SMTPAuth = "yes";
					$mail_send->Username = "angkorauto@motorbb.com";
					$mail_send->Password = "eKROUAQHtG4-";
					$mail_send->FromName = "angkorauto.com";
					//$mail_send->From     = $to_1;
					$mail_send->AddAddress($to_2);		
					$mail_send->Subject  = $subject;		
					$mail_send->MsgHTML($message);
					$mail_send->IsHTML(true);		
					//$mail_send->Send();	
					if(!$mail_send->Send()) {
					   echo "Error sending: " . $mail_send->ErrorInfo;					   					   
					}else {
				
		
						require_once BASE_CLASS . 'class-connect.php';
						$db = new Connect();
						$db->open();
		
		if( !$sql = @mysql_query("INSERT INTO `register` (`firstname`,`lastname`,`gender`,`province`,`member_type3`, `email`, `password` , `name`, `group`, `creation`, `banned`, `activated`, `member_type1`, `member_type2`, `business_type`, `business_field`, `company_name`, `card_id`, `country`, `post_code`, `tel`, `fax`, `mobile`, `web`, `avalible_service`, `introduction`, `vehicle_id`, `address`,`register_type`)
							VALUES('$first_name','$last_name','$gender','$province_city','$member_type3','$email', '$passMD5', '$valid_security', 'user', '".date('Y-m-d')."', '0', '0', '$member_type', '$member_type_2', '$business_type', '$business_field', '$company_name', '$card_id', 'kh', '$post_code', '$tel', '$fax', '$mobile', '$website', '$available_services', '$introduction', '$id', '$address','m1')") ) {

							
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

							$db->close();
							$this->register_form_status = 'query';

							return;
						}else {
							
							$reg_id = mysql_insert_id();

							$query = "INSERT INTO data_count SET register_id = '".$reg_id."',
																 country = '$country',
																 car_count = '0',
																 truck_count = '0',
																 bus_count = '0',
																 part_count = '0',
																 accessories_count = '0',
																 equipment_count = '0',
																 motorbike_count = '0',
																 aircraft_count = '0',
																 watercraft_count = '0'";
							mysql_query($query);
						}
					
						header("Location: " .BASE_RELATIVE ."signup-success");
						exit;					
					
				}
	   	 
} 	//End of Process of Personal Form


	 
			
	  /*
     * Process registration form seller -----------------------------------------------------
     * @return void.
     */		
	protected function processRegisterForm_seller($secu_value){
		//condition for valid and invalid.
	   if($secu_value!=""){
		$valid_security=$secu_value;
	   }
	   else{
		$valid_security="";
	   }
	   
	   
		//require_once BASE_CLASS .'resize-class.php';
		// Initialize
		$member_type_1 = array();
		$member_type_1_string = "";
		$member_type_2 = array();
		$member_type_2_string = "";
		$business_type = array();
		$business_type_string = "";
		$business_field = array();
		$business_field_string = "";
		$log_id = "";
		$password = "";
		$fullname = "";
		
		$user_id="";
		$first_name="";
		$last_name="";
		$gender="";
		$province_city="";
		$member_type3="";
		$membership_type="";
		
		$confirm_password = "";
		$email = "";
		$email_error = "";
		$confirm_email = "";
		$introduction = "";
		$msg = "";
		$company_name = "";
		$name = "";
		$card_id = "";
		$country = "";
		$address = "";
		$post_code = "";
		$tel = "";
		$fax = "";
		$phonecode = "";
		$reg_name="";
		$website = "";
		$available_services = array();
		$available_services_string = "";
		$term = "";
		$securityInput;
		extract($_POST);
		
		$passMD5 = md5($password);
		$email=trim($email);
		$user_id=trim($user_id);
		
				// Check file upload
						if($_FILES['file']['name']!=""){
						if ($_FILES['file']['size']<10485760){
							
						$ext=strtolower(substr($_FILES['file']['name'],-3));
						$fname=date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;
						move_uploaded_file($_FILES['file']['tmp_name'] ,BASE_ROOT."/upload/profile/".$fname);	

							}
					}		
					
					require BASE_CLASS . 'mail/PHPMailer.php';
					require BASE_CLASS . 'mail/PHPMailerPlugins/SMTP.class.php';
					
					$message = "";
					$subject = "";	
					$to_1 = "support@angkorauto.com";
					$to_2 = $email;	
					$to_sale="sales@angkorauto.com";
					//$active_link = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';
					
					$subject = $this->_LANG['SENDEMAIL_SUBJECT_LABEL'];
					
					/* $message.="Thank you for registering with Angkorauto.<br/>";
					$message.="Please click on the vertification link below to activate your account.<br/>"; */
					$message.='
						<html>							
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								<title>Email confirm</title>

								<style>
									
									*{
										margin:0px;
										padding:0px;
									}
									body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td { 
									  margin:0;
									  padding:0;  
									}

									div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td {
									  font-size: 100%;
									}
									p,pre{
										margin: 0;
										letter-spacing:0;
									}
									table {
									  border-collapse:collapse;
									  border-spacing:0;
									}
									fieldset,img { 
									  border:0;
									}
									address,caption,cite,code,dfn,em,strong,th,var {
									  font-style:normal;
									  font-weight:normal;
									}
									ol,ul {
									  list-style:none;
									  margin: 0;
									  padding: 0;
									}
									caption,th {
									  text-align:left;
									}
									h1,h2,h3,h4,h5,h6 {
									  font-weight:normal;
									}
									q:before,q:after {
									  content:"";
									}
									abbr,acronym {
									  border:0;
									}

									img,
									input[type="image"] {
									  vertical-align: bottom;	  
									}
									
									img {
										display:block;
									}

									a {
									  color: blue;
									  text-decoration: underline;
									}

									input[type="submit"],
									input[type="button"],
									input[type="text"],
									input[type="password"],
									textarea {
									  margin:0;
									  padding:0;
									}
									input,
									select,
									textarea {
									  font-size: 100%; /* for iPhone */
									}
									input[type="text"],
									input[type="password"] {
									  vertical-align: baseline;
									}
								</style>

								</head>

								<body style="margin:0 auto; padding:0;">
									<div style="margin:0 auto;line-height:10px;padding:0;width:100%;">
										<div style="width:650px; margin:0 auto; padding:0 auto; background:#eeeeee;">
											<h2 style="padding:20px 0;text-align:center;color:#2c65b6;font-size:24px;font-weight:bold;">Thank You for Your Registration</h2>
											<p style="font-size:14px;font-weight:bold;text-align:center;padding-bottom:15px;">Thank you for registering with Angkorauto.</p>
											<p style="text-align:center;font-size:14px;padding-bottom:15px;">Please click on the vertification link below to activate your account.</p>
											<p style="text-align:center;color:blue;text-decoration:underline;padding-bottom:15px;">
												<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'&&userID='.$user_id.'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>
											</p>
											
											<div style="border-top:1px dashed #CCC;border-bottom:1px dashed #CCC;width:550px;margin:0 auto;padding-bottom:15px;">
												<p style="font-size:14px;font-weight:bold;text-align:center;padding-top:15px;">Your account will not be activated until you verify your email address.</p>
												<p style="text-align:center;font-size:14px;padding-top:15px;"><span style="font-size:14px;font-weight:bold;text-align:center;">Having trouble accessing your account?</span> <span>Please contact</span> 
												<span><a style="text-decoration:underline;color:blue;" class="notification" href="mailto:support@angkorauto.com">Online Customer Support.</a></span></p>
											</div>
											<div style="border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
												<div style="float:left;padding:10px 0;"><img src="http://angkorauto.com/images/common/header/softbloom.png" /></div>
												<div style="float:left;padding:10px 0;margin-left:160px;width:75px;">
													<table> 
														<tr>
															<td colspan="4" style="padding-bottom:10px;" >Email:info@angkorauto.com</td>
														</tr>
														<tr><td></td></tr>
														<tr>
															<td><img title="Angkorauto Facebook" alt="Angkorauto Facebook" src="http://angkorauto.com/images/social_icons/fb.png" /></td>
															<td><img title="Angkorauto Twitter" alt="Angkorauto Twitter" src="http://angkorauto.com/images/social_icons/tt.png" /></td>
															<td><img title="Angkorauto Google+ " alt="Angkorauto Google+ " src="http://angkorauto.com/images/social_icons/gplus.png" /></td>
															<td><img title="Angkorauto LinkedIn " alt="Angkorauto LinkedIn " src="http://angkorauto.com/images/social_icons/in.png" /></td>
														</tr>
													</table>
												</div>
												<p style="clear:both"></p>
											</div>
											
											<p style="text-align:center;font-size:14px;padding:15px 0 20px 0;">Copyright &copy; 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>
										</div>
									</div>
								</body>
						</html>
					';		
										
					
					$mail_send = new PHPMailer();
					$mail_send->IsSMTP();
					$mail_send->Host = "mail.angkorauto.com";
					$mail_send->SMTPAuth = "yes";
					$mail_send->Username = "system@angkorauto.com";
					$mail_send->Password = "[qHeS{7rL.DT";
					$mail_send->Port     = "26";
					$mail_send->FromName = "angkorauto.com";
					$mail_send->From     = $to_1;
					$mail_send->AddAddress($to_2);
					$mail_send->AddAddress($to_sale);
					$mail_send->Subject  = $subject;		
					$mail_send->MsgHTML($message);
					$mail_send->IsHTML(true);		
					//$mail_send->Send();	
					 if(!$mail_send->Send()) {
					   echo "Error sending: " . $mail_send->ErrorInfo;	
						
					}else { 
							
							
		
						require_once BASE_CLASS . 'class-connect.php';
						$db = new Connect();
						$db->open();
		


		/* if( !$sql = @mysql_query("INSERT INTO `register` (`firstname`,`lastname`,`username`,`gender`,`province`,`member_type3`,`membership_type`,`image`,`address` ,`email`, `password` , `name`, `group`, `creation`, `banned`, `activated`, `member_type1`, `member_type2`, `business_type`, `business_field`, `company_name`, `card_id`, `country`, `post_code`, `tel`, `fax`, `mobile`, `web`, `avalible_service`, `introduction`, `vehicle_id`,`register_type`)
							VALUES('$first_name','$last_name',null,'$gender','$province_city','$member_type3','$membership_type','$fname','$address','$email', '$passMD5', '$valid_security', 'user', '".date('Y-m-d')."', '0', '0', '$member_type', '$member_type_2', '$business_type', '$business_field', '$company_name', '$card_id', 'kh', '$post_code', '$tel', '$fax', '$mobile', '$website', '$available_services', '$introduction', '$id','m1')") ) { */						
			
			if( !$sql = @mysql_query("INSERT INTO `register` (`firstname`,`lastname`,`username`,`gender`,`province`,`member_type3`,`membership_type`,`address` ,`email`, `password`, `temp_password` , `group`, `creation`, `banned`, `activated`, `member_type1`, `member_type2`, `company_name`, `country`, `mobile`, `web`, `avalible_service`, `introduction`, `register_type`)
							VALUES('$first_name','$last_name','$user_id','$gender','$province_city','$member_type3','$membership_type','$address','$email', '$passMD5', '$password', 'user', '".date('Y-m-d')."', '0', '0', '$member_type', '$member_type_2', '$company_name', 'kh', '$mobile', '$website', '$available_services', '$introduction' ,'m1')") ) {
							
							
							
							
							
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

							$db->close();
							$this->register_form_status = 'query';

							return;
						}
			// insert user id in to tb seller				
			if( !$sql = @mysql_query("INSERT INTO `tbl_carfinder_sellers` (`user_id`,`email`,`contact_no`)
									VALUES('".$this->user_id."','$email','$mobile')") ) {
							
							
							
							
							
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

							$db->close();
							$this->register_form_status = 'query';

							return;
						}else {
							if($fname != ""){
								$selectuser = "select * from register where username='$user_id' and password='$passMD5'";
								$selectquery =  @mysql_query($selectuser);
								$userinfo = @mysql_fetch_assoc($selectquery);
								$mysql = "insert into more_image (owner,m_image,primary_photo) values ('".$userinfo['user_id']."','$fname','1')";
								$myquery = @mysql_query($mysql);			
								$reg_id = mysql_insert_id();
	
								$query = "INSERT INTO data_count SET register_id = '".$reg_id."',
																	 country = '$country',
																	 car_count = '0',
																	 truck_count = '0',
																	 bus_count = '0',
																	 part_count = '0',
																	 accessories_count = '0',
																	 equipment_count = '0',
																	 motorbike_count = '0',
																	 aircraft_count = '0',
																	 watercraft_count = '0'";
								mysql_query($query);
							}
						}
					
						header("Location: " .BASE_RELATIVE ."signup-success");
						exit;					
					
				}
	   	 
} 	// End Process of Seller.
			
			

	  /*
     * Process registration form dealer -----------------------------------------------------
     * @return void.
     */		
	protected function processRegisterForm_dealer($secu_value){
		//condition for valid and invalid.

	   if($secu_value!=""){
		$valid_security=$secu_value;
	   }
	   else{
		$valid_security="";
	   }
	   
	   //condition for country selection
	   
	   /*if($select_country!=""){
		$user_country=$select_country;
	   }
	   else{
	    $user_country="";
	   }*/
	   
	   
	    require_once BASE_CLASS . 'class-connect.php';
		//require_once BASE_CLASS .'resize-class.php';
		// Initialize
		$member_type_1 = array();
		$member_type_1_string = "";
		$member_type_2 = array();
		$member_type_2_string = "";
		$business_type = array();
		$business_type_string = "";
		$business_field = array();
		$business_field_string = "";
		$log_id = "";
		$password = "";
		//$fullname = "";
		
		$company_name="";
		$contact_name="";
		//$gender="";
		$province_city="";
		$member_type3="";
		$membership_type="";
		
		$confirm_password = "";
		$email = "";
		$email_error = "";
		$confirm_email = "";
		$company_description = "";
		$msg = "";
		//$company_name = "";
		$name = "";
		$card_id = "";
		$country = "";
		$address = "";
		$post_code = "";
		$tel = "";
		$fax = "";
		$phonecode = "";
		$reg_name="";
		$website = "";
		$available_services = array();
		$available_services_string = "";
		$term = "";
		$securityInput;
		extract($_POST);
		
		$passMD5 = md5($password);
		$email=trim($email);
		
		// Check file upload
		if($_FILES['file']['name']!=""){
			    $fname=$_FILES['file']['name'];
				move_uploaded_file($_FILES['file']['tmp_name'] ,BASE_ROOT."/upload/profile/".$fname);
		}			
					require BASE_CLASS . 'mail/PHPMailer.php';
					require BASE_CLASS . 'mail/PHPMailerPlugins/SMTP.class.php';
					
					$message = "";
					$subject = "";	
					$to_1 = "support@angkorauto.com";
					$to_2 = $email;											
					//$active_link = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';
					
					$subject = $this->_LANG['SENDEMAIL_SUBJECT_LABEL'];
					
					/* $message.="Thank you for registering with Angkorauto.<br/>";
					$message.="Please click on the vertification link below to activate your account.<br/>"; */
					$message.='
						<html>							
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								<title>Email confirm</title>

								<style>
									
									*{
										margin:0px;
										padding:0px;
									}
									body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td { 
									  margin:0;
									  padding:0;  
									}

									div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td {
									  font-size: 100%;
									}
									p,pre{
										margin: 0;
										letter-spacing:0;
									}
									table {
									  border-collapse:collapse;
									  border-spacing:0;
									}
									fieldset,img { 
									  border:0;
									}
									address,caption,cite,code,dfn,em,strong,th,var {
									  font-style:normal;
									  font-weight:normal;
									}
									ol,ul {
									  list-style:none;
									  margin: 0;
									  padding: 0;
									}
									caption,th {
									  text-align:left;
									}
									h1,h2,h3,h4,h5,h6 {
									  font-weight:normal;
									}
									q:before,q:after {
									  content:"";
									}
									abbr,acronym {
									  border:0;
									}

									img,
									input[type="image"] {
									  vertical-align: bottom;	  
									}
									
									img {
										display:block;
									}

									a {
									  color: blue;
									  text-decoration: underline;
									}

									input[type="submit"],
									input[type="button"],
									input[type="text"],
									input[type="password"],
									textarea {
									  margin:0;
									  padding:0;
									}
									input,
									select,
									textarea {
									  font-size: 100%; /* for iPhone */
									}
									input[type="text"],
									input[type="password"] {
									  vertical-align: baseline;
									}
								</style>

								</head>

								<body style="margin:0 auto; padding:0;">
									<div style="margin:0 auto;line-height:10px;padding:0;width:100%;">
										<div style="width:650px; margin:0 auto; padding:0 auto; background:#eeeeee;">
											<h2 style="padding:20px 0;text-align:center;color:#2c65b6;font-size:24px;font-weight:bold;">Thank You for Your Registration</h2>
											<p style="font-size:14px;font-weight:bold;text-align:center;padding-bottom:15px;">Thank you for registering with Angkorauto.</p>
											<p style="text-align:center;font-size:14px;padding-bottom:15px;">Please click on the vertification link below to activate your account.</p>
											<p style="text-align:center;color:blue;text-decoration:underline;padding-bottom:15px;">
												<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>
											</p>
											
											<div style="border-top:1px dashed #CCC;border-bottom:1px dashed #CCC;width:550px;margin:0 auto;padding-bottom:15px;">
												<p style="font-size:14px;font-weight:bold;text-align:center;padding-top:15px;">Your account will not be activated until you verify your email address.</p>
												<p style="text-align:center;font-size:14px;padding-top:15px;"><span style="font-size:14px;font-weight:bold;text-align:center;">Having trouble accessing your account?</span> <span>Please contact</span> 
												<span><a style="text-decoration:underline;color:blue;" class="notification" href="mailto:support@angkorauto.com">Online Customer Support.</a></span></p>
											</div>
											<div style="border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
												<div style="float:left;padding:10px 0;"><img src="http://demo.angkorauto.com/images/common/header/softbloom.png" /></div>
												<div style="float:left;padding:10px 0;margin-left:160px;width:75px;">
													<table> 
														<tr>
															<td colspan="4" style="padding-bottom:10px;" >Email:info@angkorauto.com</td>
														</tr>
														<tr><td></td></tr>
														<tr>
															<td><img title="Angkorauto Facebook" alt="Angkorauto Facebook" src="http://demo.angkorauto.com/images/social_icons/fb.png" /></td>
															<td><img title="Angkorauto Twitter" alt="Angkorauto Twitter" src="http://demo.angkorauto.com/images/social_icons/tt.png" /></td>
															<td><img title="Angkorauto Google+ " alt="Angkorauto Google+ " src="http://demo.angkorauto.com/images/social_icons/gplus.png" /></td>
															<td><img title="Angkorauto LinkedIn " alt="Angkorauto LinkedIn " src="http://demo.angkorauto.com/images/social_icons/in.png" /></td>
														</tr>
													</table>
												</div>
												<p style="clear:both"></p>
											</div>
											
											<p style="text-align:center;font-size:14px;padding:15px 0 20px 0;">Copyright © 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>
										</div>
									</div>
								</body>
						</html>
					';					
					
					$mail_send = new PHPMailer();
					$mail_send->IsSMTP();
					$mail_send->Host     = "ssl://angkorauto.com";
					$mail_send->Port     = "465";
					$mail_send->SMTPAuth = "yes";
					$mail_send->Username = "support@angkorauto.com";
					$mail_send->Password = "Angkorauto168";
					$mail_send->FromName = "angkorauto.com";
					$mail_send->From     = $to_1;
					$mail_send->AddAddress($to_2);		
					$mail_send->Subject  = $subject;		
					$mail_send->MsgHTML($message);
					$mail_send->IsHTML(true);		
					//$mail_send->Send();	
					if(!$mail_send->Send()) {
					   echo "Error sending: " . $mail_send->ErrorInfo;	
						
					}else {
							
							
		
						require_once BASE_CLASS . 'class-connect.php';
						$db = new Connect();
						$db->open();
		
		if( !$sql = @mysql_query("INSERT INTO `register` (`company_name`,`contact_name`,`province`,`member_type3`,`membership_type`,`image`,`address` ,`email`, `password` , `group`, `creation`, `banned`, `activated`, `member_type1`, `member_type2`, `country`, `mobile`, `web`, `company_description`, `avalible_service`, `introduction`,`register_type`)
							VALUES('$company_name','$contact_name','$province_city','$member_type3','$membership_type','$fname','$address','$email', '$passMD5', 'user', '".date('Y-m-d')."', '0', '0', '$member_type', '$member_type_2', 'kh', '$mobile', '$website', '$company_description', '$available_services', '$introduction', 'm1')") ) {

							
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

							$db->close();
							$this->register_form_status = 'query';

							return;
						}else {
							
							$reg_id = mysql_insert_id();

							$query = "INSERT INTO data_count SET register_id = '".$reg_id."',
																 country = '$country',
																 car_count = '0',
																 truck_count = '0',
																 bus_count = '0',
																 part_count = '0',
																 accessories_count = '0',
																 equipment_count = '0',
																 motorbike_count = '0',
																 aircraft_count = '0',
																 watercraft_count = '0'";
							mysql_query($query);
						}
					
						header("Location: " .BASE_RELATIVE ."signup-success");
						exit;					
					
				}
	   	 
} 	// End Process of Dealer.		
			
			
	
    protected function processRegisterForm($secu_value)
    {			
		
	     //condition for valid and invalid.
	   if($secu_value!=""){
		$valid_security=$secu_value;
	   }
	   else{
		$valid_security="";
	   }
	   
	   //condition for country selection
	   
	   /*if($select_country!=""){
		$user_country=$select_country;
	   }
	   else{
	    $user_country="";
	   }*/
	   
	   
	    require_once BASE_CLASS . 'class-connect.php';
		//require_once BASE_CLASS .'resize-class.php';
		// Initialize
		$member_type_1 = array();
		$member_type_1_string = "";
		$member_type_2 = array();
		$member_type_2_string = "";
		$business_type = array();
		$business_type_string = "";
		$business_field = array();
		$business_field_string = "";
		$log_id = "";
		$password = "";
		$fullname = "";
		
		$first_name="";
		$last_name="";
		$gender="";
		$province_city="";
		$confirm_password = "";
		$email = "";
		$email_error = "";
		$confirm_email = "";
		$introduction = "";
		$msg = "";
		$company_name = "";
		$name = "";
		$card_id = "";
		$country = "";
		$address = "";
		$post_code = "";
		$tel = "";
		$fax = "";
		$phonecode = "";
		$reg_name="";
		$website = "";
		$available_services = array();
		$available_services_string = "";
		$term = "";
		$securityInput;
		extract($_POST);
		
		
		
		//$file = $_FILES['file']['name'];
//		$uploadedfile = $_FILES['file']['tmp_name'];

		// set registration form session.
		
		$_SESSION['reg_log_id'] = $log_id =trim($log_id);
		
		
		$_SESSION['reg_email'] = $email=trim($email);
		$_SESSION['reg_password'] = $password=trim($password);
		$_SESSION['reg_confirm_password'] = $confirm_password=trim($confirm_password);
		$_SESSION['reg_confirm_email'] = $confirm_email;
		$_SESSION['reg_company_name'] = $company_name;
		$_SESSION['reg_fullname'] = $fullname=trim($fullname);
        $_SESSION['reg_name'] = $name;
		$_SESSION['reg_card_id'] = $card_id;
		$_SESSION['reg_country'] = $country;
		$_SESSION['reg_address'] = $address;
		$_SESSION['reg_post_code'] = $post_code;
		$_SESSION['reg_tel'] = $tel;
		$_SESSION['reg_fax'] = $fax;
		$_SESSION['reg_mobile'] = $mobile;
		$_SESSION['reg_name'] = $reg_name;
		$_SESSION['reg_website'] = $website;
        $system_value=$_SESSION['security_question_a'] + $_SESSION['security_question_b'];
        // unset security session.
        unset($_SESSION['security_question_a']);
        unset($_SESSION['security_question_b']);


		require_once BASE_CLASS . 'class-utilities.php';


        // validate fields.
		/* checks if any member_type_1 is checked */
		if( isset($member_type_1) ) {
			/* Iterate the member_type_1 array and get the keys and values */
			foreach($member_type_1 as $key => $value) {
				if( isset($member_type_1[$key]) ) {
					$_SESSION['reg_member_type_1_'.$key] = $value;
				}
				$member_type_1_string.= $value.",";
			}

			/* unset member_type_1 session */
			if( empty($member_type_1[0]) ) { unset($_SESSION['reg_member_type_1_0']); }
			if( empty($member_type_1[1]) ) { unset($_SESSION['reg_member_type_1_1']); }
			if( empty($member_type_1[2]) ) { unset($_SESSION['reg_member_type_1_2']); }

			$member_type_1 = substr($member_type_1_string, 0, -1);
		}

		/* checks if any member_type_2 is checked */
		if( isset($member_type_2) ) {
			/* Iterate the member_type_2 array and get the keys and values */
			foreach($member_type_2 as $key => $value) {
				if( isset($member_type_2[$key]) ) {
					$_SESSION['reg_member_type_2_'.$key] = $value;
				}
				$member_type_2_string.= $value.",";
			}

			/* unset member_type_2 session */
			if( empty($member_type_2[0]) ) { unset($_SESSION['reg_member_type_2_0']); }
			if( empty($member_type_2[1]) ) { unset($_SESSION['reg_member_type_2_1']); }

			$member_type_2 = substr($member_type_2_string, 0, -1);
		}

		/* checks if any business_type is checked */
		if( isset($business_type) ) {
			/* Iterate the business_type array and get the keys and values */
			foreach($business_type as $key => $value) {
				if( isset($business_type[$key]) ) {
					$_SESSION['reg_business_type_'.$key] = $value;
				}
				$business_type_string.= $value.",";
			}

			/* unset business_type session */
			if( empty($business_type[0]) ) { unset($_SESSION['reg_business_type_0']); }
			if( empty($business_type[1]) ) { unset($_SESSION['reg_business_type_1']); }

			$business_type = substr($business_type_string, 0, -1);
		}

		/* checks if any business_field is checked */
		if( isset($business_field) ) {
			/* Iterate the business_field array and get the keys and values */
			foreach($business_field as $key => $value) {
				if( Utilities::Required($business_field[$key]) ) {
					$_SESSION['reg_business_field_'.$key] = $value;
				}
				$business_field_string.= $value.",";
			}

			/* unset business_field session */
			if( empty($business_field[0]) ) { unset($_SESSION['reg_business_field_0']); }
			if( empty($business_field[1]) ) { unset($_SESSION['reg_business_field_1']); }
			if( empty($business_field[2]) ) { unset($_SESSION['reg_business_field_2']); }
			if( empty($business_field[3]) ) { unset($_SESSION['reg_business_field_3']); }
			if( empty($business_field[4]) ) { unset($_SESSION['reg_business_field_4']); }
			if( empty($business_field[5]) ) { unset($_SESSION['reg_business_field_5']); }
			if( empty($business_field[6]) ) { unset($_SESSION['reg_business_field_6']); }
			if( empty($business_field[7]) ) { unset($_SESSION['reg_business_field_7']); }
			if( empty($business_field[8]) ) { unset($_SESSION['reg_business_field_8']); }
			if( empty($business_field[9]) ) { unset($_SESSION['reg_business_field_9']); }

			$business_field = substr($business_field_string, 0, -1);
		}

		/* checks if any available_services is checked */
		if( isset($available_services) ) {
			/* Iterate the available_services array and get the keys and values */
			foreach($available_services as $key => $value) {
				if( isset($available_services[$key]) ) {
					$_SESSION['reg_available_services_'.$key] = $value;
				}
				$available_services_string.= $value.",";
			}

			/* unset available_services session */
			if( empty($available_services[0]) ) { unset($_SESSION['reg_available_services_0']); }
			if( empty($available_services[1]) ) { unset($_SESSION['reg_available_services_1']); }
			if( empty($available_services[2]) ) { unset($_SESSION['reg_available_services_2']); }
			if( empty($available_services[3]) ) { unset($_SESSION['reg_available_services_3']); }
			if( empty($available_services[4]) ) { unset($_SESSION['reg_available_services_4']); }
			if( empty($available_services[5]) ) { unset($_SESSION['reg_available_services_5']); }
			if( empty($available_services[6]) ) { unset($_SESSION['reg_available_services_6']); }
			if( empty($available_services[7]) ) { unset($_SESSION['reg_available_services_7']); }

			$available_services = substr($available_services_string, 0, -1);
		}

		if( Utilities::Required($introduction) ) {
			$_SESSION['reg_introduction'] = $introduction;
			/*$breaks = array("\n");
			$introduction = str_ireplace($breaks, "<br />", $introduction);*/
		}

        if( $term != 'accept' ) {
            $this->register_form_status = 'error';
            return;
        }
        //validate user id
        if(!Utilities::validateUserId($log_id)){
        	$this->register_form_status = 'invalidUserId';
            return;
        }
       /* if(empty($securityInput)){
			 $this->register_form_status = 'errorblank';
            return;
		}*/
		//echo "SYS=". $system_value . "INPUT=".$securityInput;
	/*	if($system_value!=$securityInput){


			 $this->register_form_status = 'errorinput';

            return;
		}*/

        // check if email is already registered.
        require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();



		if( (!$sql = @mysql_query("SELECT email FROM register WHERE email = '$email'")) || (!$sql_2 = @mysql_query("SELECT log_id FROM register WHERE log_id = '$log_id'")) ) {
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

            $db->close();
            $this->register_form_status = 'query';
        }

		if( @mysql_num_rows($sql_2) > 0 ) {
			$db->close();
			$this->register_form_status = 'log_id';
			return;
		} else {
		    
			if( @mysql_num_rows($sql) > 0 ) {
				$db->close();
				$this->register_form_status = 'email';
				return;
			}
			else {
			

				// unset register form session.
				unset($_SESSION['reg_member_type_1_0']);
				unset($_SESSION['reg_member_type_1_1']);
				unset($_SESSION['reg_member_type_1_2']);
				unset($_SESSION['reg_member_type_2_0']);
				unset($_SESSION['reg_member_type_2_1']);
				unset($_SESSION['reg_business_type_0']);
				unset($_SESSION['reg_business_type_1']);
				unset($_SESSION['reg_business_field_0']);
				unset($_SESSION['reg_business_field_1']);
				unset($_SESSION['reg_business_field_2']);
				unset($_SESSION['reg_business_field_3']);
				unset($_SESSION['reg_business_field_4']);
				unset($_SESSION['reg_business_field_5']);
				unset($_SESSION['reg_business_field_6']);
				unset($_SESSION['reg_business_field_7']);
				unset($_SESSION['reg_business_field_8']);
				unset($_SESSION['reg_business_field_9']);
				unset($_SESSION['reg_log_id']);
				unset($_SESSION['reg_email']);
				unset($_SESSION['reg_password']);
				unset($_SESSION['reg_confirm_password']);
				unset($_SESSION['reg_confirm_email']);
				unset($_SESSION['reg_fullname']);
				unset($_SESSION['reg_introduction']);
				unset($_SESSION['reg_company_name']);
				unset($_SESSION['reg_name']);
				unset($_SESSION['reg_card_id']);
				unset($_SESSION['reg_country']);
				unset($_SESSION['reg_address']);
				unset($_SESSION['reg_post_code']);
				unset($_SESSION['reg_tel']);
				unset($_SESSION['reg_fax']);
				unset($_SESSION['reg_name']);
				unset($_SESSION['reg_mobile']);
				unset($_SESSION['reg_website']);
				unset($_SESSION['reg_available_services_0']);
				unset($_SESSION['reg_available_services_1']);
				unset($_SESSION['reg_available_services_2']);
				unset($_SESSION['reg_available_services_3']);
				unset($_SESSION['reg_available_services_4']);
				unset($_SESSION['reg_available_services_5']);
				unset($_SESSION['reg_available_services_6']);
				unset($_SESSION['reg_available_services_7']);

				
				
				 
				 
				// filter values.
				$email = mysql_real_escape_string($email);
				$reg_name = mysql_real_escape_string($reg_name);
				$passMD5 = md5($password);

				$rows = mysql_fetch_array(mysql_query("SELECT * FROM register ORDER BY user_id DESC"));
				$ids = $rows['user_id'];
				$ids++;
				
				if(isset($_POST['member_type'])){
				   $member_type=$_POST['member_type'];
				}
				else{
				   $member_type="";
				}								

						// echo "Success Update Query";
					// Сheck that we have a file

						// Check if the file is JPEG or PNG image and it's size is less than 350Kb
						/* $ext=strtolower(substr($_FILES['file']['name'],-3));

	if ( $ext != "php" ) {

							// Determine the path to which we want to save this file
						//	$file=$_FILES['file']['name'];
							$fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;

						    move_uploaded_file($_FILES['file']['tmp_name'] ,BASE_ROOT."/upload/".$fname);


			                $my_info=$fname;
							$thum_myinfo=$fname;


							$resize_myinfo=new resize(BASE_ROOT."/upload/".$fname);
					        $resize_thumb = new resize(BASE_ROOT."/upload/".$fname);


						    $resize_myinfo->resizeImage(500, 400, 'crop');
					        $resize_thumb->resizeImage(160, 120, 'crop');


						    $resize_myinfo->saveImage(BASE_ROOT."/upload/myinfo/".$fname, 100);
					        $resize_thumb->saveImage(BASE_ROOT."/upload/thumb/".$fname, 100);
							unlink(BASE_ROOT."/upload/".$fname);


							mysql_query("UPDATE register SET image = '$fname' WHERE log_id = '$log_id'");

							$db->close();


							$this->register_form_status = 'complete';

							header("Location: " . BASE_RELATIVE . DEFAULT_HOME_SLUG);
							

						}*/

					// set send mail message.
					//$message = $this->_LANG['SENDEMAIL_MESSAGE_REGISTRATION'];
					//$message = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';

					// send activation email to user.
					/* require BASE_CLASS . 'class-sendmail.php';

					$send = new SendEmail();
					$send->setEmail($email);
					$send->setMessage($message);
					$send->setSubject($this->_LANG['SENDEMAIL_SUBJECT_LABEL']);
					$send->send(); */
					
					require BASE_CLASS . 'mail/PHPMailer.php';
					require BASE_CLASS . 'mail/PHPMailerPlugins/SMTP.class.php';
					
					$message = "";
					$subject = "";	
					$to_1 = "support@angkorauto.com";
					$to_2 = $email;											
					//$active_link = '<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>';
					
					$subject = $this->_LANG['SENDEMAIL_SUBJECT_LABEL'];
					
					/* $message.="Thank you for registering with Angkorauto.<br/>";
					$message.="Please click on the vertification link below to activate your account.<br/>"; */
					$message.='
						<html>							
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								<title>Email confirm</title>

								<style>
									
									*{
										margin:0px;
										padding:0px;
									}
									body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td { 
									  margin:0;
									  padding:0;  
									}

									div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,
									form,fieldset,input,textarea,p,blockquote,th,td {
									  font-size: 100%;
									}
									p,pre{
										margin: 0;
										letter-spacing:0;
									}
									table {
									  border-collapse:collapse;
									  border-spacing:0;
									}
									fieldset,img { 
									  border:0;
									}
									address,caption,cite,code,dfn,em,strong,th,var {
									  font-style:normal;
									  font-weight:normal;
									}
									ol,ul {
									  list-style:none;
									  margin: 0;
									  padding: 0;
									}
									caption,th {
									  text-align:left;
									}
									h1,h2,h3,h4,h5,h6 {
									  font-weight:normal;
									}
									q:before,q:after {
									  content:"";
									}
									abbr,acronym {
									  border:0;
									}

									img,
									input[type="image"] {
									  vertical-align: bottom;	  
									}
									
									img {
										display:block;
									}

									a {
									  color: blue;
									  text-decoration: underline;
									}

									input[type="submit"],
									input[type="button"],
									input[type="text"],
									input[type="password"],
									textarea {
									  margin:0;
									  padding:0;
									}
									input,
									select,
									textarea {
									  font-size: 100%; /* for iPhone */
									}
									input[type="text"],
									input[type="password"] {
									  vertical-align: baseline;
									}
								</style>

								</head>

								<body style="margin:0 auto; padding:0;">
									<div style="margin:0 auto;line-height:10px;padding:0;width:100%;">
										<div style="width:650px; margin:0 auto; padding:0 auto; background:#eeeeee;">
											<h2 style="padding:20px 0;text-align:center;color:#2c65b6;font-size:24px;font-weight:bold;">Thank You for Your Registration</h2>
											<p style="font-size:14px;font-weight:bold;text-align:center;padding-bottom:15px;">Thank you for registering with Angkorauto.</p>
											<p style="text-align:center;font-size:14px;padding-bottom:15px;">Please click on the vertification link below to activate your account.</p>
											<p style="text-align:center;color:blue;text-decoration:underline;padding-bottom:15px;">
												<a href="'.BASE_SITE.'activate/?account='.base64_encode($email).'">' . $this->_LANG['SENDEMAIL_ACTVATE_NOW'] . '</a><br>
											</p>
											
											<div style="border-top:1px dashed #CCC;border-bottom:1px dashed #CCC;width:550px;margin:0 auto;padding-bottom:15px;">
												<p style="font-size:14px;font-weight:bold;text-align:center;padding-top:15px;">Your account will not be activated until you verify your email address.</p>
												<p style="text-align:center;font-size:14px;padding-top:15px;"><span style="font-size:14px;font-weight:bold;text-align:center;">Having trouble accessing your account?</span> <span>Please contact</span> 
												<span><a style="text-decoration:underline;color:blue;" class="notification" href="mailto:support@angkorauto.com">Online Customer Support.</a></span></p>
											</div>
											<div style="border-bottom:1px dashed #CCC;width:550px;margin:0 auto;">
												<div style="float:left;padding:10px 0;"><img src="http://demo.angkorauto.com/images/common/header/softbloom.png" /></div>
												<div style="float:left;padding:10px 0;margin-left:160px;width:75px;">
													<table> 
														<tr>
															<td colspan="4" style="padding-bottom:10px;" >Email:info@angkorauto.com</td>
														</tr>
														<tr><td></td></tr>
														<tr>
															<td><img title="Angkorauto Facebook" alt="Angkorauto Facebook" src="http://demo.angkorauto.com/images/social_icons/fb.png" /></td>
															<td><img title="Angkorauto Twitter" alt="Angkorauto Twitter" src="http://demo.angkorauto.com/images/social_icons/tt.png" /></td>
															<td><img title="Angkorauto Google+ " alt="Angkorauto Google+ " src="http://demo.angkorauto.com/images/social_icons/gplus.png" /></td>
															<td><img title="Angkorauto LinkedIn " alt="Angkorauto LinkedIn " src="http://demo.angkorauto.com/images/social_icons/in.png" /></td>
														</tr>
													</table>
												</div>
												<p style="clear:both"></p>
											</div>
											
											<p style="text-align:center;font-size:14px;padding:15px 0 20px 0;">Copyright © 2014 Angkorauto. All Rights Reserved by Softbloom co., ltd</p>
										</div>
									</div>
								</body>
						</html>
					';					
					
					$mail_send = new PHPMailer();
					$mail_send->IsSMTP();
					$mail_send->Host     = "ssl://angkorauto.com";
					$mail_send->Port     = "465";
					$mail_send->SMTPAuth = "yes";
					$mail_send->Username = "support@angkorauto.com";
					$mail_send->Password = "Angkorauto168";
					$mail_send->FromName = "angkorauto.com";
					$mail_send->From     = $to_1;
					$mail_send->AddAddress($to_2);		
					$mail_send->Subject  = $subject;		
					$mail_send->MsgHTML($message);
					$mail_send->IsHTML(true);		
					//$mail_send->Send();	
					if(!$mail_send->Send()) {
					   echo "Error sending: " . $mail_send->ErrorInfo;					   					   
					}else {
					
					
					
						// register the user account.
						if( !$sql = @mysql_query("INSERT INTO `register` (`firstname`,`lastname`,`gender`,`province`, `email`, `password` , `name`, `group`, `creation`, `banned`, `activated`, `member_type1`, `member_type2`, `business_type`, `business_field`, `company_name`, `card_id`, `country`, `post_code`, `tel`, `fax`, `mobile`, `web`, `avalible_service`, `introduction`, `vehicle_id`, `address`,`register_type`)
							VALUES('$first_name','$last_name','$sex','$province_city','$email', '$passMD5', '$valid_security', 'user', '".date('Y-m-d')."', '0', '0', '$member_type', '$member_type_2', '$business_type', '$business_field', '$company_name', '$card_id', 'kh', '$post_code', '$tel', '$fax', '$mobile', '$website', '$available_services', '$introduction', '$id', '$address','m1')") ) {

							
							require_once BASE_CLASS . 'class-log.php';
							LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

							$db->close();
							$this->register_form_status = 'query';

							return;
						}else {
							
							$reg_id = mysql_insert_id();

							$query = "INSERT INTO data_count SET register_id = '".$reg_id."',
																 country = '$country',
																 car_count = '0',
																 truck_count = '0',
																 bus_count = '0',
																 part_count = '0',
																 accessories_count = '0',
																 equipment_count = '0',
																 motorbike_count = '0',
																 aircraft_count = '0',
																 watercraft_count = '0'";
							mysql_query($query);
						}
					
						header("Location: " .BASE_RELATIVE ."signup-success");
						exit;					
					}
				
			}
		}
    }

    /**
     * Private method: load country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	/*private function loadCountryList(){
        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT * FROM `country_list` ORDER BY `country_name` ASC") ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load country list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->country_list = array();

            return;
        }

        if( @mysql_num_rows($sql) < 0 ){
            $cnx->close();
            return;
        }

        $this->country_list = array();

        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->country_list, $r);

        }

        @mysql_free_result($sql);
        $cnx->close();
    }*/
	
	
	    /*
     * Protected method: handle login form submit ------------------------------
     * @return void.
     */
    protected function handleLoginSubmit()
    {
        $email = trim($_POST['emailInput']);
        $pass  = trim($_POST['passwordInput']);	
		
        if(isset($_GET['pg'])) $currentPage= urldecode($_GET['pg']); else $currentPage= BASE_RELATIVE;
        // validate.
       // require_once BASE_CLASS . 'class-utilities.php';
//
//        if( !Utilities::checkEmail($email) )
//        {
//            $this->error_status = true;
//            $this->error_type = 'error';
//            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
//            $this->error_formname = 'login';
//            return;
//        }
//
//        if( strlen($pass) < 5 || strlen($pass) > 20 )
//        {
//            $this->error_status = true;
//            $this->error_type = 'error';
//            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
//            $this->error_formname = 'login';
//            return;
//        }
        require_once BASE_CLASS . 'class-utilities.php';

       /* if( !Utilities::checkEmail($email) )
        {
            $this->error_status = true;
            $this->error_type = 'error';
            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
            $this->error_formname = 'login';
            return;
        }*/

        /*if( strlen($pass) >=4 || strlen($pass) <= 30 )
        {
            $this->error_status = true;
            $this->error_type = 'error';
            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
            $this->error_formname = 'login';
            return;
        }
		*/

        // check if email matches the super admin's.
        if( $email == ADMIN_CONTACT_EMAIL )
        {
            // validate admin password.
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);
			
			

            if( !$sql = mysql_query("SELECT `admin_email`, `admin_pass` FROM `setting` WHERE `admin_email`='$email' AND `admin_pass`='$pass'") )
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());



                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
                 $db->close();
				

            }else{
                  header("Location: " . BASE_RELATIVE . $currentPage);
                exit;
            }

            if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }
            else
            {
                // set admin session.
                $_SESSION['log_group'] = 'admin';
                $_SESSION['log_name']  = 'Administrator';
                $_SESSION['log_id']    = -1;
                $_SESSION['log_email'] = $email;

                header("Location: " . BASE_RELATIVE . $currentPage);
                exit;
            }
        }
        // user is not administrator.
        else
        {
            require_once BASE_CLASS . 'class-connect.php';

            $db = new Connect();
            $db->open();

            $pass = md5($pass);
            $email = mysql_real_escape_string($email);


            //echo $email."Test Email";
            if( !$sql = mysql_query("SELECT * FROM `register` WHERE `log_id`='$email' AND `password`='$pass' AND `activated`='1'") )
			
            {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
                
                $db->close();

                $this->error_status = true;
                $this->error_type = 'warning';
                $this->error_message = $this->lang['LOGIN_QUERY_ERROR'];
                $this->error_formname = 'login';
                return;
            }
			
			if (mysql_num_rows($sql) == 0){
				$this->invalid_log = "Invalid ID or Password";
			}
            
			if( @mysql_num_rows($sql) != 1 )
            {
                $db->close();

                $this->error_status = true;
                $this->error_type = 'error';
                $this->error_message = $this->lang['LOGIN_INVALID_EMAIL'];
                $this->error_formname = 'login';
                return;
            }

            // check if account is active and user is not banned.
            $r = mysql_fetch_assoc($sql);

            @mysql_free_result($sql);
            $db->close();

            //if( (bool)$r['banned'] )
//            {
//                $this->error_status = true;
//                $this->error_type = 'warning';
//                $this->error_message = $this->lang['LOGIN_BANNED_ACCOUNT'];
//                $this->error_formname = 'login';
//                return;
//            }
//
//            if( !(bool)$r['activated'] )
//            {
//                $this->error_status = true;
//                $this->error_type = 'warning';
//                $this->error_message = $this->lang['LOGIN_NON_ACTIVE_ACCOUNT'];
//                $this->error_formname = 'login';
//                return;
//            }
            //Check if user allow to remember the account
            if(isset($_POST['keep_login'])){
                if(empty($r['session'])){
                    if(empty($r['token'])){
                        $r['token']=strtotime("now");
                    }
                    $r['session']=md5($r['log_id']."^".$r['password']."^".$r['token']);
                    $this->updateSessionInDatabase($r['user_id'], $r['token'], $r['session']);
                }
                setcookie('ussid', $r['session'], strtotime( '+30 days' ) );
            }
            // set login session.
            $_SESSION['log_group'] = $r['group'];
			$_SESSION['log_fullname'] = $r['fullname'];
            $_SESSION['log_name']  = $r['name'];
			//$_SESSION['company_name']=$r[''];
			$_SESSION['log_id']    = $r['user_id'];
            $_SESSION['userlog_id']    = $r['log_id'];
            $_SESSION['log_email'] = $r['email'];
			$_SESSION['register_type'] = $r['register_type'];



			$db = new Connect();
            $db->open();

			$query="SELECT * FROM register_types WHERE register_types.code='".$_SESSION['register_type']."'";
			$result = mysql_query($query);						
			
			$row = mysql_fetch_array($result);

			$_SESSION['user_max_image_upload'] = $row['max_image'];
			$_SESSION['user_max_pro'] = $row['max_product'];
			//Script For Close Fancybox Pop up.
			/* echo '<script language="javascript">				
					parent.location.reload(true);			
			</script>'; */			

			
            header("Location: " .BASE_RELATIVE);
            exit;
        }
    } // End of Login submit
	
	
    //update session field in table register
    protected function updateSessionInDatabase($id, $token, $session){
        require_once BASE_CLASS . 'class-connect.php';
        $db = new Connect();
        $db->open();
        $sql="UPDATE register SET `token`='$token', `session`='$session' WHERE `user_id`='$id'";
        if(mysql_query($sql)){
            return true;
        }
       $db->close();
    }
	
    /*
     * Protected method: handle password recovery form submit ------------------
     * @return void.
     */
//    protected function handlePasswordRecoverySubmit()
//    {
//        $email = trim($_POST['femailInput']);
//
//        require_once BASE_CLASS . 'class-utilities.php';
//
//        // validate email.
//       // if( !Utilities::checkEmail($email) )
////        {
////            $this->error_status = true;
////            $this->error_type = 'error';
////            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL_RECOVER'];
////            $this->error_formname = 'recover';

////            return;
////        }
//
//        require_once BASE_CLASS . 'class-connect.php';
//
//        $db = new Connect();
//        $db->open();
//
//        // remove all older entries from recover table.
//       // if( $sql = @mysql_query("SELECT `stamp` FROM `recover_pass_request`") )
////        {
////            if( @mysql_num_rows($sql) > 0 )
////            {
////                // if stamp is older than 23 hours, remove it.
////                while( $r = @mysql_fetch_assoc($sql) )
////                {
////                    $stamp = $r['stamp'];
////
////                    if( $stamp <= strtotime('-23 hours') )
////                    {
////                        @mysql_query("DELETE FROM `recover_pass_request` WHERE `stamp`='$stamp' LIMIT 1;");
////                    }
////                }
////
////                @mysql_free_result($sql);
////            }
////        }
//       // else
////        {
////            // do not break operation, let it fail silently and log the error.
////            require_once BASE_CLASS . 'class-log.php';
////
////            LogReport::write('Unable to remove old recover entries from database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
////        }
//
//        // check if email exists on database.
//       // if( !$sql = mysql_query("SELECT `email` FROM `register` WHERE `email`='$email'") )
////        {
////            require_once BASE_CLASS . 'class-log.php';
////
////            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
////
////            $db->close();
////
////            $this->error_status = true;
////            $this->error_type = 'warning';
////            $this->error_message = $this->lang['LOGIN_QUERY_ERROR_RECOVER'];
////            $this->error_formname = 'recover';
////            return;
////        }
////
////        if( @mysql_num_rows($sql) != 1 )
////        {
////            $db->close();
////
////            $this->error_status = true;
////            $this->error_type = 'error';
////            $this->error_message = $this->lang['LOGIN_INVALID_EMAIL_RECOVER'];
////            $this->error_formname = 'recover';
////            return;
////        }
//
//        // generate a validation code which will be part of the URL sent to user.
//        $validation_code = Utilities::generateRandomString(21);
//
//        // define URL.
//        $changeURL = BASE_SITE . '/login/forgot-password/?auth=' . base64_encode($validation_code .',' . $email);
//
//        // register update request.
//       // if( !$sql = @mysql_query("INSERT INTO `recover_pass_request` (`email`,`code`, `stamp`) VALUES ('$email','$validation_code','".time()."')") )
////        {
////            require_once BASE_CLASS . 'class-log.php';
////
////            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
////
////            $db->close();
////
////            $this->error_status = true;
////            $this->error_type = 'warning';
////            $this->error_message = $this->lang['LOGIN_QUERY_ERROR_REG_RECOVER'];
////            $this->error_formname = 'recover';
////            return;
////        }
//
//        // define email message.
//        $message = $this->lang['LOGIN_EMAIL_MESSAGE'];
//        $message .= '<br><a href="'.BASE_RELATIVE.'home/forgot-password/?auth=' . base64_encode($validation_code .',' . $email) . '" target="_blank">' . $this->lang['LOGIN_RESET_LABEL'] . '</a><br>';
//
//        // send email to user.
//        require_once BASE_CLASS . 'class-sendmail.php';
//
//        $send = new SendEmail();
//        $send->setSubject(FRAMEWORK_NAME . ': ' . $this->lang['LOGIN_EMAIL_SUBJECT']);
//        $send->setMessage($message);
//        $send->setEmail($email);
//
//        if( !$send->send() )
//        {
//            // remove entry from database.
//            if( !@mysql_query("DELETE FROM `recover_pass_request` WHERE `code`='$validation_code' LIMIT 1;") )
//            {
//
//            }
//
//            $db->close();
//
//            require_once BASE_CLASS . 'class-log.php';
//
//            LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());
//
//            $db->close();
//
//            $this->error_status = true;
//            $this->error_type = 'warning';
//            $this->error_message = $this->lang['LOGIN_QUERY_ERROR_EMAIL_RECOVER'];
//            $this->error_formname = 'recover';
//            return;
//        }
//
//        $db->close();
//
//        $this->error_status = true;
//        $this->error_type = 'complete';
//        $this->error_message = $this->lang['LOGIN_EMAIL_COMPLETE'];
//        $this->error_formname = 'recover';
//        return;
//    }

    /*
     * Getter: get form status -------------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->error_status;
    }

    /*
     * Getter: get form status type --------------------------------------------
     * @return string (error | warning | complete).
     */
    public function getFormStatusType()
    {
        return $this->error_type;
    }

    /*
     * Getter: get form status message -----------------------------------------
     * @return string.
     */
    public function getStatusMessage()
    {
        return $this->error_message;
    }

    /*
     * Getter: get form name ---------------------------------------------------
     * @return string.
     */
    public function getFormName()
    {
        return $this->error_formname;
    }
	
    /**
     * Public method: get country list
     * <br>---------------------------------------------------------------------
     * @return array
     */
	public function getCountryList(){
        return $this->country_list;
    }
    /*
     * Get registration status -------------------------------------------------
     * @return string.
     */
    public function getRegistrationStatus()
    {
        return $this->register_form_status;
    }
}