var loading_html="<table class='make_table' id='make_table'><tr><th></th><th>N<sup>o</sup></th><th>Make</th><th></th></tr></table>";
loading_html+='<img id="loading_image" src="images/loading_75.gif" style="position:absolute;left:0;right:0;margin:auto;z-index:999;margin-top:20px;"/>';
$(document).ready(function($) {

	showLoading();
	$(".top_tab").each(function(e){
        if($(this).attr("value")==product_type){
            $('#admin_tab ul li').removeClass('current');
            $(this).addClass('current');
            $("#product_type").val(product_type);

        }
    });
	$("#make_table_wrapper").load("ajax/admin_saved_make_table.php?product_type="+product_type, function(e){
   		
	});



	$('#admin_tab ul li').click(function(){


        $('#admin_tab ul li').removeClass('current');
		$(this).addClass('current');

		product_type=$(this).attr("data-value");
		$("#product_type").val(product_type);
		showLoading();
        //alert(product_type);
		$("#make_table_wrapper").load("ajax/admin_saved_make_table.php?product_type="+product_type, function(e){
   		
		});
	});
	$(document.body).on('click', "#delete_selected", function(e){
        if(confirm("Are you sure want to delete the selected makes in this category?")==false) return false;
    	
    });
    $(document.body).on('click', ".edit_button", function(e){
        var current_id=$(this).attr('edit_id');
        var current_make=$("#make_"+current_id).text();

        if(!$("#new_make_"+current_id).length){
            $("#make_"+current_id).html(function(e){
                return "<input type='text' class='make_text' value='"+current_make+"' id='new_make_"+current_id+"' name='new_make["+current_id+"]'/>";
            });
        }
        return false;
    });
    $(document.body).on('change', "#countrySelect", function(e){
        showLoading();
        var currentMake=$(this).val();
        $("#make_table_wrapper").load("ajax/admin_saved_make_table.php?country="+currentMake, function(e){
            //load_ready();
        });
    });
    $(document.body).on('click', ".delete_button", function(e){
        
        $(".make_check").prop('checked', false);
        var delete_id=$(this).attr('delete_id');
        $("#make_check_"+delete_id).prop('checked', true);
        $("#delete_selected").click();
    });
});

function load_ready(){   
    
   
}

function showLoading(){    
    $("#make_table_wrapper").html(loading_html);
}