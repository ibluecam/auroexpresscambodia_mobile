<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class publicvehicledetail
{
    protected $lang;
    protected $group = 'public';
    protected $slug = 'cardetail';
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $send_message_status;
    private $vehicle_detail;
    private $gallery_list;
    private $vehicleid = 0;
    private $vehicle_detailimg;
    private $vehicle_type = '';
    private $related_post;
    private $news_list;
    private $country_list;
    var $slug_detail='';
	var $error = array();

    //Validate controll on form.
	var $error_email;
	var $error_message;
	var $no_photo;

    /*
     * Constructor -------------------------------------------------------------
     */
    public function publicvehicledetail($lang=array())
    {
        $this->lang = $lang;
        $this->loadVehicleDetailData();
        $this->LoadVehicleGallery();
		$this->loadRelatedPost();

        $this->loadVehicleDetailDataimg();


        // initialize status.
		if( isset($_POST['btn_send']) )
        {				
		   
			  $this->sendEmail();
		}

        // if order form is submit, process it.
        if( isset($_POST['submit']) )
        {
            $this->processSendMessageForm();
        }
    }

    private function loadCountry(){
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';

        $cnx = new Connect();
        $cnx->open();

        $query = "SELECT * FROM country_list ORDER BY country_name ASC";
        $result = mysql_query($query);
        if(!$result){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();

            $this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }

        $this->country_list = array();

        while( $r = @mysql_fetch_assoc($result) ){
            array_push($this->country_list, $r);
        }

        $cnx->close();
    }


    private function loadNewsList(){
        require_once BASE_CLASS . 'class-connect.php';
        require_once BASE_CLASS . 'class-utilities.php';

        $cnx = new Connect();
        $cnx->open();

        // load news.
        if( !$sql = @mysql_query("SELECT * FROM `news` ORDER BY `id` DESC LIMIT 6" ) ){
            require_once BASE_CLASS . 'class-log.php';
            $cnx->close();

            LogReport::write('Unable to load news list due a query error at ' . __FILE__ . ':' . __LINE__);

            $this->form_message = $this->lang['MANAGE_NEWS_LOAD_QUERY_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }

        $this->news_list = array();

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
            return;
        }

        ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );

        while( $r = @mysql_fetch_assoc($sql) ){
            $day = explode('-',$r['date']);
            $date = Utilities::checkDateFormat($day[0],$day[1],$day[2],$format);
            $date=date("F j, Y",strtotime($r['date']));
            $obj = array(
                'id' => $r['id'],
                'title' => stripslashes($r['title']),
                'type' => stripslashes($r['product_type']),
                'html' => stripslashes($r['html']),
                'date' => $date,
                'time' => $r['time'],
                'author' => stripslashes($r['author'])
            );
            array_push($this->news_list,$obj);
        }

        @mysql_free_result($sql);
        $cnx->close();
    }

    public function getNewsList(){
        return $this->news_list;
    }

    public function loadReserve($product_id){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        $sql_query = "SELECT * FROM product WHERE status='Reserved' AND car_id='$product_id'";

        if( !$sql = mysql_query($sql_query) ){
            die('Unable to load reserve. '. __FILE__ .':'. __LINE__);
            $cnx->close();
        }

        $cnx->close();

        return mysql_num_rows($sql);
    }

    public function loadSold($product_id){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        $sql_query = "SELECT * FROM product WHERE status='Sold' AND car_id='$product_id'";

        if( !$sql = mysql_query($sql_query) ){
            die('Unable to load sold. '. __FILE__ .':'. __LINE__);
            $cnx->close();
        }

        $cnx->close();

        return mysql_num_rows($sql);
    }

    /*public function loadNew($product_id){
        require_once BASE_ROOT . 'core/class-connect.php';
        $cnx = new Connect();
        $cnx->open();

        $sql_query = "SELECT * FROM product WHERE created_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND id='$product_id'";

        if( !$sql = mysql_query($sql_query) ){
            die('Unable to load New Car. '. __FILE__ .':'. __LINE__);
            $cnx->close();
        }

        $cnx->close();

        return mysql_num_rows($sql);
    }
*/	
    function loadDealweek(){

        require_once BASE_CLASS . 'class-connect.php';

        $cnx = new Connect();
        $cnx->open();

        $sql_search="SELECT * from deal_week  ORDER BY id DESC limit 5";

        if( !$sql = @mysql_query($sql_search) ){
            require_once BASE_CLASS . 'class-log.php';
            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);
            $cnx->close();
            return;
        }

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
        }


       $this->deal_week = array();

        while( $r = @mysql_fetch_assoc($sql)){

            array_push($this->deal_week, $r);

        }


        @mysql_free_result($sql);
        $cnx->close();
        return $this->deal_week;

    }

    public function getIpAddresses()
    {
       $ipAddresses = array();
       if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {

          $ipAddresses['proxy'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
          $ipAddresses['user'] = $_SERVER["HTTP_X_FORWARDED_FOR"];

       } else {
          $ipAddresses['user'] = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
       }
       return $ipAddresses;
    }

    public function processSendMessageForm() {
        //Custome Init
        $description = "";
        $country = "";
        $port_name = "";
        $payment_term = array();
        $payment_term_string = "";
        $price_term = array();
        $price_term_string = "";
        $row_email = array();
        $cid = "";
        $sender = "";
        $respon_id = "";
        $id = 0;
        $buyer_id = $_SESSION['log_id'];

        extract($_POST);
        extract($_GET);

        $get_ip = $this->getIpAddresses();
        $ips = $get_ip['user'];

       /*  if(!empty($_SESSION['log_email'])) {
            $sender = $_SESSION['log_email'];
        } */

        $breaks = array("\n");
        $description = str_ireplace($breaks, "<br />", $description);

        /* checks if any payment_term is checked */
        if( isset($payment_term) ) {
            /* Iterate the payment_term array and get the values */
            foreach($payment_term as $value) {
                $payment_term_string.= $value.",";
            }

            $payment_term = substr($payment_term_string, 0, -1);
        }

        // validate fields.
        // checks if any price_term is checked
        if( isset($price_term) ) {
            /* Iterate the price_term array and get the values */
            foreach($price_term as $value) {
                $price_term_string.= $value.",";
            }

            $price_term = substr($price_term_string, 0, -1);
        }

        require_once BASE_CLASS . 'class-connect.php';

        $db = new Connect();
        $db->open();

        if(!empty($sender) || $sender!="") {

            if( $sql = @mysql_query("SELECT register.email AS response_id FROM register JOIN product ON register.user_id=product.owner WHERE product.car_id='$cid'") ) {

                /* Get the replier ID */
                $row_email = mysql_fetch_array($sql);
                $respon_id = $row_email['response_id'];

                if( $sender!=$respon_id ) {
                    $row = mysql_fetch_array(mysql_query("SELECT * FROM message ORDER BY id DESC"));
                    $id = $row['id'];
                    $id++;

                    if( $sql = mysql_query("INSERT INTO `message` (`id`, `message`, `sent_id`, `response_id`, `duration`, `seen`, `product_id`, `payment_term`, `price_term`, `country`, `port_name`, `ip`, `buyer_id`) VALUES('$id', '$description', '$sender', '$respon_id', '".date('Y-m-d H:i:s')."', '0', '$cid', '$payment_term', '$price_term', '$country', '$port_name', '$ips', $buyer_id)") ) {

                        $db->close();
                        $this->send_message_status = 'complete';
                    }  else {
                        echo mysql_error();
                        $db->close();
                        $this->send_message_status = 'query';
                    }
                } else {
                    echo mysql_error();
                    $db->close();
                    $this->send_message_status = 'query';
                }
            } else {
                require_once BASE_CLASS . 'class-log.php';

                LogReport::write('Unable to query database at ' . __FILE__ . ':' . __LINE__ . '. ' . mysql_error());

                $db->close();
                $this->send_message_status = 'query';
            }
        } else {
            $db->close();
            $this->send_message_status = 'session';
        }
    }

    /*
     * Public method: get page content data ------------------------------------
     * @return array | false.
     */
    /*
     * private method: get vehicle detail from database -----------------------------------------
     * @return string.
     */

    private function loadRelatedPost(){
        require_once BASE_CLASS . 'class-connect.php';
        if(isset($_GET['owner'])){
            $this->vehicle_own =$_GET['owner'];
        }

        $cnx = new Connect();
        $cnx->open();

        $query = "SELECT p.*, c.photo_url as carImg, rg.user_id as company_id
                FROM product as p
                LEFT JOIN car_media as c on p.car_id=c.car_id AND c.primary_photo=1
				INNER JOIN `register` as rg on `p`.`owner`=`rg`.`user_id`
                WHERE p.product_type='Car' AND p.is_deleted='0' AND rg.user_id ='".$this->vehicle_own."' ORDER BY RAND() LIMIT 0,3";
        $result = mysql_query($query);

        $this->related_post = array();

        while( $r = @mysql_fetch_assoc($result) ){
            array_push($this->related_post, $r);
        }

        @mysql_free_result($result);
        $cnx->close();

    }
        private function loadVehicleDetailDataimg(){
        require_once BASE_CLASS . 'class-connect.php';
        if(isset($_GET['cid'])){
            $this->vehicleid =$_GET['cid'];
        }

        $cnx = new Connect();
        $cnx->open();

        if( !$query = @mysql_query("SELECT  car_id,`owner` FROM product
            where car_id='".$this->vehicleid."'") ){
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to load vehicle detail due a query error at ' . __FILE__ . ':' . __LINE__);

            $cnx->close();
            return;
        }
        $s=@mysql_fetch_array($query);

        if( !$sql = @mysql_query("SELECT * from more_image WHERE `owner`=".$s['1']." and primary_photo=1 ") ){
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to load vehicle detail due a query error at ' . __FILE__ . ':' . __LINE__);

            $cnx->close();
            return;
        }

        $this->vehicle_detailimg = array();



        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->vehicle_detailimg, $r);
        }
        //set page titlesdd


        @mysql_free_result($sql);
        $cnx->close();

    }
    private function loadVehicleDetailData(){
        require_once BASE_CLASS . 'class-connect.php';
        if(isset($_GET['cid'])){
            $this->vehicleid =$_GET['cid'];
        }

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT *
           
            From (product as p
            LEFT JOIN car_media as c on p.car_id=c.car_id)
            INNER JOIN country_list as cl on p.location=cl.cc
            LEFT JOIN country_list as mi on p.make=mi.cc
            INNER JOIN `register` as rg on `p`.`owner`=`rg`.`user_id`
            INNER JOIN `country_list` as cl_user on `cl_user`.`cc`=`rg`.`country`
            where p.car_id='".$this->vehicleid."'") ){
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to load vehicle detail due a query error at ' . __FILE__ . ':' . __LINE__);

            $cnx->close();
            return;
        }

        $this->vehicle_detail = array();



        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->vehicle_detail, $r);
        }
        //set page title
        if( @mysql_num_rows($sql) >0 ){
            $arr_slug=array();
            $arr_slug[]=$this->vehicle_detail['0']['car_type'];
            $arr_slug[]=$this->vehicle_detail['0']['year'];
            $arr_slug[]=$this->vehicle_detail['0']['make'];
            $arr_slug[]=$this->vehicle_detail['0']['model'];
            /* $arr_slug[]=$this->vehicle_detail['0']['part_name']; */

            $arr_slug[]="From ".$this->vehicle_detail['0']['city'];
            $arr_slug[]=$this->vehicle_detail['0']['user_country_name'];
            $arr_slug[]=", stock id: ".$this->vehicle_detail['0']['car_id'];
            if(!empty($this->vehicle_detail['0']['price'])) $arr_slug[]=$this->vehicle_detail['0']['currency']." ".number_format($this->vehicle_detail['0']['price']); else $arr_slug[]="Ask for Price";
            $explode_slug=implode(" ", $arr_slug);
            $this->slug_detail=$explode_slug;
        }

        @mysql_free_result($sql);
        $cnx->close();

    }


    private function LoadVehicleGallery(){
        require_once BASE_CLASS . 'class-connect.php';
        if(isset($_GET['cid'])){
            $this->vehicleid =$_GET['cid'];
        }

        $cnx = new Connect();
        $cnx->open();

        if( !$sql = @mysql_query("SELECT car_media.car_id, car_media.photo_id, car_media.author, car_media.type,car_media.photo_url ,car_media.thumb_url, car_media.`mode` FROM car_media WHERE car_id='".$this->vehicleid."'") ){
            require_once BASE_CLASS . 'class-log.php';

            LogReport::write('Unable to load car maker list due a query error at ' . __FILE__ . ':' . __LINE__);

            $cnx->close();
            return;
        }

        $this->gallery_list = array();

        if( @mysql_num_rows($sql) < 1 ){
            $cnx->close();
			$this->no_photo="Noimage";
       }

        while( $r = @mysql_fetch_assoc($sql) ){
            array_push($this->gallery_list, $r);
			
        }

        @mysql_free_result($sql);
        $cnx->close();

    }
	
	//public function sendEmail(){
//		$to=$_POST['email'];
//		$message=$_POST['message'];
//		$subject = 'Your Account ID';
//		$headers = "Reply-To: support@angkorauto.com\r\n";
//		
//		if(mail($to,$subject,$message,$headers)){
//			echo "Your have been sent an email...!";
//			}
//		}

    /*
     * Public method: get message status ------------------------------------------
     * @return bool.
     */
    public function getMessageStatus()
    {
        return $this->send_message_status;
    }

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
    /*
     * Public method: get vehicle data -------------------------------------------
     * @return string.
     */
    public function getVehicleDetail(){
        return $this->vehicle_detail;
    }
    public function getVehicleDetailimg(){
        return $this->vehicle_detailimg;
    }
    public function getVehicleGallery(){
        return $this->gallery_list;
    }
    public function getRelatedPost(){
        return $this->related_post;
    }
    public function getCountry_list(){
        return $this->country_list;
    }
	
	//Process sending email if user is logged in and clicked on submit button
	private function sendEmail(){
       
        require_once BASE_ROOT . 'core/class-connect.php';

        $cnx = new Connect();
        $cnx->open();

		if(isset($_GET['cid']))
			$cid=$_GET['cid'];
		$emial_to = $_POST['email'];
		$message_user=$_POST['message'];
		//$log_id=(mt_rand());
		$result="";
		$message="";
		$com_result=""; 
		$model=''; $image=''; $stock_id=""; $steering=""; $country=""; $seller=""; $price="";
		$query =mysql_query("SELECT p.*,
             c.photo_url as carImg,
             c.thumb as thumb,
             cl.country_name as country,
             mi.country_name as madein,
             cl.cc,
             `rg`.`user_id` as `company_id`,
             `rg`.`messengerID`,
             `rg`.`image`,
             
             
             `rg`.`company_name`,
             `rg`.`country` as `user_country`,
             
             `rg`.`web`,
			 `rg`.`mobile`,
			 `rg`.`email`,
             `cl_user`.`country_name` as `user_country_name`
            From (product as p
            LEFT JOIN car_media as c on p.car_id=c.car_id)
            INNER JOIN country_list as cl on p.location=cl.cc
            LEFT JOIN country_list as mi on p.made_in=mi.cc
            INNER JOIN `register` as rg on `p`.`owner`=`rg`.`user_id`
            INNER JOIN `country_list` as cl_user on `cl_user`.`cc`=`rg`.`country`
 
            where p.car_id='".$cid."'");
 
		
		$result=mysql_fetch_array($query);
		$model=$result['model_year'].' '.$result['make'].' '.$result['model'];
		$image=$result['thumb_url']; $stock_id=$result['id']; $steering=$result['steering'];
		$country=$result['country']; $seller=$result['company_name']; $price=$result['price'];
		$subject = 'My product';
 
		$headers = "From:angkorauto.com <info@angkorauto.com>\r\n";
		$headers .= "Reply-To: support@angkorauto.com\r\n";
		$headers .= "CC: support@angkorauto.com\r\n";
 
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message .= '<html><body style="margin:0 auto; padding:0 auto;">';
 
		$message .= '<div style=" width:700px; padding-bottom:10px; background:#EEE;">
			<div style="text-align:center;float:left;margin:10px 0 10px 10px;width:678px;border-bottom: 1px dashed #bbb;padding:0px 0 20px;">
				<h2 style="text-align:center;color:#3264B5">Hi [:::'.$seller.':::]!</h2>
				<p>You have received one inquiry from <u><b>[:::'.$emial_to.':::]</b></u></p>
			</div>
            <div style="float:left; width:688px; margin-bottom:20px; padding-right:10px;">
            	<div style="width:330px; float:left;">
            		<div style="width:330px; float:left; margin:10px; ">
                    	<label style=" width:60px;">Subject :</label>
                         <div style="width:260px; float:right; text-indent:8px; resize:none; padding-top:10px; border:1px solid #ccc; height:35px;">
							'.$emial_to.'
						</div>
                    </div>
                    <div style="width:330px; float:left; margin:0 10px 10px 10px; ">
                    	<label style=" width:60px;">Message :</label>
                        <div style="width:260px; float:right; border:1px solid #ccc; resize:none; text-indent:8px; height:390px;">
							'.$message_user.'
						</div>
                    </div>
                </div>
                <div style="width:315px; float:right; margin-top:12px; border:1px solid #ccc; margin-bottom:12px; padding-right:15px;">
					<div style="width:300px; color:#999; float:right; padding-bottom:10px;">
                        <div style="width:300px; margin:0 auto; padding:10px 0; font-size:22px; color:#0071ba; border-bottom:2px solid #0071ba;">
                        	'.$model.'
                    	</div>
                    	<div style="width:300px; padding-bottom:10px; margin:10px 0; border-bottom:2px solid #0071ba;">
                    		<img src="'.$image.'" width="298" />
                    	</div>
                    	<table cellspacing="0" cellpadding="0" style="color:#999;font-size:18px; font-family:Cambria; width:298px;">
                        	<tr>
                            	<td style="border-bottom:1px solid #ccc;">Stock Id: <span style="float:right;">'.$stock_id.'</span></td>
                            </tr> 
                            <tr>
                            	<td style="border-bottom:1px solid #ccc;">Steering: <span style="float:right;">'.$steering.'</span></td>
                            </tr>
                             <tr>
                            	<td style="border-bottom:1px solid #ccc;">Country: <span style="float:right;">'.$country.'</span></td>
                            </tr>
                             <tr>
                            	<td style="border-bottom:1px solid #ccc;">Seller: <span style="float:right;">'.$seller.'</span></td>
                            </tr>
                             <tr>
                            	<td style="border-bottom:1px solid #ccc;"><font color="red">FOB: USD <span style="float:right;">'.$price.'</span></font></td>
                            </tr>
                             <tr>
                            	<td style="text-align:center;"><font color="blue"><a href="angkorautodev.com/vehicle-detail?&cid='.$stock_id.'" style="text-decoration:none; color:blue;">More Detail</a></font></td>
                            </tr>  
                        </table>
                    </div>
               	</div>
                </div>
                <div style=" margin:0 0 0 10px; float:left; width:678px; border-top: 1px dashed #bbb;border-bottom: 
                1px dashed #bbb;padding:20px 0;">
                            <div style="width:50%;float:left"><a href="http://angkorauto.com">
                            <img alt="angkorauto.com" src="http://angkorauto.com/images/common/header/softbloom.png"/></a></div>
                            <div style="float:right;text-align:right;">
                                Email: info@angkorauto.com<br/>
                                <img style="margin:2px 2px;" src="http://angkorauto.com/images/social_icons/fb.png">
                                <img style="margin:2px 2px;" src="http://angkorauto.com/images/social_icons/tt.png">	
                                <img style="margin:2px 2px;" src="http://angkorauto.com/images/social_icons/gplus.png">	
                                <img style="margin:2px 2px;" src="http://angkorauto.com/images/social_icons/in.png">			
                            </div>
                </div>
                <div style="width:100%;float:left;text-align:center;padding:20px 0">
                    Copyright @ 2014 angkorauto. All Rights Reserved by Softbloom co., Ltd
                </div>
             <p style="clear:both;"></p>
		</div>';
			
		$message .= '</body></html>';
		
 
		//echo "$message";
		if(mail("info@angkorauto.com", $subject, $message, $headers)){
			//$_SESSION['log_id'] = $log_id;
			//echo '<p style="color:#900000">Please check your email, the key for reseting your password already send to your email!</p>';
			header("Location: " .BASE_RELATIVE . "success");
		}

		$cnx->close();

	}
		
}
?>
