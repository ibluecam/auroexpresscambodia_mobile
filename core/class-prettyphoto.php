<?php
/**
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
 * Manage prettyPhoto jquery module options.
 *
 * @author J. Taniguchi
 * @date 2012.09.10
 */
class PrettyPhotoManager {
    protected $animation_speed;
    protected $slideshow;
    protected $autoplay_slideshow;
    protected $opacity;
    protected $show_title;
    protected $allow_resize;
    protected $default_width;
    protected $default_height;
    protected $counter_separator;
    protected $theme;
    protected $horizontal_padding;
    protected $hide_flash;
    protected $wmode;
    protected $autoplay;
    protected $modal;
    protected $deeplinking;
    protected $overlay_gallery;
    protected $keyboard_shortcuts;
    
    /*
     * Constructor
     * initialize default values.
     * @return void.
     */
    public function PrettyPhotoManager()
    {
        $this->animation_speed      = 'fast';         /* fast/slow/normal */
        $this->slideshow            = '5000';         /* false OR interval time in ms */
        $this->autoplay_slideshow   = 'false';        /* true/false */
        $this->opacity              = '0.80';         /* Value between 0 and 1 */
        $this->show_title           = 'false';        /* true/false */
        $this->allow_resize         = 'true';         /* Resize the photos bigger than viewport. true/false */
        $this->default_width        = '500';          
        $this->default_height       = '344';          
        $this->counter_separator    = '/';            /* The separator for the gallery counter 1 "of" 2 */
        $this->theme                = 'pp_default';   /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        $this->horizontal_padding   = '20';           /* The padding on each side of the picture */
        $this->hide_flash           = 'false';        /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
        $this->wmode                = 'opaque';       /* Set the flash wmode attribute */
        $this->autoplay             = 'true';         /* Automatically start videos: True/False */
        $this->modal                = "false";        /* If set to true, only the close button will close the window */
        $this->deeplinking          = 'true';         /* Allow prettyPhoto to update the url to enable deeplinking. */
        $this->overlay_gallery      = 'true';         /* If set to true, a gallery will overlay the fullscreen image on mouse over */
        $this->keyboard_shortcuts   = 'true';         /* Set to false if you open forms inside prettyPhoto */
        
        // load properties from database.
        require_once BASE_CLASS . 'class-connect.php';
        
        $db = new Connect();
        $db->open();
        
        $sql = @mysql_query("SELECT * FROM `prettyphoto` LIMIT 1;");
        
        if( !$sql )
        {
            $db->close();
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to query database. Error thrown at ' . __FILE__ . ':' . __LINE__);
            LogReport::fatalError('Unable to load page due an internal error. Please report the problem to the system administrator.');
        }
        
        if( @mysql_num_rows($sql) == 1 )
        {
            $r = mysql_fetch_assoc($sql);
            
            @mysql_free_result($sql);
            
            $this->animation_speed      = $r['animation_speed'];
            $this->slideshow            = $r['slideshow'];
            $this->autoplay_slideshow   = $r['autoplay_slideshow'];
            $this->opacity              = $r['opacity'];
            $this->show_title           = $r['show_title'];
            $this->allow_resize         = $r['allow_resize'];
            $this->default_width        = $r['default_width'];
            $this->default_height       = $r['default_height'];
            $this->counter_separator    = $r['counter_separator'];
            $this->theme                = $r['theme'];
            $this->horizontal_padding   = $r['horizontal_padding'];
            $this->hide_flash           = $r['hide_flash'];
            $this->wmode                = $r['wmode'];
            $this->autoplay             = $r['autoplay'];
            $this->modal                = $r['modal'];
            $this->deeplinking          = $r['deeplinking'];
            $this->overlay_gallery      = $r['overlay_gallery'];
            $this->keyboard_shortcuts   = $r['keyboard_shortcuts'];
        }
        
        $db->close();
    }
    
    /*
     * Public getter: get keyboard shortcut status
     * set to false if you are handling forms with prettyphoto.
     * @return bool.
     */
    public function getKeyboardShortcut()
    {
        return $this->keyboard_shortcuts;
    }
    
    /*
     * Public getter: get overlay gallery status
     * if true, a gallery will overlay the fullscreen image on mouse over.
     * @return bool.
     */
    public function getOverlayGallery()
    {
        return $this->overlay_gallery;
    }
    
    /*
     * Public getter: get deep linking status
     * allows prettyphoto to update the url to enable deeplinking.
     * @return bool.
     */
    public function getDeeplinking()
    {
        return $this->deeplinking;
    }
    
    /*
     * Public getter: get modal mode
     * if set to true, only the close button will close the window.
     * @return bool.
     */
    public function getModal()
    {
        return $this->modal;
    }
    
    /*
     * Public getter: get autoplay status
     * automatically starts videos.
     * @return bool.
     */
    public function getAutoStart()
    {
        return $this->autoplay;
    }
    
    /*
     * Public getter: get wmode
     * @return string.
     */
    public function getWMode()
    {
        return $this->wmode;
    }
    
    /*
     * Public getter: get hide flash status
     * hides all the flash objects on a page, set to TRUE if flash appears over prettyPhoto
     * @return bool.
     */
    public function getHideFlash()
    {
        return $this->hide_flash;
    }
    
    /*
     * Public getter: get horizontal padding
     * the padding on each side of the picture.
     * @return int.
     */
    public function getHorizontalPadding()
    {
        return $this->horizontal_padding;
    }
    
    /*
     * Public getter: theme
     * @return string.
     */
    public function getThemeName()
    {
        return $this->theme;
    }
    
    /*
     * Public getter: get counter separator string
     * separator for the gallery counter. ie.: 1 / 2
     * @return string.
     */
    public function getCounterSeparator()
    {
        return $this->counter_separator;
    }
    
    /*
     * Public getter: get dafault height
     * @return int.
     */
    public function getDefaultHeight()
    {
        return $this->default_height;
    }
    
    /*
     * Public getter: default width.
     * @return int.
     */
    public function getDefaultWidth()
    {
        return $this->default_width;
    }
    
    /*
     * Public getter: allow image resize
     * resize the photos bigger than the viewport.
     * @return bool.
     */
    public function getAllowResize()
    {
        return $this->allow_resize;
    }
    
    /*
     * Public getter: show foot title.
     * @return bool.
     */
    public function getShowTitle()
    {
        return $this->show_title;
    }
    
    /*
     * Public getter: opacity
     * @return float.
     */
    public function getOpacity()
    {
        return $this->opacity;
    }
    
    /*
     * Public getter: auto-play slideshow enabled.
     * @return bool
     */
    public function getAutoPlaySlideShow()
    {
        return $this->autoplay_slideshow;
    }
    
    /*
     * Public getter: enable slideshow.
     * returns the interval's in MS or false (int 0) for disabled.
     * @return int
     */
    public function getSlideShow()
    {
        return $this->slideshow;
    }
    
    /*
     * Public getter: animation speed
     * defines the speed of the transiction animation.
     * @return int.
     */
    public function getAnimationSpeed()
    {
        return $this->animation_speed;
    }
}
