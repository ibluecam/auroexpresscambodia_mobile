<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.1.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

class adminhome 
{
    protected $lang;
    protected $form_status;
    protected $form_message;
    protected $form_style;
    protected $reg_users        = 0;
    protected $inbox            = 0;
    protected $fw_log           = 0;
    protected $total_vehicles   = 0;

    /*
     * Constructor -------------------------------------------------------------
     */
    public function adminhome($lang=array())
    {
        $this->lang = $lang;
        
        // handle VIN validation.
        if( isset($_POST['vinbtn']) ){
            $this->handleVIN();
        }
        
        // handle remove temp files.
        if( isset($_POST['tmpbtn']) ){
            $this->removeTempFiles();
        }
        
        // handle Edmunds API registration.
        if( isset($_POST['regApiBtn']) ){
            $this->registerAPIHandler();
        }
        
        // handle VIN request.
        if( isset($_POST['vinDoBtn']) ){
            $this->getVINAPI();
        }
        
        // load notification values.
        $this->loadNotificationValues();
    }
    
    /**
     * Private method: get vehicle data by vin form handler
     * @return array
     */
    private function getVINAPI(){
        ( isset($_POST['_apiHid']) ? $api = trim($_POST['_apiHid']) : $api = '' );
        ( isset($_POST['vinNumberInput']) ? $vin = trim($_POST['vinNumberInput']) : $vin = '' );
        
        if( empty($api) || strlen($vin) != 17 ){
            $this->form_message = $this->lang['ADMIN_HOME_INVALID_VIN_ERROR'];
            $this->form_status  = true;
            $this->form_style   = 'alert-error';
            return;
        }
        
        
        // get vehicle data by get_content (because of xdomain problems, I'm not using JS).
        if( !$data = @file_get_contents('http://api.edmunds.com/v1/api/toolsrepository/vindecoder?vin='.$vin.'&fmt=json&api_key=' . $api) ){
            $this->form_message = $this->lang['ADMIN_HOME_JSON_REQUEST_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // decode json.
        $data = json_decode($data, true);
        
        
        // set styleId
        $style_id = $data['styleHolder'][0]['id'];
        
        // set car table array data.
        $car_table = array(
            'maker'         => $data['styleHolder'][0]['makeName'],            // Ford
            'model'         => $data['styleHolder'][0]['modelName'],           // Fiesta
            'year'          => $data['styleHolder'][0]['year'],                // 2013
            'transmission'  => $data['styleHolder'][0]['transmissionType'],    // automatic
            'engine_size'   => $data['styleHolder'][0]['engineSize'],          // 2.0
            'price_base'    => $data['styleHolder'][0]['price']['baseMSRP'],             
            'price_invoice' => $data['styleHolder'][0]['price']['baseInvoice'],
            'trim'          => $data['styleHolder'][0]['trim']['name'],
            'body_type'     => $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['PRIMARY_BODY_TYPE']['value'],
            'doors'         => $data['styleHolder'][0]['attributeGroups']['DOORS']['attributes']['NUMBER_OF_DOORS']['value'],
            'gear'          => $data['styleHolder'][0]['squishVins'][0]['squishVinTransmissions'][0]['numberOfSpeeds'],
            'type'          => $data['styleHolder'][0]['attributeGroups']['MAIN']['attributes']['NAME']['value'],
            'fuel'          => $data['styleHolder'][0]['engineFuelType']
        );
        
        // set html content.
        # engine type.
        $html_content  = '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= $this->lang['ADMIN_HOME_ENGINE_TYPE_LABEL'].'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= $data['styleHolder'][0]['engineType'].'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        # cylinders.
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= $this->lang['ADMIN_HOME_CYLINDER_LABEL'].'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= $data['styleHolder'][0]['engineCylinder'].'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        # attributes.
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ',$data['styleHolder'][0]['attributeGroups']['MAIN']['attributes']['NAME']['name']) .'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['MAIN']['attributes']['NAME']['value']).'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['AIR_CONDITIONING']['attributes']['FRONT_AIR_CONDITIONING']['name']) .'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['AIR_CONDITIONING']['attributes']['FRONT_AIR_CONDITIONING']['value']).'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['CARGO_DIMENSIONS']['attributes']['MAX_CARGO_CAPACITY']['name']) .'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['CARGO_DIMENSIONS']['attributes']['MAX_CARGO_CAPACITY']['value']).'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['EPA_CLASS']['name']) .'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['EPA_CLASS']['value']).'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['TMV_CATEGORY']['name']) .'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['TMV_CATEGORY']['value']).'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['WHERE_BUILT']['name']) .'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['WHERE_BUILT']['value']).'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        $html_content .= '<div style="overflow:hidden;">' . "\r\n";
        $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['VEHICLE_STYLE']['name']) .'</p>' . "\r\n";
        $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
        $html_content .= str_replace('_',' ', $data['styleHolder'][0]['attributeGroups']['STYLE_INFO']['attributes']['VEHICLE_STYLE']['value']).'</p>' . "\r\n";
        $html_content .= '<div class="clearfix"></div>' . "\r\n";
        $html_content .= '</div>' . "\r\n";
        $html_content .= '<h3>' . $this->lang['ADMIN_HOME_FEATURES_LABEL'] . '</h3>' . "\r\n";        
        # features.
        $features_table = array();
        $feat = (array)$data['styleHolder'][0]['attributeGroups']['NCI_STANDARD_FACET']['attributes'];
        foreach($feat as $k => $v ){
            $fitem = explode(':',$k);
            
            if( count($fitem) < 2 ){
                // has not attribute.
                $item = str_replace('_',' ', $fitem[0]);
                $item = trim($item);
                $html_content .= '<div style="overflow:hidden;">' . "\r\n";
                $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
                $html_content .= $item .'</p>' . "\r\n";
                $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
                $html_content .= 'Ok</p>' . "\r\n";
                $html_content .= '<div class="clearfix"></div>' . "\r\n";
                $html_content .= '</div>' . "\r\n";
                array_push($features_table, strtolower($item));
            }
            else {
                // has attribute.
                $label = str_replace('_',' ', $fitem[0]);
                $label = trim($label);
                $item = str_replace('_',' ', $fitem[1]);
                $item = trim($item);
                
                $html_content .= '<div style="overflow:hidden;">' . "\r\n";
                $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
                $html_content .= $label .'</p>' . "\r\n";
                $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
                $html_content .= $item . '</p>' . "\r\n";
                $html_content .= '<div class="clearfix"></div>' . "\r\n";
                $html_content .= '</div>' . "\r\n";
                array_push($features_table, strtolower($item));
            }
        }
        # specifications.        
        $html_content .= '<h3>' . $this->lang['ADMIN_HOME_SPECIFICATION_LABEL'] . '</h3>' . "\r\n";
        $spec = $data['styleHolder'][0]['attributeGroups']['SPECIFICATIONS']['attributes'];
        foreach( $spec as $k => $v ){
            $name = trim($v['name']);
            $name = str_replace('_', ' ', $name);
            $value = trim($v['value']);
            
            $html_content .= '<div style="overflow:hidden;">' . "\r\n";
            $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
            $html_content .= $name .'</p>' . "\r\n";
            $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
            $html_content .= $value . '</p>' . "\r\n";
            $html_content .= '<div class="clearfix"></div>' . "\r\n";
            $html_content .= '</div>' . "\r\n";
        }
        # security.
        $html_content .= '<h3>' . $this->lang['ADMIN_HOME_SECURITY_LABEL'] . '</h3>' . "\r\n";
        $sec = $data['styleHolder'][0]['attributeGroups']['SECURITY']['attributes'];
        foreach( $sec as $k => $v ){
            $name = trim($v['value']);
            $name = str_replace('_', ' ', $name);
            
            $html_content .= '<div style="overflow:hidden;">' . "\r\n";
            $html_content .= '  <p style="display:block; width:250px; overflow:hidden; font-weight:bold; float: left; height:30px; line-height:30px;">';
            $html_content .= $name .'</p>' . "\r\n";
            $html_content .= '  <p style="display:block; overflow:hidden; float: left; height:30px; line-height:30px;">';
            $html_content .= 'Ok</p>' . "\r\n";
            $html_content .= '<div class="clearfix"></div>' . "\r\n";
            $html_content .= '</div>' . "\r\n";
        }
        
        // get media.
        $media_path = 'http://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId='.$style_id.'&fmt=json&api_key=' . $api;
        $media_table = array();
        $interior = array();
        $exterior = array();
        
        if( !$media = @file_get_contents($media_path) ){
            // do nothing. api didn't responded.
        } else {
            $media = json_decode($media);
            
            for( $i=0; $i < count($media); $i++ ){
                // get interior pics.
                if( $media[$i]->subType == 'interior' ){
                    $asset = (array)$media[$i]->photoSrcs;
                    for( $a=0; $a < count($asset); $a++ ){
                        list($width) = getimagesize('http://media.ed.edmunds-media.com' . $asset[$a]);
                    
                        if( $width > 480 ){                    
                            array_push($interior, 'http://media.ed.edmunds-media.com' . $asset[$a]);
                        }
                    }
                }
                
                // get exterior pics.
                if( $media[$i]->subType == 'exterior' ){
                    $asset = (array)$media[$i]->photoSrcs;
                    for( $a=0; $a < count($asset); $a++ ){
                        list($width) = getimagesize('http://media.ed.edmunds-media.com' . $asset[$a]);
                        
                        if( $width > 480 ){
                            array_push($exterior, 'http://media.ed.edmunds-media.com' . $asset[$a]);
                        }
                    }
                }
            }            
        }
        
        $media_table = array(
            'interior' => $interior,
            'exterior' => $exterior
        );
        
        $car_table['html'] = $html_content;
              
        // add to session before redirecting to the add new vehicle page.
        $_SESSION['VIN_DATA']['CAR_TABLE'] = $car_table;
        $_SESSION['VIN_DATA']['CAR_MEDIA'] = $media_table;
        $_SESSION['VIN_DATA']['CAR_FEATURE'] = $features_table;
        $_SESSION['VIN_DATA']['VIN'] = $vin;
        
        // redirect to add vehicle page.
        header("Location: " . BASE_RELATIVE . 'add-vehicle-by-vin/');
    }
    
    /**
     * Private method: save edmunds api
     * @return void
     */
    private function registerAPIHandler(){
        ( isset($_POST['apiInput']) ? $api = trim($_POST['apiInput']) : $api = '' );
        
        if( strlen($api) < 10 ){
            $this->form_message = $this->lang['ADMIN_HOME_API_INVALID'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        // save API number.
        $content = '<?php' . "\r\n";
        $content .= '$_EDMUNDS_API_KEY = \''.$api.'\';' . "\r\n";
        
        $path = BASE_ROOT . 'app/edmunds/config.php';
        
        if( !$fo = @fopen($path,'w') ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save Edmunds API key. Write permission denied at ' . __FILE__ . ':' . __LINE__);
            
            $this->form_message = $this->lang['ADMIN_HOME_API_SAVE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        if( !@fwrite($fo, $content) ){
            require_once BASE_CLASS . 'class-log.php';
            
            LogReport::write('Unable to save Edmunds API key. Write permission denied at ' . __FILE__ . ':' . __LINE__);
            
            @fclose($fo);
            $this->form_message = $this->lang['ADMIN_HOME_API_SAVE_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-warning';
            return;
        }
        
        @fclose($fo);
        
        $this->form_message = $this->lang['ADMIN_HOME_API_SAVE_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
        
        return;
    }
    
    /**
     * Private method: remove temporary files [/tmp]
     * <br>---------------------------------------------------------------------
     * @return void.
     */
    private function removeTempFiles(){
        $path = BASE_ROOT . 'tmp/';
        
        require_once BASE_CLASS . 'class-utilities.php';
        
        Utilities::remove_directory($path,false);
        
        // recreate htaccess.
        if( $fo = @fopen($path.'.htaccess','w') ){
            @fwrite($fo,'Options -Indexes');
            @fclose($fo);
        }
        
        $this->form_message = $this->lang['ADMIN_HOME_CLEAR_SUCCESS'];
        $this->form_status = true;
        $this->form_style = 'alert-success';
    }
    
    /**
     * Private method: handle VIN validation
     * <br>---------------------------------------------------------------------
     * @return void
     */
    private function handleVIN(){
        ( isset($_POST['_vinInput']) ? $vin = trim($_POST['_vinInput']) : $vin = '' );
        
        // validate.
        if( empty($vin) ){
            $this->form_message = $this->lang['ADMIN_HOME_VIN_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        }
        
        require_once BASE_CLASS . 'class-vin-checker.php';
        
        $vc = new VINChecker();
        
        $result = array();
        $result = $vc->checksum($vin);
        
        if( !$result['status'] ){
            $this->form_message = $this->lang['ADMIN_HOME_VIN_ERROR'];
            $this->form_status = true;
            $this->form_style = 'alert-error';
            return;
        } else {
            $this->form_message = $this->lang['ADMIN_HOME_VIN_SUCCESS'];
            $this->form_status = true;
            $this->form_style = 'alert-success';
            return;
        }
    }
    
    /*
     * Private method: load notification values --------------------------------
     * @return void.
     */
    private function loadNotificationValues()
    {
        require_once BASE_ROOT . 'core/class-connect.php';
        
        $cnx = new Connect();
        $cnx->open();
        
        // registered active users.
        if( $sql = @mysql_query("SELECT `group`,`activated` FROM `register` WHERE `group`='user' AND `activated`='1'") )
        {
            $this->reg_users = @mysql_num_rows($sql);
        }
        
        // inbox.
        if( $sql = @mysql_query("SELECT `read` FROM `contact` WHERE `read`='0'") )
        {
            $this->inbox = @mysql_num_rows($sql);
        }
        
        // vehicles in inventory.
        if( $sql = @mysql_query("SELECT `id` FROM `car`") ){
            $this->total_vehicles = @mysql_num_rows($sql);
        }
        
        // framework log entries.
        $fwdir = BASE_ROOT . 'log/';
        
        if( $dir = @opendir($fwdir) )
        {
            while( ($file = @readdir($dir)) !== FALSE )
            {
                if( $file != '.htaccess' && $file != 'index.php' && $file != '..' && $file != '.' )
                {
                    $this->fw_log += 1;
                }
            }
            @closedir($dir);
        }
    }
    
    /**
     * Public method: get page rank.
     * <br>---------------------------------------------------------------------
     * @return string
     */
    public function getPageRank(){
        require_once BASE_ROOT . 'core/class-pagerank.php';
        
        $domain = str_replace('http://', '', BASE_SITE);
        $domain = str_replace('https://', '', $domain);
        
        $check = new GooglePageRankChecker;
        $pr = $check -> getRank($domain);
        $result = $pr;
        return $result;
    }
    
    /**
     * Public method: get total vehicles registered in inventory.
     * <br>---------------------------------------------------------------------
     * @return int.
     */
    public function getTotalVehiclesInInventory(){
        return $this->total_vehicles;
    }
    
    /*
     * Public method: get framework log ----------------------------------------
     * @return int.
     */
    public function getFWLog()
    {
        return $this->fw_log;
    }
    
    /*
     * Public method: get inbox ------------------------------------------------
     * @return int.
     */
    public function getInbox()
    {
        return $this->inbox;
    }
    
    /*
     * Public method: get registered users -------------------------------------
     * @return int.
     */
    public function getRegisteredUsers()
    {
        return $this->reg_users;
    }
    
    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }
    
    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }
    
    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }
}
?>
