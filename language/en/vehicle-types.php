<?php
/**
 * IMPORTANT
 * 
 * The list below defines the car's body type which can be translated to your 
 * language. DO NOT CHANGE THE ID ORDER OF EACH ROW!!!!! IF YOU DO, YOU WILL
 * SCREW IT UP!
 * 
 * ID(never change this) => body type name(you can change this)
 * 
 * If you need more types, just add another row. IE. '31' => 'New type'
 */
$_CAR_BODY = array(
    '0' => 'Undefined',
    '1' => 'Convertible',
    '2' => 'SUV',
    '3' => 'Coupe',
    '4' => 'Pick Up',
    '5' => 'Hatchback',
    '6' => 'Van/MiniVan',
    '7' => 'Sedan',
    '8' => 'Wagon'
    
);
