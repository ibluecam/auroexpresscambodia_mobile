<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';

// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();

$news = $_CLASS->getNews();
?>
<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/upgrade_member/upgrade_member.css" />

<div id="content" style="height:500px;">
	<div id="request_wrap">
        <div id="request_pic">
            <img src="<?php echo BASE_RELATIVE;?>images/sent-success/upgrade_member.png" />
        </div>
        <div id="request_text">
            <p><span>Request successed !</span><br/>
                <span class="imp">*</span>You will receive an email & a call from our staff after sending this form.
            </p>
        </div>
    </div>
</div><!-- end div id ="content"-->
