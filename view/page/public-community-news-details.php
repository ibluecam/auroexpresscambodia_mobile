<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';
// load page content.
$pg_details = $_CLASS->getPageHTML();
$listnews = $_CLASS->getNewsList();
?>


<link rel="stylesheet" href="<?php echo BASE_RELATIVE;?>css/community/community.css" />
            <div id="sectionContenWrapper">
            	<!--sectionSidebar--> 
                <div id="topMenu">
                	<p><a href="#" class="linkfade">Community</a> > News</p>
                </div>
            	<?php include("php/sidebar/community.php");?>
                
            </div>           
            <div id="sectionContent"> 
            	<div id="community_title">
                	<p>News Details</p>
                </div>
                <div id="community_view" class="clearfix"> 
                        <?php
                            foreach($listnews as $new){
                                ( $_SESSION['log_language_iso'] == 'en' ? $format = 'yyyymmdd' : $format = 'ddmmyyyy' );
                        ?>
                    <div id="detail">
                        <p class="title">
                       Title: <?php echo '['.$new['type'].'] '.$new['title'];?> 
                        </p>
                        <p>Written by : <span class="writer"><?php echo $new['author']?></span>  Date : <?php echo date("F. j. Y",strtotime($new['date']));?>   <span> </span></p>
                    </div>
                    <div id="img_wrap" class="clearfix">
                        <div id="text_editor" style="width:100%;height:100%;word-wrap:break-word;border-width:3px;border-style:solid;border-color:#d1d5fa;padding:10px;border-radius:5px;overflow:auto;">
                             <?php echo $new['html'];?> 
                        </div>                      
                    </div>          
                        <?php
                            }
                        ?>
                    </ul>
                </div>
                
                <div id="pagination">
                	 
                </div>
            </div><!-- end div id="sectionContent"-->
        </div><!-- end div id="sectionContentWraper" -->   

  
  
  
  
  
  