<?php
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
$url = trim($_POST['url']);
$cid = trim($_POST['carid']);

// validate.
if( empty($url) || empty($cid) ){
    echo 'ir,Error: Insuficient data.';
    exit;
}

require_once dirname(dirname(__FILE__)) . '/config.php';
require_once BASE_CLASS . 'class-connect.php';

$cnx = new Connect();
$cnx->open();

$url = @mysql_real_escape_string($url);
$cid = @mysql_real_escape_string($cid);

if( !@mysql_query("INSERT INTO `car_media` (
                                            `car_id`,
                                            `type`,
                                            `mode`,
                                            `source`
                                            ) VALUES (
                                            '$cid',
                                            'video',
                                            'video',
                                            '$url'
                                            )") ){
    require_once BASE_CLASS . 'class-log.php';
    $cnx->close();
    LogReport::write('Unable to save car video url to table car_media due a query error at ' . __FILE__ . ':' . __LINE__);
    die('ir,Error:Internal error.');
}

$cnx->close();
die('ok,' . $url);