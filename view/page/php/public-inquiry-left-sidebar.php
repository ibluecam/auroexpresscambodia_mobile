<div class="left-sidebar-wrapper">
    <div class="nav-wrapper"><a href="#">Register Item</a> &gt; <span>Buyer's Inquiry</span></div>
    <div class="left-menu-wrapper">
        <ul class="left-menu">
            <li><a href="<?php echo BASE_RELATIVE;?>buyerinquiry">Buyer's Inquiry</a></li>
            <li><a href="<?php echo BASE_RELATIVE;?>write-inquiry">Write an Inquiry</a></li>
            
        </ul>
    </div>
    <div class="left-info-wrapper">
        <div class="info-title"><div class="label">Expert Company</div><a href="#"><img class="btnMore" src="images/home/moreBtn.png" /></a></div>
        <div class="info-body">
        	<ul>
        	<?php foreach($expert_company as $row){?>
                <li>
                    <div class="info-box">
                        <a href="company-detail?id=<?php echo $row['id'];?>">                  
                            <?php                                    
                                if((!file_exists('upload/'.$row['image'])) || ($row['image'] == "")){                                                                          
                            ?>
                                <img src="<?php echo BASE_RELATIVE;?>images/noimg.jpg" width="188" height="124" />
                            <?php                                           
                                } else{
                                
                            ?>
                                <img src="<?php echo BASE_RELATIVE.'upload/'.$row['image'];?>" width="188" height="124" />
                                           
                            <?php
                                }                                            
                                        
                            ?>
                        </a>
                         <p class="info-name"><a href="company-detail?id=<?php echo $row['id'];?>"><?php echo $row['company_name'];?></a></p>
                        <p class="info-detail"><?php echo substr($row['introduction'], 0, 15);?></p>
                    </div>
                </li>
            <?php }?>
            </ul>
        </div>
        
    </div>
</div>