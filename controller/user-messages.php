<?php

if(!isset($_SESSION)) @session_start();
################################################################################
### This disclaimer must be kept intact in order to use this product.        ###
################################################################################
### Project: jT CarFramework [http://intersofts.com]
### Author: J. Toshio Taniguchi
### Since: 27.04.2011
### Version: 1.0.0
### Copyrights: J. Toshio Taniguchi
### Contact: j.taniguchi@taniguchi-blog.com
################################################################################
### CONTRIBUTORS
### - none
################################################################################
class usermessages
{
    protected $lang;
    protected $group = 'user';
    protected $slug = 'message';
    protected $form_status;
    protected $form_message;
    protected $form_style;
	protected $delete_form_message;
    protected $restore_form_message;
    private $outbox_pagination;
    private $inbox_pagination;
    private $trash_pagination;
    private $num_msg_per_page;
    private $box_flag;
    /*
     * Constructor -------------------------------------------------------------
     */
    public function usermessages($lang=array())
    {
        $this->lang = $lang;
        // $this->num_msg_per_page = 20;
        // $this->box_flag = isset($_GET['box_flag']) ? $_GET['box_flag'] : 0;

        // if delete form is submit, process it.
        // if( array_key_exists('inbox_submit', $_POST) )
        // {
        //     $this->deleteFromInbox();
        //     $this->box_flag = 0;
        // }

        // if( array_key_exists('outbox_submit', $_POST) )
        // {
        //     $this->deleteFromOutbox();
        //     $this->box_flag = 1;
        // }

        // if( array_key_exists('trash_submit', $_POST))
        // {
        //     $this->deleteFromTrash();
        //     $this->box_flag = 2;
        // }

        // if(array_key_exists('restore_msg_submit', $_POST))
        // {
        //     $this->restoreMessage();
        //     $this->box_flag = 2;
        // }

    }


    /*
     * Public method: delete inbox message ------------------------------------
     */
	

    /*
     * Public method: get form status ------------------------------------------
     * @return bool.
     */
    public function getFormStatus()
    {
        return $this->form_status;
    }

    /*
     * Public method: get form message -----------------------------------------
     * @return string.
     */
    public function getFormMessage()
    {
        return $this->form_message;
    }

    /*
     * Public method: get form style -------------------------------------------
     * @return string.
     */
    public function getFormStyle()
    {
        return $this->form_style;
    }

    

}
?>
