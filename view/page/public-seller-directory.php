<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = false;
$_CKEDITOR_ID = '';
// get form feed.
$fstatus = $_CLASS->getFormStatus();
$fmessage = $_CLASS->getFormMessage();
$fstyle = $_CLASS->getFormStyle();
$group = 'public';
$slug  = 'car';

// load page content.
$pg_details = $_CLASS->getPageHTML();
$deal_week = $_CLASS->loadDealweek();


if(isset($_GET['country'])) $country=htmlspecialchars(stripcslashes($_GET['country'])); else $country="";
if(isset($_GET['business_type'])) $business_type=htmlspecialchars(stripcslashes($_GET['business_type'])); else $business_type="";
if(isset($_GET['product_type'])) $product_type=htmlspecialchars(stripcslashes($_GET['product_type'])); else $product_type="";
if(isset($_GET['condition'])) $condition=htmlspecialchars(stripcslashes($_GET['condition'])); else $condition="";
if(isset($_GET['steering'])) $steering=htmlspecialchars(stripcslashes($_GET['steering'])); else $steering="";
if(isset($_GET['fuel_type'])) $fuel_type=htmlspecialchars(stripcslashes($_GET['fuel_type'])); else $fuel_type="";
if(isset($_GET['min_price'])) $min_price=htmlspecialchars(stripcslashes($_GET['min_price'])); else $min_price="";
if(isset($_GET['max_price'])) $max_price=htmlspecialchars(stripcslashes($_GET['max_price'])); else $max_price="";
if(isset($_GET['make'])) $make=htmlspecialchars(stripcslashes($_GET['make'])); else $make="";
if(isset($_GET['category'])) $category=htmlspecialchars(stripcslashes($_GET['category'])); else $category="";


$traderList= $_CLASS->getTraderList();
$pagination=$_CLASS->pagination_html;
$total_row=$_CLASS->total_num_row;
$current_page=$_CLASS->current_page;
$total_page=$_CLASS->total_page;
$countryNumber=$_CLASS->getSellerNumberByCountry();
$traderNumber=$_CLASS->getSellerNumberLike('business_type', 'Trader');
$manuNumber=$_CLASS->getSellerNumberLike('business_type', 'Manufacturer');
$numberByProductType = $_CLASS->getNumberByProductType();
$numberByCondition = $_CLASS->getNumberByProduct("condition");
$numberBySteering = $_CLASS->getNumberByProduct("steering");
$numberByFuelType = $_CLASS->getNumberByProduct("fuel_type");

$numberAllSeller=$_CLASS->getAllSellerNumber();
$numberByMaxPrice['3000']=$_CLASS->getNumberByPrice('3000', '<');
$numberByMaxPrice['5000']=$_CLASS->getNumberByPrice('5000', '<');
$numberByMaxPrice['10000']=$_CLASS->getNumberByPrice('10000', '<');
$numberByMinPrice['15000']=$_CLASS->getNumberByPrice('15000', '>');
$numberByMake=$_CLASS->getNumberByProduct('make');
$numberByCategory=$_CLASS->getNumberByProduct('category');
if(isset($_GET['product_type'])) $product_type=htmlspecialchars(stripcslashes($_GET['product_type'])); else $product_type="";

?>

<div id="sectionContenWrapper">
        	<div id="carMenu">
            	<ul class="clearfix">
                	<li class="first_child"><a href="#" class="linkfade">Find Trader</a> > </li>
                    <li class="menu">
                    	Sell's Directory
                  </li>
                </ul>
            </div>
        	<!--sectionSidebar--> 
            
            <?php 				
				include("php/sidebar/findtrader.php");	 	
				include("php/sidebar/bottom.php");	 				 						 			
			?>    

        	<!--end sectionSidebar-->
            
            <div id="sectionContent" class="clearfix">            	
            	
                <div class="findtrader_title">
                    <p>Seller's Directory</p>                	
                </div>       
                    <p class="detail">
                    The total of 8,987 sellers have been currently registered at Global iBlue.<br/>
sellers who are on the list of seller's directory  are Global iBlue's premium memebers as well as the experts in import & export business.<br/>
Please search by categories and find out the best sutiable supplier for your business here!
                    </p>             
            	

				<div id="tab_wrap" class="clearfix">

                	<table>
                        <tr>
                        	<td class="all getClass<?php if($product_type=='') echo ' all_current';?>" title="all">
                                <a href="seller-directory">
                                    <?php 
                                       
                                        echo "All (".$numberAllSeller.")";
                                    ?>
                                </a>
                            </td>
                            <td class="car getClass<?php if($product_type=='Car') echo ' car_current';?>" title="car">
                                <a href="seller-directory?product_type=Car">Car (<?php if(isset($numberByProductType['Car'])) echo $numberByProductType['Car'];  else echo "0";?>)</a>
                            </td>
                            <td class="truck getClass<?php if($product_type=='Truck') echo ' truck_current';?>" title="truck">
                                <a href="seller-directory?product_type=Truck">Truck (<?php if(isset($numberByProductType['Truck'])) echo $numberByProductType['Truck'];  else echo "0";?>)</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bus getClass<?php if($product_type=='Bus') echo ' bus_current';?>" title="bus">
                                <a href="seller-directory?product_type=Bus">Bus (<?php if(isset($numberByProductType['Bus'])) echo $numberByProductType['Bus'];  else echo "0";?>)</a>
                            </td>
                            <td class="equipment getClass<?php if($product_type=='Equipment') echo ' equipment_current';?>" title="equipment">
                                <a href="seller-directory?product_type=Equipment">Heavy Machine (<?php if(isset($numberByProductType['Equipment'])) echo $numberByProductType['Equipment'];  else echo "0";?>)</a>
                            </td>
                            <td class="part getClass<?php if($product_type=='Part') echo ' part_current';?>" title="part">
                                <a href="seller-directory?product_type=Part">Part (<?php if(isset($numberByProductType['Part'])) echo $numberByProductType['Part'];  else echo "0";?>)</a>
                            </td>
                        </tr>    
                    </table>
                </div>             
           		<div id="tab_all" <?php if($product_type=='') echo "style='display:block;'"; else echo "style='display:none;'";?>>
                	<table border="0" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td class="label">Country</td>
                            <td class="list" colspan="3">
                            	<ul>
                                	<li><img src="<?php echo BASE_RELATIVE;?>images/flag/kr.png" /> <a <?php if($country=='kr') echo "class='selected_filter'";?> href="seller-directory?country=kr">S. Korea (<?php if(isset($countryNumber['kr'])) echo $countryNumber['kr']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/jp.png" /> <a <?php if($country=='jp') echo "class='selected_filter'";?> href="seller-directory?country=jp">Japan (<?php if(isset($countryNumber['jp'])) echo $countryNumber['jp']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/us.png" /> <a <?php if($country=='us') echo "class='selected_filter'";?> href="seller-directory?country=us">U.S.A (<?php if(isset($countryNumber['us'])) echo $countryNumber['us']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/tr.png" /> <a <?php if($country=='tr') echo "class='selected_filter'";?> href="seller-directory?country=tr">Turkey (<?php if(isset($countryNumber['tr'])) echo $countryNumber['tr'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cl.png" /> <a <?php if($country=='cl') echo "class='selected_filter'";?> href="seller-directory?country=cl">Chile (<?php if(isset($countryNumber['cl'])) echo $countryNumber['cl'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cn.png" /> <a <?php if($country=='cn') echo "class='selected_filter'";?> href="seller-directory?country=cn">China(<?php if(isset($countryNumber['cn'])) echo $countryNumber['cn'];  else echo "0";?>)</a></li>
                                    
                                </ul>
                            </td>
                        </tr>
                        <tr>
                        	<td class="label">Business Type</td>
                            <td class="mid_child"><a <?php if($business_type=='Trader') echo "class='selected_filter'";?> href="seller-directory?business_type=Trader">Trader (<?php echo $traderNumber;?>)</a></td>
                            <td class="mid_child"><a <?php if($business_type=='Manufacturer') echo "class='selected_filter'";?> href="seller-directory?business_type=Manufacturer">Manufacturer (<?php echo $manuNumber;?>)</a></td>
                            <td class="mid_child"></td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="label">Products</td>
                            <td class="mid_child no-border"><a href="seller-directory?product_type=Car">Car (<?php if(isset($numberByProductType['Car'])) echo $numberByProductType['Car'];  else echo "0";?>)</a></td>
                            <td class="mid_child no-border"><a href="seller-directory?product_type=Truck">Truck (<?php if(isset($numberByProductType['Truck'])) echo $numberByProductType['Truck'];  else echo "0";?>)</a></td>
                            <td class="mid_child no-border"><a href="seller-directory?product_type=Bus">Bus (<?php if(isset($numberByProductType['Bus'])) echo $numberByProductType['Bus'];  else echo "0";?>)</a></td>
                        </tr>
                        <tr>
                            
                            <td class="mid_child"><a href="seller-directory?product_type=Equipment">Heavy Machine (<?php if(isset($numberByProductType['Equipment'])) echo $numberByProductType['Equipment'];  else echo "0";?>)</td>
                            <td class="mid_child"><a href="seller-directory?product_type=Part">Part (<?php if(isset($numberByProductType['Part'])) echo $numberByProductType['Part'];  else echo "0";?>)</td>
                           
                        </tr>

                    </table>
                </div>
                <div id="tab_car" <?php if($product_type=='Car') echo "style='display:block;'";?>>
                	<table border="0" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td class="label">Country</td>
                            <td class="list" colspan="3">
                            	<ul>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/kr.png" /> <a <?php if($country=='kr') echo "class='selected_filter'";?> href="seller-directory?country=kr&product_type=Car">S. Korea (<?php if(isset($countryNumber['kr'])) echo $countryNumber['kr']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/jp.png" /> <a <?php if($country=='jp') echo "class='selected_filter'";?> href="seller-directory?country=jp&product_type=Car">Japan (<?php if(isset($countryNumber['jp'])) echo $countryNumber['jp']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/us.png" /> <a <?php if($country=='us') echo "class='selected_filter'";?> href="seller-directory?country=us&product_type=Car">U.S.A (<?php if(isset($countryNumber['us'])) echo $countryNumber['us']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/tr.png" /> <a <?php if($country=='tr') echo "class='selected_filter'";?> href="seller-directory?country=tr&product_type=Car">Turkey (<?php if(isset($countryNumber['tr'])) echo $countryNumber['tr'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cl.png" /> <a <?php if($country=='cl') echo "class='selected_filter'";?> href="seller-directory?country=cl&product_type=Car">Chile (<?php if(isset($countryNumber['cl'])) echo $countryNumber['cl'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cn.png" /> <a <?php if($country=='cn') echo "class='selected_filter'";?> href="seller-directory?country=cn&product_type=Car">China(<?php if(isset($countryNumber['cn'])) echo $countryNumber['cn'];  else echo "0";?>)</a></li>
            	
                                </ul>
                            </td>
                        </tr>
                        <tr>
                        	<td class="label">By Condition</td>
                            <td class="mid_child"><a <?php if($condition=='New') echo "class='selected_filter'";?> href="seller-directory?condition=New&product_type=Car">New (<?php if(isset($numberByCondition['New'])) echo $numberByCondition['New']; else echo "0";?>)</a></td>
                            <td class="mid_child"><a <?php if($condition=='Used') echo "class='selected_filter'";?> href="seller-directory?condition=Used&product_type=Car">Used (<?php if(isset($numberByCondition['Used'])) echo $numberByCondition['Used']; else echo "0";?>)</a></td>
                            <td class="mid_child"><a <?php if($condition=='Salvage') echo "class='selected_filter'";?> href="seller-directory?condition=Salvage&product_type=Car">Salvage (<?php if(isset($numberByCondition['Salvage'])) echo $numberByCondition['Salvage']; else echo "0";?>)</a></td>
                        </tr>
                        <tr>
                            <td class="label">By Make</td>
                            <td class="list" colspan="3">
                                <ul>
                                <?php 
                                    foreach($numberByMake as $nmake=>$value){
                                        if(!empty($nmake)&&$value>0){
                                            if($make==$nmake) $selected_filter="class='selected_filter'"; else $selected_filter="";
                                            echo "<li><a $selected_filter href='seller-directory?make={$nmake}&product_type=Car'>$nmake ({$value})</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                        	<td class="label">By Steering</td>
                            <td class="mid_child"><a <?php if($steering=='LHD') echo "class='selected_filter'";?> href="seller-directory?steering=LHD&product_type=Car">Left (<?php if(isset($numberBySteering['LHD'])) echo $numberBySteering['LHD'];  else echo "0";?>)</a></td>
                            <td class="mid_child" colspan="2"><a <?php if($steering=='RHD') echo "class='selected_filter'";?> href="seller-directory?steering=RHD&product_type=Car">Right (<?php if(isset($numberBySteering['RHD'])) echo $numberBySteering['RHD'];  else echo "0";?>)</a></td>
                        </tr>
                         <tr>
                        	<td class="label">By fuel</td>
                            <td class="list" colspan="3">
                            	<ul>
                                	<li><a <?php if($fuel_type=='Gasoline') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Gasoline&product_type=Car">Gasoline(<?php if(isset($numberByFuelType['Gasoline'])) echo $numberByFuelType['Gasoline'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Diesel') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Diesel&product_type=Car">Diesel(<?php if(isset($numberByFuelType['Diesel'])) echo $numberByFuelType['Diesel'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='CNG') echo "class='selected_filter'";?> href="seller-directory?fuel_type=CNG&product_type=Car">CNG(<?php if(isset($numberByFuelType['CNG'])) echo $numberByFuelType['CNG'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Electric') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Electric&product_type=Car">Electric(<?php if(isset($numberByFuelType['Electric'])) echo $numberByFuelType['Electric'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Hybrid Electric') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Hybrid Electric&product_type=Car">Hybrid Electric(<?php if(isset($numberByFuelType['Hybrid Electric'])) echo $numberByFuelType['Hybrid Electric'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Biodiesel') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Biodiesel&product_type=Car">Biodiesel(<?php if(isset($numberByFuelType['Biodiesel'])) echo $numberByFuelType['Biodiesel'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='LPG') echo "class='selected_filter'";?> href="seller-directory?fuel_type=LPG&product_type=Car">LPG(<?php if(isset($numberByFuelType['LPG'])) echo $numberByFuelType['LPG'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Others') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Others&product_type=Car">Others (<?php if(isset($numberByFuelType['Others'])) echo $numberByFuelType['Others'];  else echo "0";?>)</a></li>
                                    
                                </ul>
                            </td>                            
                        </tr>
                        
                        <tr>
                        	<td class="label">By price range</td>
                            <td class="list" colspan="3">
                            	<ul>
                                	<li><a <?php if($max_price=='3000') echo "class='selected_filter'";?> href="seller-directory?max_price=3000&product_type=Car">$3,000 under(<?php echo $numberByMaxPrice['3000'];?>)</a></li>
                                    <li><a <?php if($max_price=='5000') echo "class='selected_filter'";?> href="seller-directory?max_price=5000&product_type=Car">$5,000 under(<?php echo $numberByMaxPrice['5000'];?>)</a></li>
                                    <li><a <?php if($max_price=='10000') echo "class='selected_filter'";?> href="seller-directory?max_price=10000&product_type=Car">$10,000 under(<?php echo $numberByMaxPrice['10000'];?>)</a></li>
                                    <li><a <?php if($min_price=='15000') echo "class='selected_filter'";?> href="seller-directory?min_price=15000&product_type=Car">$15,000 above(<?php echo $numberByMinPrice['15000'];?>)</a></li>                                    
                                </ul>
                            </td>                            
                        </tr>
                        
                    </table>
                </div>
                <div id="tab_truck" <?php if($product_type=='Truck') echo "style='display:block;'";?>>
                	<table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="label">Country</td>
                            <td class="list" colspan="3">
                                <ul>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/kr.png" /> <a <?php if($country=='kr') echo "class='selected_filter'";?> href="seller-directory?country=kr&product_type=Truck">S. Korea (<?php if(isset($countryNumber['kr'])) echo $countryNumber['kr']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/jp.png" /> <a <?php if($country=='jp') echo "class='selected_filter'";?> href="seller-directory?country=jp&product_type=Truck">Japan (<?php if(isset($countryNumber['jp'])) echo $countryNumber['jp']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/us.png" /> <a <?php if($country=='us') echo "class='selected_filter'";?> href="seller-directory?country=us&product_type=Truck">U.S.A (<?php if(isset($countryNumber['us'])) echo $countryNumber['us']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/tr.png" /> <a <?php if($country=='tr') echo "class='selected_filter'";?> href="seller-directory?country=tr&product_type=Truck">Turkey (<?php if(isset($countryNumber['tr'])) echo $countryNumber['tr'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cl.png" /> <a <?php if($country=='cl') echo "class='selected_filter'";?> href="seller-directory?country=cl&product_type=Truck">Chile (<?php if(isset($countryNumber['cl'])) echo $countryNumber['cl'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cn.png" /> <a <?php if($country=='cn') echo "class='selected_filter'";?> href="seller-directory?country=cn&product_type=Truck">China(<?php if(isset($countryNumber['cn'])) echo $countryNumber['cn'];  else echo "0";?>)</a></li>
                
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">By Category</td>
                            <td class="list" colspan="3">
                                <ul>
                                <?php 
                                    foreach($numberByCategory as $ncategory=>$value){
                                        if(!empty($ncategory)&&$value>0){
                                            if($category==$ncategory) $selected_filter="class='selected_filter'"; else $selected_filter="";
                                            echo "<li><a $selected_filter href='seller-directory?category={$ncategory}&product_type=Truck'>$ncategory ({$value})</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                            </td>    
                        </tr>
                        <tr>
                            <td class="label">By Make</td>
                            <td class="list" colspan="3">
                                <ul>
                                <?php 
                                    foreach($numberByMake as $nmake=>$value){
                                        if(!empty($nmake)&&$value>0){
                                            if($make==$nmake) $selected_filter="class='selected_filter'"; else $selected_filter="";
                                            echo "<li><a $selected_filter href='seller-directory?make={$nmake}&product_type=Truck'>$nmake ({$value})</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                            </td>
                        </tr>
                        
                        
                    </table>
                </div>
                <div id="tab_bus" <?php if($product_type=='Bus') echo "style='display:block;'";?>>
                	<table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="label">Country</td>
                            <td class="list" colspan="3">
                                <ul>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/kr.png" /> <a <?php if($country=='kr') echo "class='selected_filter'";?> href="seller-directory?country=kr&product_type=Bus">S. Korea (<?php if(isset($countryNumber['kr'])) echo $countryNumber['kr']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/jp.png" /> <a <?php if($country=='jp') echo "class='selected_filter'";?> href="seller-directory?country=jp&product_type=Bus">Japan (<?php if(isset($countryNumber['jp'])) echo $countryNumber['jp']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/us.png" /> <a <?php if($country=='us') echo "class='selected_filter'";?> href="seller-directory?country=us&product_type=Bus">U.S.A (<?php if(isset($countryNumber['us'])) echo $countryNumber['us']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/tr.png" /> <a <?php if($country=='tr') echo "class='selected_filter'";?> href="seller-directory?country=tr&product_type=Bus">Turkey (<?php if(isset($countryNumber['tr'])) echo $countryNumber['tr'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cl.png" /> <a <?php if($country=='cl') echo "class='selected_filter'";?> href="seller-directory?country=cl&product_type=Bus">Chile (<?php if(isset($countryNumber['cl'])) echo $countryNumber['cl'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cn.png" /> <a <?php if($country=='cn') echo "class='selected_filter'";?> href="seller-directory?country=cn&product_type=Bus">China(<?php if(isset($countryNumber['cn'])) echo $countryNumber['cn'];  else echo "0";?>)</a></li>
                
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">By Condition</td>
                            <td class="mid_child"><a <?php if($condition=='New') echo "class='selected_filter'";?> href="seller-directory?condition=New&product_type=Bus">New (<?php if(isset($numberByCondition['New'])) echo $numberByCondition['New']; else echo "0";?>)</a></td>
                            <td class="mid_child"><a <?php if($condition=='Used') echo "class='selected_filter'";?> href="seller-directory?condition=Used&product_type=Bus">Used (<?php if(isset($numberByCondition['Used'])) echo $numberByCondition['Used']; else echo "0";?>)</a></td>
                            
                        </tr>
                        
                        <tr>
                            <td class="label">By Make</td>
                            <td class="list" colspan="3">
                                <ul>
                                <?php 
                                    foreach($numberByMake as $nmake=>$value){
                                        if(!empty($nmake)&&$value>0)
                                            echo "<li><a href='seller-directory?make={$nmake}&product_type=Truck'>$nmake ({$value})</a></li>";
                                    }
                                ?>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">By fuel</td>
                            <td class="list" colspan="3">
                                <ul>


                                    <li><a <?php if($fuel_type=='Gasoline') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Gasoline&product_type=Bus">Gasoline(<?php if(isset($numberByFuelType['Gasoline'])) echo $numberByFuelType['Gasoline'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Diesel') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Diesel&product_type=Bus">Diesel(<?php if(isset($numberByFuelType['Diesel'])) echo $numberByFuelType['Diesel'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='CNG') echo "class='selected_filter'";?> href="seller-directory?fuel_type=CNG&product_type=Bus">CNG(<?php if(isset($numberByFuelType['CNG'])) echo $numberByFuelType['CNG'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Electric') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Electric&product_type=Bus">Electric(<?php if(isset($numberByFuelType['Electric'])) echo $numberByFuelType['Electric'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Hybrid Electric') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Hybrid Electric&product_type=Bus">Hybrid Electric(<?php if(isset($numberByFuelType['Hybrid Electric'])) echo $numberByFuelType['Hybrid Electric'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Biodiesel') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Biodiesel&product_type=Bus">Biodiesel(<?php if(isset($numberByFuelType['Biodiesel'])) echo $numberByFuelType['Biodiesel'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='LPG') echo "class='selected_filter'";?> href="seller-directory?fuel_type=LPG&product_type=Bus">LPG(<?php if(isset($numberByFuelType['LPG'])) echo $numberByFuelType['LPG'];  else echo "0";?>)</a></li>
                                    <li><a <?php if($fuel_type=='Others') echo "class='selected_filter'";?> href="seller-directory?fuel_type=Others&product_type=Bus">Others (<?php if(isset($numberByFuelType['Others'])) echo $numberByFuelType['Others'];  else echo "0";?>)</a></li>
                                    
                                </ul>
                            </td>                            
                        </tr>
                        
                    </table>
                </div>
                <div id="tab_equipment" <?php if($product_type=='Equipment') echo "style='display:block;'";?>>
                	<table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="label">Country</td>
                            <td class="list" colspan="3">
                                <ul>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/kr.png" /> <a <?php if($country=='kr') echo "class='selected_filter'";?> href="seller-directory?country=kr&product_type=Equipment">S. Korea (<?php if(isset($countryNumber['kr'])) echo $countryNumber['kr']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/jp.png" /> <a <?php if($country=='jp') echo "class='selected_filter'";?> href="seller-directory?country=jp&product_type=Equipment">Japan (<?php if(isset($countryNumber['jp'])) echo $countryNumber['jp']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/us.png" /> <a <?php if($country=='us') echo "class='selected_filter'";?> href="seller-directory?country=us&product_type=Equipment">U.S.A (<?php if(isset($countryNumber['us'])) echo $countryNumber['us']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/tr.png" /> <a <?php if($country=='tr') echo "class='selected_filter'";?> href="seller-directory?country=tr&product_type=Equipment">Turkey (<?php if(isset($countryNumber['tr'])) echo $countryNumber['tr'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cl.png" /> <a <?php if($country=='cl') echo "class='selected_filter'";?> href="seller-directory?country=cl&product_type=Equipment">Chile (<?php if(isset($countryNumber['cl'])) echo $countryNumber['cl'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cn.png" /> <a <?php if($country=='cn') echo "class='selected_filter'";?> href="seller-directory?country=cn&product_type=Equipment">China(<?php if(isset($countryNumber['cn'])) echo $countryNumber['cn'];  else echo "0";?>)</a></li>
                
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">By Category</td>
                            <td class="list" colspan="3">
                                <ul>
                                <?php 
                                    foreach($numberByCategory as $ncategory=>$value){
                                        if(!empty($ncategory)&&$value>0){
                                            if($category==$ncategory) $selected_filter="class='selected_filter'"; else $selected_filter="";
                                            echo "<li><a $selected_filter href='seller-directory?category={$ncategory}&product_type=Equipment'>$ncategory ({$value})</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                            </td>    
                        </tr>
                        
                    </table>
                </div>
                <div id="tab_part" <?php if($product_type=='Part') echo "style='display:block;'";?>>
                	<table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="label">Country</td>
                            <td class="list" colspan="3">
                                <ul>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/kr.png" /> <a <?php if($country=='kr') echo "class='selected_filter'";?> href="seller-directory?country=kr&product_type=Part">S. Korea (<?php if(isset($countryNumber['kr'])) echo $countryNumber['kr']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/jp.png" /> <a <?php if($country=='jp') echo "class='selected_filter'";?> href="seller-directory?country=jp&product_type=Part">Japan (<?php if(isset($countryNumber['jp'])) echo $countryNumber['jp']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/us.png" /> <a <?php if($country=='us') echo "class='selected_filter'";?> href="seller-directory?country=us&product_type=Part">U.S.A (<?php if(isset($countryNumber['us'])) echo $countryNumber['us']; else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/tr.png" /> <a <?php if($country=='tr') echo "class='selected_filter'";?> href="seller-directory?country=tr&product_type=Part">Turkey (<?php if(isset($countryNumber['tr'])) echo $countryNumber['tr'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cl.png" /> <a <?php if($country=='cl') echo "class='selected_filter'";?> href="seller-directory?country=cl&product_type=Part">Chile (<?php if(isset($countryNumber['cl'])) echo $countryNumber['cl'];  else echo "0";?>)</a></li>
                                    <li><img src="<?php echo BASE_RELATIVE;?>images/flag/cn.png" /> <a <?php if($country=='cn') echo "class='selected_filter'";?> href="seller-directory?country=cn&product_type=Part">China(<?php if(isset($countryNumber['cn'])) echo $countryNumber['cn'];  else echo "0";?>)</a></li>
                
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">By Condition</td>
                            <td class="mid_child"><a <?php if($condition=='New') echo "class='selected_filter'";?> href="seller-directory?condition=New&product_type=Part">New (<?php if(isset($numberByCondition['New'])) echo $numberByCondition['New']; else echo "0";?>)</a></td>
                            <td class="mid_child"><a <?php if($condition=='Used') echo "class='selected_filter'";?> href="seller-directory?condition=Used&product_type=Part">Used (<?php if(isset($numberByCondition['Used'])) echo $numberByCondition['Used']; else echo "0";?>)</a></td>
                            <td class="mid_child"><a <?php if($condition=='Remade') echo "class='selected_filter'";?> href="seller-directory?condition=Remade&product_type=Part">Remade (<?php if(isset($numberByCondition['Remade'])) echo $numberByCondition['Remade']; else echo "0";?>)</a></td>
                        </tr>
                        <tr>
                            <td class="label">By Category</td>
                            <td class="list" colspan="3">
                                <ul>
                                <?php 
                                    foreach($numberByCategory as $ncategory=>$value){
                                        if(!empty($ncategory)&&$value>0){
                                            if($category==$ncategory) $selected_filter="class='selected_filter'"; else $selected_filter="";
                                            echo "<li><a $selected_filter href='seller-directory?category={$ncategory}&product_type=Part'>$ncategory ({$value})</a></li>";
                                        }
                                    }
                                ?>
                                </ul>
                            </td>    
                        </tr>
                        
                    </table>
                </div>
                
                <?php 
                if(count($traderList)>0){
                foreach($traderList as $trader){
                ?>
                <div class="seller_wrap clearfix">
                	<div class="left_item">
                    <?php 
                        $image_src="images/seller-directory/empty.jpg";
                        if(!empty($trader['image'])&&file_exists("upload/thumb/".$trader['image'])){
                            $image_src="upload/thumb/".$trader['image'];
                        }
                    ?>
                    	<a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade"><img src="<?php echo $image_src;?>" /></a>
                        <p><a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade">Visit Seller's Homepage</a></p>
                    </div>
                    <div class="right_item">
                    	<p class="title"><a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade"><?php echo $trader['company_name'];?></a></p>
                        <ul>
                        	<li><img src="<?php echo BASE_RELATIVE;?>images/flag/<?php echo $trader['country'];?>.png" /> <?php echo $trader['country_name'];?></li>
                            <li>ID : <?php echo $trader['card_id'];?></li>
                            <li>Name : <?php echo $trader['name'];?></li>                            
                            <?php
								if($trader['messengerID'] != ''){
							?>
									<li>Skype : <a href="skype:<?php echo $trader['messengerID'];?>?add"><?php echo $trader['messengerID'];?></a></li>
							<?php
								}
							?>
                            <li>Email : <?php echo $trader['email'];?></li>
                            <li>
                            <?php 
                            $productNumber=$_CLASS->getProductNumber($trader['id']);
                            echo "Products: ";
                            foreach($productNumber as $pn){
                                echo "<a href='vehicle?product_type={$pn['product_type']}&owner={$trader['id']}'><u>".$pn['product_type']."({$pn['number']})</u></a> ";
                            } ?>
                            </li>
                        </ul>
                        <div class="data_detail">
                        	<p>
                            	<?php echo $trader['introduction'];?>
                            </p>
                            <br/>
                            <p class="more"><a href="company-detail?id=<?php echo $trader['id'];?>" class="linkfade"><< More >></a></p>
                        </div>
                    </div>
                </div>
                <?php }}else{
                    echo "<div style='padding-top:20px;text-align:center;'><font color='red'>..:: Sorry there is no sellers found for your inquiry at the moment! ::..</font></div>";
                }

                ?>

                <div id="pagernation">
                     <?php 
      
                         echo $pagination;
                                            
                    ?>
                      
                </div>
                      
        	</div><!-- end div id="sectionContent" -->  
        </div>      
        