<?php
/*
 * If you need to use CKEditor in this page, make sure to set the controller
 * variable to TRUE and set it's ID. Otherwise, just leave it at FALSE.
 */
$_CKEDITOR_ENABLED = true;
$_CKEDITOR_ID = 'ebody';
/*
 * VALIDATE ADMINISTRATOR GROUP ------------------------------------------------
 */
if( $_SESSION['log_group'] == 'admin' )
{
    // get form feed.
    $fstatus = $_CLASS->getFormStatus();
    $fmessage = $_CLASS->getFormMessage();
    $fstyle = $_CLASS->getFormStyle();
    
    // get newsletter list.
    $list = $_CLASS->getNewsletter();
?>
        <div id="content-wrapper">
            <div id="vertical-menu">
                <?php $_MENU->loadMenu($_GLANG); ?>
            </div>
            <div id="inner-content">
                <!-- START CONTENT -->
                <h1><?php echo $_LANG['NEWSLETTER_HEADER'];?></h1>
                <?php
                    if( $fstatus ){
                ?>
                <div class="alert <?php echo $fstyle;?>">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?php echo $fmessage;?>
                </div>
                <?php
                    }
                    
                    // display form.
                    if( count($list) < 1 ){
                ?>
                <div class="alert alert-info">
                    <?php echo $_LANG['NEWSLETTER_NOUSER_LABEL'];?>
                </div>
                <?php
                    }
                    else {
                ?>
                <p><?php echo $_LANG['NEWSLETTER_HEADER_LABEL'];?></p>
                <form method="post">
                    <div class="input-prepend">
                        <span class="add-on" style="width:120px;"><?php echo $_LANG['NEWSLETTER_SUBJECT_LABEL'];?></span>
                        <input type="text" name="subjectInput" style="width:585px;" />
                    </div>
                    <textarea id="ebody" name="msgInput"><?php echo $_CLASS->getEmailTemplate();?></textarea>
                    <div class="clearfix"></div>
                    <br>
                    <input type="submit" name="sendbtn" class="btn btn-small btn-info" value="<?php echo $_LANG['NEWSLETTER_SEND_BUTTON'];?>" />
                </form>
                <?php
                    }
                ?>
                <!-- END CONTENT -->
            </div>
            <div class="clearfix"></div>
        </div>
<?php
} // end validation.
else
{
?>
        <p class="text-warning"><?php echo $_LOCAL['UNAUTHORIZED_ACCESS_MESSAGE'];?></p>
<?php
}